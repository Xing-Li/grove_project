import _ from 'lodash';
import isGraphXrDarkMode from 'util/isGraphXrDarkMode';
import actions from './define/Actions';
import { NoticeOptions, ReadWriteOptions, ReadWriteOptionMapping } from 'util/utils';

const { DefaultDatabase, DefaultSecret } = require('./util/databaseApi');

export const DrawerType = {
    None: { index: 0, w: window.innerWidth / 4 },
    Folder: { index: 1, w: window.innerWidth / 3 },
    Help: { index: 2, w: window.innerWidth / 2 },
    Settings: { index: 3, w: window.innerWidth / 2 },
    Panels: { index: 4, w: window.innerWidth * 3 / 4 },
    FileAttachments: { index: 5, w: window.innerWidth / 3 },
    ImportModules: { index: 6, w: window.innerWidth / 3 },
    FindAndReplace: { index: 7, w: window.innerWidth / 4 },
}
export function verifyW(width) {
    return width > window.screen.availWidth ? window.screen.availWidth : width;
}
export function verifyH(heidht) {
    return heidht > window.screen.availHeight ? window.screen.availHeight : width;
}
const query = new URLSearchParams(
    window.location.href.substring(window.location.href.indexOf("?") + 1)
);
const urlReadWriteMode = query.get("blockMode");
export const InitSettings = {
    theme: "eclipse",
    readOnlyChecked: false,
    pinCode: undefined,
    drawerType: DrawerType,
    published: [],
    extension: true,
    widthScale: 100,
    heightScale: 100,
    autoSaveInSecond: 10,
    autoSave: false,
    readwrite_mode: (urlReadWriteMode && ReadWriteOptionMapping[urlReadWriteMode]) || ReadWriteOptions[1],
    notice_mode: NoticeOptions[1],
    lineNumbers: false,
    vimEnabled: false,
    darkMode: false,
    autoFormat: false,
    mask: false,
    usersSharePermission: {},
    /**@type{[DefaultDatabase]} */
    databases: [],
    /**@type{[DefaultSecret]} */
    secrets: [],
    rowLimiting: true,
}
export default class EditorSettings {

    constructor() {
        this.lineNumbers = InitSettings.lineNumbers;
        this.vimEnabled = InitSettings.vimEnabled;
        this.autoFormat = InitSettings.autoFormat;
        this.theme = InitSettings.theme;
    }

    /**
     * 
     * @param {InitSettings} settings 
     */
    loadSettings(settings) {
        this.readOnlyChecked = undefined === settings.readOnlyChecked ? InitSettings.readOnlyChecked : settings.readOnlyChecked;
        actions.variable(actions.types.READONLY_CHECKED, [], () => {
            return this.readOnlyChecked;
        })
        this.darkMode = isGraphXrDarkMode();
        actions.variable(actions.types.DARK_MODE, [], () => {
            return this.darkMode;
        })
        this.theme = undefined === settings.theme ? InitSettings.theme : settings.theme;
        actions.variable(actions.types.THEME, [], () => this.theme);
        this.pinCode = settings.pinCode || InitSettings.pinCode;
        for (let key in DrawerType) {
            _.assign(DrawerType[key], settings.drawerType && settings.drawerType[key] || {});
        }
        this.published = new Set(settings.published instanceof Array ? settings.published : _.cloneDeep(InitSettings.published));
        this.extension = undefined === settings.extension ? InitSettings.extension : settings.extension;
        this.widthScale = settings.widthScale || InitSettings.widthScale;
        this.heightScale = settings.heightScale || InitSettings.heightScale;
        actions.variable(actions.types.RESIZE_WINDOW, [], () => {
            return {
                widthScale: this.widthScale,
                heightScale: this.heightScale
            }
        })
        this.autoSaveInSecond = settings.autoSaveInSecond || InitSettings.autoSaveInSecond;
        actions.variable(actions.types.AUTO_SAVE_IN_SECOND, [], () => {
            return this.autoSaveInSecond;
        })
        this.autoSave = undefined === settings.autoSave ? InitSettings.autoSave : settings.autoSave;
        actions.variable(actions.types.AUTO_SAVE, [], () => {
            return this.autoSave;
        })
        this.readwrite_mode = settings.readwrite_mode || InitSettings.readwrite_mode;
        actions.variable(actions.types.READWRITE_MODE, [], () => {
            return this.readwrite_mode;
        })
        this.notice_mode = settings.notice_mode || InitSettings.notice_mode;
        actions.variable(actions.types.NOTICE_MODE, [], () => {
            return this.notice_mode;
        })
        this.usersSharePermission = settings.usersSharePermission || InitSettings.usersSharePermission;
        actions.variable(actions.types.USERS_SHARE_PERMISSION, [], () => {
            return this.usersSharePermission;
        })
        this.lineNumbers = undefined === settings.lineNumbers ? InitSettings.lineNumbers : settings.lineNumbers;
        actions.variable(actions.types.LINE_NUMBERS, [], () => {
            return this.lineNumbers;
        })
        this.vimEnabled = undefined === settings.vimEnabled ? InitSettings.vimEnabled : settings.vimEnabled;
        actions.variable(actions.types.VIM_ENABLED, [], () => {
            return this.vimEnabled;
        })
        this.autoFormat = undefined === settings.autoFormat ? InitSettings.autoFormat : settings.autoFormat;
        actions.variable(actions.types.AUTO_FORMAT, [], () => {
            return this.autoFormat;
        })
        this.mask = undefined === settings.mask ? InitSettings.mask : settings.mask;
        actions.variable(actions.types.MASK, [], () => {
            return this.mask;
        })
        /**@type{[DefaultDatabase]} */
        this.databases = settings.databases || InitSettings.databases;
        actions.variable(actions.types.DATABASES, [], () => {
            return this.databases;
        })
        /**@type{[DefaultSecret]} */
        this.secrets = settings.secrets || InitSettings.secrets;
        actions.variable(actions.types.SECRETS, [], () => {
            return this.secrets;
        })
        this.rowLimiting = undefined === settings.rowLimiting ? InitSettings.rowLimiting : settings.rowLimiting;
    }

    /**
     * 
     */
    getSettings() {
        return {
            theme: this.getTheme(),
            readOnlyChecked: this.getReadOnlyChecked(),
            drawerType: DrawerType,
            published: Array.from(this.published.keys()),
            extension: this.getExtension(),
            widthScale: this.getWidthScale(),
            heightScale: this.getHeightScale(),
            lineNumbers: this.getLineNumbers(),
            vimEnabled: this.getVimEnabled(),
            darkMode: this.getDarkMode(),
            autoFormat: this.getAutoFormat(),
            mask: this.getMask(),
            autoSave: this.getAutoSave(),
            autoSaveInSecond: this.getAutoSaveInSecond(),
            notice_mode: this.getNotice_mode(),
            usersSharePermission: this.getUsersSharePermission(),
            databases: this.getDatabases(),
            secrets: this.getSecrets(),
        }
    }
    getReadwrite_mode() {
        return this.readwrite_mode || InitSettings.readwrite_mode;
    }
    setReadwriteMode(readwrite_mode) {
        this.readwrite_mode = readwrite_mode;
    }
    getUsersSharePermission() {
        return this.usersSharePermission || InitSettings.usersSharePermission;
    }
    setUsersSharePermission(usersSharePermission) {
        this.usersSharePermission = usersSharePermission;
    }
    getDatabases() {
        return this.databases || InitSettings.databases;
    }
    setDatabases(databases) {
        this.databases = databases;
    }
    getSecrets() {
        return this.secrets || InitSettings.secrets;
    }
    setSecrets(secrets) {
        this.secrets = secrets;
    }
    getRowLimiting() {
        return undefined === this.rowLimiting ? InitSettings.rowLimiting : this.rowLimiting;
    }
    getHeightScale() {
        return this.heightScale;
    }
    setHeightScale(heightScale) {
        this.heightScale = heightScale;
    }
    getWidthScale() {
        return this.widthScale;
    }
    setWidthScale(widthScale) {
        this.widthScale = widthScale;
    }
    getTheme() {
        return this.theme;
    }
    setTheme(theme) {
        this.theme = theme;
    }
    getExtension() {
        return this.extension;
    }
    setExtension(extension) {
        this.extension = extension;
    }
    getReadOnlyChecked() {
        return this.readOnlyChecked;
    }
    setReadOnlyChecked(readOnlyChecked) {
        this.readOnlyChecked = readOnlyChecked;
    }
    getLineNumbers() {
        return this.lineNumbers;
    }
    setLineNumbers(lineNumbers) {
        this.lineNumbers = lineNumbers;
    }
    getVimEnabled() {
        return this.vimEnabled;
    }
    setVimEnabled(vimEnabled) {
        this.vimEnabled = vimEnabled;
    }
    getDarkMode() {
        return this.darkMode;
    }
    setDarkMode(darkMode) {
        this.darkMode = darkMode;
    }
    getAutoFormat() {
        return this.autoFormat;
    }
    setAutoFormat(autoFormat) {
        this.autoFormat = autoFormat;
    }
    getMask() {
        return this.mask;
    }
    setMask(mask) {
        this.mask = mask;
    }
    getNotice_mode() {
        return this.notice_mode || InitSettings.notice_mode;
    }
    setNotice_mode(notice_mode) {
        this.notice_mode = notice_mode;
    }
    getAutoSave() {
        return this.autoSave;
    }
    setAutoSave(autoSave) {
        this.autoSave = autoSave;
    }
    getAutoSaveInSecond() {
        return this.autoSaveInSecond;
    }
    setAutoSaveInSecond(autoSaveInSecond) {
        this.autoSaveInSecond = autoSaveInSecond;
    }
}