import { macMetaOrCtrl } from "util/utils";

/**
 * 
 * @param {string} fileKey 
 * @param {string} text 
 */
export default function link(fileKey, text) {
    let divEle = document.createElement("a");
    divEle.classList.add("link", "clicker");
    divEle.innerHTML = String(text || (~fileKey.lastIndexOf("/") ?
        fileKey.substring(fileKey.lastIndexOf("/") + 1) : fileKey));
    divEle.title = fileKey;
    if (/^(\w+:)|\/\//i.test(fileKey)) {
        divEle.href = fileKey;
    } else if (/^[.]{0,2}\//i.test(fileKey)) {
        divEle.href = fileKey;
    } else if (!fileKey.length || /^[\s.]/.test(fileKey) || /\s$/.test(fileKey)) {
        throw new Error("Link has illegal name");
    } else if (/^\$/.test(fileKey)) {
        divEle.href = fileKey.replace(/^\$/, "/static/commonLibs/");
    } else {
        divEle.addEventListener("click", (e) => {
            let fromModuleName = fileKey;
            let replaceEditor;
            if (macMetaOrCtrl(e) && !e.shiftKey && !e.altKey) {
                replaceEditor = undefined;
            } else {
                replaceEditor = window.currentEditor;
            }
            window.currentEditor.switchTagEditor(fromModuleName, replaceEditor);
        })
    }
    return divEle;
}

export function Link(props = { fileKey: "", text: "" }) {
    const { fileKey, text } = props;
    let href;
    let innerHTML = String(text || (~fileKey.lastIndexOf("/") ?
        fileKey.substring(fileKey.lastIndexOf("/") + 1) : fileKey));
    if (/^(\w+:)|\/\//i.test(fileKey)) {
        href = fileKey;
    } else if (/^[.]{0,2}\//i.test(fileKey)) {
        href = fileKey;
    } else if (!fileKey.length || /^[\s.]/.test(fileKey) || /\s$/.test(fileKey)) {
        throw new Error("illegal name");
    } else if (/^\$/.test(fileKey)) {
        href = fileKey.replace(/^\$/, "/static/commonLibs/");
    } else {
        return <a className="link clicker" onClick={(e) => {
            let fromModuleName = fileKey
            let replaceEditor;
            if (macMetaOrCtrl(e) && !e.shiftKey && !e.altKey) {
                replaceEditor = undefined;
            } else {
                replaceEditor = window.currentEditor;
            }
            window.currentEditor.switchTagEditor(fromModuleName, replaceEditor);
        }}>{innerHTML}</a>
    }
    return <a className="link clicker" href={href}>{innerHTML}</a>
}
export function createLinkElement(fromModuleName) {
    let divEle = document.createElement("a");
    divEle.classList.add("link");
    divEle.innerHTML = String(fromModuleName).bold();
    divEle.title = decodeURI([
        `${fromModuleName.replace('%', "%25")}`,
        `Click to open it in current editor`,
        `Ctrl + Click to open it in new editor`
    ].join("%0A"))
    divEle.addEventListener("click", (e) => {
        let editor = window.currentEditor;
        let replaceEditor;
        if (macMetaOrCtrl(e) && !e.shiftKey && !e.altKey) {
            replaceEditor = undefined;
        } else {
            replaceEditor = editor;
        }
        editor && editor.switchTagEditor(fromModuleName, replaceEditor);
    })
    return divEle;
}