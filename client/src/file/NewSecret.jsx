import {
    Button, Select, Space, Form,
    Input,
    Radio,
    Cascader,
    DatePicker,
    InputNumber,
    TreeSelect,
    Switch,
    Row,
    Col,
} from 'antd';
import Text from 'antd/lib/typography/Text';
import axios from 'axios';
import _ from 'lodash';
import PropTypes from "prop-types";
import React from 'react';
import MdFiles from './MdFiles';
import Modal from 'antd/lib/modal/Modal';
import { WIDTH, Z_INDEX, Title } from './fileUtils.js';
import { ModalType, showToast, DefaultSecret } from '../util/utils.js';

const { Option } = Select;
export default class NewSecret extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        width: PropTypes.number,
        react_component: PropTypes.object,
    };
    static defaultProps = {};

    constructor(props) {
        super(props);
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
        /**@type {DefaultSecret} */
        let secret = react_component.state.modalData.secret;
        let newState = () => {
            return {
                modalName: ModalType.NewSecret,
                name: "",
                value: "",
            }
        }
        this.state = secret ? { modalName: "Edit Secret", ...secret } : newState();
    }

    componentDidMount() {
    }

    componentDidUpdate(prevProps, prevState) {
    }

    handleOk = () => {
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
    };

    onFormLayoutChange = ({ size }) => {
        setComponentSize(size);
    };

    render() {
        const { className, width } = this.props;
        const { name, value, modalName } = this.state;
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
        const { modalType } = react_component.state;
        const closeFunc = () => {
            react_component.setState({
                modalType: ModalType.None,
            });
        };
        const onFinish = (values) => {
            console.log('Success:', values);
            const { react_component } = this.props;
            const { modalData: { cb } } = react_component.state;
            cb(values);
            closeFunc();
        };
        const onFinishFailed = (errorInfo) => {
            showToast(_.reduce(errorInfo.errorFields, (prev, curr, index) => {
                prev += curr.errors.join(",")
                return prev + "; ";
            }, ""), 'error')
            console.log('Failed:', errorInfo);
        };
        return <Modal zIndex={Z_INDEX}
            className={`modal-comp data-html2canvas-ignore ${className}`}
            width={width || WIDTH}
            title={<div><h4>{modalName}</h4></div>}
            visible={modalType !== ModalType.None}
            onCancel={closeFunc}
            footer={null}
        >
            <Form layout={"vertical"}
                initialValues={this.state}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                    <Col className="gutter-row" span={{ md: 24, lg: 12 }}>
                        <Form.Item label="Name" name="name" rules={[
                            {
                                required: true,
                            },
                        ]}>
                            <Input readOnly={modalName === ModalType.NewSecret ? false : true}
                                onChange={(e) => {
                                    this.setState({ name: e.target.value })
                                }} />
                        </Form.Item>
                    </Col>
                    <Col className="gutter-row" span={{ md: 24, lg: 12 }}>
                        <Form.Item label="Value" name="value" rules={[
                            {
                                required: true,
                            },
                        ]}>
                            <Input onChange={(e) => {
                                this.setState({ value: e.target.value })
                            }} />
                        </Form.Item>
                    </Col>
                </Row>
                <Form.Item key="back"  >
                    <div className="d-flex justify-content-end">
                        <Button key="back" onClick={closeFunc}>
                            Cancel
                        </Button>&nbsp;&nbsp;
                        <Button key="addNew" type="primary" htmlType="submit">
                            {modalName === ModalType.NewSecret ? "Add New" : "Save Changes"}
                        </Button>
                    </div>
                </Form.Item>
            </Form>
        </Modal>
    }
}
