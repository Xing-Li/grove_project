import { EllipsisOutlined, UserOutlined } from '@ant-design/icons';
import { Card, Dropdown, Form, List, Avatar, Button, Input, Menu, Select, Tabs, Affix } from 'antd';
import axios from 'axios';
import cx from "classnames";
import PropTypes from "prop-types";
import React from 'react';
import { DndProvider, DragSource, DropTarget } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import editor from './commonEditor';
import { isNotLogin, PROJECT_ID, USER_ID, DefaultShareData, ShapeShared, DEFAULT_VER, TAG_TYPE, getSkey, FILESDIR, SYSTEM_USER } from './util/localstorage';
import { ChevronDownIcon } from 'block/code/Icon';

const { Search } = Input;
const { showToast } = require("./util/utils");
const { SHAREDS_TYPE, ShareProjectCanOptions } = require("./util/MsgType");
const { codeTime, ImageType } = require("./util/helpers");
const { getFileKey, getFileType } = require("./file/fileUtils");

const { Option } = Select;
const { Meta } = Card;
const { TabPane } = Tabs;
const cols = { xs: 1, sm: 2, md: 4, lg: 4, xl: 6, xxl: 6 }
const getFileName = (fileKey) => {
    return (fileKey && ~fileKey.lastIndexOf('/')) ? fileKey.substring(fileKey.lastIndexOf('/') + 1) : fileKey
}
const TABS = {
    Overview: "Overview",
    Share: "Share",
    Mostliked: "Most liked",
}
const DisplayType = {
    grid: "grid",
    list: "list"
}
const ONE_PAGE_NUM = 24;
export default class Panels extends React.Component {
    static propTypes = {
        className: PropTypes.string,
    };
    static defaultProps = {};
    constructor(props) {
        super(props);
        const { className } = this.props;
        const pubOptions = [
            { value: "", label: "All" },
            {
                value: PROJECT_ID,
                label: "Current Project"
            },
        ];
        this.state = {
            className: className,
            readOnly: editor.readOnly,
            activeKey: isNotLogin() ? TABS.Overview : TABS.Share,
            pubOptions: pubOptions,
            /**search */
            published: pubOptions[0].value,
            cloud: [],
            shareds: [],
            displayType: DisplayType.grid,
            initLoading: true,
            loading: false,
            condition: {
                search: {},
                option: {},
                multiOptions: {},
                page: 0,
                items: ONE_PAGE_NUM,
                timeRange: [],
                timeRangeName: "createTime",
                sort: { createTime: -1 },
            },
            end: false,
        }
        this.ref = React.createRef();
    }

    getGrid = (w) => {
        if (w >= 1600) {
            return "xxl"
        } else if (w >= 1200) {
            return "xl";
        } else if (w >= 992) {
            return "lg"
        } else if (w >= 768) {
            return "md"
        } else if (w >= 576) {
            return "sm"
        } else {
            return "xs"
        }
    }

    componentDidMount() {
        this.refreshTabs()
        let self = this;
        let last_known_scroll_position = 0;
        this.ticking = false;
        let targetEle = document.querySelector(".drawer-div .ant-drawer-body");
        this.scrollFunc = function (ev) {
            last_known_scroll_position = targetEle.scrollTop;
            if (targetEle.clientHeight + targetEle.scrollTop + 1 >= targetEle.scrollHeight) {
                if (!self.ticking) {
                    self.ticking = true;
                    window.requestAnimationFrame(async function () {
                        // Do something with the scroll position
                        // console.log("i am bottom!");
                        const { published } = self.state;
                        await self.handleChange(published, undefined, true);
                        self.ticking = false;
                    });
                }
            }
        };
        targetEle.addEventListener("scroll", this.scrollFunc);
    }

    componentWillUnmount() {
        let targetEle = document.querySelector(".drawer-div .ant-drawer-body");
        targetEle.removeEventListener("scroll", this.scrollFunc)
        this.unmount = true;
    }

    refreshTabs = () => {
        const { published, activeKey, shareds, nPerPage } = this.state;
        if (activeKey === TABS.Overview) {
            this.handleChange(published, undefined);
        } else if (activeKey === TABS.Share) {
            this.getAllShareds();
        }
    }

    getAllShareds = async (stateTmp) => {
        let state = stateTmp || {};
        let res = await axios.post(`/api/grove/getAllShareds`, {
            toUserId: USER_ID,
            type: SHAREDS_TYPE.Shared_To_User
        });
        if (this.unmount) {
            return;
        }
        if (!res.data.status) {
            _.assign(state, { shareds: res.data.content })
            !stateTmp && this.setState(state);
        }
        res.data.message && showToast(res.data.message, res.data.status ? "error" : "success");
    }


    componentDidUpdate(preProps, preState) {
        const { className } = this.props;
        if (preProps.className !== className) {
            this.setState({ className: className })
        }
    }

    handleChange = async (published, stateTmp, turnpage = false) => {
        const { condition, end } = this.state;
        let state = stateTmp || {};
        if (turnpage) {
            if (end) {
                return showToast("There's no more page.");
            }
            condition.page += 1;
            condition.items = 8
        } else {
            condition.page = 0;
            condition.items = ONE_PAGE_NUM
            if (published) {
                condition.option.projectId = published;
            } else {
                delete condition.option.projectId;
            }
        }
        _.assign(state, { condition });
        let res = await axios.post(`/api/grove/getAllShareProjects`, {
            projectId: published,
            condition
        });
        if (this.unmount) {
            return;
        }
        if (!res.data.status) {
            const { cloud } = this.state;
            if (condition.page !== 0) {
                _.assign(state, { cloud: _.concat(cloud, res.data.content), end: res.data.content.length < condition.items })
            } else {
                _.assign(state, { cloud: res.data.content, end: res.data.content.length < condition.items })
            }
            !stateTmp && this.setState(state);
        }
        res.data.message && showToast(res.data.message, res.data.status ? "error" : "success");
    }

    onValuesChange = async (published) => {
        const { } = this.state;
        let state = { published }
        await this.handleChange(published, state);
        this.setState(state);
    }

    onSearch = async (value) => {
        const { published, condition } = this.state;
        condition.search = value ? { fileName: value, title: value } : {}
        await this.handleChange(published, undefined);
    }

    DirComp = (props) => {
        const { src } = props;
        return (<div className="relative collection-thumbnail"  >
            <div className="absolute bg-white ba b--silver br2 collect-1">
            </div>
            <div className="absolute bg-white ba b--silver br2 collect-2"
            >
            </div>
            <div className="absolute top-0 left-0 ba b--silver br2 collect-3"  >
                <img className="absolute top-0 left-0 w-100 h-100" loading="lazy"
                    src={src} />
            </div>
        </div>)
    }

    /**
     * 
     * @param {*} info 
     * @param {DefaultShareData} transItem 
     */
    menuClickHandler = async (info, transItem) => {
        let item = _.cloneDeep(transItem);
        item.userId = item.userId._id;
        switch (info.key) {
            case "view":
                this.openTabFunc(transItem.tag, transItem);
                break;
            case "fork":
                item.name = getFileName(item.fileKey);
                item.type = getFileType(item.fileKey);
                editor.fork(item);
                break;
            case "unpublish":
                break;
            case "SetPreviewImage":
                await this.openFunc(transItem.tag, transItem);
                await editor.react_component.setPreviewImageFunc(editor
                    .react_component.state.fileNamesJson[item.fileKey])
                break;
        }
    }
    /** @param {ShapeShared} item */
    openFunc = async (tag, item) => {
        let info = this.transferInfo(tag, item);
        await editor.handleSearch(info, false);
    }
    /**
     * 
     * @param {*} tag 
     * @param {*} item 
     * @returns {DefaultShareData}
     */
    transferInfo = (tag, item) => {
        /** @type {DefaultShareData} */
        let info = _.cloneDeep(item);
        info.userId = item.userId._id;
        info.userName = `${item.userId.firstName} ${item.userId.lastName}`;
        info.email = item.userId.email;
        info.tag = tag;
        info.name = getFileName(info.fileKey);
        info.type = getFileType(info.fileKey);
        if (tag === TAG_TYPE.shared) {
            info.sharedId = item._id;
            info.version = DEFAULT_VER;
        }
        return info;
    }

    openTabFunc = async (tag, item) => {
        let info = this.transferInfo(tag, item);
        if (info.userId !== SYSTEM_USER) {
            let res = await axios.post(`/api/grove/linkTo`, info);
            if (this.unmount) {
                return;
            }
            if (!res.data.status) {
                if (res.data.content instanceof Object) {
                    info.attachments = res.data.content;
                }
                info.userName = res.data.userName;
                info.email = res.data.email;
            }
        }
        await editor.switchLinkTagEditor(info)
    }

    ListComp = (props) => {
        const { datas, tag } = props;
        const { displayType } = this.state;
        const UserFunc = (item) => (<div className="card-file-title" title={item.userId.email}>
            <UserOutlined className="icon icon-muted icon-xs" />
            {item.userId._id === USER_ID ? (<span className="current_user">(You)</span>) : `${item.userId.firstName} ${item.userId.lastName}`}
        </div>)
        const MenuFunc = /** @param {ShapeShared} item */(item) => (<Dropdown arrow={true} overlay={
            <Menu onClick={(info) => {
                item.tag = tag;
                this.menuClickHandler(info, item)
            }}>
                {item.status >= ShareProjectCanOptions["View"] && <Menu.Item key="view">
                    <i className="icon fas fa-external-link-square-alt"></i>view
                </Menu.Item>}
                {item.status >= ShareProjectCanOptions["Forks"] && <Menu.Item key="fork">
                    <i className="icon fas fa-code-branch"></i>fork
                </Menu.Item>}
                {item.userId._id === USER_ID && item.projectId === getSkey() && <Menu.Item key="SetPreviewImage">
                    <i className="icon far fa-image"></i>Set Preview Image
                </Menu.Item>}
            </Menu>
        } getPopupContainer={() => this.ref.current} trigger={['hover']}>
            <EllipsisOutlined
                onClick={e => e.preventDefault()} className="icon" title="more" />
        </Dropdown>)
        const TimeFunc = (item) => (<div className="f7 mid-gray"><i className={cx(`f7 icon icon-muted icon-xs`,
            { "fas fa-globe": tag === TAG_TYPE.published },
            { "fas fa-share-alt-square": tag === TAG_TYPE.shared }
        )} title={`${tag}`}></i>{`${tag} ${codeTime(new Date().getTime(), new Date(item.createTime).getTime())}`}</div>)
        if (displayType === DisplayType.grid) {
            return <List
                grid={{
                    gutter: 16
                }}
                dataSource={datas}
                renderItem={ /** @param {ShapeShared} item */ (item) => {
                    let fileName = getFileName(item.fileKey);
                    let imageFileKey = getFileKey(getFileKey(item.fileKey, FILESDIR), fileName);
                    let imageURL = `${item.uploadUri}${imageFileKey}${ImageType}`
                    let version = (tag === TAG_TYPE.shared) ? DEFAULT_VER : item.version;
                    return <List.Item>
                        <Card className="card-file" hoverable
                            cover={<div className="image-wrap  bg-img"
                                style={{ backgroundImage: `url(${imageURL})` }} onClick={() => this.openFunc(tag, item)}>
                            </div>}
                        >
                            <Meta
                                title={<div>
                                    <div className="truncate f7 mid-gray d-flex justify-content-between">
                                        <div className="blablano" title={`${item.fileKey}@${version}`}>{`${fileName}@${version}`}</div>
                                        <div className="blablano" title="Project Name"><i className="icon icon-muted icon-xs fas fa-project-diagram"></i>{item.projectId === PROJECT_ID ?
                                            (<span className="current_user">(Current Project)</span>) : item.projectName}</div>
                                    </div>
                                    <div className="d-flex justify-content-between">
                                        {UserFunc(item)}
                                        {MenuFunc(item)}
                                    </div>
                                </div>}
                                description={TimeFunc(item)} />
                        </Card>
                    </List.Item>
                }}
            />
        } else {
            return <List
                className="demo-loadmore-list"
                itemLayout="horizontal"
                dataSource={datas}
                renderItem={ /** @param {ShapeShared} item */ item => {
                    let fileName = getFileName(item.fileKey);
                    let imageFileKey = getFileKey(getFileKey(item.fileKey, FILESDIR), fileName);
                    let imageURL = `${item.uploadUri}${imageFileKey}${ImageType}`
                    let version = (tag === TAG_TYPE.shared) ? DEFAULT_VER : item.version;
                    return <List.Item
                        actions={[MenuFunc(item)]}
                    >
                        <List.Item.Meta
                            avatar={
                                <Avatar className="cursorp" shape="square" size={64} src={imageURL}
                                    onClick={() => this.openFunc(tag, item)} />
                            }
                            title={<div className="cursorp"
                                onClick={() => this.openFunc(tag, item)} title={`${item.fileKey}@${version}`}>{`${fileName}@${version}`}</div>}
                            description={
                                <div className="d-flex">
                                    {/* <div title="Project Name"><i className="icon icon-muted icon-xs fas fa-project-diagram"></i>{item.projectId === PROJECT_ID ?
                                        (<span className="current_user">(Current Project)</span>) : item.projectName}</div> */}
                                    {UserFunc(item)}
                                    {TimeFunc(item)}
                                </div>
                            }
                        />
                    </List.Item>
                }}
            />
        }
    }

    render() {
        const { cloud, shareds, className, pubOptions, published, activeKey, displayType, end } = this.state;
        const displays = <div>
            <i className={`icon fas fa-th ${displayType !== DisplayType.grid ? "icon-muted" : ""}`} onClick={() => {
                this.setState({ displayType: DisplayType.grid });
            }}></i>
            <i className={`icon fas fa-th-list ${displayType !== DisplayType.list ? "icon-muted" : ""}`} onClick={() => {
                this.setState({ displayType: DisplayType.list });
            }}></i>
        </div>;
        return <div ref={this.ref} className={`${className || ''}`} >
            <DraggableTabs style={{ paddingTop: "32px" }} tabBarExtraContent={displays}
                onChange={(activeKey) => {
                    this.setState({ activeKey }, () => {
                        this.refreshTabs()
                    })
                }}
                activeKey={activeKey}
            >
                <TabPane tab={TABS.Overview} key={TABS.Overview}>
                    <Affix className={'folder-affix'} target={() => document.querySelector(".drawer-div .ant-drawer-body")} offsetTop={0}>
                        <div>
                            <div className="d-flex justify-content-between align-items-center">
                                Published
                                <Select onChange={this.onValuesChange}
                                    style={{ width: 120 }} value={published} options={pubOptions}>
                                </Select>
                            </div>
                            <Search
                                placeholder="Search"
                                allowClear
                                onSearch={this.onSearch}
                                style={{
                                    width: 200,
                                }}
                            />
                            <div>Showing {cloud.length} notebooks</div>
                        </div>
                    </Affix>
                    <this.ListComp datas={cloud} tag={TAG_TYPE.published}></this.ListComp>
                    <div>{end ? "There's no more page." : <Button icon={<ChevronDownIcon />} size='small' onClick={async () => {
                        if (!this.ticking) {
                            this.ticking = true;
                            const { published } = this.state;
                            await this.handleChange(published, undefined, true);
                            this.ticking = false;
                        }
                    }} > More results </Button>}</div>
                </TabPane>
                <TabPane tab={TABS.Share} key={TABS.Share}>
                    <this.ListComp datas={shareds} tag={TAG_TYPE.shared}></this.ListComp>
                </TabPane>
                {/* <TabPane tab={TABS.Mostliked} key={TABS.Mostliked}>
                    </TabPane> */}
            </DraggableTabs>
        </div>
    }
}
// Drag & Drop node
class TabNode extends React.Component {
    render() {
        const { connectDragSource, connectDropTarget, children } = this.props;

        return connectDragSource(connectDropTarget(children));
    }
}
const cardTarget = {
    drop(props, monitor) {
        const dragKey = monitor.getItem().index;
        const hoverKey = props.index;

        if (dragKey === hoverKey) {
            return;
        }

        props.moveTabNode(dragKey, hoverKey);
        monitor.getItem().index = hoverKey;
    },
};

const cardSource = {
    beginDrag(props) {
        return {
            id: props.id,
            index: props.index,
        };
    },
};
const WrapTabNode = DropTarget('DND_NODE', cardTarget, connect => ({
    connectDropTarget: connect.dropTarget(),
}))(
    DragSource('DND_NODE', cardSource, (connect, monitor) => ({
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging(),
    }))(TabNode),
);

class DraggableTabs extends React.Component {
    state = {
        order: [],
    };

    moveTabNode = (dragKey, hoverKey) => {
        const newOrder = this.state.order.slice();
        const { children } = this.props;

        React.Children.forEach(children, c => {
            if (newOrder.indexOf(c.key) === -1) {
                newOrder.push(c.key);
            }
        });

        const dragIndex = newOrder.indexOf(dragKey);
        const hoverIndex = newOrder.indexOf(hoverKey);

        newOrder.splice(dragIndex, 1);
        newOrder.splice(hoverIndex, 0, dragKey);

        this.setState({
            order: newOrder,
        });
    };

    renderTabBar = (props, DefaultTabBar) => (
        <DefaultTabBar {...props}>
            {node => (
                <WrapTabNode key={node.key} index={node.key} moveTabNode={this.moveTabNode}>
                    {node}
                </WrapTabNode>
            )}
        </DefaultTabBar>
    );

    render() {
        const { order } = this.state;
        const { children } = this.props;

        const tabs = [];
        React.Children.forEach(children, c => {
            tabs.push(c);
        });

        const orderTabs = tabs.slice().sort((a, b) => {
            const orderA = order.indexOf(a.key);
            const orderB = order.indexOf(b.key);

            if (orderA !== -1 && orderB !== -1) {
                return orderA - orderB;
            }
            if (orderA !== -1) {
                return -1;
            }
            if (orderB !== -1) {
                return 1;
            }

            const ia = tabs.indexOf(a);
            const ib = tabs.indexOf(b);

            return ia - ib;
        });

        return (
            <DndProvider backend={HTML5Backend}>
                <Tabs renderTabBar={this.renderTabBar} {...this.props}>
                    {orderTabs}
                </Tabs>
            </DndProvider>
        );
    }
}