import { Component } from 'react';
import { PureComponent, useState } from 'react';
import Selection from './components/Selection'

class Greeting extends Component {
    render() {
        return <h3>Hello{this.props.name && ', '}{this.props.name}!
            {"Greeting was rendered at" + new Date().toLocaleTimeString()}

        </h3>;
    }
}

export default function MyApp() {
    return <Selection width={300} height={60} data={["1", "2", "3", "4"]} />;
}