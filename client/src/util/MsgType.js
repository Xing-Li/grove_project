
export const _ = require('lodash')
export const ErrorId = {
    "Success": 0,
    "Already logged in the project in other place": 100,
    "Not in a room": 101,
    "Already logged in the room in other place": 102,
    "Someone is editing": 103,
}
export const Msg_Error = _.invert(ErrorId);
export const SHAREDS_TYPE = {
    /** projectId, userId  */
    User_Shared_By_ProjectId: 1,
    /** toUserId */
    Shared_To_User: 2,
    /** userId, fileKey, projectId */
    User_Shared_By_FileKey: 3
}
export const Msg_SharedsType = _.invert(SHAREDS_TYPE);
/**
 * @example
 * {
 *  projectRoomId:string,
 *  userName:string, 
 *  userId:string
 * }
 */
export const cm_login_project_room = "cm_login_project_room";
/**
 * @example
 * errorId:number,
 * projectRoomId:string,
 * userList:{
 *  userId:string,
 *  userName:string, 
 *  pro_user_status:number,
 * }[]
 */
export const sm_login_project_room = "sm_login_project_room";
/** 
 * @example 
 * {
 *  projectRoomId:string,
 *  userId:string,
 *  userName:string, 
 *  pro_user_status:number,
 * }
 */
export const cast_project_user_status = "cast_project_user_status";
/**
 * @example 
 * {
 * projectRoomId:string,
 * }
 */
export const cm_leave_project_room = "cm_leave_project_room";
/**
 * @example 
 * {
 * projectRoomId:string,
 * }
 */
export const sm_leave_project_room = "sm_leave_project_room";
/**
 * @example
 * {
 *  roomId:string,
 *  userName:string, 
 *  userId:string
 * }
 */
export const cm_login_room = "cm_login_room";
/**
 * @example
 * errorId:number,
 * roomId:string,
 * userList:{
 *  userId:string,
 *  userName:string, 
 *  status:number,
 *  lineId:number,//edit which block 
 * }[]
 */
export const sm_login_room = "sm_login_room";
/** 
 * @example 
 * {
 *  roomId:string
 *  userId:string,
 *  userName:string, 
 *  status:number,
 *  lineId:number,//edit which block
 *  content:string
 * }
 */
export const cast_user_status = "cast_user_status";
/** 
 * @example 
 * {
 *  projectRoomId:string
 *  status:number,
 *  data:{}
 * }
 */
export const cast_project_status = "cast_project_status";
/** 
 * @example 
 * {
 *  roomId:string
 *  status:number,
 * }
 */
export const cast_file_status = "cast_file_status";
/**
 * @example 
 * {
 * roomId:string,
 * }
 */
export const cm_leave_room = "cm_leave_room";
/**
 * @example 
 * {
 * roomId:string,
 * }
 */
export const sm_leave_room = "sm_leave_room";
/**
 * @example 
 * {
 * roomId:string,
 * edit:boolean
 * }
 */
export const cm_apply_edit = "cm_apply_edit";
/** 
 * @example
* {
* roomId:string,
* edit:boolean
* }
*/
export const sm_apply_edit = "sm_apply_edit";
/**
 * @example
 * {
 * roomId:string,
 * lineId:number,
 * content:string
 * }
 */
export const cm_edit = "cm_edit";
/**
 * @example
 * {
 * roomId:string,
 * }
 */
export const sm_edit = "sm_edit";
/**
 * @example
 * {
 * roomId:string,
 * }
 */
export const cm_publish = "cm_publish";
/**
 * @example
 * {
 * roomId:string,
 * }
 */
export const sm_publish = "sm_publish";
/**
 * @example
 * {
 * roomId:string,
 * }
 */
export const cm_close_edit = "cm_close_edit";
/**
 * @example
 * {
 * roomId:string,
 * }
 */
export const sm_close_edit = "sm_close_edit";
/**
 * @example 
 * {
 * roomId:string,
 * outUserId:string,
 * }
 */
export const cm_kickout_room = "cm_kickout_room";
/**
 * @example 
 * {
 * roomId:string,
 * outUserId:string
 * }
 */
export const sm_kickout_room = "sm_kickout_room";

export const UserStatus = {
    "offline": 0,
    "online": 1,
    "edit": 2,
    "kickout": 3
}
export const Msg_UserStatus = _.invert(UserStatus);

export const FileStatus = {
    "idle": 0,
    "locked": 1,
    "updated": 2
}
export const Msg_FileStatus = _.invert(FileStatus);

export const ProjectStatus = {
    /**
     * @example
     * {
     * fileKey
     * }
     */
    "removeFile": 1,
    /**
     * @example
     * {
     * oldFileKey,
     * newFileData
     * }
     */
    "renameFile": 2,
    /**
     * @example
     * {
     * fileData:DefaultFileData,
     * gzip:{}
     * }
     */
    "uploadFile": 3,
    /**
     * @example
     * {
     * removeFolderKey
     * }
     */
    "removeFolder": 4,
    /**
     * @example
     * {
     * oldFolderKey,
     * ret
     * }
     */
    "renameFolder": 5,
    /**
     * @example
     * {
     * folderData:DefaultFolderData
     * }
     */
    "uploadFolder": 6,
    /**
     * @example
     * {
     * ret
     * }
     */
    "copyFolder": 7,
    /**
     * @example
     * {
     * ret:ShapeShared
     * }
     */
    "addShared": 8,
    /**
     * @example
     * {
     * shared:ShapeShared
     * }
     */
    "removeShared": 9,
}
export const Msg_ProjectStatus = _.invert(ProjectStatus);


export const PublishOptions = { "Forks": 3, "View code": 2, "View": 1 };

export const CanOptions = _.merge({}, PublishOptions, { "Edit": 100 });

export const ShareProjectCanOptions = _.merge({}, CanOptions, { "Operate file system": 110 });

export const DefaultShareProjectCanOption = ShareProjectCanOptions["Operate file system"];

export const MaxCanOption = { "Super": 10000 };

export const Msg_CanOptions = _.invert(CanOptions);