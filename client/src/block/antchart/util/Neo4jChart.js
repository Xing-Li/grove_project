import { Record as Neo4jRecord } from 'neo4j-driver';

// Interface for all charts that NeoDash can render.
// When you extend NeoDash, make sure that your component implements this interface.
export class ChartProps {
    constructor() {
        /**@type{Neo4jRecord[]} Query output, Neo4j records as returned from the driver. */
        this.records = [];
        /**@type{Object.<string, any>} A dictionary of enabled extensions.*/
        this.extensions = {};
        /**@type{Object.<string, any>} A dictionary with the selection made in the report footer.*/
        this.selection = {};
        /**@type{Object.<string, any>} A dictionary with the 'advanced settings' specified through the NeoDash interface.*/
        this.settings = {};
        /**@type{Object.<string, number>} a dictionary with the dimensions of the report (likely not needed, charts automatically fill up space).*/
        this.dimensions = {};
        /**@type{Object.<string, any>} A dictionary with the global dashboard parameters.*/
        this.parameters = {};
        /**flag indicating whether the report is rendered in a fullscreen view.  */
        this.fullscreen = false;
    }
    /**
     *  Optionally, a way for the report to read more data from Neo4j.
     * @param {string} query 
     * @param {Object.<string, any>} parameters 
     * @param {Neo4jRecord[]} records 
     */
    queryCallback = function (query, parameters, records) { return null; }
    /**
     * Allows a chart to update a global dashboard parameter to be used in Cypher queries for other reports.
     * @param {string} name 
     * @param {string} value 
     */
    setGlobalParameter = function (name, value) {

    }
    /**
     *  Allows a chart to get a global dashboard parameter.
     * @param {string} name 
     */
    getGlobalParameter = function (name) {
        return "";
    };

}