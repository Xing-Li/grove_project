const mongoose = require('mongoose');
const schemaCRUD = require('./../schemaCRUD/schemaCRUD');

const Schema = mongoose.Schema;

const UserShareProjects = new Schema({
    userId: { type: Schema.Types.ObjectId, ref: 'users' },
    addUser: { type: Schema.Types.ObjectId, ref: 'users' },
    email: String,
    projectId: { type: Schema.Types.ObjectId, ref: 'customDB' },
    projectName: String,
    purview: String,
    status: Number,
    createTime: { type: Date, default: Date.now },
});

UserShareProjects.plugin(schemaCRUD.CRUD);

UserShareProjects.plugin(function (schema) {

    schema.statics.addUserToShareProject = function (addInfo, cb) {
        return schemaCRUD.add(this, addInfo, function (err, result) {
            return cb(err, result);
        }, false);
    }

    schema.statics.stopUser = function (userInfo, projectInfo, cb) {
        return schemaCRUD.add(this, data, function (err, result) {
            return cb(err, result);
        }, false);
    }

    schema.statics.getAllUserByProject = function (projectId, cb) {

        this.find({ 'projectId': projectId }).select({}).sort({ "createTime": -1 })
            .exec(function (err, projects) {
                return cb(err, projects)
            });
    }
    schema.statics.getAllUserDetailByProject = function (projectId, cb) {

        this.find({ 'projectId': projectId }).populate({
            path: 'userId',
            select: 'email  lastName firstName'
        }).select({}).sort({ "createTime": -1 })
            .exec(function (err, projects) {
                return cb(err, projects)
            });
    }

    schema.statics.getAllProjectByUser = function (userId, cb) {
        this.find({ 'userId': userId }).select({}).sort({ "createTime": -1 })
            .populate('projectId') 
            .exec(function (err, projects) {
                if(Array.isArray(projects)){
                    projects = projects.map(p => p.get('projectId')).filter(p => p);
                }
                return cb(err, projects)
            });
    }

    schema.statics.deleteShareProjectById = function (userId, cb) {
        this.deleteOne({ '_id': userId }, cb)
    }


    schema.statics.changeUserPurviewById = function (info, cb) {
        // let data = {
        //     userInfo: userInfo,
        //     projectInfo: projectInfo
        // }


        return schemaCRUD.edit(this, { _id: info.id }, { purview: info.purview }, function (err, result) {
            return cb(err, result);
        }, false);
    }

});



module.exports = mongoose.model('UserShareProjects', UserShareProjects);