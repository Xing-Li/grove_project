import { Area, Bar, Column, Heatmap, Line, Pie, Radar, Rose, Scatter, WordCloud, Waterfall, Liquid } from '@antv/g2plot';
import { TimelineConstruct, GroupsTimelineConstruct, SubGroupsTimelineConstruct } from './TimelinePlot';
import _ from 'lodash';
import { autoObjectsType } from 'util/helpers';
import {
    getMultiColors, areaDefault, barDefault, radarDefault, multiRadarDefault, columnDefault, timelineDefault,
    rangeTimelineDefault, groupsTimelineDefault, groupsRangeTimelineDefault, subGroupsTimelineDefault,
    subGroupsRangeTimelineDefault, lineDefault, multiAreaDefault, multiBarDefault,
    multiColumnDefault, multiLineDefault, continuousHeatmapDefault, heatmapDefault, unevenHeatmapDefault,
    wordCloudDefault, waterfallDefault, pieDefault, roseDefault, multiRoseDefault, scatterDefault, bubbleDefault,
    multiBubbleDefault, liquidDefault
} from 'util/chartConfigUtil'
import {
    chartOptionsDefault, ChartTypes, DataTypes, ChartTypeTemplate, getRenderTypeOptions,
    DefaultAntChartConfig, extractOptions, extractDatas, Fields,
} from './OptionsTypes';
import { isChartRenderType } from 'util/databaseApi';


export const getChartConstructor = function (type) {
    let constructorFunc;
    switch (type) {
        case ChartTypes['Base Timeline']:
        case ChartTypes['Base Range Timeline']:
            constructorFunc = TimelineConstruct;
            break;
        case ChartTypes['Groups Timeline']:
        case ChartTypes['Groups Range Timeline']:
            constructorFunc = GroupsTimelineConstruct;
            break;
        case ChartTypes['SubGroups Timeline']:
        case ChartTypes['SubGroups Range Timeline']:
            constructorFunc = SubGroupsTimelineConstruct;
            break;
        case ChartTypes['Basic Line']:
        case ChartTypes['Multi Line']:
            constructorFunc = Line;
            break;
        case ChartTypes['Basic Column']:
        case ChartTypes['Multi Column']:
            constructorFunc = Column;
            break;
        case ChartTypes['Waterfall']:
            constructorFunc = Waterfall;
            break;
        case ChartTypes['Liquid']:
            constructorFunc = Liquid;
            break;
        case ChartTypes['Basic Area']:
        case ChartTypes['Multi Area']:
            constructorFunc = Area;
            break;
        case ChartTypes['Basic Bar']:
        case ChartTypes['Multi Bar']:
            constructorFunc = Bar;
            break;
        case ChartTypes['Basic Scatter']:
        case ChartTypes['Multi Scatter']:
        case ChartTypes['Basic Bubble']:
        case ChartTypes['Multi Bubble']:
            constructorFunc = Scatter;
            break;
        case ChartTypes['Heatmap']:
        case ChartTypes['Continuous Heatmap']:
        case ChartTypes['Uneven Heatmap']:
            constructorFunc = Heatmap;
            break;
        case ChartTypes['Basic Word Cloud']:
        case ChartTypes['Word Cloud']:
            constructorFunc = WordCloud;
            break;
        case ChartTypes['Basic Radar']:
        case ChartTypes['Multi Radar']:
            constructorFunc = Radar;
            break;
        case ChartTypes['Basic Rose']:
        case ChartTypes['Multi Rose']:
            constructorFunc = Rose;
            break;
        case ChartTypes['Basic Pie']:
            constructorFunc = Pie;
            break;
        default:
            constructorFunc = undefined;
            break;
    }
    return constructorFunc;
}
export const getDefaultOptionsByType = function (type) {
    let options;
    switch (type) {
        case ChartTypes['Base Timeline']:
            options = timelineDefault;
            break;
        case ChartTypes['Base Range Timeline']:
            options = rangeTimelineDefault;
            break;
        case ChartTypes['Groups Timeline']:
            options = groupsTimelineDefault;
            break;
        case ChartTypes['Groups Range Timeline']:
            options = groupsRangeTimelineDefault;
            break;
        case ChartTypes['SubGroups Timeline']:
            options = subGroupsTimelineDefault;
            break;
        case ChartTypes['SubGroups Range Timeline']:
            options = subGroupsRangeTimelineDefault;
            break;
        case ChartTypes['Basic Line']:
            options = lineDefault;
            break;
        case ChartTypes['Multi Line']:
            options = multiLineDefault;
            break;
        case ChartTypes['Basic Column']:
            options = columnDefault;
            break;
        case ChartTypes['Multi Column']:
            options = multiColumnDefault;
            break;
        case ChartTypes['Waterfall']:
            options = waterfallDefault;
            break;
        case ChartTypes['Liquid']:
            options = liquidDefault;
            break;
        case ChartTypes['Basic Area']:
            options = areaDefault;
            break;
        case ChartTypes['Multi Area']:
            options = multiAreaDefault;
            break;
        case ChartTypes['Basic Bar']:
            options = barDefault;
            break;
        case ChartTypes['Multi Bar']:
            options = multiBarDefault;
            break;
        case ChartTypes['Basic Scatter']:
            options = scatterDefault;
            break;
        case ChartTypes['Multi Scatter']:
            options = scatterDefault;
            break;
        case ChartTypes['Basic Bubble']:
            options = bubbleDefault;
            break;
        case ChartTypes['Basic Word Cloud']:
        case ChartTypes['Word Cloud']:
            options = wordCloudDefault;
            break;
        case ChartTypes['Multi Bubble']:
            options = multiBubbleDefault;
            break;
        case ChartTypes['Continuous Heatmap']:
            options = continuousHeatmapDefault;
            break;
        case ChartTypes['Heatmap']:
            options = heatmapDefault;
            break;
        case ChartTypes['Uneven Heatmap']:
            options = unevenHeatmapDefault;
            break;
        case ChartTypes['Basic Radar']:
            options = radarDefault;
            break;
        case ChartTypes['Multi Radar']:
            options = multiRadarDefault;
            break;
        case ChartTypes['Basic Rose']:
            options = roseDefault;
            break;
        case ChartTypes['Multi Rose']:
            options = multiRoseDefault;
            break;
        case ChartTypes['Basic Pie']:
            options = pieDefault;
            break;
        default:
            options = chartOptionsDefault;
            break;
    }
    return options;
}
/**
 * 
 * @param {RenderType} renderType 
 * @param {ChartTypes} type 
 * @returns 
 */
export const cloneDeepDefaultOptions = function (renderType, type) {
    return isChartRenderType(renderType) ? _.cloneDeep(getDefaultOptionsByType(type)) : _.cloneDeep(getRenderTypeOptions(renderType));
}
/**
 * 
 * @param {ChartTypeTemplate} chartType 
 */
export const getChartConfig = function (chartType) {
    const { type, config } = chartType;
    /**@type {chartOptionsDefault} */
    let options;
    let constructorFunc = getChartConstructor(type);
    switch (type) {
        case ChartTypes['Basic Line']:
        case ChartTypes['Multi Line']:
        case ChartTypes['Basic Column']:
        case ChartTypes['Multi Column']:
        case ChartTypes['Waterfall']:
        case ChartTypes['Basic Area']:
        case ChartTypes['Multi Area']:
        case ChartTypes['Basic Bar']:
        case ChartTypes['Multi Bar']:
        case ChartTypes['Basic Scatter']:
        case ChartTypes['Multi Scatter']:
        case ChartTypes['Basic Bubble']:
        case ChartTypes['Multi Bubble']:
        case ChartTypes['Continuous Heatmap']:
        case ChartTypes['Heatmap']:
        case ChartTypes['Uneven Heatmap']:
            let otherOptions = {
                xAxis: {
                    title: {
                        text: config.xField
                    }
                },
                yAxis: {
                    title: {
                        text: config.yField
                    }
                }
            };
            options = _.merge(_.cloneDeep(getDefaultOptionsByType(type)), config, otherOptions);
            break;
        case ChartTypes['Basic Radar']:
        case ChartTypes['Multi Radar']:
        case ChartTypes['Basic Rose']:
        case ChartTypes['Multi Rose']:
        case ChartTypes['Basic Word Cloud']:
        case ChartTypes['Word Cloud']:
        case ChartTypes['Basic Pie']:
        case ChartTypes['Liquid']:
            options = _.merge(_.cloneDeep(getDefaultOptionsByType(type)), config);
            break;
        case ChartTypes['Base Timeline']:
        case ChartTypes['Groups Timeline']:
        case ChartTypes['Base Range Timeline']:
        case ChartTypes['Groups Range Timeline']:
        case ChartTypes['SubGroups Timeline']:
        case ChartTypes['SubGroups Range Timeline']:
            options = _.merge(_.cloneDeep(getDefaultOptionsByType(type)), config);
            break;
        default:
            options = _.merge(_.cloneDeep(getDefaultOptionsByType(type)), config);
            break;
    }
    return {
        options,
        constructorFunc,
        type,
    }
}

const getPeriod = function (a, b) {
    if (typeof a === 'number' && typeof b === 'number') {
        return a - b;
    } else if (a instanceof Date && b instanceof Date) {
        let days = (a - b) / (24 * 3600 * 1000)
        if (days >= 28 && days <= 31) {
            return 30;
        } else if (days >= 365 && days <= 366) {
            return 365;
        } else {
            return days;
        }
    } else { return a - b; }

}
export const checkPeriodDateOrNumber = function (allData, b) {
    let sortArr = _.uniq(_.map(allData, (value) => { return value[b] }));
    if (sortArr.length === allData.length) {
        sortArr.sort((o1, o2) => { return o1 > o2 ? 1 : -1 });
        let period = getPeriod(sortArr[1], sortArr[0]);
        if (!isNaN(period)) {
            if (_.find(sortArr.slice(1), (value, index, arr) => {
                let nPeriod = getPeriod(value, sortArr[index]);
                return isNaN(nPeriod) || Math.abs(period - nPeriod) > 0.01;
            }) === undefined) {
                return sortArr[0] instanceof Date ? DataTypes['period date'] : DataTypes['period number'];
            }
        }
    }
}
const sortDataTypesFunc = function (first, a, b) {
    if (typeof first[a] === 'string') {
        return -1;
    } else if (typeof first[b] !== 'string') {
        return 1;
    }
    return 0;
}
/**
 * 
 * @param {Array<Object.<string, number|string>>} data 
 */
export const checkChartTypes = function (data) {
    /**
     * @type {[ChartTypeTemplate]}
     */
    const chartTypes = [];
    if (!data || !data.length) {
        return chartTypes;
    }
    if (data.length === 1) {
        _.map(data[0], (v, k, obj) => {
            if (!isNaN(v)) {
                if (v >= 0 && v <= 100) {
                    chartTypes.push({
                        type: ChartTypes['Liquid'],
                        config: {
                            percent: v <= 1 ? v : v / 100,
                            statistic: {
                                title: {
                                    content: k,
                                },
                            }
                        },
                    })
                }
                let keys = _.filter(_.keys(obj), (kk, index) => { return k !== kk });
                if (keys.length) {
                    chartTypes.push({
                        type: ChartTypes['Basic Pie'],
                        config: {
                            angleField: k,
                            colorField: keys[0],
                        }
                    })
                }
            }
        })
        return chartTypes;
    }
    /**@type {Array<Object.<string, number|string|Date|boolean|null|NaN>>} */
    let allData = autoObjectsType(_.cloneDeep(data), true)
    let first = allData[0];
    let allKeys = _.keys(first);
    let numbers = _.filter(allKeys, (key) => {
        return typeof first[key] === 'number'
    });
    let strings = _.filter(allKeys, (key) => {
        return typeof first[key] === 'string'
    });
    let dates = _.filter(allKeys, (key) => {
        return typeof first[key] === 'object' && first[key] instanceof Date
    }).sort((a, b) => { return first[a] - first[b] });
    if (dates.length == 1) {
        let fArr = _.filter(allKeys, (v, index) => { return v !== dates[0] }).sort((a, b) => { return sortDataTypesFunc(first, a, b) });
        if (allKeys.length === 2) {
            chartTypes.push({
                type: ChartTypes['Base Timeline'],
                config: {
                    startField: dates[0],
                    contentField: fArr[0],
                }
            })
        } else if (allKeys.length >= 3) {
            chartTypes.push({
                type: ChartTypes['Groups Timeline'],
                config: {
                    startField: dates[0],
                    groupField: fArr[0],
                    contentField: fArr[1],
                }
            })
            chartTypes.push({
                type: ChartTypes['SubGroups Timeline'],
                config: {
                    startField: dates[0],
                    groupField: fArr[0],
                    contentField: fArr[1],
                }
            })
        }
    } else if (dates.length >= 2) {
        let fArr = _.filter(allKeys, (v, index) => { return v !== dates[0] && v !== dates[1] }).sort((a, b) => { return sortDataTypesFunc(first, a, b) });
        if (allKeys.length === 3) {
            chartTypes.push({
                type: ChartTypes['Base Range Timeline'],
                config: {
                    startField: dates[0],
                    endField: dates[1],
                    contentField: fArr[0],
                }
            })
        } else if (allKeys.length >= 4) {
            chartTypes.push({
                type: ChartTypes['Groups Range Timeline'],
                config: {
                    startField: dates[0],
                    endField: dates[1],
                    groupField: fArr[0],
                    contentField: fArr[1],
                }
            })
            chartTypes.push({
                type: ChartTypes['SubGroups Range Timeline'],
                config: {
                    startField: dates[0],
                    endField: dates[1],
                    groupField: fArr[0],
                    contentField: fArr[1],
                }
            })
        }
    }
    /**
     * 
     * @param {*} a yAxis
     * @param {*} b xAxis
     * @param {*} allData 
     * @param {*} seriesField 
     * @param {boolean} sameLengthCategory 
     */
    let oneNumberOneTime_or_ordinal = (a, b, allData, seriesField, sameLengthCategory) => {
        if (!allData || !allData.length || allData.length < 2) {
            return;
        }
        if (typeof first[a] !== 'number') {
            let tmp = a;
            a = b;
            b = tmp;
        }
        if (typeof first[a] !== 'number') {
            return;
        }
        let pairs;
        if (typeof first[a] === 'number' && typeof first[b] === 'number') {
            pairs = [[a, b], [b, a]];
        } else {
            pairs = [[b, a]];
        }
        _.each(pairs, (pair, index) => {
            let xAxis = pair[0], yAxis = pair[1], types;
            let sortArr = _.uniq(_.map(allData, (value) => { return value[xAxis] }));
            if (sortArr.length === allData.length) {
                sortArr.sort((o1, o2) => { return o1 > o2 ? 1 : -1 });
                let period = getPeriod(sortArr[1], sortArr[0]);
                if (!isNaN(period)) {
                    if (_.find(sortArr.slice(1), (value, index, arr) => {
                        let nPeriod = getPeriod(value, sortArr[index]);
                        return isNaN(nPeriod) || Math.abs(period - nPeriod) > 0.01;
                    }) === undefined) {
                        if (seriesField) {
                            types = [ChartTypes['Multi Line'], ChartTypes['Multi Area'], ChartTypes['Multi Column'], ChartTypes['Multi Bar']];
                        } else {
                            types = [ChartTypes['Basic Line'], ChartTypes['Basic Area'], ChartTypes['Basic Column'], ChartTypes['Basic Bar']];
                        }
                    }
                } else {
                    if (seriesField) {
                        types = [ChartTypes['Multi Column'], ChartTypes['Multi Bar'], ChartTypes['Multi Rose']];
                        if (allData.length >= 3) {
                            types.push(ChartTypes['Multi Radar']);
                        }
                    } else {
                        types = [ChartTypes['Basic Column'], ChartTypes['Waterfall'], ChartTypes['Basic Bar'], ChartTypes['Basic Pie'], ChartTypes['Basic Rose']];
                        if (allData.length >= 3) {
                            types.push(ChartTypes['Basic Radar']);
                        }
                    }
                }
                _.each(types, (type) => {
                    if (type === ChartTypes['Basic Pie']) {
                        chartTypes.push({
                            type: type,
                            config: {
                                angleField: yAxis,
                                colorField: xAxis,
                            }
                        })
                    } else if (~[ChartTypes['Basic Bar'], ChartTypes['Multi Bar']].indexOf(type)) {
                        chartTypes.push({
                            type: type,
                            config: {
                                xField: yAxis,
                                yField: xAxis,
                                seriesField,
                            }
                        })
                    } else {
                        chartTypes.push({
                            type: type,
                            config: {
                                xField: xAxis,
                                yField: yAxis,
                                seriesField,
                            }
                        })
                    }
                })
            }
            if (typeof first[xAxis] === 'string' && (allData.length - sortArr.length) / allData.length < 0.1) {
                if (seriesField) {
                    chartTypes.push({
                        type: ChartTypes['Word Cloud'],
                        config: {
                            wordField: xAxis,
                            weightField: yAxis,
                            colorField: seriesField,
                        }
                    })
                } else {
                    chartTypes.push({
                        type: ChartTypes['Basic Word Cloud'],
                        config: {
                            wordField: xAxis,
                            weightField: yAxis,
                        }
                    })
                }
            }
        })
    }
    /**
     * 
     * @param {*} a xAxis
     * @param {*} b yAxis
     * @param {*} colorField 
     */
    let oneNumberOneNumber = (a, b, colorField, sameLengthCategory) => {
        let xAxis, yAxis, types;
        if (typeof first[a] === 'number' && typeof first[b] === 'number') {
            xAxis = a;
            yAxis = b;
            if (colorField) {
                types = [ChartTypes['Multi Scatter']];
            } else {
                types = [ChartTypes['Basic Scatter']];
            }
            _.each(types, (type) => {
                chartTypes.push({
                    type: type,
                    config: {
                        xField: xAxis,
                        yField: yAxis,
                        colorField
                    }
                })
            })
        }
    }
    if (allKeys.length === 2) {
        oneNumberOneTime_or_ordinal(allKeys[0], allKeys[1], allData);
        oneNumberOneNumber(allKeys[0], allKeys[1]);
    } else if (allKeys.length === 3) {
        if (numbers.length === 3) {
            chartTypes.push({
                type: ChartTypes['Basic Bubble'],
                config: {
                    xField: allKeys[0],
                    yField: allKeys[1],
                    sizeField: allKeys[2]
                }
            })
            chartTypes.push({
                type: ChartTypes['Continuous Heatmap'],
                config: {
                    xField: allKeys[0],
                    yField: allKeys[1],
                    colorField: allKeys[2]
                }
            })
        } else if (numbers.length > 0 && strings.length > 0) {
            if (numbers.length === 1) {
                chartTypes.push({
                    type: ChartTypes['Heatmap'],
                    config: {
                        xField: strings[0],
                        yField: strings[1],
                        colorField: numbers[0],
                    }
                })
            }
            _.each(strings, (maySerkey) => {
                /**@type {Object<string, Array<Object>>} */
                let categories = {};
                _.each(allData, (value) => {
                    let categoryName = value[maySerkey];
                    if (categoryName) {
                        if (!categories[categoryName]) {
                            categories[categoryName] = [];
                        }
                        let obj = _.clone(value);
                        delete obj[maySerkey];
                        categories[categoryName].push(obj);
                    }
                })
                let sameLengthCategory = true;
                let values = _.values(categories);
                _.each(values, (value, index, values) => {
                    if (Math.abs(value.length - values[0].length) / values[0].length > 0.1) {
                        sameLengthCategory = false;
                        return false;
                    }
                });
                // if (sameLengthCategory) {
                let leftTwoKeys = _.filter(allKeys, (value) => { return value !== maySerkey });
                oneNumberOneTime_or_ordinal(leftTwoKeys[0], leftTwoKeys[1], values[0], maySerkey, sameLengthCategory);
                oneNumberOneNumber(leftTwoKeys[0], leftTwoKeys[1], maySerkey, sameLengthCategory);
                // } else {
                //     let leftTwoKeys = _.filter(allKeys, (value) => { return value !== maySerkey });
                //     oneNumberOneNumber(leftTwoKeys[0], leftTwoKeys[1], maySerkey);
                // }
            })
        }
    } else if (allKeys.length === 4) {
        if (numbers.length === 3 && strings.length === 1) {
            let threeNumKeys = _.filter(allKeys, (value) => { return value !== strings[0] });
            chartTypes.push({
                type: ChartTypes['Multi Bubble'],
                config: {
                    xField: threeNumKeys[0],
                    yField: threeNumKeys[1],
                    sizeField: threeNumKeys[2],
                    colorField: strings[0],
                }
            })
        }
        if (numbers.length === 2 && strings.length === 2) {
            chartTypes.push({
                type: ChartTypes['Uneven Heatmap'],
                config: {
                    xField: strings[0],
                    yField: strings[1],
                    sizeField: numbers[0],
                    colorField: numbers[1],
                }
            })
        }
    }
    _.each(chartTypes, (chartType) => {
        let series = chartType.config.colorField || chartType.config.seriesField;
        if (series && !~chartType.type.indexOf("Heatmap")) {
            let set = _.reduce(allData, (prev, curr, index) => {
                prev.add(curr[series]);
                return prev;
            }, new Set());
            _.assign(chartType.config, {
                color: getMultiColors(set.size)
            })
        }
    })
    // console.log(chartTypes);
    return chartTypes
}

/** @param {DefaultAntChartConfig}  chartConfig*/
export const getExOptions = (chartConfig) => {
    const { renderType, type } = chartConfig;
    let defaultOptions = cloneDeepDefaultOptions(renderType, type);
    if (isChartRenderType(renderType)) {
        let chartDataTmp = _.cloneDeep(chartConfig.chartData);
        chartConfig.chartData.keysArr && (chartDataTmp.keysArr = chartConfig.chartData.keysArr)
        let optionsTmp = _.assign(defaultOptions, chartConfig.options,
            {
                data: autoObjectsType(extractDatas(chartDataTmp, _.values(_.pick(chartConfig.options, _.keys(Fields)))), false)
            }
        )
        let options = extractOptions(optionsTmp);
        options.data.keysArr = optionsTmp.data.keysArr;
        return options;
    } else {
        return extractOptions(_.assign(defaultOptions, chartConfig.options))
    }
}