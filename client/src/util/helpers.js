import _ from 'lodash';
import { DEFAULT_BREAK_POINTS, DEFAULT_CELL_PADDING } from './databaseApi';
import { autoType as d3AutoType, csvParse, csvParseRows, tsvParse, tsvParseRows } from "d3-dsv";
const ExcelJS = require('exceljs');

export const DEBUG_CHAT = true;
export const DEBUG_DRAG = false;
export const DEBUG_EDITOR_JS = false;
/**
 * VERBOSE	Show all messages (default)
 * INFO	Show info and debug messages
 * WARN	Show only warn messages
 * ERROR	Show only error messages
 */
export const DEBUG_EDITOR_JS_LEVEL = 'ERROR';
export const DEBUG_EDITOR = false;
export const DEBUG_HQ = false;
export const DEBUG_REQUIRE = false;
export const UNKOWN = "UNKOWN";
/**
 * Enum for file / directory.
 * @readonly
 * @enum {string}
 */
export const SelectType = {
  dir: "dir",
  file: 'file'
}
/**
 * check is file or dir
 * @param {DefaultFileData||DefaultFolderData} file 
 */
export const getType = function (file) {
  return file.type === SelectType.dir ? SelectType.dir : SelectType.file;
}
export const isFile = function (file) {
  return getType(file) === SelectType.file;
}
export const isDir = function (file) {
  return getType(file) === SelectType.dir;
}
// Opera 8.0+
let isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

// Firefox 1.0+
let isFirefox = typeof InstallTrigger !== 'undefined';

// Safari 3.0+ "[object HTMLElementConstructor]" 
let isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));

// Internet Explorer 6-11
let isIE = /*@cc_on!@*/false || !!document.documentMode;

// Edge 20+
let isEdge = !isIE && !!window.StyleMedia;

// Chrome 1 - 71
let isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);

// Blink engine detection
let isBlink = (isChrome || isOpera) && !!window.CSS;

export const OSType = {
  Windows: "Windows",
  MacOS: "MacOS",
  UNIX: "UNIX",
  Linux: "Linux",
  Unkown: "Unkown",
}

export function osfunction() {
  let os = navigator.userAgent;
  let finalOs = OSType.Unkown;
  if (os.search('Windows') !== -1) {
    finalOs = OSType.Windows;
  }
  else if (os.search('Mac') !== -1) {
    finalOs = OSType.MacOS;
  }
  else if (os.search('X11') !== -1 && !(os.search('Linux') !== -1)) {
    finalOs = OSType.UNIX;
  }
  else if (os.search('Linux') !== -1 && os.search('X11') !== -1) {
    finalOs = OSType.Linux
  }

  return finalOs;
}

export const BrowserChecks = {
  isOpera: isOpera,
  isFirefox: isFirefox,
  isSafari: isSafari,
  isIE: isIE,
  isEdge: isEdge,
  isChrome: isChrome,
  isBlink: isBlink,
}
/**
 * Helpers
 */
export const fallbackimg = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
const escapeTest = /[&<>"']/;
const escapeReplace = /[&<>"']/g;
const escapeTestNoEncode = /[<>"']|&(?!#?\w+;)/;
const escapeReplaceNoEncode = /[<>"']|&(?!#?\w+;)/g;
const escapeReplacements = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#39;'
};
const unescapeReplacements = _.invert(escapeReplacements)
export function unescapeHtml(html) {
  if (!html) {
    return;
  }
  return String(html).replace(new RegExp(`${Object.values(escapeReplacements).join("|")}`, "g"), getUnescapeReplacement);
}
const getEscapeReplacement = (ch) => escapeReplacements[ch];
const getUnescapeReplacement = (ch) => unescapeReplacements[ch];
export function escape(html, encode) {
  if (encode) {
    if (escapeTest.test(html)) {
      return html.replace(escapeReplace, getEscapeReplacement);
    }
  } else {
    if (escapeTestNoEncode.test(html)) {
      return html.replace(escapeReplaceNoEncode, getEscapeReplacement);
    }
  }

  return html;
}

// https://github.com/d3/d3-dsv/issues/45
const fixtz = new Date("2019-01-01T00:00").getHours() || new Date("2019-07-01T00:00").getHours();

/**
 * 
 * @param {object} object 
 * @param {boolean} date 
 */
export function autoType(object, date = true) {
  for (let key in object) {
    if (typeof object[key] !== 'string') continue;
    let value = object[key].trim(), number, m;
    if (!value) value = null;
    else if (value === "true") value = true;
    else if (value === "false") value = false;
    else if (value === "NaN") value = NaN;
    else if (!isNaN(number = +value)) value = number;
    else if (date && (m = value.match(/^([-+]\d{2})?\d{4}(-\d{2}(-\d{2})?)?(T\d{2}:\d{2}(:\d{2}(\.\d{3})?)?(Z|[-+]\d{2}:\d{2})?)?$/))) {
      if (fixtz && !!m[4] && !m[7]) value = value.replace(/-/g, "/").replace(/T/, " ");
      value = new Date(value);
    }
    else continue;
    object[key] = value;
  }
  return object;
}

/**
 * 
 * @param {Array<{}>} objects 
 * @param {boolean} date is need transfer date to Date
 */
export function autoObjectsType(objects, date = false) {
  let objTypes = calcAutoType(objects, date, false);
  _.each(objects, (object) => {
    if (typeof object !== "object") {
      console.error(objects)
      return;
    }
    for (let key in object) {
      if (typeof object[key] !== 'string') {
        if (objTypes[key].type === "date" && object[key] instanceof Date && !date) {
          object[key] = object[key].toLocaleString();
        }
        continue;
      }
      let objType = objTypes[key], value = object[key].trim(), number, m;
      if (objType.type === "undefined" || objType.type === "null") {
        value = undefined;
      } else if (objType.type === "boolean") {
        if (value === "true") {
          value = true;
        } else {
          value = false;
        }
      } else if (objType.type === "number") {
        if (value === "NaN") {
          value = NaN;
        } else if (!isNaN(number = +value)) {
          value = number;
        } else {
          value = NaN;
        }
      } else if (date && objType.type === "date") {
        m = value.match(/^([-+]\d{2})?\d{4}(-\d{2}(-\d{2})?)?(T\d{2}:\d{2}(:\d{2}(\.\d{3})?)?(Z|[-+]\d{2}:\d{2})?)?$/);
        if (fixtz && !!m[4] && !m[7]) { value = value.replace(/-/g, "/").replace(/T/, " "); }
        value = new Date(value);
      }
      object[key] = value;
    }
  })
  return objects;
}
/**
 * 
 * @param {{}[]} objects 
 */
export function extractAllKeys(objects) {
  let obj = {};
  _.each(objects, (object) => {
    _.each(object, (value, key) => {
      if (undefined === obj[key]) {
        obj[key] = value;
      }
    })
  })
  return _.keys(obj);
}

export function ConvertToCSV(objArray) {
  let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
  let keys = objArray.columns || extractAllKeys(objArray)
  let header = _.reduce(keys, (line, key, index) => {
    if (index != 0) line += ','
    line += key;
    return line;
  }, '')
  let str = '';
  for (let i = 0; i < array.length; i++) {
    let line = '';
    if (typeof array[i] != 'object') {
      continue;
    }
    for (let index = 0; index < keys.length; index++) {
      let key = keys[index];
      if (index != 0) line += ','
      if (array[i][key] === undefined) {
        line += '';
      } else {
        line += array[i][key];
      }
    }
    str += line + '\r\n';
  }

  let universalBOM = "\uFEFF";
  return universalBOM + header + '\r\n' + str;
}
export async function ConvertToXLSX(sheetName, objArray) {
  let workbook = new ExcelJS.Workbook();
  let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
  let keys = objArray.columns || extractAllKeys(objArray)
  let columns = _.reduce(keys, (prev, key, index) => {
    prev.push({ header: key, key: key, width: key.length });
    return prev;
  }, [])
  const worksheet = workbook.addWorksheet(`${sheetName}`);
  worksheet.columns = columns;
  for (let i = 0; i < array.length; i++) {
    if (typeof array[i] != 'object') {
      continue;
    }
    worksheet.addRow(array[i]);
  }
  return await workbook.xlsx.writeBuffer()
}
export const _BLANKS = [undefined, '', null];
export const _BLANKS_STRINGS = ["undefined", '', "null"]
export const nanOrBlank = function (v) {
  return ~_BLANKS.indexOf(v) || (v).toString() === 'NaN';
}
export const stringifyNanOrBlank = function (v) {
  if (~_BLANKS.indexOf(v)) {
    return _BLANKS_STRINGS[_BLANKS.indexOf(v)];
  } else if ((v).toString() === 'NaN') {
    return 'NaN'
  } else {
    return v;
  }
}
/**
 * 
 * @param {number} percent 
 */
export function transferPercent(percent) {
  return percent < 10 && !Number.isInteger(percent) ? percent.toFixed(1) : percent.toFixed(0);
}
/**
 * 
 * @param {number} integer 
 * @param {number} fixNum 
 */
export function transferInteger(integer, fixNum) {
  if (Number.isNaN(integer) || undefined === integer) {
    return integer;
  }
  if (typeof integer === 'string') {
    integer = integer.indexOf('.') != -1 ? parseFloat(integer) : parseInt(integer);
  }
  return !Number.isInteger(integer) ?
    (integer.toFixed(fixNum).length > 5 ? integer.toExponential(fixNum) : integer.toFixed(fixNum)) :
    (integer.toFixed(0).length > 5 ? integer.toExponential(0) : integer.toFixed(0));
}
/**
 * 
 * @param {{}[]} objects
 */
export function calcSqliteAutoType(objects) {
  /**
   * @type {Object.<string,{type:string}>}
   */
  const objTypes = {};
  const prior = ['NULL', 'INTEGER', 'REAL', 'TEXT', 'BLOB'];
  /**
   * 
   * @param {string} key 
   * @param {string} type 
   */
  function setMaxType(key, type,) {
    if (objTypes[key]) {
      let oldType = objTypes[key].type;
      if (oldType === "TEXT") {
        return;
      }
      if (type !== oldType) {
        if (prior.indexOf(type) > prior.indexOf(oldType)) {
          objTypes[key] = { type };
        }
      }
    } else {
      objTypes[key] = { type };
    }
  }
  _.each(objects, (object, index) => {
    for (let key in object) {
      if (typeof object[key] !== 'string') {
        let type;
        switch (typeof object[key]) {
          case 'undefined':
            type = 'NULL';
            break;
          case 'number':
            type = (Number.isInteger(object[key]) || isNaN(object[key])) ? 'INTEGER' : 'REAL';
            break;
          case 'boolean':
            type = 'TEXT';
            break;
          case 'string':
            type = 'TEXT';
            break;
          default:
            type = 'TEXT';
            break;
        }
        setMaxType(key, type);
        continue;
      }
      let value = object[key].trim(), number, type, m;
      if (!value) {
        type = "NULL";
      } else if (value === "true") {
        type = "INTEGER";
      } else if (value === "false") {
        type = "INTEGER";
      } else if (value === "NaN") {
        type = "INTEGER";
      } else if (!isNaN(number = +value)) {
        if (!Number.isInteger(+value)) {
          type = "REAL";
        } else {
          type = "INTEGER";
        }
      } else if ((m = value.match(/^([-+]\d{2})?\d{4}(-\d{2}(-\d{2})?)?(T\d{2}:\d{2}(:\d{2}(\.\d{3})?)?(Z|[-+]\d{2}:\d{2})?)?$/))) {
        type = "TEXT";
      } else {
        type = "TEXT";
      }
      setMaxType(key, type);
    }
  })
  return objTypes;
}
/**
 * 
 * @param {{}[]} objects
 */
export function autoSqliteObjectsType(objects) {
  let objTypes = calcSqliteAutoType(objects);
  _.each(objects, (object) => {
    if (typeof object !== "object") {
      console.error(objects)
      return;
    }
    for (let key in objTypes) {
      let objType = objTypes[key], value = object[key], number, m;
      if (objType.type === "NULL") {
        value = null;
      } else if (objType.type === "INTEGER") {
        if (value === "NaN") {
          value = null;
        } else if (!isNaN(number = +value)) {
          value = number;
        } else {
          value = null;
        }
      } else if (objType.type === "REAL") {
        if (value === "NaN") {
          value = null;
        } else if (!isNaN(number = +value)) {
          value = number;
        } else {
          value = null;
        }
      } else if (objType.type === "TEXT") {
        value = (typeof value === 'string') ? value : null;
      }
      object[key] = value;
    }
  })
  return objects;
}

/**
 * 
 * @param {{}[]} objects
 * @param {boolean} date need calc date type?
 * @param {boolean} ignoreBlank ignore blank or empty fields (default false)
 * @param {Object.<string,function>} transferFuncs transfer field to type
 */
export function calcAutoType(objects, date = false, ignoreBlank = false, transferFuncs) {
  /**
   * @type {Object.<string,{type:string}>}
   */
  const objTypes = {};
  if (objects.length === 0) {
    return objTypes;
  }
  /**
   * 
   * @param {string} key 
   * @param {string} type 
   */
  function setMaxType(key, type) {
    if (objTypes[key]) {
      let oldType = objTypes[key].type;
      if (oldType === "string") {
        return;
      }
      if (type !== oldType) {
        if (oldType === "null" || oldType === "undefined") {
          objTypes[key].type = type;
        } else if (ignoreBlank && (type === "null" || type === "undefined")) {
          objTypes[key].type = oldType;
        } else {
          objTypes[key].type = "string";
        }
      }
    } else {
      objTypes[key] = { type };
    }
  }
  _.each(objects, (object, index) => {
    for (let key in object) {
      if (undefined === object[key]) {
        setMaxType(key, "undefined");
        continue;
      }
      if (null === object[key]) {
        setMaxType(key, "null");
        continue;
      }
      if (typeof object[key] !== 'string') {
        setMaxType(key, object[key] instanceof Date ? 'date' : typeof object[key]);
        continue;
      }
      let value = object[key].trim(), number, type, m;
      if (!value) {
        type = "null";
      } else if (value === "true") {
        type = "boolean";
      } else if (value === "false") {
        type = "boolean";
      } else if (value === "NaN") {
        type = "number";
      } else if (!isNaN(number = +value)) {
        type = "number";
      } else if (date && (m = value.match(/^([-+]\d{2})?\d{4}(-\d{2}(-\d{2})?)?(T\d{2}:\d{2}(:\d{2}(\.\d{3})?)?(Z|[-+]\d{2}:\d{2})?)?$/))) {
        type = "date";
      } else {
        type = "string";
      }
      setMaxType(key, type);
    }
  })
  return objTypes;
}

const unescapeTest = /&(#(?:\d+)|(?:#x[0-9A-Fa-f]+)|(?:\w+));?/ig;

export function unescape(html) {
  // explicitly match decimal, hex, and named HTML entities
  return html.replace(unescapeTest, (_, n) => {
    n = n.toLowerCase();
    if (n === 'colon') return ':';
    if (n.charAt(0) === '#') {
      return n.charAt(1) === 'x'
        ? String.fromCharCode(parseInt(n.substring(2), 16))
        : String.fromCharCode(+n.substring(1));
    }
    return '';
  });
}

const caret = /(^|[^\[])\^/g;
export function edit(regex, opt) {
  regex = regex.source || regex;
  opt = opt || '';
  const obj = {
    replace: (name, val) => {
      val = val.source || val;
      val = val.replace(caret, '$1');
      regex = regex.replace(name, val);
      return obj;
    },
    getRegex: () => {
      return new RegExp(regex, opt);
    }
  };
  return obj;
}

const nonWordAndColonTest = /[^\w:]/g;
const originIndependentUrl = /^$|^[a-z][a-z0-9+.-]*:|^[?#]/i;
export function cleanUrl(sanitize, base, href) {
  if (sanitize) {
    let prot;
    try {
      prot = decodeURIComponent(unescape(href))
        .replace(nonWordAndColonTest, '')
        .toLowerCase();
    } catch (e) {
      return null;
    }
    if (prot.indexOf('javascript:') === 0 || prot.indexOf('vbscript:') === 0 || prot.indexOf('data:') === 0) {
      return null;
    }
  }
  if (base && !originIndependentUrl.test(href)) {
    href = resolveUrl(base, href);
  }
  try {
    href = encodeURI(href).replace(/%25/g, '%');
  } catch (e) {
    return null;
  }
  return href;
}

const baseUrls = {};
const justDomain = /^[^:]+:\/*[^/]*$/;
const protocol = /^([^:]+:)[\s\S]*$/;
const domain = /^([^:]+:\/*[^/]*)[\s\S]*$/;

export function resolveUrl(base, href) {
  if (!baseUrls[' ' + base]) {
    // we can ignore everything in base after the last slash of its path component,
    // but we might need to add _that_
    // https://tools.ietf.org/html/rfc3986#section-3
    if (justDomain.test(base)) {
      baseUrls[' ' + base] = base + '/';
    } else {
      baseUrls[' ' + base] = rtrim(base, '/', true);
    }
  }
  base = baseUrls[' ' + base];
  const relativeBase = base.indexOf(':') === -1;

  if (href.substring(0, 2) === '//') {
    if (relativeBase) {
      return href;
    }
    return base.replace(protocol, '$1') + href;
  } else if (href.charAt(0) === '/') {
    if (relativeBase) {
      return href;
    }
    return base.replace(domain, '$1') + href;
  } else {
    return base + href;
  }
}

export const noopTest = { exec: function noopTest() { } };

export function merge(obj) {
  let i = 1,
    target,
    key;

  for (; i < arguments.length; i++) {
    target = arguments[i];
    for (key in target) {
      if (Object.prototype.hasOwnProperty.call(target, key)) {
        obj[key] = target[key];
      }
    }
  }

  return obj;
}

export function splitCells(tableRow, count) {
  // ensure that every cell-delimiting pipe has a space
  // before it to distinguish it from an escaped pipe
  const row = tableRow.replace(/\|/g, (match, offset, str) => {
    let escaped = false,
      curr = offset;
    while (--curr >= 0 && str[curr] === '\\') escaped = !escaped;
    if (escaped) {
      // odd number of slashes means | is escaped
      // so we leave it alone
      return '|';
    } else {
      // add space before unescaped |
      return ' |';
    }
  }),
    cells = row.split(/ \|/);
  let i = 0;

  if (cells.length > count) {
    cells.splice(count);
  } else {
    while (cells.length < count) cells.push('');
  }

  for (; i < cells.length; i++) {
    // leading or trailing whitespace is ignored per the gfm spec
    cells[i] = cells[i].trim().replace(/\\\|/g, '|');
  }
  return cells;
}

// Remove trailing 'c's. Equivalent to str.replace(/c*$/, '').
// /c*$/ is vulnerable to REDOS.
// invert: Remove suffix of non-c chars instead. Default falsey.
export function rtrim(str, c, invert) {
  const l = str.length;
  if (l === 0) {
    return '';
  }

  // Length of suffix matching the invert condition.
  let suffLen = 0;

  // Step left until we fail to match the invert condition.
  while (suffLen < l) {
    const currChar = str.charAt(l - suffLen - 1);
    if (currChar === c && !invert) {
      suffLen++;
    } else if (currChar !== c && invert) {
      suffLen++;
    } else {
      break;
    }
  }

  return str.substr(0, l - suffLen);
}

export function findClosingBracket(str, b) {
  if (str.indexOf(b[1]) === -1) {
    return -1;
  }
  const l = str.length;
  let level = 0,
    i = 0;
  for (; i < l; i++) {
    if (str[i] === '\\') {
      i++;
    } else if (str[i] === b[0]) {
      level++;
    } else if (str[i] === b[1]) {
      level--;
      if (level < 0) {
        return i;
      }
    }
  }
  return -1;
}

export function checkSanitizeDeprecation(opt) {
  if (opt && opt.sanitize && !opt.silent) {
    console.warn('marked(): sanitize and sanitizer parameters are deprecated since version 0.7.0, should not be used and will be removed in the future. Read more here: https://marked.js.org/#/USING_ADVANCED.md#options');
  }
}

export function isJs(token) {
  if (token && token.type === "code") {
    const lang = (token.lang || '').match(/[a-zA-Z]*/)[0];
    if (~["js", "javascript"].indexOf(String(lang).toLowerCase()))
      return true;
  }
  return false;
}

export function isAutoJs(token) {
  if (isJs(token)) {
    const matches = (token.lang || '').match(/[a-zA-Z]*/g).map(function (value) {
      return value.toLowerCase();
    }).slice(1);
    if (~matches.indexOf("auto"))
      return true;
  }
  return false;
}

export function modal(active = true,
  title = "Confrim something",
  body = "Are you sure ?",
  closeFunc = () => { },
  confirmFunc = () => { }) {
  return new Promise((resolve, reject) => {
    let modalEle = document.querySelector(".modal.fade");
    if (!modalEle) {
      reject(new Error("is visible now ,repeat modal!"));
      return;
    }
    let inactiveFunc = () => {
      modalEle.className = modalEle.className.replace(/\s*active*/g, "");
      modalEle.className = modalEle.className.replace(/\s*show*/g, "");
      document.body.className = document.body.className.replace(/\s*modal-open*/g, "");
      modalEle.removeAttribute("role");
      modalEle.removeAttribute("aria-modal");
      modalEle.setAttribute("aria-hidden", "true");
    }
    if (active) {
      if (!/active/.test(modalEle.className))
        modalEle.className += " active";
      if (!/show/.test(modalEle.className))
        modalEle.className += " show";
      if (!/modal-open/.test(document.body.className))
        document.body.className += " modal-open";
      title !== undefined && (modalEle.querySelector(".modal-title").innerHTML = title);
      if (body !== undefined) {
        typeof body === 'string' && (modalEle.querySelector(".modal-body").innerHTML = body);
        typeof body === 'function' && body(modalEle.querySelector(".modal-body"));
      }
      modalEle.setAttribute("role", "dialog");
      modalEle.setAttribute("aria-modal", "true");
      modalEle.removeAttribute("aria-hidden");
      let close = function (e) {
        e.preventDefault();
        let ret = closeFunc && closeFunc();
        inactiveFunc();
        e.currentTarget.removeEventListener("click", close);
        resolve(ret || false);
      };
      let confirm = function (e) {
        e.preventDefault();
        let ret = confirmFunc && confirmFunc(modalEle.querySelector(".modal-body"));
        inactiveFunc();
        e.currentTarget.removeEventListener("click", confirm);
        resolve(ret || true);
      };
      modalEle.querySelector(".cancel").addEventListener("click", close)
      modalEle.querySelector(".close").addEventListener("click", close)
      modalEle.querySelector(".confirm").addEventListener("click", confirm)
    } else {
      inactiveFunc();
    }
  })
}

export const showWrapperObject = (ret, data = document.body, delare) => {
  if (ret) {
    if (typeof ret === "object") {
      if (!delare) {
        data.innerHTML = `<details>
<summary>${ret.constructor.name}</summary>
</details>`;
        showObject(ret, data.querySelector(":scope > details"));
      } else {
        let key = _.keys(ret)[0];
        let value = ret[key];
        if (value && (typeof value === "object")) {
          data.innerHTML = `<details>
  <summary>${key} : ${value.constructor.name}</summary>
  </details>`;
          showObject(value, data.querySelector(":scope > details"));
        } else {
          data.innerHTML = ``;
          const keyValueTemplate = document.getElementById('keyValue');
          const element = document.importNode(keyValueTemplate.content, true);
          element.querySelector('.key').textContent = key;
          element.querySelector('.value').textContent = value;
          data.appendChild(element);
        }
      }
    } else {
      data.innerHTML = `&nbsp;${ret}`;
    }
  } else {
    data.innerHTML = `&nbsp;${ret + ""}`;
  }
}

const MAX_DISPLAY = 20;
export const showObject = (object = {}, parent = document.body, startIndex = 0, show = false) => {
  const keyValueTemplate = document.getElementById('keyValue');
  if (!object || typeof object !== 'object') {
    // Use a template since the structure is somewhat complex.
    const element = document.importNode(keyValueTemplate.content, true);
    element.querySelector('.key').textContent = "Not Object!";
    parent.appendChild(element);
    return;
  }
  const showFunc = (value, key) => {
    if (value && typeof value === "object") {
      // Since this structure is really simple, just create the elements.
      const element = document.createElement('details');
      const summary = element.appendChild(document.createElement('summary'));
      summary.textContent = key;

      if (show) {
        element.setAttribute("open", "true");
        showObject(value, element);
      } else {
        element.addEventListener('toggle', () => {
          showObject(value, element)
        }, { once: true });
      }
      parent.appendChild(element);
    } else {
      // Use a template since the structure is somewhat complex.
      const element = document.importNode(keyValueTemplate.content, true);
      element.querySelector('.key').textContent = key;
      let view;
      switch (typeof value) {
        case "string":
          view = `<span class="text-dark">"${value}"</span>`
          break;
        case "number":
          view = `<span class="text-primary">${value}</span>`
          break;
        case "boolean":
          view = `<span class="text-success">${value}</span>`
          break;
        case "undefined":
          view = `<span class="text-danger">${value}</span>`
          break;
        default:
          view = `<span class="text-muted">${value}</span>`
          break;
      }
      element.querySelector('.value').innerHTML = view;
      // element.querySelector('.type').textContent = typeof value;
      parent.appendChild(element);
    }
  }
  if (object instanceof Array) {
    if (startIndex + MAX_DISPLAY < object.length) {
      _.each(object.slice(startIndex, startIndex + MAX_DISPLAY), (value, index) => {
        showFunc(value, index + startIndex);
      });
      const element = document.createElement('details');
      const summary = element.appendChild(document.createElement('summary'));
      summary.textContent = "... more";
      element.addEventListener('toggle', (e) => {
        parent.removeChild(e.target);
        showObject(object, parent, startIndex + MAX_DISPLAY)
      }, { once: true });
      parent.appendChild(element);
    } else {
      _.each(object.slice(startIndex), (value, index) => {
        showFunc(value, index + startIndex);
      });
    }
  } else {
    _.each(object, (v, k) => {
      showFunc(v, k);
    });
  }
};

/**
 * @param {String} HTML representing a single element
 * @return {Element}
 */
export function htmlToElement(html) {
  var template = document.createElement('template');
  html = html.trim(); // Never return a text node of whitespace as the result
  template.innerHTML = html;
  return template.content.firstChild;
}

/**
* @param {String} HTML representing any number of sibling elements
* @return {NodeList} 
*/
export function htmlToElements(html) {
  var template = document.createElement('template');
  template.innerHTML = html;
  return template.content.childNodes;
}
/**
 * 
 * @param {string} innerHTML
 * @param {string} tagName 
 */
export function createElemnt(innerHTML, tagName = "div", title) {
  let div = document.createElement(tagName || "div");
  div.innerHTML = innerHTML;
  title && (div.title = title);
  return div;
};

export function isHtml(token) {
  return token.type === 'html';
}

export function isDetails(token) {
  return token.type === 'html' && ~['DETAILS'].indexOf(token.tagName);
}

export function isDetailsEle(ele) {
  return ele && ele.tagName && ~['DETAILS'].indexOf(ele.tagName);//, 'SUMMARY'
}

export function checkIsMobile() {
  return window.innerWidth < 650;
}
/**
 * 
 * @param {{type:string}} file 
 */
export function getFileIcon(file) {
  if (isFileCsv(file)) {
    return "fas fa-file-csv";
  } else if (isDir(file)) {
    return "fas fa-folder";
  } else if (isFileEditorJs(file)) {
    return "fas fa-file";
  } else if (isFileImage(file)) {
    return "far fa-file-image";
  } else if (isFileGzip(file)) {
    return "fas fa-file-archive";
  } else if (isFileZip(file)) {
    return "fas fa-file-archive";
  } else if (isFileJs(file)) {
    return "far fa-file-code";
  } else if (isFileHtml(file)) {
    return "far fa-file-code";
  } else if (isFileSvg(file)) {
    return "far fa-file-image";
  } else if (isFileExcel(file)) {
    return "fas fa-file-excel";
  } else if (isFileArrow(file)) {
    return "fas fa-file";
  } else if (isFileDb(file)) {
    return "fas fa-database";
  } else if (isFile(file)) {
    return "fas fa-file";
  } else {
    return "fas fa-file";
  }
}
export function transferType(type) {
  if (type === "application/vnd.ms-excel") {
    type = "text/csv";
  } else if (type === "application/json") {
    type = "text/json";
  } else if (type === "text/javascript") {
    type = "text/js";
  } else if (type === "image/svg+xml") {
    type = "text/svg";
  }
  return type;
}
export const ImageType = ".png";
export const imageTypes = "jpg|png|gif|ps|jpeg|jfif".split("|")
export const imageTypeReg = new RegExp(`\\.(${imageTypes.join("|")})$`);
export const acceptedImageTypes = _.reduce(imageTypes, (prev, curr) => {
  prev.push(`image/${curr}`)
  return prev;
}, []);
export const videoTypes = "mp4|webm|ogg".split("|")
export const videoTypeReg = new RegExp(`\\.(${videoTypes.join("|")})$`);
export const acceptedVideoTypes = _.reduce(videoTypes, (prev, curr) => {
  prev.push(`video/${curr}`)
  prev.push(`application/${curr}`)
  prev.push(`text/${curr}`)
  return prev;
}, []);
export const audioTypes = "mp3|webm|ogg|ogv|wave|wav|x-wav|x-pn-wav".split("|")
export const audioTypeReg = new RegExp(`\\.(${audioTypes.join("|")})$`);
export const acceptedAudioTypes = _.reduce(audioTypes, (prev, curr) => {
  prev.push(`audio/${curr}`)
  prev.push(`application/${curr}`)
  prev.push(`text/${curr}`)
  return prev;
}, []);
export const ExcpetImgAccept = '.json,.csv,.db,.arrow,.xlsx,.js,.html,.svg,.grove,.md,.markdown,.zip,.tgz';
export const GraphXrFileTypes = '.graphxr';
export const ImgAccept = acceptedImageTypes.join(",");
export const AudioAccept = acceptedAudioTypes.join(",");
export const VideoAccept = acceptedVideoTypes.join(",");
export const AllAccept = `${ImgAccept},${AudioAccept},${VideoAccept},${ExcpetImgAccept},${GraphXrFileTypes}`;
export const AllAcceptArr = AllAccept.split(",");
/**
 * 'image/jpg', 'image/png', 'image/gif', 'image/ps', 'image/jpeg', 'image/jfif'
 * @param {typeof DefaultFileData} file 
 * @returns true/false or undefined if file undefined
 */
export function isFileImage(file) {
  return file && acceptedImageTypes.includes(file['type'].toLowerCase())
}
export function isFileNoEdit(file) {
  return isFileImage(file) || isFileGzip(file) || isFileZip(file) || isFileDb(file) || isFileVideo(file) || isFileExcel(file) || isFileArrow(file) || isFileAudio(file) || isFileGraphxr(file);
}
export function isFileGzip(file) {
  return file && (file['type'].toLowerCase() === 'text/tgz' || file['type'].toLowerCase() === 'application/gzip');
}
export function isFileZip(file) {
  return file && (file['type'].toLowerCase() === 'text/zip' || file['type'].toLowerCase() === 'application/zip');
}
export function isFileGraphxr(file) {
  return file && (file['type'].toLowerCase() === 'text/graphxr' ||
    file['type'].toLowerCase() === 'graphxr');
}
export function isFileVideo(file) {
  return file && (acceptedVideoTypes.includes(file['type'].toLowerCase()));
}
export function isFileAudio(file) {
  return file && (acceptedAudioTypes.includes(file['type'].toLowerCase()));
}
/**
 * excel file
 * @param {*} file 
 */
export function isFileExcel(file) {
  return file && (file['type'].toLowerCase() === 'text/xlsx' || file['type'].toLowerCase() === 'text/xls' || file['type'].toLowerCase() === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
}
/**
 * arrow file
 * @param {*} file 
 */
export function isFileArrow(file) {
  return file && file['type'].toLowerCase() === 'text/arrow';
}
/**
 * Json file
 * @param {*} file 
 */
export function isFileJson(file) {
  return file && file['type'].toLowerCase() === 'text/json';
}

export function isFileDb(file) {
  return file && (file['type'].toLowerCase() === 'text/db' || file['type'].toLowerCase() === 'text/sqlite');
}

export function isFilePlain(file) {
  return file && file['type'].toLowerCase() === 'text/plain';
}
/**
 * js file
 * @param {*} file 
 */
export function isFileJs(file) {
  return file && file['type'].toLowerCase() === 'text/js';
}
export function isFileHtml(file) {
  return file && file['type'].toLowerCase() === 'text/html';
}
export function isFileSvg(file) {
  return file && file['type'].toLowerCase() === 'text/svg';
}
/**
 * markdown file
 * @param {*} file 
 */
export function isFileMd(file) {
  return file && (
    ~['text/md', 'text/markdown'].indexOf(file['type'].toLowerCase()) ||
    (
      ~file['name'].indexOf(".") &&
      ~['md', 'markdown'].indexOf(file['name'].substring(file['name'].indexOf(".") + 1).toLowerCase())
    )
  );
}
/**
 * csv file
 * @param {*} file 
 */
export function isFileCsv(file) {
  return file && file['type'].toLowerCase() === 'text/csv';
}

export const isFileEditorJs = file => {
  if (!file) return false;
  else if (isFileJs(file)) return false;
  else if (isFileHtml(file)) return false;
  else if (isFileSvg(file)) return false;
  else if (isFileJson(file)) return false;
  else if (isFileCsv(file)) return false;
  else if (isFileExcel(file)) return false;
  else if (isFileArrow(file)) return false;
  else if (isFileNoEdit(file)) return false;
  else if (isDir(file)) return false;
  else return true;
}

export function formatSizeUnits(bytes) {
  if (bytes >= 1073741824) { bytes = (bytes / 1073741824).toFixed(2) + "GB"; }
  else if (bytes >= 1048576) { bytes = (bytes / 1048576).toFixed(2) + "MB"; }
  else if (bytes >= 1024) { bytes = (bytes / 1024).toFixed(2) + "KB"; }
  else if (bytes > 1) { bytes = bytes + "bytes"; }
  else if (bytes == 1) { bytes = bytes + "byte"; }
  else { bytes = "0bytes"; }
  return bytes;
}
/**
 * key : fileKey , value : blob/file url
 * @type {Object.<string,string>}
 */
export const ImageCache = {};

export const sortObjectByKey = o => _.sortBy(Object.keys(o), n => n.toLowerCase()).reduce((r, k) => (r[k] = o[k], r), {});

export const commonHref = href => /^(ftp|http|https):\/\/[^ "]*$/.test(href);

Date.prototype.format = function (fmt) {
  let o = {
    "Y+": this.getFullYear(),
    "M+": this.getMonth() + 1,
    "d+": this.getDate(),
    "h+": this.getHours(),
    "m+": this.getMinutes(),
    "s+": this.getSeconds(),
    "q+": Math.floor((this.getMonth() + 3) / 3), //quarter (of a year) ;
    "S": this.getMilliseconds()
  };
  if (/(y+)/.test(fmt))
    fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (let k in o)
    if (new RegExp("(" + k + ")").test(fmt))
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  return fmt;
};

export const formatTime = function (timeMs) {
  var date = new Date();
  date.setTime(timeMs)
  return date.format("yyyy-MM-dd hh:mm:ss");
}

/**
 * 
 * @param {number} nowTimeMs 
 * @param {number} timeMs 
 */
export const codeTime = function (nowTimeMs = new Date(), timeMs) {
  let sub = nowTimeMs - timeMs;
  sub /= 1000;
  if (sub < 60) {
    return `${(sub).toFixed(1)}s ago`;
  }
  sub /= 60;
  if (sub < 60) {
    return `${sub.toFixed(1)}minutes ago`;
  }
  sub /= 60;
  if (sub < 60) {
    return `${sub.toFixed(1)}hours ago`;
  }
  sub /= 24;
  if (sub < 30) {
    return `${sub.toFixed(1)}days ago`;
  }
  sub /= 30;
  if (sub < 12) {
    return `${sub.toFixed(1)}months ago`;
  }
  sub /= 12;
  return `${sub.toFixed(1)}years ago`;
}

export function isInViewport(element, top, right, bottom, left) {
  const rect = element.getBoundingClientRect();
  let T = rect.top - (top || 0);
  let L = rect.left - (left || 0);
  let B = rect.bottom - (bottom || 0);
  let R = rect.right - (right || 0);

  return (
    T >= 0 &&
    B <= (window.innerHeight || document.documentElement.clientHeight) &&
    L >= 0 &&
    R <= (window.innerWidth || document.documentElement.clientWidth)
  );
}

export function isVisibleInViewport(element, top, right, bottom, left) {
  const rect = element.getBoundingClientRect();
  let T = rect.top - (top || 0);
  let L = rect.left - (left || 0);
  let B = rect.bottom - (bottom || 0);
  let R = rect.right - (right || 0);
  return (
    ((T >= 0 && T <= (window.innerHeight || document.documentElement.clientHeight)) || (B >= 0 && B <= (window.innerHeight || document.documentElement.clientHeight)))
    &&
    ((L >= 0 && L <= (window.innerWidth || document.documentElement.clientWidth)) || (R >= 0 && R <= (window.innerWidth || document.documentElement.clientWidth)))
  );
}

export const AsyncFunction = eval(`Object.getPrototypeOf(async function () { }).constructor`);
export const GeneratorFunction = eval(`Object.getPrototypeOf(function* () { }).constructor`);
export const AsyncGeneratorFunction = eval(`Object.getPrototypeOf(async function* () { }).constructor`);

/**
 * 
 * @param {{key:string,value:any}[]} objs 
 */
export const assembling = function (objs) {
  return _.reduce(objs, (prev, curr) => {
    const { key, value } = curr;
    prev[key] = value;
    return prev;
  }, {})
}

/**
 * 
 * @param {[]]} ret 
 * @param {HTMLElement} rootEle 
 * @param {string} s 
 */
export const searchEleText = function (ret, rootEle, s) {
  return _.reduce(rootEle.childNodes, (prev, curr, index) => {
    if (curr instanceof Text) {
      if (~curr.textContent.indexOf(s)) {
        prev.push(curr)
      }
    } else {
      searchEleText(prev, curr, s);
    }
    return prev;
  }, ret);
}

/**
 * search string occur positions
 * @param {string} content 
 * @param {string|RegExp} s 
 * @param {boolean} wholeWord 
 */
export const searchFunc = function (content, s, wholeWord = false) {
  if (!content || !s) {
    return [];
  }
  let ret = [];
  if (typeof s === 'string') {
    let startIndex = 0;
    let tmp;
    while (~(tmp = content.substring(startIndex).indexOf(s))) {
      if (wholeWord) {
        if (!(startIndex + tmp === 0 || /^\W$/g.test(content.charAt(startIndex + tmp - 1))) ||
          !(startIndex + tmp + s.length >= content.length || /^\W$/g.test(content.charAt(startIndex + tmp + s.length)))) {
          startIndex = startIndex + tmp + s.length;
          continue;
        }
      }
      ret.push({ start: startIndex, end: startIndex + tmp });
      ret.push({ start: startIndex + tmp, end: startIndex + tmp + s.length, flag: true });
      startIndex = startIndex + tmp + s.length;
    }
    if (startIndex < content.length) {
      ret.push({ start: startIndex, end: content.length });
    }
  } else if (s instanceof RegExp) {
    let startIndex = 0;
    let matches = content.matchAll(s);
    let next;
    let tmp;
    while (!(next = matches.next()).done) {
      let v = next.value[0];
      tmp = next.value.index;
      if (wholeWord) {
        if (!(tmp === 0 || /^\W$/g.test(content.charAt(tmp - 1))) ||
          !(tmp + v.length > content.length || /^\W$/g.test(content.charAt(tmp + v.length)))) {
          // startIndex = tmp + v.length;
          continue;
        }
      }
      ret.push({ start: startIndex, end: tmp });
      ret.push({ start: tmp, end: tmp + v.length, flag: true });
      startIndex = tmp + v.length;
    }
    if (startIndex < content.length) {
      ret.push({ start: startIndex, end: content.length });
    }
  }
  return ret;
}

export const sliceC = 20;
export const maxC = sliceC * 2;
export const SearchType = {
  cm: 'cm',
  nocm: 'nocm',
  others: 'others',
}

/**
 * 
 * @param {string|RegExp} searchValue 
 * @param {string} content 
 * @param {boolean} wholeWord 
 */
export const highligthComp = function (searchValue, content, wholeWord = false) {
  if (!content || !searchValue) {
    return;
  }
  let ret = searchFunc(content, searchValue, wholeWord);
  if (ret.length < 2) {
    return;
  }
  return _.map(ret, (obj, index, ret) => {
    const { start, end, flag } = obj;
    if (!flag) {
      if (index == 0) {
        return <span key={index} className={flag ? "highlight-warning" : ""}>{end - start > sliceC ? `...${content.substring(end - sliceC, end)}` : content.substring(start, end)}</span>
      } else if (!ret[index + 1]) {
        return <span key={index} className={flag ? "highlight-warning" : ""}>{end - start > sliceC ? `${content.substring(start, start + sliceC)}...` : content.substring(start, end)}</span>
      } else {
        return <span key={index}>
          <span className={flag ? "highlight-warning" : ""}>{end - start > maxC ? `${content.substring(start, start + sliceC)}...` : content.substring(start, parseInt((end + start) / 2))}</span>
          <br />
          <span className={flag ? "highlight-warning" : ""}>{end - start > maxC ? `...${content.substring(end - sliceC, end)}` : content.substring(parseInt((end + start) / 2), end)}</span>
        </span>
      }
    } else {
      return <span key={index} className={flag ? "highlight-warning" : ""}>{content.substring(start, end)}</span>
    }
  })
}

function componentToHex(c) {
  let hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

export function rgbaToHex(r, g, b, a) {
  return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b) + a ? componentToHex(a) : "";
}

export function rgba2hex(orig) {
  let a, isPercent,
    rgb = orig.replace(/\s/g, '').match(/^rgba?\((\d+),(\d+),(\d+),?([^,\s)]+)?/i),
    alpha = (rgb && rgb[4] || "").trim(),
    hex = rgb ?
      (rgb[1] | 1 << 8).toString(16).slice(1) +
      (rgb[2] | 1 << 8).toString(16).slice(1) +
      (rgb[3] | 1 << 8).toString(16).slice(1) : orig;
  if (alpha !== "") {
    a = alpha;
  } else {
    a = 1;
  }
  // multiply before convert to HEX
  a = ((a * 255) | 1 << 8).toString(16).slice(1)
  hex = hex + a;
  return hex;
}

/**
* ECMA2016 / ES6
*/
export const convertHexToRGBA = (hexCode, opacity = 1) => {
  let hex = hexCode.replace('#', '');
  if (hex.length === 3) {
    hex = `${hex[0]}${hex[0]}${hex[1]}${hex[1]}${hex[2]}${hex[2]}`;
  }
  const r = parseInt(hex.substring(0, 2), 16);
  const g = parseInt(hex.substring(2, 4), 16);
  const b = parseInt(hex.substring(4, 6), 16);
  /* Backward compatibility for whole number based opacity values. */
  if (opacity > 1 && opacity <= 100) {
    opacity = opacity / 100;
  }
  return `rgba(${r},${g},${b},${opacity})`;
};

export function getViewport() {
  // https://stackoverflow.com/a/8876069
  const width = Math.max(
    document.documentElement.clientWidth,
    window.innerWidth || 0
  )
  let viewport = "xxs";
  _.each(DEFAULT_BREAK_POINTS, (v, k) => {
    if (width - (DEFAULT_CELL_PADDING[k] + 5) * 2 >= v) {
      viewport = k;
      return false;
    }
  })
  return viewport;
}
/**
   * parse memory file data
   * @param {File} file 
   */
export const parse = async function (file) {
  // Always return a Promise
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    // Wait till complete
    reader.onloadend = function (e) {
      let content = e.target.result;
      resolve(content);
    };
    // Make sure to handle error states
    reader.onerror = function (e) {
      reject(e);
    };
    reader.readAsText(file);
  });
}

export function dsv(text, delimiter, { array = false, typed = false } = {}) {
  return (delimiter === "\t" ? (array ? tsvParseRows : tsvParse) : array ? csvParseRows : csvParse)(
    text,
    typed && d3AutoType
  );
}