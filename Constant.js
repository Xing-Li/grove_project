const Path = require("path");

const ONE_MINUTE = 60 * 1000;
/** The milliseconds of one day */
const ONE_DAY = 1 * 24 * 60 * ONE_MINUTE;

const IS_PRODUCTION = process.env.NODE_ENV === "production";
const IS_APP_PACK = process.env.APP_TYPE === 'app';

/** Project path, base with ./build dir */
const ROOT_PATH = Path.join(__dirname, "./..");
/** Temp files path, eg. upload image to the dir*/
let TEMP_PATH = Path.join(ROOT_PATH, "./tmp/")
/** Web and static resource */
const STATIC_PATH = Path.join(ROOT_PATH, "./static/");
let EXTENSION_SYS_PATH = Path.join(ROOT_PATH, "./web/static/tempModules");
let EXTENSION_PATH = Path.join(EXTENSION_SYS_PATH, "./site");
let APP_PATH = ROOT_PATH;

const ADMIN_EMAIL = AppConfig && AppConfig.adminEmail ? String(AppConfig.adminEmail).trim().toLowerCase() : '';

const TIME_ZONE = typeof (AppConfig.timeZone) != 'undefined' && !isNaN(parseInt(AppConfig.timeZone))
  ? parseInt(AppConfig.timeZone)
  : -8;

const AUDIT_LOGS = Object.assign({
  datePattern: "YYYY-MM-DD",
  zippedArchive: true,
  filename: "graphxr.log.%DATE%",
  maxSize: "500m",
  maxFiles: "60d",
}, AppConfig.auditLogs || {});

const SEARCH_BOTS = [
  "Googlebot",
  "Bingbot",
  "Slurp",
  "Yahoo",
  "DuckDuckBot",
  "Baiduspider",
  "YandexBot",
  "Sogou",
  "Konqueror",
  "Exabot",
  "facebot",
  "ia_archiver",
  "alexa",
];

if (IS_APP_PACK) {
  TEMP_PATH = Path.join(process.cwd(), "./data");
  EXTENSION_PATH = Path.join(process.cwd(), "./extensions");
  APP_PATH = process.cwd();
}

module.exports = {
  ONE_MINUTE,
  ONE_DAY,

  IS_PRODUCTION,
  IS_APP_PACK,

  AUDIT_LOGS,

  ROOT_PATH,
  APP_PATH,
  TEMP_PATH,
  STATIC_PATH,
  EXTENSION_SYS_PATH,
  EXTENSION_PATH,

  ADMIN_EMAIL,
  TIME_ZONE,

  SEARCH_BOTS,
};
