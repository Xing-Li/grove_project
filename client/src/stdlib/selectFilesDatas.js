import observe from "@observablehq/stdlib/src/generators/observe.js";
import { getFilesColumnsData } from "../block/antchart/antchartUtils";
import actions from "../define/Actions";

export default function () {
  return observe(function (change) {
    const refreshFileAttachmentsElement = document.createElement("div");
    // const fileChangeElement = document.createElement("div");
    // const linkChangeElement = document.createElement("div");
    change(getFilesColumnsData);
    actions.inspector(refreshFileAttachmentsElement, [actions.types.REFRESH_FILE_ATTACHMENTS], () => {
      change(getFilesColumnsData);
    })
    // actions.inspector(fileChangeElement, [actions.types.FILE_CHANGE], () => {
    //   change(getFilesColumnsData);
    // })
    // actions.inspector(linkChangeElement, [actions.types.LINK_CHANGE], () => {
    //   change(getFilesColumnsData);
    // })
    return function () {
      actions.deleteCache(refreshFileAttachmentsElement);
      // actions.deleteCache(fileChangeElement);
      // actions.deleteCache(linkChangeElement);
    };
  });
}
