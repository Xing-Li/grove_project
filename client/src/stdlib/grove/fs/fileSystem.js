import Common from '../../../util/Common';
import { PROJECT_ID, USER_ID } from "../../../util/localstorage";
import { compressBlob, decompressBlob } from './compression';

/**
 * Save data to the file system.
 * path: required. e.g. "dir/data.json" 
 * [blob]: e.g. `new Blob(["Hello, world!"], {type : 'text/plain'})`
 * [json]: e.g. `{a: 1, b: 2}`
 */
export async function save({path, blob, json,}) {
  let fd = new FormData();
  fd.append("fileKey", path);
  fd.append("fileName", path.split("/").pop());
  fd.append("unzip", false);

  let data;
  if (blob) {
    data = blob;
  } else if (json) {
    data = new Blob([JSON.stringify(json)], {type: "application/json"});
  } else {
    throw new Error("save: blob or json required");
  }

  const compressedData = await compressBlob(data);
  fd.append("data", compressedData);

  fd.append("userId", USER_ID);
  fd.append("projectId", PROJECT_ID);
  return await Common.fetch(`/api/grove/uploadFile`, {
    method: "POST",
    body: fd,
  });
}


/**
 * Load data from the file system.
 * path: required. e.g. "dir/data.json"
 * [type]: if "json", return json object. otherwise return Response
 */
export async function load({path, type}) {
  const response = await Common.fetch(`/tmp/observable/projects/${PROJECT_ID}/${path}`)
  if (response.redirected) return undefined;
  const compressedBlob = await response.blob();
  const blob = await decompressBlob(compressedBlob);
  if (type === "json") {
    const text = await blob.text();
    return JSON.parse(text);
  } else {
    return blob;
  }
}