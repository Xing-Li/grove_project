var fs = require('fs');
var path = require('path');
var child_process = require('child_process');
const { ROOT_PATH ,TEMP_PATH } = require('../../Constant');

var exec = function () {
    let command =arguments[0];

    if ((/win(32|64)/).test(process.platform)) {
        command = `powershell.exe ${command}`;
    }
    console.log(command);
    return child_process.exec(command, arguments[1]);

};
var RecordingUtil = {
    saveImage(name, index, data) {
        if (!(data instanceof Buffer) && !(/^data\:image\/png\;base64/ig).test(data)) {
            return console.error(new Error("Only support png buffer blob, or base64 data"));
        }
        var wstream = fs.createWriteStream(path.join(ROOT_PATH, './tmp/' + name + '/frame-' + index + '.png'));
        // wstream.on('finish', function () {
        // console.log('file has been written');
        // });
        if (data instanceof Buffer) {
            wstream.write(data);
        } else if ((/^data\:image\/png\;base64/ig).test(data)) {
            wstream.write(Buffer.from(data.split(',').pop(), 'base64'));
        }
        wstream.end();
        wstream = null;
    },

    saveVedio(name, index, data) {

        // fs.writeFileSync(path.join(ROOT_PATH, './tmp/' + name + '/stream-' + index + '.webm'),
        //  Buffer.from(data.split(',').pop(), 'base64'));

        var wstream = fs.createWriteStream(path.join(ROOT_PATH, './tmp/' + name + '/stream-' + index + '.webm'));

        if (data instanceof Buffer) {
            wstream.write(data);
        } else if ((/^data[\:\/a-zA-Z0-9]+;base64/ig).test(data)) {
            wstream.write(Buffer.from(data.split(',').pop(), 'base64'));
        }
        wstream.end();
        wstream = null;

    },

    add360VideoMetadata(tmpName, cb) {
        cb = cb || function () { }
        console.log("add 360 video matedata to MP4 <" + tmpName + ">");
        if (!(/darwin/ig).test(process.platform)) {
            console.warn("Only support mac os add  360 videmo metadata");
            return cb();
        }

        var currentPath = path.join(ROOT_PATH, './modules/thirdParties/').replace(/\s+/g, "\\ ");
        var injectionMetadataFileName = tmpName + "_injection_metadata";
        exec(" cd " + currentPath + "  &&   python spatialmedia -i --stereo=none  ./../../tmp/" + tmpName + ".mp4  ./../../tmp/" + injectionMetadataFileName + ".mp4  ", function (err, stdout, stderr) {
            if (!err) {
                console.log("add 360 video matedata to MP4 <" + tmpName + ">  ====== success");
            } else {
                console.error(err)
            }
            cb(err, injectionMetadataFileName);
        });

    },
    clearnData(tmpName, cb, onlyClear) {
        console.log("Clear the data before 1 day and mkdir <" + tmpName + ">");
        var currentPath = path.join(ROOT_PATH, './tmp').replace(/\s+/g, "\\ ");
        exec(" cd " + currentPath + " && find * -mmin +120 -print0 | xargs -0 rm -rf && rm -rf " + tmpName + " &&  mkdir " + tmpName, function (err, stdout, stderr) {
            if (cb) {
                cb(err);
            }
        });
    },

    generateMP4FromImages(tmpName, cb, is360) {
        console.log("generate MP4 from images <" + tmpName + ">");
        var ffmpegCommand = is360 ? "  ffmpeg -i frame-%d.png -c:v libx264 -profile:v high  -pix_fmt yuv420p " : " ffmpeg -r 60 -i frame-%d.png  -c:v libx264 -profile:v high -pix_fmt yuv420p "
        var currentPath = path.join(ROOT_PATH, './tmp/', tmpName).replace(/\s+/g, "\\ ");
        exec(" cd " + currentPath + "  &&  " + ffmpegCommand + " ./../" + tmpName + ".mp4 -y ", function (err, stdout, stderr) {
            if (is360) {
                RecordingUtil.add360VideoMetadata(tmpName, cb);
            } else if (cb) {
                cb(err)
            }

        });
    },

    generateMP4FromStream(tmpName, cb, is360) {
        console.log("convert webm to mp4 <" + tmpName + ">");
        var currentPath = path.join(ROOT_PATH, './tmp/', tmpName).replace(/\s+/g, "\\ ");
        exec(
            `cd ${currentPath}  
        && for f in ./*.webm; do echo "file '$f'" >> webmlist.txt; done  
        && ffmpeg -f concat -safe 0 -i webmlist.txt -c copy ${tmpName}.webm
        && ffmpeg -i ${tmpName}.webm ./../${tmpName}.mp4 -y`.replace(/\n/ig, ' '),
            function (err, stdout, stderr) {
                if (is360) {
                    RecordingUtil.add360VideoMetadata(tmpName, cb);
                } else if (cb) {
                    cb(err)
                }
            });
    }
}

module.exports = RecordingUtil;