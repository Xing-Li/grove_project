import { Button, Dropdown, Input, Menu, Select, Space } from 'antd';
import { fileDownloadHandler } from 'block/markdown/FileHandler';
import { DownOutlined } from '@ant-design/icons';
import _ from 'lodash';
import PropTypes from "prop-types";
import React from "react";
import { copyContent, saveLink, showMessage } from 'util/utils';
import AntChartSetting from '../AntChartSetting';
import { DataComp } from '../antchartUtils';
import { RawJSONComp } from './TableComp';
import { extractLinks } from 'js/linkgopher';
import { extractAllKeys, ConvertToCSV, ConvertToXLSX } from 'util/helpers';
import { RenderType } from 'util/databaseApi';
import { getExOptions } from './CheckChartTypes';
import { showToast } from '../../../util/utils';
const { Option } = Select;
/**@type {import('html2canvas').default} */
const html2canvas = window.html2canvas;

export default class ExportFiles extends React.Component {
    static propTypes = {
        settings: PropTypes.object,
        className: PropTypes.string,
    };

    static defaultProps = {};
    constructor(props) {
        super(props);
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        const { state: { chartEditor: { state: { selectedKey } }, chartConfig, chartConfig: { chartData, chartColumns, type, options }, current } } = settings;

        this.state = {
            className: this.props.className,
            title: options.title.text,
            csvTitle: options.title.text,
        }
    }

    componentDidUpdate(preProps, preState) {
        const { className } = this.props;
        let state = {};
        if (preProps.className !== className) {
            _.assign(state, { className });
        }
        !_.isEmpty(state) && this.setState(state);
    }

    componentWillUnmount() {

    }

    render() {
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        const { state: { chartEditor: { state: { chartConfigs, selectedKey } }, chartConfig, chartConfig: { chartData, chartColumns, type }, current } } = settings;
        const currentChartData = chartData.length ? chartData : chartConfigs[chartConfig.chartKey].chartData;
        const { className, title, csvTitle } = this.state;
        const imageTypes = ["png", "jpeg"];
        const menu = (
            <Menu onClick={(info) => {
                /**@type {AntChartSetting} */
                const settings = this.props.settings;
                const { state: { chartEditor: { state: { selectedKey } } } } = settings;
                let imageType = info.key
                let canvas = document.querySelector(`.react-grid-item.chart-${selectedKey}.selected`)
                if (!canvas) return;
                window.currentEditor.setWrapperLoading(true, () => {
                    let scrollY = window.pageYOffset || window.scrollY || document.body.scrollTop;
                    let scrollX = window.pageXOffset || window.scrollX || document.body.scrollLeft;
                    window.scrollTo(0, 0);
                    canvas.classList.toggle("selected", false);
                    html2canvas(canvas, { backgroundColor: getComputedStyle(document.body).getPropertyValue('--background-color') }).then(function (tcanvas) {
                        let imgData = tcanvas.toDataURL(`image/${imageType}`, 1.0);
                        imgData && saveLink(imgData, title);
                        window.scrollTo(scrollX, scrollY);
                        canvas.classList.toggle("selected", true);
                        window.currentEditor.setWrapperLoading(false);
                    }, () => {
                        showToast(`export image/${imageType} fail!`, "error");
                        window.scrollTo(scrollX, scrollY);
                        canvas.classList.toggle("selected", true);
                        window.currentEditor.setWrapperLoading(false);
                    });
                })
            }}>
                {
                    _.map(imageTypes, (type, index) => {
                        return <Menu.Item key={type}>.{type}</Menu.Item>
                    })
                }
            </Menu>
        );
        const fileTypes = ["csv", "xlsx"];
        const fileMenu = (
            <Menu onClick={(info) => {
                /**@type {AntChartSetting} */
                const settings = this.props.settings;
                const { state: { chartEditor: { state: { selectedKey } } } } = settings;
                let fileType = info.key
                window.currentEditor.setWrapperLoading(true, async () => {
                    if (fileType === 'csv') {
                        let content = ConvertToCSV(currentChartData);
                        const file = new File([content], { type: 'text/plain', endings: 'transparent' });
                        const url = URL.createObjectURL(file);
                        saveLink(url, csvTitle + ".csv");
                    } else if (fileType === 'xlsx') {
                        let buffer = await ConvertToXLSX(csvTitle, currentChartData);
                        const blob = new Blob([buffer], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8' });
                        saveLink(URL.createObjectURL(blob), csvTitle + ".xlsx");
                    }
                    window.currentEditor.setWrapperLoading(false);
                });
            }}>
                {
                    _.map(fileTypes, (type, index) => {
                        return <Menu.Item key={type}>.{type}</Menu.Item>
                    })
                }
            </Menu>
        );
        let allKeys = extractAllKeys(currentChartData)
        let columns = _.reduce(allKeys, (prev, key, index, list) => {
            prev.push({
                key: key,
                title: key,
                dataIndex: key,
            });
            return prev;
        }, []);
        let content = `{
    let type = "${chartConfig.type}"
    const container = document.createElement("div");
    let options = ${JSON.stringify(getExOptions(_.assign({}, chartConfig, { chartData: currentChartData })), undefined, 1)}
    let constructorFunc = chartConstructor(type);
    if (constructorFunc) {
        delete options.width;
        delete options.height;
    } else {
        return container;
    }
    const chart = new constructorFunc(container, options);
    chart.render();
    return container;
}`
        let canvas = document.querySelector(`.react-grid-item.chart-${selectedKey}.selected`)
        return <div className="normal-icon">
            <Space direction="vertical" size='small'>
                <Space direction="horizontal" size="middle">
                    <h2>Images</h2>
                    <Input value={title} onChange={(e) => {
                        this.setState({ title: e.target.value })
                    }}></Input>
                    <Dropdown style={{ width: 120 }} overlay={menu}>
                        <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                            Export <DownOutlined />
                        </a>
                    </Dropdown>
                    {!!canvas && `${canvas.getBoundingClientRect().width} X ${canvas.getBoundingClientRect().height} px`}
                </Space>
                <h2>Data</h2>
                <Space direction="horizontal" size="middle">
                    <Input value={csvTitle} onChange={(e) => {
                        this.setState({ csvTitle: e.target.value })
                    }}></Input>
                    <Dropdown style={{ width: 120 }} overlay={fileMenu}>
                        <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                            Export <DownOutlined />
                        </a>
                    </Dropdown>
                    {currentChartData.length}Rows.
                </Space>
                <h2>Code&nbsp;&nbsp;<i title={"Copy Source"} className="icon fas fa-copy" onClick={
                    () => {
                        copyContent(content, () => {
                            showMessage("copy success!", "success")
                        })
                    }
                }></i></h2>
                <RawJSONComp className="limit-height" renderType={RenderType.Code} result={content} onChange={(cm, data, value) => {
                }}></RawJSONComp>
                <DataComp open={true} data={chartData} columns={columns}></DataComp>
            </Space>
        </div>
    }
}