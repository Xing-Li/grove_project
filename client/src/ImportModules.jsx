import { Tree } from 'antd';
import PropTypes from "prop-types";
import React from 'react';
import { Link } from 'stdlib/link';
import { ROOT_URI, getModule } from 'util/hqUtils';
import actions from "./define/Actions";

const mapTreeData =
    /**
    * 
    * @param {Object.<string,{names:[],localNames:[]}} importModules 
    * @param {import ("./editorAbs").default} editor 
    */
    function (importModules, editor) {
        if (!importModules) { return undefined }
        return _.map(importModules, (importModule, v) => {
            const { names, localNames } = importModule;
            let key = v;
            if (v.startsWith(editor.getUploadUri())) {
                v = v.replace(editor.getUploadUri(), "");
            } else {
                v = v.startsWith(ROOT_URI) ? v.replace(ROOT_URI, "%") : v;
            }
            let module = getModule(key);
            return {
                title: <div key={v} title={_.isEqual(names, localNames) ? `${names}` : `${names}=>${localNames}`}>
                    <Link fileKey={v} text={v}></Link> <i title="Jump to defining cell" className="icon fas fa-crosshairs" onClick={() => {
                        editor.focusImportSource(v)
                    }}></i>
                </div>,
                key: key,
                isLeaf: module ? !!!_.keys(module._cachedata.importModules).length : true
            }
        })
    }

export default class ImportModules extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        editor: PropTypes.object,
        info: PropTypes.object,
    };
    static defaultProps = {};
    constructor(props) {
        super(props);
        /**@type {import ("./editorAbs").default} */
        const editor = this.props.editor;
        const { className, info } = this.props;
        let importModulesTmp = editor.getMain() ? editor.getMain()._cachedata.importModules : undefined;
        this.state = {
            className,
            editor,
            info,
            importModules: importModulesTmp,
            treeData: mapTreeData(importModulesTmp, editor)
        }
        this.runCompleteElement = document.createElement("div");
    }

    componentDidMount() {
        actions.inspector(this.runCompleteElement, [actions.types.RUN_COMPLETE],
            ({ editorKey }) => {
                const { editor } = this.state;
                if (editor.getTransferKey() === editorKey) {
                    let importModulesTmp = editor.getMain() ? editor.getMain()._cachedata.importModules : undefined;
                    if (importModulesTmp) {
                        this.setState({
                            importModules: importModulesTmp,
                            treeData: mapTreeData(importModulesTmp, editor)
                        });
                    }
                }
            })
    }

    componentWillUnmount() {
        actions.deleteCache(this.runCompleteElement);
    }

    componentDidUpdate(prevProps, prevState) {
        let state = {};
        /**@type {import ("./editorAbs").default} */
        const editor = this.props.editor;
        const { className } = this.props;
        /**@type {Object.<string,{names:[],localNames:[]}} */
        let importModulesTmp;
        if (prevProps.className !== className) {
            _.assign(state, { className });
        }
        if (prevProps.editor !== editor) {
            _.assign(state, { editor });
            importModulesTmp = editor.getMain()._cachedata.importModules;
        }
        if (importModulesTmp) {
            _.assign(state, {
                importModules: importModulesTmp,
                treeData: mapTreeData(importModulesTmp, editor)
            });
        }
        if (!_.isEmpty(state)) {
            this.setState(state, async () => {
            });
        }
    }

    getKeys = (list = [], keys = new Set()) => {
        _.each(list, (node) => {
            keys.add(node.key);
            if (node.children) {
                this.getKeys(node.children, keys);
            }
        });
        return keys;
    }

    updateTreeData = (list, key, children) => {
        return _.map(list, (node) => {
            if (node.key === key) {
                return { ...node, children };
            }

            if (node.children) {
                return { ...node, children: this.updateTreeData(node.children, key, children) };
            }

            return node;
        });
    }

    /**
     * 
     * @param {ShapeEditor} editor 
     * @param {[{}]} treeData 
     */
    getTreeData = (editor, treeData) => {
        if (!treeData || !treeData.length) {
            return treeData;
        }
        _.each(treeData, (data, index) => {
            const { key, isLeaf, children } = data;
            if (isLeaf || children) {
                return;
            }
            let nchildren = []
            let module = getModule(key);
            if (module) {
                let importModulesTmp = _.cloneDeep(module._cachedata.importModules);
                let keys = this.getKeys(this.state.treeData, new Set());
                _.each(Array.from(keys), (key) => {
                    if (importModulesTmp[key]) {
                        delete importModulesTmp[key]
                    }
                })
                nchildren = mapTreeData(importModulesTmp, editor) || [];
            }
            data.children = nchildren;
            nchildren.length && this.getTreeData(editor, nchildren);
        })
        return treeData;
    }
    render() {
        const { editor, treeData, info, className } = this.state;
        if (!treeData) {
            return <div></div>;
        }
        const loopTreeData = this.getTreeData(editor, treeData);
        let ind = this.getKeys(loopTreeData).size - loopTreeData.length;
        return <div className={`${!!className && className}`}  >
            <div className="d-flex justify-content-between align-items-center">
                <div className="h6">{loopTreeData.length ? `${loopTreeData.length} direct imports, ${ind} indirect imports.` : "This notebook has no dependencies."}  </div>
                <div className="mr4 cursorp" title="Refresh" onClick={() => {
                    let importModulesTmp = editor.getMain()._cachedata.importModules;
                    if (importModulesTmp) {
                        this.setState({
                            importModules: importModulesTmp,
                            treeData: mapTreeData(importModulesTmp, editor)
                        });
                    }
                }}><i className="icon fas fa-sync-alt"></i></div>
            </div>
            <Tree treeData={loopTreeData} />
        </div>
    }
}