import _ from 'lodash';
import React from "react";
import ReactDOM from "react-dom";
import editor from 'commonEditor';
import { v4 as uuidv4, NIL as NIL_UUID } from 'uuid';
import PlotChartEditor, { PlotChartData } from "./PlotChartEditor";
import { cell_shortcuts_keys, IsKey, linkKeys } from 'util/utils';
import { createElemnt } from 'util/helpers';
import { TemplateValues } from './PlotChartTools';

/**
 * PlotChart for the Editor.js 2.0
 */
export default class PlotChart {
    static get toolbox() {
        return {
            title: 'Plot Chart',
            icon: '<i class="fas fa-chart-line"></i>'
        };
    }
    /**
     * Render plugin`s main Element and fill it with saved data
     *
     * @param {object} options - block constructor options
     * @param {PlotChartData} options.data - previously saved data
     * @param {object} options.config - user config for Tool
     * @param {object} options.api - Editor.js API
     * @param {boolean} options.readOnly - read only mode flag
     */
    constructor({ data, config, api, readOnly }) {
        this.api = api;
        this.readOnly = readOnly;
        this.CSS = {
            baseClass: this.api.styles.block,
            input: this.api.styles.input,
            wrapper: 'ce-plot-chart',
        };
        /**@type{import ('../../tagEditor').default} */
        this.editor = (!config.editorjsKey || config.editorjsKey === editor.getTransferKey()) ? editor :
            _.filter(window.tagEditors, (tagEditor) => { return tagEditor.getTransferKey() === config.editorjsKey })[0];
        this.renderTask = this.editor.renderTask;
        if (!data.dname) {
            data.dname = uuidv4();
        }
        if (!data.customise) {
            data.customise = {}
        }
        if (!data.marks) {
            data.marks = {}
        }
        /**
         * @type {PlotChartData}
        */
        this.data = data;
        this.ref = React.createRef();
        this.initSettingWrapper();
    }

    render() {
        this.wrapper = document.createElement('div');
        this.wrapper.classList.add(this.CSS.baseClass, this.CSS.wrapper);
        this.renderTask.submitTask((callback) => {
            ReactDOM.render(<PlotChartEditor
                ref={this.ref}
                editor={this.editor}
                data={this.data}
                saveData={this.saveData}
                api={this.api}
                readOnly={this.readOnly}
                settingWrapper={this.settingWrapper}
            ></PlotChartEditor>, this.wrapper, () => {
                callback(this.ref.current);
            });
        })
        return this.wrapper;
    }

    beforeRemoved(params) {
        if (this.ref.current) {
            let data = this.wrapper.querySelector(".code-edit-result > .data");
            this.editor.deleteCacheV(data);
            if (!params || params.type !== 'all') {
                this.editor.refreshAnchors();
                this.editor.refreshAttaches(this.wrapper.closest('.ce-block'));
                this.editor.refreshRuns(this.wrapper.closest('.ce-block'));
            }
            ReactDOM.unmountComponentAtNode(this.wrapper)
        }
    }

    isFirstBlock() {
        return this.api.blocks.getCurrentBlockIndex() === 0;
    }

    initSettingWrapper = () => {
        this.settingWrapper = document.createElement('div')
        const settings = [
            {
                name: 'edit-code',
                icon: `<i class="fas fa-pen"></i>`,
                func: "editCode",
                title: "Edit Chart",
                className: 'btn-no-fill',
            },
            {
                name: 'show-code',
                icon: `<i class="fas fa-code"></i>`,
                func: "showCodeFunc",
                title: "Show Code",
                className: 'btn-no-fill',
            },
        ];
        settings.forEach(tune => {
            let button = document.createElement('div');
            this.api.tooltip.onHover(button, tune.title, { placement: "top" });
            button.classList.add(tune.name, 'cdx-settings-button', tune.className);
            button.innerHTML = tune.icon;
            button.addEventListener('click', (domEvent) => {
                this.ref.current[tune.func](...(tune.args ? tune.args() : []));
                // button.classList.toggle('cdx-settings-button--active');
            });
            this.settingWrapper.appendChild(button);
        });
    }

    renderSettings() {
        return this.settingWrapper;
    }

    saveData = (data) => {
        this.data = data;
        this.editor.setNeedSave(true);
    }

    save(blockContent) {
        return this.data;
    }

    keydownFunc(e) {
        e = e || window.event;
        let key = String.fromCharCode(e.keyCode).toLowerCase();
        //filter the number & char key
        if (!(/^[0-9a-z]$/g).test(key) || e.key === e.code) {
            key = e.key;
        }
    }

    static get isReadOnlySupported() {
        return true;
    }

    static get notSupportBackspace() {
        return true;
    }

    static get enableLineBreaks() {
        return true;
    }
}