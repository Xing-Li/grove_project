import { Cascader, Collapse, Select } from 'antd';
import actions from 'define/Actions';
import { DrawerType } from 'editorSettings';
import _ from 'lodash';
import PropTypes from "prop-types";
import React from "react";
import { CategoryFromType, uuidValidateV4, ShortcutTitle, window_shortcuts_keys, HelpType } from "../../util/utils";
import { getCatogeries } from "../antchart/antchartUtils";

const { DefaultDatabase } = require("util/databaseApi");
const { Option } = Select;
const { Panel } = Collapse;
export const FilesComp = function () {
    return <div className="normal-icon">
        {CategoryFromType.files}
        <i title={ShortcutTitle(window_shortcuts_keys['Open Data Pane'])} className="icon icon-inactive icon-xs fas fa-paperclip"
            onClick={() => {
                actions.variable(actions.types.DRAWER_TYPE, [], () => {
                    return {
                        drawerObj: DrawerType.FileAttachments,
                        cb: () => { }
                    }
                })
            }}
        ></i>
    </div>;
}
export const DatabasesComp = function () {
    return <div className="normal-icon">
        {CategoryFromType.database}
        <i title={ShortcutTitle(window_shortcuts_keys['Open Settings Pane'], "Add Database Configuration")} className="icon icon-inactive icon-xs fas fa-database"
            onClick={() => {
                actions.variable(actions.types.DRAWER_TYPE, [], () => {
                    return {
                        drawerObj: DrawerType.Settings,
                        cb: () => { },
                        dprops: { tabActiveKey: "Databases" }
                    }
                })
            }}
        ></i>
    </div>;
}
export const NeodashComp = function () {
    return <div className="normal-icon">
        {CategoryFromType.neo4j}
        <i title={ShortcutTitle(window_shortcuts_keys['Help?'], "Reference Neo4j Report Examples")} className="icon icon-inactive icon-xs fas fa-chart-area"
            onClick={() => {
                actions.variable(actions.types.DRAWER_TYPE, [], () => {
                    return {
                        drawerObj: DrawerType.Help,
                        cb: () => { },
                        dprops: { defaultActiveKey: HelpType['Neo4j Report Examples'] }
                    }
                })
            }}
        ></i>
    </div>;
}

export default class ChooseDatas extends React.Component {
    static propTypes = {
        optionTypes: PropTypes.arrayOf(PropTypes.string),
        className: PropTypes.string,
        selectedCategoryFrom: PropTypes.string,
        selectedCategory: PropTypes.string,
        onCategoryChanged: PropTypes.func,
        editor: PropTypes.object,
        dname: PropTypes.string,
    }

    static defaultProps = {};
    constructor(props) {
        super(props);
        const { optionTypes, className, selectedCategoryFrom, selectedCategory, onCategoryChanged, editor, dname } = this.props;
        this.editor = editor;
        this.dname = dname;
        const categoriesOptions = [];
        ~optionTypes.indexOf(CategoryFromType.main) && (categoriesOptions[optionTypes.indexOf(CategoryFromType.main)] =
        {
            value: CategoryFromType.main,
            label: CategoryFromType.main,
            children: getCatogeries(this.editor, CategoryFromType.main).map((value) => {
                return {
                    value: value,
                    label: value,
                }
            })
        });
        ~optionTypes.indexOf(CategoryFromType.shareData) && (categoriesOptions[optionTypes.indexOf(CategoryFromType.shareData)] =
        {
            value: CategoryFromType.shareData,
            label: CategoryFromType.shareData,
            children: getCatogeries(this.editor, CategoryFromType.shareData).map((value) => {
                return {
                    value: value,
                    label: <div>
                        {value}
                        {uuidValidateV4(value) && <a onClick={(e) => {
                            let editorKey = this.editor.getTransferKey();
                            let className = `editorjs-anchor-code-${value}`;
                            let data = document.querySelector(`.editorjs-wrapper.${editorKey}>.editorjs .${className}`);
                            data && data.closest(".code-edit") && data.closest(".code-edit").editCode(true);
                            e.preventDefault(); e.stopPropagation();
                        }} className="normal-icon"
                            title="navigate to" href={`#code-${value}`}  >
                            <i className="icon icon-xs icon-inactive fas fa-location-arrow"></i>
                        </a>}
                    </div>,
                    disabled: value === this.dname,
                }
            })
        });
        ~optionTypes.indexOf(CategoryFromType.files) && (categoriesOptions[optionTypes.indexOf(CategoryFromType.files)] =
        {
            value: CategoryFromType.files,
            label: <FilesComp></FilesComp>,
            children: getCatogeries(this.editor, CategoryFromType.files).map(/**@param {DefaultFileData} value */(value) => {
                return {
                    value: value.fileKey,
                    label: value.name,
                }
            })
        });
        ~optionTypes.indexOf(CategoryFromType.database) && (categoriesOptions[optionTypes.indexOf(CategoryFromType.database)] =
        {
            value: CategoryFromType.database,
            label: <DatabasesComp></DatabasesComp>,
            children: getCatogeries(this.editor, CategoryFromType.database).map(/**@param {DefaultDatabase} value */(value) => {
                return {
                    value: value.name,
                    label: value.name,
                }
            })
        });
        ~optionTypes.indexOf(CategoryFromType.neo4j) && (categoriesOptions[optionTypes.indexOf(CategoryFromType.neo4j)] =
        {
            value: CategoryFromType.neo4j,
            label: <NeodashComp></NeodashComp>,
            children: getCatogeries(this.editor, CategoryFromType.neo4j).map(/**@param {DefaultDatabase} value */(value) => {
                return {
                    value: value.name,
                    label: value.name,
                }
            })
        });
        ~optionTypes.indexOf(CategoryFromType.mongodb) && (categoriesOptions[optionTypes.indexOf(CategoryFromType.mongodb)] =
        {
            value: CategoryFromType.mongodb,
            label: CategoryFromType.mongodb,
            children: getCatogeries(this.editor, CategoryFromType.mongodb).map(/**@param {DefaultDatabase} value */(value) => {
                return {
                    value: value.name,
                    label: value.name,
                }
            })
        });
        ~optionTypes.indexOf(CategoryFromType.dynamodb) && (categoriesOptions[optionTypes.indexOf(CategoryFromType.dynamodb)] =
        {
            value: CategoryFromType.dynamodb,
            label: CategoryFromType.dynamodb,
            children: getCatogeries(this.editor, CategoryFromType.dynamodb).map(/**@param {DefaultDatabase} value */(value) => {
                return {
                    value: value.name,
                    label: value.name,
                }
            })
        });
        ~optionTypes.indexOf(CategoryFromType.client) && (categoriesOptions[optionTypes.indexOf(CategoryFromType.client)] =
        {
            value: CategoryFromType.client,
            label: CategoryFromType.client,
            children: getCatogeries(this.editor, CategoryFromType.client).map(/**@param {DefaultDatabase} value */(value) => {
                return {
                    value: value.dialect,
                    label: value.dialect,
                }
            })
        });
        this.state = {
            optionTypes,
            className: className,
            categoriesOptions,
            selectedCategoryFrom, selectedCategory, onCategoryChanged
        }
        this.refreshGraphElement = document.createElement("div");
        this.refreshFileAttachmentsElement = document.createElement("div");
        // this.fileChangeElement = document.createElement("div");
        // this.linkChangeElement = document.createElement("div");
        this.shareDataElement = document.createElement("div");
        this.databaseElement = document.createElement("div");
        this.clientElement = document.createElement("div");
    }

    componentDidUpdate(preProps, preState) {
        const { className, selectedCategoryFrom, selectedCategory, onCategoryChanged, } = this.props;
        let state = {};
        if (preProps.className !== className) {
            _.assign(state, { className });
        }
        if (preProps.selectedCategoryFrom !== selectedCategoryFrom) {
            _.assign(state, { selectedCategoryFrom });
        }
        if (preProps.selectedCategory !== selectedCategory) {
            _.assign(state, { selectedCategory });
        }
        if (preProps.onCategoryChanged !== onCategoryChanged) {
            _.assign(state, { onCategoryChanged });
        }
        !_.isEmpty(state) && this.setState(state);
    }

    componentWillUnmount() {
        actions.deleteCache(this.refreshGraphElement);
        actions.deleteCache(this.refreshFileAttachmentsElement);
        // actions.deleteCache(this.fileChangeElement);
        // actions.deleteCache(this.linkChangeElement);
        actions.deleteCache(this.shareDataElement);
        actions.deleteCache(this.databaseElement);
        actions.deleteCache(this.clientElement);
    }

    refresh(type) {
        const { selectedCategoryFrom, selectedCategory, onCategoryChanged } = this.state;
        if (selectedCategoryFrom && selectedCategory && selectedCategoryFrom === type) {
            onCategoryChanged({ selectedCategoryFrom, selectedCategory, refresh: true });
        }
    }

    componentDidMount() {
        const { optionTypes } = this.props;
        ~optionTypes.indexOf(CategoryFromType.main) && actions.inspector(this.refreshGraphElement, [actions.types.REFRESH_GRAPH], async (graphxrApi) => {
            const { categoriesOptions } = this.state;
            categoriesOptions[optionTypes.indexOf(CategoryFromType.main)] = {
                value: CategoryFromType.main,
                label: CategoryFromType.main,
                children: getCatogeries(this.editor, CategoryFromType.main).map((value) => {
                    return {
                        value: value,
                        label: value,
                    }
                })
            };
            this.refresh(CategoryFromType.main);
            this.setState({ categoriesOptions })
        })
        let func = () => {
            const { categoriesOptions } = this.state;
            categoriesOptions[optionTypes.indexOf(CategoryFromType.files)] = {
                value: CategoryFromType.files,
                label: <FilesComp></FilesComp>,
                children: getCatogeries(this.editor, CategoryFromType.files).map(/**@param {DefaultFileData} value */(value) => {
                    return {
                        value: value.fileKey,
                        label: value.name,
                    }
                })
            };
            this.setState({ categoriesOptions })
        }
        if (~optionTypes.indexOf(CategoryFromType.files)) {
            actions.inspector(this.refreshFileAttachmentsElement, [actions.types.REFRESH_FILE_ATTACHMENTS], func)
            // actions.inspector(this.fileChangeElement, [actions.types.FILE_CHANGE], func)
            // actions.inspector(this.linkChangeElement, [actions.types.LINK_CHANGE], func)
        }
        ~optionTypes.indexOf(CategoryFromType.shareData) && actions.inspector(this.shareDataElement, [actions.types.SHARE_DATA], ({ ShareData, key }) => {
            const { categoriesOptions, selectedCategory, selectedCategoryFrom } = this.state;
            categoriesOptions[optionTypes.indexOf(CategoryFromType.shareData)] = {
                value: CategoryFromType.shareData,
                label: CategoryFromType.shareData,
                children: getCatogeries(this.editor, CategoryFromType.shareData).map((value) => {
                    return {
                        value: value,
                        label: <div>
                            {value}
                            {uuidValidateV4(value) && <a onClick={(e) => {
                                let editorKey = this.editor.getTransferKey();
                                let className = `editorjs-anchor-code-${value}`;
                                let data = document.querySelector(`.editorjs-wrapper.${editorKey}>.editorjs .${className}`);
                                data && data.closest(".code-edit") && data.closest(".code-edit").editCode(true);
                                e.preventDefault(); e.stopPropagation();
                            }} className="normal-icon"
                                title="navigate to" href={`#code-${value}`}  >
                                <i className="icon icon-xs icon-inactive fas fa-location-arrow"></i>
                            </a>}
                        </div>,
                        disabled: value === this.dname,
                    }
                })
            };
            if (selectedCategoryFrom === CategoryFromType.shareData && key === selectedCategory) {
                this.refresh(CategoryFromType.shareData);
            }
            this.setState({ categoriesOptions })
        });
        (~optionTypes.indexOf(CategoryFromType.database) || ~optionTypes.indexOf(CategoryFromType.neo4j) || ~optionTypes.indexOf(CategoryFromType.mongodb) || ~optionTypes.indexOf(CategoryFromType.dynamodb))
            && actions.inspector(this.databaseElement, [actions.types.DATABASES], (databases) => {
                const { categoriesOptions, selectedCategory, selectedCategoryFrom } = this.state;
                ~optionTypes.indexOf(CategoryFromType.database) && (categoriesOptions[optionTypes.indexOf(CategoryFromType.database)] = {
                    value: CategoryFromType.database,
                    label: <DatabasesComp></DatabasesComp>,
                    children: getCatogeries(this.editor, CategoryFromType.database).map(/** @param {DefaultDatabase} value */(value) => {
                        return {
                            value: value.name,
                            label: value.name,
                        }
                    })
                });
                ~optionTypes.indexOf(CategoryFromType.neo4j) && (categoriesOptions[optionTypes.indexOf(CategoryFromType.neo4j)] = {
                    value: CategoryFromType.neo4j,
                    label: <NeodashComp></NeodashComp>,
                    children: getCatogeries(this.editor, CategoryFromType.neo4j).map(/** @param {DefaultDatabase} value */(value) => {
                        return {
                            value: value.name,
                            label: value.name,
                        }
                    })
                });
                ~optionTypes.indexOf(CategoryFromType.mongodb) && (categoriesOptions[optionTypes.indexOf(CategoryFromType.mongodb)] = {
                    value: CategoryFromType.mongodb,
                    label: CategoryFromType.mongodb,
                    children: getCatogeries(this.editor, CategoryFromType.mongodb).map(/** @param {DefaultDatabase} value */(value) => {
                        return {
                            value: value.name,
                            label: value.name,
                        }
                    })
                });
                ~optionTypes.indexOf(CategoryFromType.dynamodb) && (categoriesOptions[optionTypes.indexOf(CategoryFromType.dynamodb)] = {
                    value: CategoryFromType.dynamodb,
                    label: CategoryFromType.dynamodb,
                    children: getCatogeries(this.editor, CategoryFromType.dynamodb).map(/** @param {DefaultDatabase} value */(value) => {
                        return {
                            value: value.name,
                            label: value.name,
                        }
                    })
                });
                if (~[CategoryFromType.database, CategoryFromType.neo4j, CategoryFromType.mongodb, CategoryFromType.dynamodb].indexOf(selectedCategoryFrom)) {
                    this.refresh(selectedCategoryFrom);
                }
                this.setState({ categoriesOptions })
            })
        ~optionTypes.indexOf(CategoryFromType.client) && actions.inspector(this.clientElement, [actions.types.DB_CLIENTS], (dbClients) => {
            const { categoriesOptions, selectedCategory, selectedCategoryFrom } = this.state;
            ~optionTypes.indexOf(CategoryFromType.client) && (categoriesOptions[optionTypes.indexOf(CategoryFromType.client)] = {
                value: CategoryFromType.client,
                label: CategoryFromType.client,
                children: getCatogeries(this.editor, CategoryFromType.client).map(/** @param {DefaultDatabase} value */(value) => {
                    return {
                        value: value.key,
                        label: `${value.key}(${value.dialect})`,
                    }
                })
            });
            if (~[CategoryFromType.client].indexOf(selectedCategoryFrom)) {
                this.refresh(selectedCategoryFrom);
            }
            this.setState({ categoriesOptions })
        })
    }

    render() {
        const { getPopupContainer, displayRender, style } = this.props;
        const { className, categoriesOptions, selectedCategoryFrom, selectedCategory, onCategoryChanged } = this.state;
        let hasCategory = false;
        _.each(categoriesOptions, (option) => {
            if (option.value === selectedCategoryFrom) {
                _.each(option.children, (category) => {
                    if (category.value === selectedCategory) {
                        hasCategory = true;
                        return false;
                    }
                })
                return false;
            }
        })
        return <span>
            <Cascader
                getPopupContainer={getPopupContainer || (() => { return document.body })}
                style={style || ({ width: 200 })}
                options={categoriesOptions}
                value={selectedCategoryFrom && selectedCategory ? [selectedCategoryFrom, selectedCategory] : undefined}
                expandTrigger="hover"
                displayRender={displayRender || ((label) => {
                    return label[label.length - 1];
                })}
                placeholder="Please Select Datas"
                onChange={(value) => {
                    if (value.length === 2) {
                        onCategoryChanged({
                            selectedCategoryFrom: value[0],
                            selectedCategory: value[1],
                            refresh: false
                        })
                    } else if (value.length === 0) {
                        onCategoryChanged({
                            selectedCategoryFrom: "",
                            selectedCategory: "",
                            refresh: false
                        })
                    }
                }}
            />
            {selectedCategoryFrom && selectedCategory && !hasCategory &&
                <div className="text-danger">Can't Find Datasource! "{selectedCategory}"</div>}
        </span>;
    }
}