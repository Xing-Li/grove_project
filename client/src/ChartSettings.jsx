import React from 'react';
import AntChartSetting from './block/antchart/AntChartSetting';
import { DefaultAntChartConfig } from './block/antchart/comp/OptionsTypes';

export default class ChartSettings extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            /**@type {import('block/antchart/AntChartEditor').default} */
            chartEditor: undefined,
        }
        this.chartEditorElement = document.createElement("div");
    }

    componentDidMount() {
        actions.inspector(this.chartEditorElement, [actions.types.CHART_EDITOR],
            (nChartEditor) => {
                const { chartEditor } = this.state;
                if (chartEditor && nChartEditor !== chartEditor && chartEditor.state.selectedKey && !chartEditor.unmount) {
                    chartEditor.cancelEdit();
                    // chartEditor.setState({ selectedKey: undefined });
                }
                this.setState({ chartEditor: nChartEditor });
            })
    }

    componentWillUnmount() {
        actions.deleteCache(this.chartEditorElement);
    }

    render() {
        const { chartEditor } = this.state;
        let selectedChartConfig;
        if (!chartEditor) {
            selectedChartConfig = DefaultAntChartConfig;
        } else {
            const { chartConfigs, selectedKey } = chartEditor.state;
            selectedChartConfig = chartConfigs[selectedKey] || DefaultAntChartConfig
        }
        return <AntChartSetting ref={chartEditor && chartEditor.settingRef}
            chartEditor={chartEditor} chartConfig={selectedChartConfig}>
        </AntChartSetting>
    }
}