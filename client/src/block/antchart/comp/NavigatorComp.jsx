import React from "react";
import { Timeline } from 'vis-timeline/standalone';

/**
 * 
 * @param {Timeline} timeline 
 */
export const addMarkers = function (timeline) {
    timeline.startMarkerId = timeline.addCustomTime(timeline.getWindow().start, timeline.startMarkerId);
    timeline.setCustomTimeMarker('start', timeline.startMarkerId, false);
    timeline.endMarkerId = timeline.addCustomTime(timeline.getWindow().end, timeline.endMarkerId);
    timeline.setCustomTimeMarker('end', timeline.endMarkerId, false);
}
/**
 * 
 * @param {Timeline} timeline 
 */
export const removeMarkers = function (timeline) {
    timeline.removeCustomTime(timeline.startMarkerId);
    timeline.removeCustomTime(timeline.endMarkerId);
    timeline.dragMarkers = false;
}

const INTERVAL = false;

/**
 * 
 * @param {Timeline} timeline 
 */
export const destroyInterval = function (timeline) {
    if (INTERVAL) {
        timeline.timerID && clearInterval(timeline.timerID);
    } else {
        timeline.timerID && clearTimeout(timeline.timerID);
    }
}
/**
* 
* @param {Timeline} timeline 
* @param {function} func 
* @param {options} options 
*/
const createInterval = function (timeline, func, options) {
    if (INTERVAL) {
        destroyInterval(timeline);
        func();
        timeline.timerID = setInterval(func, options.duration * 2);
    } else {
        let tick = function () {
            destroyInterval(timeline);
            func();
            timeline.timerID = setTimeout(tick, options.duration * 2);
        }
        tick();
    }
}

export default class NavigateComp extends React.Component {
    constructor(props) {
        super(props);
        /**@type {Timeline} */
        this.timeline = props.timeline;
        const { zoomKey, showAllGroups } = this.props;
        this.state = {
            speedVisible: false,
            options: { duration: 500, easingFunction: 'linear' },
            zoomKey,
            showAllGroups,
            hideGroup: false,
            closeMarker: this.timeline.closeMarker
        }
    }

    componentDidMount() {
        this.timeline.hideGroupCb = (groupId) => {
            !this.state.hideGroup && this.setState({ hideGroup: !this.state.hideGroup })
        }
    }

    componentWillUnmount() {
        this.timeline.hideGroupCb = undefined;
    }

    move = (percentage, options) => {
        let range = this.timeline.getWindow();
        let interval = range.end - range.start;

        this.timeline.setWindow(range.start.valueOf() - interval * percentage,
            range.end.valueOf() - interval * percentage, options
        );
    }

    zoomIn = (e) => {
        const { options } = this.state;
        createInterval(this.timeline, () => {
            this.timeline.zoomIn(0.2, options);
        }, options)
    }

    zoomOut = (e) => {
        const { options } = this.state;
        createInterval(this.timeline, () => {
            this.timeline.zoomOut(0.2, options);
        }, options)
    }

    moveLeft = (e) => {
        const { options } = this.state;
        createInterval(this.timeline, () => {
            this.move(0.2, options);
        }, options)
    }

    moveRight = (e) => {
        const { options } = this.state;
        createInterval(this.timeline, () => {
            this.move(-0.2, options);
        }, options)
    }

    stopTimer = () => {
        destroyInterval(this.timeline);
    }

    render() {
        const { speedVisible, options, zoomKey, showAllGroups, closeMarker, hideGroup } = this.state;
        return <div >
            <div className="tl-menu normal-icon">
                <span onClick={(e) => {
                    this.timeline.closeMarker = !this.timeline.closeMarker;
                    if (this.timeline.closeMarker) {
                        removeMarkers(this.timeline)
                    } else {
                        addMarkers(this.timeline);
                    }
                    this.setState({ closeMarker: this.timeline.closeMarker });
                }} className={`icon-bordered icon fas fa-bookmark ${closeMarker ? "icon-muted" : ""}`} title="Marker"></span>
                <span onClick={(e) => {
                    showAllGroups();
                    this.setState({ hideGroup: false })
                }} className={`icon-bordered icon fas fa-eye ${!hideGroup ? "icon-muted" : ""} ${showAllGroups ? "" : "hide"}`} title="Restore Hidden Groups"></span>
            </div>
            <div className="tl-navs">
                <div className="grid-container normal-icon">
                    <div className="grid-item moveLeft" onMouseDown={this.moveLeft} onTouchStart={this.moveLeft}
                        onMouseUp={this.stopTimer} onTouchEnd={this.stopTimer} title="Move Left(Drag To Right)">
                        <i draggable={false} className="icon fas fa-caret-left">  </i>
                    </div>
                    <div className="grid-item reset" title="Fit all items" onClick={() => {
                        destroyInterval(this.timeline);
                        this.timeline.fit();
                    }}>
                        <i draggable={false} className="icon far fa-circle"></i>
                    </div>
                    <div className="grid-item moveRight" onMouseDown={this.moveRight} onTouchStart={this.moveRight}
                        onMouseUp={this.stopTimer} onTouchEnd={this.stopTimer} title={`Move Right(Drag To Left)`}>
                        <i draggable={false} className="icon fas fa-caret-right"></i>
                    </div>
                    <div className="grid-item zoomIn" onMouseDown={this.zoomIn} onTouchStart={this.zoomIn}
                        onMouseUp={this.stopTimer} onTouchEnd={this.stopTimer}
                        title={`Zoom In(${zoomKey ? `${zoomKey} +` : ""} MouseScroll)`}>
                        <i draggable={false} className="icon fas fa-search-plus"></i>
                    </div>
                    <div className="grid-item speed" onClick={() => {
                        this.setState({ speedVisible: !speedVisible });
                    }}>
                        <i draggable={false} title="set speed" className={`icon fas fa-tachometer-alt ${speedVisible ? "" : "icon-muted"}`}></i>
                    </div>
                    <div className="grid-item zoomOut" onMouseDown={this.zoomOut} onTouchStart={this.zoomOut}
                        onMouseUp={this.stopTimer} onTouchEnd={this.stopTimer}
                        title={`Zoom Out(${zoomKey ? `${zoomKey} +` : ""} MouseScroll)`} >
                        <i draggable={false} className="icon fas fa-search-minus"></i>
                    </div >
                    <div className={`grid-item range ${speedVisible ? "" : "hide"}`}>
                        <input onChange={(e) => {
                            options.duration = parseInt(e.target.value) || 500;
                            this.setState({ options });
                        }} title={`Speed ${options.duration}`} type="range" min="100" max="2000" step="10" value={options.duration} />
                    </div>
                </div>
            </div>
        </div>
    }
} 