import { Button, Cascader, Col, Form, InputNumber, Row, Select, Slider, Space, Switch, TimePicker, Tooltip } from 'antd';
import actions from 'define/Actions';
import { DrawerType } from 'editorSettings';
import _ from 'lodash';
import moment from 'moment';
import PropTypes from "prop-types";
import React from "react";
import databaseClient, { DatabaseClient, workbookToDb } from 'stdlib/databaseClient';
import FileAttachments from 'stdlib/fileAttachment';
import { resolveBase } from 'stdlib/grove-require';
import { __query } from "stdlib/table.js";
import {
    assembleErrMessage,
    CategoryFromType,
    COMMON_RENDER_TYPES, NEO_FOOTER_RENDERS,
    copyContent, DatabaseType, extractDynamoDatas, getColumnName,
    getOperations, isFailedResults,
    ReDatabaseTypes,
    ReDbDialets,
    RenderTypesOfCategory,
    RenderTypesOfDialect,
    SelectProps, ShortcutTitle, showToast, uuidValidateV4, window_shortcuts_keys
} from "../../../util/utils";
import { RunCodeIcon } from '../../code/Icon';
import AntChartSetting from "../AntChartSetting";
import NeoCardViewFooter from '../neocomp/CardViewFooter';
import { getFieldsSelection, getReportTypes } from '../util/ExtensionUtils';
import { extractNodePropertiesFromRecords, extractRelationshipPropertiesFromRecords } from '../util/ReportRecordProcessing';
import { ChartTypeTemplate, getRenderTypeOptions } from './OptionsTypes';
import CypherEditor from './CypherEditor';
import { DatabaseComp, ParameterSelectComp, MongoComp, DynamoComp } from './DatabaseComp';
import ChooseDatas, { FilesComp, DatabasesComp, NeodashComp } from '../../code/ChooseDatas';
import TableComp, { ClientStatus, extractColumnDatas, RawJSONComp, TurnPageRawJSONComp, TagComp } from './TableComp';
import { assignFiltersSorts } from 'block/code/CodeTools';
import { DuckDBClient } from 'stdlib/duckdb';
import { fileSeparator } from 'file/fileUtils';
const { DefaultDatabase, RenderType, RenderTypeDesc, SelectionTypes, DEFAULT_BREAK_POINTS, isChartRenderType,
    mongo_Template, dynamo_Template, neo_Template, reD_Template } = require("util/databaseApi");
const { getViewport, isFileDb, isFileExcel, calcAutoType, isFileArrow } = require("util/helpers");
const { ShapeEditor } = require("util/hqApi");

const { Option } = Select;

export default class UploadData extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        settings: PropTypes.object
    }

    static defaultProps = {};
    constructor(props) {
        super(props);
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        const { chartConfig: { reD, neo, mongo, dynamo } } = settings.state;
        const { dbTable, query, selectedTable, filters, selectedColumns, sorts, slice } = _.assign(_.cloneDeep(reD_Template), reD);
        const neoTmp = _.assign(_.cloneDeep(neo_Template), neo);
        const { selectionType, propertyName, propertyNum, propertyValue, nodeLabel } = neoTmp;
        const mongoTmp = _.assign(_.cloneDeep(mongo_Template), mongo);
        const dynamoTmp = _.assign(_.cloneDeep(dynamo_Template), dynamo);
        const { collectionName } = mongoTmp;
        const { TableName } = dynamoTmp;
        this.state = {
            className: this.props.className,
            needRun: false,
            datasets: [],
            tables: [],
            columns: [],
            db: undefined,
            /////reD cache
            dbTable,
            query,
            tableName: "",
            selectedTable,
            filters,
            selectedColumns,
            sorts,
            slice,
            /** @type {DatabaseClient} */
            client: undefined,
            result: null,
            status: ClientStatus.stop,
            error: "",
            /////neo cache
            fields: undefined,
            relationProperties: undefined,
            /**neo parameter select */
            value: neoTmp.query,
            selectionType,
            nodeLabel,
            propertyName,
            propertyNum,
            propertyValue,
            nodeLabels: nodeLabel ? [nodeLabel] : [],
            relTypes: [],
            propertyNames: propertyName ? [propertyName] : [],
            propertyValues: propertyValue ? [propertyValue] : [],
            /////mongo cache
            collectionNames: collectionName ? [collectionName] : [],
            collectionFields: [],
            mongo: mongoTmp,
            /////dynamo cache
            TableNames: TableName ? [TableName] : [],
            TableFields: [],
            dynamo: dynamoTmp,
        }
        /**@type {ShapeEditor} */
        this.editor = window.currentEditor
        this.refreshGraphElement = document.createElement("div");
        this.refreshFileAttachmentsElement = document.createElement("div");
        this.fileChangeElement = document.createElement("div");
        this.linkChangeElement = document.createElement("div");
        this.shareDataElement = document.createElement("div");
        this.databaseElement = document.createElement("div");
        this.declareElement = document.createElement("div");
        this.dataElement = document.createElement("div");
    }

    componentDidUpdate(preProps, preState) {
        const { className } = this.props;
        let state = {};
        if (preProps.className !== className) {
            _.assign(state, { className });
        }
        !_.isEmpty(state) && this.setState(state);
    }

    componentWillUnmount() {
        actions.deleteCache(this.refreshGraphElement);
        actions.deleteCache(this.refreshFileAttachmentsElement);
        actions.deleteCache(this.fileChangeElement);
        actions.deleteCache(this.linkChangeElement);
        actions.deleteCache(this.shareDataElement);
        actions.deleteCache(this.databaseElement);
        this.clearCache();
        this.unmount = true;
    }

    clearCache() {
        if (!this.editor) {
            return;
        }
        this.editor.deleteCacheV(this.declareElement);
        this.editor.deleteCacheV(this.dataElement);
        this.declareElement.innerHTML = ""
        this.dataElement.innerHTML = ""
    }

    async componentDidMount() {
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        const { chartConfig: { selectedCategory, selectedCategoryFrom, selectedClientType, renderType, mongo, dynamo } } = settings.state;
        if (selectedCategory && !~COMMON_RENDER_TYPES.indexOf(renderType)) {
            if (selectedCategoryFrom === CategoryFromType.database ||
                selectedCategoryFrom === CategoryFromType.client && ~ReDbDialets.indexOf(selectedClientType) ||
                this.isDbFile(selectedCategoryFrom, selectedCategory) ||
                this.isExcelFile(selectedCategoryFrom, selectedCategory) ||
                this.isArrowFile(selectedCategoryFrom, selectedCategory)) {
                this.getTables(selectedCategoryFrom, selectedCategory, async () => {
                    const { selectedTable } = this.state
                    await this.handleSelectTableChange(selectedTable, true);
                });
            } else if (selectedCategoryFrom === CategoryFromType.neo4j || selectedCategoryFrom === CategoryFromType.client && selectedClientType === DatabaseType.Neo4j) {
                if (renderType === RenderType.ParameterSelect) {
                    const { selectionType } = this.state;
                    this.querySelectionTypes(selectedCategoryFrom, selectedCategory, selectionType, true)
                } else {
                    this.initClient(selectedCategoryFrom, selectedCategory, () => {
                        this.renderChart(() => {
                            this.inspector();
                        })
                    });
                }
            } else if (selectedCategoryFrom === CategoryFromType.mongodb || selectedCategoryFrom === CategoryFromType.client && selectedClientType === DatabaseType.MongoDB) {
                this.renderChart(async () => {
                    await this.inspector();
                    await this.getCollectionNames(selectedCategoryFrom, selectedCategory, () => { });
                    if (mongo.collectionName) {
                        await this.getCollectionFields(selectedCategoryFrom, selectedCategory, mongo.collectionName);
                    }
                })
            } else if (selectedCategoryFrom === CategoryFromType.dynamodb || selectedCategoryFrom === CategoryFromType.client && selectedClientType === DatabaseType.DynamoDB) {
                this.renderChart(async () => {
                    await this.inspector();
                    await this.getTableNames(selectedCategoryFrom, selectedCategory, () => { });
                    if (dynamo.TableName) {
                        await this.getTableFields(selectedCategoryFrom, selectedCategory, dynamo.TableName);
                    }
                })
            }
            // if (renderType === RenderType.Chart && chartData.length > 0) {
            //     settings.setColumnsData(chartData, false);
            // }
        }
    }

    setEditorLoading(editorLoading) {
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        settings.setEditorLoading(editorLoading)
    }

    async runCode(commit = false, reset = false) {
        const { db, query, dbTable, selectedTable, filters, selectedColumns, sorts, slice, client } = this.state;
        const sqlType = client.dialect;
        this.setEditorLoading(true);
        let result;
        let error = "";
        if (dbTable) {
            let operations = getOperations(db, sqlType, selectedTable, filters, selectedColumns, sorts, slice);
            if (!operations) {
                this.setEditorLoading(false);
                return;
            }
            result = await __query(client, operations, new Promise(
                (value) => {
                    // console.log(value);
                    return value;
                }, (reason) => {
                    showToast(reason);
                    error = reason;
                }
            ))
        } else {
            result = await client.query(query);
        }
        if (this.unmount) {
            return;
        }
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        let status;
        if (result && !isFailedResults(result)) {
            settings.setColumnsData(result, reset);
            settings.modifyChartConfig({ reD: { dbTable, query, selectedTable, filters, selectedColumns, sorts, slice } }, () => {
                if (commit) {
                    this.renderChart();
                }
            });
            status = ClientStatus.success;
        } else {
            status = ClientStatus.error;
            result && (error = result[0].Results)
        }
        this.setEditorLoading(false);
        this.setState({ needRun: false, result, status, error })
    }

    /**
     * @returns {AntChartSetting}
     */
    getSettings() {
        /**@type {AntChartSetting} */
        return this.props.settings;
    }

    /**
     * 
     * @param {function} [cb] 
     */
    renderChart = (cb) => {
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        const { chartEditor } = settings.state;
        chartEditor && chartEditor.run(() => {
            cb && cb();
            this.setState({ needRun: false })
        })
    }

    /**
     * 
     * @param {function} [cb] 
     */
    changeGrid = (cb) => {
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        const { chartEditor } = settings.state;
        if (chartEditor) {
            const { chartConfig: { chartKey, renderType } } = settings.state;
            const { layouts } = chartEditor.state
            _.each(_.keys(DEFAULT_BREAK_POINTS), (viewport) => {
                if (!layouts[viewport]) {
                    layouts[viewport] = [];
                }
                let layout = chartEditor.add(layouts, viewport, renderType, chartKey);
                layouts[viewport].push(layout);
            })
            chartEditor.onLayoutChange(undefined, _.cloneDeep(layouts));
        }
    }

    async inspector() {
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        const { chartConfig: { chartKey, selectedCategoryFrom, selectedClientType }, chartEditor } = settings.state;
        /**@type {ShapeEditor} */
        this.editor = chartEditor && chartEditor.props.editor
        if (!this.editor) {
            return;
        }
        let name = `_chart_${chartKey.replaceNoWordToUnderline()}`;
        this.setEditorLoading(true);
        this.clearCache();
        await this.editor.runFunc(`${name}`, this.declareElement, this.dataElement, (result, name) => {
            this.getResultCb(result);
        }, (error, name) => {
            this.getResultCb(selectedCategoryFrom === CategoryFromType.neo4j || selectedCategoryFrom === CategoryFromType.client && selectedClientType === DatabaseType.Neo4j ? { error } : assembleErrMessage(error));
        }, true, `_inspector_${chartKey.replaceNoWordToUnderline()}`);
    }

    getResultCb = (result) => {
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        const { chartConfig: { selectedCategoryFrom, selectedClientType } } = settings.state;
        if (selectedCategoryFrom === CategoryFromType.mongodb || selectedCategoryFrom === CategoryFromType.client && selectedClientType === DatabaseType.MongoDB) {
            this.getMongoResultCb(result);
        } else if (selectedCategoryFrom === CategoryFromType.dynamodb || selectedCategoryFrom === CategoryFromType.client && selectedClientType === DatabaseType.DynamoDB) {
            this.getDynamoResultCb(result);
        } else if (selectedCategoryFrom === CategoryFromType.neo4j || selectedCategoryFrom === CategoryFromType.client && selectedClientType === DatabaseType.Neo4j) {
            this.getNeoResultCb(result);
        }
    }

    getMongoResultCb = (result) => {
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        const { chartConfig: { renderType, mongo }, chartConfig } = settings.state;
        let status;
        let error = "";
        if (result && !isFailedResults(result)) {
            let state = {};
            settings.setColumnsData(result, false, state);
            const { selectedProperties, emptyFills, castColumns, joinColumns } = state.chartConfig || chartConfig;
            settings.onRowSelectionChanged(selectedProperties, state, emptyFills, castColumns, joinColumns)
            settings.setState(state);
            status = ClientStatus.success;
        } else {
            status = ClientStatus.error;
            result && (error = result[0].Results)
        }
        this.setEditorLoading(false);
        this.setState({ needRun: false, result, status, error })
    }

    getDynamoResultCb = (result) => {
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        const { chartConfig: { renderType, mongo }, chartConfig } = settings.state;
        let status;
        let error = "";
        if (result && !isFailedResults(result)) {
            result = extractDynamoDatas(result);
            let state = {};
            settings.setColumnsData(result, false, state);
            const { selectedProperties, emptyFills, castColumns, joinColumns } = state.chartConfig || chartConfig;
            settings.onRowSelectionChanged(selectedProperties, state, emptyFills, castColumns, joinColumns)
            settings.setState(state);
            status = ClientStatus.success;
        } else {
            status = ClientStatus.error;
            result && (error = result[0].Results)
        }
        this.setEditorLoading(false);
        this.setState({ needRun: false, result, status, error })
    }

    getNeoResultCb = (result) => {
        const { value } = this.state;
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        const { chartConfig: { renderType, neo }, chartConfig } = settings.state;
        let status;
        let error = "";
        let fields;
        let relationProperties;
        let oldSelection = _.cloneDeep(neo && neo.selection);
        let selection;
        let labelProperty;
        if (result && !result.error && result.records) {
            const { records, summary } = result;
            if (renderType === RenderType.Chart) {
                const { columns, rows } = extractColumnDatas(false, records);
                let state = {};
                settings.setColumnsData(rows, false, state);
                const { selectedProperties, emptyFills, castColumns, joinColumns } = state.chartConfig || chartConfig;
                settings.onRowSelectionChanged(selectedProperties, state, emptyFills, castColumns, joinColumns)
                settings.setState(state);
            }
            const { useReturnValuesAsFields, useNodePropsAsFields } = RenderTypeDesc[renderType]
            if (useReturnValuesAsFields) {
                // Send a deep copy of the returned record keys as the set of fields.
                fields = (records && records[0] && records[0].keys) ? records[0].keys.slice() : [];
            } else if (useNodePropsAsFields) {
                // If we don't use dynamic field mapping, but we do have a selection, use the discovered node properties as fields.
                fields = extractNodePropertiesFromRecords(records);
            }
            const reportTypes = getReportTypes();
            if (reportTypes[renderType]) {
                oldSelection = oldSelection ? oldSelection : _.reduce(reportTypes[renderType].selection, (prev, curr) => {
                    prev[curr[0]] = undefined;
                    return prev;
                }, {});
                selection = fields ? getFieldsSelection(renderType, fields, oldSelection, undefined, records) : {};
                let properties = extractRelationshipPropertiesFromRecords(records);
                labelProperty = properties.length ? properties[0] : "";
                relationProperties = properties;
            } else {
                selection = undefined;
                labelProperty = undefined;
                relationProperties = undefined;
            }
            status = ClientStatus.success;
        } else {
            fields = undefined;
            selection = undefined;
            labelProperty = undefined;
            relationProperties = undefined;
            status = ClientStatus.error;
            result && (error = result.error)
        }
        let needRefresh = false;
        if (!_.isEqual({ neo: { query: neo && neo.query, selection: neo && neo.selection, labelProperty: neo && neo.labelProperty } }, { neo: { query: value, selection, labelProperty } })) {
            needRefresh = true;
        }
        settings.modifyChartConfig({ neo: _.assign(neo && _.cloneDeep(neo) || {}, { query: value, selection, labelProperty }) }, () => {
            needRefresh && this.renderChart();
        });
        this.setEditorLoading(false);
        this.setState({ needRun: false, result, status, fields, relationProperties, error })
    }

    /**
     * 
     * @param {string} selectedTable 
     * @param {boolean} init flag is call when init Component 
     */
    handleSelectTableChange = async (selectedTable, init = false) => {
        const [schema, table] = selectedTable && ~selectedTable.indexOf(".") ? selectedTable.split(".") : [null, selectedTable];
        const { db, dbTable, client, datasets } = this.state;
        if (client.dialect === DatabaseType.BigQuery && schema) {
            let arr = _.filter(datasets, (dataset) => { return dataset.id === schema });
            client.location = arr.length ? arr[0].location : undefined;
        }
        if (dbTable) {
            if (!selectedTable) {
                this.setState({
                    needRun: true, selectedTable: undefined, columns: [], nData: [],
                    filters: [{}], selectedColumns: [], sorts: [{}], slice: { to: 100, from: 0 }
                }, () => {
                });
                return;
            }
            this.setEditorLoading(true);
            let columns = await client.describeColumns({ schema, table })
            if (this.unmount) {
                return;
            }
            let columnNames = _.reduce(columns, (prev, column, index) => { prev.push(getColumnName(column)); return prev }, [])
            let state = {
                needRun: true, columns
            };
            !init && _.assign(state, {
                needRun: true, selectedTable, columns, nData: [],
                filters: [{}], selectedColumns: columnNames, sorts: [{}], slice: { to: 100, from: 0 }
            })
            this.setState(state, async () => {
                await this.runCode(true, !init);
            });
        } else {
            if (!selectedTable) {
                this.setState({
                    selectedTable: undefined, columns: []
                }, () => {
                });
                return;
            }
            let columns = await client.describeColumns({ schema, table })
            if (this.unmount) {
                return;
            }
            this.setState({ columns });
            !init && copyContent(selectedTable);
        }
    }

    Neo4jComp = () => {
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        const { chartConfig: { selectedCategoryFrom, selectedCategory, active, renderType, fetchPeriod, neo } } = settings.state;
        const { needRun, result, client, error } = this.state;
        let cypherRun = () => {
            !active ? settings.modifyChartConfig({
                chartData: [],
                chartColumns: [],
            }, () => {
                this.renderChart(() => {
                    this.inspector();
                })
            }) : this.renderChart(() => {
                this.inspector();
            })
        };
        return <React.Fragment>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                <Col className="gutter-row" sm={24} md={12}>
                    <CypherEditor className="limit-height" value={neo && neo.query || ""} onValueChange={(value) => {
                        this.setState({ value: value, needRun: true });
                        let state = {
                        }
                        settings.modifyChartConfig(_.assign(state, { neo: _.assign(neo && _.cloneDeep(neo) || {}, { query: value }) }), () => {
                        });
                    }} run={cypherRun}></CypherEditor>
                    {!!RenderTypeDesc[renderType].maxRecords &&
                        <div>Max Records Limit:<InputNumber value={neo && neo.maxRecords || RenderTypeDesc[renderType].maxRecords} min={1} max={100000000} onChange={(value) => {
                            this.setState({ needRun: true });
                            let state = {
                            }
                            settings.modifyChartConfig(_.assign(state, { neo: _.assign(neo && _.cloneDeep(neo) || {}, { maxRecords: value }) }), () => {
                            });
                        }} /></div>}
                </Col>
                <Col className="gutter-row" sm={24} md={12}>
                    <div className="d-flex">
                        <Button icon={<RunCodeIcon fill={(needRun || error) && client} />} onClick={cypherRun}>Run</Button>
                    </div>
                </Col>
            </Row>
        </React.Fragment>
    }

    async getTables(selectedCategoryFrom, selectedCategory, cb = () => { }) {
        if (!selectedCategory) {
            return;
        }
        this.setEditorLoading(true);
        let dbClient;
        if (this.isDbFile(selectedCategoryFrom, selectedCategory)) {
            let constructor = FileAttachments(resolveBase);
            dbClient = await constructor(this.editor.getFileAttachmentUrl(selectedCategory, this.editor.getUploadUri())).sqlite();
        } else if (this.isExcelFile(selectedCategoryFrom, selectedCategory)) {
            let constructor = FileAttachments(resolveBase);
            let wb = await constructor(this.editor.getFileAttachmentUrl(selectedCategory, this.editor.getUploadUri())).xlsx();
            dbClient = await workbookToDb(wb);
        } else if (this.isArrowFile(selectedCategoryFrom, selectedCategory)) {
            let constructor = FileAttachments(resolveBase);
            let ff = await constructor(this.editor.getFileAttachmentUrl(selectedCategory, this.editor.getUploadUri()));
            dbClient = await DuckDBClient.of({
                [selectedCategory.substring(~selectedCategory.lastIndexOf(fileSeparator) ? selectedCategory.lastIndexOf(fileSeparator) + 1 : 0).toCommonName()]: ff
            });
        } else if (selectedCategoryFrom === CategoryFromType.client) {
            dbClient = window.DbClients[this.editor.getSelfModuleName()][selectedCategory];
        } else {
            dbClient = databaseClient(selectedCategory);
        }
        let datasets, tables;
        if (dbClient.dialect === DatabaseType.BigQuery) {
            datasets = await dbClient.getDatasets();
            if (this.unmount) {
                return;
            }
            if (isFailedResults(datasets)) {
                datasets = [];
            }
            if (datasets && datasets.length) {
                let promiseList = [];
                _.each(datasets, (dataset) => {
                    promiseList.push(dbClient.describeTables({ schema: dataset.id, location: dataset.location }));
                })
                let tbs = await Promise.all(promiseList);
                tables = _.concat(...tbs);
            } else {
                tables = assembleErrMessage("Haven't datasets!");
            }
        } else {
            datasets = [];
            tables = await dbClient.describeTables();
        }
        if (this.unmount) {
            return;
        }
        let status = ClientStatus.waiting;
        let error = "";
        if (isFailedResults(tables)) {
            tables = [];
            status = ClientStatus.error;
            error = "Can not get tables"
        }
        this.setState({ db: selectedCategory, datasets, tables, client: dbClient, status, error }, async () => {
            cb && (await cb());
            this.setEditorLoading(false);
        });
    }

    async getCollectionNames(selectedCategoryFrom, selectedCategory, cb = () => { }) {
        if (!selectedCategory) {
            return;
        }
        this.setEditorLoading(true);
        let client = selectedCategoryFrom === CategoryFromType.client ? window.DbClients[this.editor.getSelfModuleName()][selectedCategory] : databaseClient(selectedCategory);
        let collectionNames = await client.describeTables();
        if (this.unmount) {
            return;
        }
        let status = ClientStatus.waiting;
        let error = "";
        if (isFailedResults(collectionNames)) {
            collectionNames = [];
            status = ClientStatus.error;
            error = "Can not get Collections!";
        }
        this.setState({ db: selectedCategory, collectionNames, client, status, error }, async () => {
            cb && (await cb());
            this.setEditorLoading(false);
        });
    }
    async getTableNames(selectedCategoryFrom, selectedCategory, cb = () => { }) {
        if (!selectedCategory) {
            return;
        }
        this.setEditorLoading(true);
        let client = selectedCategoryFrom === CategoryFromType.client ? window.DbClients[this.editor.getSelfModuleName()][selectedCategory] : databaseClient(selectedCategory);
        let tableNames = await client.describeTables();
        if (this.unmount) {
            return;
        }
        let status = ClientStatus.waiting;
        let error = "";
        if (isFailedResults(tableNames)) {
            tableNames = [];
            status = ClientStatus.error;
            error = "Can not get Tables!";
        }
        this.setState({ db: selectedCategory, TableNames: tableNames, client, status, error }, async () => {
            cb && (await cb());
            this.setEditorLoading(false);
        });
    }
    async getCollectionFields(selectedCategoryFrom, selectedCategory, collectionName, cb = () => { }) {
        if (!selectedCategory) {
            return;
        }
        this.setEditorLoading(true);
        let client = selectedCategoryFrom === CategoryFromType.client ? window.DbClients[this.editor.getSelfModuleName()][selectedCategory] : databaseClient(selectedCategory);
        let collectionFields = await client.describeColumns(collectionName);
        if (this.unmount) {
            return;
        }
        let status = ClientStatus.waiting;
        let error = "";
        if (isFailedResults(collectionFields)) {
            collectionFields = [];
            status = ClientStatus.error;
            error = "Can not get Collection Fields!";
        }
        this.setState({ db: selectedCategory, collectionFields, client, status, error }, async () => {
            cb && (await cb());
            this.setEditorLoading(false);
        });
    }
    async getTableFields(selectedCategoryFrom, selectedCategory, tableName, cb = () => { }) {
        if (!selectedCategory) {
            return;
        }
        this.setEditorLoading(true);
        let client = selectedCategoryFrom === CategoryFromType.client ? window.DbClients[this.editor.getSelfModuleName()][selectedCategory] : databaseClient(selectedCategory);
        let tableFields = await client.describeColumns({ table: tableName });
        if (this.unmount) {
            return;
        }
        let status = ClientStatus.waiting;
        let error = "";
        if (isFailedResults(tableFields)) {
            tableFields = [];
            status = ClientStatus.error;
            error = "Can not get Table Fields!";
        }
        this.setState({ db: selectedCategory, TableFields: tableFields, client, status, error }, async () => {
            cb && (await cb());
            this.setEditorLoading(false);
        });
    }

    initClient(selectedCategoryFrom, selectedCategory, cb = () => { }) {
        this.setEditorLoading(true);
        let client = selectedCategoryFrom === CategoryFromType.client ? window.DbClients[this.editor.getSelfModuleName()][selectedCategory] : databaseClient(selectedCategory);
        this.setState({ db: selectedCategory, client, status: ClientStatus.waiting }, async () => {
            cb && (await cb());
            this.setEditorLoading(false);
        });
    }

    querySelectionTypes = async (selectedCategoryFrom, selectedCategory, value, init = false) => {
        if (!SelectionTypes[value]) {
            return;
        }
        let client = selectedCategoryFrom === CategoryFromType.client ? window.DbClients[this.editor.getSelfModuleName()][selectedCategory] : databaseClient(selectedCategory)
        if (!client) {
            return;
        }
        this.setEditorLoading(true);
        let result = await client.query(SelectionTypes[value].query);
        if (this.unmount) {
            return;
        }
        if (result && !result.error && result.records) {
            let state = init ? {} : { selectionType: value, nodeLabel: "", nodeLabels: [], propertyName: "" };
            this.setState(_.assign(state, {
                nodeLabels: _.reduce(extractColumnDatas(false, result.records, false).rows, (prev, obj, index) => {
                    prev.push(_.values(obj)[0]);
                    return prev;
                }, [])
            }), this.setEditorLoading)
        } else {
            showToast("query result error!", "error");
            this.setState({ selectionType: "", nodeLabel: "", nodeLabels: [], propertyName: "" },
                this.setEditorLoading)
        }
    }

    queryPropertyNames = async (selectedCategoryFrom, selectedCategory, value) => {
        let client = selectedCategoryFrom === CategoryFromType.client ? window.DbClients[this.editor.getSelfModuleName()][selectedCategory] : databaseClient(selectedCategory)
        if (!client) {
            return;
        }
        let query = `CALL db.propertyKeys() YIELD propertyKey as propertyName WITH propertyName WHERE toLower(propertyName) CONTAINS toLower($input) RETURN DISTINCT propertyName`;
        let prameters = { input: value };
        this.setEditorLoading(true);
        let result = await client.query(query, prameters);
        if (this.unmount) {
            return;
        }
        if (result && !result.error && result.records) {
            this.setState({
                propertyNames: _.reduce(extractColumnDatas(false, result.records, false).rows, (prev, obj, index) => {
                    prev.push(_.values(obj)[0]);
                    return prev;
                }, []),
                propertyNum: undefined,
            }, this.setEditorLoading)
        } else {
            showToast("query result error!", "error");
            this.setState({ propertyNames: [], propertyNum: undefined, },
                this.setEditorLoading)
        }
    };

    queryPropertyValues = async (selectedCategoryFrom, selectedCategory, selectionType, nodeLabel, propertyName, value) => {
        let client = selectedCategoryFrom === CategoryFromType.client ? window.DbClients[this.editor.getSelfModuleName()][selectedCategory] : databaseClient(selectedCategory)
        if (!client) {
            return;
        }
        let query = SelectionTypes[selectionType].queryValue(nodeLabel, propertyName);
        let prameters = { input: value };
        this.setEditorLoading(true);
        let result = await client.query(query, prameters);
        if (this.unmount) {
            return;
        }
        if (result && !result.error && result.records) {
            this.setState({
                propertyValues: _.reduce(extractColumnDatas(false, result.records, false).rows, (prev, obj, index) => {
                    prev.push(_.values(obj)[0]);
                    return prev;
                }, []),
            }, this.setEditorLoading)
        } else {
            showToast("query result error!", "error");
            this.setState({ propertyValues: [], },
                this.setEditorLoading)
        }
    };

    isDbFile(selectedCategoryFrom, selectedCategory) {
        return selectedCategoryFrom === CategoryFromType.files && isFileDb(this.editor.react_component.getFileData(selectedCategory));
    }
    isExcelFile(selectedCategoryFrom, selectedCategory) {
        return selectedCategoryFrom === CategoryFromType.files && isFileExcel(this.editor.react_component.getFileData(selectedCategory));
    }
    isArrowFile(selectedCategoryFrom, selectedCategory) {
        return selectedCategoryFrom === CategoryFromType.files && isFileArrow(this.editor.react_component.getFileData(selectedCategory));
    }

    render() {
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        const { chartConfig: { selectedCategoryFrom, selectedCategory, selectedClientType, active, renderType, fetchPeriod, text, neo }, data, chartEditor } = settings.state;
        const { className, result, status, error, fields, needRun, relationProperties, selectionType } = this.state;
        let getJSON = () => {
            if (~[CategoryFromType.database, CategoryFromType.neo4j, CategoryFromType.mongodb, CategoryFromType.dynamodb].indexOf(selectedCategoryFrom)) {
                return result;
            } else {
                return data;
            }
        }
        const FORMAT = 'mm:ss';
        return <div className={`upload-data ${className || ''}`}>
            {/* <h2>Data Prepare</h2> */}
            <Form name="basic" labelCol={{ span: 6, }} wrapperCol={{ span: 18, }} autoComplete="off">
                <Form.Item label="Select datasource" className="config-item">
                    <Row>
                        <Col md={24}>
                            <Space
                                direction="vertical"
                                size="small"
                                style={{
                                    display: 'flex',
                                }}
                            >
                                <div className="d-flex align-items-center div-gutter">
                                    <ChooseDatas
                                        style={{ width: 240 }}
                                        getPopupContainer={SelectProps.getPopupContainer}
                                        displayRender={(label, selectedOptions) => {
                                            return _.reduce(selectedOptions, (prev, curr) => { prev.push(curr.value); return prev; }, []).join("/")
                                        }}
                                        optionTypes={[CategoryFromType.files, CategoryFromType.main, CategoryFromType.shareData, CategoryFromType.database, CategoryFromType.neo4j, CategoryFromType.mongodb, CategoryFromType.dynamodb, CategoryFromType.client]}
                                        editor={this.editor}
                                        dname={undefined}
                                        selectedCategoryFrom={selectedCategoryFrom}
                                        selectedCategory={selectedCategory}
                                        onCategoryChanged={async ({ selectedCategoryFrom, selectedCategory, refresh }) => {
                                            if (refresh) { return; }
                                            let value = selectedCategoryFrom && selectedCategory ? [selectedCategoryFrom, selectedCategory] : [];
                                            if (value.length === 2) {
                                                let selectedClientType = value[0] === CategoryFromType.client ? window.DbClients[this.editor.getSelfModuleName()][value[1]].dialect : undefined;
                                                let state = {
                                                    selectedCategoryFrom: value[0],
                                                    selectedCategory: value[1],
                                                    selectedClientType: selectedClientType,
                                                    selectedProperties: [],
                                                    emptyFills: {},
                                                    castColumns: {},
                                                    joinColumns: {},
                                                    type: ChartTypeTemplate.type,
                                                }
                                                if (!renderType || !~(value[0] === CategoryFromType.client && selectedClientType ? RenderTypesOfDialect(selectedClientType) : RenderTypesOfCategory[value[0]])
                                                    .indexOf(renderType) || ~COMMON_RENDER_TYPES.indexOf(renderType)) {//previous select renderType not support ,then set RenderType.Chart
                                                    _.assign(state, {
                                                        renderType: RenderType.Chart,
                                                        options: _.cloneDeep(getRenderTypeOptions(RenderType.Chart))
                                                    })
                                                }
                                                await settings.onCategoryChanged(state)
                                                if (value[0] === CategoryFromType.database || value[0] === CategoryFromType.client && ~ReDbDialets.indexOf(selectedClientType) ||
                                                    this.isDbFile(value[0], value[1]) ||
                                                    this.isExcelFile(value[0], value[1]) ||
                                                    this.isArrowFile(value[0], value[1])) {
                                                    await this.getTables(value[0], value[1]);
                                                } else if (value[0] === CategoryFromType.mongodb || value[0] === CategoryFromType.client && selectedClientType === DatabaseType.MongoDB) {
                                                    await this.getCollectionNames(value[0], value[1]);
                                                } else if (value[0] === CategoryFromType.dynamodb || value[0] === CategoryFromType.client && selectedClientType === DatabaseType.DynamoDB) {
                                                    await this.getTableNames(value[0], value[1]);
                                                } else if (value[0] === CategoryFromType.neo4j || value[0] === CategoryFromType.client && selectedClientType === DatabaseType.Neo4j) {
                                                    this.initClient(value[0], value[1]);
                                                } else {
                                                    if (renderType === RenderType.Table) {
                                                        this.setState({ needRun: true }, this.renderChart);
                                                    }
                                                }
                                            } else if (value.length === 0) {
                                                let state = {
                                                    selectedCategoryFrom: "",
                                                    selectedCategory: "",
                                                    selectedClientType: undefined,
                                                    selectedProperties: [],
                                                    emptyFills: {},
                                                    castColumns: {},
                                                    joinColumns: {},
                                                    type: ChartTypeTemplate.type,
                                                    chartData: [],
                                                    chartColumns: [],
                                                }
                                                if (!renderType || !~RenderTypesOfCategory.none.indexOf(renderType)) {
                                                    _.assign(state, {
                                                        renderType: RenderType.Markdown,
                                                        options: _.cloneDeep(getRenderTypeOptions(RenderType.Markdown)),
                                                    })
                                                }
                                                await settings.onCategoryChanged(state)
                                            }
                                        }}
                                    />
                                    <Tooltip color="cyan" title={RenderTypeDesc[renderType].helperText} placement='rightTop'>
                                        <Select style={{ width: 120 }}
                                            options={_.map(selectedCategoryFrom === CategoryFromType.client ?
                                                RenderTypesOfDialect(window.DbClients[this.editor.getSelfModuleName()][selectedCategory].dialect) : RenderTypesOfCategory[selectedCategoryFrom] || RenderTypesOfCategory.none, (value, key) => {
                                                    return { value: value, label: value, }
                                                })}
                                            getPopupContainer={SelectProps.getPopupContainer}
                                            showSearch
                                            placeholder="Search to Select"
                                            optionFilterProp="children"
                                            filterOption={(input, option) => (option?.label ?? '').includes(input)}
                                            filterSort={(optionA, optionB) =>
                                                (optionA?.label ?? '').toLowerCase().localeCompare((optionB?.label ?? '').toLowerCase())
                                            }
                                            value={renderType}
                                            onChange={(ntype) => {
                                                let state = {};
                                                if (ntype === RenderType.Chart && result) {
                                                    if ((selectedCategoryFrom === CategoryFromType.database || selectedCategoryFrom === CategoryFromType.client && ~ReDbDialets.indexOf(selectedClientType) ||
                                                        this.isDbFile(selectedCategoryFrom, selectedCategory) ||
                                                        this.isExcelFile(selectedCategoryFrom, selectedCategory) ||
                                                        this.isArrowFile(selectedCategoryFrom, selectedCategory)) &&
                                                        result instanceof Array) {
                                                        settings.setColumnsData(result);
                                                    } else if ((selectedCategoryFrom === CategoryFromType.neo4j || selectedCategoryFrom === CategoryFromType.client && selectedClientType === DatabaseType.Neo4j) && result.records && !result.error) {
                                                        const { columns, rows } = extractColumnDatas(false, result.records);
                                                        settings.setColumnsData(rows);
                                                    } else if ((~[CategoryFromType.mongodb, CategoryFromType.dynamodb].indexOf(selectedCategoryFrom) || selectedCategoryFrom === CategoryFromType.client && ~[DatabaseType.MongoDB, DatabaseType.DynamoDB].indexOf(selectedClientType)) && result && !result.error && result instanceof Array) {
                                                        settings.setColumnsData(result);
                                                    }
                                                } else if (ntype === RenderType.ParameterSelect) {
                                                    this.querySelectionTypes(selectedCategoryFrom, selectedCategory, selectionType, true);
                                                }
                                                if (neo && neo.maxRecords && RenderTypeDesc[ntype] && RenderTypeDesc[ntype].maxRecords && neo.maxRecords > RenderTypeDesc[ntype].maxRecords) {
                                                    _.assign(state, { neo: _.assign(_.cloneDeep(neo), { maxRecords: RenderTypeDesc[ntype].maxRecords }) });
                                                }
                                                settings.modifyChartConfig(_.assign(state, { renderType: ntype, options: _.cloneDeep(getRenderTypeOptions(ntype)) })/* , this.changeGrid */);
                                            }}>
                                        </Select>
                                    </Tooltip>
                                    {!!(~COMMON_RENDER_TYPES.indexOf(renderType) || renderType === RenderType.Table && ~[CategoryFromType.main, CategoryFromType.shareData, CategoryFromType.files].indexOf(selectedCategoryFrom)) &&
                                        <Button icon={<RunCodeIcon fill={needRun || error} />} onClick={() => {
                                            this.renderChart();
                                        }}>Run</Button>}
                                </div>
                                {!!~[RenderType.Chart].indexOf(renderType) && <div className="d-flex align-items-center div-gutter">
                                    <span>File Storage:</span>
                                    <Switch checkedChildren="On" unCheckedChildren="Off" checked={!active} onChange={(checked) => {
                                        settings.modifyChartConfig({
                                            active: !checked
                                        })
                                    }} />
                                    {/* <span>Fetch Period:</span>
                                <TimePicker placeholder="Select Refresh Time" onChange={(time, timeString) => {
                                    console.log(time, timeString);
                                    time && settings.modifyChartConfig({
                                        fetchPeriod: time.get("m") * 60 + time.get("s")
                                    })
                                }} defaultValue={moment('00:10', FORMAT)} format={FORMAT} /> */}
                                </div>}
                            </Space>
                        </Col>
                    </Row>
                </Form.Item>
                {!!(~[CategoryFromType.database, CategoryFromType.neo4j, CategoryFromType.mongodb, CategoryFromType.dynamodb, CategoryFromType.client].indexOf(selectedCategoryFrom) ||
                    this.isDbFile(selectedCategoryFrom, selectedCategory) ||
                    this.isExcelFile(selectedCategoryFrom, selectedCategory) ||
                    this.isArrowFile(selectedCategoryFrom, selectedCategory)) &&
                    !~[...COMMON_RENDER_TYPES, RenderType.ParameterSelect].indexOf(renderType) &&
                    <Form.Item label={<div className="d-flex align-items-center">
                        <i className="icon icon-inactive fas fa-database"></i>&nbsp;&nbsp;{selectedCategory}
                        <TagComp status={status} desc={error || ""}></TagComp>
                    </div>}>
                        {(selectedCategoryFrom === CategoryFromType.database ||
                            selectedCategoryFrom === CategoryFromType.client && ~ReDbDialets.indexOf(selectedClientType) ||
                            this.isDbFile(selectedCategoryFrom, selectedCategory) ||
                            this.isExcelFile(selectedCategoryFrom, selectedCategory) ||
                            this.isArrowFile(selectedCategoryFrom, selectedCategory)) &&
                            <DatabaseComp settings={this}></DatabaseComp>}
                        {(selectedCategoryFrom === CategoryFromType.neo4j || selectedCategoryFrom === CategoryFromType.client && selectedClientType === DatabaseType.Neo4j) && <this.Neo4jComp></this.Neo4jComp>}
                        {(selectedCategoryFrom === CategoryFromType.mongodb || selectedCategoryFrom === CategoryFromType.client && selectedClientType === DatabaseType.MongoDB) && <MongoComp settings={this}></MongoComp>}
                        {(selectedCategoryFrom === CategoryFromType.dynamodb || selectedCategoryFrom === CategoryFromType.client && selectedClientType === DatabaseType.DynamoDB) && <DynamoComp settings={this}></DynamoComp>}
                    </Form.Item>}
            </Form>

            {renderType === RenderType.ParameterSelect && <Form labelCol={{ span: 6, }} wrapperCol={{ span: 18, }}>
                <Form.Item label="&nbsp;" colon={false}><ParameterSelectComp settings={this}></ParameterSelectComp></Form.Item>
            </Form>}

            {!!~[RenderType.IFrame, RenderType.Markdown, RenderType.Python, RenderType.Code].indexOf(renderType) && <div>
                <RawJSONComp className="limit-height" renderType={renderType} result={text || ""} onChange={(cm, data, value) => {
                    settings.modifyChartConfig({ text: value });
                    this.setState({ needRun: true })
                }} run={this.renderChart}></RawJSONComp>
            </div>}

            {renderType === RenderType.RawJSON && <TurnPageRawJSONComp className="limit-height" renderType={renderType}
                result={getJSON()}></TurnPageRawJSONComp>}

            {!!~NEO_FOOTER_RENDERS.indexOf(renderType) && !!result && !!fields && !!fields.length &&
                <Form labelCol={{ span: 6, }} wrapperCol={{ span: 18, }}>
                    <Form.Item label="Selection">
                        <NeoCardViewFooter type={renderType} fields={fields} selection={neo && neo.selection || {}}
                            onSelectionUpdate={(nodeLabel, value) => {
                                let selection = neo && neo.selection || {}
                                selection[nodeLabel] = value;
                                settings.modifyChartConfig({ neo: _.assign(neo || {}, { selection }) }, () => {
                                    this.renderChart();
                                });
                            }}
                            showOptionalSelections={true}
                        ></NeoCardViewFooter>
                    </Form.Item>
                    {!!~[RenderType.SankeyChart].indexOf(renderType) && <Form.Item label="Relationship value Property">
                        <Select
                            getPopupContainer={SelectProps.getPopupContainer}
                            value={neo && neo.labelProperty || ""} onChange={(value) => {
                                settings.modifyChartConfig({ neo: _.assign(neo || {}, { labelProperty: value }) }, () => {
                                    this.renderChart();
                                });
                            }}>{_.map(relationProperties, (value, index) => {
                                return <Option key={value} value={value}>{value}</Option>
                            })}
                        </Select>
                    </Form.Item>}
                </Form>
            }

            {isChartRenderType(renderType) && <TableComp saveFunc={this.state.dbTable && ((startEndMap, filteredInfo, sortedInfo) => {
                const { filters, sorts } = this.state;
                assignFiltersSorts(startEndMap, filteredInfo, sortedInfo, filters, sorts);
                this.setState({ needRun: true, filters, sorts,/*, nData: []*/ }, () => {
                    this.runCode(true);
                });
            })} settings={settings}></TableComp>}
            <hr />
            <div className="b--silver">
                <div className="ph3 mt2 mb1 f7 mid-gray lh-f7"> {RenderTypeDesc[renderType].helperText}</div>
            </div>
        </div >;
    }
}