'use strict';

const express = require('express');
const _ = require('lodash');
const auth = require('./../util/auth');

const UserShareProjects = require('./UserShareProjects.model');


let router = express.Router();

router.all('/addUserToShareProject', auth.protected, function (req, res) {
    let addInfo = {
        userId: req.parameters.addInfo.userId,
        email: req.parameters.addInfo.email,
        projectId: req.parameters.addInfo.projectId,
        projectName: req.parameters.addInfo.projectName,
        purview: req.parameters.addInfo.purview,
        status: req.parameters.addInfo.status,
        addUser: req.parameters.userId
    }
    UserShareProjects.addUserToShareProject(addInfo, function (err) {
        if (err) {
            console.error(err);
        }
        return res.send({ 
            status: err ? 1 : 0, 
            message: err ? err.message : "tips.successful",
        });
    });
});

router.all('/getAllUserByProject', auth.protected, function (req, res) {
    let projectId = req.parameters.projectId
    UserShareProjects.getAllUserByProject(projectId, function (err, users) {
        if (err) {
            console.error(err);
        }
        return res.send({ 
            status: err ? 1 : 0, 
            message: err ? err.message : "tips.successful", 
            content: users 
        });
    });
});

router.all('/getAllProjectByUser', auth.protected, function (req, res) {

    let userId = req.parameters.userId

    UserShareProjects.getAllProjectByUser(userId, function (err, projects) {
        if (err) {
            console.error(err);
        }
        return res.send({ 
            status: err ? 1 : 0, 
            message: err ? err.message : "tips.successful", 
            content: projects 
        });
    });
});


router.all('/deleteShareProjectById', auth.protected, function (req, res) {
    let id = req.parameters.id
    UserShareProjects.deleteShareProjectById(id, function (err) {
        if (err) {
            console.error(err);
        }
        return res.send({ 
            status: err ? 1 : 0, 
            message: err ? err.message : "tips.successful",
        });
    });
});
router.all('/changeUserPurviewById', auth.protected, function (req, res) {
    let info = {
        id: req.parameters.id,
        purview: req.parameters.purview
    }
    UserShareProjects.changeUserPurviewById(info, function (err) {
        if (err) {
            console.error(err);
        }
        return res.send({ 
            status: err ? 1 : 0, 
            message: err ? err.message : "tips.successful",
        });
    });
});



module.exports = router;