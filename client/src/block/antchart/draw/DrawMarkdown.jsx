import React from "react";
import { v4 as uuidv4, NIL as NIL_UUID } from 'uuid';

export default class DrawMarkdown extends React.Component {
    /**
     *
     * @constructor
     * @param {{editor:import ('tagEditor').default}} props
     */
    constructor(props) {
        super(props);
        this.props = props;
        const { ele } = this.props;
        this.domRef = React.createRef();
        this.id = uuidv4();
    }

    componentDidUpdate(prevProps, prevState) {
        const { declareEle, ele, dText } = this.props;
        if (prevProps.declareEle !== declareEle || prevProps.ele !== ele || prevProps.dText !== dText) {
            this.draw();
        }
    }

    componentWillUnmount() {
        const { editor } = this.props;
        editor.refreshAnchors();
    }

    draw() {
        const { declareEle, ele, dText, editor } = this.props;
        /**@type {HTMLElement} */
        const container = this.domRef.current;
        if (container && ele) {
            container.innerHTML = "";
            declareEle && container.appendChild(declareEle);
            ele.setAttribute("d-text", dText);
            container.appendChild(ele)
            editor.refreshAnchors();
        }
    }

    componentDidMount() {
        this.draw();
    }

    render() {
        return <div id={this.id} ref={this.domRef}></div>
    }
}