import {
    Button, Col,
    Form, Input,
    InputNumber, Modal, Row,
    Slider, Space, Table, Tooltip
} from 'antd';
import axios from 'axios';
import Cropper from 'cropperjs';
import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { Controlled } from 'react-codemirror2';
import editor from '../commonEditor';
import DuplicateFile from '../file/DuplicateFile';
import SyncFile from '../file/SyncFile';
import MdFiles from "../file/MdFiles";
import MyDropzone from '../file/MyDropzone';
import NewDatabase from '../file/NewDatabase';
import NewSecret from '../file/NewSecret';
import NewNotebook from '../file/NewNotebook';
import NeoDashDashboardGallery from '../file/NeoDashDashboardGallery';
import OpenChangesComp from '../file/OpenChangesComp';
import ChartFullScreen from '../file/ChartFullScreen';
import Publish from '../file/Publish';
import ReplaceFile from '../file/ReplaceFile';
import { DEFAULT_VER, FILESDIR, DefaultFileData, getUndefinedContent, getFormatUndefinedContent } from '../util/localstorage';
import { IsKey, showMessage } from '../util/utils';
import { cursorActivityFunc } from 'util/CodeMirror.showHint';
import { NeoGraphItemInspectModal } from 'block/antchart/modal/GraphItemInspectModal';

let maxLength = 50 * 1024 * 1024;
const GG = {
    /**@type {Cropper} */
    cropper: null,
    quality: 0.50,
}
const destroyCropper = function () {
    if (GG.cropper) {
        GG.cropper.destroy();
        GG.cropper = null;
    }
}
const { getFileKey, Z_INDEX, WIDTH, Title } = require('../file/fileUtils');
const { isFileImage, ImageType, fallbackimg, codeTime, formatSizeUnits, getFileIcon, imageTypes, ImgAccept,
    assembling } = require('../util/helpers.js');
const { showToast, ModalType, dataURItoBlob, copyContent, cell_shortcuts_keys, code_editor_keys, linkKeys, } = require("../util/utils");

/**
 * 
 * 
 */
export default function ModalComp(props) {
    /** @type {MdFiles} */
    const react_component = props.react_component;
    const { modalType, modalData, treeSelectType } = react_component.state;
    return <div>
        <DuplicateFile refreshType={(~[ModalType.DuplicateFile, ModalType.Fork, ModalType.ImportFiles].indexOf(modalType)) ? 1 :
            ((modalType === ModalType.None) ? 0 : 2)
        } width={750}  {...props} modalData={modalData}></DuplicateFile>
        <SyncFile refreshType={(~[ModalType.SyncFile].indexOf(modalType)) ? 1 :
            ((modalType === ModalType.None) ? 0 : 2)
        } width={750}  {...props} modalData={modalData}></SyncFile>
        <Comps {...props} modalData={modalData}></Comps>
    </div>
}

function Comps(props) {
    /** @type {MdFiles} */
    const react_component = props.react_component;
    const { modalType, sourceFile } = react_component.state;
    switch (modalType) {
        case ModalType.OpenChanges:
            return <ModalWrapper {...props}><OpenChangesComp {...props}></OpenChangesComp></ModalWrapper>;
        case ModalType.DisplaySource:
            return <ModalWrapper {...props}><DisplaySourceComp {...props}></DisplaySourceComp></ModalWrapper>;
        case ModalType.SetPreviewImage:
            return <SetPreviewImageComp {...props}></SetPreviewImageComp>;
        case ModalType.VersionControl:
            return <ModalWrapper {...props}><VersionControlComp {...props}></VersionControlComp></ModalWrapper>;
        case ModalType.Publish:
            return <Publish width={550} {...props} fileData={sourceFile}></Publish>;
        case ModalType.ReplaceFile:
            return <ReplaceFile width={550} {...props} ></ReplaceFile>
        case ModalType.NewDatabase:
            return <NewDatabase width={550} {...props} ></NewDatabase>
        case ModalType.NewSecret:
            return <NewSecret width={550} {...props} ></NewSecret>
        case ModalType.NewNotebook:
            return <NewNotebook {...props} ></NewNotebook>
        case ModalType.NeoDashDashboardGallery:
            return <NeoDashDashboardGallery {...props} ></NeoDashDashboardGallery>
        case ModalType.ChartFullScreen:
            return <ChartFullScreen {...props} ></ChartFullScreen>
        case ModalType.NeoGraphItemInspect:
            return <NeoGraphItemInspectModal  {...props}></NeoGraphItemInspectModal>
        default: {
            return <div></div>
        }
    }
}

function ModalWrapper(props) {
    /** @type {MdFiles} */
    const react_component = props.react_component;
    const { modalType, sourceFile } = react_component.state;
    const { children, width } = props;
    const closeFunc = () => {
        react_component.setState({
            modalType: ModalType.None,
        });
        destroyCropper();
    };
    const buttons = [
        <Button key="back" onClick={closeFunc}>
            Cancel
        </Button>
    ];
    return <Modal zIndex={Z_INDEX}
        className={"modal-comp data-html2canvas-ignore"}
        width={width || WIDTH}
        title={<Title modalType={modalType} sourceFile={sourceFile}></Title>}
        visible={modalType !== ModalType.None}
        onCancel={closeFunc}
        footer={buttons}
    >
        {children}
    </Modal>
}

function DisplaySourceComp(props) {
    /** @type {MdFiles} */
    const react_component = props.react_component;
    const { source, sourceFile } = react_component.state;
    return <div>
        <i title={"Copy Source"} className="icon fas fa-copy" onClick={
            async () => {
                let content = sourceFile ? (await react_component.getTextContent(sourceFile)) : getFormatUndefinedContent();
                copyContent(content, () => {
                    showMessage("copy success!", "success")
                })
            }
        }></i>
        <Controlled className={`code-edit-cm`}
            editorDidMount={(cm, value, cb) => {
            }}
            onFocus={(cm) => {
            }}
            value={source}
            onCursorActivity={(cm) => { cursorActivityFunc.call(cm, cm) }}
            options={_.assign({
                mode: { name: "javascript", json: true },
                theme: window.editor.getSettings().getTheme(),
                lineNumbers: true,
                lineWrapping: true,
                // viewportMargin: Infinity,
                // styleSelectedText: true,
                // matchBrackets: false,
                // styleActiveLine: false,
                // nonEmpty: false,
                foldGutter: true,
                // placeholder: "",
                gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
                readOnly: false,
                extraKeys: assembling([{
                    key: linkKeys(code_editor_keys["Full Screen"]), value: function (cm) {
                        cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                    }
                }, {
                    key: "Esc", value: (cm) => {
                        if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                    }
                }, {
                    key: linkKeys(code_editor_keys["Show Editor LineNumbers"]), value: (cm) => {
                        cm.setOption("lineNumbers", !cm.getOption("lineNumbers"));
                    }
                }, {
                    key: linkKeys(code_editor_keys["Fold Code"]), value: function (cm) { cm.foldCode(cm.getCursor()); },
                }])
            })}
        />
    </div>
}

function VersionControlComp(props) {
    /** @type {MdFiles} */
    const react_component = props.react_component;
    const { sourceFile } = react_component.state;
    const openEditor = react_component.getOpenEditor(sourceFile.fileKey);
    if (!openEditor) {
        return <div>can't find editor!</div>
    }
    const fileData = openEditor.getFileData();
    const fileVersion = openEditor.getVersion();
    if (!fileData) {
        return <div></div>
    }
    const file_version = fileData.version || DEFAULT_VER;
    const [file_version_tmp, setFile_version_tmp] = useState(fileVersion);
    const [files, setFiles] = useState([]);
    const [message, setMessage] = useState([]);
    let [form] = Form.useForm();
    useEffect(() => {
        setFile_version_tmp(openEditor.version);
    }, [openEditor.version]);
    let refreshFunc = async () => {
        if (process.env.REACT_APP_VERSION <= "1.1.10") {
            return;
        }
        let res = await axios.post(`/api/grove/versionFiles`, _.assign({}, fileData,
            { userId: react_component.props.userId, projectId: react_component.props.projectId }));
        if (!res.data.status) {
            let zeroFile = _.cloneDeep(fileData);
            zeroFile.name = '0';
            setFiles(_.concat(_.values(res.data.content), [zeroFile])
                .filter(/**@param {DefaultFileData} file*/(file) => {
                    file.name = parseInt(file.name);
                    file.key = file.fileKey;
                    return !isNaN(file.name)
                })
                .sort(/**@param {DefaultFileData} f1 @param {DefaultFileData} f2 */(f1, f2) => {
                    return f1.name - f2.name;
                }));
        }
    }
    useEffect(refreshFunc, [openEditor.fileData]);
    const marks = {
        0: {
            style: {
                color: 'black',
            },
            label: <strong>{0}</strong>,
        }
    };
    marks[file_version] = {
        style: {
            color: '#f50',
        },
        label: <strong>{file_version}</strong>,
    }
    let nowTime = new Date().getTime();
    const columns = [
        {
            title: 'Version',
            dataIndex: 'name',
            key: 'name',
            render: (version, record) => {
                return <div>
                    <a>{version}{fileData && fileData.messages[version] && `(${fileData.messages[version]})` || ""}</a>
                </div>
            },
            sorter: {
                compare: (a, b) => a.name - b.name,
                multiple: 3,
            },
        },
        {
            title: 'Edited',
            dataIndex: 'mtimeMs',
            key: 'mtimeMs',
            render: mtimeMs => codeTime(nowTime, mtimeMs)
        },
        {
            title: 'Size',
            dataIndex: 'size',
            key: 'size',
            render: size => formatSizeUnits(size),
            responsive: ['md']
        },
        {
            title: 'Action',
            key: 'action',
            render: /**@param  record {DefaultFileData} */(text, record) => (
                <Space key={record.fileKey} size="middle">
                    <Button type="link" onClick={() => {
                        react_component.switchFileTagEditor(fileData, record.name)
                        // react_component.handleSearch(react_component.getInfo(
                        //     { fileKey: fileData.fileKey, version: record.name }), true)
                    }}>view</Button>
                </Space>
            ),
        },
    ];
    const onFinish = async (values) => {
        console.log('Success:', values);
        setMessage(values.message);
        await react_component.tagVersion(fileData, values.message);
        await refreshFunc();
    };

    const onFinishFailed = (errorInfo) => {
        showToast(_.reduce(errorInfo.errorFields, (prev, curr, index) => {
            prev += curr.errors.join(",")
            return prev + "; ";
        }, ""), 'error')
        console.log('Failed:', errorInfo);
    };
    return <ul className="list-group list-group-flush">
        <li className="list-group-item">
            <div>Current Version: {fileVersion}&nbsp;&nbsp;Taged Version: {file_version}</div>
        </li>
        {(fileData.versionMtimeMs !== fileData.mtimeMs) &&
            !react_component.isNeedUpload(fileData) &&
            <li className="list-group-item">
                <Form layout={'inline'}
                    initialValues={{ message: message }}
                    onFinish={onFinish}
                    form={form}
                    onFinishFailed={onFinishFailed}
                >
                    <Form.Item name="message" rules={[
                        {
                            required: true,
                        },
                    ]}>
                        <Input onKeyDown={(e) => {
                            let key = String.fromCharCode(e.keyCode).toLowerCase();
                            //filter the number & char key
                            if (!(/^[0-9a-z]$/g).test(key) || e.key === e.code) {
                                key = e.key;
                            }
                            if (IsKey(e, key, {
                                title: "Commit Message",
                                keys: ["Ctrl", "Enter"]
                            })) {
                                form.submit();
                            }
                        }} placeholder="Message (Ctrl+Enter to commit)" />
                    </Form.Item>
                    <Form.Item key="back"  >
                        <Button type="primary" htmlType="submit">{`File Changed , Tag Version To ${fileData.version + 1}`}</Button>
                    </Form.Item>
                </Form>

            </li>}
        <li className="list-group-item">
            {<Table size='small' columns={columns} rowClassName={(record, index) => {
                if (record.name === fileVersion) {
                    return "text-success"
                }
            }} dataSource={files} />}
        </li>
    </ul>
}

function SetPreviewImageComp(props) {
    /** @type {MdFiles} */
    const react_component = props.react_component;
    const { modalType, sourceFile, previewImg } = react_component.state;
    const { width } = props;
    const closeFunc = () => {
        react_component.setState({
            modalType: ModalType.None,
        });
        destroyCropper();
    };
    useEffect(() => {
        if (!react_component.state.previewImg) {
            return;
        }
        const image = document.getElementById('previewImage');
        destroyCropper();
        GG.cropper = new Cropper(image, {
            aspectRatio: 195 / 200,
            crop(event) {
                // console.log(event.detail.x);
                // console.log(event.detail.y);
                // console.log(event.detail.width);
                // console.log(event.detail.height);
                // console.log(event.detail.rotate);
                // console.log(event.detail.scaleX);
                // console.log(event.detail.scaleY);
            },
            dragMode: 'move',
            autoCropArea: 1,
            minCropBoxWidth: 200,
            restore: false,
            guides: false,
            center: false,
            highlight: false,
            cropBoxMovable: false,
            cropBoxResizable: true,
            toggleDragModeOnDblclick: false,
        });
    }, [react_component.state.previewImg]);
    const buttons = [
        <Button key="setImage" onClick={() => {
            GG.cropper && GG.cropper.getCroppedCanvas().toBlob((blob) => {
                react_component.handleSetImageOk(blob)
            }, 'image/png', GG.quality);
        }}>Set Image</Button>,
        <Button key="back" onClick={closeFunc}>
            Close
        </Button>,
    ];
    return <Modal zIndex={Z_INDEX}
        className={"modal-comp data-html2canvas-ignore"}
        width={width || WIDTH}
        title={<Title modalType={modalType} sourceFile={sourceFile}></Title>}
        visible={modalType !== ModalType.None}
        onCancel={closeFunc}
        footer={buttons}
    >
        <Row>
            <Col sm={8}>
                <h5>current</h5>
                {PreviewImageComp(react_component)}
                <Button className='mt2' type="link" onClick={() => {
                    react_component.setWrapperLoading(true, async () => {
                        await react_component.screenshotAsPreviewImage();
                        react_component.setWrapperLoading(false);
                    }, true);
                }}>Quick take a screenshot</Button>
            </Col>
            <Col sm={16}>
                {!previewImg ?
                    <div>
                        <Button className='mb2' type="link" onClick={() => {
                            react_component.setWrapperLoading(true, async () => {
                                let file = dataURItoBlob(await window.currentEditor.getScreenshot());
                                file.lastModifiedDate = new Date();
                                file.name = "fileName.png";
                                react_component.setState({ previewImg: file });
                                react_component.setWrapperLoading(false);
                            }, true);
                        }}>Take a screenshot</Button>
                        <MyDropzone accept={ImgAccept} input={true} multiple={false} handleFilesFunc={async (files) => {
                            if (files.length === 1 && isFileImage(files[0])) {
                                if (files[0].size > maxLength) {
                                    showToast(`${files[0].name} is larger than ${formatSizeUnits(maxLength)}`, "error")
                                    return;
                                }
                                react_component.setState({ previewImg: files[0] });
                            } else {
                                showToast("please select png image")
                            }
                        }}  >{
                                <div className="text-center">
                                    <div>Drag and drop an image or</div>
                                    <div>
                                        <div className="btn btn-light">Select a file</div>
                                    </div>
                                </div>
                            }
                        </MyDropzone>
                    </div> :
                    <Button className='mb2' type="link" onClick={() => {
                        react_component.setState({ previewImg: null }, () => {
                            destroyCropper();
                        })
                    }}>Upload new</Button>
                }
                <div className={`preview-image-div2 ${previewImg ? "" : "hide"}`}>
                    <div className="preview-image-div">
                        <img id="previewImage" alt="Picture" src={previewImg && URL.createObjectURL(previewImg)}></img>
                    </div>
                    <Row className="d-flex justify-content-between align-items-center">
                        <Col className="text-center" xs={8} sm={6}>Zoom</Col>
                        <Col xs={16} sm={14}>
                            <Slider defaultValue={1} max={10} step={0.1} onChange={(value) => {
                                GG.cropper.scale(value, value);
                            }} />
                        </Col>
                    </Row>
                    <Row className="d-flex justify-content-between align-items-center">
                        <Col className="text-center" xs={8} sm={6}>Quality</Col>
                        <Col xs={16} sm={14}>
                            <Slider defaultValue={GG.quality} max={1} step={0.01} onChange={(value) => {
                                GG.quality = value;
                            }} />
                        </Col>
                    </Row>
                </div>
                <div className="font-italic text-secondary">{imageTypes.join(" or ")} are supported. For best results, images should be at least 195x200 px.</div>
            </Col>
        </Row>
    </Modal>;
}

const FailComp = <div style={{ backgroundImage: `url("${fallbackimg}")` }} className="preview-image-wrap bg-img"></div>;

function PreviewImageComp(react_component) {
    const { uploadUri, fileNamesJson, previewImgVersion } = react_component.state;
    let file = window.currentEditor.getFileData();
    if (!file) {
        return FailComp;
    }
    let imageFileKey = getFileKey(getFileKey(file.fileKey, FILESDIR), file.name);
    let currentImage;
    if (fileNamesJson[`${imageFileKey}${ImageType}`]) {
        currentImage = `${uploadUri}${imageFileKey}${ImageType}?version=${previewImgVersion}`;
        return <div style={{ backgroundImage: `url("${currentImage}")` }} className="preview-image-wrap bg-img"></div>;
    }
    return FailComp;
}