import {
    csvDataFunc, dbDataFunc, gzipDataFunc, imageDataFunc, graphxrDataFunc,
    jsDataFunc, htmlDataFunc, svgDataFunc, jsonDataFunc, audioDataFunc, videoDataFunc, zipDataFunc, excelDataFunc, arrowDataFunc
} from '../block/data.js';
import { editorjsTransferToMd } from '../block/markdown/FileHandler.js';
import { showToast } from '../util/utils.js';
import { FolderOutlined, SmileOutlined, FileOutlined } from '@ant-design/icons';
import Common from '../util/Common.js';
import { createLinkElement } from 'stdlib/link.js';

const { ImageCache, isFileImage, imageTypeReg, videoTypeReg, audioTypeReg, isFileJson,
    transferType, isFileJs, isFileSvg, isFileExcel, isFileCsv, isFileGzip,
    isFileEditorJs, isFileNoEdit, isFileDb, getFileIcon, isFileVideo,
    isFileAudio, isFileGraphxr, isFileZip, isFileHtml, SelectType, ConvertToCSV, isFileArrow, isFilePlain, isFileMd } = require('../util/helpers.js');
const { getItem, getFileNamesKey, DefaultFileData, DefaultFolderData,
    DefaultFileType, getUploadUri, USER_ID, PROJECT_ID, PROJECT_USER_ID } = require("../util/localstorage");

export const fileNameReg = /^[0-9a-zA-Z-+_\.\(\) ]*$/;
export const folderNameReg = /^[0-9a-zA-Z-+_]*$/;
export const fileSeparator = '/';
export const verFileTag = ".ver";
export const Z_INDEX = 1002;
export const WIDTH = 1024;

export const rootDir = Object.assign({}, DefaultFolderData, {
    folderKey: "", name: ""
})
export const ExportType = {
    MD: "markdown",
    GROVE: "grove",
    PDF: "pdf"
}
export const FILTER_TYPE = {
    links: "links",
    published: "published",
    shared: "shared",
}

/**
 * some (like csv,json,js) need decode the editor's content to storage data
 * @param {*} content 
 */
export const transferToRealData = function (file, content) {
    if (!content || content === "undefined") {
        return content;
    }
    if (isFileJson(file)) {
        let json = JSON.parse(content);
        content = '';
        _.each(json.blocks, (block) => {
            if (block.type === 'codeTool') {
                content = block.data.codeData.value.substring("{return ".length, block.data.codeData.value.length - 1);
                return false;
            }
        })
    } else if (isFileCsv(file)) {
        let json = JSON.parse(content);
        let block = json.blocks[1];
        if (block.type === 'codeTool') {
            content = block.data.codeData.value.substring("text = \`".length, block.data.codeData.value.length - 1);
        } else if (block.type === 'table') {
            content = _.reduce(block.data.content, (prev, curr) => {
                prev.push(curr.join(","));
                return prev;
            }, []).join("\n");
        } else {
            showToast("explain csv error", "error")
        }
    } else if (isFileJs(file)) {
        let json = JSON.parse(content);
        content = '';
        _.each(json.blocks, (block) => {
            if (block.type === 'codeTool') {
                content = block.data.codeData.value;
                return false;
            }
        })
    } else if (isFileHtml(file)) {
        let json = JSON.parse(content);
        content = '';
        _.each(json.blocks, (block) => {
            if (block.type === 'codeTool') {
                content = block.data.codeData.value;
                return false;
            }
        })
    } else if (isFileSvg(file)) {
        let json = JSON.parse(content);
        content = '';
        _.each(json.blocks, (block) => {
            if (block.type === 'codeTool') {
                content = block.data.codeData.value;
                return false;
            }
        })
    }
    return content;
}

/**
* maybe content need wrap to let editorjs render it
* @param {String} content
* @param {DefaultFileData} fileData
*/
export const wrapContent = function (content, fileData, uploadUri) {
    if (isFileCsv(fileData)) {
        content = JSON.stringify(csvDataFunc(content));
    } else if (isFileJson(fileData)) {
        content = JSON.stringify(jsonDataFunc(content));
    } else if (isFileNoEdit(fileData)) {
        if (isFileImage(fileData)) {
            content = JSON.stringify(imageDataFunc(content, fileData.name));
        } else if (isFileGzip(fileData)) {
            content = JSON.stringify(gzipDataFunc(content, fileData.name));
        } else if (isFileZip(fileData)) {
            content = JSON.stringify(zipDataFunc(content, fileData.name));
        } else if (isFileDb(fileData)) {
            content = JSON.stringify(dbDataFunc(content, fileData.name));
        } else if (isFileExcel(fileData)) {
            content = JSON.stringify(excelDataFunc(uploadUri + fileData.fileKey, fileData.name));
        } else if (isFileArrow(fileData)) {
            content = JSON.stringify(arrowDataFunc(uploadUri + fileData.fileKey, fileData.name));
        } else if (isFileAudio(fileData)) {
            content = JSON.stringify(audioDataFunc(uploadUri + fileData.fileKey, fileData.name));
        } else if (isFileVideo(fileData)) {
            content = JSON.stringify(videoDataFunc(uploadUri + fileData.fileKey, fileData.name));
        } else if (isFileGraphxr(fileData)) {
            content = JSON.stringify(graphxrDataFunc(uploadUri + fileData.fileKey, fileData.name));
        }
    } else if (isFileJs(fileData)) {
        content = JSON.stringify(jsDataFunc(content));
    } else if (isFileHtml(fileData)) {
        content = JSON.stringify(htmlDataFunc(content));
    } else if (isFileSvg(fileData)) {
        content = JSON.stringify(svgDataFunc(content));
    } else {
        content = content;
    }
    return content;
}
/**
 * 
 * @param {string} content 
 * @param {ExportType} type 
 * @returns string
 */
export const editorjsTransferTo = function (content, type) {
    if (type === ExportType.MD) {
        return editorjsTransferToMd(JSON.parse(content));
    }
    return content;
}
export class FileObj {
    /**
     *
     * @param {typeof DefaultFileData} file
     */
    constructor(file) {
        this.file = file;
    }

    toString() {
        return this.file.fileKey;
    }
}
;

export class DirObj {
    /**
     * 
     * @param {typeof DefaultFolderData} dir 
     */
    constructor(dir) {
        this.dir = dir;
        /**@type {Object.<string,DirObj|FileObj>} */
        this.files = {};
    }

    toString() {
        return this[DefaultFolderData.type].folderKey;
    }
}
export const getDisplayDirObj = function (fileNamesJson) {
    const displayObj = new DirObj(rootDir);
    _.map(fileNamesJson, (file, fileKey) => {
        if (~fileKey.indexOf(fileSeparator)) {
            let tmpObj = displayObj;
            let dir = tmpObj.dir.folderKey;
            let arr = fileKey.split(fileSeparator).filter(value => value !== '')
            for (let index = 0; index < arr.length - 1; index++) {
                dir += (arr[index] + fileSeparator);
                if (tmpObj.files[arr[index]] === undefined) {
                    tmpObj.files[arr[index]] = new DirObj(Object.assign({}, DefaultFolderData, {
                        folderKey: dir, name: arr[index]
                    }));
                }
                tmpObj = tmpObj.files[arr[index]];
            }
            if (tmpObj.files[arr[arr.length - 1]] === undefined) {
                if (file.type !== DefaultFolderData.type) {
                    tmpObj.files[arr[arr.length - 1]] = new FileObj(file);
                } else {
                    tmpObj.files[arr[arr.length - 1]] = new DirObj(file);
                }
            }
        } else {
            if (file.type !== DefaultFolderData.type) {
                displayObj.files[fileKey] = new FileObj(file);
            } else {
                displayObj.files[fileKey] = new DirObj(file);
            }
        }
    })
    return displayObj;
}

/**
 * 
 * @param {Object.<string, DefaultFileData>} fileNamesJson 
 * @param {*} projectId 
 * @param {*} projectName 
 * @param {*} userId 
 * @param {*} shareProjectId 
 * @param {*} treeSelectType default is dir
 */
export const getTreeData = function (fileNamesJson, projectId, projectName, userId, shareProjectId, treeSelectType = SelectType.dir) {
    const treeData = [];
    const map = new Map();
    const rootId = JSON.stringify({
        folderKey: "", projectId, userId, shareProjectId
    });
    treeData.push({
        id: rootId, pId: 0,
        value: rootId, title: <div> {!shareProjectId ? "" : <i className="f7 icon icon-xs fas fa-share-alt-square"></i>}{projectName}</div>,
        icon: <FolderOutlined />,
    })
    const displayObj = new DirObj(rootDir);
    _.map(fileNamesJson, (file, fileKey) => {
        if (~fileKey.indexOf(fileSeparator)) {
            let tmpObj = displayObj;
            let dir = tmpObj.dir.folderKey;
            let arr = fileKey.split(fileSeparator).filter(value => value !== '')
            for (let index = 0; index < arr.length - 1; index++) {
                let prevDir = dir;
                dir += (arr[index] + fileSeparator);
                if (tmpObj.files[arr[index]] === undefined) {
                    tmpObj.files[arr[index]] = new DirObj(Object.assign({}, DefaultFolderData, {
                        folderKey: dir, name: arr[index]
                    }));
                    const id = JSON.stringify({
                        folderKey: dir, projectId, userId, shareProjectId
                    });
                    const pId = JSON.stringify({
                        folderKey: prevDir, projectId, userId, shareProjectId
                    });
                    const data = {
                        id: id, pId: pId,
                        value: id, title: arr[index],
                        icon: <FolderOutlined />,
                    };
                    treeData.push(data);
                    if (map.has(pId)) {
                        map.delete(pId);
                        map.set(id, data);
                    } else {
                        map.set(id, data);
                    }
                }
                tmpObj = tmpObj.files[arr[index]];
            }
            if (tmpObj.files[arr[arr.length - 1]] === undefined) {
                if (file.type !== DefaultFolderData.type) {
                    tmpObj.files[arr[arr.length - 1]] = new FileObj(file);
                    if (treeSelectType === SelectType.file) {
                        const id = JSON.stringify({
                            fileKey: file.fileKey, projectId, userId, shareProjectId
                        });
                        const pId = JSON.stringify({
                            folderKey: dir, projectId, userId, shareProjectId
                        });
                        const data = {
                            id: id, pId: pId,
                            value: id, title: file.name, icon: <FileOutlined />,
                        };
                        treeData.push(data);
                        if (map.has(pId)) {
                            map.delete(pId);
                            map.set(id, data);
                        } else {
                            map.set(id, data);
                        }
                    }
                } else {
                    tmpObj.files[arr[arr.length - 1]] = new DirObj(file);
                    const id = JSON.stringify({
                        folderKey: file.folderKey, projectId, userId, shareProjectId
                    });
                    const pId = JSON.stringify({
                        folderKey: dir, projectId, userId, shareProjectId
                    });
                    const data = {
                        id: id, pId: pId,
                        value: id, title: file.name,
                        icon: <FolderOutlined />,
                    };
                    treeData.push(data);
                    if (map.has(pId)) {
                        map.delete(pId);
                        map.set(id, data);
                    } else {
                        map.set(id, data);
                    }
                }
            }
        } else {
            if (file.type !== DefaultFolderData.type) {
                displayObj.files[fileKey] = new FileObj(file);
                if (treeSelectType === SelectType.file) {
                    const id = JSON.stringify({
                        fileKey: file.fileKey, projectId, userId, shareProjectId
                    });
                    const pId = rootId;
                    const data = {
                        id: id, pId: pId,
                        value: id, title: file.name,
                        icon: <FileOutlined />,
                    };
                    treeData.push(data);
                    if (map.has(pId)) {
                        map.delete(pId);
                        map.set(id, data);
                    } else {
                        map.set(id, data);
                    }
                }
            } else {
                displayObj.files[fileKey] = new DirObj(file);
                const id = JSON.stringify({
                    folderKey: file.folderKey, projectId, userId, shareProjectId
                });
                const pId = rootId;
                const data = {
                    id: id, pId: pId,
                    value: id, title: file.name,
                    icon: <FolderOutlined />,
                };
                treeData.push(data);
                if (map.has(pId)) {
                    map.delete(pId);
                    map.set(id, data);
                } else {
                    map.set(id, data);
                }
            }
        }
    })
    // map.forEach((value, key) => {
    //     value.isLeaf = true;
    //     value.icon = <FolderOutlined />;
    // })
    return treeData;
}

/**dir always front, order by dictionary */
export const sortDirFileByKey = o => _.sortBy(Object.keys(o), [n => {
    if (o[n] instanceof DirObj) return (' ' + n.toLowerCase())
    else if (isFilePlain(o[n].file)) return '1' + n.toLowerCase();
    else if (isFileMd(o[n].file)) return 'a' + n.toLowerCase();
    else if (isFileJs(o[n].file)) return 'b' + n.toLowerCase();
    else if (isFileHtml(o[n].file)) return 'c' + n.toLowerCase();
    else if (isFileSvg(o[n].file)) return 'd' + n.toLowerCase();
    else if (isFileJson(o[n].file)) return 'e' + n.toLowerCase();
    else if (isFileCsv(o[n].file)) return 'f' + n.toLowerCase();
    else if (isFileImage(o[n].file)) return 'g' + n.toLowerCase();
    else if (isFileDb(o[n].file)) return 'h' + n.toLowerCase();
    else if (isFileExcel(o[n].file)) return 'i' + n.toLowerCase();
    else if (isFileArrow(o[n].file)) return 'j' + n.toLowerCase();
    else if (isFileAudio(o[n].file)) return 'k' + n.toLowerCase();
    else if (isFileVideo(o[n].file)) return 'l' + n.toLowerCase();
    else if (isFileGzip(o[n].file)) return 'm' + n.toLowerCase();
    else if (isFileZip(o[n].file)) return 'n' + n.toLowerCase();
    else if (isFileGraphxr(o[n].file)) return 'o' + n.toLowerCase();
    else return 'a' + o[n].type + n.toLowerCase();
}]).reduce((r, k) => (r[k] = o[k], r), {});

/**
 * 
 * @param {String} referFileKey reference file key
 * @param {String} fileName 
 * @returns {String} new file key
 */
export const getFileKey = (referFileKey, fileName) => {
    return (referFileKey && ~referFileKey.lastIndexOf(fileSeparator)) ?
        (referFileKey.substring(0, referFileKey.lastIndexOf(fileSeparator)) + fileSeparator + fileName) :
        fileName;
}
/**
 * 
 * @param {string} referFileKey 
 * @param {File} file 
 */
export const assembleFileData = (referFileKey, file) => {
    let tmpFileName = file.name;
    let tmpFileKey = (!file.path || file.path === file.name) ?
        getFileKey(referFileKey, file.name) :
        (file.path.startsWith(fileSeparator) ? file.path.substring(1) : file.path);
    tmpFileName = tmpFileName.endsWith(".grove") ? tmpFileName.substring(0, tmpFileName.length - ".grove".length) : tmpFileName
    tmpFileKey = tmpFileKey.endsWith(".grove") ? tmpFileKey.substring(0, tmpFileKey.length - ".grove".length) : tmpFileKey
    let type = transferType(file['type'] || getFileType(tmpFileKey));
    return Object.assign({}, DefaultFileData,
        { fileKey: tmpFileKey, name: tmpFileName, selected: false, type: type })
}

export const getFileType = (fileKey) => {
    let type;
    if (imageTypeReg.test(fileKey.toLowerCase())) {
        type = `image/${imageTypeReg.exec(fileKey.toLowerCase())[0].substring(1)}`
    } else if (videoTypeReg.test(fileKey.toLowerCase())) {
        type = `video/${videoTypeReg.exec(fileKey.toLowerCase())[0].substring(1)}`
    } else if (audioTypeReg.test(fileKey.toLowerCase())) {
        type = `audio/${audioTypeReg.exec(fileKey.toLowerCase())[0].substring(1)}`
    } else if (fileKey.toLowerCase().endsWith('.graphxr')) {
        return 'graphxr'
    } else {
        let fileName = getFileName(fileKey);
        type = (fileName && ~fileName.lastIndexOf('.')) ? `text/${fileName.substring(fileName.lastIndexOf('.') + 1)}` : DefaultFileType;
    }
    return type;
}

/**
 * 
 * @param {*} fileKey 
 * @returns {string}
 */
export const getFileName = (fileKey) => {
    return (fileKey && ~fileKey.lastIndexOf('/')) ? fileKey.substring(fileKey.lastIndexOf('/') + 1) : fileKey
}
/**
 * 
 * @param {String} referFileKey reference file key
 * @param {String} folderName 
 * @returns {String} new folder key
 */
export const getFolderKey = (referFileKey, folderName) => {
    return (referFileKey && ~referFileKey.lastIndexOf(fileSeparator)) ?
        (referFileKey.substring(0, referFileKey.lastIndexOf(fileSeparator)) + fileSeparator + folderName + fileSeparator) :
        folderName + fileSeparator;
}
/**
 * open dirs to file
 * @param {String} fileKey 
 */
export function openDirs(fileKey) {
    if (fileKey === null) {
        return;
    }
    let folderKeys = [""];
    let dir = "";
    let arr = (~fileKey.lastIndexOf(fileSeparator) ? fileKey.substring(0, fileKey.lastIndexOf(fileSeparator)) : fileKey)
        .split(fileSeparator).filter(value => value !== '');
    for (let index = 0; index < arr.length; index++) {
        folderKeys.push(dir += (arr[index] + fileSeparator));
    }
    document.querySelector(".folder").querySelectorAll("details").forEach((details) => {
        if (~folderKeys.indexOf(details.getAttribute("data-link"))) {
            details.setAttribute("open", "");
        }
    })
}

/**
 * @param {*} uploadUri 
 * @param {*} fileKey 
 * @param {number} version file version number, 0 is current version
 */
export function getUri(uploadUri, fileKey, version) {
    let fileAVerKey = !version ? fileKey : `${fileKey}${verFileTag}${fileSeparator}${version}`;
    return `${uploadUri}${fileAVerKey}`;
}

/**
 * get file content from storage/ImageCache/server
 * @param {*} uploadUri 
 * @param {*} cloud 
 * @param {DefaultFileData} file
 * @param {boolean} force force to fetch server file
 * @param {number} version file version number, 0 is current version
 * 
 * @returns blob uri string if image or string or
 *  throw exception if fileName not exist in cache or network exception
 */
export async function getMdFile(uploadUri, cloud, file, force, version) {
    let fileKey = file.fileKey;
    let uri = getUri(uploadUri, fileKey, version);
    if (!version && !force) {
        let item = getItem(getFileNamesKey() + '.' + fileKey);
        if (item !== undefined) {
            return item;
        }
    }
    if (uploadUri && cloud[fileKey] || isFileNoEdit(file)) {
        return await getContent(file, uri);
    } else {
        throw new Error(`Can not find the file! ${uri}`)
    }
}
/**
 * 
 * @param {DefaultFileData} file
 * 
 * @returns blob uri string if image or string or
 *  throw exception if fileName not exist in cache or network exception
 */
export async function getContent(file, uri) {
    if (isFileNoEdit(file) && ImageCache[uri]) {
        console.log("blob data in cache")
        return ImageCache[uri];
    }
    let response = await Common.fetch(uri);
    if (!response.ok) {
        throw new Error('Network response was not ok');
    }
    let content;
    if (isFileNoEdit(file)) {
        content = URL.createObjectURL(await response.blob());
        ImageCache[uri] = content;
    } else {
        content = await response.text();
    }
    return content;
}

/**
 * 
 * @param {Blob} blob 
 */
export async function readAsDataURL(blob) {
    if (!(blob instanceof Blob)) {
        return null;
    }
    return await new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onloadend = function (e) {
            resolve(e.target.result);
        };
        reader.onerror = function (e) {
            reject(e);
        };
        reader.readAsDataURL(blob);
    })
}

export async function getUserByKeyword(message, exitedEmail) {
    let res = await axios.post(`/api/user/getUserByKeyword`, { message: message, exitedEmail: exitedEmail });
    if (!res.data.status) {
        let arr = _.reduce(res.data.content, (arr, value, index) => {
            arr.push({ value: value.username })
            return arr;
        }, [])
        return arr[0];
    }
}
/**
 * get file content
 * @param {*} uploadUri 
 * @param {*} fileKey 
 */
export async function getFileContent(uploadUri, fileKey, version) {
    let response = await Common.fetch(`${uploadUri}${fileKey}${version ? `${verFileTag}${fileSeparator}${version}` : ""}`);
    if (!response.ok) {
        throw new Error('Network response was not ok');
    }
    let content;
    if (imageTypeReg.test(fileKey.toLowerCase())) {
        content = URL.createObjectURL(await response.blob());
        ImageCache[getUri(uploadUri, fileKey, version)] = content;
    } else {
        content = await response.text();
    }
    return content;
}

export function pasteScreenShot(processFunc) {
    // window.addEventListener('paste', ... or
    document.onpaste = function (event) {
        let items = (event.clipboardData || event.originalEvent.clipboardData).items;
        console.log(JSON.stringify(items)); // will give you the mime types
        for (let index in items) {
            let item = items[index];
            if (item.kind === 'file') {
                console.log(item);
                let blob = item.getAsFile();
                // let reader = new FileReader();
                // reader.onloadend = function (event) {
                //     console.log(event.target.result)
                // }; // data url!
                // reader.readAsDataURL(blob);
                processFunc && processFunc(blob, item.type);
            }
        }
    }
}

/**
 * 
 * @param {{
 * modalType:ModalType,
 * sourceFile:DefaultFileData|DefaultFolderData,
 * acceptedFiles:File[]}} props 
 */
export function Title(props) {
    const { modalType, sourceFile, acceptedFiles } = props;
    return <div><h4>{modalType}</h4>
        {sourceFile &&
            <div title={sourceFile.fileKey || sourceFile.folderKey} className="file-name-display normal-icon blablano"><i className={`icon ${getFileIcon(sourceFile)}`}></i>
                <span>{sourceFile.name || sourceFile.folderKey}</span>
            </div>
        }
        {acceptedFiles && _.map(acceptedFiles, (file) => {
            let fileData = assembleFileData("", file)
            return <div key={fileData.name} title={fileData.fileKey} className="file-name-display normal-icon blablano"><i className={`icon ${getFileIcon(fileData)}`}></i>
                <span>{fileData.fileKey}</span>
            </div>
        })}
    </div>
}
/**
 * 
 * @param {HTMLInputElement} startEle 
 * @param {boolean} toggle 
 */
export function loopParentChecked(startEle, toggle) {
    let detail = startEle.closest("details");
    if (!detail) {
        return;
    }
    let summaryInput = detail.querySelector("summary .form-check-input");
    if (toggle) {
        let checked = true;
        _.each(detail.querySelectorAll(".form-check-input"), (checkboxInput, index) => {
            if (checkboxInput !== summaryInput && !checkboxInput.checked) {
                checked = false;
                return false;
            }
        })
        summaryInput.checked = checked;
        if (checked) {
            return loopParentChecked(detail.parentNode, checked);
        }
    } else {
        summaryInput.checked = toggle;
        return loopParentChecked(detail.parentNode, toggle);
    }
}

export async function saveArrayToFile(variant, syncFileKey) {
    let content = ConvertToCSV(variant);
    console.log(`save ${content} to ${syncFileKey}`);
    let react_component = window.editor.react_component;
    if (!react_component.state.fileNamesJson[syncFileKey]) {
        await react_component.newFileFunc({ target: { value: getFileName(syncFileKey) } }, content, syncFileKey, false)
    } else {
        await react_component.uploadMdFile(react_component.state.fileNamesJson[syncFileKey], undefined, content);
    }
    return createLinkElement(syncFileKey);
}


/**
 * 
 * @param {TagEditor} editor 
 * @param {string} firstParam 
 * @param {string} value 
 */
export const replaceParam = function (editor, firstParam, value) {
    const { variables } = editor.getMain()._cachedata;
    let param = firstParam;
    if (variables[firstParam]) {
        for (let i = 1; i < 1000; i++) {
            param = `${firstParam}_${i}`;
            if (!variables[param]) {
                break;
            }
        }
    }
    return value.replace(/\{param\}/g, param.replace(/\$/g, "$$$$"));//in replaceValue two dollar instead of one dollar
}

/**
 * @param {TagEditor} editor 
 * @param {DefaultFileData} transItem  
 */
export function getFileAttachmentContent(editor, transItem) {
    let append = "";
    if (isFileImage(transItem)) {
        append = ".image()"
    } else if (isFileZip(transItem)) {
        append = ".zip()"
    } else if (isFileCsv(transItem)) {
        append = ".csv()"
    } else if (isFileJson(transItem)) {
        append = ".json()"
    } else if (isFileDb(transItem)) {
        append = ".sqlite()"
    } else if (isFileExcel(transItem)) {
        append = ".xlsx()"
    } else if (isFileArrow(transItem)) {
        append = ".arrow()"
    }
    return replaceParam(editor, transItem.name.toCommonName(), `{param} = FileAttachment(\"${transItem.name}\")${append}`);
}