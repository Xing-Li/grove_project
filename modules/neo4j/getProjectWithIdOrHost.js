const async = require("async");
const Neo4jDBModel = require("../neo4jDB/neo4jDB.model");
const CustomDB = require("./customDB.model");
const CryptoUtil = require("../util/cryptoUtil");
const ConfigDB = require("./configDB");

/**
 *  return Project JSON Struct, refer ConfigDB
 */
function getProjectWithIdOrHost(projectIdOrHost, needDecrypt) {
  return new Promise((reslove, reject) => {
    async.waterfall(
      [
        //1. disable the default db config
        function (callback) {
          callback(
            null,
            projectIdOrHost == "default"
              ? Object.assign({}, ConfigDB, {
                  hostname: "",
                })
              : null
          );
        },
        //2. read from customDB
        function (projectItem, callback) {
          if (!projectItem && /^[a-z0-9]{22,}$/.test(projectIdOrHost)) {
            CustomDB.getInfo(projectIdOrHost, (err, project) => {
              callback(null, err || !project ? null : Object.assign({}, JSON.parse(JSON.stringify(project))));
            });
          } else {
            callback(null, projectItem);
          }
        },
        //3. read from neo4jDB with host,
        //3.1 or read demo password and username when the project is demo
        function (projectItem, callback) {
          if ((!projectItem && (/.+:[0-9a-z]{2,}$/ig).test(projectIdOrHost)) || (projectItem && projectItem.isDemo)) {
            let idArr = projectIdOrHost.split(":");
            let hostname = projectItem ? projectItem.hostname : idArr[0];
            let boltPort = projectItem ? projectItem.boltPort : idArr[1];
            let currentNeo4jDB = projectItem ? projectItem.currentNeo4jDB : idArr[2];
            projectItem = projectItem || {};
            Neo4jDBModel.getDemoDB(hostname, boltPort, currentNeo4jDB, function (err, demoDB) {
              projectItem._id = projectIdOrHost;
              projectItem.hostname = hostname;
              projectItem.boltPort = boltPort;
              projectItem.projectName = projectItem.projectName || (demoDB ? demoDB.get("name") : projectIdOrHost);
              projectItem.username = demoDB ? demoDB.get("readAuth.user") : null;
              projectItem.password = demoDB ? demoDB.get("readAuth.pass") : null;
              projectItem.currentNeo4jDB = currentNeo4jDB;
              callback(err, projectItem);
            });
          } else if (!projectItem) {
            callback(new Error("Can't read the project with :", projectIdOrHost));
          } else {
            callback(null, projectItem);
          }
        },
      ],
      (err, projectItem) => {
        if (needDecrypt && projectItem && projectItem.username && projectItem.password) {
          projectItem.username = CryptoUtil.decrypt(projectItem.username);
          projectItem.password = CryptoUtil.decrypt(projectItem.password);
        }

        if (err) {
          reject(err);
        } else {
          reslove(projectItem);
        }
      }
    );
  });
}

module.exports = getProjectWithIdOrHost;
