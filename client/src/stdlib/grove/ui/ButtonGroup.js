import { Div } from "./Div.js";
import { groveComponent } from "./groveComponent.js";
import classNames from 'classnames'

export const ButtonGroup = groveComponent(({ className, children }) =>
  Div({
    className: classNames(`btn-group`, className),
  }, children.map((button) => {
    button.className = `btn btn-outline-primary`;
    return button;
  })))