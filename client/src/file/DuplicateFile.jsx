import { Button, Select, TreeSelect, Input, Row, Col, Form, Checkbox } from 'antd';
import { FolderOutlined } from '@ant-design/icons';
import Text from 'antd/lib/typography/Text';
import axios from 'axios';
import _ from 'lodash';
import PropTypes from "prop-types";
import React from 'react';
import editor from '../commonEditor.js';
import { InitSettings } from '../editorSettings';
import MdFiles from './MdFiles';
import Modal from 'antd/lib/modal/Modal';
import { WIDTH, Z_INDEX, Title, assembleFileData, getTreeData, fileNameReg, readAsDataURL, fileSeparator, getFolderKey, getFileKey } from './fileUtils.js';
import { ModalType, showConfirm, showToast } from '../util/utils.js';
import { DefaultOptions, USER_ID } from '../util/localstorage.js';
import { isFileGzip, isFile, isDir, isFileZip } from '../util/helpers.js';

const { Option } = Select;
export default class DuplicateFile extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        width: PropTypes.number,
        react_component: PropTypes.object,
        refreshType: PropTypes.number,

    };
    static defaultProps = {};

    constructor(props) {
        super(props);
        this.state = {
            /**
             * @type {{ projectId, userId, folderKey: dir,
             * name: arr[index], shareProjectId }}
            */
            value: undefined,
            treeData: undefined,
            newName: undefined,
            decompression: true,
            /**@type Object.<string,{
                userId, projectName, projectId,shareProjectId, fileNamesJson
            }> */
            cache: {},
        }
        this.formRef = React.createRef();
    }

    onChange = value => {
        console.log(value);
        this.setState({ value: value });
    };

    componentDidMount() {
    }

    refreshTreeData = async () => {
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
        const { fileNamesJson, projectId, userId, shareProjectId, sourceFile, modalType, modalData: { folderKey } } = react_component.state;
        let onlyCurrentProject = modalType === ModalType.ImportFiles ? true : false;
        let cache = {};
        let newName = sourceFile ? sourceFile.name : "Not Need Name";
        let options = _.cloneDeep(DefaultOptions());
        let currentDataFunc = () => {
            const { fileNamesJson, projectId, userId, shareProjectId } = react_component.state;
            cache[projectId] = {
                userId, projectName: react_component.props.projectName, projectId,
                shareProjectId, fileNamesJson
            }
            return getTreeData(fileNamesJson, projectId, react_component.props.projectName, userId, shareProjectId);
        }
        let treeData = currentDataFunc();
        let defaultValue = (treeData.length > 0 ? (
            folderKey && JSON.stringify({
                folderKey: folderKey, projectId, userId, shareProjectId
            }) || treeData[0].id) : undefined);
        if (!onlyCurrentProject) {
            let leftOptions = _.filter(options, (value, index) => {
                return value.projectId !== projectId;
            })
            for (let index = 0; index < leftOptions.length; index++) {
                let option = leftOptions[index];
                const { userId, projectName, projectId } = option;
                let res = await axios.post(`/api/grove/load`, {
                    userId: userId, settings: InitSettings, projectId: projectId
                });
                let fileNamesJson = res.data.content;
                let shareProjectId = userId === USER_ID ? undefined : projectId;
                treeData.push(...getTreeData(fileNamesJson, projectId, projectName, userId, shareProjectId));
                cache[projectId] = {
                    userId, projectName, projectId,
                    shareProjectId, fileNamesJson
                }
            }
        }
        this.setState({
            treeData,
            value: defaultValue,
            newName,
            cache,
        }, () => {
            this.formRef.current.setFieldsValue(this.state);
            console.log("refresh data")
        });

    }

    componentDidUpdate(prevProps, prevState) {
        if ((prevProps.refreshType !== this.props.refreshType) && (this.props.refreshType === 1)) {
            this.refreshTreeData();
        }
    }

    handleOk = async () => {
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
        const { modalType } = react_component.state;
        react_component.setWrapperLoading(true);
        if (modalType === ModalType.ImportFiles) {
            await this.importFiles();
        } else if (modalType === ModalType.Fork) {
            await this.fork();
        } else if (modalType === ModalType.DuplicateFile) {
            await this.duplicateFile();
        }
        react_component.setWrapperLoading(false);
    };

    duplicateFile = async () => {
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
        const { sourceFile, fileVersion, modalType } = react_component.state;
        const { newName, cache } = this.state;
        const { projectId, userId, shareProjectId, folderKey } = JSON.parse(this.state.value);
        const currentProjectId = react_component.state.projectId;
        const currentUserId = react_component.state.userId;
        const currentShareProjectId = react_component.state.shareProjectId;
        const state = { modalType: ModalType.None };
        if (isFile(sourceFile)) {
            let content = await react_component.getRealFileContent(sourceFile, fileVersion);
            let newFileKey = `${folderKey}${newName}`;
            if (projectId === currentProjectId) {
                if (newFileKey === sourceFile.fileKey) {
                    showToast("Can not overwrite itself.", 'error')
                    return;
                }
                const { fileNamesJson } = react_component.state;
                if (fileNamesJson[`${folderKey}${newName}${fileSeparator}`]) {
                    showToast("The specified file name is the same as an existing folder name. Please specify a different name.", 'error')
                    return;
                }
                if (fileNamesJson[newFileKey] && !await showConfirm(`File exists, Overwrite ${newFileKey}?`)) {
                    return;
                }
                if (await react_component.uploadMdFile(_.assign(_.cloneDeep(sourceFile), {
                    fileKey: newFileKey,
                    name: newName
                }), undefined, content, state, fileVersion, false)) {
                    showToast("Copy file successful", "success");
                }
            } else if (cache[projectId]) {
                const { fileNamesJson } = cache[projectId];
                if (fileNamesJson[newFileKey] && !await showConfirm(`File exists, Overwrite ${newFileKey}?`)) {
                    return;
                }
                await react_component.copyFile(_.assign(_.cloneDeep(sourceFile), {
                    fileKey: newFileKey,
                    name: newName
                }), userId, projectId, shareProjectId,
                    undefined, content, fileVersion, false);
            }
        } else if (isDir(sourceFile)) {
            let newFolderKey = `${folderKey}${newName}${fileSeparator}`;
            if (projectId === currentProjectId) {
                if (newFolderKey === sourceFile.folderKey) {
                    showToast("Can not overwrite itself.", 'error')
                    return;
                }
                const { fileNamesJson } = react_component.state;
                if (fileNamesJson[`${folderKey}${newName}`]) {
                    showToast("The specified folder name is the same as an existing file name. Please specify a different name!", 'error')
                    return;
                }
                if (fileNamesJson[newFolderKey] && !await showConfirm(`Folder exists, Overwrite ${newFolderKey}?`)) {
                    return;
                }
                await react_component.copyFolder(sourceFile.folderKey, currentShareProjectId, currentUserId, currentProjectId,
                    newFolderKey, currentShareProjectId, currentUserId, currentProjectId, undefined, state);
            } else if (cache[projectId]) {
                const { fileNamesJson, projectName, userId, shareProjectId } = cache[projectId];
                if (fileNamesJson[newFolderKey] && !await showConfirm(`Folder exists, Overwrite ${newFolderKey}?`)) {
                    return;
                }
                await react_component.copyFolder(sourceFile.folderKey, currentShareProjectId, currentUserId, currentProjectId,
                    newFolderKey, shareProjectId, userId, projectId, undefined, state);
            }
        }
        react_component.setState(state, () => {
        });
    }

    importFiles = async () => {
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
        const { modalData: { cb } } = react_component.state;
        const { newName, cache, decompression } = this.state;
        const { projectId, userId, shareProjectId, folderKey } = JSON.parse(this.state.value);
        const state = { modalType: ModalType.None };
        const currentProjectId = react_component.state.projectId;
        if (projectId === currentProjectId) {
            cb && cb(folderKey, decompression);
        }
        react_component.setState(state);
    }

    fork = async () => {
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
        const { sourceFile, modalData: { content, files } } = react_component.state;
        const { newName, cache } = this.state;
        const { projectId, userId, shareProjectId, folderKey } = JSON.parse(this.state.value);
        const state = { modalType: ModalType.None };
        const currentProjectId = react_component.state.projectId;
        const renameMainFileKey = sourceFile.name !== newName ? getFileKey(sourceFile.fileKey, newName) : undefined;
        const fileNamesJson = cache[projectId].fileNamesJson;
        let existFiles = [];//files already in project
        files.forEach((fileKey) => {
            if (fileKey === sourceFile.fileKey && renameMainFileKey) {
                if (fileNamesJson[`${folderKey}${renameMainFileKey}`]) {
                    existFiles.push(`${folderKey}${renameMainFileKey}`)
                }
            } else if (fileNamesJson[`${folderKey}${fileKey}`]) {
                existFiles.push(`${folderKey}${fileKey}`)
            }
        })
        if (existFiles.length && !await showConfirm(<div>Processing fork, Overwrite exist files:
            {_.map(existFiles, (v, index) => {
                return <div key={v}>{v}</div>
            })}?</div>)) {
            return;
        }
        let res = await axios.post(`/api/grove/fork`, _.assign({}, sourceFile, {
            mainFileKey: sourceFile.fileKey,
            renameMainFileKey: renameMainFileKey,
            fileKeys: Array.from(files),
            content,
            toUserId: userId,
            toShareProjectId: shareProjectId,
            toProjectId: projectId,
            toFolderKey: folderKey,//fork to which project
        }));
        if (projectId === currentProjectId && !res.data.status) {
            await react_component.refreshFiles(res.data.content, `${folderKey}${renameMainFileKey || sourceFile.fileKey}`);
        }
        res.data.message && showToast(res.data.message, res.data.status ? "error" : "info");
        react_component.setState(state, () => {
            react_component.switchToPage(`${folderKey}${renameMainFileKey || sourceFile.fileKey}`, content);
        });
    }


    render() {
        const { className, width } = this.props;
        const { treeData, value, newName, cache } = this.state;
        if (!treeData) {
            return <div></div>;
        }
        const { projectId, userId, shareProjectId, folderKey, name } = JSON.parse(this.state.value);
        let projectName = cache[projectId].projectName;
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
        const { modalType, sourceFile, modalData: { acceptedFiles } } = react_component.state;
        const closeFunc = () => {
            react_component.setState({
                modalType: ModalType.None,
            });
        };
        const handleType = sourceFile ? (isFile(sourceFile) ? "File" : "Folder") : "";
        const onFinish = (values) => {
            console.log('Success:', values);
            this.handleOk();
            closeFunc();
        };
        const onFinishFailed = (errorInfo) => {
            showToast(_.reduce(errorInfo.errorFields, (prev, curr, index) => {
                prev += curr.errors.join(",")
                return prev + "; ";
            }, ""), 'error')
            console.log('Failed:', errorInfo);
        };
        let zips = acceptedFiles ? _.filter(acceptedFiles, /**@param {File} file*/(file) => {
            let fileData = assembleFileData("", file);
            return isFileGzip(fileData) || isFileZip(fileData);
        }) : [];
        return <Modal zIndex={Z_INDEX}
            className={`modal-comp data-html2canvas-ignore ${className}`}
            width={width || WIDTH}
            title={<Title modalType={`${modalType} ${handleType}`} acceptedFiles={acceptedFiles} sourceFile={sourceFile}></Title>}
            visible={~[ModalType.DuplicateFile, ModalType.Fork, ModalType.ImportFiles].indexOf(modalType)}
            onCancel={closeFunc}
            footer={null}
        >
            <Form layout={"horizontal"}
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 18 }}
                ref={this.formRef}
                initialValues={this.state}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <Form.Item label="Place Folder" rules={[
                    {
                        required: true,
                    },
                ]}>
                    <TreeSelect
                        treeDataSimpleMode
                        treeLine
                        value={value}
                        dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                        placeholder="Please Select Folder To Place"
                        onChange={this.onChange}
                        // loadData={this.onLoadData}
                        treeData={treeData}
                        icon={<FolderOutlined />}
                    />
                    <div className="d-flex flex-column">
                        <div>Project: {projectName}</div>
                        <div>Folder: {folderKey}</div>
                    </div>
                </Form.Item>
                {ModalType.ImportFiles !== modalType && <Form.Item label={`${handleType} Name`} name="newName" rules={[
                    {
                        required: true,
                    },
                ]}>
                    <Input onChange={(e) => {
                        if (!e.target.value.match(fileNameReg)) {
                            e.target.value = newName;
                            return;
                        }
                        this.setState({ newName: e.target.value });
                    }} />
                </Form.Item>}
                {zips.length > 0 && ModalType.ImportFiles === modalType && <Form.Item name="decompression" valuePropName="checked" wrapperCol={{ offset: 6, span: 18 }} rules={[
                    {
                        required: true,
                    },
                ]}>
                    <Checkbox onChange={(e) => {
                        this.setState({ decompression: e.target.checked });
                    }}>
                        <div>Decompression compressed files</div>
                    </Checkbox>
                </Form.Item>}
                <Form.Item key="back"  >
                    <div className="d-flex justify-content-end">
                        <Button key="back" onClick={closeFunc}>
                            Cancel
                        </Button>&nbsp;&nbsp;
                        <Button key="addNew" disabled={!value || !newName} type="primary" htmlType="submit">
                            Upload
                        </Button>
                    </div>
                </Form.Item>
            </Form>
        </Modal>
    }
}
