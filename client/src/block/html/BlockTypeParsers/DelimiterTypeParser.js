export function parseDelimiterToHtml() {
  return `<hr>`;
}

export function parseMarkdownToDelimiter() {
  let delimiterData = {};

  delimiterData = {
    data: {
      items: [],
    },
    type: 'delimiter',
  };

  return delimiterData;
}
