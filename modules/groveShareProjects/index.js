'use strict';

const express = require('express');
const _ = require('lodash');
const auth = require('./../util/auth');

const GroveShareProjects = require('./GroveShareProjects.model');


let router = express.Router();

router.all('/addShareProject', auth.protected, function (req, res) {
    let addInfo = {
        userId: req.user.id,
        projectId: req.parameters.addInfo.projectId,
        projectName: req.parameters.addInfo.projectName,
        fileKey: req.parameters.addInfo.fileKey,
        status: req.parameters.addInfo.status
    }
    GroveShareProjects.addShareProject(addInfo, function (err) {
        if (err) {
            console.error(err);
        }
        return res.send({ 
            status: err ? 1 : 0,
            message: err ? "tips.failed": "tips.successful",
        });
    });
});

router.all('/getAllShareProjects', auth.protected, function (req, res) {
    GroveShareProjects.getAllShareProjects(function (err, shareProjects) {
        if (err) {
            console.error(err);
        }
        return res.send({ 
            status: err ? 1 : 0, 
            message: err ? "tips.failed": "tips.successful",
            content: shareProjects
        });
    });
});

router.all('/deleteShareProjectById', auth.protected, function (req, res) {
    let id = req.parameters.id
    GroveShareProjects.deleteShareProjectById(id, function (err) {
        if (err) {
            console.error(err);
        }
        return res.send({ 
            status: err ? 1 : 0,
            message: err ? "tips.failed": "tips.successful"
        });
    });
});


module.exports = router;