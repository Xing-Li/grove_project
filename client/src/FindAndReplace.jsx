import { Affix, Checkbox, Collapse, Input } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import { ReplaceIcon, ReplaceAllIcon, ReplaceCurrentIcon } from "./block/code/Icon";
import PropTypes from "prop-types";
import React from 'react';
import { isInViewport, maxC, SearchType, sliceC } from './util/helpers';
import EditorAbs from "./editorAbs";
import { getCodeModeIcon } from 'util/utils';
import { cmSearchFunc, cmHighlightFunc, cmReplaceFunc } from './util/CodeMirror.showHint'
import actions from 'define/Actions';

export default class FindAndReplace extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        editor: PropTypes.object,
    };
    static defaultProps = {};

    constructor(props) {
        super(props);
        /**@type {EditorAbs} */
        const editor = this.props.editor;
        this.state = {
            className: this.props.className,
            editor,
            tags: editor.tagSearch(),
            searchText: editor.getSearchText(),
            replaceValue: "",
            caseSensitive: false,
            wholeWord: false,
            regular: false,
            selectIndex: -1, subIndex: -1,
        }
        this.inputRef = React.createRef();
        this.openCodeElement = document.createElement("div");
    }

    refreshTags = (tags, cb) => {
        this.setState({ tags: tags }, cb);
    }

    componentDidMount() {
        if (this.inputRef.current) {
            setTimeout(() => this.inputRef.current.focus({ cursor: 'all' }));
        }
        actions.inspector(this.openCodeElement, [actions.types.OPEN_CODE],
            ({ type, data, ret }) => {
                const { tags } = this.state;
                let findTag = _.filter(tags, (tag) => { return tag.ele === data })
                if (findTag.length === 1) {
                    // alert(data)
                    findTag[0].type = type;
                    findTag[0].ret = ret;
                    this.refreshTags(tags, () => {
                        const { selectIndex, subIndex } = this.state;
                        if (findTag[0] === tags[selectIndex] && findTag[0].ret[subIndex]) {
                            if (findTag[0].type === SearchType.cm) {
                                const { cm, from, to } = findTag[0].ret[subIndex];
                                cm.scrollIntoView({ from: from, to: to }, 20)
                                cmHighlightFunc.call(cm, cm, from, to)
                            } else {
                                this.setState({ subIndex: -1 });
                            }
                        }
                    });
                }
            })
    }

    componentWillUnmount() {
        actions.deleteCache(this.openCodeElement);
    }

    componentDidUpdate(preProps, preState) {
        let state = {};
        if (preProps.className !== this.props.className) {
            _.assign(state, { className: this.props.className })
        }
        if (preProps.editor !== this.props.editor) {
            _.assign(state, {
                editor: this.props.editor,
            })
        }
        if (preProps.drawRefreshs !== this.props.drawRefreshs || preProps.info !== this.props.info || preProps.editor !== this.props.editor) {
            _.assign(state, {
                tags: this.props.editor.tagSearch(),
                searchText: this.props.editor.getSearchText(),
                selectIndex: -1, subIndex: -1,
            })
        }
        !_.isEmpty(state) && this.setState(state);
    }

    TagsComp = () => {
        const { className, editor, tags, caseSensitive, wholeWord, regular, selectIndex, subIndex, searchText } = this.state;
        let arr = _.map(tags, (tag, index) => {
            const { ele, ret, type, name } = tag;
            if (SearchType.cm === type) {
                let codeEle = ele.closest(".code-edit");
                return (<details className={index === selectIndex ? "selected" : ""} open key={`cell_${index}`}/*  onClick={() => {
                    if (!codeEle) { return; }
                    if (!isInViewport(codeEle)) {
                        codeEle.scrollIntoView({ behavior: "auto", block: "center", inline: "nearest" });
                    }
                    this.setState(state => { return state.selectIndex !== index ? { selectIndex: index, subIndex: -1 } : {} })
                }} */>
                    <summary><i className={`icon ${getCodeModeIcon(codeEle.state.codeMode)}`}></i>&nbsp;
                        {ele.cacheNames && ele.cacheNames.join(" ")}(#{index})</summary>
                    {_.map(ret, (cell, sindex) => {
                        const { line, from, to, cm } = cell;
                        return <div key={`line_${from.line}_ch_${from.ch}`} className={sindex === this.state.subIndex ? "sub-selected" : ""} onClick={() => {
                            cm.scrollIntoView({ from: from, to: to }, 20)
                            cmHighlightFunc.call(cm, cm, from, to)
                            this.setState({ selectIndex: index, subIndex: sindex })
                        }}>
                            <span>{line.substring(from.ch > maxC ? from.ch - sliceC : 0, from.ch)}</span>
                            <span className={"highlight-warning"}>{line.substring(from.ch, to.ch)}</span>
                            <span>{line.substring(to.ch, line.length - to.ch > maxC ? to.ch + sliceC : line.length)}</span>
                        </div>
                    })}
                </details>);
            } else if (SearchType.nocm === type) {
                let codeEle = ele.closest(".code-edit");
                return (<details className={index === selectIndex ? "selected" : ""} open key={`cell_${index}`} onClick={() => {
                    if (!codeEle) { return; }
                    if (!isInViewport(codeEle)) {
                        codeEle.scrollIntoView({ behavior: "auto", block: "center", inline: "nearest" });
                    }
                    codeEle.editCode && codeEle.editCode(true);
                    this.setState({ selectIndex: index, subIndex: -1 })
                }}>
                    <summary><i className={`icon-muted ${getCodeModeIcon(codeEle.state.codeMode)}`}></i>&nbsp;
                        {ele.cacheNames && ele.cacheNames.join(" ")}(#{index})</summary>
                    {ret}
                </details>);
            } else if (SearchType.others === type) {
                const config = editor.editorInstance.tools.getAllToolsConfig(name);
                return (<details className={index === selectIndex ? "selected" : ""} open key={`cell_${index}`} onClick={() => {
                    if (!ele) { return; }
                    if (!isInViewport(ele)) {
                        ele.scrollIntoView({ behavior: "auto", block: "center", inline: "nearest" });
                    }
                    ele.focus();
                    this.setState({ selectIndex: index, subIndex: -1 })
                }}>
                    <summary><i title={config.title} className={"icon-muted"} dangerouslySetInnerHTML={{ __html: config.icon }} />&nbsp;
                        (#{index})</summary>
                    {ret}
                </details>);
            }
        })
        return arr.length ? arr : <div>No results{searchText ? " for " + searchText : ""}.</div>;
    }

    render() {
        const { className, editor, tags, selectIndex, subIndex, caseSensitive, wholeWord, regular, searchText, replaceValue } = this.state;
        let canReplaceAll = tags[selectIndex] && tags[selectIndex].type === SearchType.cm && tags[selectIndex].ret.length > 0;
        let canReplaceCurrent = canReplaceAll && subIndex >= 0;
        return <div className={`find-and-replace ${className || ''}`}>
            <Affix className={'folder-affix'} target={() => document.querySelector(".drawer-div .ant-drawer-body")} offsetTop={0}>
                <div>
                    <div className="d-flex align-items-center">
                        <Input style={{ maxWidth: "200px" }} value={searchText} placeholder="Find" allowClear prefix={<SearchOutlined className="icon-muted" />}
                            ref={this.inputRef}
                            onKeyUp={(e) => {
                                if (~["Enter"].indexOf(e.key) && searchText === e.target.value) {
                                    editor.setSearch(e.target.value, caseSensitive, wholeWord, regular);
                                    this.setState({
                                        tags: editor.tagSearch(),
                                        searchText: editor.getSearchText(),
                                        selectIndex: -1, subIndex: -1,
                                    })
                                }
                            }}
                            onBlur={(e) => {
                                this.setState({
                                    tags: editor.tagSearch(),
                                    selectIndex: -1, subIndex: -1,
                                })
                            }}
                            onChange={(e) => {
                                if (searchText !== e.target.value) {
                                    editor.setSearch(e.target.value, caseSensitive, wholeWord, regular);
                                    this.setState({
                                        searchText: editor.getSearchText(),
                                    })
                                }
                            }}></Input>
                        <i title="Previous search result" className="icon-bordered icon fas fa-chevron-up" onClick={() => {
                            if (tags[selectIndex] && tags[selectIndex].type === SearchType.cm && subIndex > 0) {
                                this.setState((state) => { return { subIndex: state.subIndex - 1 } }, () => {
                                    const { tags, selectIndex, subIndex } = this.state;
                                    const { line, from, to, cm } = tags[selectIndex].ret[subIndex];
                                    cm.scrollIntoView({ from: from, to: to }, 20)
                                    cmHighlightFunc.call(cm, cm, from, to)
                                })
                            } else {
                                this.setState((state) => { return (state.selectIndex > 0) ? { selectIndex: state.selectIndex - 1, subIndex: -1 } : {} }, () => {
                                    const { tags, selectIndex } = this.state;
                                    const { ele } = tags[selectIndex] || {}
                                    if (!ele) { return; }
                                    if (!isInViewport(ele)) {
                                        ele.scrollIntoView({ behavior: "auto", block: "center", inline: "nearest" });
                                    }
                                })
                            }
                        }} ></i>
                        <i title="Next search result" className="icon-bordered icon fas fa-chevron-down" onClick={() => {
                            if (tags[selectIndex] && tags[selectIndex].type === SearchType.cm && tags[selectIndex].ret.length - 1 > subIndex) {
                                this.setState((state) => { return { subIndex: state.subIndex + 1 } }, () => {
                                    const { tags, selectIndex, subIndex } = this.state;
                                    const { line, from, to, cm } = tags[selectIndex].ret[subIndex];
                                    cm.scrollIntoView({ from: from, to: to }, 20)
                                    cmHighlightFunc.call(cm, cm, from, to)
                                })
                            } else {
                                this.setState((state) => { return (state.tags.length - 1 > state.selectIndex) ? { selectIndex: state.selectIndex + 1, subIndex: -1 } : {} }, () => {
                                    const { tags, selectIndex } = this.state;
                                    const { ele } = tags[selectIndex] || {}
                                    if (!ele) { return; }
                                    if (!isInViewport(ele)) {
                                        ele.scrollIntoView({ behavior: "auto", block: "center", inline: "nearest" });
                                    }
                                })
                            }
                        }} ></i>
                    </div>
                    {!editor.getReadOnly() && <details>
                        <summary> <b>Replace </b></summary>
                        <div className="d-flex align-items-center">
                            <Input style={{ maxWidth: "200px" }} value={replaceValue} placeholder="Replace" allowClear prefix={<ReplaceIcon className="icon-muted" />}
                                onChange={(e) => {
                                    this.setState({ replaceValue: e.target.value });
                                }}></Input>
                            <ReplaceCurrentIcon title="Replace current" disabled={!canReplaceCurrent} className={`icon-bordered icon`} onClick={() => {
                                const { tags, selectIndex, subIndex, replaceValue } = this.state;
                                if (tags[selectIndex] && tags[selectIndex].type === SearchType.cm && subIndex >= 0) {
                                    const { tags, selectIndex, subIndex } = this.state;
                                    const { line, from, to, cm } = tags[selectIndex].ret[subIndex];
                                    cm.replaceRange(replaceValue, from, to, '+input');
                                }
                            }} />
                            <ReplaceAllIcon title="Replace all in current block" disabled={!canReplaceAll} className={`icon-bordered icon`} onClick={() => {
                                const { tags, selectIndex, replaceValue } = this.state;
                                if (tags[selectIndex] && tags[selectIndex].type === SearchType.cm && tags[selectIndex].ret.length > 0) {
                                    const { tags, selectIndex } = this.state;
                                    const { line, from, to, cm } = tags[selectIndex].ret[0];
                                    setTimeout(function () { cmReplaceFunc.call(cm, cm, editor.getSearchText(true), editor.isWholeWord(), replaceValue) });
                                }
                            }} />
                        </div>
                    </details>}
                    <details>
                        <summary> <b>Settings</b> </summary>
                        <div>
                            <div>
                                <Checkbox onChange={(e) => {
                                    this.setState({ caseSensitive: e.target.checked }, () => {
                                        const { caseSensitive, wholeWord, regular } = this.state;
                                        editor.setSearch(editor.getSearchText(), caseSensitive, wholeWord, regular);
                                        this.setState({
                                            tags: editor.tagSearch(),
                                            searchText: editor.getSearchText(),
                                            selectIndex: -1, subIndex: -1,
                                        })
                                    })
                                }} checked={caseSensitive}>Case Sensitive</Checkbox>
                            </div>
                            <div>
                                <Checkbox onChange={(e) => {
                                    this.setState({ wholeWord: e.target.checked }, () => {
                                        const { caseSensitive, wholeWord, regular } = this.state;
                                        editor.setSearch(editor.getSearchText(), caseSensitive, wholeWord, regular);
                                        this.setState({
                                            tags: editor.tagSearch(),
                                            searchText: editor.getSearchText(),
                                            selectIndex: -1, subIndex: -1,
                                        })
                                    })
                                }} checked={wholeWord}>Whole Word</Checkbox>
                            </div>
                            <div>
                                <Checkbox onChange={(e) => {
                                    this.setState({ regular: e.target.checked }, () => {
                                        const { caseSensitive, wholeWord, regular } = this.state;
                                        editor.setSearch(editor.getSearchText(), caseSensitive, wholeWord, regular);
                                        this.setState({
                                            tags: editor.tagSearch(),
                                            searchText: editor.getSearchText(),
                                            selectIndex: -1, subIndex: -1,
                                        })
                                    })
                                }} checked={regular}>Regular Expression</Checkbox>
                            </div>

                        </div>
                    </details>
                </div>
            </Affix>
            {this.TagsComp()}
        </div>
    }
}