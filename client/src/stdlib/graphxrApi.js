/**
 * When ExInspector detects GraphXR API in a cell variable, it stores it in this module.
 * The search modal (GraphxrApiSearchModal) uses this module to get the loaded version.
 */

import { requireModuleUsingGlobalLeaksPattern } from "./requireModuleUsingGlobalLeaksPattern";
import { require as requireDefault, requireFrom, resolveBase, resolveAnywhere } from "./grove-require";
const requireModule = requireFrom(resolveAnywhere);

/**
 * This variable stores a list of callbacks to be called when the GraphXR API is loaded.
 */
const listeners = [];

/**
 * This variable stores an instance of GraphXR API.
 */
export let graphxrApi;

export function setGraphxrApi(api) {
  graphxrApi = api;
  listeners.forEach((listener) => listener(api));
}

export function getGraphxrApiVersion() {
  return graphxrApi ? graphxrApi._version : undefined;
}

export const getApi = async (version) => {
  console.log("getApi called with version", version)
  let path = "@kineviz/graphxr-api"
  if (version) {
    path += "@" + version
  }

  console.log("Loading GraphXR API from", path)
  return (await requireModule(path)).getApi();
}

/**
 * Make a link to the GraphXR API documentation for a given symbol and version. 
 */
export function getDocUrl(symbol, graphxrApiVersion) {
  const version = graphxrApiVersion || getGraphxrApiVersion();
  const path = symbol ? `#${symbol}` : "";
  return version
    ? `https://kineviz.github.io/graphxr-api-docs/${version}/modules.html${path}`
    : `https://kineviz.github.io/graphxr-api-docs/modules.html${path}`;
}

/**
 * This is how you hook this module into the React component lifecycle.
 * e.g.
 * executeOnLoad(() => {
 *   this.setState({
 *     version: graphxrApi.getGraphxrApiVersion()
 *   })
 * })
 */
export function executeOnLoad(fn) {
  if (graphxrApi) {
    fn(graphxrApi);
  } else {
    listeners.push(fn);
  }
}
