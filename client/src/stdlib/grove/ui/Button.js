import html from "@observablehq/stdlib/src/html.js";
import { groveComponent } from "./groveComponent";

export const Button = groveComponent(({ className = "btn-primary", children, label, onClick, tooltip }) => {
  const button = html`<button class="btn ${className}" type="button" title="${tooltip}"></button>`;
  children.forEach(child => button.appendChild(child))
  if (label)
    button.appendChild(html`${label}`)
  button.onclick = onClick;
  return button;
})