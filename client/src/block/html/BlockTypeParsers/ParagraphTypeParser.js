import { FILESDIR } from "../../../util/localstorage";

export function parseParagraphToHtml(blocks) {
  return `${blocks.text}\n`;
}

export function parseTableToMarkdown(blocks) {
  let table = _.reduce(blocks.content, (prev, currArr, index) => {
    prev.push(`| ${currArr.join(" | ")} |`);
    if (index === 0) {
      prev.push(`| ${_.map(currArr, (value, index) => {
        return "----"
      }).join(" | ")} |`);
    }
    return prev;
  }, []).join("\n");
  return `${table}\n`;
}

export function parseMarkdownToParagraph(blocks) {
  let paragraphData = {};

  if (blocks.type === 'paragraph') {
    blocks.children.forEach((item) => {
      if (item.type === 'text') {
        if (item.value.startsWith("|") && item.value.endsWith("|")) {
          let lines = item.value.split("\n");
          let content = _.reduce(lines, (prev, curr, index) => {
            if (index !== 1) {
              let arr = curr.split("|");
              arr = arr.slice(1, arr.length - 1);
              prev.push(_.reduce(arr, (p, c) => {
                p.push(c.trim());
                return p;
              }, []))
            }
            return prev;
          }, []);
          paragraphData = {
            "type": "table",
            "data": {
              "content": content
            }
          };
        } else {
          paragraphData = {
            data: {
              text: item.value,
            },
            type: 'paragraph',
          };
        }
      }
      if (item.type === 'image') {
        paragraphData = {
          data: {
            caption: item.title,
            stretched: false,
            url: item.url.startsWith(FILESDIR) ? item.url.substring(FILESDIR.length) : item.url,
            withBackground: false,
            withBorder: false,
          },
          type: 'simpleImage',
        };
      }
    });
  }
  return paragraphData;
}
