import arrayBuffer from "@observablehq/stdlib/src/files/buffer.js";
import text from "@observablehq/stdlib/src/files/text.js";
import url from "@observablehq/stdlib/src/files/url.js";
import { require as requireDefault } from "./grove-require";
import sqlite, { SQLiteDatabaseClient } from "./sqlite.js";

export default {
    buffer: arrayBuffer,
    text: text,
    url: url,
    sqlite: sqliteClient
};

async function sqliteClient(file) {
    const [SQL, buffer] = await Promise.all([sqlite(requireDefault), await arrayBuffer(file)]);
    const db = new SQL.Database(new Uint8Array(buffer));
    return new SQLiteDatabaseClient(db);
}
