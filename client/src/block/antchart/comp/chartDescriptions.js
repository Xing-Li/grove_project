import { ChartTypes } from "./OptionsTypes";

/**
 * @type {Object.<string,{
 *       Alias: "Lines",
 *       Definition: "A line chart uses lines with segments to show changes in data in a ordinal dimension.",
 *       Family: "Statistic - LineCharts",
 *       Channel: "Color, Position",
 *       Purposes: "Comparison, Distribution, Rank",
 *       DataPrepare: `1「Time」or「Ordinal」 Field
 *
 *       0 ~ 1「Nominal」 Field
 *       
 *       1「Interval」 Field`,
 *   }}
 */
export const ChartTypesDesc = {
    [ChartTypes['SubGroups Timeline']]: {
        Alias: "SubGroups Timeline",
        Definition: "A subgroups timeline chart",
        Family: "Statistic - TimelineCharts",
        Channel: "Color, Position",
        Purposes: "Comparison, Distribution, Rank",
        DataPrepare: `2「Nominal」 Field
            
            1 ~ 2「Date」 Field`,
    },
    [ChartTypes['Groups Timeline']]: {
        Alias: "Groups Timeline",
        Definition: "A groups timeline chart",
        Family: "Statistic - TimelineCharts",
        Channel: "Color, Position",
        Purposes: "Comparison, Distribution, Rank",
        DataPrepare: `2「Nominal」 Field
            
            1 ~ 2「Date」 Field`,
    },
    [ChartTypes['Base Timeline']]: {
        Alias: "Timeline",
        Definition: "A base timeline chart",
        Family: "Statistic - TimelineCharts",
        Channel: "Color, Position",
        Purposes: "Comparison, Distribution, Rank",
        DataPrepare: `1「Nominal」 Field
            
            1 ~ 2「Date」 Field`,
    },
    [ChartTypes["Basic Line"]]: {
        Alias: "Lines",
        Definition: "A line chart uses lines with segments to show changes in data in a ordinal dimension.",
        Family: "Statistic - LineCharts",
        Channel: "Color, Position",
        Purposes: "Comparison, Distribution, Rank",
        DataPrepare: `1「Time」or「Ordinal」 Field
    
            0 ~ 1「Nominal」 Field
            
            1「Interval」 Field`,
    },
    [ChartTypes["Multi Line"]]: {
        Alias: "Lines",
        Definition: "A line chart uses lines with segments to show changes in data in a ordinal dimension.",
        Family: "Statistic - LineCharts",
        Channel: "Color, Position",
        Purposes: "Comparison, Distribution, Rank",
        DataPrepare: `1「Time」or「Ordinal」 Field
    
            0 ~ 1「Nominal」 Field
            
            1「Interval」 Field`,
    },
    [ChartTypes["Basic Column"]]: {
        Alias: "Columns",
        Definition: "A column chart uses series of columns to display the value of the dimension. The horizontal axis shows the classification dimension and the vertical axis shows the corresponding value.",
        Family: "Statistic - ColumnCharts",
        Channel: "Position, Color",
        Purposes: "Comparison, Distribution, Rank",
        DataPrepare: `1 ~ 2「Nominal」 Fields
    
            1「Interval」 Field`,
    },
    [ChartTypes["Multi Column"]]: {
        Alias: "Grouped Column",
        Definition: "A grouped column chart uses columns of different colors to form a group to display the values ​​of dimensions. The horizontal axis indicates the grouping of categories, the color indicates the categories, and the vertical axis shows the corresponding value.",
        Family: "Statistic - ColumnCharts",
        Channel: "Color, Position",
        Purposes: "Comparison, Distribution, Rank",
        DataPrepare: `1 ~ 2「Nominal」 Fields
    
            1「Interval」 Field`,
    },

    [ChartTypes["Basic Area"]]: {
        Alias: "Area Chart",
        Definition: "An area chart uses series of line segments with overlapped areas to show the change in data in a ordinal dimension.",
        Family: "Statistic - AreaCharts",
        Channel: "Color, Position",
        Purposes: "Comparison, Trend",
        DataPrepare: `1「Time」or「Ordinal」 Field
    
            1「Interval」 Field
            
            0 ~ 1「Nominal」 Field`,
    },
    [ChartTypes["Multi Area"]]: {
        Alias: "Stacked Area Chart",
        Definition: "A stacked area chart uses layered line segments with different styles of padding regions to display how multiple sets of data change in the same ordinal dimension, and the endpoint heights of the segments on the same dimension tick are accumulated by value.",
        Family: "Statistic - AreaCharts",
        Channel: "Color, Length",
        Purposes: "Comparison, Composition, Trend",
        DataPrepare: `1「Time」or「Ordinal」 Field
    
            1「Interval」 Field
            
            1「Nominal」 Field`,
    },

    [ChartTypes["Basic Bar"]]: {
        Alias: "Bars",
        Definition: "A bar chart uses series of bars to display the value of the dimension. The vertical axis shows the classification dimension and the horizontal axis shows the corresponding value.",
        Family: "Statistic - BarCharts",
        Channel: "Position, Color",
        Purposes: "Comparison, Distribution, Rank",
        DataPrepare: `1 ~ 2「Nominal」 Fields
    
            1「Interval」 Field`,
    },
    [ChartTypes["Multi Bar"]]: {
        Alias: "Grouped Bar",
        Definition: "A grouped bar chart uses bars of different colors to form a group to display the values of the dimensions. The vertical axis indicates the grouping of categories, the color indicates the categories, and the horizontal axis shows the corresponding value.",
        Family: "Statistic - BarCharts",
        Channel: "Color, Position",
        Purposes: "Comparison, Distribution, Rank",
        DataPrepare: `2「Nominal」 Fields
    
            1「Interval」 Field`,
    },
    [ChartTypes["Basic Pie"]]: {
        Alias: "Circle Chart, Pie",
        Definition: "A pie chart is a chart that the classification and proportion of data are represented by the color and arc length (angle, area) of the sector.",
        Family: "Statistic - PieCharts",
        Channel: "Angle, Area, Color",
        Purposes: "Comparison, Composition, Proportion",
        DataPrepare: `1「Nominal」 Field
    
            1「Interval」 Field`,
    },
    [ChartTypes["Basic Scatter"]]: {
        Alias: "Scatter Chart, Scatterplot",
        Definition: "A scatter plot is a type of plot or mathematical diagram using Cartesian coordinates to display values for typically two variables for series of data.",
        Family: "Statistic - ScatterCharts",
        Channel: "Color, Position",
        Purposes: "Comparison, Distribution",
        DataPrepare: `2「Interval」 Fields
    
            0 ~ 1「Nominal」 Field`,
    },
    [ChartTypes["Multi Scatter"]]: {
        Alias: "Scatter Chart, Scatterplot",
        Definition: "A scatter plot is a type of plot or mathematical diagram using Cartesian coordinates to display values for typically two variables for series of data.",
        Family: "Statistic - ScatterCharts",
        Channel: "Color, Position",
        Purposes: "Comparison, Distribution",
        DataPrepare: `2「Interval」 Fields
    
            0 ~ 1「Nominal」 Field`,
    },
    [ChartTypes["Basic Bubble"]]: {
        Alias: "Bubble Chart",
        Definition: "A bubble chart is a type of chart that displays four dimensions of data with x, y positions, circle size and circle color.",
        Family: "Statistic - ScatterCharts",
        Channel: "Color, Position, Size",
        Purposes: "Comparison, Distribution",
        DataPrepare: `3「Interval」 Fields
    
            0 ~ 1「Nominal」 Field`,
    },
    [ChartTypes["Multi Bubble"]]: {
        Alias: "Bubble Chart",
        Definition: "A bubble chart is a type of chart that displays four dimensions of data with x, y positions, circle size and circle color.",
        Family: "Statistic - ScatterCharts",
        Channel: "Color, Position, Size",
        Purposes: "Comparison, Distribution",
        DataPrepare: `3「Interval」 Fields
    
            0 ~ 1「Nominal」 Field`,
    },
    [ChartTypes["Basic Radar"]]: {
        Alias: "Web Chart, Spider Chart, Star Chart, Cobweb Chart, Irregular Polygon, Kiviat diagram",
        Definition: "A radar chart maps series of data volume of multiple dimensions onto the axes. Starting at the same center point, usually ending at the edge of the circle, connecting the same set of points using lines.",
        Family: "Statistic - RadarCharts",
        Channel: "Color, Position",
        Purposes: "Comparison",
        DataPrepare: `1 ~ 2「Nominal」 Fields
    
            1「Interval」 Field`,
    },
    [ChartTypes["Multi Radar"]]: {
        Alias: "Web Chart, Spider Chart, Star Chart, Cobweb Chart, Irregular Polygon, Kiviat diagram",
        Definition: "A radar chart maps series of data volume of multiple dimensions onto the axes. Starting at the same center point, usually ending at the edge of the circle, connecting the same set of points using lines.",
        Family: "Statistic - RadarCharts",
        Channel: "Color, Position",
        Purposes: "Comparison",
        DataPrepare: `1 ~ 2「Nominal」 Fields
    
            1「Interval」 Field`,
    },
    [ChartTypes["Continuous Heatmap"]]: {
        Alias: "Heatmap, Heat map",
        Definition: "A continuous heatmap is a type of heatmap that based on a map.",
        Family: "Statistic - HeatmapCharts",
        Channel: "Position, Color",
        Purposes: "Distribution",
        DataPrepare: `3「Interval」 Fields`,
    },
    [ChartTypes["Heatmap"]]: {
        Definition: "A heatmap is a graphical representation of data where the individual values contained in a matrix are represented as colors.",
        Family: "Statistic - HeatmapCharts",
        Channel: "Color, Position",
        Purposes: "Distribution",
        DataPrepare: `2「Nominal」or「Ordinal」 Fields
    
            1「Interval」 Field`,
    },
    [ChartTypes["Uneven Heatmap"]]: {
        Definition: "An uneven heatmap is a heatmap with size channel.",
        Family: "Statistic - HeatmapCharts",
        Channel: "Position, Color, Size",
        Purposes: "Distribution",
        DataPrepare: `2「Nominal」or「Ordinal」 Fields
    
            2「Interval」 Fields`,
    },
    [ChartTypes["Basic Word Cloud"]]: {
        Definition: "A Word cloud chart lets you visualize text data. Text values are displayed with their size based on a measure value. The measure can be anything you want to measure against, for example: times used, alphabetically, by importance, or by context. You can customize your chart with different shapes, fonts, layouts, and color schemes. It is included in the Visualization bundle.",
        Family: "Statistic - WordCharts",
        Channel: "Size",
        Purposes: "Comparison, Trend",
        DataPrepare: `1「Nominal」 Field
    
            1「Interval」 Field`,
    },
    [ChartTypes["Word Cloud"]]: {
        Definition: "A Word cloud chart lets you visualize text data. Text values are displayed with their size based on a measure value. The measure can be anything you want to measure against, for example: times used, alphabetically, by importance, or by context. You can customize your chart with different shapes, fonts, layouts, and color schemes. It is included in the Visualization bundle.",
        Family: "Statistic - WordCharts",
        Channel: "Color, Size",
        Purposes: "Comparison, Trend",
        DataPrepare: `1 ~ 2「Nominal」 Field
    
            1「Interval」 Field`,
    },
    [ChartTypes["Waterfall"]]: {
        Alias: "Flying Bricks Chart, Mario Chart, Bridge Chart, Cascade Chart",
        Definition: "A waterfall chart is used to portray how an initial value is affected by a series of intermediate positive or negative values",
        Family: "Statistic - ColumnCharts",
        Channel: "Color, Length, Position",
        Purposes: "Comparison, Trend",
        DataPrepare: `1「Ordinal」or「Time」or「Nominal」 Field
    
            1「Interval」 Field`,
    },
}