import actions from "../define/Actions";

const { getSafeMode, setSafeMode, getReadOnlyMode, setReadOnlyMode } = require("util/localstorage");

class CommonData {
    constructor() {
        this.safeMode = getSafeMode();
        this.readOnlyMode = getReadOnlyMode();
        actions.variable(actions.types.SAFE_MODE, [], () => {
            return this.safeMode;
        })
        actions.variable(actions.types.READ_ONLY_MODE, [], () => {
            return this.readOnlyMode;
        })
    }
    getReadOnlyMode() {
        return this.readOnlyMode;
    }
    setReadOnlyMode(readOnlyMode) {
        this.readOnlyMode = readOnlyMode;
        setReadOnlyMode(readOnlyMode);
        actions.variable(actions.types.READ_ONLY_MODE, [], () => {
            return readOnlyMode
        })
    }
    getSafeMode() {
        return this.safeMode;
    }
    setSafeMode(safeMode) {
        this.safeMode = safeMode;
        setSafeMode(safeMode);
        actions.variable(actions.types.SAFE_MODE, [], () => {
            return safeMode
        })
    }
}
const commonData = new CommonData();
export default commonData;
window.commonData = commonData;