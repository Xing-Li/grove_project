export default function() {
  if (window.opener) {
    return window.opener.document.querySelector('#light-theme') === null
  } else {
    return undefined;
  }
}
