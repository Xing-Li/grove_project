const Fetch = require('node-fetch');
const passport = require('passport');
const async = require('async');
const OAuth2 = require('oauth').OAuth2;
const LocalStrategy = require('passport-local')
const SamlStrategy = require('passport-saml').Strategy;
const OIDCStrategy = require('passport-azure-ad').OIDCStrategy
const GoogleStrategy = require('passport-google-oauth20').Strategy
const LdapStrategy = require('passport-ldapauth');
const OAuth2Strategy = require('passport-openid-oauth20').Strategy;
const urlcat = require('urlcat').default;
const cryptoUtil = require('../util/cryptoUtil');

const _ = require('lodash')
const CheckDiskSpace = require('check-disk-space')

const User = require('./../user/user.model')
const Util = require('./util')
const { TmpDir  } = require('./../../lib/fileUtils');

const UserAPITokenStrategy = require('./UserAPITokenStrategy')
const Logger = require('./../logger')

const GLogger = Logger.getLogger(__filename);
const GOneDay = 86400000; // milliseconds
const GOneHour = 3600000; // milliseconds
let   GLastCheckDiskTime = 0;
const GLastDisk ={
    free:0,
    size:0
}
const neo4jAppName = String(AppConfig.appName || "GraphXR");
const neo4jAppID = neo4jAppName.replace(/\W/g, '').toLowerCase();
const cors = AppConfig.cors || {};
let Auth = {
    neo4jAppURLWithReq: function (req) {
        return `/p/Neo4jDesktop/default`
    },
    isNeo4jAppReq: function (req) {
        return (/Neo4jDesktop.+(Electron)?/ig).test(req.headers['user-agent']) || req.query['neo4jDesktopGraphAppId'] === neo4jAppID
    },

    getGithubEmail : (oauth, profile, accessToken, callback) => {
        oauth._oauth2.useAuthorizationHeaderforGET(true);
        oauth._oauth2._request(
          "GET",
          AppConfig.oauth2.userProfileURL + "/emails",
          { 
              Accept: "application/json",
              Authorization: "token " + accessToken 
          },
          "",
          accessToken,
          function (err, body, res) {
            if (err) {
              return callback(null, profile);
            }
            let emails;
            try {
              emails = JSON.parse(body) || [];
              if (!emails.length) {
                  return callback(null, profile);
              }
              profile.email = (emails.find(e => e.primary) || {}).email;
            } catch (err) {
              return callback(null, profile);
            }
            return callback(null, profile);
          }
        );
      },
      getProfile : (oauth, profile, accessToken, callback, userProfileURL) => {
        let oauth2 = oauth ? oauth._oauth2 : new OAuth2();
        oauth2.useAuthorizationHeaderforGET(true);
        oauth2._request(
          "GET",
          userProfileURL,
          { 
              Accept: "application/json",
              Authorization: "Bearer " + accessToken 
          },
          "",
          accessToken,
          function (err, body, res) {
            if (err) {
              return callback(null, profile);
            }
            try {
              let  newProfile = JSON.parse(body) || {};
              Object.assign(profile || {}, newProfile);
            } catch (err) {
              return callback(null, profile);
            }
            return callback(null, profile);
          }
        );
      },
    init: function (app) {

        if (AppConfig.adminEmail) {

            app.use(passport.initialize());
            app.use(passport.session());

            passport.use(new LocalStrategy.Strategy(
                function (username, password, next) {

                    User.findByUsername(username, function (err, user) {
                        if (err) {
                            return next(err);
                        }
                        if (!user) {
                            return next(null, false, {
                                message: 'tips.auth.incorrectUsernameOrPassword'
                            });
                        } else {
                            return user.verifyPassword(password, next);
                        }
                    });
                }
            ));

            if (AppConfig.azureAD) {
                passport.use(new OIDCStrategy(Object.assign( {
                    // Required
                    identityMetadata: 'https://login.microsoftonline.com/common/v2.0/.well-known/openid-configuration',
                    responseType: 'id_token',
                    // Required
                    responseMode: 'form_post',
                    // Required if we use http for redirectUrl
                    allowHttpForRedirectUrl: true,
                    // Required to set to false if you don't want to validate issuer
                    validateIssuer: false,
                    // Required to set to true if the `verify` function has 'req' as the first parameter
                    passReqToCallback: true,
                    // Recommended to set to true. By default we save state in express session, if this option is set to true, then
                    // we encrypt state and save it in cookie instead. This option together with { session: false } allows your app
                    // to be completely express session free.
                    useCookieInsteadOfSession: false,
                    // The additional scopes we want besides 'openid'.
                    // 'profile' scope is required, the rest scopes are optional.
                    // (1) if you want to receive refresh_token, use 'offline_access' scope
                    // (2) if you want to get access_token for graph api, use the graph api url like 'https://graph.microsoft.com/mail.read'
                    scope: ["email", "profile"],
                    // Optional, 'error', 'warn' or 'info'
                    loggingLevel: process.env.NODE_ENV === "production" ? "error" : "info",
                    // Optional. The lifetime of nonce in session or cookie, the default value is 3600 (seconds).
                    nonceLifetime: null,
                    // Optional. The max amount of nonce saved in session or cookie, the default value is 10.
                    nonceMaxAmount: 5,
                    // Optional. The clock skew allowed in token validation, the default value is 300 seconds.
                    clockSkew: null,
                  }, AppConfig.azureAD),
                  function (req,iss, sub, profile, accessToken, refreshToken, done) {
 
                    if (!profile.oid) {
                      return done(new Error("No oid found"), null);
                    }
                    profile = profile._json || profile;
                    req.query.username = profile.email;
                    
                    process.nextTick(function () {
                      Auth.handleAzureADLogin(profile, done);
                    })
                   }
                )
              );
            }


            passport.use(new UserAPITokenStrategy({},
                function (auth, done) {
                    if (!auth || !auth.key || !auth.secret) {
                        return done(new Error("The auth must contains key and secret, {key:Your key,secret: Your secret }"));
                    }
                    process.nextTick(function () {
                        User.apiAuth(auth.key, auth.secret, function (err, user) {
                            if (err || !user) {
                                return done(new Error("Can not found the auth info"));
                            }
                            return done(null, user);
                        });
                    })
                }
            ));

            // passport.use('emailAuth', new CustomStrategy.Strategy(
            //     function (req, next) {
            //         let token = req.query.token || req.body.token;
            //         User.findByUsername(req.query.email || req.body.email, function (err, user) {
            //             if (err) {
            //                 return next(err);
            //             }
            //             if (user && user.emailAuth && user.emailAuth.code == token) {
            //                 return next(null, user);
            //             } else {
            //                 return next({
            //                     message: 'Incorrect username.'
            //                 }, false);
            //             }
            //         });
            //     }
            // ));

            if (AppConfig.google) {
                passport.use(new GoogleStrategy(Object.assign( {
                    clientID: AppConfig.google.clientID,
                    clientSecret: AppConfig.google.clientSecret,
                    callbackURL: "/api/auth/google/callback",
                    passReqToCallback: true,
                },AppConfig.google), function (req, accessToken, refreshToken, profile, done) {
                    // GLogger.log('google auth : ', accessToken, refreshToken, profile);
                 
                    profile = profile._json || profile;

                    if (!profile.email) {
                        return done(new Error("No email found"), null);
                    }

                    req.query.username = profile.email;

                    return Auth.handleGoogleLogin(profile, done);

                }));
            }

            if (AppConfig.ldap) {
              let ldapConfig = Object.assign({}, AppConfig.ldap, {
                passReqToCallback: true,
                usernameField: "username",
                passwordField: "password",
                cache:false
              });
              Object.assign(ldapConfig.server, {
                tlsOptions: {
                  rejectUnauthorized: false,
                },
                searchAttributes: ["dn", "mail", "uid", "userid", "sn", "surname", "cn", "commonname"],
              });
      
              if (ldapConfig.server.adminDN) {
                ldapConfig.server.groupSearchBase = ldapConfig.server.adminDN;
                ldapConfig.server.groupSearchFilter = "(member={{dn}})";
                ldapConfig.server.groupSearchAttributes = ["dn"];
              }
      
              passport.use(
                new LdapStrategy(ldapConfig, function (req, profile, done) {
                  //GLogger.log("LDAP Auth :",profile);
                  if (!profile.mail) {
                    return done(new Error("No email found"), null);
                  }
      
                  req.query.username = profile.mail;
                  return Auth.handleLDAPLogin(profile, done);
                })
              );
            }


            //refer okta auth
            if (AppConfig.saml) {
                let samlConfig = _.cloneDeep(AppConfig.saml);
                delete samlConfig.loginShowName;
                delete samlConfig.profileMapping;
                passport.use(new SamlStrategy(Object.assign( 
                    {
                        path:  "/saml/login/callback",
                        passReqToCallback: true,
                    }, samlConfig),
                    function (req, profile, done) {
                        const profileMapping = AppConfig.saml.profileMapping || {};
                        const emailField = profileMapping.email ? profileMapping.email : "email";
                        if (!profile[emailField]) {
                           return done(new Error("No email found"), null);
                        }
                        req.query.username = profile[emailField];
                        return Auth.handleSamlLogin(profile, done);
                    }
                ));
            }

            if (AppConfig.oauth2) {
                const oauth = new OAuth2Strategy(Object.assign( 
                    {
                        passReqToCallback: true,
                        callbackURL: "/oauth2/login/callback",
                    }, AppConfig.oauth2), function (req, accessToken, refreshToken, profile, done) {                 
                        const usernameField = AppConfig.oauth2.usernameField  || AppConfig.oauth2.profileMapping?.usernameField || 'email';

                        const handleCallback = (newProfile) =>{
                            if (!usernameField && !newProfile.email) {
                                return done(new Error("No email or username found"), null);
                            }
                            req.query.username = _.get(newProfile,usernameField);
                            return Auth.handleOAuth2Login(newProfile, done);
                        }
                        let profileJSON = profile._json || profile || {};
                        if(AppConfig.oauth2.userProfileURL && ((/github.com/).test(AppConfig.oauth2.tokenURL))){
                            //github does not provide email as default
                            return Auth.getGithubEmail(oauth, profileJSON, accessToken, function (err, githubProfile) {
                                return handleCallback(githubProfile);
                            })
                        }else if(AppConfig.oauth2.userProfileURL && !profileJSON[usernameField]){
                            return Auth.getProfile( oauth, profileJSON, accessToken, function (err, fullProfile) {
                                return handleCallback(fullProfile);
                            }, AppConfig.oauth2.userProfileURL);
                        }else {
                           return handleCallback(profileJSON);
                        }
                    }
                );
                oauth._oauth2.useAuthorizationHeaderforGET(true);
                passport.use(oauth);
            }
   
            passport.serializeUser(function (user, done) {
                // GLogger.log("serializeUserID :", user.id);
                done(null, user);
            });

            passport.deserializeUser(function (user, done) {
                // GLogger.log("deserializeUserID :", user._id);
                User.getInfo(typeof(user) === 'string' ? user : user._id, user.email, function (err, userDoc) {
                    done(null, userDoc);
                })
            });
            // passport.serializeUser(User.serializeUser());
            // passport.deserializeUser(User.deserializeUser());

        } else if (AppConfig.auth && AppConfig.auth.password) {
            app.use(this.simpleAuth)
            app.use("/user/login", this.simpleAuthLogin);
        } else {
            app.use(this.noAuth)
        }

        //add isManager and isAdminPage
        app.use(function (req, res, next) {

            req.isManager = function () {
                return req.isAuthenticated && req.isAuthenticated() && req.user && req.user.type == 1
            }

            req.isAdminPage = function () {
                return req.isManager() && (/^\/admin\//ig).test(req.headers.referer)
            }

            next();
        });

    },
    handleThirdPartyLogin: function (userInfo, next) {

        if (userInfo.email === AppConfig.adminEmail) {
            userInfo.type = 1;
        //default is normal user account.
        }else if([null,undefined,''].indexOf(userInfo.type)){
            userInfo.type = 0;
        }
        //force active the ThirdParty user account.
        userInfo.access = 0;
        if(!userInfo.username && !userInfo.email){
            const err = new Error("Can not got user info");
            return next(err);
        }
       User.findByUsername(userInfo.username || userInfo.email, function (err, user) {
        if (err) {
          return next(err);
        }
        if (!user) {
          //if only have the email info. We should set the firstName and lastName as default
          userInfo.firstName = userInfo.firstName || "Default";
          userInfo.lastName =  userInfo.lastName || "";
          User.register(userInfo, Util.randomAlphaNum(32), function (err, newUser) {
            if (err) {
              GLogger.error(err);
            }
            return next(err, newUser);
          });
        } else {
          // Merge existing user into SSO user.
          // Otherwise we can't change SSO users into admins,
          // because the type is set to 0 (normal user) every time they login.
          // I think we edit the profile every login to intake profile changes,
          // like first name and last name from the SSO provider.
          // As far as I know, `type` is the only GraphXR specific parameter
          // which we care about setting manually in the GraphXR user manager.
          userInfo.type = user.type;

          //auto clean the empty or null field, because it will replace exist value.
          Object.keys(userInfo).forEach(k =>{
            if([null,undefined,''].indexOf(userInfo[k]) >= 0){
                delete userInfo[k];
            }
          })

          User.edit(user.id, userInfo, function (err, newUser) {
            return next(err, newUser);
          });
        }
      });
    },
  
    handleAzureADLogin: function (profile, next) {
      let azureUser = {
        firstName: profile.given_name,
        lastName: profile.family_name,
        email: profile.email,
        username: profile.email,
        type: 0,
      };
       //GLogger.log("azureAD：", profile, azureUser, next);
      Auth.handleThirdPartyLogin(azureUser, next);
    },
    handleLDAPLogin: function (profile, next) {
      let ldapUser = {
        firstName: profile.cn || profile.commonname,
        lastName: profile.sn || profile.surname || "",
        email: profile.mail || profile.email,
        username: profile.mail || profile.email,
        type: 0,
      };
  
      Auth.handleThirdPartyLogin(ldapUser, next);
    },
    handleGoogleLogin: function (profile, next) {
      let googleUser = {
        firstName: profile.given_name,
        lastName: profile.family_name,
        email: profile.email,
        username: profile.email,
        avatarURL: profile.picture,
        type: 0,
      };
  
      if (profile.hd === AppConfig.google.adminDomain || googleUser.email === AppConfig.adminEmail) {
        googleUser.type = 1;
      }
  
      Auth.handleThirdPartyLogin(googleUser, next);
    },
    handleSamlLogin: function (profile, next) {
        const profileMapping = AppConfig.saml.profileMapping || {};
        const emailField =  profileMapping.email ? profileMapping.email : 'email' ;
        const firstNameField =  profileMapping.firstName ? profileMapping.firstName : 'firstName';
        const lastNameField = profileMapping.lastName ? profileMapping.lastName : 'lastName';
        const avatarUrlField = profileMapping.avatarUrl ? profileMapping.avatarUrl : 'avatarUrl';
        let samlUser = {
          firstName: profile[firstNameField]  || profile.first_name || profile.given_name || "",
          lastName: profile[lastNameField]  || profile.last_name || profile.family_name || "",
          email: profile[emailField] || profile.mail,
          username: profile[emailField] || profile.mail,
          avatarURL: profile[avatarUrlField] || profile.picture || profile.photo || profile.avatarURL || profile.avatar || "",
          type: 0,
        };
        Auth.handleThirdPartyLogin(samlUser, next);
      },

      handleOAuth2Login: function (profile, next) {
        const profileMapping = AppConfig.oauth2.profileMapping || {};
        const emailField =  profileMapping.email ? profileMapping.email : 'email' ;
        const firstNameField =  profileMapping.firstName ? profileMapping.firstName : 'firstName';
        const lastNameField = profileMapping.lastName ? profileMapping.lastName : 'lastName';
        const avatarUrlField = profileMapping.avatarUrl ? profileMapping.avatarUrl : 'avatarUrl';
        const usernameField = AppConfig.oauth2.usernameField || profileMapping.usernameField || emailField;
        let oAuthUser = {
          firstName: _.get(profile, firstNameField, profile.first_name || profile.given_name || ""),
          lastName: _.get(profile, lastNameField, profile.last_name || profile.family_name || ""),
          email: _.get(profile, emailField, profile.mail || ''),
          username: _.get(profile, usernameField, profile.username || profile.mail || ''),
          avatarURL: _.get(profile, avatarUrlField,  profile.picture || profile.photo || profile.avatarURL || profile.avatar || ""),
          type: 0,
        };
        //auto generate email
        if(AppConfig.oauth2.domain && oAuthUser.username && !(/.+@.+/ig).test(oAuthUser.email)){
            oAuthUser.email = `${oAuthUser.username}@${AppConfig.oauth2.domain}`
        }
        Auth.handleThirdPartyLogin(oAuthUser, next);
      },
    noAuth: function (req, res, next) {
        req.user = {
            type: 1 //always admin when the not set adminEmail & simple auth
        }
        req.isAuthenticated = function () {
            return true;
        }
        next();
    },
    simpleAuthLogin: function (req, res, next) {
        if (AppConfig.auth && AppConfig.auth.password == req.parameters.password) {

            res.cookie('auth', "success", {
                maxAge: 86400000,
                expires: new Date(Date.now() + 86400000)
            });

            res.send({
                status: 0,
                message: "tips.successful"
            })

        } else {

            res.cookie('auth', "fail", {
                maxAge: 86400000,
                expires: new Date(Date.now() + 86400000)
            });

            res.send({
                status: 1,
                message: "tips.failed"
            })
        }
    },

    simpleAuth: function (req, res, next) {
        req.user = {
            type: req.cookies && req.cookies["auth"] == 'success' ? 1 : 0
        }

        req.isAuthenticated = function () {
            if (req.cookies && req.cookies["auth"] == 'success') {
                return true;
            } else {
                return false;
            }
        }

        next();
    },

    verifyIframeAccount : function(req, res, next){
     let keyName = AppConfig.iframeAuth.keyName;
     let keyValue = req.query[keyName];
     let email = req.query["email"];

     let refererURL = new URL(req.headers.referer);
    //  let host = req.headers['host'];
    // verify the iframe domains
     if(Array.isArray(AppConfig.iframeAuth.domains) &&
     AppConfig.iframeAuth.domains.length > 0 &&
     !AppConfig.iframeAuth.domains.includes(refererURL.host)){
        console.error(`do not allow the ${refererURL.host} site embed the graphXR with iframe auth`)
        return next();
     }

     //Only support email can skip the verify
     if(!AppConfig.iframeAuth.userProfileURL && email){
        let iframeUser = {
            email: email,
            username: email,
            type: 0,
          };
        return Auth.handleThirdPartyLogin(iframeUser,function(loginErr,user){
            if(user){
                req.query.username = iframeUser.email;
                return req.login(user, (err) => {
                    next && next(err);
                })  
            }  
            next && next(loginErr)
         })
     } else if (AppConfig.iframeAuth.userProfileURL){
        keyValue = keyValue || AppConfig.iframeAuth[keyName];
        let iframeUser = {
            email: email,
            username: email,
            token:{key:keyValue},
            type: 0,
          };
        async.waterfall([
            //1. Check the token and email 
            callback =>{
              User.findByToken(keyValue, iframeUser.email, callback);
            },
            //2. Get the Userinfo
            (user, callback) =>{
                if(user){
                   return callback(null, user);
                }

                let headerInfo = {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json',
                    "Authorization" : `Bearer ${keyValue}`,
                }

                //support static token, if the token is static, we need send the email info to the third system
                if(keyValue === AppConfig.iframeAuth[keyName]){
                    headerInfo = Object.assign(headerInfo, {
                        "Email": iframeUser.email,
                    })
                }

                Fetch(AppConfig.iframeAuth.userProfileURL, {
                    method: 'GET',
                    headers: headerInfo,
                })
                .then(res => {
                    return res.json()
                })
                .then(res=> {
                    let profile = res;
                    let profileMapping = AppConfig.iframeAuth.profileMapping || {};

                    const emailNameField =   profileMapping.email || "email";
                    const usernameField = profileMapping.username || emailNameField;
                    //default update the graphXR account info from iframe auth system.
                    if(!AppConfig.iframeAuth.disableUpdateProfile){
                        const firstNameField =  profileMapping.firstName || "firstName";
                        const lastNameField = profileMapping.lastName || "lastName";
                        const avatarUrlField = profileMapping.avatarUrl || "avatarUrl";

                        iframeUser.firstName = _.get(profile,firstNameField , profile.firstName || profile.first_name || profile.given_name || "");
                        iframeUser.lastName = _.get(profile, lastNameField , profile.lastName || profile.last_name || profile.family_name || "");
                        iframeUser.avatarURL =  _.get(profile, avatarUrlField , profile.picture || profile.photo || profile.avatarURL || profile.avatar || "");
                        iframeUser.username =  _.get(profile, usernameField, profile.username || profile.mail || profile.email);
                    }

                    //email is optional when the third system , Oauth profile API can return the email info
                    //@sean need improved, because the logic maybe never invoke
                    if(!iframeUser.email && _.get(profile, emailNameField)){
                        iframeUser.email = _.get(profile, emailNameField);
                        iframeUser.username = _.get(profile, usernameField);
                    }
                    callback(null, iframeUser);
                })
                .catch(err=> {
                    callback(err)
                }) 
            },          
        ], (err,userData) => {
            if(err){
                return next && next(err);
            }
            return Auth.handleThirdPartyLogin(userData,function(loginErr,user){
                if(user){
                    req.query.username = userData.email;
                    return req.login(user, (err) => {
                        next && next(err);
                    })  
                }  
                next && next(loginErr)
             })
        })
     }else{
        next && next(new Error("Do not allow embed graphXR."))
     }
   },

    

    protected: function (req, res, next) {
        //enable iframe Auth, use referer handle the iframe refer
        if(req.headers.referer && AppConfig.iframeAuth 
            && AppConfig.iframeAuth.keyName 
            && (req.query[AppConfig.iframeAuth.keyName] || req.query['email'] && AppConfig.iframeAuth[AppConfig.iframeAuth.keyName])){
            Auth.verifyIframeAccount(req, res, () =>{
                Auth.protectedUser(req, res, next);
            });
        }else{
            Auth.protectedUser(req, res, next);
        }
    },

    protectedUser: function (req, res, next) {

        if (req.isAuthenticated()) {
            let redirect = req.session && req.session.redirect ? req.session.redirect : null;
            redirect = redirect || (req.cookies && req.cookies.redirect ? req.cookies.redirect : "");

            //clear the cookies for redirect
            if (req.cookies && req.cookies.redirect) {
                res.clearCookie('redirect');
            }
            req.session.redirect = null;
            if (!(/^\/login/ig).test(redirect) && (/^\//ig).test(redirect)) {
                GLogger.log("Auth success redirect:", redirect);
                res.redirect(redirect);
            } else {
                next();
            }
        } else if ((/^\/(api\/)?login/ig).test(req.originalUrl)) {
            next();
        } else if (req.originalUrl && (/\/api\//i).test(req.originalUrl) && !(req.parameters && req.parameters['meta'])) {
            return res.send({
                status: 401,
                message: "Please login at first"
            });
        } else {
            //only save the cookies five minutes, default redirect to /projects page (or the root path)
            let redirect = req.originalUrl != "/" ? req.originalUrl : (AppConfig.disableRootPath ? "/projects" : "/");
            if (redirect == '/projects' && Auth.isNeo4jAppReq(req)) {
                redirect = Auth.neo4jAppURLWithReq(req);
            }
            // I don't know why, but this redirect is not a 302, so proxyPath.js doesn't intercept it.
            // So I'm prefixing the aliasPath manually.
            const redirectUrl = `/login?url=${encodeURIComponent(Util.prefixAliasPath(redirect))}`;
            GLogger.log(`User is unauthorized to ${req.originalUrl}. Redirecting to ${redirectUrl}}`);
            res.redirect(redirectUrl);
        }
    },
    protectedManager: function (req, res, next) {
        if (req.isManager()) {
            return next();
        } else if (req.originalUrl && (/\/api\//i).test(req.originalUrl)) {
            return res.send({
                status: 401,
                message: "Your are not manager",
                content: {
                    email: req.user ? req.user.email : ""
                }
            });
        } else {
            res.redirect('/');
        }
    },
    protectedDisk: function (req, res, next) {

        function checkDisk() {
			let minDiskSpace = AppConfig.stopServerMinDiskSpace || "1 GB";
			minDiskSpace = minDiskSpace ? String(minDiskSpace).trim().toUpperCase() : minDiskSpace;
			let diskUnit = "GB";
			if (/^\d+[KMG]B?$/gi.test(minDiskSpace)) {
				minDiskSpace = minDiskSpace.replace(/([KMG])$/gi, "$1 B");
				diskUnit = minDiskSpace.replace(/^\d+([KMG]B)$/gi, "$1");
			}
			let minDiskSpaceBytes = Util.formatSize(minDiskSpace);
			let freeDisk = Util.formatBytes(GLastDisk.free, 0, diskUnit).replace(",", "");
			let freeDiskBytes = Util.formatSize(freeDisk);
			// GLogger.warn("protectedDisk:",minDiskSpace,diskUnit,freeDisk, parseInt(freeDisk) , parseInt(minDiskSpace));

			if (minDiskSpaceBytes && freeDiskBytes > minDiskSpaceBytes) {
					return next();
			}else if (minDiskSpaceBytes && freeDiskBytes <= minDiskSpaceBytes) {
					res.status(500);
					return res.send({
						status: 1,
						message: `GraphXR server disk space less then ${minDiskSpace} (${minDiskSpaceBytes} Bytes): ${freeDisk} (${freeDiskBytes} Bytes), please feedback to the admin ${AppConfig.adminEmail}`,
						content: {
							email: req.user ? req.user.email : ""
						}
					});
			}else{
				return next();
			}
		}
  
        if (Date.now() - GLastCheckDiskTime > GOneHour) {
			return CheckDiskSpace(TmpDir).then((diskSpace) => {
				return Object.assign(GLastDisk, diskSpace);
			}).then(() => {
				return checkDisk();
			}).catch((err) => {
				GLogger.error("protectedDisk:", err);
				return next();
			});
		} else {
			return checkDisk();
		}
    },
    ignoreScriptScan: function (req, res, next) {
        //.php .asp .aspx .do .action .jsp
        if ((/\.(php|asp|aspx|do|action|jsp)$/ig).test(req.path)) {
            res.set('WWW-Authenticate', 'Basic realm="401"')
            res.status(401).send('Thank you for your interest, please contact kineviz Inc.')
        } else {
            next();
        }
    }

};


module.exports = Auth;
module.exports.neo4jAppName = neo4jAppName;
module.exports.neo4jAppID = neo4jAppID;