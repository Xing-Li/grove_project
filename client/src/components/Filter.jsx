import { List, Tabs, Select } from 'antd';
import _ from 'lodash';
import MdFiles from "../file/MdFiles";

const { FILTER_TYPE } = require('../file/fileUtils');
const { isFileImage, ImageType, fallbackimg } = require('../util/helpers.js');
const { showToast, ModalType } = require("../util/utils");
const { imageDataFunc } = require('../block/data');
const { TabPane } = Tabs;
const { Option } = Select;

export default function Filter(props) {
    /** @type {MdFiles} */
    const react_component = props.react_component;
    const { filterType } = react_component.state;
    function callback(key) {
        react_component.setState({ filterType: key });
    }
    return <div className="filter">
        <div className="d-flex justify-content-between align-items-center">
            Filter by:<Select dropdownClassName="normal-icon" value={filterType} style={{ width: 120 }} onChange={callback}>
                <Option value={FILTER_TYPE.links}><i className="f7 icon icon-xs fas fa-link"></i>{FILTER_TYPE.links}</Option>
                <Option value={FILTER_TYPE.published}><i className="f7 icon icon-xs fas fa-globe"></i>{FILTER_TYPE.published}</Option>
                <Option value={FILTER_TYPE.shared}><i className="f7 icon fas icon-xs fa-share-alt-square"></i>{FILTER_TYPE.shared}</Option>
            </Select>
        </div>
        {filterType === FILTER_TYPE.links && <FilterLinks react_component={react_component}></FilterLinks>}
        {filterType === FILTER_TYPE.shared && <FilterShareds react_component={react_component}></FilterShareds>}
    </div>;
    /* <Tabs defaultActiveKey={filterType} onChange={callback}>
        <TabPane tab={<span><i className="f7 icon fas fa-link"></i>{FILTER_TYPE.links}</span>} key={FILTER_TYPE.links}>
            <FilterLinks react_component={react_component}></FilterLinks>
        </TabPane>
        <TabPane tab={<span><i className="f7 icon fas fa-globe"></i>{FILTER_TYPE.published}</span>} key={FILTER_TYPE.published}>
        </TabPane>
        <TabPane tab={<span><i className="f7 icon fas fa-share-alt-square"></i>{FILTER_TYPE.shared}</span>} key={FILTER_TYPE.shared}>
            <FilterShareds react_component={react_component}></FilterShareds>
        </TabPane>
    </Tabs> */
}
/**
 * 
 * 
 */
function FilterLinks(props) {
    /** @type {MdFiles} */
    const react_component = props.react_component;
    const { linkedFilesMap, fileNamesJson } = react_component.state;
    const { projectId } = react_component.props;
    let keys = _.reduce(linkedFilesMap, (prev, value, key, linkedFilesMap) => {
        if (undefined === fileNamesJson[key]) {
            let link_projectId = new URLSearchParams(value.accessPath.indexOf("?") + 1);
            if (link_projectId.get("projectId") === projectId) {
                prev.push(key);
            }
        }
        return prev;
    }, [])
    return <div>
        {keys.length > 0 && <List
            size="small"
            header={<div className="d-flex justify-content-between">
                <div>Not Used Links</div>
                <a onClick={async () => {
                    let state = {};
                    let promiseList = [];
                    _.keys(keys).filter(fileKey => {
                        if (linkedFilesMap[fileKey]) {
                            promiseList.push(react_component.handleRemoveLinkedToGraphXR(fileKey, state));
                        }
                    })
                    await Promise.all(promiseList);
                    react_component.setState(state);
                }}  >Delete All</a>
            </div>}
            // footer={<div>Footer</div>}
            bordered
            dataSource={keys}
            renderItem={item => <List.Item
                actions={[<a onClick={() => {
                    react_component.handleRemoveLinkedToGraphXR(item)
                }} key="list-loadmore-delete">Delete</a>]}
            >{item} </List.Item>}
        />}
    </div>
}
function FilterShareds(props) {
    /** @type {MdFiles} */
    const react_component = props.react_component;
    const { sharedFilesMap, fileNamesJson } = react_component.state;
    let keys = _.reduce(sharedFilesMap, (prev, value, key, sharedFilesMap) => {
        if (undefined === fileNamesJson[key]) {
            prev.push(key);
        }
        return prev;
    }, [])
    return <div>
        {keys.length > 0 && <List
            size="small"
            header={<div className="d-flex justify-content-between">
                <div>Not Used Shareds</div>
                <a onClick={async () => {
                    let state = {};
                    let promiseList = [];
                    _.each(keys, (fileKey) => {
                        if (sharedFilesMap[fileKey]) {
                            promiseList.push(react_component.removeShareds(fileKey, state));
                        }
                    })
                    await Promise.all(promiseList);
                    react_component.setState(state);
                }}  >Delete All</a>
            </div>}
            // footer={<div>Footer</div>}
            bordered
            dataSource={keys}
            renderItem={item => <List.Item
                actions={[<a onClick={() => {
                    react_component.removeShareds(item)
                }} key="list-loadmore-delete">Delete</a>]}
            >{item} </List.Item>}
        />}
    </div>
}
