
import React from 'react';
import { Modal, Table } from 'antd';
import RenderObject from '../comp/RenderObject';
import { Z_INDEX } from 'file/fileUtils';
import { ModalType } from 'util/utils';

export const NeoGraphItemInspectModal = (props) => {
    /**@type {import('../../../file/MdFiles').default}*/
    const react_component = props.react_component;
    const { modalType, modalData: { title, object, } } = react_component.state;
    const closeFunc = () => {
        react_component.setState({
            modalType: ModalType.None,
        });
    };
    return <Modal zIndex={Z_INDEX}
        title={title || "Modal"}
        visible={modalType !== ModalType.None}
        closable={true}
        onOk={closeFunc}
        onCancel={closeFunc}
        maskClosable={true}
        okText="OK"
        footer={null}
    >
        <RenderObject obj={object} rows={true} pagination={false} ></RenderObject>
    </Modal>;
}

export default (NeoGraphItemInspectModal);


