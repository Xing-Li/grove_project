const Fs = require("fs");
const Path = require("path");
const _ = require("lodash");
const Mongoose = require('mongoose');
const Async = require('async')
const Winston = require("winston");
const DailyRotateFile = require("winston-daily-rotate-file");

const Constant = require("../../Constant");
const Util = require("./../util/util");

const AuditLogDir = Path.join(Constant.TEMP_PATH, "auditLog");
if (!Fs.existsSync(AuditLogDir)) {
  Fs.mkdirSync(AuditLogDir, { recursive: true });
}

const LogLevelType = {
  audit: "audit",
  error: "error",
  warn: "warn",
  info: "info",
  debug: "debug",
};

const auditFileTransport = new DailyRotateFile(Object.assign({
  filename: "graphxr.%DATE%.log",
  dirname: AuditLogDir,
  datePattern: "YYYY-MM-DD",
  zippedArchive: true,
  maxSize: "500mb",
  maxFiles: "60d",
  level: LogLevelType.audit,
  format: Winston.format.combine(
    //only the audit
    Winston.format(function (info, opts) {
      if (info.level === LogLevelType.audit || info.private) {
        return info;
      } else {
        return false;
      }
    })(),
    Winston.format.printf(function ({ level, message }) {
      return message;
    })
  ),
}, Constant.AUDIT_LOGS));

const consoleTransport = new Winston.transports.Console({
  format: Winston.format.combine(
    //skip the audit
    Winston.format(function (info, opts) {
      if (info.level === LogLevelType.audit || info.private) {
        return false;
      }
      return info;
    })(),
    Winston.format.colorize(),
    Winston.format.simple()
  ),
  level: Constant.IS_PRODUCTION ? LogLevelType.error : LogLevelType.info,
  handleExceptions: false,
});

const logger = Winston.createLogger({
  levels: Object.keys(LogLevelType).reduce(function (levels, v, k) {
    levels[v] = k;
    return levels;
  }, {}),
  transports: [auditFileTransport, consoleTransport],
});

function handleMessages(messages) {
  if (!Array.isArray(messages)) {
    messages = [messages];
  }
  if (!Constant.IS_PRODUCTION) {
    messages = messages.map((m) => {
      if (_.isObject(m) && m.message) {
        return m.message;
      }
      return _.isObject(m) ? `${JSON.stringify(m, null, 2)}` : m;
    });
  }
  return messages.join(" ");
}

/**
 * @enum { String }
 */
const AuditActionTypes = {
  "Signup": "Signup",
  "Login": "Login",
  "Logout": "Logout",

  "UserAdded": "UserAdded",
  "UserDeleted": "UserDeleted",
  "UserModified": "UserModified",

  "UserManagerAdded": "UserManagerAdded",
  "UserManagerDeleted": "UserManagerDeleted",

  "ProjectAdded": "ProjectAdded",
  "ProjectDeleted": "ProjectDeleted",
  "ProjectModified": "ProjectModified",

  "ProjectViewAdded": "ProjectViewAdded",
  "ProjectViewDeleted": "ProjectViewDeleted",
  "ProjectViewModified": "ProjectViewModified",

  "ProjectShareOpened": "ProjectShareOpened",
  "ProjectShareClosed": "ProjectShareClosed",

  "ProjectShareUIModified": "ProjectShareUIModified",

  "ExtensionAdd": "ExtensionAdd",
  "ExtensionModified": "ExtensionModified",
  "ExtensionDelete": "ExtensionDelete",

  "DemoNeo4jAdd": "DemoNeo4jAdd",
  "DemoNeo4jModified": "DemoNeo4jModified",
  "DemoNeo4jDelete": "DemoNeo4jDelete",

  "Neo4jQuery": "Neo4jQuery",
  "SQLQuery": "SQLQuery",

  "GrovePinnedAdd": "GrovePinnedAdd",
  "GrovePinnedModified": "GrovePinnedModified",
  "GrovePinnedDelete": "GrovePinnedDelete",


}

/**
 * @enum { String }
 */
const AuditStatusTypes = {
  "Failed": "Failed",
  "Successful": "Successful"
}


function getObjWithFields(obj, keepFields) {
  obj = obj || {};
  if (_.isEmpty(keepFields) || !Array.isArray(keepFields)) {
    return obj;
  }
  let filterObj = {};
  _.uniq(['_id'].concat(keepFields)).forEach(key => {
    if (obj[key] !== undefined) {
      filterObj[key] = obj[key];
    }
  })
  return filterObj
}

/**
 * Audit
 * @param {Express.Request} req Express req object
 * @param {String} message Change user email example , email(xx@xx.com > yy@yy.com )
 * @param {AuditActionTypes} action Default with {AuditActionTypes.Login}
 * @param {AuditStatusTypes} status Default with {AuditStatusTypes.Successful}
 * @param {Object} newObj {username:"abc"}
 * @param {Object} oldObj {username:"abc"}
 * @description e.g output  
 * time,who,action,status,id,diff,message,path,ip,agent
 * 
 */
function audit(
  req,
  message = "",
  action = AuditActionTypes.Login,
  status = AuditStatusTypes.Successful,
  newObj = null,
  oldObj = null,
  keepFields = []
) {
  try {
    newObj = getObjWithFields(newObj || {}, keepFields);
    oldObj = getObjWithFields(oldObj || {}, keepFields);
    let time = new Date().format("yyyy-MM-dd hh:mm:ss");
    let who = req.user ? req.user.email : req.body?.username || req.query?.username;
    let path = req.originalUrl;
    let ip = Util.getIP(req);
    let agent = req.get("user-agent");
    //Auto handle the login & logout & signUp _id
    if (!newObj._id && [AuditActionTypes.Login, AuditActionTypes.Logout, AuditActionTypes.Signup].includes(action)) {
      newObj._id = who;
      oldObj._id = who;
    }
    let id = newObj._id || oldObj._id;
    message = String(message).replace(/\n|(\r\n)/ig, ' ');
    Async.waterfall(
      [
        (callback) => {
          let diff = Util.JSONDiff(newObj || {}, oldObj || {});
          callback(null, JSON.stringify(diff || ""));
        },
        (diff, callback) => {
          callback(
            null,
            [time, who, action, status, id, diff, message, path, ip, agent].map((str) => {
              return String(str).replace(/"/gi, '""');
            })
          );
        },
      ], (err, messages) => {
        logger.log({
          private: true,
          level: LogLevelType.audit,
          message: `"${messages.join('","')}"`,
        });
        if (err) {
          throw err;
        }
      }
    );
  } catch (err) {
    console.error(err);
    logger.log({
      level: LogLevelType.err,
      message: handleMessages([err.message]),
    });
  }
}

const _defaultFunc = (...messages) => {
  logger.log({
    level: LogLevelType[type],
    message: handleMessages(messages),
  });
}

module.exports = {
  AuditActionTypes,
  AuditStatusTypes,
  audit,
  getLogger: function (fileName) {
    let customLogger = {
      //Only for Tips
      audit: audit,
      log: _defaultFunc,

      error: _defaultFunc,
      warn: _defaultFunc,
      info: _defaultFunc,
      debug: _defaultFunc,
    };

    Object.keys(customLogger)
      .filter((type) => !['log', 'audit'].includes(type))
      .forEach((type) => {
        customLogger[type] = (...messages) => {
          return logger.log({
            level: LogLevelType[type],
            message: handleMessages((fileName ? [`[${fileName}] => `] : []).concat(messages)),
          });
        };
      });
    //set log as info
    customLogger.log = customLogger.info;
    return customLogger;
  },
  instance: logger
};
