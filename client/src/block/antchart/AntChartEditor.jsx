import { getNodesByLabel } from 'common/graphOps';
import _ from 'lodash';
import PropTypes from "prop-types";
import React from "react";
import { v4 as uuidv4, NIL as NIL_UUID } from 'uuid';
import { CategoryFromType, cell_shortcuts_keys, linkKeys, showConfirm, showMessage } from 'util/utils';
import actions from '../../define/Actions';
import AntChartApart from './AntChartApart';
import AntChartSetting from './AntChartSetting';
import { getCatogeries, NEW_CHART_CONFIG } from "./antchartUtils";
import { chartDataFunc, DefaultAntChartConfig } from './comp/OptionsTypes';
import { WidthProvider, Responsive } from "react-grid-layout";
import { DEFAULT_BREAK_POINTS, DEFAULT_COLS, DEFAULT_DATA_GRID, DEFAULT_ROW_HEIGHT, RenderType, RenderTypeDesc } from 'util/databaseApi';
import { getViewport } from 'util/helpers';
import commonData from 'common/CommonData';
import { AddIcon } from 'block/code/Icon';

const ResponsiveReactGridLayout = WidthProvider(Responsive);
const noSaveState = {
    readOnly: false,
    selectedKey: "",
    safeMode: false,
    selected: false,
    darkMode: false,
    reload: false,
}
/**
 * can edit multiple chart, antchart block component
 * @augments {Component<Props, State>}
 */
export default class AntChartEditor extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        data: PropTypes.object,
        saveData: PropTypes.func,
        api: PropTypes.object,
        readOnly: PropTypes.bool,
        editor: PropTypes.object,
    };
    static defaultProps = {};

    constructor(props) {
        super(props);
        const { readOnly, editor } = this.props;
        /**
         * @type {{
         *  chartConfigs: Object.<string, DefaultAntChartConfig>
         *  layouts: {},
         *}}
        */
        const data = this.props.data;
        let selectedKey = undefined;
        if (editor.runComplete && !readOnly && _.keys(data.chartConfigs).length === 1 &&
            data.chartConfigs[_.keys(data.chartConfigs)[0]].chartData.length === 0) {
            selectedKey = data.chartConfigs[_.keys(data.chartConfigs)].chartKey;
        }
        // let viewport = getViewport();
        // this.add(data.layouts, viewport, RenderType.Markdown, "add");
        this.state = _.assign({}, noSaveState, { readOnly, selectedKey, safeMode: commonData.getSafeMode(), darkMode: undefined, reload: true }, data);
        /**@type {AntChartSetting} will reference when Open the  AntChartSetting */
        this.settingRef = React.createRef();
        this.domRef = React.createRef();
        this.refreshGraphElement = document.createElement("div");
        this.shareDataElement = document.createElement("div");
        this.safeModeElement = document.createElement("div");
        this.darkModeElement = document.createElement("div");
    }

    onLayoutChange(layout, layouts) {
        const { chartConfigs } = this.state;
        let keys = _.keys(chartConfigs);
        _.each(layouts, (layoutArr, screenSize) => {//remove caches
            _.remove(layoutArr, (layout, index) => {
                return !~keys.indexOf(layout.i) && layout.i !== "add"
            })
        })
        // let viewport = getViewport();
        // this.add(layouts, viewport, RenderType.Markdown, "add")
        console.log(layout, layouts)
        this.setDatas({ layouts });
    }

    componentWillUnmount() {
        actions.deleteCache(this.refreshGraphElement);
        actions.deleteCache(this.shareDataElement);
        actions.deleteCache(this.safeModeElement);
        actions.deleteCache(this.darkModeElement);
        this.unmount = true;
    }

    componentDidMount() {
        const { api } = this.props;
        const { readOnly, selectedKey, chartConfigs } = this.state;
        actions.inspector(this.refreshGraphElement, [actions.types.REFRESH_GRAPH], (graphxrApi) => {
            const { editor } = this.props;
            const { selectedKey, chartConfigs } = this.state;
            let state = {};
            _.each(chartConfigs, (chartConfig, chartKey) => {
                const { selectedCategoryFrom, selectedCategory, selectedProperties, emptyFills, castColumns, joinColumns } = chartConfig;
                if (selectedCategoryFrom === CategoryFromType.main) {
                    let isMainContain = false;
                    getCatogeries(editor, CategoryFromType.main).map((value) => {
                        if (!isMainContain) {
                            isMainContain = selectedCategory === value;
                        }
                    })
                    if (isMainContain) {
                        let nodes = getNodesByLabel(selectedCategory);
                        let keysArr = [];
                        let data = nodes.map((n) => { keysArr[keysArr.length] = n.id; return n.data.detail.data });
                        data.keysArr = keysArr;
                        let chartData = chartDataFunc(data, selectedProperties, emptyFills, castColumns, joinColumns)
                        let chartConfigTmp = _.assign({}, chartConfig, { chartData })
                        this.saveChart(chartConfigTmp, state)
                    }
                }
            })
            !_.isEmpty(state) && this.setDatas(state);
        })
        actions.inspector(this.shareDataElement, [actions.types.SHARE_DATA], ({ ShareData, key }) => {
            const { editor } = this.props;
            const { selectedKey, chartConfigs } = this.state;
            let state = {};
            _.each(chartConfigs, (chartConfig, chartKey) => {
                const { selectedCategoryFrom, selectedCategory, selectedProperties, emptyFills, castColumns, joinColumns } = chartConfig;
                if (selectedCategoryFrom === CategoryFromType.shareData && selectedCategory === key) {
                    let isShareDataContain = false;
                    getCatogeries(editor, CategoryFromType.shareData).map((value) => {
                        if (!isShareDataContain) {
                            isShareDataContain = selectedCategory === value;
                        }
                    })
                    if (isShareDataContain) {
                        let data = ShareData[editor.getSelfModuleName()] && ShareData[editor.getSelfModuleName()][selectedCategory] || [];
                        let chartData = chartDataFunc(data, selectedProperties, emptyFills, castColumns, joinColumns)
                        let chartConfigTmp = _.assign({}, chartConfig, { chartData })
                        this.saveChart(chartConfigTmp, state)
                    }
                }
            })
            !_.isEmpty(state) && this.setDatas(state);
        })
        actions.inspector(this.safeModeElement, [actions.types.SAFE_MODE], (safeMode) => {
            this.setState({ safeMode });
        });
        actions.inspector(this.darkModeElement, [actions.types.DARK_MODE], (darkMode) => {
            if (this.state.darkMode !== darkMode) {
                this.setState({ darkMode: darkMode });
            }
        })
        if (!readOnly && selectedKey) {
            document.querySelector(`.chart-wrapper.chart-${selectedKey}`) &&
                document.querySelector(`.chart-wrapper.chart-${selectedKey}`).scrollIntoView({
                    behavior: "smooth", block: "center", inline: "nearest"
                })
            actions.variable(actions.types.CHART_EDITOR, [], () => {
                return this;
            })
        }
    }

    componentDidUpdate(prevProps, prevState) {
    }

    run = (cb, reload = true) => {
        if (this.settingRef.current) {
            let chartConfig = this.settingRef.current.state.chartConfig;
            const { chartConfigs, selectedKey } = this.state;
            let state = { reload };
            let newKey = this.saveChart(chartConfig, state);
            if (newKey) {
                if (newKey !== selectedKey) {
                    if (selectedKey) {
                        delete chartConfigs[selectedKey];
                        _.assign(state, { chartConfigs })
                    }
                    _.assign(state, { selectedKey: newKey });
                }
            }
            this.setDatas(state);
            cb && cb()
        }
    }

    publish = (chartConfig) => {
        const { chartConfigs, selectedKey } = this.state;
        let state = {};
        let newKey = this.saveChart(chartConfig, state);
        if (newKey) {
            if (newKey !== selectedKey) {
                if (selectedKey) {
                    delete chartConfigs[selectedKey];
                    _.assign(state, { chartConfigs })
                }
            }
            _.assign(state, { selectedKey: undefined })
            this.setDatas(state);
        }
    }


    focus = () => {
        if (this.settingRef.current) {
            let chartConfig = this.settingRef.current.state.chartConfig;
            const { chartConfigs, selectedKey } = this.state;
            selectedKey && this.domRef.current.querySelector(`.chart-${selectedKey}`).scrollIntoView({
                behavior: "auto", block: "start", inline: "nearest"
            })
        }
    }

    cancelEdit = async () => {
        if (this.settingRef.current) {
            /**@type {AntChartSetting} */
            let settings = this.settingRef.current;
            const { chartConfig, oldChartConfig } = settings.state;
            if (!_.isEqual(chartConfig, oldChartConfig)) {
                if (await showConfirm("Cancel Current Chart Settings?",
                    { okText: "YES", cancelText: "NO" })) {
                    this.publish(oldChartConfig);
                } else {
                    this.publish(chartConfig);
                }
            } else {
                this.setDatas({ selectedKey: undefined })
            }
        }
    }

    publishEdit = () => {
        if (this.settingRef.current) {
            /**@type {AntChartSetting} */
            let settings = this.settingRef.current;
            const { chartConfig, oldChartConfig } = settings.state;
            if (!_.isEqual(chartConfig, oldChartConfig)) {
                this.publish(chartConfig)
            } else {
                this.setDatas({ selectedKey: undefined })
            }
        }
    }

    /**
     * 
     * @param {DefaultAntChartConfig} chartConfig 
     * @param {{}} stateTmp transfer this means later save
     */
    saveChart = (chartConfig, stateTmp) => {
        if (!chartConfig) return;
        let state = stateTmp || {};
        const { chartConfigs } = this.state;
        chartConfigs[chartConfig.chartKey] = chartConfig;
        _.assign(state, { chartConfigs });
        !stateTmp && this.setDatas(state);
        return chartConfig.chartKey;
    }

    /**
     * need save
     * @param {DefaultAntChartConfig} chartConfig need save
     * @param {string} swSelectedKey 
     */
    switchChart = (chartConfig, swSelectedKey, stateTmp) => {
        const { chartConfigs, selectedKey } = this.state;
        let state = stateTmp || {};
        let newKey = this.saveChart(chartConfig, state);
        if (newKey) {
            if (newKey !== selectedKey) {
                if (selectedKey) {
                    delete chartConfigs[selectedKey];
                    _.assign(state, { chartConfigs });
                }
            }
            _.assign(state, { selectedKey: swSelectedKey })
            !stateTmp && this.setDatas(state);
        }
    }

    deleteChart = (chartKey) => {
        if (!chartKey) {
            return;
        }
        const { chartConfigs, selectedKey } = this.state;
        delete chartConfigs[chartKey];
        let state = { chartConfigs: chartConfigs };
        if (chartKey === selectedKey) _.assign(state, { selectedKey: undefined });
        this.setDatas(state)
    }

    /**
     * set and save
     * @param {{}} state 
     * @param {function} cb
     */
    setDatas = (state, cb) => {
        const { saveData, readOnly } = this.props;
        this.setState(state, () => {
            if (_.has(state, "selectedKey")) {
                actions.variable(actions.types.CHART_EDITOR, [], () => {
                    return state.selectedKey ? this : undefined;
                })
            }
            if (_.filter(state, (value, key) => {
                return !_.has(noSaveState, key)
            }).length === 0 || readOnly) {
                return;
            }
            let dataTmp = {};
            _.each(_.keys(this.state), (key) => {
                if (!_.has(noSaveState, key)) {
                    if (key === 'chartConfigs') {
                        dataTmp.chartConfigs = _.reduce(this.state.chartConfigs, (prev, chartConfig, chartKey) => {
                            prev[chartKey] = _.reduce(chartConfig, (p, v, k) => {
                                if (k === "chartData" && v.length > 0) {
                                    p[k] = !chartConfig.active && chartConfig.renderType === RenderType.Chart ? v : [];
                                } else if (k === "chartColumns") {
                                    p[k] = !chartConfig.active && chartConfig.renderType === RenderType.Chart ? v : [];
                                } else if (k === "options") {
                                    p[k] = _.reduce(chartConfig.options, (prev2, value2, key2) => {
                                        if (key2 === "data") {
                                            prev2[key2] = [];
                                        } else {
                                            prev2[key2] = _.clone(value2);
                                        }
                                        return prev2;
                                    }, {})
                                } else {
                                    p[k] = _.clone(v);
                                }
                                return p;
                            }, {});
                            return prev;
                        }, {});
                    } else {
                        dataTmp[key] = _.clone(this.state[key]);
                    }
                }
            })
            saveData(dataTmp);
            // showToast("save chart success!", 'warning');
            cb && cb()
        })
    }

    async newChart(duplicate) {
        const { chartConfigs, layouts } = this.state;
        let state = {};
        /**@type {DefaultAntChartConfig} */
        let nchartConfig = _.cloneDeep(duplicate || NEW_CHART_CONFIG);
        nchartConfig.createTime = new Date().getTime();
        nchartConfig.chartKey = uuidv4();
        chartConfigs[nchartConfig.chartKey] = nchartConfig;
        _.each(_.keys(DEFAULT_BREAK_POINTS), (viewport) => {
            if (!layouts[viewport]) {
                layouts[viewport] = [];
            }
            let layout = this.add(layouts, viewport, nchartConfig.renderType, nchartConfig.chartKey);
            layouts[viewport].push(layout);
        })
        _.assign(state, { chartConfigs: chartConfigs, selectedKey: nchartConfig.chartKey, layouts: _.clone(layouts) })
        if (this.settingRef.current) {
            /**@type {AntChartSetting} */
            let settings = this.settingRef.current;
            const { chartConfig, oldChartConfig } = settings.state;
            if (!_.isEqual(chartConfig, oldChartConfig)) {
                if (await showConfirm("Save Current Chart Settings?")) {
                    this.switchChart(chartConfig, nchartConfig.chartKey, state)
                } else {
                    this.switchChart(oldChartConfig, nchartConfig.chartKey, state)
                }
            } else {
                _.assign(state, { selectedKey: nchartConfig.chartKey })
            }
        }
        this.setDatas(state, () => {
            setTimeout(() => { this.focus() })
        });
    }

    add(layouts, viewport, renderType, chartKey) {
        let removes = _.filter(_.remove(layouts[viewport], (layout) => {
            return layout.i === chartKey || layout.i === "add";
        }), (rLayout) => {
            return rLayout.i === chartKey;
        });
        let currentLayout;
        if (removes.length === 0) {
            currentLayout = {
                w: 3,
                h: 4,
                x: 0,
                y: 0,
                i: chartKey,
                minW: 2,
                minH: 3,
                isResizable: chartKey !== "add",
                isDraggable: chartKey !== "add"
            }
        } else {
            currentLayout = removes[0];
        }
        let defaultDataGrid = _.cloneDeep(RenderTypeDesc[renderType].dataGrid && RenderTypeDesc[renderType].dataGrid[viewport] || DEFAULT_DATA_GRID[viewport]);
        if (!layouts[viewport]) {
            layouts[viewport] = [];
        }
        if (layouts[viewport].length) {
            let lastGrid = layouts[viewport][0];
            let maxHs = new Array(DEFAULT_COLS[viewport]);
            maxHs.fill(0);
            _.each(layouts[viewport], (layout) => {
                for (let i = 0; i < layout.w; i++) {
                    if (layout.y + layout.h > maxHs[layout.x + i]) {
                        maxHs[layout.x + i] = layout.y + layout.h;
                    }
                }
                if (layout.y > lastGrid.y) {
                    lastGrid = layout;
                } else if (layout.y === lastGrid.y) {
                    if (layout.x > lastGrid.x) {
                        lastGrid = layout;
                    }
                }
            });
            let x = lastGrid.x + lastGrid.w;
            if (x + defaultDataGrid.w > DEFAULT_COLS[viewport]) {
                x = 0;
            }
            let y = 0;
            for (let i = 0; i < defaultDataGrid.w; i++) {
                if (y < maxHs[x + i]) {
                    y = maxHs[x + i];
                }
            }
            defaultDataGrid.x = x;
            defaultDataGrid.y = y;
        }
        _.assign(currentLayout, defaultDataGrid);
        if (chartKey === "add") {
            layouts[viewport].push(currentLayout);
        }
        return currentLayout;
    }

    async copyChart(chartKey) {
        const { chartConfigs } = this.state;
        if (!chartKey || !chartConfigs[chartKey]) {
            return;
        }
        window.copyChart = _.cloneDeep(chartConfigs[chartKey]);
        showMessage(`Chart copied. Click **Paste Chart(${linkKeys(cell_shortcuts_keys["New Sub Block"])})** menu to paste.`)
    }

    async duplicateChart(chartKey) {
        const { chartConfigs } = this.state;
        if (!chartKey || !chartConfigs[chartKey]) {
            return;
        }
        await this.newChart(chartConfigs[chartKey]);
    }

    render() {
        const { chartConfigs, selectedKey, readOnly, layouts, darkMode, reload } = this.state;
        const { api } = this.props;
        let viewport = getViewport();
        return <div className={`main chart ${readOnly ? "read-only" : ""}`} ref={this.domRef}>
            {/* {_.isEmpty(chartConfigs) && <div>Haven't charts now, please Add Chart Here</div>} */}
            <ResponsiveReactGridLayout
                draggableCancel=".grid-drag-cancel"
                className="layout"
                cols={DEFAULT_COLS}
                breakpoints={DEFAULT_BREAK_POINTS}
                rowHeight={DEFAULT_ROW_HEIGHT}
                layouts={layouts}
                onLayoutChange={(layout, layouts) =>
                    this.onLayoutChange(layout, layouts)
                }
            >
                {_.map(chartConfigs, (chartConfig, index, sChartConfigs) => {
                    const { renderType } = chartConfig;
                    let moreProps = {};
                    let filts = _.filter(layouts[viewport], (layout) => {
                        return layout.i === chartConfig.chartKey
                    });
                    if (filts.length === 0) {
                        let defaultDataGrid = this.add(layouts, viewport, renderType, chartConfig.chartKey);
                        _.assign(moreProps, { "data-grid": defaultDataGrid })
                    } else {
                        _.assign(moreProps, { "data-grid": filts[0] })
                    }
                    return <div key={chartConfig.chartKey} className={`chart-${chartConfig.chartKey} ${selectedKey === chartConfig.chartKey ? "selected" : ""}`}
                        {...moreProps}>
                        <AntChartApart darkMode={darkMode} reload={reload} chartEditor={this} chartConfig={chartConfig} currentDataGrid={moreProps['data-grid']}>
                        </AntChartApart>
                    </div>
                })}
                {!readOnly && <div key="add" className={`chart-add`} data-grid={this.add(layouts, viewport, RenderType.Markdown, "add")}>
                    <AddIcon className="text" onClick={() => {
                        this.newChart();
                    }} />
                </div>}
            </ResponsiveReactGridLayout>
        </div >
    }
}
AntChartEditor.propTypes = {
    api: PropTypes.shape({
        listeners: PropTypes.shape({
            on: PropTypes.func
        })
    }),
    data: PropTypes.any,
    readOnly: PropTypes.any,
    saveData: PropTypes.func,
}
