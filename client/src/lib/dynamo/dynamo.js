import {
    DynamoDBClient, ListTablesCommand, DescribeTableCommand, QueryCommand, ExecuteStatementCommand, BatchExecuteStatementCommand, ScanCommand
} from '@aws-sdk/client-dynamodb';
import { DynamoDBDocumentClient } from "@aws-sdk/lib-dynamodb";
import _ from 'lodash';
import { DynamoOperationType, assembleErrMessage } from 'util/utils';

const parseJSON = function (json = "") {
    try {
        return JSON.parse(json);
    } catch (e) {
        return null;
    }
};

/**
 * @param {string} url
 */
const DynamoClient = function (url, proxyConfig) {
    this.connString = url;
    this.proxyConfig = proxyConfig;
    this.db = new DynamoDBClient({
        credentials: {
            accessKeyId: this.proxyConfig.accessKeyId,
            secretAccessKey: this.proxyConfig.secretAccessKey,
        }, region: this.proxyConfig.region
    })
    const marshallOptions = {
        // Whether to automatically convert empty strings, blobs, and sets to `null`.
        convertEmptyValues: false, // false, by default.
        // Whether to remove undefined values while marshalling.
        removeUndefinedValues: false, // false, by default.
        // Whether to convert typeof object to map attribute.
        convertClassInstanceToMap: false, // false, by default.
    };

    const unmarshallOptions = {
        // Whether to return numbers as a string instead of converting them to native JavaScript numbers.
        wrapNumbers: false, // false, by default.
    };

    const translateConfig = { marshallOptions, unmarshallOptions };
    // Create the DynamoDB document client.
    this.ddbDocClient = DynamoDBDocumentClient.from(this.db, translateConfig);
}

DynamoClient.prototype.query =
    /**
     * 
     * @param {string} sql 
     * @param {*} p 
     * @returns 
     */
    async function (sql, p) {
        if (!sql) {
            return assembleErrMessage({ error: "Sql Empty!" });
        }
        sql = _.reduce(sql.trim().split("\n"), (prev, line, index) => {
            line = line.trim();
            if (line && !line.startsWith("--")) {
                prev += line;
            }
            return prev;
        }, "")
        if (/^select\s+/i.test(sql)) {
            try {
                let match = sql.match(/(\s+limit\s+(\d+))(\s+)?$/i)
                let limit;
                if (null !== match && match.length === 4) {
                    limit = parseInt(match[2])
                    sql = sql.substring(0, sql.length - match[0].length);
                } else {
                    limit = 100;
                }
                const data = await this.ddbDocClient.send(new ExecuteStatementCommand(_.assign({
                    Statement: sql,
                    Limit: limit,
                    ConsistentRead: true,
                }, limit ? { Limit: limit } : {})));
                return data
            } catch (err) {
                return assembleErrMessage({ error: JSON.stringify(err) });
            }
        }
        const sqlJSON = parseJSON(sql);
        if (!sqlJSON) {
            return assembleErrMessage({ error: "Sql Error!" });
        }
        if (typeof sqlJSON !== 'object') {
            return assembleErrMessage({ error: "Sql Type Error" });
        }
        const { operationType, tableName, id, params } = sqlJSON;
        console.log(sql)
        if (!this.db) {
            return assembleErrMessage({ error: "Server Busy!" });
        }
        switch (operationType || _.values(DynamoOperationType)[0]) {
            case DynamoOperationType.ListTables:
                try {
                    const data = await this.db.send(new ListTablesCommand({}));
                    return data.TableNames
                } catch (err) {
                    return assembleErrMessage({ error: JSON.stringify(err) });
                }
            case DynamoOperationType.DescribeTable:
                try {
                    const data = await this.db.send(new DescribeTableCommand({ TableName: tableName }));
                    return data.Table
                } catch (err) {
                    return assembleErrMessage({ error: JSON.stringify(err) });
                }
            case DynamoOperationType.Query:
                try {
                    const data = await this.db.send(new QueryCommand(params));
                    return data;
                } catch (err) {
                    return assembleErrMessage({ error: JSON.stringify(err) });
                }
            case DynamoOperationType.ExecuteStatement:
                try {
                    const data = await this.ddbDocClient.send(new ExecuteStatementCommand(params));
                    return data
                } catch (err) {
                    return assembleErrMessage({ error: JSON.stringify(err) });
                }
            case DynamoOperationType.BatchExecuteStatement:
                try {
                    const data = await this.ddbDocClient.send(
                        new BatchExecuteStatementCommand(params)
                    );
                    return data.Responses
                } catch (err) {
                    return assembleErrMessage({ error: JSON.stringify(err) });
                }
            case DynamoOperationType.Scan:
                try {
                    const data = await this.db.send(new ScanCommand(params));
                    return data;
                } catch (err) {
                    return assembleErrMessage({ error: JSON.stringify(err) });
                }
            default:
                return assembleErrMessage({ error: "Operation type not support!" });
        }
    };

DynamoClient.prototype.close = function () {
    if (!this.db) {
        return;
    }
    this.ddbDocClient.destroy();
    this.db.destroy();
    console.log(` ${this.connString} have ended`);
}

export default DynamoClient;