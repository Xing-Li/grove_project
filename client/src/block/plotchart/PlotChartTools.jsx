import {
    Button,
    Cascader,
    DatePicker,
    Form,
    Input,
    InputNumber,
    Radio,
    Select,
    Switch,
    TreeSelect,
    Checkbox,
    Row,
    Col,
} from 'antd';
import { colorSchemes } from 'block/code/Icon';
import _ from 'lodash';
import { nanOrBlank } from 'util/helpers'
import React, { useState } from 'react';
import { CategoryFromType, IsKey, confirm_key } from 'util/utils';

const CheckboxGroup = Checkbox.Group;

export const TemplateMarksValues = {
    xf: '',
    x: '',
    yf: '',
    y: '',
    colorf: '',
    color: '',
    sizef: '',
    size: '',
    fx: '',
    fy: '',
    mark: ''
}

export const TemplateValues = {
    title: '',
    subtitle: '',
    caption: '',
    height: '',
    widthType: 'auto',
    width: 300,

    marginTop: '',
    marginRight: '',
    marginBottom: '',
    marginLeft: '',

    insetTop: '',
    insetRight: '',
    insetBottom: '',
    insetLeft: '',


    xLabel: '',
    xScale: '',
    xOptions: [],

    yLabel: '',
    yScale: '',
    yOptions: [],

    colorLegend: true,
    colorScheme: '',
    colorScale: '',
    colorOptions: []
}
export function extractMarks(copy = false, moduleName, selectedCategoryFrom, selectedCategory, marks = TemplateMarksValues, /**@type {Object.<string,{type:""}>} */ dataTypes, columnsData = { properties: [], columns: [], data: [] }) {
    const { mark, xf, x, yf, y, color, colorf, size, sizef, fx, fy } = marks;
    if (isDefined(x) && !dataTypes[x]) {
        return "";
    }
    if (isDefined(y) && !dataTypes[y]) {
        return "";
    }
    let aMark = autoMark(marks, dataTypes);
    if (!aMark) {
        return "";
    }

    let aXf = autoXf(marks);
    let aYf = autoYf(marks);

    let mk;
    let group = undefined;
    let kvMap = { x, y, r: size, fx, fy }
    let addRule = undefined;
    if (aMark === 'bar') {
        if (isDefined(x) && isDefined(y) && ~['date', 'string'].indexOf(dataTypes[x].type) && !~['date', 'string'].indexOf(dataTypes[y].type)) {
            mk = 'barY';
            addRule = `Plot.ruleY([0])`
        } else if (isDefined(x) && isDefined(y) && !~['date', 'string'].indexOf(dataTypes[x].type) && ~['date', 'string'].indexOf(dataTypes[y].type)) {
            mk = 'barX';
            addRule = `Plot.ruleX([0])`
        } else if (isDefined(x) && !isDefined(y) && ~['date', 'number'].indexOf(dataTypes[x].type)) {
            mk = 'rectY';
            group = 'binX';
            addRule = `Plot.ruleY([0])`
        } else if (!isDefined(x) && isDefined(y) && ~['date', 'number'].indexOf(dataTypes[y].type)) {
            mk = 'rectX';
            group = 'binY';
            addRule = `Plot.ruleX([0])`
        } else if (isDefined(x) && isDefined(y) && ~['date', 'number'].indexOf(dataTypes[x].type) && !~['date', 'number'].indexOf(dataTypes[y].type)) {
            mk = 'rectY';
            group = 'binX';
            addRule = `Plot.ruleY([0])`
        } else if (isDefined(x) && isDefined(y) && ~['date', 'number'].indexOf(dataTypes[y].type) && !~['date', 'number'].indexOf(dataTypes[x].type)) {
            mk = 'rectX';
            group = 'binY';
            addRule = `Plot.ruleX([0])`
        } else if (isDefined(x) && isDefined(y) && ~['date', 'number'].indexOf(dataTypes[y].type) && ~['date', 'number'].indexOf(dataTypes[x].type)) {
            mk = 'rect';
            group = 'bin';
        } else {
            mk = 'cell'
        }
        kvMap.fill = color;
    } else if (aMark === 'line') {
        if (isDefined(x) && isDefined(y) && ~['date', 'string'].indexOf(dataTypes[x].type) && !~['date', 'string'].indexOf(dataTypes[y].type)) {
            mk = 'lineY';
        } else if (isDefined(x) && isDefined(y) && !~['date', 'string'].indexOf(dataTypes[x].type) && ~['date', 'string'].indexOf(dataTypes[y].type)) {
            mk = 'lineX';
        } else if (isDefined(x) && !isDefined(y) && ~['date', 'number'].indexOf(dataTypes[x].type)) {
            mk = 'lineY';
            group = 'binX';
        } else if (!isDefined(x) && isDefined(y) && ~['date', 'number'].indexOf(dataTypes[y].type)) {
            mk = 'lineX';
            group = 'binY';
        } else if (isDefined(x) && isDefined(y) && ~['date', 'number'].indexOf(dataTypes[x].type) && !~['date', 'number'].indexOf(dataTypes[y].type)) {
            mk = 'lineX';
            group = 'binY';
        } else if (isDefined(x) && isDefined(y) && ~['date', 'number'].indexOf(dataTypes[y].type) && !~['date', 'number'].indexOf(dataTypes[x].type)) {
            mk = 'lineX';
            group = 'binY';
        } else if (isDefined(x) && isDefined(y) && ~['date', 'number'].indexOf(dataTypes[y].type) && ~['date', 'number'].indexOf(dataTypes[x].type)) {
            mk = 'line';
            group = 'bin';
        } else {
            mk = 'line'
        }
        kvMap.stroke = color;
    } else if (aMark === 'area') {
        if (isDefined(x) && isDefined(y) && ~['date', 'string'].indexOf(dataTypes[x].type) && !~['date', 'string'].indexOf(dataTypes[y].type)) {
            mk = 'areaY';
            addRule = `Plot.ruleY([0])`
        } else if (isDefined(x) && isDefined(y) && !~['date', 'string'].indexOf(dataTypes[x].type) && ~['date', 'string'].indexOf(dataTypes[y].type)) {
            mk = 'areaX';
            addRule = `Plot.ruleX([0])`
        } else {
            mk = 'areaY'
        }
        kvMap.fill = color;
    } else if (aMark === 'rule') {
        if (isDefined(x) && isDefined(y) && ~['date', 'string'].indexOf(dataTypes[x].type) && !~['date', 'string'].indexOf(dataTypes[y].type)) {
            mk = 'ruleX';
            addRule = `Plot.ruleY([0])`
        } else if (isDefined(x) && isDefined(y) && !~['date', 'string'].indexOf(dataTypes[x].type) && ~['date', 'string'].indexOf(dataTypes[y].type)) {
            mk = 'ruleY';
            addRule = `Plot.ruleX([0])`
        } else {
            mk = 'ruleX';
            addRule = `Plot.ruleY([0])`
        }
        kvMap.stroke = color;
    } else {
        mk = aMark;
        kvMap.stroke = color;
    }
    let arr = _.reduce(kvMap, (prev, v, k) => {
        if (v) {
            prev.push(`${k}:"${v}"`)
        }
        return prev;
    }, []);
    let config = `{ ${arr.length ? `${arr.join(", ")}, ` : ""}tip: true}`
    if (aXf) {
        config = `Plot.${group || 'groupY'}({ x: "${aXf}" }, ${config})`
    } else if (aYf) {
        config = `Plot.${group || 'groupX'}({ y: "${aYf}" }, ${config})`
    } else if (group === 'bin' && isDefined(colorf)) {
        config = `Plot.${group}({ fill: "${colorf}" }, ${config})`
    }
    let vName = "";
    if (copy) {
        switch (selectedCategoryFrom) {
            case CategoryFromType.main:
                vName = `selectMainDatas(`;
                break;
            case CategoryFromType.files:
                vName = `getColumnsData("${moduleName}", "${selectedCategoryFrom}", `;
                break;
            case CategoryFromType.shareData:
                vName = `selectShareDataDatas("${moduleName}", `;
                break;
            default:
                vName = `getColumnsData("${moduleName}", "${selectedCategoryFrom}", `;
                break
        }
    } else {
        vName = `getColumnsData("${moduleName}", "${selectedCategoryFrom}", `
    }
    return `\tmarks: [
${isDefined(fx) || isDefined(fy) ? "\t\tPlot.frame({ strokeOpacity: 0.1 }),\n" : ""}\t\tPlot.${mk}((await ${vName}"${selectedCategory}", true)).data,
\t\t\t${config}),
\t\t${addRule || ""}
\t]`;

}
export function extractCustomise(customise = TemplateValues) {
    let obj = _.reduce(customise, (prev, value, key, list) => {
        if ("widthType" === key) {
            return prev;
        } else if ("width" === key && "value" !== list.widthType) {
            if ("full" === list.widthType) {
                _.set(prev, "width", "width");
            }
            return prev;
        }
        /**@type{Array} */
        let arr = key.camelPeakToBlankSplit().split(" ");
        if (~["xOptions", "yOptions", "colorOptions"].indexOf(key)) {
            if (value instanceof Array && value.length > 0) {
                _.each(value, (v, i) => {
                    let path = [arr[0], v].join(".").toLowerCase();
                    _.set(prev, path, true);
                })
            }
            return prev;
        }
        let path = ~["margin", "inset"].indexOf(arr[0]) ? key : arr.join(".").toLowerCase();
        if (!nanOrBlank(value)) {
            _.set(prev, path, value);
        }
        return prev;
    }, {})
    return _.isEmpty(obj) ? "" : _.map(obj, (v, k) => {
        return `\t${k}:${v === "width" ? v : JSON.stringify(v)}`;
    }).join(",\n") + ",\n"
}

const styleNumberInput = { width: 80 }
export class CustomiseComp extends React.Component {
    constructor(props = {
        values: _.cloneDeep(TemplateValues)
    }) {
        super(props);
        this.props = props;
    }

    changeValues = (updateObj) => {
        const { setValues, values } = this.props;
        setValues(_.assign({}, values, updateObj));
    }

    InputComp = ({ kk }) => {
        let value = this.props[kk];
        return <Input style={{ width: 120 }} value={value} allowClear
            onChange={(e) => {
                let k = String.fromCharCode(e.keyCode).toLowerCase();
                //filter the number & char key
                if (!(/^[0-9a-z]$/g).test(k) || e.key === e.code) {
                    k = e.key;
                }
                if (IsKey(e, k, confirm_key) && value !== e.target.value) {
                    this.changeValues({ [kk]: e.target.value })
                }
            }} onBlur={(e) => {
                if (value !== e.target.value) {
                    this.changeValues({ [kk]: e.target.value })
                }
            }}></Input>
    }

    InputNumberComp = ({ kk = '', minV = 10, maxV = 10000, step = 1, ...propsa }) => {
        let v = this.props[kk];
        return <InputNumber min={minV} max={maxV} value={v} controls={false} {...propsa}
            onPressEnter={(e) => {
                let value = Number.isInteger(step) ? parseInt(e.target.value) : parseFloat(e.target.value);
                if (v !== value) {
                    this.changeValues({ [kk]: value })
                }
            }}
            onBlur={(e) => {
                let value = Number.isInteger(step) ? parseInt(e.target.value) : parseFloat(e.target.value);
                if (v !== value) {
                    this.changeValues({ [kk]: value })
                }
            }} step={step}></InputNumber>
    }

    render() {
        const { title,
            subtitle,
            caption,
            height,
            widthType,
            width,
            marginTop,
            marginRight,
            marginBottom,
            marginLeft,
            insetTop,
            insetRight,
            insetBottom,
            insetLeft,
            xLabel,
            xScale,
            xOptions,
            yLabel,
            yScale,
            yOptions,
            colorLegend,
            colorScheme,
            colorScale,
            colorOptions } = this.props.values;
        return <div style={{ width: "500px", maxHeight: "300px", overflowY: "auto" }}>
            <Form
                className='plot-customise-form'
                labelCol={{
                    span: 6,
                }}
                wrapperCol={{
                    span: 18,
                }}
                layout="horizontal"
                size={'small'}
            >
                <Form.Item label="Title">
                    <this.InputComp kk={"title"} />
                </Form.Item>
                <Form.Item label="Subtitle">
                    <this.InputComp kk={"subtitle"} />
                </Form.Item>
                <Form.Item label="Caption">
                    <this.InputComp kk={"caption"} />
                </Form.Item>
                <Form.Item label="Height">
                    <this.InputNumberComp kk={"height"} min={10} max={10000} step={1} />
                </Form.Item>

                <Form.Item label="Width">
                    <Radio.Group value={widthType} onChange={(e) => {
                        this.changeValues({ widthType: e.target.value })
                    }}>
                        <div className='d-flex'>
                            <Radio value="auto">Auto</Radio>
                            <Radio value="value">
                                <this.InputNumberComp kk={"width"} disabled={widthType !== 'value'} style={{ width: 50 }} min={10} max={10000} step={1} />
                            </Radio>
                            <Radio value="full">Full</Radio>
                        </div>
                    </Radio.Group>
                </Form.Item>
                <Form.Item label={" "} colon={false}>
                    <div className='d-flex'>
                        <div style={styleNumberInput} >Top</div>
                        <div style={styleNumberInput} >Right</div>
                        <div style={styleNumberInput} >Bottom</div>
                        <div style={styleNumberInput} >Left</div>
                    </div>
                </Form.Item>

                <Form.Item label="Margin">
                    <div className='d-flex'>
                        <this.InputNumberComp kk={"marginTop"} min={0} max={500} step={1} style={styleNumberInput} />
                        <this.InputNumberComp kk={"marginRight"} min={0} max={500} step={1} style={styleNumberInput} />
                        <this.InputNumberComp kk={"marginBottom"} min={0} max={500} step={1} style={styleNumberInput} />
                        <this.InputNumberComp kk={"marginLeft"} min={0} max={500} step={1} style={styleNumberInput} />
                    </div>
                </Form.Item>
                <Form.Item label="Inset">
                    <div className='d-flex'>
                        <this.InputNumberComp kk={"insetTop"} min={0} max={500} step={1} style={styleNumberInput} />
                        <this.InputNumberComp kk={"insetRight"} min={0} max={500} step={1} style={styleNumberInput} />
                        <this.InputNumberComp kk={"insetBottom"} min={0} max={500} step={1} style={styleNumberInput} />
                        <this.InputNumberComp kk={"insetLeft"} min={0} max={500} step={1} style={styleNumberInput} />
                    </div>
                </Form.Item>

                <Form.Item label="Legend" valuePropName='checked'  >
                    <Switch checked={colorLegend} checkedChildren="Auto" unCheckedChildren="None" onChange={(checked) => {
                        this.changeValues({ colorLegend: checked });
                    }} />
                </Form.Item>

                <Form.Item label="X label" >
                    <this.InputComp kk={"xLabel"} />
                </Form.Item>
                <Form.Item label="X scale" >
                    <Select value={xScale}
                        style={{ width: 120 }}
                        onChange={(e) => {
                            this.changeValues({ xScale: e })
                        }}
                        options={_.reduce(["linear", "log", "sqrt", "symlog", "point", "band"], (prev, curr, index) => {
                            prev.push({
                                value: curr,
                                label: curr,
                            })
                            return prev;
                        }, [{
                            value: "",
                            label: "Auto",
                        }])}
                    />
                </Form.Item>
                <Form.Item label="X options" >
                    <CheckboxGroup value={xOptions} options={["Grid", "Reverse"]} onChange={(e) => {
                        this.changeValues({ xOptions: e })
                    }} />
                </Form.Item>

                <Form.Item label="Y label" >
                    <this.InputComp kk={"yLabel"} />
                </Form.Item>
                <Form.Item label="Y scale" >
                    <Select value={yScale}
                        style={{ width: 120 }}
                        onChange={(e) => {
                            this.changeValues({ yScale: e })
                        }}
                        options={_.reduce(["linear", "log", "sqrt", "symlog", "point", "band"], (prev, curr, index) => {
                            prev.push({
                                value: curr,
                                label: curr,
                            })
                            return prev;
                        }, [{
                            value: "",
                            label: "Auto",
                        }])}
                    />
                </Form.Item>
                <Form.Item label="Y options" >
                    <CheckboxGroup value={yOptions} options={["Grid", "Reverse"]} onChange={(e) => {
                        this.changeValues({ yOptions: e })
                    }} />
                </Form.Item>

                <Form.Item label="Color scheme" >
                    <Select value={colorScheme}
                        style={{ width: 120 }}
                        onChange={(e) => {
                            this.changeValues({ colorScheme: e })
                        }}
                        options={colorSchemes}
                    />
                </Form.Item>
                <Form.Item label="Color scale" >
                    <Select value={colorScale}
                        style={{ width: 120 }}
                        onChange={(e) => {
                            this.changeValues({ colorScale: e })
                        }}
                        options={_.reduce(["linear", "log", "sqrt", "symlog", "categorical", "ordinal"], (prev, curr, index) => {
                            prev.push({
                                value: curr,
                                label: curr,
                            })
                            return prev;
                        }, [{
                            value: "",
                            label: "Auto",
                        }])}
                    />
                </Form.Item>
                <Form.Item label="Color options" >
                    <CheckboxGroup value={colorOptions} options={["Reverse"]} onChange={(e) => {
                        this.changeValues({ colorOptions: e })
                    }} />
                </Form.Item>
            </Form>
        </div>

    }
}

const spans = { xs: 24, md: 12, lg: 6 };
const formulaOptions = [
    {
        label: 'Reducer',
        options: _.reduce(['count', 'distinct', 'sum', 'min', 'max', 'mean', 'median', 'mode'], (prev, curr) => {
            prev.push({
                label: curr,
                value: curr,
            })
            return prev;
        }, [
            {
                label: '-',
                value: '-',
            },
            {
                label: 'Auto',
                value: '',
            },
        ])
    }
]

const isDefined = function (e) {
    return !(e === '' || e === '-');
}


const autoXf = function (marks) {
    const { x, xf, y, yf, color, colorf, size, sizef, fx, fy, mark } = marks;
    if (isDefined(xf)) {
        return xf;
    }
    return isDefined(y) && !isDefined(x) ? 'count' : ''
}

const autoYf = function (marks) {
    const { x, xf, y, yf, color, colorf, size, sizef, fx, fy, mark } = marks;
    if (isDefined(yf)) {
        return yf;
    }
    return isDefined(x) && !isDefined(y) ? 'count' : ''
}
const autoMark = function (marks, dataTypes) {
    const { x, xf, y, yf, color, colorf, size, sizef, fx, fy, mark } = marks;
    if (isDefined(mark)) {
        return mark;
    }
    if (isDefined(x) && !dataTypes[x]) {
        return "";
    }
    if (isDefined(y) && !dataTypes[y]) {
        return "";
    }
    if (isDefined(y) && isDefined(x)) {
        if (dataTypes[x].type === 'date' | dataTypes[y].type === 'date' && dataTypes[x].type === 'number' | dataTypes[y].type === 'number') {
            return 'line'
        }
    }
    let aXf = autoXf(marks);
    let aYf = autoYf(marks);
    if (aXf === 'count' | aYf === 'count') {
        return 'bar';
    }
    return 'dot';
}

export class MarksComp extends React.Component {
    constructor(props = {
        marks: _.cloneDeep(TemplateMarksValues)
    }) {
        super(props);
        this.props = props;
    }

    changeMarks = (updateObj) => {
        const { setMarks, marks } = this.props;
        setMarks(_.assign({}, marks, updateObj));
    }

    render() {
        const { marks, marks: { x, xf, y, yf, color, colorf, size, sizef, fx, fy, mark }, dataTypes, setMarks } = this.props;
        const columnsOptions = [
            {
                label: 'Field',
                options: _.reduce(dataTypes, (prev, curr, k) => {
                    prev.push({
                        label: <div>{k}&nbsp;&nbsp;&nbsp;&nbsp;<span className='text-muted small'>{curr.type}</span></div>,
                        value: k,
                    })
                    return prev;
                }, [
                    {
                        label: '-',
                        value: '',
                    }
                ])
            }
        ]
        return <div>
            <Row gutter={[10, 5]}>
                <Col {...spans}>
                    <div className="d-flex justify-content-between mark-padding">
                        X
                        <Select
                            allowClear
                            value={autoXf(marks)}
                            style={{
                                width: 120,
                            }}
                            size='small'
                            onChange={(e) => {
                                if (undefined === e) {
                                    e = ''
                                }
                                let st = { xf: e }
                                if (isDefined(e)) {
                                    _.assign(st, { /* x: '', */ yf: '' });
                                }
                                this.changeMarks(st)
                            }}
                            options={formulaOptions}
                        />
                    </div>
                    <Select
                        allowClear
                        style={{ width: "100%" }}
                        value={x}
                        onChange={(e) => {
                            if (undefined === e) {
                                e = ''
                            }
                            let st = { x: e }
                            if (isDefined(e)) {
                                _.assign(st, { xf: '' });
                            }
                            this.changeMarks(st)
                        }}
                        options={columnsOptions}
                    />
                </Col>
                <Col {...spans}>
                    <div className="d-flex justify-content-between mark-padding">
                        Y
                        <Select
                            allowClear
                            value={autoYf(marks)}
                            style={{
                                width: 120,
                            }}
                            size='small'
                            onChange={(e) => {
                                if (undefined === e) {
                                    e = ''
                                }
                                let st = { yf: e }
                                if (isDefined(e)) {
                                    _.assign(st, { /* y: '', */ xf: '' });
                                }
                                this.changeMarks(st)
                            }}
                            options={formulaOptions}
                        />
                    </div>
                    <Select
                        allowClear
                        style={{ width: "100%" }}
                        value={y}
                        onChange={(e) => {
                            if (undefined === e) {
                                e = ''
                            }
                            let st = { y: e }
                            if (isDefined(e)) {
                                _.assign(st, { yf: '' });
                            }
                            this.changeMarks(st)
                        }}
                        options={columnsOptions}
                    />
                </Col>
                <Col {...spans}>
                    <div className="d-flex justify-content-between mark-padding">
                        Color
                        <Select
                            allowClear
                            value={colorf}
                            style={{
                                width: 120,
                            }}
                            size='small'
                            onChange={(e) => {
                                if (undefined === e) {
                                    e = ''
                                }
                                let st = { colorf: e }
                                if (isDefined(e)) {
                                    _.assign(st, { /* color: '' */ });
                                }
                                this.changeMarks(st)
                            }}
                            options={formulaOptions}
                        />
                    </div>
                    <Select
                        allowClear
                        style={{ width: "100%" }}
                        value={color}
                        onChange={(e) => {
                            if (undefined === e) {
                                e = ''
                            }
                            let st = { color: e }
                            if (isDefined(e)) {
                                _.assign(st, { colorf: '' });
                            }
                            this.changeMarks(st)
                        }}
                        options={columnsOptions}
                    />
                </Col>
                <Col {...spans}>
                    <div className="d-flex justify-content-between mark-padding">
                        Size
                        <Select
                            allowClear
                            value={sizef}
                            style={{
                                width: 120,
                            }}
                            size='small'
                            onChange={(e) => {
                                if (undefined === e) {
                                    e = ''
                                }
                                let st = { sizef: e }
                                if (isDefined(e)) {
                                    _.assign(st, { size: '' });
                                }
                                this.changeMarks(st)
                            }}
                            options={formulaOptions}
                        />
                    </div>
                    <Select
                        allowClear
                        style={{ width: "100%" }}
                        value={size}
                        onChange={(e) => {
                            if (undefined === e) {
                                e = ''
                            }
                            let st = { size: e }
                            if (isDefined(e)) {
                                _.assign(st, { sizef: '' });
                            }
                            this.changeMarks(st)
                        }}
                        options={columnsOptions}
                    />
                </Col>
                <Col {...spans}>
                    <div className="d-flex justify-content-between mark-padding">
                        Facet X
                    </div>
                    <Select
                        allowClear
                        style={{ width: "100%" }}
                        value={fx}
                        onChange={(e) => {
                            if (undefined === e) {
                                e = ''
                            }
                            this.changeMarks({ fx: e })
                        }}
                        options={columnsOptions}
                    />
                </Col>
                <Col {...spans}>
                    <div className="d-flex justify-content-between mark-padding">
                        Facet Y
                    </div>
                    <Select
                        allowClear
                        style={{ width: "100%" }}
                        value={fy}
                        onChange={(e) => {
                            if (undefined === e) {
                                e = ''
                            }
                            this.changeMarks({ fy: e })
                        }}
                        options={columnsOptions}
                    />
                </Col>
                <Col {...spans}>
                    <div className="d-flex justify-content-between mark-padding">
                        Mark
                    </div>
                    <Select
                        allowClear
                        style={{ width: "100%" }}
                        value={autoMark(marks, dataTypes)}
                        onChange={(e) => {
                            if (undefined === e) {
                                e = ''
                            }
                            this.changeMarks({ mark: e })
                        }}
                        options={[
                            {
                                label: 'Mark',
                                options: _.reduce(['bar', 'dot', 'line', 'area', 'rule'], (prev, curr) => {
                                    prev.push({
                                        label: curr,
                                        value: curr,
                                    })
                                    return prev;
                                }, [
                                    {
                                        label: 'Auto',
                                        value: '',
                                    }
                                ])
                            }
                        ]}
                    />
                </Col>
            </Row>
        </div>
    }

}