import React, { useEffect, useState } from 'react';
import { NoDrawableDataErrorMessage } from './CodeViewerComponent';
import { convertRecordObjectToString, recordToNative } from '../util/ChartUtils';
import DrawPlot from '../draw/DrawPlot'
import { getChartConfig } from '../comp/CheckChartTypes';
import { ChartTypes, extractDatas, extractOptions, getKeys } from '../comp/OptionsTypes';
import { extractColumnDatas } from '../comp/TableComp';
import { autoObjectsType } from 'util/helpers';

/**
 * Embeds a RadarChart (from Charts) into NeoDash.
 */
const NeoRadarChart = (props) => {
  if (
    !props.selection ||
    props.selection.values == undefined ||
    props.records == null ||
    props.records.length == 0 ||
    props.records[0].keys == null
  ) {
    return <NoDrawableDataErrorMessage />;
  }
  const { records, height } = props;
  const selection = props.selection ? props.selection : {};
  const settings = props.settings ? props.settings : {};

  const { columns, rows } = extractColumnDatas(false, records, true, true);
  let allData = autoObjectsType(rows, false);
  if (allData.length < 1) {
    return <NoDrawableDataErrorMessage />;
  }
  let first = allData[0];
  let allKeys = _.keys(first);
  let numbers = _.filter(allKeys, (key) => {
    return typeof first[key] === 'number' && _.filter(allData, (d) => { d[key] < 0 }).length === 0;
  });
  let strings = _.filter(allKeys, (key) => {
    return typeof first[key] === 'string'
  });
  if (strings.length < 1 || numbers.length < 3) {
    return <NoDrawableDataErrorMessage />;
  }
  let chartType;
  if (allData.length > 1) {
    chartType = ChartTypes['Multi Radar'];
  } else {
    chartType = ChartTypes['Basic Radar'];
  }

  if (!(selection.values && selection.values.length > 0 && selection.index && (typeof selection.index === 'string'))) {
    return <NoDrawableDataErrorMessage />;
  }
  let valid = true;
  let keys = selection.values.concat(selection.index)
  const data = allData.map((r) => {
    const entry = {};
    keys.forEach((k) => {
      if (k !== selection.index && isNaN(r[k])) {
        valid = false;
      }
      entry[k] = k !== selection.index ? (+r[k]) : `${r[k]}`;
    });
    return entry;
  });

  // If we find inconsitent data, return an error/
  if (!valid) {
    return <NoDrawableDataErrorMessage />;
  }

  const [heightValue, setHeightValue] = useState(height);
  useEffect(() => {
    setHeightValue(height);
  }, [height]);

  let datas = _.reduce(data, (prev, d, index) => {
    if (selection.values > 2) {
      _.each(selection.values, (k, ind) => {
        prev.push({
          xField: k, yField: d[k], seriesField: d[strings[0]],
        })
      })
    } else {
      _.each(selection.values, (k, ind) => {
        prev.push({
          xField: d[strings[0]], yField: d[k], seriesField: k,
        })
      })
    }
    return prev;
  }, [])
  let exOptions = _.assign({}, settings,
    {
      data: datas
    }
  );
  exOptions.xField = "xField"
  exOptions.yField = "yField"
  if (chartType === ChartTypes['Multi Radar']) {
    exOptions.seriesField = "seriesField"
  }
  const darkMode = document.body.classList.contains("darkmode");
  return (
    <DrawPlot height={heightValue} type={chartType} exOptions={exOptions} darkMode={darkMode}></DrawPlot>
  );
};

export default NeoRadarChart;
