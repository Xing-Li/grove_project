
export default class Node{
    constructor(){
        this.id = null
        this.index = 0
        this.degree = 0
        this.neighbor = [];
        this.position = null
        this.color = null
        this.alpha = 1.0;
        this.icon = 0;
        this.properties = {};
        this.tags = [];
        this.pinned = false;
        this.fixed = false;
        this.selected = false;
        this.highlight = false
        this.size = Math.pow(10, 0.2);
    }

    cloneFromNode(node){
        this.id = node.id
        this.index = node.index
        this.degree = node.degree
        this.neighbor = node.neighbor
        this.position = node.position
        this.color = node.color
        this.alpha = node.alpha
        this.icon = node.icon
        this.properties = node.data.detail.data
        this.category = node.data.detail.type
        this.tags = node.tags
        this.pinned = node.pinned
        this.fixed = node.fixed
        this.selected = node.selected
        this.highlight = node.highlight
        this.size = node.size
    }
}