import _ from 'lodash';

/**
 * 
 * @param {import ('commonEditor').CommonEditor} editor 
 */
var RenderTask = function (editor) {
    this.editor = editor;
    this.taskList = [];
    this.code_components = [];
    this.runOverTask = undefined;
    this.taskTimeout = undefined;
}
/**
 * stop render task, reset to init
 */
RenderTask.prototype.clearTasks = function () {
    if (this.taskTimeout !== undefined) {
        clearTimeout(this.taskTimeout);
    }
    this.taskList = [];
    this.code_components = [];
    this.runOverTask = undefined;
    this.taskTimeout = undefined;
}
/**
 * submit timeout task run after 1ms
 * @param {Function} task 
 */
RenderTask.prototype.submitTask = function (task) {
    if (!(typeof task === "function")) {
        console.error("task error!");
        return;
    }
    if (this.taskTimeout !== undefined) {
        this.taskList.push(task);
    } else {
        this.taskTimeout = setTimeout(() => {
            task(this.taskCallback.bind(this));
        })
    }
}
/**
 * submit task in queue to run
 */
RenderTask.prototype.taskCallback = async function (code_component) {
    this.taskTimeout = undefined;
    code_component && this.code_components.push(code_component);
    if (this.taskList.length > 0) {
        let task = this.taskList.splice(0, 1)[0];
        this.submitTask(task);
    } else {
        await Promise.all(_.map(this.code_components, /** @param {import ('../block/code/Code').default} component */(component) => {
            return component.runCode && component.runCode(false, false);
        }));
        await this.editor.run();
        await Promise.all(_.map(this.code_components, /** @param {import ('../block/code/Code').default} component */(component) => {
            return component.cacheRun && component.cacheRun();
        }));
        this.code_components.length = 0;
        if (this.runOverTask !== undefined) {
            setTimeout(() => {
                this.runOverTask && this.runOverTask();
                this.runOverTask = undefined;
            })
        }
    }
}
/**
 * 
 * @param {Function} task 
 */
RenderTask.prototype.submitRunOverTask = function (task) {
    if (!(typeof task === "function")) {
        console.error("task error!");
        return;
    }
    if (this.taskTimeout == undefined && this.taskList.length === 0) {
        setTimeout(() => {
            task();
        })
        return;
    }
    this.runOverTask = task;
}

export default RenderTask;