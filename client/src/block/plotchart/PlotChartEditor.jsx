import { Button, Col, Dropdown, Input, Menu, Row, Select, Spin } from "antd";
import React from "react";
import actions from "../../define/Actions";
import ChooseDatas from "../code/ChooseDatas";
import PropTypes from "prop-types";
import { AnchorType, CategoryFromType, copyContent, showMessage, swStyle } from "../../util/utils";
import { ErrorComp, withPopMenu } from "../code/CodeTools";
import { CustomiseComp, TemplateValues, extractCustomise, MarksComp, extractMarks, TemplateMarksValues } from './PlotChartTools'
import { CopyIcon, PinIcon, SlidersHIcon } from "block/code/Icon";
import RenderObject from "block/antchart/comp/RenderObject";
import { CloseOutlined, Loading3QuartersOutlined } from "@ant-design/icons";
import { FormulaIcon, SwapIcon } from "../code/Icon";
import { getColumnsData } from "block/antchart/antchartUtils";
import { calcAutoType, isVisibleInViewport } from "util/helpers";
import _ from "lodash";
import commonData from "common/CommonData";
import { fileSeparator } from "file/fileUtils";

export const PlotChartData = {
    /**user defined variant name */
    name: PropTypes.string,
    /**default name of block element ，uuidv4 string */
    dname: PropTypes.string,
    selectedCategoryFrom: PropTypes.string,
    selectedCategory: PropTypes.string,
    customise: _.cloneDeep(TemplateValues),
    marks: _.cloneDeep(TemplateMarksValues)
};

const noSaveState = {
    showCode: false,
    /** open edit */
    openEdit: false,
    /**  */
    needRun: true,
    readOnly: false,
    openSelect: false,
    safeMode: false,
    /**@type {Error} */
    error: undefined,
    /**@type {Map} */
    fileAttachments: undefined,
    columnsData: {
        properties: [],
        columns: [],
        data: [],
    },
    codeLoading: false,
};
const defState = {
    /**@type {Object.<string,{type:""}>} */
    dataTypes: {},
    pinCode: true,
}

export default class PlotChartEditor extends React.Component {
    static propTypes = {
        readOnly: PropTypes.bool,
        data: PropTypes.object,
        api: PropTypes.object,
        saveData: PropTypes.func,
        editor: PropTypes.object,
        settingWrapper: PropTypes.object,
    };

    static defaultProps = {};
    /**
     * Creates
     *
     * @constructor
     * @param {{
     *  api:import('@editorjs/editorjs').default,
     *  settingWrapper:HTMLElement,
     *  readOnly:boolean,
     * }} props
     */
    constructor(props) {
        super(props);
        this.props = props;
        const {
            readOnly,
            data,
            data: { selectedCategoryFrom, selectedCategory },
            editor,
        } = this.props;
        /**@type{import ('../../tagEditor').default} */
        this.editor = editor;
        /**@type {noSaveState & PlotChartData &defState } */
        this.state = _.assign(
            {},
            _.cloneDeep(noSaveState),
            {
                readOnly: readOnly,
                safeMode: commonData.getSafeMode(),
            },
            _.cloneDeep(defState),
            data
        )
        this.debounce = undefined;
        this.domRef = React.createRef();
        this.ref = React.createRef();
        this.safeModeElement = document.createElement("div");
        this.openSelectElement = document.createElement("div");
    }

    componentDidMount() {
        const { settingWrapper, api } = this.props;
        this.domRef.current.showCodeFunc = this.showCodeFunc.bind(this);
        this.domRef.current.editCode = this.editCode.bind(this);
        this.domRef.current.runCode = this.runCode.bind(this);
        this.domRef.current.state = this.state;
        this.domRef.current.getAnchorValue = this.getAnchorValue;
        actions.inspector(this.safeModeElement, [actions.types.SAFE_MODE], (safeMode) => {
            this.setState({ safeMode });
        });
        actions.inspector(this.openSelectElement, [actions.types.OPEN_SELECT], ({ editorKey, openSelect }) => {
            if (this.editor.getTransferKey() === editorKey) {
                this.setState({ openSelect });
            }
        });
        api.listeners.on(
            this.domRef.current.parentElement.parentElement.querySelector(".ce-block-border-left-wrap"),
            "mousedown",
            (e) => {
                if (e.target.tagName !== "SPAN") {
                    setTimeout(this.editCode);
                }
            },
            false
        );
        api.listeners.on(
            this.domRef.current.parentElement.parentElement.querySelector(".ce-block-border-right-wrap"),
            "mousedown",
            (e) => {
                if (e.target.tagName !== "SPAN") {
                    setTimeout(this.editCode);
                }
            },
            false
        );
        const { showCode, openEdit, pinCode, readOnly, openSelect } = this.state;
        let showCodeBtn = settingWrapper.querySelector(".show-code");
        if (showCode) {
            showCodeBtn.classList.remove("anticon-muted");
        } else {
            showCodeBtn.classList.add("anticon-muted");
        }
        let editCodeBtn = settingWrapper.querySelector(".edit-code");
        if (openEdit) {
            editCodeBtn.classList.remove("anticon-muted");
            let index = api.blocks.getCurrentBlockIndex();
            api.caret.setToBlock(index, "start", 0);
        } else {
            editCodeBtn.classList.add("anticon-muted");
        }
        if (this.domRef.current) {
            /**@type {HTMLFontElement} */
            let d = this.domRef.current.parentElement.parentElement.querySelector(".ce-block-border-left-wrap>i");
            d.className = `icon fas fa-chevron-${openEdit ? "down" : "right"} ${pinCode || readOnly || openSelect ? "hide" : ""
                }`;
            d.setAttribute("title", openEdit ? "Edit cell" : "Close cell Esc");
        }
    }

    cacheRun = async () => {
        const { selectedCategoryFrom, selectedCategory } = this.state;
        if (selectedCategoryFrom && selectedCategory) {
            if (selectedCategoryFrom === CategoryFromType.shareData && (!ShareData[this.editor.getSelfModuleName()] || !ShareData[this.editor.getSelfModuleName()][selectedCategory])) {
                // setTimeout(() => { this.cacheRun() }, 200);
                return;
            }
            let columnsData = await getColumnsData(this.editor.getSelfModuleName(), selectedCategoryFrom, selectedCategory, true);
            if (this.unmount) {
                return;
            }
            let dataTypes = calcAutoType(columnsData.data, true, true);
            this.setState({ dataTypes, columnsData })
        }
    };

    getAnchorValue = () => {
        return this.getRealValue(true);
    };

    componentWillUnmount() {
        actions.deleteCache(this.safeModeElement);
        actions.deleteCache(this.openSelectElement);
        clearTimeout(this.debounce);
        this.unmount = true;
    }

    componentDidUpdate(prevProps, prevState) {
        const { saveData, settingWrapper } = this.props;
        const { showCode, openEdit, pinCode, readOnly, openSelect } = this.state;
        if (showCode !== prevState.showCode) {
            let showCodeBtn = settingWrapper.querySelector(".show-code");
            if (showCode) {
                showCodeBtn.classList.remove("anticon-muted");
            } else {
                showCodeBtn.classList.add("anticon-muted");
            }
        }
        if (openEdit !== prevState.openEdit) {
            let editCodeBtn = settingWrapper.querySelector(".edit-code");
            if (openEdit) {
                editCodeBtn.classList.remove("anticon-muted");
            } else {
                editCodeBtn.classList.add("anticon-muted");
            }
        }
        if (
            openEdit !== prevState.openEdit ||
            pinCode !== prevState.pinCode ||
            readOnly !== prevState.readOnly ||
            openSelect !== prevState.openSelect
        ) {
            /**@type {HTMLFontElement} */
            let d = this.domRef.current.parentElement.parentElement.querySelector(".ce-block-border-left-wrap>i");
            d.className = `icon fas fa-chevron-${openEdit ? "down" : "right"} ${pinCode || readOnly || openSelect ? "hide" : ""
                }`;
        }
        if (!_.isEqual(prevState, this.state)) {
            this.domRef.current && (this.domRef.current.state = this.state);
            let dataTmp = this.getData(this.state);
            let prevDataTmp = this.getData(prevState);
            if (!_.isEqual(prevDataTmp, dataTmp) || this.needSave) {
                saveData(dataTmp);
                this.needSave = false;
            }
        }
    }

    getData(stateData) {
        let dataTmp = {};
        _.each(_.keys(stateData), (key) => {
            if (!(_.has(noSaveState, key) || (_.has(defState, key) && _.isEqual(defState[key], stateData[key])))) {//if not (not need save state or default state) then save it
                dataTmp[key] = _.cloneDeep(stateData[key]);
            }
        });
        return dataTmp;
    }

    onBlurFunc() {
    }

    focusResult = (err) => {
        swStyle(this.ref.current, err);
    };

    setCodeLoading(codeLoading) {
        this.setState({ codeLoading });
    }

    refreshAnchors = (now = false) => {
        if (now) {
            const { api } = this.props;
            if (~api.blocks.getCurrentBlockIndex()) {
                let block = api.blocks.getBlockByIndex(api.blocks.getCurrentBlockIndex());
                if (
                    block &&
                    block.name === "plotChart" &&
                    block.holder &&
                    block.holder.querySelector(".code-edit") &&
                    this.domRef.current &&
                    block.holder.querySelector(".code-edit") === this.domRef.current
                ) {
                    this.editor.refreshAnchors(this.getAnchorState());
                } else {
                    this.editor.refreshAnchors();
                }
            }
        } else if (this.editor.runComplete) {
            this.editor.refreshAnchors();
        }
    };

    run = (now = false) => {
        const { dname } = this.state;
        let declare = this.ref.current.querySelector(":scope > .declare");
        declare.innerHTML = "";
        let data = this.ref.current.querySelector(":scope > .data");
        data.innerHTML = "&nbsp;";
        let once = false;
        let realValue = this.getRealValue();
        let cell;
        try {
            cell = realValue.trim() ? parseCell(realValue) : undefined;
        } catch (err) { }
        this.editor.runFunc(realValue, declare,
            data,
            async () => {
                if (this.unmount) {
                    return;
                }
                this.debounce = undefined;
                this.focusResult();
                const { needRun, fileAttachments, error, selectedCategoryFrom, selectedCategory } = this.state;
                let state = {
                    needRun: false,
                    fileAttachments: cell && cell.fileAttachments.size ? cell.fileAttachments : (selectedCategoryFrom === CategoryFromType.files && selectedCategory ?
                        new Map().set(~selectedCategory.indexOf(fileSeparator) ? selectedCategory.substring(selectedCategory.lastIndexOf(fileSeparator) + 1) : selectedCategory, selectedCategory) :
                        undefined),
                    error: undefined
                };
                let oldState = { needRun, fileAttachments, error };
                if (!(_.isEqual(state, oldState) && once)) {
                    //may be successFunc is called as a timer when use variant "now" etc.
                    once = true;
                    if (this.unmount) {
                        return;
                    }
                    this.state.codeLoading && this.setCodeLoading(false);
                    this.setState(state, () => {
                        if (!_.isEqual(fileAttachments, this.state.fileAttachments)) {
                            this.editor.refreshAttaches();
                        }
                        this.refreshAnchors(now);
                    });
                }
            },
            (err) => {
                if (this.unmount) {
                    return;
                }
                this.debounce = undefined;
                this.focusResult(err);
                const { fileAttachments, selectedCategoryFrom, selectedCategory } = this.state;
                this.setState({
                    needRun: true,
                    fileAttachments: cell && cell.fileAttachments.size ? cell.fileAttachments : (selectedCategoryFrom === CategoryFromType.files && selectedCategory ?
                        new Map().set(~selectedCategory.indexOf(fileSeparator) ? selectedCategory.substring(selectedCategory.lastIndexOf(fileSeparator) + 1) : selectedCategory, selectedCategory) : undefined),
                    error: err
                }, () => {
                    if (!_.isEqual(fileAttachments, this.state.fileAttachments)) {
                        this.editor.refreshAttaches();
                    }
                    this.refreshAnchors(now);
                    this.state.codeLoading && this.setCodeLoading(false);
                });
            },
            now,
            `_${dname.replaceNoWordToUnderline()}`)
    }

    getRealValue(copy = false) {
        const { name, dname, selectedCategoryFrom, selectedCategory, customise, marks, dataTypes, columnsData } = this.state;
        return `${name ? `${name} = ` : ""}Plot.plot({\n${extractCustomise(customise)}${extractMarks(copy, this.editor.getSelfModuleName(), selectedCategoryFrom, selectedCategory, marks, dataTypes, columnsData)}
})`
    }

    /**
     * run code
     * @param {boolean} force force run
     * @param {boolean} now run it right now
     */
    runCode = async (force = false, now = true) => {
        const { needRun, safeMode } = this.state;
        if (safeMode || this.unmount) {
            this.debounce = undefined;
            return;
        }
        if (needRun || force) {
            await this.run(now);
        } else {
            this.debounce = undefined;
        }
    };

    debounceRun = () => {
        if (this.debounce) {
            return;
        }
        this.debounce = setTimeout(this.runCode, 200);
    }

    getAnchorState = () => {
        let codeEle = this.ref.current;
        if (codeEle.parentNode && codeEle.parentNode.getAttribute("id")) {
            let anchor = document.querySelector(
                `.editorjs-wrapper.${this.editor.getTransferKey()}>.editorjs-anchor a[href='#${codeEle.parentNode.getAttribute(
                    "id"
                )}']`
            );
            if (anchor) {
                const { error } = this.state;
                let data = this.domRef.current.querySelector(".code-edit-result.display > .data");
                if (data) {
                    let className = `editorjs-anchor-${codeEle.parentNode.getAttribute("id").replace(/\s/g, "_")}`;
                    let type = AnchorType.Common;
                    if (error) {
                        type = AnchorType.Error;
                    } else if (data.cacheNames) {
                        type = AnchorType.Variant;
                    }
                    let args;
                    if (data.cacheNames) {
                        args = data.args || [];
                    } else {
                        args = (data.vary && data.vary.args) || [];
                    }
                    return { className, value: this.getAnchorValue(), type, args, cacheNames: data.cacheNames };
                }
            }
        }
    };

    focusAnchor = () => {
        let anchorState = this.getAnchorState();
        if (anchorState) {
            let anchor = document.querySelector(
                `.editorjs-wrapper.${this.editor.getTransferKey()}>.editorjs-anchor a[href='#${this.ref.current.parentNode.getAttribute(
                    "id"
                )}']`
            );
            anchor && anchor.scrollIntoView({ behavior: "auto", block: "center", inline: "nearest" });
            this.editor.ref.current && this.editor.ref.current.focus(anchorState);
        }
    };

    editCode = (open) => {
        const { readOnly, error } = this.state;
        if (!readOnly && (undefined === open || open !== this.state.openEdit)) {
            this.setState(
                (state) => {
                    return { openEdit: !state.openEdit };
                },
                () => { }
            );
        } else if (this.domRef.current) {
            const { error } = this.state;
            if (!isVisibleInViewport(this.domRef.current)) {
                this.domRef.current.scrollIntoView({
                    behavior: "auto",
                    block: "nearest",
                    inline: "nearest",
                });
            }
            this.focusResult(error);
            let codeEle = this.ref.current;
            if (codeEle.parentNode && codeEle.parentNode.getAttribute("id")) {
                let anchor = document.querySelector(
                    `.editorjs-wrapper.${this.editor.getTransferKey()}>.editorjs-anchor a[href='#${codeEle.parentNode.getAttribute(
                        "id"
                    )}']`
                );
                anchor && anchor.scrollIntoView({ behavior: "auto", block: "center", inline: "nearest" });
            }
            this.focusAnchor();
        }
    }

    pinCodeFunc = (pin) => {
        if (!(undefined !== pin && pin === this.state.pinCode)) {
            this.setState({ pinCode: !this.state.pinCode });
        }
    };

    showCodeFunc = (show) => {
        if (!(undefined !== show && show === this.state.showCode)) {
            this.setState({ showCode: !this.state.showCode });
        }
    };

    render() {
        const { api } = this.props;
        const { name, dname, selectedCategoryFrom, selectedCategory, customise, marks, dataTypes, columnsData, codeLoading, readOnly, openSelect, openEdit, pinCode, showCode } = this.state;
        const customiseButton = withPopMenu(CustomiseComp, {
            icon: <SlidersHIcon></SlidersHIcon>,
            label: <span className="button-label">Customise</span>,
            values: customise,
            setValues: (customise) => {
                this.setState({ needRun: true, customise }, () => {
                    this.debounceRun();
                });
            }
        })
        return <div
            ref={this.domRef}
            contentEditable={false}
            onClick={() => {
                this.focusAnchor();
            }}
            className={`code-edit ${openSelect ? "open-select" : ""} ${readOnly ? "read-only" : ``
                }`}
        >
            <div id={`code-${dname}`} className="code-edit-content">
                <div
                    className={`code-edit-result display d-flex justity-content-start`}
                    ref={this.ref}
                >
                    <ErrorComp component={this} ></ErrorComp>
                    <div className="declare">&nbsp;</div>
                    <div className="data flex-fill"></div>
                </div>
                <div className={`code-edit-wrapper ${openEdit || pinCode ? "" : "hide"}`}>
                    {!readOnly && <div className="code-edit-wrapper-tools normal-icon">
                        <PinIcon
                            onMouseEnter={(event) => {
                                api.tooltip.show(event.currentTarget, `${pinCode ? "Unpin cell" : "Pin cell"}`, {
                                    placement: "bottom",
                                });
                            }}
                            onMouseLeave={(event) => {
                                api.tooltip.hide();
                            }}
                            onMouseUp={() => {
                                this.pinCodeFunc();
                            }}
                            className={`${pinCode ? "" : "anticon-inactive"}`}
                        />
                    </div>}
                    {showCode && <div> <CopyIcon className="text-icon" title={"Copy Source"} onClick={
                        async () => {
                            copyContent(this.getRealValue(true), () => {
                                showMessage("copy success!", "success")
                            })
                        }
                    } />   <pre>{this.getRealValue(true)}</pre></div>}
                    {/* <RenderObject obj={extractCustomise(customise)} ></RenderObject> */}
                    <div className="d-flex align-items-center justify-content-between">
                        <div className="d-flex align-items-center">
                            <Input
                                onBlur={this.onBlurFunc}
                                style={{ width: 120 }}
                                allowClear
                                value={name}
                                placeholder="unnamed"
                                onChange={(e) => {
                                    this.setState({ needRun: true, name: e.target.value }, () => {
                                        this.debounceRun();
                                    });
                                }}
                            />
                            &nbsp;=&nbsp;
                            <ChooseDatas
                                optionTypes={[CategoryFromType.files, CategoryFromType.main, CategoryFromType.shareData]}
                                editor={this.editor}
                                dname={dname}
                                selectedCategoryFrom={selectedCategoryFrom}
                                selectedCategory={selectedCategory}
                                onCategoryChanged={async ({ selectedCategoryFrom, selectedCategory, refresh }) => {
                                    if (!this.editor.runComplete) {
                                        return;
                                    }
                                    let columnsData = await getColumnsData(this.editor.getSelfModuleName(), selectedCategoryFrom, selectedCategory, true);
                                    let dataTypes = calcAutoType(columnsData.data, true, true);
                                    let state = { needRun: true, selectedCategoryFrom, selectedCategory, columnsData, dataTypes };
                                    !refresh && _.assign(state, { customise: _.cloneDeep(TemplateValues), marks: _.cloneDeep(TemplateMarksValues) });
                                    this.setState(state, () => {
                                        this.debounceRun();
                                    });
                                }}
                            ></ChooseDatas>
                        </div>
                        <div>
                            <Button icon={<CloseOutlined />} onClick={() => {
                                this.setState({ needRun: true, customise: _.cloneDeep(TemplateValues), marks: _.cloneDeep(TemplateMarksValues) }, () => {
                                    this.debounceRun();
                                });
                            }}>
                                <span className="button-label">Clear All</span>
                            </Button>
                            <Button icon={<SwapIcon />} onClick={() => {
                                let temp = _.cloneDeep(marks);
                                temp.x = marks.y;
                                temp.xf = marks.yf;
                                temp.y = marks.x;
                                temp.yf = marks.xf;
                                this.setState({ needRun: true, marks: temp }, () => {
                                    this.debounceRun();
                                })
                            }}>
                                <span className="button-label">Swap X/Y</span>
                            </Button>
                            {customiseButton}
                        </div>

                    </div>
                    <MarksComp marks={marks} dataTypes={dataTypes} setMarks={(marks) => {
                        this.setState({ needRun: true, marks }, () => {
                            this.debounceRun();
                        });
                    }}></MarksComp>
                </div>
            </div>
            {codeLoading && (
                <div className="code-loading-top">
                    <Spin spinning={true} indicator={<Loading3QuartersOutlined style={{ fontSize: 24, }} spin />}>
                        <div className="code-full-div"></div>
                    </Spin>
                </div>
            )}
        </div>
    }
}