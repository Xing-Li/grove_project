import axios from 'axios';
import _ from 'lodash';
import moment from 'moment';
import ReactDOM from "react-dom";
import Anchors from './Anchors';
import actions from './define/Actions';
import {
    fileSeparator,
    getContent,
    getFileContent, getFileKey,
    getFileName, getFileType, getFolderKey, transferToRealData, verFileTag, wrapContent
} from './file/fileUtils';
import MdFiles from './file/MdFiles';
import {
    DefaultFileData, DefaultShareData, setItem, getItem, getFileNamesKey,
    getUndefinedKey, getUndefinedContent, ShapeChatInfo, USER_ID, PROJECT_ID,
    ShapeLinkChange, ShapeFileChange, isNotLogin, removeItem,
    DEFAULT_VER, TAG_TYPE, SYSTEM_USER, notMyOwnProject, FILESDIR, FILESDIR_NAME, DefaultOptions, COMMON, COMMON_EDITOR_KEY, setWindowState
} from "./util/localstorage";
import { ShapeElement, ShapeModule, ShapeInspector, ShapeVariable } from './util/hqApi';
import MsgHandler from 'file/MsgHandler';
import RenderTask from './util/renderTask';
import EDITOR_JS_TOOLS from './constants';
import EditorAbs from 'editorAbs';
import { InitSettings } from 'editorSettings';
import commonData from 'common/CommonData';

const { ShareProjectCanOptions } = require("./util/MsgType");
const { showToast, SETTINGS_NAME, showConfirm, dataURItoBlob,
    ModalType, ContentType, ReadWriteOptions } = require('./util/utils');
const { ROOT_URI, runVariables, expireModule, copyShareFile,
    getReferRealFileUri, abosolutePath, dispose, createModule, getAndLoadModule, disposeModule, removeModule } = require('./util/hqUtils');
const { AllAcceptArr, isFileEditorJs, transferType, isFileNoEdit, DEBUG_EDITOR, SelectType } = require('./util/helpers.js');
const { errorDataFile, largeDataFile } = require('./block/data');
/**@type {import('html2canvas').default} */
const html2canvas = window.html2canvas;

export class CommonEditor extends EditorAbs {
    constructor() {
        super();
        window.editor = this;
        window.currentEditor = this;
        actions.variable(actions.types.CURRENT_EDITOR, [], () => {
            return window.currentEditor;
        });
        this.editorjsKey = COMMON_EDITOR_KEY;
    }
    /**
     * init editor
     * @param {*} option 
     * @param {MdFiles} react_component  
     * @param {InitSettings} settings
     */
    init = (option, react_component, settings) => {
        /**@type {ShapeModule} */
        this.main = createModule("main");
        /**@type {ShapeModule} */
        this.currentMain = undefined;
        //_app.controller.API.on("change",null,"Your Extension Name")
        /**@type {DefaultFileData} */
        this.fileData = undefined;
        /**@type {DefaultShareData} */
        this.linkData = undefined;
        /**@type {ShapeChatInfo} */
        this.shared = undefined;
        this.react_component = react_component;
        this.msgHandler = new MsgHandler(this.react_component);
        /**this is a state */
        this.readOnly = true;
        this.settings.loadSettings(settings);
        window.removeEventListener('resize', this.resizeFunc.bind(this));
        window.addEventListener('resize', this.resizeFunc.bind(this));
        /** need save in one second  */
        this.setNeedSave(false);
        this.countSecond = 0;
        this.initEditorJs(option);
    }
    resizeFunc = () => {
        this.settings.setWidthScale(window.innerWidth * 100 / window.screen.availWidth);
        this.settings.setHeightScale(window.innerHeight * 100 / window.screen.availHeight);
        actions.variable(actions.types.RESIZE_WINDOW, [], () => {
            return {
                widthScale: this.settings.getWidthScale(),
                heightScale: this.settings.getHeightScale()
            }
        })
        this.saveSettings();
        DEBUG_EDITOR && console.log("resize")
    };
    getDiffSetting = (key) => {
        let difSetting = {
            Diff_Timeout: 1,
            Diff_EditCost: 4,
            semantic: true,
            efficiency: false
        }
        return difSetting[key];
    }
    /**
     * @returns get editorJs
     */
    get = () => {
        return this.editorInstance;
    }

    /**
     * reset to init then set editorJs's content
     * @param {string} content 
     * @param {ContentType} contentType 
     */
    value = async (content, contentType, version = DEFAULT_VER) => {
        this.setScrollPos(0);
        this.renderTask.clearTasks();
        this.setNeedSave(false);
        // dispose(this.getMain());
        let _delSelfModule = false;
        _.each(this.getModuleNames(), (moduleName) => {
            if (_.filter(window.tagEditors, (tagEditor) => {
                return ~tagEditor.getModuleNames().indexOf(moduleName)
            }).length === 0) {
                if (moduleName === this.getSelfModuleName()) {
                    _delSelfModule = true;
                }
                disposeModule(moduleName);
                removeModule(moduleName);
            }
        });
        this.contentType = contentType || ContentType.File;
        if (this.contentType === ContentType.Link) {
            this.uploadUri = this.linkData.uploadUri;
        } else {
            this.uploadUri = this.react_component.state.uploadUri;
        }
        this.key = this.uploadUri + (this.getData() || {}).fileKey;
        this.version = version;
        this.runComplete = false;
        this.attaches.clear();
        this.moduleNames.splice(0, this.moduleNames.length);
        this.clearCache();
        this._delSelfModule = _delSelfModule;
        await this.editorInstance.blocks.clear();
        this._delSelfModule = true;
        document.querySelector(`.editorjs-wrapper.${this.getTransferKey()}>.editorjs-anchor`) &&
            ReactDOM.unmountComponentAtNode(document.querySelector(`.editorjs-wrapper.${this.getTransferKey()}>.editorjs-anchor`));
        this.recordSelfModule();
        this.currentMain = getAndLoadModule((this.getData() || {}).fileKey, this.getUploadUri()) || this.main;
        dispose(this.getMain());
        this.option.data = content;
        let largeJson = largeDataFile(this.fileData);
        let errorJson = errorDataFile(this.option.data);
        /**
         * 
         */
        if (largeJson || errorJson || (this.shared && !this.version) || this.isLinkDataReadOnly()) {
            !this.getReadOnly() && await this.setReadOnly(true);
            if (this.isLinkDataReadOnly() && this.readwrite_mode === ReadWriteOptions[2]) {
                this.readwrite_mode = ReadWriteOptions[1];
                actions.variable(actions.types.READWRITE_MODE, [], () => {
                    return this.readwrite_mode;
                })
            }
        } else {
            if (commonData.getReadOnlyMode()) {
                !this.getReadOnly() && await this.setReadOnly(true);
            } else {
                this.getReadOnly() && await this.setReadOnly(false);
            }
        }
        this.refreshViewCode();
        actions.variable(actions.types.CHART_EDITOR, [], () => {
            return undefined;
        })
        await this.editorInstance.blocks.render(largeJson || errorJson || JSON.parse(this.option.data));
        await new Promise((resolve, reject) => {
            this.renderTask.submitRunOverTask(() => {
                try {
                    this.editorInstance.caret.setToFirstBlock('start', 0);
                    document.querySelector(`.editorjs-wrapper.${this.getTransferKey()} .codex-editor__redactor`)
                        && document.querySelector(`.editorjs-wrapper.${this.getTransferKey()} .codex-editor__redactor`).click();
                    actions.variable(actions.types.RUN_COMPLETE, [], () => { return { editorKey: this.getTransferKey() } });
                    resolve()
                } catch (e) {
                    reject(e)
                }
            })
        })
        if (window.currentEditor === this) {
            window.currentEditor.setScrollPos(0)
            window.requestAnimationFrame(() => {
                window.scrollTo(0, window.currentEditor.getScrollPos());
            })
        }
    }
    isLocalEditFile = () => {
        return (this.contentType === ContentType.Link && !this.shared) || this.version;
    }
    /**
     * open a link page
     * @param {*} fileKey 
     * @param {*} version 
     */
    linkTo = async (fileKey, version) => {
        if (!fileKey) {
            return await this.value(getUndefinedContent(), ContentType.Link);
        }
        let uri = this.linkData.uploadUri + `${fileKey}${version ?
            `${verFileTag}${fileSeparator}${version}` : ""}`;
        let content = await getContent(this.linkData, uri);
        await this.value(wrapContent(content, this.linkData, this.getUploadUri()), ContentType.Link, version);
    }
    /**
     * @returns get editorJs's content
     */
    text = () => {
        return this.option.data || getUndefinedContent();
    }
    /**
     * switch current file's version
     * @param {*} version 
     */
    async switchFileVersion(version) {
        if (version === this.version) {
            return;
        }
        if (this.fileData && this.contentType === ContentType.File) {
            await this.react_component.switchFileVersion(version);
        } else if (this.linkData && this.contentType === ContentType.Link) {
            let content = await this.getReferRealFileContent
                (`${this.linkData.fileKey}${version ? `@${version}` : ""}`, this.getUploadUri());
            await this.value(content, this.contentType, version);
        } else {
            return;
        }
    }
    containsKey = (key) => {
        return this.react_component.state.fileNamesJson.hasOwnProperty(key)
    }
    clearEditor = async (cb, initData = false, clearSelected = false, stateTmp, refreshWindowState = true) => {
        await this.react_component.clearEditor(cb, initData, clearSelected, stateTmp, refreshWindowState);
    }
    getBlocks = async () => {
        if (!this.fileData) {
            return [];
        }
        let text = await this.react_component.getRealFileContent(this.fileData);
        if (text === null) {
            return;
        }
        let json = JSON.parse(text);
        return _.filter(json.blocks, (block) => { return block.type === "codeTool" && block.data.codeData.value });
    }
    showTip = (ele, message, placement) => {
        this.editorInstance && this.editorInstance.tooltip.show(ele, message, placement);
    }
    hideTip = () => {
        this.editorInstance && this.editorInstance.tooltip.hide();
    }
    async switchFile(fileKey, version = DEFAULT_VER) {
        return await this.react_component.switchFileByFileKey(fileKey, undefined, version);
    }
}
const editor = new CommonEditor();
export default editor;