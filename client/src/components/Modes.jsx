import React, { useEffect, useState } from "react";
import { useThemeSwitcher } from "react-css-theme-switcher";
import { Switch } from "antd";

export default function Modes(props) {
    const { onChange, checked, ...propsa } = props;
    const { switcher, currentTheme, status, themes } = useThemeSwitcher();
    const toggleTheme = (isChecked) => {
        onChange(isChecked);
    };
    useEffect(() => {
        let theme = checked ? themes.dark : themes.light;
        if (theme !== currentTheme) {
            switcher({ theme: theme });
        }
    }, [checked]);

    // Avoid theme change flicker
    if (status === "loading") {
        return null;
    }

    return <Switch {...propsa} checked={checked} onChange={toggleTheme} />;
}