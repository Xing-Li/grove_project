import React from 'react';

export const Section = ({ children }) => (
  <div className='d-flex flex-column'>{children}</div>
);

export const SectionTitle = ({ children }) => <div className='h5'>{children}</div>;

export const SectionContent = ({ children }) => (
  <div className='body-medium'>{children}</div>
);
