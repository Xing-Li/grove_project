// Most of this is a copy and paste from d3-require. The main difference is resolveOffline.
// If it needs to be merged back into d3-require, you could just extract resolveOffline and remove this file.

import Common from "../util/Common";
import { DEBUG_REQUIRE } from "../util/helpers";

export const cdn = "https://cdn.jsdelivr.net/npm/";
const metas = new Map();
const queue = [];
const map = queue.map;
const some = queue.some;
const hasOwnProperty = queue.hasOwnProperty;
const origin = cdn;
const identifierRe = /^((?:@[^/@]+\/)?[^/@]+)(?:@([^/]+))?(?:\/(.*))?$/;
const versionRe = /^\d+\.\d+\.\d+(-[\w-.+]+)?$/;
const extensionRe = /\.[^/]*$/;
const mains = ["unpkg", "jsdelivr", "browser", "main"];

export class RequireError extends Error {
  constructor(message) {
    super(message);
  }
}

RequireError.prototype.name = RequireError.name;

function main(meta) {
  for (const key of mains) {
    const value = meta[key];
    if (typeof value === "string") {
      return extensionRe.test(value) ? value : `${value}.js`;
    }
  }
}

function parseIdentifier(identifier) {
  const match = identifierRe.exec(identifier);
  return (
    match && {
      name: match[1],
      version: match[2],
      path: match[3],
    }
  );
}

function resolveMeta(target) {
  const url = `${origin}${target.name}${target.version ? `@${target.version}` : ""}/package.json`;
  let meta = metas.get(url);
  if (!meta || meta.isRejected)
    metas.set(
      url,
      (meta = fetch(url).then((response) => {
        if (!response.ok) {
          console.error(`unable to load ${response.url}`);
          meta.isRejected = true;
          return null;
        }
        if (response.redirected && !metas.has(response.url)) metas.set(response.url, meta);
        meta.isFulfilled = true;
        return response.json();
      }, (reason) => {
        console.error(`${url} ${reason}`);
        meta.isRejected = true;
      }))
    );
  return meta;
}

export async function resolveBase(name, base) {
  if (name.startsWith(origin)) name = name.substring(origin.length);
  name = Common.prefixAliasPath(name);
  if (/^(\w+:)|\/\//i.test(name)) return name;
  if (/^[.]{0,2}\//i.test(name)) return new URL(name, base == null ? location : base).href;
  if (!name.length || /^[\s._]/.test(name) || /\s$/.test(name)) throw new RequireError("illegal name");
  const target = parseIdentifier(name);
  if (!target) return `${origin}${name}`;
  if (!target.version && base != null && base.startsWith(origin)) {
    const meta = await resolveMeta(parseIdentifier(base.substring(origin.length)));
    if (!meta) {
      return null;
    }
    target.version =
      (meta.dependencies && meta.dependencies[target.name]) ||
      (meta.peerDependencies && meta.peerDependencies[target.name]);
  }
  if (target.path && !extensionRe.test(target.path)) target.path += ".js";
  if (target.path && target.version && versionRe.test(target.version))
    return `${origin}${target.name}@${target.version}/${target.path}`;
  const meta = await resolveMeta(target);
  if (!meta) {
    return null;
  }
  return `${origin}${meta.name}@${meta.version}/${target.path || main(meta) || "index.js"}`;
}

/**
 * e.g. fetchPackageFile("@kineviz/graphxr-api/docs/searchIndex.json") 
 */
export async function fetchPackageFile(path) {
  const url = await resolveAnywhere(path);
  const file = await fetch(url);
  return file;
}

export async function resolveOffline(name) {
  const target = parseIdentifier(name);
  const packagesRoot = `/extension/site/${groveModulePackId}/node_modules`;
  const packageUrl = `${packagesRoot}/${target.name}/package.json`;
  const file = await Common.fetch(packageUrl);
  const json = await file.json();
  const jsPath = json.unpkg ?? json.main;
  const fullPath = Common.prefixAliasPath(`${packagesRoot}/${target.name}/${jsPath}`);
  DEBUG_REQUIRE && console.log(`Loading module offline ${target.name} from ${fullPath}`);
  return fullPath;
}

let groveConfig = {};
let groveModulePackId = "";
let extensionConfigLoader = new Promise(async (resolve) => {
  DEBUG_REQUIRE && console.log("Loading extension config");

  try {
    groveModulePackId = await Common.fetch(`/api/tempModuleForUsers/getId?name=Grove%20Module%20Pack`).then(
      (response) => response.json()
    ).then(json => json.content);
  } catch (error) {
    console.log("An error occurred while trying to fetch the Grove Module Pack config", error);
  }

  if (groveModulePackId) {
    try {
      groveConfig = await Common.fetch(`/api/tempModuleForUsers/getConfigJson?name=Grove`).then(
        (response) => response.json()
      ).then(json => json.content);
      DEBUG_REQUIRE && console.log("Loaded Grove.configJson", groveConfig);
    } catch (error) {
      console.log("An error occurred while trying to fetch Grove.configJson", error);
    }
  } else {
    console.log("Grove Module Pack not found");
  }

  resolve();
});

export async function resolveAnywhere(name, base) {
  DEBUG_REQUIRE && console.log("Waiting for extension config to be loaded");
  await extensionConfigLoader;

  if (groveConfig.noOnlineRequire) {
    DEBUG_REQUIRE && console.log(`noOnlineRequire is enabled. Attempting to load ${name} offline.`);
    return resolveOffline(name);
  } else {
    DEBUG_REQUIRE && console.log(`noOnlineRequire is disabled. Attempting to load ${name} online.`);
    return resolveBase(name, base);
  }
}

export var require = requireFrom(resolveBase);

export function requireFrom(resolver) {
  const cache = new Map();
  const requireBase = requireRelative(null);

  function requireAbsolute(url) {
    if (typeof url !== "string") return url;
    let module = !url.includes('localhost') ? cache.get(url) : undefined;
    if (!module || module.isRejected)
      cache.set(
        url,
        (module = new Promise((resolve, reject) => {
          const script = document.createElement("script");
          script.onload = () => {
            try {
              resolve(queue.pop()(requireRelative(url)));
              module.isFulfilled = true;
            } catch (error) {
              console.error(error);
              reject(new RequireError(`${url} failed to load`));
              module.isRejected = true;
            }
            script.remove();
          };
          script.onerror = () => {
            reject(new RequireError("unable to load module"));
            module.isRejected = true;
            script.remove();
          };
          script.async = true;
          script.src = url;
          window.define = define;
          document.head.appendChild(script);
        }))
      );
    return module;
  }

  function requireRelative(base) {
    return (name) => Promise.resolve(resolver(name, base)).then(requireAbsolute);
  }

  function requireAlias(aliases) {
    return requireFrom((name, base) => {
      if (name in aliases) {
        (name = aliases[name]), (base = null);
        if (typeof name !== "string") return name;
      }
      return resolver(name, base);
    });
  }

  function require(name) {
    return arguments.length > 1 ? Promise.all(map.call(arguments, requireBase)).then(merge) : requireBase(name);
  }

  require.alias = requireAlias;
  require.resolve = resolver;

  return require;
}

function merge(modules) {
  const o = {};
  for (const m of modules) {
    for (const k in m) {
      if (hasOwnProperty.call(m, k)) {
        if (m[k] == null) Object.defineProperty(o, k, { get: getter(m, k) });
        else o[k] = m[k];
      }
    }
  }
  return o;
}

function getter(object, name) {
  return () => object[name];
}

function isbuiltin(name) {
  name = name + "";
  return name === "exports" || name === "module";
}

function define(name, dependencies, factory) {
  const n = arguments.length;
  if (n < 2) (factory = name), (dependencies = []);
  else if (n < 3) (factory = dependencies), (dependencies = typeof name === "string" ? [] : name);
  queue.push(
    some.call(dependencies, isbuiltin)
      ? (require) => {
          const exports = {};
          const module = { exports };
          return Promise.all(
            map.call(dependencies, (name) => {
              name = name + "";
              return name === "exports" ? exports : name === "module" ? module : require(name);
            })
          ).then((dependencies) => {
            factory.apply(null, dependencies);
            return module.exports;
          });
        }
      : (require) => {
          return Promise.all(map.call(dependencies, require)).then((dependencies) => {
            return typeof factory === "function" ? factory.apply(null, dependencies) : factory;
          });
        }
  );
}

define.amd = {};
