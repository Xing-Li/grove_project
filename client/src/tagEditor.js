import commonData from "common/CommonData";
import ReactDOM from "react-dom";
import actions from './define/Actions';
import EditorAbs from './editorAbs';
import {
    DefaultFileData, DefaultShareData,
    DEFAULT_VER, ShapeChatInfo,
    TAG_TYPE
} from "./util/localstorage";
// import MdFiles from './file/MdFiles';
const { ShareProjectCanOptions } = require("./util/MsgType");
const { ContentType, ReadWriteOptions } = require('./util/utils');
const { dispose, getAndLoadModule } = require('./util/hqUtils');
const { errorDataFile, largeDataFile } = require('./block/data');

export const ShapeTagEditorInfo = {
    fileData: DefaultFileData,
    linkData: DefaultShareData,
    shared: ShapeChatInfo,
    contentType: ContentType.Link,
    version: DEFAULT_VER,
    react_component: {},
    key: "",
    uploadUri: "",
}
/**
 * @param {ShapeTagEditorInfo} info
 */
export default class TagEditor extends EditorAbs {
    constructor(info) {
        super(info.settings);
        const { fileData, linkData, shared, contentType, version, react_component, key, uploadUri } = info
        /**@type {DefaultFileData} */
        this.fileData = fileData;
        /**@type {DefaultShareData} */
        this.linkData = linkData;
        /**@type {ShapeChatInfo} */
        this.shared = shared;
        this.contentType = contentType;
        this.version = version;
        /**@type{MdFiles} */
        this.react_component = react_component;
        this.key = key;
        this.editorjsKey = this.key.replaceNoWordToUnderline();
        this.uploadUri = uploadUri;
        /**@type {ShapeModule} */
        this.main = getAndLoadModule((this.getData() || {}).fileKey, this.getUploadUri());
        if (!this.main) {
            throw new Error("null module!");
        }
        dispose(this.main);
        this.recordSelfModule();
    }
    /**
     * init editor
     */
    init = (option) => {
        /**this is a state */
        this.readOnly = true;
        /** need save in one second  */
        this.setNeedSave(false);
        this.countSecond = 0;
        this.initEditorJs(option);
    }
    value = async (content, version) => {
        this.version = version;
        this.getData() && (this.getData().selectedVersion = version);
        this.setScrollPos(0);
        this.renderTask.clearTasks();
        this.setNeedSave(false);
        dispose(this.getMain());
        this.runComplete = false;
        this.attaches.clear();
        this.moduleNames.splice(0, this.moduleNames.length);
        this.clearCache();
        this._delSelfModule = true;
        await this.editorInstance.blocks.clear();
        this._delSelfModule = true;
        document.querySelector(`.editorjs-wrapper.${this.getTransferKey()}>.editorjs-anchor`) &&
            ReactDOM.unmountComponentAtNode(document.querySelector(`.editorjs-wrapper.${this.getTransferKey()}>.editorjs-anchor`));
        this.recordSelfModule();
        this.option.data = content;
        let largeJson = largeDataFile(this.fileData);
        let errorJson = errorDataFile(this.option.data);
        /**
         * 
         */
        if (largeJson || errorJson || (this.shared && !this.version) || this.isLinkDataReadOnly()) {
            !this.getReadOnly() && await this.setReadOnly(true);
            if (this.isLinkDataReadOnly() &&
                this.readwrite_mode === ReadWriteOptions[2]) {
                this.readwrite_mode = ReadWriteOptions[1];
                actions.variable(actions.types.READWRITE_MODE, [], () => {
                    return this.readwrite_mode;
                })
            }
        } else {
            if (commonData.getReadOnlyMode()) {
                !this.getReadOnly() && await this.setReadOnly(true);
            } else {
                this.getReadOnly() && await this.setReadOnly(false);
            }
        }
        actions.variable(actions.types.CHART_EDITOR, [], () => {
            return undefined;
        })
        await this.editorInstance.blocks.render(largeJson || errorJson || JSON.parse(this.option.data));
        await new Promise((resolve, reject) => {
            this.renderTask.submitRunOverTask(() => {
                try {
                    this.editorInstance.caret.setToFirstBlock('start', 0);
                    document.querySelector(`.editorjs-wrapper.${this.getTransferKey()} .codex-editor__redactor`)
                        && document.querySelector(`.editorjs-wrapper.${this.getTransferKey()} .codex-editor__redactor`).click();
                    actions.variable(actions.types.RUN_COMPLETE, [], () => { return { editorKey: this.getTransferKey() } });
                    resolve()
                } catch (e) {
                    reject(e)
                }
            })
        })
    }
    isLocalEditFile = () => {
        return (this.contentType === ContentType.Link && !this.shared) || this.version;
    }
    clear = async (_delSelfModule = true) => {
        this.ready = false;
        this.renderTask.clearTasks();
        this.setNeedSave(false);
        // dispose(this.getMain());
        this.runComplete = false;
        this.attaches.clear();
        this.moduleNames.splice(0, this.moduleNames.length);
        this.clearCache();
        this._delSelfModule = _delSelfModule;
        await this.editorInstance.blocks.clear();
        this._delSelfModule = true;
        document.querySelector(`.editorjs-wrapper.${this.getTransferKey()}>.editorjs-anchor`) &&
            ReactDOM.unmountComponentAtNode(document.querySelector(`.editorjs-wrapper.${this.getTransferKey()}>.editorjs-anchor`));
        this.editorInstance.destroy();
    }
}