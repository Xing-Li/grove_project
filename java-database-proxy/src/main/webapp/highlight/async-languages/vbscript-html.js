/*
Language: VBScript in HTML
Requires: xml.js, vbscript.js
Author: Ivan Sagalaev <maniac@softwaremaniacs.org>
Description: "Bridge" language defining fragments of VBScript in HTML within <% .. %>
Category: scripting
*/

define(["./xml.js","./vbscript.js"], (...$requiredLanguages) => function(hljs) {
        hljs.registerLanguage("xml", $requiredLanguages[0]);
hljs.registerLanguage("vbscript", $requiredLanguages[1]);
          
          
  return {
    subLanguage: 'xml',
    contains: [
      {
        begin: '<%', end: '%>',
        subLanguage: 'vbscript'
      }
    ]
  };
}
)