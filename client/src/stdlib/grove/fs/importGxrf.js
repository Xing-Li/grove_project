export async function importGxrf(fileAttachment) {
  const blob = await fileAttachment.blob();
  const body = window.opener.document.querySelector("body");
  const fileName = fileAttachment.name.split("/").pop();
  const imported = new File([blob], fileName, {
    type: "application/zip",
  });
  const dataTransfer = new DataTransfer();
  dataTransfer.items.add(imported);
  body.dispatchEvent(new DragEvent("drop", { dataTransfer, force: true }));
}