import CheckList from "./block/checklist/index";
import Delimiter from "./block/delimiter/index";
import Embed from "./block/embed/index";
import Header from "./block/header/header";
import InlineCode from "./block/inlineCode/index";
import LinkTool from "@editorjs/link";
import List from "./block/nestedList/index";
import Marker from "@editorjs/marker";
// import Quote from "@editorjs/quote";
import Raw from "@editorjs/raw";
// import Table from "@editorjs/table";
import Warning from "@editorjs/warning";
import AntChart from "./block/antchart/antchart";
import PlotChart from "./block/plotchart/plotchart";
// import Chart from "./block/chart/chart";
import CommonCode from "./block/commonCode/commonCode";
import Quote from "./block/quote/index";
import CodeTool from "./block/code/codeTool";
import SimpleImage from "./block/image/simpleImage";
import MDImporter from "./block/markdown/MarkdownImporter";
import MDParser from "./block/markdown/MarkdownParser";
import Table from "./block/table/index";

const EDITOR_JS_TOOLS = function (editorjsKey) {
    return {
        codeTool: {
            class: CodeTool,
            config: {
                preventKeydownEvent: true,
                editorjsKey: editorjsKey,
            },
            shortcut: 'CMD+ALT+E'
        },
        antChart: {
            class: AntChart,
            config: {
                editorjsKey: editorjsKey,
            },
        },
        plotChart: {
            class: PlotChart,
            config: {
                editorjsKey: editorjsKey,
            },
        },
        code: CommonCode,
        embed: Embed,
        table: {
            class: Table,
            inlineToolbar: true
        },
        list: {
            class: List,
            inlineToolbar: true
        },
        warning: Warning,
        linkTool: LinkTool,
        // chart: {
        //     class: Chart,
        // },
        header: {
            class: Header,
            inlineToolbar: true,
            config: {
                placeholder: 'Header',
                editorjsKey: editorjsKey,
            },
        },
        raw: Raw,
        quote: {
            class: Quote,
            inlineToolbar: true
        },
        marker: Marker,
        checklist: {
            class: CheckList,
            inlineToolbar: true
        },
        delimiter: Delimiter,
        inlineCode: InlineCode,
        simpleImage: {
            class: SimpleImage,
            config: {
                editorjsKey: editorjsKey,
            },
            // shortcut: 'CMD+ALT+C'
        },
        markdownParser: MDParser,
        markdownImporter: MDImporter,
    }
};
export default EDITOR_JS_TOOLS;
