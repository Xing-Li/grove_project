import {
    CloseOutlined, SearchOutlined
} from '@ant-design/icons';
import {
    Button, Checkbox, Divider, Dropdown, Input, InputNumber, Menu, Radio, Select, Switch, Space, Table
} from 'antd';
import Selection from 'components/Selection';
import _ from 'lodash';
import React, { useEffect, useState, useRef } from 'react';
import { calcAutoType, calcSqliteAutoType, highligthComp, transferInteger, transferPercent, nanOrBlank, stringifyNanOrBlank, extractAllKeys } from 'util/helpers';
import { DatabaseType, EmptyResults, getColumnKeyType, getColumnName, getColumnType, isColumnNoNullable, isFailedResults } from 'util/utils';
import { ChevronDownIcon, ChevronRightIcon } from './Icon';
import { getCellName } from 'util/hqUtils';
const CheckboxGroup = Checkbox.Group;
const { Option, OptGroup } = Select;

const TB_WIDTH = 130;
export const MAX_COLUMN_WIDTH = 175;
/**
* set content editable
* @param {CodeMirror.Editor} cm 
* @param {boolean} readOnly 
*/
export function contentEditable(cm, readOnly) {
    if (readOnly) {
        return;
    }
    _.each(cm.getWrapperElement().getElementsByClassName("CodeMirror-line"), (ele) => {
        !ele.hasAttribute("contenteditable")
            && ele.setAttribute("contenteditable", "true");
    });
    _.each(cm.getWrapperElement().getElementsByClassName("CodeMirror-line-like"), (ele) => {
        !ele.hasAttribute("contenteditable")
            && ele.setAttribute("contenteditable", "false");
    });
}

export function proxyArrayCode({ variantName, dname, editorjsKey }) {
    return `${variantName} = {
        let func = function () {
            let data = document.querySelector("#code-${dname} .code-edit-result > .data");
            if(!data){
                return;
            }
            let codeEditEle = data.closest(".code-edit");
            if (codeEditEle) {
                codeEditEle.syncFileFunc(true, this);
            }
            let editor = "${editorjsKey}" === window.editor.getTransferKey() ? window.editor :
                _.filter(window.tagEditors, (tagEditor) => { return tagEditor.getTransferKey() === "${editorjsKey}" })[0];
            const { variables } = editor.getMain()._cachedata;
            let name = "${variantName}_origin"
            let variable = variables[name]
            variable = !variable.redefines ? variable :
                _.assign({}, variable, variable.redefines[variable.redefines.length - 1])
            if(!variable.binded){
                variable.func = variable.func.bind(variable.varia._value);
                variable.binded = true;
            }
            variable.varia.define(variable.name, variable.args, variable.func);
        };
        return  new ProxyArray("${editorjsKey}" ,"${variantName}_origin", func);

        
        // let _node = document.querySelector("#code-${dname} .code-edit-result > .data > span > a");
        // if(!_node){
        //     return;
        // }
        // var evt = document.createEvent("MouseEvents");
        // evt.initEvent("mouseup", true, true);
        // _node.dispatchEvent(evt);
        // _node = document.querySelector("#code-${dname} .code-edit-result > .data > span > a");
        // _node.dispatchEvent(evt);
    }`
}

export function sqliteDatasToChartDatas(nData = [{}]) {
    const dataTypes = calcSqliteAutoType(nData, true);
    return _.reduce(dataTypes, (prev, dataType, key) => {
        if (dataType.type === 'TEXT') {
            let datas = _.countBy(nData, key);
            let values = _.values(datas);
            let ele;
            if (values.length === nData.length) {
                ele = function (props) {
                    return <div className="th-2">
                        <div className="th-2-data-type">{dataType.type}</div>
                        <div className="relative">
                            <div className="th-2-chart"><div className="unique-bk" style={{ width: `${TB_WIDTH}px` }}>{values.length} unique values</div></div>
                            <div className="th-2-desc"></div>
                        </div>
                    </div>
                }
            } else if (values.length === 1) {
                ele = function (props) {
                    return <div className="th-2">
                        <div className="th-2-data-type">{dataType.type}</div>
                        <div className="relative">
                            <div className="th-2-chart"><div className="all-same-bk" style={{ width: `${TB_WIDTH}px` }}>all values: "{_.keys(datas)[0]}"</div></div>
                            <div className="th-2-desc"></div>
                        </div>
                    </div>
                }
            } else {
                let sum = _.sum(values);
                ele = function (props = {}) {
                    const { setFilterValues, startEnd } = props
                    const moreThanOnes = _.reduce(datas, (prev, v, k) => { if (v > 1 && k !== "undefined" && k !== "null" && k !== "NULL") { prev.push({ v, k }) } return prev; }, []);
                    moreThanOnes.sort((a, b) => { return b.v - a.v });
                    const uniques = _.reduce(datas, (prev, v, k) => { if (v === 1 && k !== "undefined" && k !== "null" && k !== "NULL") { prev.push({ v, k }) } return prev; }, []);
                    const nullOne = _.find(datas, (v, k) => { return k === "undefined" || k === "null" || k === "NULL" });
                    const defaultLabels = {
                        dataType: dataType.type,
                        desc: `${_.keys(datas).length} categories`,
                        type: "default",
                    };
                    const [labels, setLabels] = useState(defaultLabels);
                    useEffect(() => {
                        let currentLabels;
                        const { hoverRow } = props;
                        if (hoverRow && undefined !== datas[hoverRow[key]]) {
                            let propertyValue = hoverRow[key]
                            if (undefined === propertyValue || null === propertyValue || 'null' === propertyValue || 'NULL' === propertyValue) {
                                currentLabels = {
                                    dataType: `${nullOne} rows(${transferPercent(nullOne / sum * 100)}%)`,
                                    desc: `NULL/EMPTY`,
                                    type: "null-empty",
                                }
                            } else if (_.filter(datas, (v, k) => { return v === 1 && k === (propertyValue).toString() }).length === 1) {
                                currentLabels = {
                                    dataType: `1 rows(${transferPercent(uniques.length / sum * 100)}%)`,
                                    desc: `${propertyValue}(unique)`,
                                    type: "unique",
                                    k: (propertyValue).toString(),
                                }
                            } else {
                                currentLabels = {
                                    dataType: `${datas[propertyValue]} rows(${transferPercent(datas[propertyValue] / sum * 100)}%)`,
                                    desc: `${propertyValue}`,
                                    type: "common",
                                    k: (propertyValue).toString(),
                                }
                            }
                        } else {
                            currentLabels = defaultLabels;
                        }
                        setLabels(currentLabels);
                    }, [props.hoverRow])
                    return <div className="th-2">
                        <div className="th-2-data-type">{labels.dataType}</div>
                        <div className="relative" onMouseDown={(e) => {
                            e.preventDefault();
                            e.stopPropagation();
                        }} onClick={(e) => {
                            startEnd && setFilterValues(null);
                            e.preventDefault();
                            e.stopPropagation();
                        }}>
                            <div className="th-2-chart">
                                {_.map(moreThanOnes, (value, index) => {
                                    return <div key={value.k} className={`common-bk ${labels === defaultLabels || labels.k !== value.k ? "opacity-4" : ""} ${startEnd && startEnd.single && startEnd.start === value.k ? "orange-bk" : ""}`}
                                        onMouseOver={(e) => {
                                            setLabels({
                                                dataType: `${value.v} rows(${transferPercent(value.v / sum * 100)}%)`,
                                                desc: `${value.k}`,
                                                type: "common",
                                                k: value.k,
                                            })
                                        }} onMouseLeave={(e) => { setLabels(defaultLabels) }}
                                        onClick={() => {
                                            setFilterValues({ single: true, start: value.k })
                                        }}
                                        style={{ width: `${value.v / sum * TB_WIDTH}px` }}>{value.k}</div>
                                })}
                                {_.map(uniques, (value, index) => {
                                    return <div key={value.k} className={`unique-bk ${labels === defaultLabels || labels.k !== value.k ? "opacity-4" : ""} ${startEnd && startEnd.single && startEnd.start === value.k ? "orange-bk" : ""}`}
                                        onMouseOver={(e) => {
                                            setLabels({
                                                dataType: `1 rows(${transferPercent(1 / sum * 100)}%)`,
                                                desc: `${value.k}(unique)`,
                                                type: "unique",
                                                k: value.k,
                                            })
                                        }} onMouseLeave={(e) => { setLabels(defaultLabels) }}
                                        onClick={() => {
                                            setFilterValues({ single: true, start: value.k })
                                        }}
                                        style={{ width: `${1 / sum * TB_WIDTH}px` }}></div>
                                })}
                                <div key={"null"} className={`null-empty-bk ${labels === defaultLabels || labels.type !== "null-empty" ? "opacity-4" : ""} ${startEnd && startEnd.single && startEnd.start === undefined ? "orange-bk" : ""}`}
                                    onMouseOver={(e) => {
                                        setLabels({
                                            dataType: `${nullOne} rows(${transferPercent(nullOne / sum * 100)}%)`,
                                            desc: `NULL/EMPTY`,
                                            type: "null-empty",
                                        })
                                    }} onMouseLeave={(e) => { setLabels(defaultLabels) }}
                                    onClick={() => {
                                        setFilterValues({ single: true, start: undefined })
                                    }} style={{ width: `${nullOne / sum * TB_WIDTH}px` }}></div>
                            </div>
                            <div className="th-2-desc">{labels.desc}</div>
                        </div>
                    </div>
                }
            }
            prev[key] = { datas, type: dataType.type, ele, }
        } else if (~['INTEGER', 'REAL'].indexOf(dataType.type)) {
            let datas = _.countBy(nData, key);
            let values = _.values(datas);
            let ele;
            if (values.length === 1) {
                ele = function (props) {
                    return <div className="th-2">
                        <div className="th-2-data-type">{dataType.type}</div>
                        <div className="relative">
                            <div className="th-2-chart"><div className="all-same-bk" style={{ width: `${TB_WIDTH}px` }}>all numbers: "{_.keys(datas)[0]}"</div></div>
                            <div className="th-2-desc"></div>
                        </div>
                    </div>
                }
            } else {
                const nullOne = _.find(datas, (v, k) => { return k === "undefined" || k === "null" || k === "NULL" || k === "NaN" });
                let arr = _.reduce(datas, (prev, curr, key) => {
                    prev.push({
                        value: curr,
                        key: dataType.type === 'INTEGER' ? parseInt(key) : parseFloat(key),
                    })
                    return prev;
                }, [])
                arr.sort((a, b) => { return a.key - b.key })
                let rangeOld = arr[arr.length - 1].key - arr[0].key;
                let rateOld = rangeOld / 20;
                let startValue = rateOld > 0.5 ? Math.floor(arr[0].key) : arr[0].key;
                startValue = rateOld > 2 && startValue > 0 && startValue < 2 * rateOld ? 0 : startValue;
                let endValue = rateOld > 0.5 ? Math.ceil(arr[arr.length - 1].key) : arr[arr.length - 1].key;
                let range = endValue - startValue;
                let count = _.find(_.range(10, 30), (r) => { return range % r === 0 }) || 20;
                let rate = range / count;
                let fixNum = 0;
                for (let i = 0; i < 10; i++) {
                    if (rate * Math.pow(10, i) >= 1) {
                        fixNum = i;
                        break;
                    }
                }
                let width = TB_WIDTH / (count + 1);
                let borderRight = width / 10 < 1 ? width / 10 : 1;
                let scaleArr = [];
                let maxNum = 0;
                for (let i = 0; i < count + 1; i++) {
                    let start = startValue + i * rate,
                        end = startValue + (i + 1) * rate, num = 0, rows = [];
                    _.each(arr, (v, index) => {
                        if (v.key >= start && v.key < end) {
                            num += v.value;
                            rows.push(v.key)
                        }
                    })
                    maxNum = maxNum < num ? num : maxNum;
                    if (!isNaN(start) && !isNaN(end)) {
                        scaleArr.push({
                            start, end, num, rows,
                        })
                    }
                }
                let sum = _.sum(values);
                ele = function (props) {
                    const { setFilterValues, startEnd } = props
                    const defaultLabels = {
                        dataType: dataType.type,
                        type: "default",
                        style: {},
                    };
                    const [labels, setLabels] = useState(defaultLabels);
                    useEffect(() => {
                        let currentLabels;
                        const { hoverRow } = props;
                        if (hoverRow && undefined !== datas[hoverRow[key]]) {
                            let propertyValue = hoverRow[key]
                            if (null === propertyValue || 'null' === propertyValue || 'NULL' === propertyValue) {
                                currentLabels = {
                                    dataType: `${nullOne} rows(${transferPercent(nullOne / sum * 100)}%)`,
                                    desc: `NULL/EMPTY`,
                                    type: "null-empty",
                                }
                            } else {
                                let left = _.findIndex(scaleArr, (curr, index) => {
                                    return curr.start <= propertyValue && propertyValue < curr.end;
                                }) * width;
                                currentLabels = {
                                    dataType: `${datas[propertyValue]} rows(${transferPercent(datas[propertyValue] / sum * 100)}%)`,
                                    desc: `${transferInteger(propertyValue, fixNum)}`,
                                    type: "single",
                                    k: propertyValue,
                                    left: left,
                                    style: { left: left - 5 },
                                }
                            }
                        } else {
                            currentLabels = defaultLabels;
                        }
                        setLabels(currentLabels);
                    }, [props.hoverRow])
                    let svgDatas = _.reduce(scaleArr, (svgDatas, curr, index) => {
                        svgDatas.push(<rect key={index} x={`${0 + index * width}`}
                            width={`${width - borderRight}`}
                            height={`${curr.num / maxNum * 30}`}
                            y={`${30 - curr.num / maxNum * 30}`}
                            fill="rgba(123,146,209)" fillOpacity="1"></rect>)
                        return svgDatas;
                    }, []);
                    return <div className="th-2">
                        <div className="th-2-data-type">{labels.dataType}</div>
                        <div className="relative" onMouseDown={(e) => {
                            e.preventDefault();
                            e.stopPropagation();
                        }} onClick={(e) => {
                            startEnd && setFilterValues(null);
                            e.preventDefault();
                            e.stopPropagation();
                        }}>
                            <svg height="30" width={TB_WIDTH} style={{ overflow: "visible" }}>
                                {svgDatas}
                                <line y1="31" y2="31" x1="0" x2={TB_WIDTH} strokeWidth="0.5" stroke="#d3d3d3"></line>
                                <rect width={TB_WIDTH} height="50" fillOpacity="0.1" fill="white"></rect>
                            </svg>
                            {<Selection setFilterValues={setFilterValues} startEnd={startEnd} className="th-2-positive" width={TB_WIDTH} height={50} data={scaleArr} maxNum={maxNum}></Selection>}
                            <div className="th-2-positive th-2-chart" style={{ width: `${TB_WIDTH}px` }}>
                                {_.map(scaleArr, (value, index) => {
                                    return <div key={value.start}
                                        className={`integer-bk ${labels === defaultLabels || (labels.k < value.start || labels.k >= value.end) ? "opacity-4" : ""}`}
                                        onClick={() => {
                                            setFilterValues({ start: value.start, end: value.end, selection: [width * index, width * (index + 1)] })
                                        }}
                                        onMouseOver={(e) => {
                                            setLabels({
                                                dataType: `${value.num} rows(${transferPercent(value.num / sum * 100)}%)`,
                                                desc: `${transferInteger(value.start, fixNum)} ${transferInteger(value.end, fixNum)}`,
                                                type: "common",
                                                k: value.start,
                                                style: { left: 0 + index * width - 5 },
                                            })
                                        }} onMouseLeave={(e) => { setLabels(defaultLabels) }} style={{ width: `${width}px`, borderRight: `${borderRight}px` }}></div>
                                })}
                            </div>
                            {labels.type === 'single' && <div
                                className={`absolute single-bk`}
                                style={{ width: `${2}px`, left: `${labels.left}px`, top: "0px" }}></div>}
                            {!labels.desc && <div className={`th-2-desc-left`}>{scaleArr[0] && transferInteger(scaleArr[0].start, fixNum)}</div>}
                            {!labels.desc && <div className={`th-2-desc-right`}>{scaleArr.length && transferInteger(scaleArr[scaleArr.length - 1].end, fixNum)}</div>}
                            <div className={`th-2-desc`} style={labels.style}>{labels.desc}</div>
                        </div>
                    </div>
                }
            }
            prev[key] = { datas, type: dataType.type, ele, }
        } else if (dataType.type === 'NULL') {
            prev[key] = {
                type: "null", ele: function (props) {
                    return <div className="th-2">
                        <div className="th-2-data-type">{dataType.type}</div>
                        <div className="relative">
                            <div className="th-2-chart"><div className="null-empty-bk" style={{ width: `${TB_WIDTH}px` }}>NULL/EMPTY</div></div>
                            <div className="th-2-desc">NULL/EMPTY</div>
                        </div>
                    </div>
                }
            }
        } else {
            prev[key] = {
                datas: _.countBy(nData, key), type: "others", ele: function (props) {
                    return <div className="th-2">
                        <div className="th-2-data-type">{dataType.type}</div>
                        <div className="relative">
                            <div className="th-2-chart"><div className="null-empty-bk" style={{ width: `${TB_WIDTH}px` }}>OTHERS</div></div>
                            <div className="th-2-desc">OTHERS</div>
                        </div>
                    </div>
                }
            }
        }
        return prev;
    }, {})
}
export function assignFiltersSorts(startEndMap, filteredInfo, sortedInfo, filters, sorts) {
    _.each(filteredInfo, (v, columnKey) => {
        if (v && columnKey) {
            let config = {
                columnName: columnKey,
                association: "c",
                value: v[0]
            };
            if (0 === _.filter(filters, (f, index) => {
                return _.isEqual(f, config);
            }).length) {
                if (!_.isEqual(filters[filters.length - 1], {})) {
                    filters.push({});
                }
                filters[filters.length - 1] = config
                filters.push({});
            }
        }
    })
    _.each(sortedInfo, (sorter, index) => {
        const { columnKey, order } = sorter
        if (columnKey && order) {
            let config = {
                column: columnKey,
                direction: order.startsWith("asc") ? "asc" : "desc"
            };
            if (0 === _.filter(sorts, (f, index) => {
                return _.isEqual(f, config);
            }).length) {
                if (!_.isEqual(sorts[sorts.length - 1], {})) {
                    sorts.push({});
                }
                sorts[sorts.length - 1] = config
                sorts.push({});
            } else {
                let samekeyConfigs = _.filter(sorts, (f, index) => {
                    return _.isEqual(f.column, config.column);
                })
                if (samekeyConfigs.length === 1) {
                    samekeyConfigs[0].direction = config.direction;
                }
            }
        }
    })
    _.map(startEndMap, (startEnd, key) => {
        const { single, start, end } = startEnd;
        let configs = [];
        if (single) {
            if (undefined === start) {
                configs.push({
                    columnName: key,
                    association: "n",
                    value: ""
                })
            } else {
                configs.push({
                    columnName: key,
                    association: "eq",
                    value: nanOrBlank(start) ? stringifyNanOrBlank(start) : start
                })
            }
        } else {
            configs.push({
                columnName: key,
                association: "gte",
                value: start
            })
            configs.push({
                columnName: key,
                association: "lt",
                value: end
            });
        }
        for (let index = 0; index < configs.length; index++) {
            const config = configs[index];
            if (0 === _.filter(filters, (f, index) => {
                return _.isEqual(f, config);
            }).length) {
                if (!_.isEqual(filters[filters.length - 1], {})) {
                    filters.push({});
                }
                filters[filters.length - 1] = config
                filters.push({});
            }
        }
    })
}
export function filterDatasFunc(startEndMap, nDataTmp) {
    return _.isEmpty(startEndMap) ? nDataTmp : _.filter(nDataTmp, (value, index) => {
        let ret = true;
        _.each(startEndMap, (startEnd, key) => {
            const { single, start, end } = startEnd;
            if (single) {
                if (undefined === start) {
                    if (!(nanOrBlank(value[key]) || value[key] === 'null' || value[key] === 'NULL')) {
                        ret = false;
                        return false;
                    }
                } else if (nanOrBlank(value[key]) || (value[key]).toString() !== start) {
                    ret = false;
                    return false;
                }
            } else if (!(value[key] >= start && value[key] < end)) {
                ret = false;
                return false;
            }
        })
        return ret;
    });
}
const KEY = "key";
export function TableComp(props) {
    const { nData, className, saveFunc } = props;
    if (!nData || !nData.length) {
        return <div>No Data.</div>
    }
    const [hoverRow, setHoverRow] = useState(null);
    const [startEndMap, setStartEndMap] = useState({});
    const [open, setOpen] = useState(~[EmptyResults].indexOf(nData) || isFailedResults(nData) ? false : true);
    const chartDatas = sqliteDatasToChartDatas(nData);
    const dataTypes = calcAutoType(nData, true, false);
    let keys = extractAllKeys(nData);
    if (!~keys.indexOf(KEY)) {
        keys.push(KEY);
        dataTypes[KEY] = { type: "number" }
    }
    keys.sort((a, b) => { return a === KEY ? -1 : (b === KEY ? 1 : 0) })
    const [filteredInfo, setFilteredInfo] = useState({});
    const [sortedInfo, setSortedInfo] = useState([]);
    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
    };
    const handleReset = () => {
        cache.clearFilters && cache.clearFilters();
        setFilteredInfo({});
        setSortedInfo([]);
    };
    const cache = {
        clearFilters: undefined,
    }
    const searchInput = useRef(null);
    const getColumnSearchProps = (dataIndex) => {
        if (dataIndex === KEY) {
            return {};
        }
        return {
            filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters, close }) => {
                cache.clearFilters = clearFilters;
                return <div style={{ padding: 8, }} onKeyDown={(e) => e.stopPropagation()}>
                    <Input ref={searchInput} placeholder={`Search ${dataIndex}`} value={selectedKeys[0]}
                        onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                        onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        style={{ marginBottom: 8, display: 'block', }}
                    />
                    <Space>
                        <Button type="primary" onClick={() => handleSearch(selectedKeys, confirm, dataIndex)} icon={<SearchOutlined />} size="small">
                            Search
                        </Button>
                        <Button onClick={() => handleReset()} size="small">
                            Reset
                        </Button>
                    </Space>
                </div>
            },
            filterIcon: (filtered) => (
                <SearchOutlined
                    style={{
                        color: filtered ? '#1890ff' : undefined,
                    }}
                />
            ),
            onFilter: (value, record) => {
                let text = record[dataIndex];
                if (nanOrBlank(text)) {
                    return stringifyNanOrBlank(text) === value
                }
                return (text).toString().includes(value)
            },
            onFilterDropdownOpenChange: (visible) => {
                if (visible) {
                    setTimeout(() => searchInput.current?.select(), 100);
                }
            },
            render: (text) => {
                if (nanOrBlank(text)) {
                    return <span className="text-muted">{stringifyNanOrBlank(text)}</span>
                }
                if (~[EmptyResults].indexOf(nData)) {
                    return <span>{text}</span>
                }
                if (isFailedResults(nData)) {
                    return <span className="text-danger">{text}</span>
                }
                let fts = _.filter(filteredInfo, (v, k) => { return v && k === dataIndex });
                if (fts.length === 1) {
                    return highligthComp(filteredInfo[dataIndex][0], (text).toString(), false);
                } else {
                    return (text).toString()
                }
            },
        }
    };
    let multiple = keys.length;
    let columns = _.reduce(keys, (prev, property, index) => {
        let chartData = chartDatas[property];
        let Comp = undefined === chartData ? (props) => { } : chartData.ele;
        let len = index === 0 ? new Number(nData.length).toString().length : property.length;
        let width = (!open || index === 0) && len * 14 < MAX_COLUMN_WIDTH ? len * 14 : MAX_COLUMN_WIDTH;
        let sorted = _.filter(sortedInfo, (v, index) => { return v.columnKey === property });
        let ncolumn = {
            key: property,
            title: index === 0 ?
                <div className="text-primary clicker" onClick={(e) => { setOpen(!open); e.stopPropagation(); }}>
                    {open ? <ChevronDownIcon /> : <ChevronRightIcon />}
                </div> :
                <div>
                    <div className="text-primary">{property}</div>
                    {<div className={!open ? "hide" : ""}><Comp startEnd={startEndMap[property]} setFilterValues={(props) => {
                        let startEndMapTmp = _.cloneDeep(startEndMap);
                        if (props) {
                            startEndMapTmp[property] = props;
                        } else {
                            delete startEndMapTmp[property];
                        }
                        setStartEndMap(startEndMapTmp)
                    }} hoverRow={hoverRow} ></Comp></div>}
                </div>,
            className: startEndMap[property] ? "bg-washed-yellow" : "",
            dataIndex: property,
            width: width,
            sortOrder: sorted.length === 1 && sorted[0].columnKey === property ? sorted[0].order : null,
            ellipsis: true,
            ...getColumnSearchProps(property),
        };
        if (index !== 0 && dataTypes[property].type === 'number') _.assign(ncolumn, { sorter: { compare: (a, b) => a[property] - b[property], multiple: multiple-- } })
        else if (dataTypes[property].type === 'string') _.assign(ncolumn, { sorter: { compare: (a, b) => a[property] > b[property] ? 1 : -1, multiple: multiple-- } })
        prev.push(ncolumn);
        return prev;
    }, []);
    let nDataTmp
    if (nData.length && undefined === nData[0][KEY]) {
        nDataTmp = _.map(_.cloneDeep(nData), (obj, index) => {
            if (!obj[KEY]) {
                obj[KEY] = index;
            }
            return obj
        })
    } else {
        nDataTmp = nData;
    }
    let datas = filterDatasFunc(startEndMap, nDataTmp);
    return <ResetTable reset={!_.isEmpty(startEndMap) || !_.isEmpty(filteredInfo) || !_.isEmpty(sortedInfo)} resetFunc={() => {
        setStartEndMap({});
        handleReset();
    }} saveFunc={saveFunc && (() => {
        saveFunc(startEndMap, filteredInfo, sortedInfo);
        setStartEndMap({});
        handleReset()
    })}><Table onChange={(pagination, filters, sorter, extra) => {
        setFilteredInfo(_.filter(filters, (v, k) => { return v }).length ? filters : {});
        setSortedInfo(sorter.length ? sorter : (sorter.columnKey && sorter.order ? [sorter] : []));
        // console.log("params", pagination, filters, sorter, extra);
    }} size="small" className={`${!!className && className}`} onRow={record => {
        return {
            onMouseEnter: event => { open && setHoverRow(record) }, //
            onMouseLeave: event => { open && setHoverRow(null) },
        };
    }} pagination={(!_.isEmpty(startEndMap) || !_.isEmpty(filteredInfo) || !_.isEmpty(sortedInfo) || datas.length > 5) && {
        size: 'small', pageSizeOptions: _.reduce([5, 10, 20, 50, 100, 200, 500, 1000], (prev, curr) => { if (datas.length >= curr) { prev.push(curr); } return prev; }, []),
        defaultPageSize: 5, showTotal: (total, range) => `${range[0]}-${range[1]} of ${total}`
    }} showSorterTooltip={false} sticky scroll={{ x: '90%' }} columns={columns} dataSource={datas} /></ResetTable>;
}


export function withPopMenu(WrappedComponent, props) {
    const { icon, label, ...propsa } = props;
    let menus = <Menu className={``}>
        <Menu.Item key="file" className="pop-menu-title" disabled >
            <WrappedComponent label={label} {...propsa}></WrappedComponent>
        </Menu.Item>
    </Menu>
    return <Dropdown overlay={menus} trigger={'click'} placement="topCenter">
        <Button icon={icon}>
            {label}
        </Button>
    </Dropdown>
}

const NUMBER_NONULL = [{ label: '=', value: "eq" }, { label: '≠', value: "ne" }, { label: '<', value: "lt" }, { label: '>', value: "gt" },
{ label: '<=', value: "lte" }, { label: '>=', value: "gte" }, { label: 'is in', value: "in", placeholder: '1,2' }, { label: 'is not in', value: "nin", placeholder: '1,2' }]
const STRING_NONULL = [{ label: 'is', value: "eq" }, { label: 'is not', value: "ne" }, { label: 'contains', value: "c" },
{ label: 'does not contain', value: "nc" }, { label: 'is in', value: "in", placeholder: '"string1","string2"' }, { label: 'is not in', value: "nin", placeholder: '"string1","string2"' }]
const ABLE_NULL = [{ label: 'is null', value: "n" }, { label: 'is not null', value: "nn" }];
const OBJECT_NONULL = [{ label: 'is', value: "eq" }, { label: 'is not', value: "ne" },]
const mapByDataType = {
    number_nonull: NUMBER_NONULL,
    number: _.concat(ABLE_NULL, NUMBER_NONULL),
    string_nonull: STRING_NONULL,
    string: _.concat(ABLE_NULL, STRING_NONULL),
    object_nonull: OBJECT_NONULL,
    object: _.concat(ABLE_NULL, OBJECT_NONULL),
}


const SelectComp = (props) => {
    const { columns, columnName, setColumnName, association, setAssociation, value, setValue, removeFilter, getPopupContainer } = props;
    const genAssociations = (columnName) => {
        if (!columnName) {
            return [];
        }
        let column = _.filter(columns, (column, index) => { return getColumnName(column) === columnName })[0];
        if (!column) {
            return [];
        }
        let associations;
        if (/^(string|char|varchar|text|S)$/i.test(getColumnType(column))) {
            associations = isColumnNoNullable(column) ? mapByDataType.string_nonull : mapByDataType.string
        } else if (/^(object)$/i.test(getColumnType(column))) {
            associations = isColumnNoNullable(column) ? mapByDataType.object_nonull : mapByDataType.object
        } else {
            associations = isColumnNoNullable(column) ? mapByDataType.number_nonull : mapByDataType.number
        }
        return associations;
    }
    const [associations, setAssociations] = useState(genAssociations(columnName));
    const handleColumnChange = (columnName) => {
        let associations = genAssociations(columnName);
        setColumnName(columnName);
        setAssociation(associations[0].value)
        setValue("");
        setAssociations(associations);
    };
    const onAssociationChange = (association) => {
        setAssociation(association)
    }
    return <div className="d-flex">
        <Select
            getPopupContainer={getPopupContainer || (() => { return document.body })}
            style={{
                width: 160,
            }}
            placeholder={"Column"}
            value={columnName}
            onChange={handleColumnChange}
        >
            {columns.map((column) => (
                <Option key={getColumnName(column)}>
                    <div className="d-flex justify-content-between">
                        <span>{getColumnName(column)}&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span className="text-muted small">{getColumnType(column)}{getColumnKeyType(column)}</span>
                    </div>
                </Option>
            ))}
        </Select>
        <Select
            getPopupContainer={getPopupContainer || (() => { return document.body })}
            style={{
                width: 160,
            }}
            placeholder={"Operator"}
            value={association}
            onChange={onAssociationChange}
        >
            {associations.map((association) => (
                <Option key={association.value} value={association.value}>{association.label}</Option>
            ))}
        </Select>
        {!~['n', 'nn'].indexOf(association) && <Input style={{
            width: 160,
        }} value={value} placeholder={association && _.reduce(associations, (prev, v) => { if (v.value === association) { prev = v.placeholder } return prev; }, "") || ""} onChange={(e) => {
            setValue(e.target.value);
        }} ></Input>}
        {removeFilter && <Button type="text" icon={<CloseOutlined />} onClick={removeFilter} />}
    </div>
}

const MultiSelectComp = (props) => {
    const { columns, removeJoinArr, joinArr, setJoinArr, fieldName, setFieldName } = props;
    return <div className="d-flex">
        <Input style={{
            width: 160,
        }} value={fieldName} placeholder={"Field Name"} onChange={(e) => {
            if (0 === _.filter(columns, (v, index) => { return e.target.value === v.value }).length) {
                setFieldName(e.target.value);
            }
        }} ></Input>
        <Select
            mode="multiple"
            allowClear
            style={{
                width: 160,
            }}
            placeholder="Select Columns"
            value={joinArr}
            onChange={(value) => {
                setJoinArr(value);
            }}
            options={columns}
        />
        {removeJoinArr && <Button type="text" icon={<CloseOutlined />} onClick={removeJoinArr} />}
    </div>
}
/**
 * 
 * @param {*} props 
 * @returns {Table}
 */
export const ResetTable = (props) => {
    const { reset, resetFunc, saveFunc, ...propsa } = props;
    return <div className='reset-table-div'>
        {props.children}
        <div className={`relative ${!reset ? "hide" : ""}`}>
            <div className='reset-table'>
                {saveFunc && <Button size='small' onClick={() => {
                    saveFunc && saveFunc();
                }}>Save</Button>}
                <Button size='small' onClick={() => {
                    resetFunc && resetFunc();
                }}>Reset</Button>
            </div>
        </div>
    </div>
}

export const JoinComp = (props = { label: "", columns: [], joinColumns: {}, setJoinColumns: () => { } }) => {
    const { label, columns, joinColumns, setJoinColumns } = props;
    const list = _.reduce(joinColumns, (prev, v, k) => {
        prev.push({
            fieldName: k,
            joinArr: v,
        })
        return prev;
    }, []);
    if (!_.isEqual(list[list.length - 1], {
        fieldName: "",
        joinArr: [],
    })) {
        list.push({
            fieldName: "",
            joinArr: [],
        });
    }
    const func = () => {
        setJoinColumns(_.reduce(list, (prev, joinObj, index) => {
            const { fieldName, joinArr } = joinObj;
            if (fieldName) {
                prev[fieldName] = joinArr
            }
            return prev;
        }, {}));
    }
    return <div>
        {label}
        <Divider style={{ margin: "4px 0px" }} />
        <div className="d-flex flex-column">
            {_.map(list, (joinObj, index) => {
                const { fieldName, joinArr } = joinObj;
                return <MultiSelectComp key={index}
                    columns={columns}
                    fieldName={fieldName}
                    setFieldName={(fieldName) => {
                        joinObj.fieldName = fieldName;
                        func();
                    }}
                    joinArr={joinArr}
                    setJoinArr={(joinArr) => {
                        joinObj.joinArr = joinArr;
                        func();
                    }}
                    removeJoinArr={index !== list.length - 1 ? () => {
                        list.splice(index, 1);
                        func();
                    } : undefined}
                ></MultiSelectComp>
            })}
        </div>
    </div>
}

export const FilterComp = (props = { columns: [] }) => {
    const { label, columns, filters, setFilters } = props;
    return <div>
        {label}
        <Divider style={{ margin: "4px 0px" }} />
        <div className="d-flex flex-column">
            {_.map(filters, (filter, index) => {
                const { columnName, association, value } = filter;
                return <SelectComp key={undefined === columnName ? "undefined" + index : columnName + index}
                    columns={columns}
                    columnName={columnName}
                    setColumnName={(columnName) => {
                        filter.columnName = columnName;
                        setFilters(filters);
                    }}
                    association={association}
                    setAssociation={(association) => {
                        filter.association = association;
                        setFilters(filters);
                    }}
                    value={value}
                    setValue={(value) => {
                        filter.value = value;
                        setFilters(filters);
                    }}
                    removeFilter={index !== filters.length - 1 ? () => {
                        filters.splice(index, 1);
                        setFilters(filters)
                    } : undefined}
                ></SelectComp>
            })}
        </div>
    </div>
}

export const FilterMongoComp = (props = {
    columns: [], filters: {
        nor: false, logic: "and", rules: [{}]
    }
}) => {
    const { label, columns, filters, setFilters, getPopupContainer } = props;
    const { nor, logic, rules } = _.assign({
        nor: false, logic: "and", rules: [{}]
    }, filters)
    return <div>
        {label}
        <Divider style={{ margin: "4px 0px" }} />
        <div className="d-flex align-items-center">
            <Switch checkedChildren="Not" unCheckedChildren="Not" checked={nor} onChange={(checked) => {
                filters.nor = checked;
                setFilters(filters)
            }} />
            <Radio.Group onChange={(e) => {
                filters.logic = e.target.value;
                setFilters(filters)
            }} value={logic}>
                <Radio value={"and"}>And</Radio>
                <Radio value={"or"}>Or</Radio>
            </Radio.Group>
        </div>
        <div className="d-flex flex-column">
            {_.map(rules, (rule, index) => {
                const { columnName, association, value } = rule;
                return <SelectComp key={undefined === columnName ? "undefined" + index : columnName + index}
                    getPopupContainer={getPopupContainer || (() => { return document.body })}
                    columns={columns}
                    columnName={columnName}
                    setColumnName={(columnName) => {
                        rule.columnName = columnName;
                        setFilters(filters);
                    }}
                    association={association}
                    setAssociation={(association) => {
                        rule.association = association;
                        setFilters(filters);
                    }}
                    value={value}
                    setValue={(value) => {
                        rule.value = value;
                        setFilters(filters);
                    }}
                    removeFilter={index !== filters.length - 1 ? () => {
                        rules.splice(index, 1);
                        setFilters(filters)
                    } : undefined}
                ></SelectComp>
            })}
        </div>
    </div >
}

const NUMBER_DY_NONULL = [
    { label: 'equal', value: "EQ" }, { label: 'not equal', value: "NE" }, { label: 'is in', value: "IN", placeholder: '1,2' },
    { label: 'less than or equal to', value: "LE" }, { label: 'less than', value: "LT" }, { label: 'greater than or equal to', value: "GE" }, { label: 'greater than', value: "GT" },
    { label: 'between', value: "BETWEEN" }
]
const STRING_DY_NONULL = [
    { label: 'equal', value: "EQ" }, { label: 'not equal', value: "NE" }, { label: 'is in', value: "IN", placeholder: '"string1","string2"' },
    { label: 'less than or equal to', value: "LE" }, { label: 'less than', value: "LT" }, { label: 'greater than or equal to', value: "GE" }, { label: 'greater than', value: "GT" },
    { label: 'between', value: "BETWEEN" },
    { label: 'contains', value: "CONTAINS", title: "substring or value in a set" },
    { label: 'not contains', value: "NOT_CONTAINS", title: "absence of a substring or absence of a value in a set" },
    { label: 'begins with', value: "BEGINS_WITH", title: "begins with for a substring prefix" },
]
const BINARY_DY_NONULL = STRING_DY_NONULL;
const BOOLEAN_DY_NONULL = [
    { label: 'equal', value: "EQ" }, { label: 'not equal', value: "NE" }
]
const ABLE_DY_NULL = [{ label: 'not exists', value: "NULL" }, { label: 'exists', value: "NOT_NULL" }];
const mapDyByDataType = {
    number: _.concat(NUMBER_DY_NONULL, ABLE_DY_NULL),
    string: _.concat(STRING_DY_NONULL, ABLE_DY_NULL),
    binary: _.concat(BINARY_DY_NONULL, ABLE_DY_NULL),
    boolean: _.concat(BOOLEAN_DY_NONULL, ABLE_DY_NULL),
    null: ABLE_DY_NULL,
}
const columnDyTypes = {
    S: "string",
    N: "number",
    B: "binary",
    BOOL: "boolean",
    NULL: "null",
}
const DEFAULT_TYPE = "S"

const SelectDyComp = (props) => {
    const { columns, columnName, setColumnName, columnType, setColumnType, association, setAssociation, values, setValues, removeFilter, getPopupContainer } = props;
    const genAssociations = (columnName) => {
        if (!columnName) {
            return [];
        }
        let column = _.filter(columns, (column, index) => { return getColumnName(column) === columnName })[0];
        if (!column) {
            return [];
        }
        let associations = mapDyByDataType[columnDyTypes[getColumnType(column)]];
        return associations || mapDyByDataType[columnDyTypes[DEFAULT_TYPE]];
    }
    const [associations, setAssociations] = useState(genAssociations(columnName));
    const handleColumnChange = (columnName) => {
        let associations = genAssociations(columnName);
        setColumnName(columnName);
        let column = _.filter(columns, (column, index) => { return getColumnName(column) === columnName })[0];
        if (column) {
            setColumnType(getColumnType(column));
        } else {
            setColumnType(DEFAULT_TYPE);
        }
        setAssociation(associations.length ? associations[0].value : undefined)
        setValues([]);
        setAssociations(associations);
    };
    const handleColumnTypeChange = (columnType) => {
        setColumnType(columnType);
        let associations = mapDyByDataType[columnDyTypes[columnType]]
        if (0 === _.filter(associations, (ass, index) => { return ass.value === association }).length) {
            setAssociation(associations.length ? associations[0].value : undefined)
            setValues([]);
        }
        setAssociations(associations);
    };
    const onAssociationChange = (association) => {
        setAssociation(association);
        setValues([])
    }
    return <div className="d-flex">
        <Select
            getPopupContainer={getPopupContainer || (() => { return document.body })}
            style={{
                width: 160,
            }}
            placeholder={"Attribute name"}
            value={columnName}
            onChange={handleColumnChange}
        >
            {columns.map((column) => (
                <Option key={getColumnName(column)}>
                    <div className="d-flex justify-content-between">
                        <span>{getColumnName(column)}&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span className="text-muted small">{getColumnType(column)}{getColumnKeyType(column)}</span>
                    </div>
                </Option>
            ))}
        </Select>
        <Select getPopupContainer={getPopupContainer || (() => { return document.body })}
            style={{
                width: 160,
            }}
            placeholder={"Type"}
            value={columnType || DEFAULT_TYPE}
            onChange={handleColumnTypeChange}
        >
            {_.map(columnDyTypes, (v, k) => (
                <Option key={k}>
                    <div className="d-flex justify-content-between">
                        <span>{v}</span>
                    </div>
                </Option>
            ))}
        </Select>
        <Select
            getPopupContainer={getPopupContainer || (() => { return document.body })}
            style={{
                width: 160,
            }}
            placeholder={"Condition"}
            value={association}
            onChange={onAssociationChange}
        >
            {associations.map((association) => (
                <Option key={association.value} value={association.value}>{association.label}</Option>
            ))}
        </Select>
        {!~['NULL', 'NOT_NULL'].indexOf(association) && <Input style={{
            width: 160,
        }} value={values[0] || ""} placeholder={association && _.reduce(associations, (prev, v) => { if (v.value === association) { prev = v.placeholder } return prev; }, "") || "Value"} onChange={(e) => {
            if (~['IN'].indexOf(association)) {
                setValues(e.target.value.split(","));
            } else {
                values[0] = e.target.value
                setValues(values);
            }
        }} ></Input>}
        {!!~['BETWEEN'].indexOf(association) && <Input style={{
            width: 160,
        }} value={values[1] || ""} placeholder={"Value"} onChange={(e) => {
            values[1] = e.target.value
            setValues(values);
        }} ></Input>}
        {removeFilter && <Button type="text" icon={<CloseOutlined />} onClick={removeFilter} />}
    </div>
}

export const FilterDyComp = (props = { label: "", columns: [], filters: {}, setFilters: () => { } }) => {
    const { label, columns, filters, setFilters } = props;
    return <div>
        {label}
        <div className="d-flex flex-column">
            {_.map(filters, (filter, index) => {
                const { columnName, columnType, association, values } = filter;
                return <SelectDyComp key={undefined === columnName ? "undefined" + index : columnName + index}
                    columns={columns}
                    columnName={columnName}
                    setColumnName={(columnName) => {
                        filter.columnName = columnName;
                        setFilters(filters);
                    }}
                    columnType={columnType}
                    setColumnType={(columnType) => {
                        filter.columnType = columnType;
                        setFilters(filters);
                    }}
                    association={association}
                    setAssociation={(association) => {
                        filter.association = association;
                        setFilters(filters);
                    }}
                    values={values || []}
                    setValues={(values) => {
                        filter.values = values;
                        setFilters(filters);
                    }}
                    removeFilter={index !== filters.length - 1 ? () => {
                        filters.splice(index, 1);
                        setFilters(filters)
                    } : undefined}
                ></SelectDyComp>
            })}
        </div>
    </div>
}


export const ColumnsDetailComp = (props) => {
    const { columns, label } = props;

    return (
        <>
            <div className="d-flex align-items-center">
                {label}
            </div>
            <Divider style={{ margin: "4px 0px" }} />
            <div style={{ display: "grid" }}>
                {columns}
            </div>
        </>
    );
};

export const ColumnsComp = (props) => {
    const { plainOptions, checkedList, setCheckedList, label } = props;
    const [indeterminate, setIndeterminate] = useState(!!checkedList.length && checkedList.length < plainOptions.length);
    const [checkAll, setCheckAll] = useState(false);

    const onChange = (list) => {
        setCheckedList(list);
        setIndeterminate(!!list.length && list.length < plainOptions.length);
        setCheckAll(list.length === plainOptions.length);
    };

    const onCheckAllChange = (e) => {
        setCheckedList(e.target.checked ? _.reduce(plainOptions, (prev, curr) => {
            prev.push(curr.value);
            return prev;
        }, []) : []);
        setIndeterminate(!e.target.checked);
        setCheckAll(e.target.checked);
    };

    return (
        <>
            <div className="d-flex align-items-center">
                {label}&nbsp;&nbsp;&nbsp;&nbsp;
                <Checkbox indeterminate={indeterminate} onChange={onCheckAllChange} checked={checkAll}>
                    Show all
                </Checkbox>
            </div>
            <Divider style={{ margin: "4px 0px" }} />
            <CheckboxGroup style={{ display: "grid" }} options={plainOptions} value={checkedList} onChange={onChange} />
        </>
    );
};


const SortSelectComp = (props) => {
    const { columns, columnName, setColumnName, sortType, setSortType, removeSort, getPopupContainer } = props;
    const sortTypes = [{ label: "Desending", value: "desc" }, { label: "Asending", value: "asc" }];
    const handleColumnChange = (columnName) => {
        setColumnName(columnName);
        setSortType(sortTypes[0].value)
    };
    const onSortTypeChange = (sortType) => {
        setSortType(sortType)
    }
    return <div className="d-flex">
        <Select
            getPopupContainer={getPopupContainer || (() => { return document.body })}
            style={{
                width: 160,
            }}
            placeholder={"Column"}
            value={columnName}
            onChange={handleColumnChange}
        >
            {columns.map((column) => (
                <Option key={getColumnName(column)}>
                    <div className="d-flex justify-content-between">
                        <span>{getColumnName(column)}&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span className="text-muted small">{getColumnType(column)}{getColumnKeyType(column)}</span>
                    </div>
                </Option>
            ))}
        </Select>
        <Select
            getPopupContainer={getPopupContainer || (() => { return document.body })}
            style={{
                width: 160,
            }}
            placeholder={"Operator"}
            value={sortType}
            onChange={onSortTypeChange}
        >
            {sortTypes.map((sortType) => (
                <Option key={sortType.value} value={sortType.value}>{sortType.label}</Option>
            ))}
        </Select>
        {removeSort && <Button type="text" icon={<CloseOutlined />} onClick={removeSort} />}
    </div>
}
export const SortComp = (props = { columns: [] }) => {
    const { label, columns, sorts, setSorts, getPopupContainer } = props;
    return <div>
        {label}
        <Divider style={{ margin: "4px 0px" }} />
        <div className="d-flex flex-column">
            {_.map(sorts, (sort, index) => {
                const { column, direction } = sort;
                return <SortSelectComp key={undefined === column ? "undefined" + index : column + index}
                    getPopupContainer={getPopupContainer || (() => { return document.body })}
                    columns={columns}
                    columnName={column}
                    setColumnName={(columnName) => {
                        sort.column = columnName;
                        setSorts(sorts);
                    }}
                    sortType={direction}
                    setSortType={(sortType) => {
                        sort.direction = sortType;
                        setSorts(sorts);
                    }}
                    removeSort={index !== sorts.length - 1 ? () => {
                        sorts.splice(index, 1);
                        setSorts(sorts)
                    } : undefined}
                ></SortSelectComp>
            })}
        </div>
    </div>
}

export const SliceComp = (props) => {
    const { setSlice, slice, slice: { from, to }, dialect } = props;

    return <div className="d-flex flex-column">
        <div className="">
            <label>From:&nbsp;&nbsp;&nbsp;&nbsp;</label>
            <Radio.Group onChange={(e) => {
                slice.from = e.target.value;
                setSlice(slice);
            }} value={from}>
                <Radio value={from === 'start' ? 0 : from}>
                    <InputNumber min={0} max={Infinity} value={from === 'start' ? 0 : from} readOnly={DatabaseType.DynamoDB === dialect} onChange={(value) => {
                        slice.from = value;
                        setSlice(slice);
                    }}></InputNumber>
                </Radio>
                <Radio value={'start'}>Start</Radio>
            </Radio.Group>
        </div>
        <div>
            <label>To:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
            <Radio.Group onChange={(e) => {
                slice.to = e.target.value;
                setSlice(slice);
            }} value={to}>
                <Radio value={to === 'end' ? 10000 : to}>
                    <InputNumber min={0} max={Infinity} value={to === 'end' ? 10000 : to} onChange={(value) => {
                        slice.to = value;
                        setSlice(slice);
                    }}></InputNumber>
                </Radio>
                <Radio value={'end'}>End</Radio>
            </Radio.Group>
        </div>
    </div>
}

export const ErrorComp = function (props) {
    const { component } = props;
    const { error } = component.state;
    /**@type{import ('../../tagEditor').default} */
    let editor = component.editor;
    if (!error) {
        return false;
    }
    if (
        error.name === "RuntimeError" &&
        error.input &&
        (runtime._builtin._scope.get(error.input) ||
            (editor.getMain()._cachedata.variables[error.input] &&
                editor.getMain()._cachedata.variables[error.input].varia))
    ) {
        if (error.message === "Failed to fetch") {
            return (
                <Button
                    type="text"
                    className="text-primary"
                    onClick={async (e) => {
                        let variant =
                            runtime._builtin._scope.get(error.input) ||
                            (editor.getMain()._cachedata.variables[error.input] &&
                                editor.getMain()._cachedata.variables[error.input].varia);
                        variant.define(error.input, [], variant._definition);
                        await component.runCode();
                    }}
                >
                    Fetch again
                </Button>
            );
        } else {
            if (getCellName(component.getRealValue(true)) === error.input) {
                return false;
            }
            return (
                <Button
                    type="text"
                    className="text-danger"
                    onClick={async (e) => {
                        let editorKey = editor.getTransferKey();
                        let className = `editorjs-anchor-${error.input.replace(/\s/g, "_")}`;
                        let data = document.querySelector(`.editorjs-wrapper.${editorKey}>.editorjs .${className}`);
                        data && data.closest(".code-edit") && setTimeout(() => data.closest(".code-edit").editCode(true));
                    }}
                >
                    Jump to error
                </Button>
            );
        }
    } else {
        return false;
    }
};