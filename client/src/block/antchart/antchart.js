import _ from 'lodash';
import React from "react";
import ReactDOM from "react-dom";
import editor from 'commonEditor';
import { v4 as uuidv4, NIL as NIL_UUID } from 'uuid';
import TagEditor from '../../tagEditor';
import AntChartEditor from "./AntChartEditor";
import { RenderType } from 'util/databaseApi';
import { cell_shortcuts_keys, IsKey, linkKeys } from 'util/utils';
import { createElemnt } from 'util/helpers';
import { DefaultAntChartConfig } from './comp/OptionsTypes'
const { genDefaultAntChartData } = require("./antchartUtils");

export default class AntChart {
    static get toolbox() {
        return {
            title: 'Chart',
            icon: '<i class="fas fa-chart-bar"></i>'
        };
    }

    constructor({ data, config, api, readOnly }) {
        this.api = api;
        this.readOnly = readOnly;
        this.CSS = {
            baseClass: this.api.styles.block,
            input: this.api.styles.input,
            wrapper: 'ce-chart',
        };
        /**@type{TagEditor} */
        this.editor = (!config.editorjsKey || config.editorjsKey === editor.getTransferKey()) ? editor :
            _.filter(window.tagEditors, (tagEditor) => { return tagEditor.getTransferKey() === config.editorjsKey })[0];
        this.renderTask = this.editor.renderTask;
        /**
         * @type {{
         *  chartConfigs: Object.<string, DefaultAntChartConfig>,
         *  layouts: {},
         *}}
        */
        this.data = data.chartConfigs ? data : genDefaultAntChartData();
        let tmp = _.reduce(this.data.chartConfigs, (prev, chartConfig, key) => {
            if (!chartConfig.renderType) {
                if (chartConfig.type) {
                    chartConfig.renderType = RenderType.Chart;
                } else {
                    chartConfig.renderType = RenderType.Markdown;
                    chartConfig.text = "";
                }
            }
            if (!chartConfig.chartKey || chartConfig.chartKey.length < NIL_UUID.length) {
                chartConfig.chartKey = uuidv4();
                prev[chartConfig.chartKey] = chartConfig;
            } else {
                prev[chartConfig.chartKey] = chartConfig;
            }
            return prev;
        }, {})
        this.data.chartConfigs = tmp;
        if (!this.data.dname) {
            this.data.dname = uuidv4();
        }
        !(this.data.layouts) && (this.data.layouts = {});
        this.ref = React.createRef();
    }

    render() {
        this.wrapper = document.createElement('div');
        this.wrapper.classList.add(this.CSS.baseClass, this.CSS.wrapper);
        this.renderTask.submitTask((callback) => {
            ReactDOM.render(<AntChartEditor ref={this.ref} editor={this.editor} data={this.data}
                saveData={this.saveData} api={this.api} readOnly={this.readOnly}
            ></AntChartEditor>, this.wrapper, () => {
                callback();
            });
        })
        return this.wrapper;
    }

    beforeRemoved(params) {
        if (this.ref.current) {
            ReactDOM.unmountComponentAtNode(this.wrapper)
        }
    }

    isFirstBlock() {
        return this.api.blocks.getCurrentBlockIndex() === 0;
    }

    renderSettings() {
        let newChartTitleEle = window.copyChart ?
            createElemnt(`<div>Paste Chart</div>
        <div class="ce-toolbar__plus-shortcut">${linkKeys(cell_shortcuts_keys["New Sub Block"])}</div>`) :
            createElemnt(`<div>New Chart</div>
        <div class="ce-toolbar__plus-shortcut">${linkKeys(cell_shortcuts_keys["New Sub Block"])}</div>`);
        const settings = [
            {
                name: 'new-chart',
                icon: `<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M15.8 10.592v2.043h2.35v2.138H15.8v2.232h-2.25v-2.232h-2.4v-2.138h2.4v-2.28h2.25v.237h1.15-1.15zM1.9 8.455v-3.42c0-1.154.985-2.09 2.2-2.09h4.2v2.137H4.15v3.373H1.9zm0 2.137h2.25v3.325H8.3v2.138H4.1c-1.215 0-2.2-.936-2.2-2.09v-3.373zm15.05-2.137H14.7V5.082h-4.15V2.945h4.2c1.215 0 2.2.936 2.2 2.09v3.42z"/></svg>`,
                func: "newChart",
                args: () => {
                    let ret = [window.copyChart];
                    window.copyChart = undefined;
                    newChartTitleEle.innerHTML = `<div>New Chart</div>
                    <div class="ce-toolbar__plus-shortcut">${linkKeys(cell_shortcuts_keys["New Sub Block"])}</div>`
                    return ret;
                },
                title: newChartTitleEle,
            }
        ];
        const wrapper = document.createElement('div');
        settings.forEach(tune => {
            let button = document.createElement('div');
            // button.title = tune.title;
            this.api.tooltip.onHover(button, tune.title, { placement: "top" });
            button.classList.add(tune.name);
            button.classList.add('cdx-settings-button');
            button.innerHTML = tune.icon;
            button.addEventListener('click', () => {
                this.ref.current[tune.func](...(tune.args ? tune.args() : []));
                button.classList.toggle('cdx-settings-button--active');
            });
            wrapper.appendChild(button);
        });
        this.settingWrapper = wrapper;
        return wrapper;
    }

    saveData = (data) => {
        this.data = data;
        this.editor.setNeedSave(true);
    }

    save(blockContent) {
        return this.data;
    }

    keydownFunc(e) {
        e = e || window.event;
        let key = String.fromCharCode(e.keyCode).toLowerCase();
        //filter the number & char key
        if (!(/^[0-9a-z]$/g).test(key) || e.key === e.code) {
            key = e.key;
        }
        if (IsKey(e, key, cell_shortcuts_keys['New Sub Block'])) {
            this.ref.current && this.ref.current.newChart(window.copyChart);
            window.copyChart = undefined;
        }
    }

    static get isReadOnlySupported() {
        return true;
    }

    static get notSupportBackspace() {
        return true;
    }

    static get enableLineBreaks() {
        return true;
    }
}