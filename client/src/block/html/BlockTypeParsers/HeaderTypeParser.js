export function parseHeaderToHtml(blocks) {
  switch (blocks.level) {
    case 1:
      return `<h1>${blocks.text}</h1>`;
    case 2:
      return `<h2>${blocks.text}</h2>`;
    case 3:
      return `<h3>${blocks.text}</h3>`;
    case 4:
      return `<h4>${blocks.text}</h4>`;
    case 5:
      return `<h5>${blocks.text}</h5>`;
    case 6:
      return `<h6>${blocks.text}</h6>`;
    default:
      break;
  }
}

export function parseMarkdownToHeader(blocks) {
  let headerData = {};

  switch (blocks.depth) {
    case 1:
      blocks.children.forEach((item) => {
        headerData = {
          data: {
            level: 1,
            text: item.value,
          },
          type: 'header',
        };
      });

      return headerData;
    case 2:
      blocks.children.forEach((item) => {
        headerData = {
          data: {
            level: 2,
            text: item.value,
          },
          type: 'header',
        };
      });

      return headerData;
    case 3:
      blocks.children.forEach((item) => {
        headerData = {
          data: {
            level: 3,
            text: item.value,
          },
          type: 'header',
        };
      });

      return headerData;
    case 4:
      blocks.children.forEach((item) => {
        headerData = {
          data: {
            level: 4,
            text: item.value,
          },
          type: 'header',
        };
      });

      return headerData;
    case 5:
      blocks.children.forEach((item) => {
        headerData = {
          data: {
            level: 5,
            text: item.value,
          },
          type: 'header',
        };
      });

      return headerData;
    case 6:
      blocks.children.forEach((item) => {
        headerData = {
          data: {
            level: 6,
            text: item.value,
          },
          type: 'header',
        };
      });

      return headerData;
    default:
      break;
  }
}
