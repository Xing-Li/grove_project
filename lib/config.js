import { existsSync, readFileSync, writeFileSync, createReadStream } from "fs";
import { homedir } from "os";
import { join } from "path";
import { exit } from "./errors";

const csv = require('csv-parser')
const configFile = "config.json";

export const config = {
  "encryption": {
    "password": "asdf1234",
  },
}
var rawConfigData = null;

export function rawConfig() {
  if (rawConfigData) {
    return rawConfigData;
  } else {
    if (!existsSync(configFile)) {
      rawConfigData = {
        "ssl": "disabled",
        "port": 2902,
      };
      writeFileSync(configFile, JSON.stringify(rawConfigData, null, 2), { mode: 0o600 });
      return rawConfigData;
    }
    rawConfigData = JSON.parse(readFileSync(configFile));
    global.AppConfig = rawConfigData;
    return rawConfigData;
  }
}

rawConfig();

export function decodeSecret(secret) {
  try {
    return JSON.parse(Buffer.from(secret, "base64"));
  } catch (error) {
    exit(error);
  }
}
