const express = require('express');
const passport = require('passport');
const util = require('./util/util');
const User = require('./user');
const auth = require('./util/auth')
const GroveShared = require('./groveShared')
const GroveShareProjects = require('./groveShareProjects')
const UserShareProjects = require('./userShareProjects')
const GrovePinned = require('./grovePinned')
const API_app = express.Router();

//support raw body JSON Data  and merge parameters
API_app.use(util.parseRawJSONData);
API_app.use(util.mergeReqParams);
API_app.use('/user', User);
// API_app.use('/grovePinned', auth.protectedShare, GrovePinned);
API_app.use('/groveShared', auth.protected, GroveShared)
API_app.use('/groveShareProjects', auth.protected, GroveShareProjects)
API_app.use('/userShareProjects', auth.protected, UserShareProjects)

module.exports = API_app;