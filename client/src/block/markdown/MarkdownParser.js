import { fileDownloadHandler, editorjsTransferToMd } from './FileHandler';

/**
 * Markdown Parsing class
 */
export default class MarkdownParser {
  /**
   * creates the Parser plugin
   * {editorData, api functions} - necessary to interact with the editor
   */
  constructor({ data, api }) {
    this.data = data;
    this.api = api;
  }

  /**
   * @return Plugin data such as title and icon
   */
  static get toolbox() {
    return {
      title: 'Export Markdown',
      icon: '<i class="fas fa-file-export"></i>',
    };
  }

  /**
   * @return empty div and run the export funtion
   */
  render() {
    const doc = document.createElement('div');

    this.getContent();
    return doc;
  }

  /**
   * Function which takes saved editor data and runs the different parsing helper functions
   * @return Markdown file download
   */
  async getContent() {
    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0'); // January is 0!
    const yyyy = today.getFullYear();
    const data = await this.api.saver.save();
    // take parsed data and create a markdown file
    fileDownloadHandler(editorjsTransferToMd(data), `entry_${dd}-${mm}-${yyyy}.md`);
  }

  save() {
    return {
      message: 'Downloading Markdown',
    };
  }
  validate(savedData) {
    return false;
  }

  static get isReadOnlySupported() {
    return true;
  }
}
