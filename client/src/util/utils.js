import _ from 'lodash';
import { Modal, message as AntDToast, notification, Space, Button } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { BrowserChecks, osfunction, OSType, assembling } from './helpers';
import { readAsDataURL } from 'file/fileUtils';
import { version as uuidVersion, validate as uuidValidate, v4 as uuidv4, NIL as NIL_UUID } from 'uuid';
import TextArea from 'antd/lib/input/TextArea';

const { DefaultDatabase, RenderType } = require("util/databaseApi");
const { confirm } = Modal;
const MAX_WIDTH = (8.5 - 2) * 96;
const MAX_HEIGHT = (11 - 2) * 96;
/**@type {import('html2canvas').default} */
const html2canvas = window.html2canvas;

export const HelpType = {
  'Keyboard shortcuts': 'Keyboard shortcuts',
  "Neo4j Report Examples": "Neo4j Report Examples",
}

export const ReadWriteOptions = ["hide codes", "normal", "display codes"]
export const ReadWriteOptionMapping = {
  hide: ReadWriteOptions[0],
  normal: ReadWriteOptions[1],
  display: ReadWriteOptions[2]
}
/**
 * 
 * @param {string} realValue 
 */
export const commentEncoder = function (realValue) {
  realValue = `${realValue.trim()}`;
  let tmp;
  let map = {};
  let matches;
  let first = null;
  let end = null;
  matches = realValue.matchAll(/^\/\/.*\n/g);
  if (!(tmp = matches.next()).done) {
    first = tmp.value[0];
    realValue = realValue.replace(tmp.value[0], "")
  }
  if (!first) {
    matches = realValue.matchAll(/^\/\*.*\*\//gs);
    if (!(tmp = matches.next()).done) {
      first = `${tmp.value[0]}\n`;
      realValue = realValue.replace(tmp.value[0], "")
    }
  }
  matches = realValue.matchAll(/\/\/.*$/g);
  if (!(tmp = matches.next()).done) {
    end = tmp.value[0];
    realValue = realValue.replace(tmp.value[0], "")
  }
  if (!end) {
    matches = realValue.matchAll(/\/\*.*\*\/$/gs);
    if (!(tmp = matches.next()).done) {
      end = `${tmp.value[0]}`;
      realValue = realValue.replace(tmp.value[0], "")
    }
  }
  matches = realValue.matchAll(/\/\/.*\n/g);
  for (let i = 0; i < 1000; i++) {
    if ((tmp = matches.next()).done) {
      break;
    }
    let id = uuidv4();
    map[id] = { multi: false, value: tmp.value[0] }
    realValue = realValue.replace(tmp.value[0], `("_${id}");`)
  }
  matches = realValue.matchAll(/\/\*.*\*\//gs)
  for (let i = 0; i < 1000; i++) {
    if ((tmp = matches.next()).done) {
      break;
    }
    let id = uuidv4();
    map[id] = { multi: true, value: tmp.value[0] }
    realValue = realValue.replace(tmp.value[0], `("_${id}");`)
  }
  return { map, realValue, first, end };
}

export const commentDecoder = function (map, formatValue, first, end) {
  if (first) {
    formatValue = first + formatValue
  }
  _.each(map, (v, k) => {
    formatValue = formatValue.replace(new RegExp(`\\("_${k}"\\);?\\n?`, "g"), v.value);
  })
  if (end) {
    formatValue = formatValue + end
  }
  return formatValue.trim();
}


/**
 * check is uuid?
 * @param {string} uuid 
 */
export const uuidValidateV4 = function (uuid) {
  return uuidValidate(uuid) && uuidVersion(uuid) === 4;
}

export const CodeTools = [];

export const Themes = ['default', 'azul', 'bee-inspired', 'cool', 'macarons', 'macarons2',
  'tech-blue', 'infographic', "roma",
  "vintage", "shine", "dark"]

export const SelectProps = {
  listHeight: 32 * 6,
  getPopupContainer: () =>
    document.querySelector(".chart-setting-drawer .ant-drawer-body"),

}

export const file_actions_keys = {
  "Import Files": {
    title: "Import Files",
    keys: ["Ctrl", "Alt", "I"]
  },
  "New File": {
    title: "New File",
    keys: ["Ctrl", "Alt", "N"]
  },
  "Duplicate Selected File/Folder": {
    title: "Duplicate Selected File/Folder",
    keys: ["Ctrl", "Alt", "C"]
  },
  "Remove Selected File/Folder": {
    title: "Remove Selected File/Folder",
    keys: ["Delete"]
  },
  "Rename Selected File": {
    title: "Rename Selected File",
    keys: ["F2"]
  },
  "Download Current File": {
    title: "Download Current File",
    keys: ["Ctrl", "Shift", "E"]
  },
  "Download All Files": {
    title: "Download All Files",
    keys: ["Ctrl", "Alt", "A"]
  },
  "Upload Current File": {
    title: "Upload Current File",
    keys: ["Ctrl", "Alt", "S"]
  },
};
export const code_editor_keys = {
  "Select all": {
    title: "Select all",
    keys: ["Ctrl", "A"],
    iosKeys: ["⌘", "A"],
  },
  "Undo last change": {
    title: "Undo last change",
    keys: ["Ctrl", "Z"],
    iosKeys: ["⌘", "Z"],
  },
  "Redo the last undone change": {
    title: "Redo the last undone change",
    keys: ["Ctrl", "Y"],
    iosKeys: ["⌘", "Y"],
  },
  "Select all": {
    title: "Select all",
    keys: ["Ctrl", "A"],
    iosKeys: ["⌘", "A"],
  },
  "Show Editor LineNumbers": {
    title: "Show Editor LineNumbers",
    keys: ["Ctrl", "L"],
    iosKeys: ["⌘", "L"],
  },
  "Delete Current Line": {
    title: "Delete Current Line",
    keys: ["Ctrl", "D"],
    iosKeys: ["⌘", "D"],
  },
  "Comment Lines": {
    title: "Comment Lines",
    keys: ["Ctrl", "/"],
    iosKeys: ["⌘", "/"],
  },
  "Format Code": {
    title: "Format Code",
    keys: ["Alt", "F"],
  },
  "Fold Code": {
    title: "Fold Code",
    keys: ["Ctrl", "Alt", "L"],
  },
  "Jump to defining cell": {
    title: "Jump to defining cell",
    keys: ["Ctrl", "Alt", "J"],
  },
  "Full Screen": {
    title: "Full Screen",
    keys: ["F10"],
  },
  "Auto-indent": {
    title: "Auto-indent the current line or selection",
    keys: ["Shift", "Tab"],
  },
  "Auto-complete": {
    title: "Auto-complete",
    keys: ["Alt", "/"],
  },
  "Find and replace": {
    title: "Find and replace",
    keys: ["Shift", "Ctrl", "F"],
  },
}
export const cell_shortcuts_keys = {
  "Run Current Cell": {
    title: "Run Current Cell",
    keys: ["Shift", "Enter"]
  },
  "Force Run Current Cell": {
    title: "Force Run Current Cell",
    keys: ["Ctrl", "Enter"]
  },
  "Delete Current Cell": {
    title: "Delete Current Cell",
    keys: ["Ctrl", "Alt", "D"]
  },
  "Insert cell above": {
    title: "Insert cell above",
    keys: ["Alt", "ArrowUp"]
  },
  "Insert cell below": {
    title: "Insert cell below",
    keys: ["Alt", "ArrowDown"]
  },
  "Move Cell Up": {
    title: "Move Cell Up",
    keys: ["Ctrl", "Alt", "ArrowUp"]
  },
  "Move Cell Down": {
    title: "Move Cell Down",
    keys: ["Ctrl", "Alt", "ArrowDown"]
  },
  "Go Back Editing Cell": {
    title: "Go Previos Editing Cell",
    keys: ["Ctrl", "Alt", "ArrowLeft"]
  },
  "Go Forward Editing Cell": {
    title: "Go Forward Editing Cell",
    keys: ["Ctrl", "Alt", "ArrowRight"]
  },
  "Focus Previos Editable Cell": {
    title: "Focus Previos Editable Cell",
    keys: ["Ctrl", "ArrowUp"],
    osKeys: ["⌘", "ArrowUp"]
  },
  "Focus Next Editable Cell": {
    title: "Focus Next Editable Cell",
    keys: ["Ctrl", "ArrowDown"],
    osKeys: ["⌘", "ArrowDown"]
  },
  "Open Cell Toolbar": {
    title: "Open Cell Toolbar",
    keys: ["Ctrl", "Q"]
  },
  "Pin/Unpin Cell": {
    title: "Pin/Unpin Cell",
    keys: ["Alt", "P"]
  },
  "Clear Selection": {
    title: "Clear Selection",
    keys: ["Escape"]
  },
  "New Sub Block": {
    title: "New Sub Block",
    keys: ["Ctrl", "Shift", "L"]
  },
}
export const window_shortcuts_keys = {
  "Search GraphXR API Docs": {
    title: "Search GraphXR API Docs",
    keys: ["Ctrl", "Alt", "K"]
  },
  "Open File Explorer": {
    title: "Open File Explorer",
    keys: ["Ctrl", "Alt", "O"]
  },
  "Open Explore Pane": {
    title: "Open Explore Pane",
    keys: ["Ctrl", "Alt", "E"]
  },
  "Open Settings Pane": {
    title: "Open Settings Pane",
    keys: ["Ctrl", "Alt", "X"]
  },
  "Full Screen": {
    title: "Full Screen",
    keys: ["Ctrl", "Alt", "Enter"]
  },
  "Escape Full Screen": {
    title: "Escape Full Screen",
    keys: ["Escape"]
  },
  "Drawer Full Screen": {
    title: "Drawer Full Screen",
    keys: ["Ctrl", "F10"]
  },
  "Focus Selected Chart": {
    title: "Focus Selected Chart",
    keys: ["Ctrl", "Alt", "J"]
  },
  "Help?": {
    title: "Help?",
    keys: ["Ctrl", "Alt", "H"]
  },
  "Open Data Pane": {
    title: "Open Data Pane",
    keys: ["Ctrl", "Alt", "U"]
  },
  "System PDF Print": {
    title: "System PDF Print",
    keys: ["Ctrl", "P"]
  },
  "PDF Print": {
    title: "PDF Print",
    keys: ["Ctrl", "Alt", "P"]
  },
}

/**
 * 
 * @param {{title:string,keys:[],iosKeys:[]}} value 
 */
export const osKeys = function (value) {
  return osfunction() === OSType.MacOS ? (value.iosKeys || value.keys) : value.keys
}

/**
 * 
 * @param {{title:string,keys:[],iosKeys:[]}} value 
 */
export const linkKeys = function (value) {
  let keys = _.cloneDeep(osKeys(value));
  _.each(keys, (key, index) => {
    if (~key.indexOf("⌘")) {
      keys[index] = "Cmd";
    } else if (~key.indexOf("Arrow")) {
      keys[index] = keys[index].replace("Arrow", "")
    }
  })
  return keys.join("-")
}
export const confirm_key = { title: "Confirm", keys: ["Enter"] }

/**
 * 
 * @param {{title:string,keys:[]}} cur 
 */
export const ShortcutTitle = function (cur, title) {
  return cur ? `${title || cur.title} (${cur.keys.join("+")})` : "undefined";
}
/**
 * 
 * @param {KeyboardEvent} e 
 * @param {"a-z"} key 
 * @param {{keys:[""]}} cur 
 */
export const IsKey = function (e, key = "", cur) {
  if (~cur.keys.indexOf("Ctrl") && !e.ctrlKey) {
    return false;
  }
  if (!~cur.keys.indexOf("Ctrl") && e.ctrlKey) {
    return false;
  }
  if (~cur.keys.indexOf("Meta") && !e.metaKey) {
    return false;
  }
  if (!~cur.keys.indexOf("Meta") && e.metaKey) {
    return false;
  }
  if (~cur.keys.indexOf("Shift") && !e.shiftKey) {
    return false;
  }
  if (!~cur.keys.indexOf("Shift") && e.shiftKey) {
    return false;
  }
  if (~cur.keys.indexOf("Alt") && !e.altKey) {
    return false;
  }
  if (!~cur.keys.indexOf("Alt") && e.altKey) {
    return false;
  }
  let lastKey = cur.keys[cur.keys.length - 1];
  if (~["Ctrl", "Shift", "Alt"].indexOf(lastKey)) {
    showToast("key config error, can not end with control key!", "error");
    return false;
  }
  if (lastKey.toUpperCase() !== key.toUpperCase()) {
    return false;
  }
  return true;
}
/**
 * 
 * @param {KeyboardEvent} e 
 */
export function macMetaOrCtrl(e) {
  return osfunction() === OSType.MacOS ? e.metaKey : e.ctrlKey
}
export const SelectPropsDef = {
  listHeight: 32 * 6,
}
export const CategoryFromType = {
  files: "files",
  main: "main",
  shareData: "shareData",
  database: "database",
  neo4j: "neo4j",
  mongodb: "mongodb",
  dynamodb: "dynamodb",
  client: "client",
}
export const COMMON_RENDER_TYPES = [RenderType.Code, RenderType.Markdown, RenderType.Python, RenderType.IFrame];
export const NEO_FOOTER_RENDERS = [RenderType.SankeyChart, RenderType.Map, RenderType.Graph, RenderType.ChoroplethMap, RenderType.CirclePacking, RenderType.Radar];
const DATAS_RENDER_TYPES = [RenderType.RawJSON, RenderType.Table, RenderType.Chart];
export const RenderTypesOfCategory = {
  files: _.concat(COMMON_RENDER_TYPES, DATAS_RENDER_TYPES),
  main: _.concat(COMMON_RENDER_TYPES, DATAS_RENDER_TYPES),
  shareData: _.concat(COMMON_RENDER_TYPES, DATAS_RENDER_TYPES),
  database: _.concat(COMMON_RENDER_TYPES, DATAS_RENDER_TYPES),
  neo4j: _.keys(RenderType),
  mongodb: _.concat(COMMON_RENDER_TYPES, DATAS_RENDER_TYPES),
  dynamodb: _.concat(COMMON_RENDER_TYPES, DATAS_RENDER_TYPES),
  none: COMMON_RENDER_TYPES,
}

export const transforMX = (mx) => {
  mx = mx.trim();
  if (!isNaN(mx)) {
    return parseFloat(mx);
  }

  if (~['dataMin', 'dataMax'].indexOf(mx)) {
    return mx;
  }
  if (~mx.indexOf("return")) {
    return new Function(['value'], mx);
  }
  return new Function(['value'], `return eval(\`${mx}\`)`);
}

export const transforFormatter = (mx) => {
  mx = mx.trim();
  if (!isNaN(mx)) {
    return parseFloat(mx);
  }
  if (~mx.indexOf("return")) {
    return new Function(['value', 'index'], mx);
  }
  return new Function(['value', 'index'], `return eval(\`${mx}\`)`);
}

export const sortObjectByKey = o => _.sortBy(Object.keys(o), n => n.toLowerCase()).reduce((r, k) => (r[k] = o[k], r), {});

const TypeTitle = {
  "success": "Success",
  "warning": "Warning",
  "error": "Error",
  "info": "Information"
}

const TypeLogFn = {
  "success": console.log,
  "warning": console.warn,
  "error": console.error,
  "info": console.info,
}

export const NoticeOptions = ["success", "info", "warning", "error"]
/**
 * 
 * @param {*} message 
 * @param {'success' | 'info' | 'error' | 'warning'} type  
 * @param {*} duration 
 * @param {*} config 
 */
export function showToast(content, type = "success", duration = 3, config = {}) {
  const typeNoticeLevel = NoticeOptions.indexOf(type)
  const settingsNoticeLevel = NoticeOptions.indexOf(window.editor.getSettings().getNotice_mode())
  if (typeNoticeLevel < settingsNoticeLevel) {
    const logContent = TypeLogFn[type];
    logContent(`${type.toUpperCase()} ${content}`)
    return;
  }

  AntDToast[type](_.assign({
    content,
    duration,
    style: {
      zIndex: 1005
    },
  }, config));
}
export function showMessage(content, type = "success", duration = 3) {
  AntDToast[type]({
    content,
    duration,
    style: {
      zIndex: 1005
    },
  });
}
/**
 * 
 * @param {ReactNode} content 
 * @param {function} successFunc 
 * @param {function} failFunc 
 */
export const copyContent = function (content = "", successFunc = () => { showMessage("copy success!", "success") }, failFunc) {
  if (!failFunc) {
    failFunc = () => {
      let key = uuidv4();
      showToast(
        <Space direction="vertical" size="small">
          <div className="text-danger">Copy failed! Please copy it manually (Ctrl + C).</div>
          <TextArea cols={50} rows={10} value={content} autoFocus onFocus={(e) => {
            e.target.setSelectionRange(0, content.length);
          }}></TextArea>
          <div className="d-flex justify-content-end">
            <Button type="primary" size="small" onClick={() => AntDToast.destroy(key)}>
              Close
            </Button>
          </div>
        </Space>
        , "warning", 30, { key });
    }
  }
  let copyFunc = () => {
    navigator.clipboard.writeText(content).then(function () {
      successFunc();
    }, function () {
      failFunc();
    });
  }
  if (BrowserChecks.isFirefox) {
    copyFunc();
  } else {
    navigator.permissions.query({ name: "clipboard-write" }).then(result => {
      if (result.state === "granted" || result.state === "prompt") {
        copyFunc();
      } else {
        failFunc();
        // showToast("no right to write clipboard!", "error")
      }
    }).catch((err) => {
      failFunc();
      console.error(err);
    });
  }
}

export const hasCopyRight = async function () {
  if (typeof InstallTrigger !== 'undefined') {
    return !!(navigator.clipboard && navigator.clipboard.writeText);
  } else {
    return navigator.permissions.query({ name: "clipboard-write" }).then(result => {
      if (result.state === "granted" || result.state === "prompt") {
        return true;
      } else {
        return false;
      }
    }).catch((err) => {
      console.error(err);
    });
  }
}

/**
 * 
 * @param {React.ReactNode} message 
 * @param {*} okHandler 
 * @param {*} cancelHandler 
 * @param {*} description 
 * @returns {Promise<boolean>}
 */
export const showConfirm = async function (message,
  okHandler = () => { }, cancelHandler = () => { }, props = {
    cancelText: "Cancel",
    okText: "OK",
    description: '',
  }) {
  if (typeof okHandler !== 'function') { props = okHandler; okHandler = () => { } };
  const { okText, cancelText, description, ...propsa } = props;
  return await new Promise((resolve, reject) => {
    window._currentModal = confirm({
      title: <div className="confirm-title">{message}</div>,
      icon: <ExclamationCircleOutlined />,
      content: description || '',
      cancelText: cancelText || "Cancel",
      okText: okText || "OK",
      onOk: () => { okHandler(); resolve(true) },
      onCancel: () => { cancelHandler(); resolve(false) },
      zIndex: 1005,
      ...propsa
    });
    window._currentModal.resolveFunc = function (result) {
      resolve(result);
    }
  });
}
export const SETTINGS_NAME = "settings.json";
export const MSG = {
  NO_FILE_IMPORTED: "No file imported!",
  FILE_NAME_REPEATED: "File name repeated!",
  FOLDER_NAME_REPEATED: "Folder name repeated!",
  FILE_NAME_CAN_NOT_EMPLTY: "File name can not be empty!",
  FILE_NAME_THE_SAME: "A file name is the same as it!",
  FILE_NAME_FILE_TYPE_NOT_SUPPORT: "Not support the file type!",
  FOLDER_NAME_THE_SAME: "A folder name is the same as it!",
  FILE_NAME_BREAK_THE_RULE: 'File name should only containg letters and numbers, ' +
    'please rename file and upload.',
  FOLDER_NAME_BREAK_THE_RULE: 'Folder name should only containg letters and numbers, ' +
    'please rename folder and upload.',
  NOT_SELETE_A_FILE: 'not select a file',
  NO_FILES_REMOVED: 'no files can removed',
  CAN_NOT_REMOVE_SETTINGS: `Can not remove the ${SETTINGS_NAME} file!`,
  CAN_NOT_MODIFY_SETTINGS: `Can not modify the ${SETTINGS_NAME} file!`,
  CONFIRM_REMOVE_SETTINGS: <div>Are you sure remove the <span className="text-primary">{SETTINGS_NAME}</span> file?</div>,
}
export const CodeMode = {
  javascript2: "javascript2",
  markdown: "markdown",
  htmlmixed: "htmlmixed",
  javascript: "javascript",
  sql: "sql",
  tex: "tex",
  jsx: "jsx",
  python: "python",
}
const CodeModeConfigs = {
  javascript2: { icon: "iconfont icon-code", svg: Js2Svg },
  markdown: { icon: "fab fa-markdown", svg: MarkdownSvg },
  htmlmixed: { icon: "fas fa-code", svg: HtmlSvg },
  javascript: { icon: "fab fa-node-js", svg: JsSvg },
  sql: { icon: "fas fa-database", svg: SqlSvg },
  tex: { icon: "fas fa-subscript", svg: TexSvg },
  jsx: { icon: "fab fa-react", svg: JsxSvg },
  python: { icon: "fab fa-python", svg: PySvg },
}
const CodeModeDefaultConfig = { icon: "fas fa-question-circle", svg: QuesSvg }
export function getSchema(table) {
  return (typeof table === 'string' || !table) ? null : (/**pg */ table.schema ||/**ms */ table.TABLE_SCHEMA ||/**snowflake */table.schema_name ||/**my */ null);
}
export function getTableName(table) {
  return (typeof table === 'string' || !table) ? table : (/**pg */ table.name ||/**ms */ table.TABLE_NAME ||/**server proxy */ _.values(table)[0]);
}
export function getColumnName(column) {
  return /**pg proxy  */column.column_name ||/**ms,snowflake */ column.COLUMN_NAME ||/**mysql proxy */column.name ||/**server proxy */ column.Field;
}
export function getColumnType(column) {
  return /**pg */ column.data_type || column.type || column.Type || /**snowflake */column.DATA_TYPE;
}
export function getColumnKeyType(column) {
  return /**dynamo */ column.key_type ? `(${column.key_type})` : "";
}
export function isColumnNoNullable(column) {
  return /**pg */ column.is_nullable === "NO" || false === column.nollable || column.Null === "NO" || /**snowflake */column.IS_NULLABLE === "NO";
}
import { transform } from '@babel/standalone'
import { HtmlSvg, Js2Svg, JsSvg, JsxSvg, MarkdownSvg, PySvg, QuesSvg, SqlSvg, TexSvg } from 'block/code/Icon';
export function getCodeModeValue(value, codeMode, db, sqlType, tableName, dbTable, selectedTable,
  filters = [{}],
  selectedColumns = [],
  sorts = [{}],
  slice = { to: 100, from: 0 }) {
  if (codeMode === CodeMode.markdown) {
    return `md\`${(value || "").replaceAll("`", "\\`")}\``
  } else if (codeMode === CodeMode.python) {
    return `py\`${(value || "").replaceAll("`", "\\`")}\``
  } else if (codeMode === CodeMode.htmlmixed) {
    return `html\`${(value || "").replaceAll("`", "\\`")}\``
  } else if (codeMode === CodeMode.tex) {
    return `tex\`${(value || "").replaceAll("`", "\\`")}\``
  } else if (codeMode === CodeMode.sql) {
    if (dbTable) {
      let operations = getOperations(db, sqlType, selectedTable, filters, selectedColumns, sorts, slice);
      if (!operations) {
        return `${tableName ? `${tableName}=` : ""}[]`
      }
      let sql = `${tableName ? `${tableName}=` : ""}__query(${db}, ${JSON.stringify(operations)}, invalidation)`
      // console.log(sql);
      return sql;
    } else {
      if (!db) {
        return `${tableName ? `${tableName}=` : ""}[]`
      }
      let sql = `${tableName ? `${tableName}=` : ""}__query.sql(${db}, invalidation)\`${(value || "").replaceAll("`", "\\`")}\``
      // console.log(sql);
      return sql;
    }
  } else if (codeMode === CodeMode.jsx) {
    try { return transform(value, { presets: ['react'] }).code; } catch (e) { console.error(e); return e; };
  } else {
    return value;
  }
}

export function getOperations(db, sqlType, selectedTable,
  filters = [{}],
  selectedColumns = [],
  sorts = [{}],
  slice = { to: 100, from: 0 }) {
  if (!db || !selectedTable) {
    return null;
  }
  const [schema, table] = selectedTable && ~selectedTable.indexOf(".") ? selectedTable.split(".") : [null, selectedTable]
  let operations = {
    sqlType: sqlType,
    select: { columns: selectedColumns },
    from: { table: { schema, table }, mimeType: null },
    filter: _.reduce(filters, (prev, filter) => {
      if (!_.isEmpty(filter)) {
        let { columnName, association, value } = filter;
        let operands = [{ type: "column", value: columnName }]
        if (~['nn', 'n'].indexOf(association)) {
        } else if (~['in', 'nin'].indexOf(association)) {
          operands.push(..._.map(value.trim().split(","), (v) => {
            return { type: "resolved", value: v };
          }))
        } else {
          operands.push({ type: "resolved", value: value })
        }
        prev.push({ type: association, operands: operands })
      }
      return prev;
    }, []),
    sort: _.reduce(sorts, (prev, sort) => {
      if (!_.isEmpty(sort)) {
        prev.push(sort);
      }
      return prev;
    }, []),
    slice: {
      from: 'start' === slice.from ? null : slice.from,
      to: 'end' === slice.to ? null : slice.to,
    }
  }
  return operations;
}

export function minusCodeModeTag(codeMode, db, tableName, dbTable) {
  if (codeMode === CodeMode.markdown) {
    return "md\`".length
  } else if (codeMode === CodeMode.python) {
    return "py\`".length
  } else if (codeMode === CodeMode.htmlmixed) {
    return "html\`".length
  } else if (codeMode === CodeMode.tex) {
    return "tex\`".length
  } else if (codeMode === CodeMode.sql && !dbTable) {
    return `${tableName ? `${tableName}=` : ""}__query.sql(${db}, invalidation)\``.length
  } else {
    return "".length;
  }

}
export function getCodeModeTag(codeMode) {
  if (codeMode === CodeMode.markdown) {
    return "md"
  } else if (codeMode === CodeMode.python) {
    return "py"
  } else if (codeMode === CodeMode.htmlmixed) {
    return "html"
  } else if (codeMode === CodeMode.tex) {
    return "tex"
  } else {
    return "";
  }
}
export function getCodeModePlaceholder(codeMode) {
  if (codeMode) {
    return `Type ${codeMode.firstUpperCase()}, then Shift-Enter.`;
  } else {
    return "Type Text, then Shift-Enter.";
  }
}
export function getCodeModeIcon(codeMode) {
  let config = CodeModeConfigs[codeMode] || CodeModeDefaultConfig;
  return config.icon;
}
export function getCodeModeSvg(codeMode) {
  let config = CodeModeConfigs[codeMode] || CodeModeDefaultConfig;
  return config.svg;
}
export const EmptyResults = [{ Results: "No results" }];
export const FailedResults = [{ Results: "Connection Failed!", _ERROR: "true" }];
export const isFailedResults = function (nData) {
  return nData && nData.length === FailedResults.length && undefined !== nData[0].Results && ("true" === nData[0]._ERROR || true === nData[0]._ERROR);
}
export const assembleErrMessage = function (error) {
  let ret = _.cloneDeep(FailedResults);
  ret[0].Results = error && typeof error === 'object' ? JSON.stringify(error) : error;
  return ret;
}
export const assembleMessage = function (results) {
  return { data: results instanceof Array ? results : [results] };
}
export const ModalType = {
  None: "None",
  OpenChanges: "Open Changes",
  DisplaySource: "Display Source",
  SetPreviewImage: "Set Preview Image",
  VersionControl: "Version Control",
  Publish: "Publish Settings",
  ReplaceFile: "Replace File",
  DuplicateFile: "Duplicate",
  SyncFile: "Proxy Array",
  Fork: "Fork",
  ImportFiles: "Import Files",
  Decompression: "Decompression",
  NewDatabase: "New Database",
  NewSecret: "New Secret",
  ChartFullScreen: "Chart Full Screen",
  CopyContent: "Copy Content",
  NeoGraphItemInspect: "Neo Graph Item Inspect",
  NewNotebook: "New Notebook",
  NeoDashDashboardGallery: "NeoDash Dashboard Gallery",
}
export const ContentType = {
  Link: "link",
  File: "file",
  SharedFile: "SharedFile"
}
export const AnchorType = {
  Variant: "Variant",
  Common: "Common",
  Error: "Error",
  Header: "Header",
}
export const DatabaseType = {
  Mysql: "Mysql",
  PostgreSQL: "PostgreSQL",
  MSSQL: "MSSQL",
  Snowflake: "Snowflake",
  MongoDB: "MongoDB",
  Neo4j: "Neo4j",
  BigQuery: "BigQuery",
  DynamoDB: "DynamoDB",
}
export const ReDatabaseTypes = [
  DatabaseType.Mysql, DatabaseType.PostgreSQL, DatabaseType.MSSQL, DatabaseType.Snowflake, DatabaseType.BigQuery, DatabaseType.DynamoDB
]
export const ReDbDialets = ['sqlite', 'duckdb', ...ReDatabaseTypes]
export const RenderTypesOfDialect = function (dialect) {
  if (~ReDbDialets.indexOf(dialect)) {
    return RenderTypesOfCategory.database;
  } else if (dialect === DatabaseType.MongoDB) {
    return RenderTypesOfCategory.mongodb;
  } else if (dialect === DatabaseType.Neo4j) {
    return RenderTypesOfCategory.neo4j;
  } else if (dialect === DatabaseType.DynamoDB) {
    return RenderTypesOfCategory.dynamodb;
  }
}
export const ShortDatabaseType = {
  Mysql: "mysql",
  PostgreSQL: "postgres",
  MSSQL: "mssql",
  Snowflake: "snowflake",
  MongoDB: "mongodb",
  Neo4j: "neo4j",
  BigQuery: "bigquery",
  DynamoDB: "dynamodb",
}
export const ConnectionHostedByType = {
  "Self-hosted proxy": "Self-hosted proxy",
  Grove: "Grove",
  "Client": "Client",
}
export const DatabaseTypeDesc = {
  Mysql: "Mysql",
  PostgreSQL: "PostgreSQL",
  MSSQL: "MSSQL",
  Snowflake: "Snowflake",
  MongoDB: "MongoDB",
  Neo4j: "Neo4j",
  BigQuery: "BigQuery",
  DynamoDB: "DynamoDB",
}
/**
 * @type {Object.<string,Object>}
 */
export const DatabaseFields = {};
DatabaseFields[DatabaseType.BigQuery] = {
  "Self-hosted proxy": {
    proxyHost: "127.0.0.1",
    port: 2901,
    location: ""
  },
  OptionFields: []
}
DatabaseFields[DatabaseType.Mysql] = {
  "Self-hosted proxy": {
    proxyHost: "127.0.0.1",
    port: 2901,
    location: ""
  },
  Grove: {
    host: "",
    port: 3306,
    user: "",
    password: "",
    database: "",
  },
  OptionFields: ["password", "location"],
  HideFields: ["password"],
}
DatabaseFields[DatabaseType.PostgreSQL] = {
  "Self-hosted proxy": {
    proxyHost: "127.0.0.1",
    port: 2901,
    location: ""
  },
  Grove: {
    host: "",
    port: 5432,
    user: "",
    password: "",
    database: "",
  },
  OptionFields: ["password", "location"],
  HideFields: ["password"],
}
DatabaseFields[DatabaseType.MSSQL] = {
  "Self-hosted proxy": {
    proxyHost: "127.0.0.1",
    port: 2901,
    location: ""
  },
  Grove: {
    host: "",
    port: 1433,
    user: "",
    password: "",
    database: "",
    encrypt: false,
  },
  OptionFields: ["password", "location"],
  HideFields: ["password"],
}
DatabaseFields[DatabaseType.MongoDB] = {
  "Self-hosted proxy": {
    proxyHost: "127.0.0.1",
    port: 2901,
    location: ""
  },
  Grove: {
    host: "localhost",
    port: 27017,
    user: "",
    password: "",
    database: "",
    encrypt: false,
  },
  OptionFields: ["user", "port", "password", "location"],
  HideFields: ["password"],
}
DatabaseFields[DatabaseType.DynamoDB] = {
  "Self-hosted proxy": {
    proxyHost: "127.0.0.1",
    port: 2901,
    location: ""
  },
  Client: {
    file: "",
    accessKeyId: "",
    secretAccessKey: "",
    region: "",
    encrypt: false,
  },
  OptionFields: [],
  InvisibleFields: [/* "accessKeyId", "secretAccessKey" */],
  HideFields: ["secretAccessKey"],
}
DatabaseFields[DatabaseType.Neo4j] = {
  "Self-hosted proxy": {
    proxyHost: "127.0.0.1",
    port: 2901,
    location: ""
  },
  Grove: {
    protocal: "neo4j",
    host: "localhost",
    port: 7687,
    user: "neo4j",
    password: "",
    database: "neo4j",
  },
  Client: {
    protocal: "neo4j",
    host: "localhost",
    port: 7687,
    user: "neo4j",
    password: "",
    database: "neo4j",
  },
  OptionFields: ["user", "port", "database", "password", "location"],
  template: "{protocal}://{host}:{port}"
}
DatabaseFields[DatabaseType.Snowflake] = {
  "Self-hosted proxy": {
    proxyHost: "127.0.0.1",
    port: 2901,
    location: ""
  },
  Grove: {
    account: "",
    user: "",
    password: "",
    database: "",
    schema: "",
    warehouse: "",
    role: "",
  },
  OptionFields: ["password", "location"]
}
/**
* @param {DefaultDatabase} dbInfo 
*/
export const getTestSql = function (dbInfo) {
  let sql;
  switch (dbInfo.type) {
    case DatabaseType.BigQuery:
      sql = "getDatasets"
      break;
    case DatabaseType.Mysql:
      sql = "show tables;"
      break;
    case DatabaseType.PostgreSQL:
      sql = `SELECT NULLIF(table_schema, current_schema()) AS schema, table_name AS name
FROM information_schema.tables
WHERE table_schema NOT IN ('pg_catalog', 'information_schema')
ORDER BY 1, 2`
      break;
    case DatabaseType.MSSQL:
      sql = `SELECT * FROM INFORMATION_SCHEMA.TABLES;`;
      break;
    case DatabaseType.Neo4j:
      sql = `MATCH (n) RETURN count(n)`;
      break;
    case DatabaseType.MongoDB:
      sql = `{}`;
      break;
    case DatabaseType.DynamoDB:
      sql = `{}`;
      break;
    default:
      sql = "show tables;"
      break;
  }
  return sql;
}
export const DynamoOperationType = {
  ListTables: 0, DescribeTable: 1, Query: 2, ExecuteStatement: 3, BatchExecuteStatement: 4, Scan: 5,
}
export const MongoOperationType = {
  listCollectionNames: 0, runCommand: 1, find: 2, listFields: 3
}
export const columnFunc = function (c, sqlType, escaper = (i) => i) {
  if (sqlType === DatabaseType.MSSQL) {
    return `t.[${escaper(c)}]`
  } else if (sqlType === DatabaseType.DynamoDB) {
    return `"${escaper(c)}"`
  } else {
    return `t.${escaper(c)}`
  }
}

export function formatTable(table, sqlType, escaper = (i) => i) {
  if (typeof table === "object") {
    let from = "";
    if (table.database != null) from += escaper(table.database) + ".";
    if (table.schema != null) from += escaper(table.schema) + ".";
    from += (sqlType === DatabaseType.DynamoDB ? `"${escaper(table.table)}"` : `${escaper(table.table)} t`);
    return from;
  }
  return sqlType === DatabaseType.DynamoDB ? `"${escaper(table)}"` : `${escaper(table)} t`;
}

/**
 * switch style of ele
 * @param {*} ele 
 * @param {*} err if error happened
 */
export function swStyle(ele, err) {
  if (!ele) {
    console.log(ele);
    return;
  }
  ele.style.borderLeft = `8px solid ${err ? "var(--error-color)" : "transparent"}`;
  setTimeout(() => {
    ele.style.borderLeft = `8px solid var(--warning-color)`;
    setTimeout(() => {
      ele.style.borderLeft = `8px solid ${err ? "var(--error-color)" : "transparent"}`;
    }, 200)
  }, 200);
}

export function saveLink(link, title) {
  let save_link = document.createElementNS('http://www.w3.org/1999/xhtml', 'a');
  document.body.appendChild(save_link);
  save_link.href = link;
  if (title) {
    save_link.download = title;
  }
  let event = document.createEvent('MouseEvents');
  event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
  save_link.dispatchEvent(event);
  URL.revokeObjectURL(save_link.href);
  document.body.removeChild(save_link);
}
/**
 * 
 * @param {HTMLDivElement} icon 
 */
async function icon_to_png(icon) {
  let canvas = await html2canvas(icon, { backgroundColor: getComputedStyle(document.body).getPropertyValue('--background-color') });
  let imgsrc = canvas.toDataURL(`image/png`, 1.0);
  return { width: canvas.width, height: canvas.height, imgsrc }
}
/**
 * 
 * @param {SVGSVGElement} svg 
 */
function svg_to_png(svg) {
  return new Promise((resolve, reject) => {
    if (typeof window.XMLSerializer != "undefined") {
      var svgData = (new XMLSerializer()).serializeToString(svg);
    } else if (typeof svg.xml != "undefined") {
      var svgData = svg.xml;
    }
    var img = document.createElement("img");
    img.setAttribute("src", "data:image/svg+xml;base64," + btoa(unescape(encodeURIComponent(svgData))));
    img.onload = function () {
      var svgSize = svg.getBoundingClientRect();
      var canvas = document.createElement("canvas");
      canvas.width = svgSize.width;
      canvas.height = svgSize.height;
      var ctx = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0);
      var imgsrc = canvas.toDataURL("image/png");
      resolve({ width: canvas.width, height: canvas.height, imgsrc });
    };
  })
}
/**
 * 
 * @param {HTMLCanvasElement} canvas 
 */
export function canvasImage(canvas) {
  let imgData = canvas.toDataURL("png", 1.0);
  let width, height;
  if (canvas.width > MAX_WIDTH) {
    width = MAX_WIDTH;
    height = MAX_WIDTH / canvas.width * canvas.height;
  } else {
    width = canvas.width;
    height = canvas.height;
  }
  if (height > MAX_HEIGHT) {
    let oldHeight = height;
    height = MAX_HEIGHT;
    width = MAX_HEIGHT / oldHeight * width;
  }
  return `<img src="${imgData}"  width="${width}" height="${height}">`;
}
/**
 * 
 * @param {HTMLElement} element 
 */
export async function extractCanvas(element) {
  let html = element.innerHTML;
  let iconList = element.querySelectorAll(".icon");
  if (iconList.length) {
    for (let index = 0; index < iconList.length; index++) {
      let value = iconList[index]
      if (value.closest(".hide") !== null) {
        continue;
      }
      var iconSize = value.getBoundingClientRect();
      if (!iconSize.width || !iconSize.height) {
        continue;
      }
      let png = await icon_to_png(value);
      let width, height;
      if (png.width > MAX_WIDTH) {
        width = MAX_WIDTH;
        height = MAX_WIDTH / png.width * png.height;
      } else {
        width = png.width;
        height = png.height;
      }
      if (!width || !height) {
        continue;
      }
      if (height > MAX_HEIGHT) {
        let oldHeight = height;
        height = MAX_HEIGHT;
        width = MAX_HEIGHT / oldHeight * width;
      }
      html = html.replace(value.outerHTML, `<img src="${png.imgsrc}" width="${width}" height="${height}">`);
    }
  }
  let svgList = element.querySelectorAll("svg");
  if (svgList.length) {
    for (let index = 0; index < svgList.length; index++) {
      let value = svgList[index]
      if (value.closest(".hide") !== null) {
        continue;
      }
      var iconSize = value.getBoundingClientRect();
      if (!iconSize.width || !iconSize.height) {
        continue;
      }
      let png = await svg_to_png(value);
      let width, height;
      if (png.width > MAX_WIDTH) {
        width = MAX_WIDTH;
        height = MAX_WIDTH / png.width * png.height;
      } else {
        width = png.width;
        height = png.height;
      }
      if (!width || !height) {
        continue;
      }
      if (height > MAX_HEIGHT) {
        let oldHeight = height;
        height = MAX_HEIGHT;
        width = MAX_HEIGHT / oldHeight * width;
      }
      html = html.replace(value.outerHTML, `<img src="${png.imgsrc}" width="${width}" height="${height}">`);
    }
  }
  let imgList = element.querySelectorAll("img");
  if (imgList.length) {
    for (let index = 0; index < imgList.length; index++) {
      let value = imgList[index]
      if (value.src) {
        if (!value.src.startsWith("data:")) {
          let blob = await fetch(value.src).then((res) => { return res.blob() });
          let content = await readAsDataURL(blob);
          content.startsWith("data:application/octet-stream;base64") && (content = content.replace("data:application/octet-stream;base64", "data:image/png;base64"))
          let width, height;
          if (value.width > MAX_WIDTH) {
            width = MAX_WIDTH;
            height = MAX_WIDTH / value.width * value.height;
          } else {
            width = value.width;
            height = value.height;
          }
          if (height > MAX_HEIGHT) {
            let oldHeight = height;
            height = MAX_HEIGHT;
            width = MAX_HEIGHT / oldHeight * width;
          }
          html = html.replace(value.outerHTML, `<img src="${content}" width="${width}" height="${height}">`);
        }
      }
    }
  }
  let canvasList = element.querySelectorAll("canvas");
  if (canvasList.length) {
    _.each(canvasList, (canvas) => {
      let image = canvasImage(canvas);
      html = html.replace(canvas.outerHTML, image);
    })
  }
  return html;
}



export async function convertToBlob(item, file) {
  let response = await fetch(item);
  let blob = await response.blob();
  return blob;
}

/**
 * dataURI to Blob
 * @param {*} dataURI 
 */
export function dataURItoBlob(dataURI) {
  try {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    var byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    //Old Code
    //write the ArrayBuffer to a blob, and you're done
    //var bb = new BlobBuilder();
    //bb.append(ab);
    //return bb.getBlob(mimeString);

    //New Code
    return new Blob([ab], { type: mimeString });
  } catch (e) {
    console.error(e)
    return null;
  }
}
const reBaseURL = /(^\w+:\/\/[^\/]+)|(^[A-Za-z0-9.-]+)\/|(^[A-Za-z0-9.-]+$)/;
export function getBaseURL(link) {
  const result = link.match(reBaseURL);
  if (!result) {
    return null;
  } else if (result[1]) {
    return `${result[1]}/`;
  } else {
    return `http://${result[2] || result[3]}/`;
  }
};
export function loadJS(jsURL) {
  return new Promise((resolve, reject) => {
    if (
      _.find(document.getElementsByTagName("script"), (e) => e.src == jsURL)
    ) {
      resolve("done");
    } else {
      let jsDom = document.createElement("script");
      jsDom.type = "text/javascript";
      jsDom.async = true;
      jsDom.src = jsURL;
      jsDom.onload = function () {
        resolve("done");
      };
      let s = document.getElementsByTagName("script")[0];
      s.parentNode.insertBefore(jsDom, s);
    }
  });
};

export const CodeOptions = {
  mode: 'javascript',
  theme: 'eclipse',
  lineNumbers: true,
  lineWrapping: true,
  viewportMargin: Infinity,
  styleSelectedText: true,
  matchBrackets: false,
  styleActiveLine: false,
  nonEmpty: false,
  foldGutter: true,
  gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
  extraKeys: assembling([{
    key: linkKeys(code_editor_keys["Full Screen"]), value: function (cm) {
      cm.setOption("fullScreen", !cm.getOption("fullScreen"));
    }
  }, {
    key: "Esc", value: (cm) => {
      if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
    }
  }, {
    key: linkKeys(code_editor_keys["Show Editor LineNumbers"]), value: (cm) => {
      cm.setOption("lineNumbers", !cm.getOption("lineNumbers"));
    }
  }, {
    key: linkKeys(code_editor_keys["Fold Code"]), value: function (cm) { cm.foldCode(cm.getCursor()); },
  }])
};
/** cast data to a type data */
export function castType(data, type) {
  let oldType = typeof data;
  if (oldType === type) {
    return data;
  }
  switch (oldType) {
    case 'number': {
      switch (type) {
        case 'string':
          return data.toString();
        case 'date':
          let date = new Date(data);
          return isNaN(date) ? data : (date).toISOString();
        default:
          break;
      }
      break;
    }
    case 'string': {
      switch (type) {
        case 'date':
          let date = new Date(data);
          return isNaN(date) ? data : (date).toISOString();
        case 'number':
          return parseFloat(data);
        default:
          break;
      }
      break;
    }
    case 'boolean': {
      switch (type) {
        case 'string':
          return data === true ? "true" : "false";
        case 'number':
          return data === true ? 1 : 0;
        default:
          break;
      }
      break;
    }
    default:
      return data;
  }
  let ret = eval(type)(data);
  if (typeof ret === 'object') {
    if (ret instanceof Date) {
      return ret.toISOString();
    } else {
      return ret.toString();
    }
  }
  return ret;
}
export function extractDynamoDatas(items) {
  if (!isFailedResults(items) && items.Items) {
    return _.reduce(items.Items, (prev, item, index) => {
      prev.push(_.reduce(item, (p, v, k) => {
        if (~['S', 'N'].indexOf(_.keys(v)[0])) {
          p[k] = _.values(v)[0];
        } else {
          p[k] = JSON.stringify(_.values(v)[0]);
        }
        return p;
      }, {}))
      return prev;
    }, []);
  } else {
    return items;
  }
}
/**
 * 
 * @param {Element} data 
 */
export function extractHeader(data) {
  let header = 0;
  let headerItem = undefined;
  let headerContent = undefined;
  if (data.children && data.children.length === 1 && (headerItem = data.children.item(0)) && headerItem.tagName.match(/^H(\d)$/)) {
    header = parseInt(headerItem.tagName.match(/^H(\d)$/)[1]);
    headerContent = headerItem.textContent;
  } else if (headerItem && headerItem.children.length > 0 && (headerItem = _.filter(_.reduce(Array.from(headerItem.children), (prev, curr, index) => {
    if (curr && curr.children && curr.children.length > 0) {
      prev.push(...Array.from(curr.children))
    }
    return prev;
  }, Array.from(headerItem.children)), (item) => { return item.tagName.match(/^H(\d)$/) })[0])) {
    header = parseInt(headerItem.tagName.match(/^H(\d)$/)[1]);
    headerContent = headerItem.textContent;
  }
  return { header, headerContent };
}