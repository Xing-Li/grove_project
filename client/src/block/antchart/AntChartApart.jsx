import {
    DownOutlined, Loading3QuartersOutlined
} from '@ant-design/icons';
import { Dropdown, Input, Menu, Space, Spin } from 'antd';
import _ from 'lodash';
import PropTypes from "prop-types";
import React from "react";
import databaseClient, { workbookToDb } from 'stdlib/databaseClient';
import FileAttachments from 'stdlib/fileAttachment';
import { resolveBase } from 'stdlib/grove-require';
import { __query } from "stdlib/table.js";
import { RenderType, RenderTypeDesc, dynamo_Template, isChartRenderType } from '../../util/databaseApi';
import { extractAllKeys, isFileArrow, isFileDb, isFileExcel } from '../../util/helpers';
import { ShapeEditor } from '../../util/hqApi';
import { CategoryFromType, DatabaseType, DynamoOperationType, ModalType, MongoOperationType, NEO_FOOTER_RENDERS, ReDbDialets, extractDynamoDatas, getCodeModeTag, getOperations, isFailedResults, showConfirm, showToast } from '../../util/utils';
import actions from '../../define/Actions';
import AntChartEditor from "./AntChartEditor";
import AntChartSetting from './AntChartSetting';
import { getColumnsData, resultsToChartData } from './antchartUtils';
import { getExOptions } from './comp/CheckChartTypes';
import GraphComp from './comp/GraphComp';
import IFrameChart from './comp/IFrameChart';
import { DefaultAntChartConfig } from './comp/OptionsTypes';
import NeoSingleValueChart from './comp/SingleValueChart';
import { TableUIComp, TurnPageRawJSONComp, extractColumnDatas } from './comp/TableComp';
import NeoCardViewFooter from './neocomp/CardViewFooter';
import NeoChoroplethMapChart from './neocomp/ChoroplethMapChart';
import NeoCirclePackingChart from './neocomp/CirclePackingChart';
import NeoMapChart from './neocomp/MapChart';
import NeoSankeyChart from './neocomp/SankeyChart';
import NeoTreeMapChart from './neocomp/TreeMapChart';
import NeoGaugeChart from './neocompadvenced/GaugeChart';
import NeoSunburstChart from './neocompadvenced/SunburstChart';
import NeoAreaMapChart from './neocompadvenced/AreaMapChart';
import NeoRadarChart from './neocomp/NeoRadarChart';
import { extractNodePropertiesFromRecords } from './util/ReportRecordProcessing';
import { DrawCode, DrawPython, DrawMarkdown, DrawParameterSelect, DrawPlot } from './draw';
import { DetailIcon, GripVerticalIcon } from '../code/Icon';
import { assignFiltersSorts } from 'block/code/CodeTools';
import { DuckDBClient } from 'stdlib/duckdb';
import { fileSeparator } from 'file/fileUtils';

export default class AntChartApart extends React.Component {

    static propTypes = {
        chartEditor: PropTypes.object,
        chartConfig: PropTypes.object,
    };
    static defaultProps = {};

    constructor(props) {
        super(props);
        /** @type{DefaultAntChartConfig} */
        const chartConfig = this.props.chartConfig;
        let exOptions = getExOptions(chartConfig);
        this.state = {
            /**loading state */
            editorLoading: false,
            chartConfig,
            exOptions: exOptions,
            /**neo fields */
            fields: this.props.fields,
            result: this.props.result,
            drawType: chartConfig.renderType,
            dText: "",
            currentDataGrid: this.props.currentDataGrid,
            darkMode: this.props.darkMode,
            reload: this.props.reload,
            fullscreen: this.props.fullscreen,
        }
        this.domRef = React.createRef();
        this.lastRender = undefined;
        this.declareElement = document.createElement("div");
        this.dataElement = document.createElement("div");
    }

    componentDidUpdate(prevProps, prevState) {
        /** @type{DefaultAntChartConfig} */
        const chartConfig = this.props.chartConfig;
        let state = {};
        let change = _.filter(DefaultAntChartConfig, (v, k) => { return !_.isEqual(prevProps.chartConfig[k], chartConfig[k]) }).length > 0;
        if (!_.isEqual(prevProps.chartConfig, chartConfig)) {
            let exOptions = getExOptions(chartConfig)
            _.assign(state, {
                chartConfig,
                exOptions: exOptions,
            }, prevProps.chartConfig.renderType !== chartConfig.renderType ? { drawType: chartConfig.renderType } : {});
        }
        if (!_.isEqual(prevProps.currentDataGrid, this.props.currentDataGrid)) {
            _.assign(state, { currentDataGrid: this.props.currentDataGrid });
        }
        if (!_.isEqual(prevProps.darkMode, this.props.darkMode)) {
            _.assign(state, { darkMode: this.props.darkMode });
        }
        if (!_.isEqual(prevProps.reload, this.props.reload)) {
            _.assign(state, { reload: this.props.reload });
        }
        !_.isEmpty(state) && this.setState(state, () => {
            if (change) {
                this.drawChart();
            }
        });
    }

    componentDidMount() {
        this.drawChart();
    }

    componentWillUnmount() {
        this.clearLastRender(RenderType.Chart, true);
        /** @type {AntChartEditor} */
        const chartEditor = this.props.chartEditor;
        const { chartConfig: { chartKey } } = this.state;
        if (chartEditor.state.selectedKey === chartKey) {
            actions.variable(actions.types.CHART_EDITOR, [], () => {
                return undefined;
            })
        }
        this.unmount = true;
    }

    /**
     * 
     * @param {boolean} editorLoading default:false
     */
    setEditorLoading = (editorLoading = false) => {
        this.setState({ editorLoading });
    }

    clearLastRender(renderType, clearCache = false) {
        if (this.lastRender) {
            if (clearCache) {
                /** @type {AntChartEditor} */
                const chartEditor = this.props.chartEditor;
                /**@type {ShapeEditor } */
                const editor = chartEditor.props.editor;
                editor.deleteCacheV(this.declareElement)
                editor.deleteCacheV(this.dataElement)
                this.declareElement.innerHTML = "";
                this.dataElement.innerHTML = "";
            }
        }
        this.lastRender = renderType || RenderType.Chart;
    }

    RenderComp = (props) => {
        const { height } = props;
        const { chartConfig, chartConfig: { chartKey, neo, selectedCategoryFrom, selectedCategory, selectedClientType, type, text }, result, fields, exOptions, drawType, dText, currentDataGrid, darkMode } = this.state;
        /** @type {AntChartEditor} */
        const chartEditor = this.props.chartEditor;
        /**@type {ShapeEditor } */
        const editor = chartEditor.props.editor;
        if (chartEditor.state.safeMode) {
            return <div>Safe Mode , Can't render {drawType}.</div>
        }
        if (drawType === RenderType.Code) {
            return <DrawCode editor={editor} dText={dText} declareEle={this.declareElement} ele={this.dataElement}></DrawCode>
        } else if (drawType === RenderType.Python) {
            return <DrawPython editor={editor} dText={dText} declareEle={this.declareElement} ele={this.dataElement}></DrawPython>
        } else if (drawType === RenderType.ParameterSelect) {
            return <DrawParameterSelect editor={editor} declareEle={this.declareElement} selectedCategoryFrom={selectedCategoryFrom} selectedCategory={selectedCategory} ele={this.dataElement} neo={neo} setEditorLoading={this.setEditorLoading}
                updateConfig={(neo) => {
                    this.setState({ chartConfig: _.assign({}, chartConfig, { neo }) }, () => {
                        this.drawChart();
                    })
                }}
            ></DrawParameterSelect>
        } else if (drawType === RenderType.Markdown) {
            return <DrawMarkdown editor={editor} dText={dText} ele={this.dataElement}></DrawMarkdown>
        } else if (drawType === RenderType.IFrame) {
            return <IFrameChart result={{ input: text || "" }}></IFrameChart>
        }
        if (!result || result.error || isFailedResults(result)) {
            return <div className="text-danger">Can not render {drawType}, result: {!result || result.error && JSON.stringify(result.error) || JSON.stringify(result)}</div>
        }
        if (isChartRenderType(drawType)) {
            return <DrawPlot height={height} type={type} exOptions={exOptions} editor={editor} chartKey={chartKey} darkMode={darkMode}></DrawPlot>
        } else if (drawType === RenderType.Table) {
            return <TableUIComp editor={editor} selectedCategoryFrom={selectedCategoryFrom} selectedCategory={selectedCategory} selectedClientType={selectedClientType} result={result}  {...exOptions} saveFunc={chartConfig.reD && chartConfig.chartKey !== chartEditor.state.selectedKey && this.saveFunc}></TableUIComp>
        } else if (drawType === RenderType.RawJSON) {
            return <TurnPageRawJSONComp renderType={drawType} result={result}></TurnPageRawJSONComp>
        } else if (drawType === RenderType.SingleValue) {
            return <NeoSingleValueChart result={result} settings={exOptions}></NeoSingleValueChart>
        }
        let graphKey = selectedCategory;
        if (selectedCategoryFrom === CategoryFromType.neo4j || selectedCategoryFrom === CategoryFromType.client && selectedClientType === DatabaseType.Neo4j) {
            if (result.summary && result.summary.query) {
                graphKey += result.summary.query.text + JSON.stringify(result.summary.query.parameters)
            } else {
                graphKey += neo.query;
            }
            if (exOptions.nodeColorScheme) {
                graphKey += exOptions.nodeColorScheme
            }
        }
        if (drawType === RenderType.Graph) {
            return <GraphComp key={graphKey} selection={neo && neo.selection} records={result.records} settings={exOptions}></GraphComp>
        } else if (drawType === RenderType.ChoroplethMap) {
            return <NeoChoroplethMapChart key={graphKey} records={result.records} selection={neo && neo.selection} settings={exOptions} txyFunc={this.txyFunc}></NeoChoroplethMapChart>
        } else if (drawType === RenderType.Map) {
            return <NeoMapChart key={graphKey} records={result.records} dimensions={currentDataGrid ? { width: currentDataGrid.w * window.innerWidth / 12, height: currentDataGrid.h * window.innerHeight / 12 } : { width: 100, height: 100 }} selection={neo && neo.selection} settings={exOptions}></NeoMapChart>
        } else if (drawType === RenderType.CirclePacking) {
            return <NeoCirclePackingChart key={graphKey} records={result.records} selection={neo && neo.selection} settings={exOptions}></NeoCirclePackingChart>
        } else if (drawType === RenderType.SankeyChart) {
            return <NeoSankeyChart key={graphKey} records={result.records} selection={neo && neo.selection} settings={_.assign(exOptions, { labelProperty: neo && neo.labelProperty })}></NeoSankeyChart>
        } else if (drawType === RenderType.GaugeChart) {
            return <NeoGaugeChart key={graphKey} records={result.records} selection={neo && neo.selection} settings={_.assign(exOptions, { labelProperty: neo && neo.labelProperty })}></NeoGaugeChart>
        } else if (drawType === RenderType.SunburstChart) {
            return <NeoSunburstChart key={graphKey} records={result.records} selection={neo && neo.selection} settings={_.assign(exOptions, { labelProperty: neo && neo.labelProperty })}></NeoSunburstChart>
        } else if (drawType === RenderType.Treemap) {
            return <NeoTreeMapChart key={graphKey} records={result.records} selection={neo && neo.selection} settings={_.assign(exOptions, { labelProperty: neo && neo.labelProperty })}></NeoTreeMapChart>
        } else if (drawType === RenderType.AreaMap) {
            return <NeoAreaMapChart key={graphKey} records={result.records} selection={neo && neo.selection} settings={_.assign(exOptions, { labelProperty: neo && neo.labelProperty })}></NeoAreaMapChart>
        } else if (drawType === RenderType.Radar) {
            return <NeoRadarChart key={graphKey} height={height} records={result.records} selection={neo && neo.selection} settings={_.assign(exOptions, { labelProperty: neo && neo.labelProperty })}></NeoRadarChart>
        } else {
            return <div>Can not render {drawType}, not support now!</div>
        }
    }

    isDbFile(selectedCategoryFrom, selectedCategory) {
        /** @type {AntChartEditor} */
        const chartEditor = this.props.chartEditor;
        /**@type {ShapeEditor } */
        const editor = chartEditor.props.editor;
        return selectedCategoryFrom === CategoryFromType.files && isFileDb(editor.react_component.getFileData(selectedCategory));
    }

    isExcelFile(selectedCategoryFrom, selectedCategory) {
        /** @type {AntChartEditor} */
        const chartEditor = this.props.chartEditor;
        /**@type {ShapeEditor } */
        const editor = chartEditor.props.editor;
        return selectedCategoryFrom === CategoryFromType.files && isFileExcel(editor.react_component.getFileData(selectedCategory));
    }
    isArrowFile(selectedCategoryFrom, selectedCategory) {
        /** @type {AntChartEditor} */
        const chartEditor = this.props.chartEditor;
        /**@type {ShapeEditor } */
        const editor = chartEditor.props.editor;
        return selectedCategoryFrom === CategoryFromType.files && isFileArrow(editor.react_component.getFileData(selectedCategory));
    }

    drawChart = async () => {
        const { chartConfig, chartConfig: { type, active, selectedCategoryFrom, selectedCategory, selectedClientType, selectedProperties, emptyFills, castColumns, joinColumns, renderType, chartData }, exOptions, reload } = this.state;
        /** @type {AntChartEditor} */
        const chartEditor = this.props.chartEditor;
        /**@type {HTMLDivElement} */
        let container = this.domRef.current
        if (container) {
            this.setEditorLoading(true);
            if (this.chart) {
                this.chart.destroy();
                this.chart = undefined;
            }
            if (chartEditor.state.safeMode) {
                this.clearLastRender(renderType, true);
                return this.setState({ drawType: renderType || RenderType.Chart }, this.setEditorLoading);
            }
            /** @type {ShapeEditor} */
            const editor = chartEditor.props.editor;
            if (isChartRenderType(renderType) && !active && exOptions.data.length > 0) {//dead data
                this.setState({ drawType: RenderType.Chart }, this.setEditorLoading);
            } else if (renderType === RenderType.ParameterSelect) {
                const { chartConfig: { neo: { selectionType, nodeLabel, propertyName, propertyNum, propertyValue }, chartKey } } = this.state;
                this.clearLastRender(renderType, true);
                await editor.runFunc(selectionType && nodeLabel && propertyName && propertyValue ? `${["neodash", nodeLabel, propertyName].join("_").toLowerCase()}${undefined !== propertyNum ? `_${propertyNum}` : ""} = "${propertyValue}"` : "",
                    this.declareElement, this.dataElement, () => {
                        this.setState({ drawType: RenderType.ParameterSelect }, this.setEditorLoading);
                    }, () => {
                        this.setState({ drawType: RenderType.ParameterSelect }, this.setEditorLoading);
                    }, true, `_${chartKey.replaceNoWordToUnderline()}`);
            } else if (~[RenderType.Markdown, RenderType.Code, RenderType.Python].indexOf(renderType)) {
                const { chartConfig: { text, chartKey } } = this.state;
                this.clearLastRender(renderType, true);
                let codeTag = getCodeModeTag(renderType.toLowerCase());
                let dText = codeTag ? `${codeTag}\`${(text || "").replaceAll("`", "\\`")}\`` : text;
                await editor.runFunc(dText, this.declareElement, this.dataElement, () => {
                    this.setState({ drawType: renderType, dText }, this.setEditorLoading);
                }, () => {
                    this.setState({ drawType: renderType, dText }, this.setEditorLoading);
                }, true, `_${chartKey.replaceNoWordToUnderline()}`);
            } else if (renderType === RenderType.IFrame) {
                const { chartConfig: { text } } = this.state;
                this.clearLastRender(renderType, true);
                this.setState({ drawType: RenderType.IFrame }, this.setEditorLoading);
            } else {
                let callback = (result, fields, clearCache = false) => {
                    !this.state.editorLoading && this.setEditorLoading(true);
                    this.clearLastRender(renderType, clearCache);
                    if (!result || result.error || isFailedResults(result)) {
                        return this.setState({ result, fields, drawType: renderType }, this.setEditorLoading);
                    }
                    if (isChartRenderType(renderType)) {//active data
                        let chartData;
                        if (selectedCategoryFrom === CategoryFromType.neo4j || selectedCategoryFrom === CategoryFromType.client && selectedClientType === DatabaseType.Neo4j) {
                            const { records, summary } = result;
                            const { columns, rows } = extractColumnDatas(false, records);
                            chartData = resultsToChartData(rows, selectedProperties, emptyFills, castColumns, joinColumns);
                        } else {
                            chartData = resultsToChartData(result, selectedProperties, emptyFills, castColumns, joinColumns);
                        }
                        _.assign(chartConfig, chartData);
                        let exOptions2 = getExOptions(_.assign({}, chartConfig, chartData));
                        // console.log(exOptions2.data && exOptions2.data.keysArr);
                        this.setState({ result, fields, drawType: renderType || RenderType.Chart, exOptions: exOptions2 }, this.setEditorLoading);
                    } else {
                        //need save some datas for export csv/xlsx file
                        if (!!~[RenderType.Table, RenderType.Graph, RenderType.Map, RenderType.CirclePacking, RenderType.SankeyChart, RenderType.ChoroplethMap, RenderType.AreaMap, RenderType.Radar].indexOf(renderType)) {
                            let chartData;
                            if (selectedCategoryFrom === CategoryFromType.neo4j || selectedCategoryFrom === CategoryFromType.client && selectedClientType === DatabaseType.Neo4j) {
                                const { records, summary } = result;
                                const { columns, rows } = extractColumnDatas(false, records);
                                chartData = resultsToChartData(rows, _.map(columns, (column) => { return column.key }), {}, {}, {});
                            } else {
                                chartData = resultsToChartData(result, extractAllKeys(result), {}, {}, {});
                            }
                            _.assign(chartConfig, chartData);
                        }
                        this.setState({ result, fields, drawType: renderType }, this.setEditorLoading);
                    }
                }
                if (!reload && this.state.result) {
                    return callback(this.state.result, this.state.fields, false);
                }
                let result;
                let fields = undefined;
                if (selectedCategoryFrom === CategoryFromType.database ||
                    selectedCategoryFrom === CategoryFromType.client && ~ReDbDialets.indexOf(selectedClientType) ||
                    this.isDbFile(selectedCategoryFrom, selectedCategory) ||
                    this.isExcelFile(selectedCategoryFrom, selectedCategory) ||
                    this.isArrowFile(selectedCategoryFrom, selectedCategory)) {
                    let dbClient;
                    if (this.isDbFile(selectedCategoryFrom, selectedCategory)) {
                        let constructor = FileAttachments(resolveBase);
                        dbClient = await constructor(editor.getFileAttachmentUrl(selectedCategory, editor.getUploadUri())).sqlite();
                    } else if (this.isExcelFile(selectedCategoryFrom, selectedCategory)) {
                        let constructor = FileAttachments(resolveBase);
                        let wb = await constructor(editor.getFileAttachmentUrl(selectedCategory, editor.getUploadUri())).xlsx();
                        dbClient = await workbookToDb(wb);
                    } else if (this.isArrowFile(selectedCategoryFrom, selectedCategory)) {
                        let constructor = FileAttachments(resolveBase);
                        let ff = await constructor(editor.getFileAttachmentUrl(selectedCategory, editor.getUploadUri()));
                        dbClient = await DuckDBClient.of({
                            [selectedCategory.substring(~selectedCategory.lastIndexOf(fileSeparator) ? selectedCategory.lastIndexOf(fileSeparator) + 1 : 0).toCommonName()]: ff
                        });
                    } else if (selectedCategoryFrom === CategoryFromType.client) {
                        dbClient = window.DbClients[editor.getSelfModuleName()][selectedCategory];
                    } else {
                        dbClient = databaseClient(selectedCategory);
                    }
                    const sqlType = dbClient.dialect;
                    const { chartConfig: { reD: { dbTable, query, selectedTable, filters, selectedColumns, sorts, slice } } } = this.state;
                    if (dbTable) {
                        if (dbClient.dialect === DatabaseType.BigQuery) {
                            const [schema, table] =
                                selectedTable && ~selectedTable.indexOf(".") ? selectedTable.split(".") : [null, selectedTable];
                            if (dbClient.dialect === DatabaseType.BigQuery && schema) {
                                let datasets = await dbClient.getDatasets();
                                if (this.unmount) {
                                    return;
                                }
                                if (isFailedResults(datasets)) {
                                    datasets = [];
                                }
                                let arr = _.filter(datasets, (dataset) => { return dataset.id === schema });
                                dbClient.location = arr.length ? arr[0].location : undefined;
                            }
                        }
                        let operations = getOperations(selectedCategory, sqlType, selectedTable, filters, selectedColumns, sorts, slice);
                        if (!operations) {
                            return callback({ error: { msg: "Can not render table, reason: operations error!" } }, fields, true);
                        }
                        result = await __query(dbClient, operations, new Promise(
                            (value) => {
                                // console.log(value);
                                return value;
                            }, (reason) => {
                                showToast(reason);
                                error = reason;
                            }
                        ))
                    } else {
                        result = await dbClient.query(query);
                    }
                    if (this.unmount) {
                        return;
                    }
                    callback(result, fields, true);
                } else if (selectedCategoryFrom === CategoryFromType.mongodb || selectedCategoryFrom === CategoryFromType.client && selectedClientType === DatabaseType.MongoDB) {
                    const { chartConfig: { mongo } } = this.state;
                    const { chartConfig: { chartKey } } = this.state;
                    let name = `_chart_${chartKey.replaceNoWordToUnderline()}`;
                    if (!mongo) {
                        return callback({ error: { msg: "Can not query, reason: query is empty!" } }, fields, true);
                    }
                    let mongoTmp = _.clone(mongo);
                    delete mongoTmp.filterStr; delete mongoTmp.filterTmp; delete mongoTmp.projectionStr; delete mongoTmp.sortStr;
                    let sql = JSON.stringify(_.assign({ operationType: MongoOperationType.find }, mongoTmp)).replaceAll("`", "\\`");
                    console.log(sql);
                    await editor.runFunc(`${name} = {
                        let client = ${selectedCategoryFrom === CategoryFromType.client ? `selectDbClients("${editor.getSelfModuleName()}", "${selectedCategory}")` : `DatabaseClient("${selectedCategory}")`}
                        if(!client){
                            return null;
                        }
                        return client.query(JSON.stringify(${sql}));
                    }`, this.declareElement, this.dataElement, (result, name) => {
                        if (this.unmount) {
                            return;
                        }
                        if (!result) {
                            callback({ error: { msg: "Can not get result or database client can not init!" } }, fields, false);
                        } else {
                            callback(result, fields, false);
                        }
                    }, (error, name) => {
                        if (this.unmount) {
                            return;
                        }
                        callback({ error }, fields, false);
                    }, true, `_${chartKey.replaceNoWordToUnderline()}`);
                } else if (selectedCategoryFrom === CategoryFromType.dynamodb || selectedCategoryFrom === CategoryFromType.client && selectedClientType === DatabaseType.DynamoDB) {
                    const { chartConfig: { dynamo } } = this.state;
                    const { chartConfig: { chartKey } } = this.state;
                    let name = `_chart_${chartKey.replaceNoWordToUnderline()}`;
                    if (!dynamo) {
                        return callback({ error: { msg: "Can not query, reason: query is empty!" } }, fields, true);
                    }
                    let dynamoTmp = _.clone(dynamo);
                    (dynamoTmp.TableName === dynamo_Template.TableName) && (delete dynamoTmp.TableName);
                    (dynamoTmp.IndexName === dynamo_Template.IndexName) && (delete dynamoTmp.IndexName);
                    delete dynamoTmp.tab;
                    delete dynamoTmp.scanFilterTmp;
                    delete dynamoTmp.scanFilterStr;
                    _.isEmpty(dynamoTmp.AttributesToGet) && (delete dynamoTmp.AttributesToGet);
                    let sql = JSON.stringify(_.assign({ operationType: dynamoTmp.tab ? DynamoOperationType.Query : DynamoOperationType.Scan, params: dynamoTmp })).replaceAll("`", "\\`");
                    console.log(sql);
                    await editor.runFunc(`${name} = {
                        let client = ${selectedCategoryFrom === CategoryFromType.client ? `selectDbClients("${editor.getSelfModuleName()}", "${selectedCategory}")` : `DatabaseClient("${selectedCategory}")`}
                        if(!client){
                            return null;
                        }
                        return client.query(JSON.stringify(${sql}));
                    }`, this.declareElement, this.dataElement, (result, name) => {
                        if (this.unmount) {
                            return;
                        }
                        if (!result) {
                            callback({ error: { msg: "Can not get result or database client can not init!" } }, fields, false);
                        } else {
                            result = extractDynamoDatas(result);
                            callback(result, fields, false);
                        }
                    }, (error, name) => {
                        if (this.unmount) {
                            return;
                        }
                        callback({ error }, fields, false);
                    }, true, `_${chartKey.replaceNoWordToUnderline()}`);
                } else if (selectedCategoryFrom === CategoryFromType.neo4j || selectedCategoryFrom === CategoryFromType.client && selectedClientType === DatabaseType.Neo4j) {
                    const { chartConfig: { neo } } = this.state;
                    const { chartConfig: { chartKey } } = this.state;
                    let name = `_chart_${chartKey.replaceNoWordToUnderline()}`;
                    let prameters = {};
                    let query = neo && neo.query;
                    if (query) {
                        let matchArr = query.match(/\$(\w+)/gm);
                        if (null != matchArr && matchArr.length !== 0) {
                            _.reduce(matchArr, (prev, v, index) => {
                                prev[v.substring(1)] = v.substring(1);
                                return prev;
                            }, prameters)
                        }
                    } else {
                        return callback({ error: { msg: "Can not query, reason: query is empty!" } }, fields, true);
                    }
                    await editor.runFunc(`${name} = {
                        let client = ${selectedCategoryFrom === CategoryFromType.client ? `selectDbClients("${editor.getSelfModuleName()}", "${selectedCategory}")` : `DatabaseClient("${selectedCategory}")`}
                        if(!client){
                            return null;
                        }
                        return client.query(\`${query.replaceAll("`", "\\`")}\`, ${JSON.stringify(prameters).replaceAll("\"", "")});
                    }`, this.declareElement, this.dataElement, (result, name) => {
                        if (this.unmount) {
                            return;
                        }
                        if (!result) {
                            callback({ error: { msg: "Can not get result or database client can not init!" } }, fields, false);
                        } else {
                            if (result && !result.error && result.records) {
                                const { useReturnValuesAsFields, useNodePropsAsFields } = RenderTypeDesc[renderType]
                                let rowLimit = neo && neo.maxRecords || RenderTypeDesc[renderType].maxRecords || 1000;
                                if (editor.getSettings().getRowLimiting() && result.records.length > rowLimit) {
                                    result.records = result.records.slice(0, rowLimit);
                                    result.notice = `Results over ${rowLimit} limit, have been truncated!`;
                                }
                                let records = result.records;
                                if (useReturnValuesAsFields) {
                                    // Send a deep copy of the returned record keys as the set of fields.
                                    fields = (records && records[0] && records[0].keys) ? records[0].keys.slice() : [];
                                } else if (useNodePropsAsFields) {
                                    // If we don't use dynamic field mapping, but we do have a selection, use the discovered node properties as fields.
                                    fields = extractNodePropertiesFromRecords(records);
                                }
                            }
                            callback(result, fields, false);
                        }
                    }, (error, name) => {
                        if (this.unmount) {
                            return;
                        }
                        callback({ error }, fields, false);
                    }, true, `_${chartKey.replaceNoWordToUnderline()}`);
                } else if (~[CategoryFromType.main, CategoryFromType.files, CategoryFromType.shareData].indexOf(selectedCategoryFrom)) {
                    const columnsData = await getColumnsData(editor.getSelfModuleName(), selectedCategoryFrom, selectedCategory);
                    result = columnsData.data;
                    callback(result, fields, true);
                } else {
                    callback({ error: { msg: "Not Implement Category Type!" } }, fields, true);
                }
            }
        }
    }

    editFunc = async () => {
        const { chartConfig, exOptions } = this.state;
        /** @type {AntChartEditor} */
        const chartEditor = this.props.chartEditor;
        const { state: { selectedKey, readOnly } } = chartEditor;
        let selectedKeyTmp = selectedKey !== chartConfig.chartKey ? chartConfig.chartKey : undefined;
        if (selectedKey) {
            /**@type {AntChartSetting} */
            let settings = chartEditor.settingRef.current;
            if (!settings) {
                return;
            }
            const { chartConfig, oldChartConfig } = settings.state;
            if (!_.isEqual(chartConfig, oldChartConfig)) {
                if (await showConfirm("Save Current Chart Settings?")) {
                    chartEditor.switchChart(chartConfig, selectedKeyTmp)
                } else {
                    chartEditor.switchChart(oldChartConfig, selectedKeyTmp)
                }
            } else {
                chartEditor.setDatas({ selectedKey: selectedKeyTmp })
            }
        } else {
            chartEditor.setDatas({ selectedKey: selectedKeyTmp })
        }
    }

    titleFunc = (text) => {
        this.alterChartConfig(function (options) {
            options.title.text = text;
        })
    }

    txyFunc = (x, y, scale) => {
        this.alterChartConfig(function (options) {
            options.projectionTranslationX.value += x;
            options.projectionTranslationY.value += y;
            options.projectionScale.value *= scale;
        })
    }

    alterChartConfig = (func) => {
        const { chartConfig, exOptions } = this.state;
        /** @type {AntChartEditor} */
        const chartEditor = this.props.chartEditor;
        const { state: { selectedKey, readOnly } } = chartEditor;
        if (readOnly || !func) {
            return;
        }
        if (selectedKey === chartConfig.chartKey) {
            /**@type {AntChartSetting} */
            let settings = chartEditor.settingRef.current;
            if (!settings || !chartConfig.options) {
                return;
            }
            let options = _.cloneDeep(chartConfig.options)
            func(options);
            settings.modifyChartConfig({ options: options }, () => {
                chartEditor.run(() => { }, false);
            })
        } else {
            if (!chartConfig.options) {
                return;
            }
            let options = _.cloneDeep(chartConfig.options)
            func(options);
            let state = { reload: false };
            chartEditor.saveChart(_.assign({}, chartConfig, { options }), state);
            chartEditor.setDatas(state);
        }
    }

    saveFunc = (startEndMap, filteredInfo, sortedInfo) => {
        const { chartConfig } = this.state;
        /** @type {AntChartEditor} */
        const chartEditor = this.props.chartEditor;
        const { state: { selectedKey, readOnly } } = chartEditor;
        if (selectedKey === chartConfig.chartKey) {
            /**@type {AntChartSetting} */
            let settings = chartEditor.settingRef.current;
            if (!settings || !chartConfig.reD) {
                return;
            }
            let reD = _.cloneDeep(chartConfig.reD)
            assignFiltersSorts(startEndMap, filteredInfo, sortedInfo, reD.filters, reD.sorts);
            settings.modifyChartConfig({ reD: reD }, () => {
                chartEditor.run(() => { });
            })
        } else {
            if (!chartConfig.reD) {
                return;
            }
            let reD = _.cloneDeep(chartConfig.reD)
            assignFiltersSorts(startEndMap, filteredInfo, sortedInfo, reD.filters, reD.sorts);
            chartEditor.saveChart(_.assign({}, chartConfig, { reD }));
        }
    }

    /**
     * 
     * @param {*} info 
     */
    menuClickHandler = async (info) => {
        const { chartConfig, exOptions, result, fields } = this.state;
        /** @type {AntChartEditor} */
        const chartEditor = this.props.chartEditor;
        switch (info.key) {
            case "View":
                window.currentEditor.react_component.setState({
                    modalType: ModalType.ChartFullScreen,
                    modalData: {
                        chartConfig,
                        chartEditor,
                        result, fields
                    }
                });
                break;
            case "Edit":
                this.editFunc();
                break;
            case "Refresh":
                this.setState({ reload: true }, () => {
                    this.drawChart();
                })
                break;
            case "Copy":
                chartEditor.copyChart(chartConfig.chartKey);
                break;
            case "Duplicate":
                chartEditor.duplicateChart(chartConfig.chartKey);
                break;
            case "Data":
                break;
            case "Query":
                break;
            case "Panel JSON":
                break;
            case "Remove":
                showConfirm(<div>Are you sure you want to remove <span className="text-primary">{exOptions.title && exOptions.title.text}</span>?</div>,
                    () => {
                        chartEditor.deleteChart(chartConfig.chartKey)
                    }, undefined, { okType: 'danger', okText: "Remove", }
                );
                break;
        }
    }

    render() {
        /** @type {AntChartEditor} */
        const chartEditor = this.props.chartEditor;
        const { readOnly, selectedKey, safeMode } = chartEditor.state;
        const { chartConfig, chartConfig: { renderType, neo, chartKey }, exOptions, exOptions: { title, backgroundColor, textAlign }, editorLoading, fields, drawType, fullscreen } = this.state;
        const { text, align, size } = title || {};
        const menu =
            <Menu className="normal-icon" style={{ width: "150px" }} onClick={(info) => {
                this.menuClickHandler(info);
            }}>
                <Menu.Item key="View">
                    <i className="icon fas fa-external-link-square-alt"></i>View
                </Menu.Item>
                <Menu.Item key="Edit">
                    <i className={`icon ${!readOnly ? "far fa-edit" : "fas fa-file-export"}`}></i>{readOnly ? `${selectedKey === chartKey ? "Exit " : ""}Export Files` : `${selectedKey === chartKey ? "Exit " : ""}Edit`}
                </Menu.Item>
                <Menu.Item key="Refresh">
                    <i className="icon fas fa-sync-alt"></i>Refresh
                </Menu.Item>
                <Menu.SubMenu key="More"
                    title={<span><i className="icon fas fa-angle-right"></i>More...</span>}>
                    {!readOnly && <Menu.Item key="Copy">
                        Copy
                    </Menu.Item>}
                    {!readOnly && <Menu.Item key="Duplicate">
                        Duplicate
                    </Menu.Item>}
                </Menu.SubMenu>
                <Menu.Divider />
                {!readOnly && <Menu.Item key="Remove">
                    <i className="icon fas fa-trash"></i>Remove
                </Menu.Item>}
            </Menu>
        /**@type {React.CSSProperties} */
        const style = {};
        if (~[RenderType.Table, RenderType.IFrame].indexOf(drawType)) {
            style.overflowY = "unset";
        }
        if (backgroundColor) {
            style.backgroundColor = backgroundColor;
        }
        if (textAlign) {
            style.textAlign = textAlign;
        }
        /**@type {React.CSSProperties} */
        const titleStyle = {};
        align && (titleStyle.textAlign = align);
        const hlevel = size || "h6";
        const level = 1 + (6 - parseInt(hlevel.substring(1))) * 0.25;
        style.height = `calc(${fullscreen ? "80vh - 48px" : "100%"} - 8px - ${1.5715 * level}rem)`
        return <React.Fragment>
            <div className="d-flex flex-column justify-content-center chart-flex">
                <div className={`d-flex justify-content-between align-items-center normal-icon drag-move chart-title-menu`} style={titleStyle}>
                    <div className='d-flex align-items-center' style={
                        { width: `calc(100% - 30px)` }
                    }>
                        <GripVerticalIcon className={`icon drag-move`} />
                        <Input key={text || "undefined"} readOnly={readOnly} className='underline-input grid-drag-cancel' defaultValue={text || ""} style={{
                            fontSize: `${level}rem`,
                            fontWeight: "bold",
                        }} placeholder="Chart name..."
                            onKeyDown={(e) => {
                                if (e.key === "Escape") {
                                    e.stopPropagation();
                                } else if (e.key === "Enter" && e.target.value !== text) {
                                    this.titleFunc(e.target.value);
                                    e.stopPropagation();
                                    e.preventDefault();
                                }
                            }}
                            onBlur={(e) => {
                                if (e.target.value !== text) {
                                    this.titleFunc(e.target.value);
                                }
                            }} />
                    </div>
                    <Dropdown overlay={menu} trigger={["click"]}>
                        <div className={`grid-drag-cancel`} title='Menu'><DetailIcon className="icon"></DetailIcon></div>
                    </Dropdown>
                </div>
                {editorLoading && <div className="code-loading-top" style={{ height: style.height }}>
                    <Spin spinning={true} indicator={<Loading3QuartersOutlined style={{ fontSize: 24, }} spin />} >
                        <div className="editor-full-div"></div>
                    </Spin>
                </div>}
                <div className="chart-render grid-drag-cancel" style={style} ref={this.domRef}>
                    <this.RenderComp height={style.height}></this.RenderComp>
                </div>
                {fields && !!fields.length && !!~NEO_FOOTER_RENDERS.indexOf(renderType) && <NeoCardViewFooter settings={exOptions} type={drawType} fields={fields} selection={neo && neo.selection} onSelectionUpdate={(nodeLabel, value) => {
                    let selection = neo && neo.selection || {}
                    selection[nodeLabel] = value;
                    this.setState({ chartConfig: _.assign({}, chartConfig, { neo: _.assign(neo || {}, { selection }) }) }, () => {
                        this.drawChart();
                    })
                }}></NeoCardViewFooter>}
            </div>
        </React.Fragment>
    }
}