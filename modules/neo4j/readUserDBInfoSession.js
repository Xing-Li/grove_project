const ONE_DAY = 24 * 60 * 60 * 1000;

function readUserDBInfoSession(req, projectId) {
	let dbs = req.session.dbs || {};
	let project = dbs[projectId];
	if (project && project.lastTime && new Date().getTime() - project.lastTime < ONE_DAY) {
		return project;
	} else {
		delete dbs[projectId];
		req.session.dbs = dbs;
		return null;
	}
}

module.exports = readUserDBInfoSession;