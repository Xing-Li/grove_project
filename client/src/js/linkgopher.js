import _ from "lodash";
export const URL_CHECK = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
export const reBaseURL = /(^\w+:\/\/[^\/]+)|(^[A-Za-z0-9.-]+)\/|(^[A-Za-z0-9.-]+$)/;
const filteringDomains = false;
/**
 * Extract links.
 *
 * @function extractLinks
 */
export function extractLinks(myDocument) {
  if(!myDocument || !myDocument.links){
    return null;
  }
  const links = [];

  for (let index = 0; index < myDocument.links.length; index++) {
    links.push(decodeURI(myDocument.links[index].href));
  }

  return links.length ? links : null;
};
/**
 * @function handler
 * @param {array} urls -- Array of links.
 * @param {string} pattern -- Pattern for filtering.
 */
export function handler(links, pattern, addNodesFilter) {
  // To filter links like: javascript:void(0)
  const resLinks = links.filter(link => link.lastIndexOf('://', 10) > 0);
  // Remove duplicate, sorting of links.
  const items = [...(new Set(resLinks))].sort();
  const re = pattern ? new RegExp(pattern, 'g') : null;
  const containerLinks = document.createElement("div");
  const containerDomains = document.createElement("div");
  const added = items.filter(link => addNodes(link, containerLinks, re, addNodesFilter));
  if (!added.length) {
    return { message: 'noMatches' };
  }
  let elements = [].slice.call(containerLinks.children)
  elements.sort(function (a, b) {
    return a.innerText > b.innerText;
  });
  _.each(elements, element => containerLinks.appendChild(element))
  // Extract base URL from link, remove duplicate, sorting of domains.
  const domains = [...(new Set(added.map(link => getBaseURL(link))))].sort();
  const reDomains = filteringDomains ? re : null;
  domains.forEach(domain => addNodes(domain, containerDomains, reDomains, addNodesFilter));
  return { links: containerLinks, domains: containerDomains };
};

export const showObject = (object, parent = document.body) => {
  const keyValueTemplate = document.getElementById('keyValue');
  // const folderTemplate = document.getElementById('folder');
  Object.entries(object).forEach(([key, value]) => {
    if (value && (value instanceof Element)) {
      const element = document.createElement('details');
      const summary = element.appendChild(document.createElement('summary'));
      summary.textContent = key;
      element.appendChild(value);
      parent.appendChild(element);
    } else if (value && typeof value === "object") {
      // Since this structure is really simple, just create the elements.
      const element = document.createElement('details');
      const summary = element.appendChild(document.createElement('summary'));
      summary.textContent = key;

      element.addEventListener('toggle', () => {
        showObject(value, element)
      }, { once: true });
      parent.appendChild(element);
    } else {
      // Use a template since the structure is somewhat complex.
      const element = document.importNode(keyValueTemplate.content, true);
      element.querySelector('.key').textContent = key;
      element.querySelector('.value').textContent = value;
      parent.appendChild(element);
    }
  });
};

/**
 * Add nodes to container.
 *
 * @function addNodes
 * @param {string} link
 * @param {Node} container
 * @param {object|null} re -- Regular Expression pattern.
 * @return {boolean} -- Whether link added into document.
 */
function addNodes(url, container, re, addNodesFilter) {
  if (re && !url.match(re)) return false;

  const div = document.createElement('div');
  const a = document.createElement('a');
  addNodesFilter && addNodesFilter(a, url);
  div.appendChild(a);
  container.appendChild(div);

  return true;
};

/**
 * Get base URL of link.
 *
 * @function getBaseURL
 * @param {string} link
 */
export function getBaseURL(link) {
  const result = link.match(reBaseURL);

  if (!result) {
    return null;
  } else if (result[1]) {
    return `${result[1]}/`;
  } else {
    return `http://${result[2] || result[3]}/`;
  }
};

export function getDomain(link) {
  const result = link.match(reBaseURL);

  if (!result) {
    return null;
  } else if (result[1]) {
    return `${result[1]}`.replace(/^\w+:\/\//, '');
  } else {
    return `${result[2] || result[3]}`;
  }
};