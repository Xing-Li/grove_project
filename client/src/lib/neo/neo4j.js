import _ from 'lodash';

const neo4j = require('neo4j-driver');
/**
 * @param {string} url
 */
const Neo4jClient = function (url, proxyConfig) {
  const { user, password } = proxyConfig;
  let config = {};
  if (!~["neo4j+s", "neo4j+ssc", "bolt+s", "bolt+ssc"].indexOf(proxyConfig.protocal) ||
    ~["127.0.0.1", "localhost"].indexOf(proxyConfig.host)
  ) {
    config.encrypted = false;
  }
  this.driver = neo4j.driver(
    url,
    neo4j.auth.basic(user, password),
    config
  )
  this.proxyConfig = proxyConfig;
};
Neo4jClient.prototype.query = async function (sql, params) {
  console.log(sql)
  console.log(params)
  if (!this.driver) {
    return { error: "server busy!" };
  }
  try {
    let session = this.driver.session({ defaultAccessMode: neo4j.session.READ })
    // Run a Cypher statement, reading the result in a streaming manner as records arrive:
    let result = await session
      .run(sql, params)
      .then(result => {
        session.close();
        return result;
      }).catch((e) => {
        console.error(e);
        session.close();
        return { error: e.message };
      })
    return result;
  } catch (e) {
    return { error: e.message };
  }
};

Neo4jClient.prototype.close = function () {
  if (!this.driver) {
    return;
  }
  this.driver.close().then(() => {
    console.log(` ${this.proxyConfig.name} ${this.proxyConfig.type} have ended`);
  });
}

export default Neo4jClient;