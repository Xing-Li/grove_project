import { Affix, Button, Dropdown, Menu, Tooltip } from 'antd';
import { EllipsisOutlined } from '@ant-design/icons';
import axios from 'axios';
import cx from "classnames";
import JSZip from 'jszip';
import _ from 'lodash';
import moment from 'moment';
import PropTypes from "prop-types";
import React from 'react';
import { mdTransferToEditorjs } from '../block/markdown/FileHandler';
import editor, { CommonEditor } from '../commonEditor';
import { DrawerType } from '../editorSettings';
import Filter from '../components/Filter';
import ModalComp from '../components/ModalComp';
import Spinner from '../components/Spinner';
import actions from '../define/Actions';
import { copyShareFile, disposeModule, removeModule, ROOT_URI } from '../util/hqUtils';
import { removeGlobalScope, addGlobalScope } from '../util/hintUtil';
import Util from "../util/util";
import { updateGroveLinks } from './../common/graphOps';
import MyDropzone from './MyDropzone';
import Share from './Share';
import Common from '../util/Common'
import { toContentByType } from '../util/toText'

/**@type {import('html2canvas').default} */
const html2canvas = window.html2canvas;
const iconv = require('iconv-lite');
const jschardet = require("jschardet")
const { ImageCache, formatSizeUnits, formatTime,
  isFileImage, isFileJs, isFileSvg, isDir, isFile,
  isFileEditorJs, isFileMd, codeTime, imageTypeReg, ImageType, SelectType,
  transferType, isFileNoEdit, isFileDb, isFileGzip, isFileAudio, isFileExcel, isFileVideo,
  isFileCsv, isFileGraphxr, isFileZip, isFileHtml, isFileJson, isInViewport, AllAccept, osfunction, OSType, isFileArrow } = require('../util/helpers.js');
const { file_actions_keys, ShortcutTitle, dataURItoBlob, saveLink, showToast,
  MSG, ModalType, ContentType, SETTINGS_NAME, showConfirm, copyContent, macMetaOrCtrl } = require("../util/utils");
const { SHAREDS_TYPE, UserStatus, ShareProjectCanOptions,
  MaxCanOption, DefaultShareProjectCanOption } = require("../util/MsgType");
const { FileObj, DirObj, rootDir, ExportType, getFileType, wrapContent,
  sortDirFileByKey, getDisplayDirObj, fileSeparator, editorjsTransferTo,
  getFolderKey, getFileKey, getFileName, getMdFile, openDirs, loopParentChecked,
  transferToRealData, readAsDataURL, fileNameReg, folderNameReg, FILTER_TYPE, getUri,
  assembleFileData } = require("./fileUtils");
const { mdDataFunc } = require("./translate")
const { DefaultInitData, imageDataFunc, DefaultData, checkContent, gzipDataFunc, dbDataFunc,
  videoDataFunc, audioDataFunc, graphxrDataFunc, zipDataFunc, excelDataFunc, arrowDataFunc } = require('../block/data');
const {
  getFileNamesKey, getItem, getUndefinedKey, getUndefinedContent, removeItem,
  setItem, DefaultFileData, DefaultFolderData, DefaultShareData,
  DEFAULT_VER, isNotLogin, ShapeShared, ShapeChatInfo, USER_NAME,
  USER_ID, notMyOwnProject, TAG_TYPE, ShapeChatData, COMMON, FILESDIR, getMasterUserId,
  EXPECT_PROJECT_IDS, PROJECT_USER_ID, getPROJECT_USER_NAME, getPROJECT_USER_EMAIL, PROJECT_ID, noSharedProjectIds, setWindowState, FILESDIR_NAME, getTaginfosKey, getTaginfosJson
} = require("../util/localstorage");
const { SubMenu } = Menu;
const MouseEnterDelay = 0.5;
let maxLength = 50 * 1024 * 1024;
export const importFilesAction = function (acceptedFiles, hideZeroError = false, selectPlace = true, referFileKey, load, cb) {
  if (0 === acceptedFiles.length) {
    !hideZeroError && showToast(MSG.NO_FILE_IMPORTED, "error")
    return;
  }
  for (let index = 0; index < acceptedFiles.length; index++) {
    if (acceptedFiles[index].size > maxLength) {
      showToast(`${acceptedFiles[index].name} is larger than ${formatSizeUnits(maxLength)}`, "error")
      return;
    }
  }
  actions.variable(actions.types.IMPORT_FILES, [], () => {
    return { selectPlace, acceptedFiles, referFileKey, load, cb };
  })
}

export default class MdFiles extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    mdfiles_transfer: PropTypes.shape({
      setLoading: PropTypes.func,
      fileNamesJson: PropTypes.object,
      cloud: PropTypes.object,
      uploadUri: PropTypes.string,
      settings: PropTypes.object,
      handleSearch: PropTypes.func,
      recordHistory: PropTypes.func,
      cacheInfo: PropTypes.object,
    }),
    userId: PropTypes.string,
    userName: PropTypes.string,
    email: PropTypes.string,
    projectName: PropTypes.string,
    projectId: PropTypes.string,
    fileKey: PropTypes.string,
    version: PropTypes.number,
    width: PropTypes.number,
  };

  static defaultProps = {};
  constructor(props) {
    super(props);
    const { className, userId, userName, email, projectId,
      mdfiles_transfer: { fileNamesJson, cloud, uploadUri, cacheInfo }, width } = this.props;
    this.state = {
      className: className,
      /**@type {CommonEditor} */
      currentEditor: editor,
      userId: userId,
      userName: userName,
      email: email,
      projectId: projectId,
      /**@type {Object.<string, DefaultFileData>} */
      fileNamesJson: fileNamesJson,
      /**@type {Object.<string, DefaultFileData>} */
      cloud: cloud,
      /**@type {string} */
      uploadUri: uploadUri,
      /**@type {DefaultShareData} */
      cacheInfo: cacheInfo,
      activeFolderKey: rootDir.folderKey,
      activeFileKey: null,
      fileVersion: DEFAULT_VER,
      rename: false,
      renameFileKey: null,
      newFile: false,
      newFolder: false,
      selectType: SelectType.dir,
      rightMenuVisible: false,
      modalType: ModalType.None,
      modalData: {},
      treeSelectType: SelectType.dir,
      source: '',
      /**@type{DefaultFolderData| DefaultFileData} */
      sourceFile: null,
      diffStrings: [],
      multipleChoice: false,
      initData: false,
      /**@type{DefaultFileData} */
      replaceFile: null,
      /**@type Object.<string, Object> */
      linkedFilesMap: {},
      /**@type Object.<string, Object.<string, ShapeShared>> ：<fileKey,<shared_id, {}>> */
      sharedFilesMap: {},
      sharedVisible: false,
      /**@type File */
      previewImg: null,
      previewImgVersion: 0,
      filter: false,
      filterType: FILTER_TYPE.links,
      notOwn: notMyOwnProject(),
      /**@type Object.<string, ShapeChatData> */
      projectShareUsers: {},
      shareProjectId: undefined,
      /**@type Object.<string,ShareProjectCanOptions> */
      usersSharePermission: editor.getSettings().getUsersSharePermission(),
      width: width,
    };
    /**@type{DefaultFolderData| DefaultFileData} */
    this.hoverFile = null;
    /**@type{DefaultFolderData| DefaultFileData} */
    this.rightMenuFile = null;
    this.ref = React.createRef();//ref current component root div
    this.focusActive = true;
    this.usersSharePermissionElement = document.createElement("div");
    this.downloadElement = document.createElement("div");
    this.downloadFileElement = document.createElement("div");
    this.importFilesElement = document.createElement("div");
    this.duplicateElement = document.createElement("div");
    this.renameElement = document.createElement("div");
    this.currentEditorElement = document.createElement("div");
  }

  componentWillUnmount() {
    actions.deleteCache(this.usersSharePermissionElement);
    actions.deleteCache(this.downloadElement);
    actions.deleteCache(this.downloadFileElement);
    actions.deleteCache(this.importFilesElement);
    actions.deleteCache(this.duplicateElement);
    actions.deleteCache(this.renameElement);
    actions.deleteCache(this.currentEditorElement);
  }

  componentDidMount() {
    editor.init({ data: DefaultInitData }, this, this.props.mdfiles_transfer.settings);
    editor.get().isReady
      .then(async (v) => {
        console.log('Editor.js is ready to work!');
        await this.afterEditorReady();
      }).catch((reason) => {
        console.log(`Editor.js initialization failed because of ${reason}`);
        showToast(reason, 'error');
      });
    actions.inspector(this.usersSharePermissionElement, [actions.types.USERS_SHARE_PERMISSION], (usersSharePermission) => {
      const { projectShareUsers } = this.state;
      _.each(projectShareUsers, (user, userId) => {
        (undefined !== usersSharePermission[userId]) && (user.status = usersSharePermission[userId]);
      })
      this.setState({ usersSharePermission, projectShareUsers })
    });
    actions.inspector(this.downloadElement, [actions.types.DOWNLOAD], (download) => {
      download && this.downloadZip();
    });
    actions.inspector(this.downloadFileElement, [actions.types.DOWNLOAD_FILE], (download) => {
      download && this.onExport(ExportType.GROVE);
    });
    actions.inspector(this.importFilesElement, [actions.types.IMPORT_FILES], ({ selectPlace, acceptedFiles, referFileKey, load, cb }) => {
      const { fileNamesJson, activeFolderKey, activeFileKey, selectType } = this.state;
      if (selectPlace) {
        this.setState({
          modalType: ModalType.ImportFiles,
          sourceFile: null,
          treeSelectType: SelectType.dir,
          modalData: {
            folderKey: activeFolderKey,
            acceptedFiles,
            cb: (referFileKey, decompression) => {
              this.onImport(acceptedFiles, referFileKey, load, decompression).then((fileNames) => {
                cb && cb(fileNames);
              });
            }
          },
        })
      } else {
        this.onImport(acceptedFiles, referFileKey, load).then((fileNames) => {
          cb && cb(fileNames);
        });
      }
    });
    actions.inspector(this.duplicateElement, [actions.types.DUPLICATE], (duplicate) => {
      this.openDuplicateDrawer();
    });
    actions.inspector(this.renameElement, [actions.types.RENAME], (rename) => {
      this.renameFileOrFolder(rename.file)
    });
    actions.inspector(this.currentEditorElement, [actions.types.CURRENT_EDITOR], /**@param {CommonEditor} currentEditor */(currentEditor) => {
      this.setState({ currentEditor }, () => {
        this.focusDrawer();
      })
    });
  }

  componentDidUpdate(preProps, preState) {
    const { className, userId, userName, email, projectId, width } = this.props;
    if (preProps.projectId !== projectId) {
      const { className, userId, userName, email, projectId,
        mdfiles_transfer: { fileNamesJson, cloud, uploadUri, settings } } = this.props;
      editor.getSettings().loadSettings(settings);
      // _.each(_.keys(this.state.fileNamesJson), (curr, index) => {
      //   removeGlobalScope(curr, curr)
      // })
      /**@type{DefaultFolderData| DefaultFileData} */
      this.hoverFile = null;
      /**@type{DefaultFolderData| DefaultFileData} */
      this.rightMenuFile = null;
      this.setState({
        className: className,
        userId: userId,
        userName: userName,
        email: email,
        projectId: projectId,
        /**@type {Object.<string, DefaultFileData>} */
        fileNamesJson: fileNamesJson,
        /**@type {Object.<string, DefaultFileData>} */
        cloud: cloud,
        /**@type {string} */
        uploadUri: uploadUri,
        /**@type {DefaultShareData} */
        // cacheInfo: cacheInfo,
        activeFolderKey: rootDir.folderKey,
        activeFileKey: null,
        fileVersion: DEFAULT_VER,
        rename: false,
        renameFileKey: null,
        newFile: false,
        newFolder: false,
        selectType: SelectType.dir,
        rightMenuVisible: false,
        modalType: ModalType.None,
        modalData: {},
        source: '',
        /**@type{DefaultFolderData| DefaultFileData} */
        sourceFile: null,
        diffStrings: [],
        multipleChoice: false,
        initData: false,
        /**@type{DefaultFileData} */
        replaceFile: null,
        /**@type Object.<string, Object> */
        // linkedFilesMap: {},
        /**@type Object.<string, Object.<string, ShapeShared>> ：<fileKey,<shared_id, {}>> */
        sharedFilesMap: {},
        sharedVisible: false,
        /**@type File */
        previewImg: null,
        previewImgVersion: 0,
        filter: false,
        filterType: FILTER_TYPE.links,
        notOwn: notMyOwnProject(),
        /**@type Object.<string, ShapeChatData> */
        projectShareUsers: {},
        shareProjectId: undefined,
        /**@type Object.<string,ShareProjectCanOptions> */
        usersSharePermission: editor.getSettings().getUsersSharePermission(),
        width: width,
      }, async () => {
        await this.afterEditorReady()
      });
      return;
    }
    let state = {};
    if (preProps.className !== className) {
      _.assign(state, { className });
    }
    if (preProps.userName !== userName) {
      _.assign(state, { userName });
    }
    if (preProps.email !== email) {
      _.assign(state, { email });
    }
    if (preProps.width !== width) {
      _.assign(state, { width });
    }
    !_.isEmpty(state) && this.setState(state);
  }

  removeShareds = async (fileKey, stateTmp) => {
    const { sharedFilesMap, projectId, shareProjectId, userId } = this.state;
    if (!sharedFilesMap[fileKey]) {
      return;
    }
    let promiseList = [];
    _.each(sharedFilesMap[fileKey], (value, _id) => {
      promiseList.push(axios.post(`/api/grove/removeShared`, {
        _id, shareProjectId, projectId, userId
      }));
    })
    await Promise.all(promiseList);
    let state = stateTmp || {};
    delete sharedFilesMap[fileKey];
    _.assign(state, { sharedFilesMap })
    !stateTmp && this.setState(state);
  }

  getLinkedFiles = async (stateTmp) => {
    let state = stateTmp || {};
    if (isNotLogin()) {
      return;
    }
    let res = await Util.sendRequest("/api/grovePinned/getWithProjectId", {
      projectId: Util.getProjectId()
    })
    let linkedFilesMap = {};
    (res.content || []).forEach(item => {
      linkedFilesMap[item.fileKey] = item;
    })
    _.assign(state, { linkedFilesMap })
    !stateTmp && this.setState(state);
  }

  getProjectShareUsers = async (stateTmp) => {
    let state = stateTmp || {};
    if (isNotLogin()) {
      return;
    }
    const masterUserId = getMasterUserId();
    const { projectId, userId, userName, email } = this.state;
    const { notOwn, usersSharePermission } = this.state;
    if (noSharedProjectIds(projectId)) {
      actions.variable(actions.types.PROJECT_SHARE_USERS, [], () => {
        return {};
      });
      return;
    }
    let res = await axios.post("/api/grove/getProjectShareUsers", {
      projectId: projectId
    });
    if (!res.data.status) {
      let projectShareUsers = _.reduce(res.data.content || [], (prev, item, index) => {
        prev[item.userId._id] = _.assign({}, ShapeChatData, {
          userId: item.userId._id, userName: `${item.userId.firstName} ${item.userId.lastName}`,
          email: item.userId.email, isMaster: false,
          userStatus: UserStatus.offline,
          status: usersSharePermission[item.userId._id] || DefaultShareProjectCanOption,
        })
        return prev;
      }, {})
      if (masterUserId === USER_ID) {
        projectShareUsers[userId] = {
          userId: userId,
          userName: userName,
          email: email,
          isMaster: true,
          userStatus: UserStatus.offline,
          status: MaxCanOption["Super"]
        }
      } else {
        let masterUserName, masterEmail;
        if (masterUserId === PROJECT_USER_ID) {
          masterUserName = getPROJECT_USER_NAME();
          masterEmail = getPROJECT_USER_EMAIL();
        } else {
          let res = await axios.post(`/api/grove/linkTo`, { fileKey: "", projectId: projectId, userId: masterUserId });
          if (!res.data.status) {
            masterUserName = res.data.userName;
            masterEmail = res.data.email;
          } else {
            masterUserName = UNKOWN;
            masterEmail = UNKOWN;
          }
        }
        projectShareUsers[masterUserId] = {
          userId: masterUserId,
          userName: masterUserName,
          email: masterEmail,
          isMaster: true,
          userStatus: UserStatus.offline,
          status: MaxCanOption["Super"]
        }
      }
      _.assign(state, { projectShareUsers })
      if (notOwn && !_.isEmpty(projectShareUsers) && ~_.keys(projectShareUsers).indexOf(USER_ID)) {
        _.assign(state, { shareProjectId: projectId });
      }
      !stateTmp && this.setState(state);
      actions.variable(actions.types.PROJECT_SHARE_USERS, [], () => {
        return projectShareUsers;
      });
    }
  }

  getSharedFiles = async (stateTmp) => {
    let state = stateTmp || {};
    const { projectId, userId } = this.state;
    if (isNotLogin()) {
      return;
    }
    let res = await axios.post("/api/grove/getAllShareds", {
      projectId: projectId,
      userId: userId,
      type: SHAREDS_TYPE.User_Shared_By_ProjectId
    });
    /**@type Object.<string, Object.<string, ShapeShared>> ：<fileKey,<shared_id, {}>> */
    let sharedFilesMap = {};
    (res.data.content || []).forEach(item => {
      if (!sharedFilesMap[item.fileKey]) {
        sharedFilesMap[item.fileKey] = {};
      }
      sharedFilesMap[item.fileKey][item._id] = item;
    })
    _.assign(state, { sharedFilesMap });
    !stateTmp && this.setState(state);
  }

  /**
   * 
   * @param {DefaultShareData} info
   */
  handleAddLinkedToGraphXR = (info) => {
    const { fileKey } = info;
    let { linkedFilesMap } = this.state;

    let params = new URLSearchParams();
    params.append("userId", info.userId)
    params.append("projectId", info.projectId)
    params.append("fileKey", info.fileKey)
    undefined !== info.projectName && params.append("projectName", info.projectName)
    undefined !== info.version && params.append("version", info.version)
    params.append("uploadUri", info.uploadUri)

    Util.sendRequest(`/api/grovePinned/add`, {
      projectId: Util.getProjectId(),
      item: {
        fileName: Util.getFileName(fileKey),
        fileKey,
        /* Don't serialize the url with the alias path because someday it could change */
        accessPath: `${Common.removeAliasPath(window.location.pathname)}?${params.toString()}`
      }
    }).then(res => {
      if (res.status === 0) {
        linkedFilesMap[fileKey] = res.content;
        updateGroveLinks();
      } else {
        console.error(res.message);
      }
      this.setState({ linkedFilesMap });
    })
  }

  /**
   * 
   * @param {*} fileKey 
   * @param {object} state transfer state that setState later or right now setState in the function
   */
  handleRemoveLinkedToGraphXR = async (fileKey, state) => {
    let { linkedFilesMap } = this.state;
    let linkeItem = linkedFilesMap[fileKey];
    if (!linkeItem) {
      return;
    }
    await Util.sendRequest(`/api/grovePinned/remove`, {
      id: linkeItem._id,
    }).then(res => {
      if (res.status === 0) {
        delete linkedFilesMap[fileKey];
        updateGroveLinks()
      } else {
        console.error(res.message);
      }
      if (state) {
        _.assign(state, { linkedFilesMap: linkedFilesMap });
      } else {
        this.setState({ linkedFilesMap: linkedFilesMap });
      }
    })
  }

  /**
   * 
   * @param {string} fileKey 
   * @param {Object.<string, ShapeShared>} shareds <shared_id, {}>
   * @param {*} cb 
   */
  refreshShareds = (fileKey, shareds, cb) => {
    const { sharedFilesMap } = this.state;
    if (!_.isEmpty(shareds)) {
      if (_.isEqual(sharedFilesMap[fileKey], shareds)) {
        return cb && cb();
      }
      sharedFilesMap[fileKey] = _.cloneDeep(shareds);
    } else {
      delete sharedFilesMap[fileKey];
    }
    let state = { sharedFilesMap };
    const { activeFileKey, fileNamesJson, fileVersion } = this.state;
    if (activeFileKey && activeFileKey === fileKey) {
      let fileData = fileNamesJson[activeFileKey];
      editor.setFileData(fileData, fileVersion, this.getChatData(fileData, state, fileVersion));
    }
    this.setState(state, cb);
  }

  /** 
   * @param {DefaultShareData} info
   * @param {boolean} history
   */
  handleSearch = async (info, history) => {
    const { mdfiles_transfer: { handleSearch } } = this.props;
    await handleSearch(info, history);
  }

  switchFileTagEditor = async (fileKeyOrData, fileVersion = DEFAULT_VER, content, shared, refresh = false, replaceEditor) => {
    const { mdfiles_transfer: { switchFileTagEditor } } = this.props;
    await switchFileTagEditor(fileKeyOrData, fileVersion, content, shared, refresh, replaceEditor);
  }
  switchLinkTagEditor = async (linkData, refresh, replaceEditor) => {
    const { mdfiles_transfer: { switchLinkTagEditor } } = this.props;
    await switchLinkTagEditor(linkData, refresh, replaceEditor);
  }
  closeTagEditor = async (tagEditor, stateTmp) => {
    const { mdfiles_transfer: { closeTagEditor } } = this.props;
    await closeTagEditor(tagEditor, stateTmp);
  }
  loadTaginfos = async (taginfos) => {
    const { mdfiles_transfer: { loadTaginfos } } = this.props;
    await loadTaginfos(taginfos);
  }

  /**
   * 
   * @param {{
   *  fileKey:string,
   *  version:number,
   *  mtimeMs: number,
   *  cache:{mtimeMs: number},
   *  tag:TAG_TYPE}} obj 
   */
  getInfo = (obj) => {
    const { sharedFilesMap, uploadUri, userId, userName, email, projectId } = this.state;
    let sharedFile = (!obj.projectId || obj.projectId === projectId) ? _.find(sharedFilesMap[obj.fileKey], (sharedUser, key) => {
      return sharedUser.toUserId._id === USER_ID;
    }) : undefined;
    return _.assign({}, DefaultShareData, {
      userId: userId,
      userName: userName,
      email: email,
      projectId: projectId,
      projectName: this.props.projectName,
      fileKey: undefined,
      version: undefined,
      uploadUri: uploadUri,
      sharedId: sharedFile && sharedFile._id,
    }, obj)
  }

  /**
   * 
   * @param {Boolean} loading 
   * @param {Function} cb callback
   */
  setWrapperLoading = (loading, cb, loadingClosable = false) => {
    actions.variable(actions.types.LOADING, [], () => {
      return { loading, loadingClosable, cb };
    });
  }

  afterEditorReady = async () => {
    const { fileKey, version } = this.props;
    const { fileNamesJson, cacheInfo, notOwn } = this.state;
    let selectedFileKeyTmp, fileVersionTmp, content;
    let state = {};
    // await this.getLinkedFiles(state);
    await this.getProjectShareUsers(state);
    await this.getSharedFiles(state);
    if (cacheInfo) {
      this.setState(_.assign(state, { initData: true, cacheInfo: undefined }), () => {
        this.callback();
        this.handleSearch(cacheInfo, false);
      })
      return;
    }
    if (!fileKey) {
      Object.keys(fileNamesJson).forEach((tmpfileKey) => {
        if (fileNamesJson[tmpfileKey].selected) {
          selectedFileKeyTmp = tmpfileKey;
          fileVersionTmp = fileNamesJson[tmpfileKey].selectedVersion || DEFAULT_VER;
        }
      })
    } else {
      Object.keys(fileNamesJson).forEach((tmpfileKey) => {
        if (fileKey !== tmpfileKey) {
          fileNamesJson[tmpfileKey].selected = false;
        } else {
          fileNamesJson[tmpfileKey].selected = true;
          selectedFileKeyTmp = tmpfileKey;
          fileVersionTmp = version;
        }
      })
      state.fileNamesJson = fileNamesJson;
    }
    if (notOwn) {
      if (undefined === selectedFileKeyTmp) {
        await editor.value(getUndefinedContent(), ContentType.File);
        this.setState(_.assign(state, { initData: true }), this.callback);
      } else {
        this.setState(_.assign(state, { initData: true }), () => {
          this.callback();
          let file = fileNamesJson[selectedFileKeyTmp];
          this.handleSearch(this.getInfo({
            fileKey: selectedFileKeyTmp, version: fileVersionTmp,
            mtimeMs: file.mtimeMs,
            cache: this.isNeedUpload(file) ? {} : { mtimeMs: file },
            tag: TAG_TYPE.projectShare,
            userId: PROJECT_USER_ID,
            userName: getPROJECT_USER_NAME(),
            email: getPROJECT_USER_EMAIL(),
          }), false);
        })
      }
    } else {
      if (undefined === selectedFileKeyTmp) {
        await editor.value(getUndefinedContent(), ContentType.File);
        this.setState(_.assign(state, { initData: true }), this.callback);
      } else {
        let fileData = fileNamesJson[selectedFileKeyTmp];
        content = await this.getFileContent(fileData, fileVersionTmp)
        await this.loadNewFile(content, fileData, fileVersionTmp, this.getChatData(fileData, state, fileVersionTmp));
        this.setState(
          _.assign(state, {
            activeFileKey: selectedFileKeyTmp, fileVersion: fileVersionTmp,
            selectType: SelectType.file, initData: true
          }),
          this.callback);
      }
    }

  }

  /**
   * 
   * @param {*} fuzzyFileKey find file startsWith
   * @param {[]} fileKeys files
   * @param {number} version
   * @param {ExportType} type
   * @param {Object} objTmp
   */
  downloadZip = (fuzzyFileKey, fileKeys, version, type = ExportType.GROVE, objTmp, fileNameTmp) => {
    const { fileNamesJson } = this.state;
    this.setWrapperLoading(true);
    let obj = objTmp || {};
    let func = async (file, tmpfileKey, version) => {
      if (file.type === DefaultFolderData.type) {
        return;
      }
      let text = await this.getRealFileContent(file, version);
      obj[tmpfileKey] = isFileEditorJs(file) ? editorjsTransferTo(text, type) : text;
    }
    let promises = [];
    _.each(fileNamesJson, /**@param {DefaultFileData} file   */(file, tmpfileKey) => {
      if ((fuzzyFileKey === undefined && fileKeys === undefined) ||
        (fuzzyFileKey !== undefined && tmpfileKey.startsWith(fuzzyFileKey)) ||
        (fileKeys !== undefined && ~fileKeys.indexOf(tmpfileKey))) {
        promises.push(func(file, tmpfileKey, version));
      }
    })
    Promise.all(promises).then(results => {
      let graphxrZip = new JSZip();
      _.each(obj, (item, tmpfileKey) => {
        if (isFileEditorJs(fileNamesJson[tmpfileKey])) {
          graphxrZip.file(tmpfileKey.endsWith(`.${type}`) ? tmpfileKey : `${tmpfileKey}.${type}`, item)
        } else {
          graphxrZip.file(tmpfileKey, item)
        }
      })
      let compressionOption = {
        compression: "DEFLATE",
        compressionOptions: {
          level: 9 // force a compression and a compression level for this file
        }
      };
      graphxrZip.generateAsync({ type: "blob", ...compressionOption })
        .then((blob) => {
          saveLink(URL.createObjectURL(blob),
            fileNameTmp || `notebook.${moment(new Date()).format("YYYY-MM-DD HH-mm-ss")}.zip`);
          this.setWrapperLoading(false);
          showToast("Save zip successful!");
        }, (error) => {
          this.setWrapperLoading(false);
          showToast("Failed to save zip", 'error');
        });
    });
  }

  /**
   * cb
   */
  callback = () => {
    const { fileNamesJson, activeFileKey } = this.state;
    openDirs(this.state.activeFileKey);
    editor.focusFolder();
    actions.variable(actions.types.READY, [], () => {
      console.log("ready");
      return true;
    });
    this.loadTaginfos(getTaginfosJson()).then(() => {
      this.setWrapperLoading(false);
    })
  }

  /**
   *  setState of fileNamesJson
   */
  setFileNamesJsonState = () => {
    const { fileNamesJson } = this.state;
    this.setState({ fileNamesJson: fileNamesJson });
  }

  /**
   * save fileNamesJson to storage
   */
  setFileNamesJsonItem = () => {
    const { fileNamesJson } = this.state;
    setItem(getFileNamesKey(), JSON.stringify(fileNamesJson));
    // _.each(_.keys(fileNamesJson), (curr, index) => {
    //   addGlobalScope(curr, curr);
    // })
    return fileNamesJson;
  }

  /**
   * get file content from storage/ImageCache/server
   * @param {typeof DefaultFileData} file 
   * @returns null if folder or blob uri string if image or string or throw exception if fileName not exist in cache or network exception
   */
  async getFileContent(file, version) {
    const { uploadUri, cloud } = this.state;
    if (file.type === DefaultFolderData.type) {
      return null;
    }
    return getMdFile(uploadUri, cloud, file, false, version);
  }

  /**
     * get file content from storage/ImageCache/server
     * @param {typeof DefaultFileData} file 
     * @param {number} version
     * @returns null if folder or Blob if image or string or throw exception if fileName not exist in cache or network exception
     */
  async getRealFileContent(file, version) {
    if (file.type === DefaultFolderData.type) {
      return null;
    }
    let item = await this.getFileContent(file, version);
    if (isFileNoEdit(file)) {
      let response = await fetch(item);
      let blob = await response.blob();
      item = blob;
    }
    return item;
  }

  getFileData(fileKey) {
    const { fileNamesJson } = this.state;
    let file = fileNamesJson[fileKey];
    return file;
  }
  /**
     * get file content from storage/ImageCache/server
     * @param {string} fileKey
     * @param {number} version
     * @returns {string|Blob} return null if folder or Blob if image or string or throw exception if fileName not exist in cache or network exception
     */
  async getRealFileContentByFileKey(fileKey, version) {
    const { fileNamesJson } = this.state;
    let file = fileNamesJson[fileKey];
    if (file === undefined) {
      return null;
    }
    return await this.getRealFileContent(file, version);
  }

  /**
   * save current file then export it
   * @param {*} type 
   */
  onExport = async (type = ExportType.GROVE) => {
    const { activeFileKey, fileVersion, fileNamesJson } = this.state;
    if (activeFileKey === null) {
      showToast("Please select a file", 'warning');
      return;
    }
    const state = {};
    await editor.saveCurrentFile(state);
    await this.exportFile(activeFileKey, fileVersion, type);
    !_.isEmpty(state) && this.setState(state);
  }

  /**
   * export editorjs file to type
   * 
   * @param {string} activeFileKey 
   * @param {number} version 
   * @param {ExportType} type 
   */
  exportFile = async (activeFileKey, version, type = ExportType.GROVE) => {
    const { fileNamesJson, uploadUri, projectId } = this.state;
    /**
     * @type DefaultFileData
     */
    let file = fileNamesJson[activeFileKey];
    this.setWrapperLoading(true);
    let markdown = await this.getRealFileContent(file, version);
    if (isFileNoEdit(file)) {
      saveLink(URL.createObjectURL(markdown), file.name);
    } else {
      if (isFileEditorJs(file)) {
        let item = Object.assign({}, DefaultShareData, {
          userName: USER_NAME,
          userId: USER_ID,
          projectId: projectId,
          projectName: this.props.projectName,
          fileKey: file.fileKey,
          version: version,
          uploadUri: uploadUri
          // status: 0,
          // createTime: "",
          // tag: TAG_TYPE.shared,
          // sharedId: "string;"
          // email: "string;"
        });
        let res = await axios.post(`/api/grove/linkTo`, item);
        if (!res.data.status) {
          if (res.data.content instanceof Object) {
            item.attachments = res.data.content;
          }
        }
        let files = new Set();//attachments
        markdown = await copyShareFile(item, files, markdown);
        if (files.size > 0) {
          let objTmp = {};
          objTmp[file.fileKey] = isFileEditorJs(file) ? editorjsTransferTo(markdown, type) : markdown;;
          return this.downloadZip(undefined, Array.from(files), DEFAULT_VER, type, objTmp, `${getFileName(file.fileKey)}.zip`);
        }
      }
      let dataStr = `data:${isFileEditorJs(file) ?
        `text/${type}` :
        file.type};charset=utf-8,${encodeURIComponent(isFileEditorJs(file) ?
          editorjsTransferTo(markdown, type) :
          markdown)}`;
      let downloadAnchorNode = document.createElement("a");
      downloadAnchorNode.setAttribute("href", dataStr);
      downloadAnchorNode.setAttribute("download", isFileEditorJs(file) ?
        (file.name.endsWith(`.${type}`) ? file.name : (`${file.name}.${type}`)) :
        file.name);
      document.body.appendChild(downloadAnchorNode);
      downloadAnchorNode.click();
      // downloadAnchorNode.remove();
    }
    this.setWrapperLoading(false)
  }

  /**
   * parse memory file data
   * @param {File} file 
   * @param {DefaultFileData} fileData 
   */
  async parse(file, fileData) {
    // Always return a Promise
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      // Wait till complete
      reader.onloadend = function (e) {
        let content = e.target.result;
        if (isFileMd(fileData)) {
          content = JSON.stringify(mdDataFunc(mdTransferToEditorjs(content), null, 1));
        }
        resolve(content);
      };
      // Make sure to handle error states
      reader.onerror = function (e) {
        reject(e);
      };
      if (isFileNoEdit(fileData)) {
        reader.readAsDataURL(file);
      } else {
        const reader2 = new FileReader();
        reader2.onloadend = function (e) {
          let csvResult = e.target.result.split(/\r|\n|\r\n/);
          reader.readAsText(file, jschardet.detect(csvResult.toString()).encoding);
        };
        // Make sure to handle error states
        reader2.onerror = function (e) {
          reject(e);
        };
        reader2.readAsBinaryString(file);
      }
    });
  }

  /**
   * 
   * @param {*} folder folder data
   * @param {*} setLoading set loading false during upload
   * @param {*} state transfer state that setState later or right now setState in the function
   * @returns 
   */
  uploadFolderFunc = async (folder, setLoading, state) => {
    const { userId, projectId, shareProjectId } = this.state;
    let res = await axios.post(`/api/grove/uploadFolder`, {
      folderKey: folder.folderKey,
      folderName: folder.name,
      userId: userId,
      projectId: projectId,
      shareProjectId: shareProjectId,
    });
    if (!res.data.status) {
      const { cloud, fileNamesJson } = this.state;
      cloud[folder.folderKey] = _.cloneDeep(res.data.content);
      fileNamesJson[folder.folderKey] !== undefined ?
        _.assign(fileNamesJson[folder.folderKey], res.data.content) :
        (fileNamesJson[folder.folderKey] = res.data.content);
      state !== undefined ?
        _.assign(state, { cloud: cloud, fileNamesJson: this.setFileNamesJsonItem() }) :
        this.setState({ cloud: cloud, fileNamesJson: this.setFileNamesJsonItem() });
    }
    res.data.message && showToast(res.data.message, res.data.status ? "error" : "success");
    setLoading && setLoading(false);
    return !res.data.status;
  }

  /**
   * upload file to server
   * @param {DefaultFileData} file upload file data
   * @param {function} setLoading set loading false during upload
   * @param {string|blob} content transfer content need upload , get from storage if it's undefined/null
   * @param {object} stateTmp transfer state that setState later or right now setState in the function
   * @param {number} version 
   * @param {boolean} largeCache
   * @return return boolean means success/fail 
   */
  uploadMdFile = async (file, setLoading, content, stateTmp, version = DEFAULT_VER, largeCache = false, decompression = false) => {
    const { notOwn } = this.state;
    if (notOwn && file.fileKey === SETTINGS_NAME) {
      setLoading && setLoading(false);
      showToast(MSG.CAN_NOT_MODIFY_SETTINGS, "warning");
      return false;
    }
    if (version !== DEFAULT_VER) {
      setLoading && setLoading(false);
      showToast("Cannot upload version <> 0 file!", "error");
      return false;
    }
    let fd = new FormData();
    fd.append("fileKey", file.fileKey);
    fd.append("fileName", file.name);
    fd.append("unzip", decompression);
    content = (content !== undefined && content !== null) ? content : getItem(getFileNamesKey() + '.' + file.fileKey);
    if (undefined === content || null === content) {
      showToast("Failed to save file.", "error")
      return false;
    }
    const { userId, projectId } = this.state;
    let data;
    if (isFileNoEdit(file)) {
      if (!(content instanceof Blob)) {
        data = dataURItoBlob(content)
      } else {
        data = content;
      }
    } else {
      if (largeCache && content.length > window.currentEditor.getAutoUploadMaxSize()) {
        document.querySelectorAll(".fa-cloud-upload-alt").forEach((value) => {
          value.classList.add("shake");
        })
        showToast(`Cannot auto-save large files. Please save manually.`);
        return false;
      }
      data = new Blob([content], { type: file.type })
    };
    fd.append('data', data);
    fd.append('userId', userId);
    fd.append('projectId', projectId);
    this.state.shareProjectId && fd.append('shareProjectId', this.state.shareProjectId);
    let shareData = this.getChatData(file, undefined, version);
    shareData && fd.append('roomId', `${this.state.uploadUri}${file.fileKey}`);
    let res = await axios.post(`/api/grove/uploadFile`, fd);
    if (!res.data.status) {
      const { cloud, fileNamesJson } = this.state;
      cloud[file.fileKey] = _.cloneDeep(res.data.content);
      //NOTE: shareData not overwrite here
      fileNamesJson[file.fileKey] !== undefined ?
        _.assign(fileNamesJson[file.fileKey], res.data.content) :
        (fileNamesJson[file.fileKey] = res.data.content);
      removeItem(getFileNamesKey() + '.' + file.fileKey);
      if (res.data.gzip) {
        _.assign(cloud, _.cloneDeep(res.data.gzip));
        _.assign(fileNamesJson, _.cloneDeep(res.data.gzip));
      }
      let state = stateTmp || {};
      _.assign(state, { cloud: cloud, fileNamesJson: this.setFileNamesJsonItem() })
      !stateTmp && this.setState(state);
      if (window.currentEditor.getFileData() === file) {
        window.currentEditor.refreshFileData(file, fileNamesJson[file.fileKey]);
        window.currentEditor.setNeedSave(false);
        if (isFileCsv(file)) {
          const { _cachedata, _cachedata: { inspectors, variables } } = window.currentEditor.getMain();
          let variable = variables["text"]
          variable.varia.define(variable.name, variable.args, () => {
            return content;
          });
        }
      }
    }
    res.data.message = (file.name === SETTINGS_NAME) ?
      (`Save ${SETTINGS_NAME} ${res.data.status ? "error" : "success"}`) :
      res.data.message;
    res.data.message && showToast(res.data.message, res.data.status ? "error" : "success");
    if (res.data.status) {
      console.error(res.data);
    }
    setLoading && setLoading(false);
    return !res.data.status;
  }

  /**
   * copy file to another project
   * @param {DefaultFileData} file 
   * @param {string} userId 
   * @param {string} projectId 
   * @param {string} shareProjectId 
   * @param {function} setLoading 
   * @param {string|blob} content 
   * @param {{}} stateTmp 
   * @param {number} version 
   * @param {boolean} largeCache 
   */
  copyFile = async (file, userId, projectId, shareProjectId,
    setLoading, content, version = DEFAULT_VER, largeCache = false) => {
    /**@type {MdFiles}*/
    let fd = new FormData();
    fd.append("fileKey", file.fileKey);
    fd.append("fileName", file.name);
    content = (content !== undefined && content !== null) ? content : getItem(getFileNamesKey() + '.' + file.fileKey);
    if (undefined === content || null === content) {
      showToast("upload error!", "error")
      return false;
    }
    let data;
    if (isFileNoEdit(file)) {
      if (!(content instanceof Blob)) {
        data = dataURItoBlob(content)
      } else {
        data = content;
      }
    } else {
      if (largeCache && content.length > editor.getAutoUploadMaxSize()) {
        document.querySelectorAll(".fa-cloud-upload-alt").forEach((value) => {
          value.classList.add("shake");
        })
        showToast(`can not auto upload large file`);
        return false;
      }
      data = new Blob([content], { type: file.type })
    };
    fd.append('data', data);
    fd.append('userId', userId);
    fd.append('projectId', projectId);
    shareProjectId && fd.append('shareProjectId', shareProjectId);
    let res = await axios.post(`/api/grove/uploadFile`, fd);
    if (!res.data.status) {
      removeItem(getFileNamesKey() + '.' + file.fileKey);
    }
    res.data.message = (file.name === SETTINGS_NAME) ?
      (`Save ${SETTINGS_NAME} ${res.data.status ? "error" : "success"}`) :
      res.data.message;
    res.data.message && showToast(res.data.message, res.data.status ? "error" : "info");
    setLoading && setLoading(false);
    return !res.data.status;
  }

  /**
   * copy folder to another place
   * @param {*} fromFolderKey 
   * @param {*} fromShareProjectId 
   * @param {*} fromUserId 
   * @param {*} fromProjectId 
   * @param {*} toFolderKey 
   * @param {*} toShareProjectId 
   * @param {*} toUserId 
   * @param {*} toProjectId 
   * @param {*} setLoading 
   * @param {*} stateTmp 
   */
  copyFolder = async (fromFolderKey, fromShareProjectId, fromUserId, fromProjectId,
    toFolderKey, toShareProjectId, toUserId, toProjectId,
    setLoading, stateTmp) => {
    /**@type {MdFiles}*/
    let fd = new FormData();
    fd.append("fromFolderKey", fromFolderKey);
    fromShareProjectId && fd.append("fromShareProjectId", fromShareProjectId);
    fd.append("fromUserId", fromUserId);
    fd.append("fromProjectId", fromProjectId);
    fd.append("toFolderKey", toFolderKey);
    toShareProjectId && fd.append("toShareProjectId", toShareProjectId);
    fd.append("toUserId", toUserId);
    fd.append("toProjectId", toProjectId);
    let res = await axios.post(`/api/grove/copyFolder`, fd);
    if (!res.data.status) {
      let state = stateTmp || {};
      const { projectId, cloud, fileNamesJson } = this.state;
      if (toProjectId === projectId) {
        _.map(res.data.content, (value, key) => {
          cloud[key] = _.cloneDeep(value);
          fileNamesJson[key] = value;
        })
        !stateTmp && this.setState(state);
      }
    }
    res.data.message && showToast(res.data.message, res.data.status ? "error" : "info");
    setLoading && setLoading(false);
    return !res.data.status;
  }

  updateFileSettings = async (file, setLoading, previewImageUpload, stateTmp) => {
    const { userId, projectId } = this.state;
    let fd = new FormData();
    fd.append("fileKey", file.fileKey);
    fd.append("fileName", file.name);
    fd.append('userId', userId);
    fd.append('projectId', projectId);
    fd.append('previewImageUpload', previewImageUpload);
    this.state.shareProjectId && fd.append('shareProjectId', this.state.shareProjectId);
    let res = await axios.post(`/api/grove/updateFileSettings`, fd);
    if (!res.data.status) {
      const { cloud, fileNamesJson } = this.state;
      fileNamesJson[file.fileKey].previewImageUpload = previewImageUpload;
      cloud[file.fileKey].previewImageUpload = previewImageUpload;
      let state = stateTmp || {};
      _.assign(state, { cloud: cloud, fileNamesJson: this.setFileNamesJsonItem() })
      !stateTmp && this.setState(state);
    }
    res.data.message && showToast(res.data.message, res.data.status ? "error" : "success");
    setLoading && setLoading(false);
    return !res.data.status;
  }

  /**
   * after fork need refresh files
   * @param {*} files forked files
   * @param {*} fileKey main file need to be open after refresh
   */
  refreshFiles = async (files, fileKey) => {
    const { cloud, fileNamesJson, activeFileKey, fileVersion } = this.state;
    _.assign(cloud, _.cloneDeep(files));
    _.assign(fileNamesJson, _.cloneDeep(files));
    let state = { cloud: cloud, fileNamesJson: this.setFileNamesJsonItem() };
    if (files[activeFileKey] && fileVersion === DEFAULT_VER) {
      let content = await this.getFileContent(fileNamesJson[activeFileKey], fileVersion)
      await this.loadNewFile(content, fileNamesJson[activeFileKey], fileVersion);
    }
    this.setState(state, async () => {
      await this.switchFileByFileKey(fileKey, undefined, DEFAULT_VER);
      this.focusDrawer();
    })
  }

  refreshFile = async (fileData, version, stateTmp) => {
    let state = stateTmp || {}
    const { fileNamesJson, cloud, userId, projectId } = this.state;
    if (!fileData) {
      return showToast(`can not find file ${fileData}`, "error");
    }
    let openEditor = this.getOpenEditor(fileData.fileKey)
    if (!openEditor) {
      return;
    }
    let res = await axios.post(`/api/grove/linkTo`, _.assign({}, fileData,
      { userId: userId, projectId: projectId }));
    if (!res.data.status) {
      let files = res.data.content;
      _.assign(cloud, _.cloneDeep(files));
      _.assign(fileNamesJson, _.cloneDeep(files));
      _.assign(state, { cloud: cloud, fileNamesJson: this.setFileNamesJsonItem() });
      let content = await this.getFileContent(fileData, version)
      if (openEditor.isCommon()) {
        // await this.loadNewFile(content, fileData, version);
        const { uploadUri } = this.state;
        content = wrapContent(content, fileData, uploadUri);
        // editor.setFileData(fileData, version, chatData || this.getChatData(fileData, undefined, version));
        editor.refreshFileData(editor.getFileData(), fileData);
        await openEditor.value(content, ContentType.File, version);
      } else {
        content = wrapContent(content, fileData, openEditor.getUploadUri());
        await openEditor.value(content, version);
      }
      !stateTmp && this.setState(state);
    }
  }

  uploadScreenshot = async () => {
    const { userId, projectId } = this.state;
    let fd = new FormData();
    let fileName = editor.fileData.fileKey.replace(new RegExp(`[\\${fileSeparator}]`, "g"), "-") + ".jpg";
    fd.append("fileKey", `screenshot${fileSeparator}${fileName}`);
    fd.append("fileName", fileName);
    let content = await editor.getScreenshot();
    if (content === undefined) return;
    fd.append('data', dataURItoBlob(content));
    fd.append('userId', userId);
    fd.append('projectId', projectId);
    this.state.shareProjectId && fd.append('shareProjectId', this.state.shareProjectId);
    await axios.post(`/api/grove/uploadFile`, fd);
  }

  /**
   * 
   * @param {File[]} files 
   * @param {*} referFileKey 
   * @param {boolean} load need load last imported file
   * @param {boolean} decompression need decompression zip/tgz file
   */
  async onImport(files = [], referFileKey, load, decompression = false) {
    if (!files || 0 === files.length) {
      return;
    }
    const { fileNamesJson, activeFileKey, activeFolderKey, selectType } = this.state;
    referFileKey = undefined !== referFileKey ? referFileKey : (selectType === SelectType.dir ? activeFolderKey : activeFileKey);
    /** only for file **/
    let content = undefined,
      fileName = undefined,
      fileKey = undefined;
    let itemsCache = {};
    /** only for folder **/
    let folderName = undefined,
      folderKey = undefined;
    let cache = {};
    let errorFiles = [];
    const OverWriteStatus = {
      none: "none",
      all: "all",
      each: "each"
    }
    let overwritestatus = OverWriteStatus.each;
    let explain = async (zip) => {
      const promiseList = _.map(_.keys(zip.files), function (zipFileKey) {
        let file = zip.files[zipFileKey];
        if (file.dir) {
          return [
            zipFileKey,
            null,
            DefaultFolderData.type
          ]
        } else {
          let type = getFileType(zipFileKey)
          if (isFileNoEdit({ type })) {
            return file.async('base64').then(function (base64) {
              return [
                zipFileKey,
                `data:${type};base64,${base64}`,
                type
              ]
            })
          } else {
            return file.async('string').then(function (string) {
              return [
                zipFileKey.endsWith(".grove") ? zipFileKey.substring(0, zipFileKey.length - ".grove".length) : zipFileKey,
                string,
                type
              ]
            })
          }
        }
      })
      let values = await Promise.all(promiseList);
      for (let index = 0; index < values.length; index++) {
        let value = values[index];
        let type = value[2];
        if (type === DefaultFolderData.type) {
          let tmpFolderKey = getFileKey(referFileKey, value[0]);
          if (fileNamesJson[tmpFolderKey]) {
            continue;
          }
          let erazerLastChar = tmpFolderKey.substring(0, tmpFolderKey.length - 1);
          folderKey = tmpFolderKey,
            folderName = erazerLastChar.substring(erazerLastChar.lastIndexOf(fileSeparator));
          cache[folderKey] = Object.assign({}, DefaultFolderData,
            { folderKey: folderKey, name: folderName, selected: false, type: type });
        } else {
          let tmpFileKey = getFileKey(referFileKey, value[0]);
          let tmpContent = value[1];
          let okHandler = () => {
            content = tmpContent,
              fileKey = tmpFileKey,
              fileName = tmpFileKey.substring(tmpFileKey.lastIndexOf(fileSeparator) + 1);
            type = transferType(type);
            let fileData = Object.assign({}, DefaultFileData,
              { fileKey: fileKey, name: fileName, selected: false, type: type });
            if (isFileMd(fileData)) {
              content = JSON.stringify(mdDataFunc(mdTransferToEditorjs(content), null, 1));
            }
            if (checkContent(fileData, content)) {
              itemsCache[fileKey] = content;
              cache[fileKey] = fileData;
            } else {
              errorFiles.push(fileKey);
            }
          }
          if (fileNamesJson[tmpFileKey] !== undefined) {
            if (overwritestatus === OverWriteStatus.none) {
              continue;
            } else if (overwritestatus === OverWriteStatus.each) {
              await showConfirm(<div>
                <div>{`Overwrite ${tmpFileKey}?`}</div>
                {values.length > 1 && <div><Button
                  key="overwrite_all"
                  type="primary"
                  onClick={() => {
                    overwritestatus = OverWriteStatus.all;
                    okHandler();
                    window._currentModal && (window._currentModal.resolveFunc(true), window._currentModal.destroy());
                  }}
                >
                  Overwrite All
                </Button>&nbsp;
                  <Button
                    key="skip_all"
                    type="primary"
                    onClick={() => {
                      overwritestatus = OverWriteStatus.none;
                      window._currentModal && (window._currentModal.resolveFunc(true), window._currentModal.destroy());
                    }}
                  >
                    Skip All
                  </Button></div>}
              </div>, okHandler);
            } else if (overwritestatus === OverWriteStatus.all) {
              okHandler();
            }
          } else {
            okHandler();
          }
        }
      }
    }
    for (let index = 0; index < files.length; index++) {
      let file = files[index];
      if (!file) {
        continue;
      }
      if (file.type === "application/zip" && decompression) {
        // 1) read the Blob
        await JSZip.loadAsync(file, {
          decodeFileName: function (bytes) {
            return iconv.decode(bytes, 'gb2312');
          }
        })
          .then(explain, function (e) {
            console.error(e);
            showToast(`GraphXR unrecognized file(${file.name})`, 'error');
          });
      } else {
        let newFileData = assembleFileData(referFileKey, file);
        let tmpContent = await this.parse(file, newFileData);
        let okHandler = () => {
          content = tmpContent;
          fileKey = newFileData.fileKey;
          fileName = newFileData.name;
          let fileData = Object.assign({}, DefaultFileData,
            { fileKey: fileKey, name: fileName, selected: false, type: newFileData.type });
          if (checkContent(fileData, content)) {
            itemsCache[fileKey] = content;
            cache[fileKey] = fileData;
          } else {
            errorFiles.push(fileKey);
          }
        }
        if (fileNamesJson[newFileData.fileKey] !== undefined) {
          if (overwritestatus === OverWriteStatus.none) {
            continue;
          } else if (overwritestatus === OverWriteStatus.each) {
            await showConfirm(<div><div>{`Overwrite ${newFileData.fileKey}?`}</div>
              {files.length > 1 && <div><Button
                key="overwrite_all"
                type="primary"
                onClick={() => {
                  overwritestatus = OverWriteStatus.all;
                  okHandler();
                  window._currentModal && (window._currentModal.resolveFunc(true), window._currentModal.destroy());
                }}
              >
                Overwrite All
              </Button>&nbsp;
                <Button
                  key="skip_all"
                  type="primary"
                  onClick={() => {
                    overwritestatus = OverWriteStatus.none;
                    window._currentModal && (window._currentModal.resolveFunc(true), window._currentModal.destroy());
                  }}
                >
                  Skip All
                </Button></div>}
            </div>, okHandler);
          } else if (overwritestatus === OverWriteStatus.all) {
            okHandler();
          }
        } else {
          okHandler();
        }
      }
    }
    if (errorFiles.length) {
      showToast(<pre>{`These files are not supported: ${errorFiles.join("\n")}`}</pre>, "error");
    }
    if (_.keys(cache).length === 0) {
      showToast(MSG.NO_FILE_IMPORTED, "error");
      return;
    }
    let state = {};
    let promiseList = [];
    return await new Promise((resolve, reject) => {
      this.setWrapperLoading(true, async () => {
        _.each(_.keys(cache), (fileKey) => {
          let file = cache[fileKey];
          let contentTmp = itemsCache[fileKey];
          if (file.type === DefaultFolderData.type) {
            promiseList.push(this.uploadFolderFunc(file, undefined, state))
          } else {
            promiseList.push(this.uploadMdFile(file, undefined, contentTmp, state, DEFAULT_VER, undefined, decompression));
          }
        })
        await Promise.all(promiseList).then(async (values) => {
          _.each(_.keys(cache), (fileKey, index) => {
            if (!values[index]) {
              console.error(fileKey + " haven't uploaded!")
            }
          })
          if (fileKey !== undefined && fileNamesJson[fileKey]) {
            let openFileKey = fileKey, tmpkey;
            if (files.length && isFileGzip(files[0]) && fileNamesJson[tmpkey = `${fileKey.substring(0, fileKey.length - '.tgz'.length)}${fileSeparator}${fileNamesJson[fileKey].name.substring(0, fileNamesJson[fileKey].name.length - '.tgz'.length)}`]) {
              openFileKey = tmpkey;
              const { uploadUri, cloud } = this.state;
              content = await getMdFile(uploadUri, cloud, fileNamesJson[openFileKey], true);
            }
            if (undefined === load) {
              await this.switchToPage(openFileKey, content, state);
            }
          }
          this.setState(state, () => {
            load && editor.focusFolder();
          });
          this.setWrapperLoading(false);
          resolve(_.map(cache, (file, key) => {
            return file.name;
          }));
          if (window.currentEditor.getFileData() && referFileKey.startsWith(getFolderKey(window.currentEditor.getFileData().fileKey, FILESDIR_NAME))) {
            actions.variable(actions.types.REFRESH_FILE_ATTACHMENTS, [], () => { })
          }
        });
      })
    })
  }

  switchToPage = async (openFileKey, content, stateTmp) => {
    let state = stateTmp || {};
    const { fileNamesJson, uploadUri, cloud } = this.state;
    if (await showConfirm(`Switch to page ${openFileKey}?`, { okText: "OK", cancelText: "NO" })) {
      if (undefined === content) {
        content = await getMdFile(uploadUri, cloud, fileNamesJson[openFileKey], true);
      }
      await editor.saveCurrentFile(state);
      _.keys(fileNamesJson).forEach((tmpfileKey) => {
        if (openFileKey !== tmpfileKey) {
          fileNamesJson[tmpfileKey].selected = false;
        } else {
          fileNamesJson[tmpfileKey].selected = true;
        }
      })
      await this.loadNewFile(content, fileNamesJson[openFileKey], DEFAULT_VER);
      _.assign(state, {
        activeFileKey: openFileKey, fileVersion: DEFAULT_VER, selectType: SelectType.file,
        fileNamesJson: this.setFileNamesJsonItem()
      });
      !stateTmp && this.setState(state);
    }
  }

  /**
   * used to rename file or move file position
   * @param {*} e  read the `target.value` that regard as new file name
   * @param {String} oldFileKey old file key 
   * @param {String} referFileKey transfer this if moved file position 
   * @param {Function} setLoading transfer this if set loading
   */
  renameOrMoveFileFunc = async (e, oldFileKey, referFileKey, setLoading) => {
    const { activeFileKey, fileVersion, fileNamesJson, uploadUri, cloud,
      userId, projectId, shareProjectId, notOwn } = this.state;
    let referFileKeyTmp = referFileKey === undefined ? oldFileKey : referFileKey;
    const oldFile = fileNamesJson[oldFileKey];
    const newFileName = e.target.value;
    const newFileKey = getFileKey(referFileKeyTmp, newFileName);
    let move = referFileKey !== undefined && newFileName === getFileName(oldFileKey);
    if (notOwn && oldFileKey === SETTINGS_NAME) {
      setLoading && setLoading(false);
      return showToast(MSG.CAN_NOT_MODIFY_SETTINGS, "warning");
    }
    if (oldFileKey === newFileKey) {
      e.target.value = oldFile.name;
      this.setState({ rename: false, renameFileKey: null }, () => {
        setLoading && setLoading(false);
        showToast('No change, can not operate.', "error");
      })
      return false;
    }
    if (fileNamesJson[newFileKey]) {
      setLoading && setLoading(false);
      showToast(MSG.FILE_NAME_REPEATED, 'warning');
      return false;
    }
    if (fileNamesJson[newFileKey + fileSeparator]) {
      setLoading && setLoading(false);
      showToast(MSG.FOLDER_NAME_THE_SAME, 'warning');
      return false;
    }
    if (!move && !newFileName.match(fileNameReg)) {
      e.target.value = oldFile.name;
      this.setState({ rename: false, renameFileKey: null }, () => {
        setLoading && setLoading(false);
        showToast(MSG.FILE_NAME_BREAK_THE_RULE, 'warning');
      })
      return false;
    }
    if (move &&
      !await showConfirm(<span>Are you sure you want to move
        <span className="text-primary">{oldFileKey}</span> to
        <span className="text-success">{newFileKey}</span>?</span>)) {
      setLoading && setLoading(false);
      return false;
    }
    let state = {};
    await this.handleRemoveLinkedToGraphXR(oldFileKey, state);
    await this.removeShareds(oldFileKey, state);
    let content;
    let openEditor = this.getOpenEditor(oldFileKey);
    if (openEditor && !openEditor.getReadOnly()) {
      content = transferToRealData(oldFile, await openEditor.latestText());
    } else {
      if (isFileNoEdit(oldFile)) {
        let newUri = getUri(uploadUri, newFileKey);
        ImageCache[newUri] = ImageCache[getUri(uploadUri, oldFileKey)];
        content = ImageCache[newUri];
      }
      if (undefined === content) {
        content = await this.getFileContent(oldFile, DEFAULT_VER);
      }
    }
    if (content !== undefined && !isFileNoEdit(oldFile) &&
      (cloud[oldFileKey].mtimeMs !== oldFile.mtimeMs)) {
      //upload modified file to path different from old path
      let ret = await this.uploadMdFile(oldFile, undefined, content, state);
      if (!ret) {
        e.target.value = oldFile.name;
        this.setState(_.assign(state, { rename: false, renameFileKey: null }))
        setLoading && setLoading(false);
        return false;
      }
    }
    let _id = fileNamesJson[oldFileKey].shareData && fileNamesJson[oldFileKey].shareData._id
    let res = await axios.post(`/api/grove/renameFile`,
      {
        oldFileKey: oldFileKey, newFileKey: newFileKey, userId: userId,
        projectId: projectId, _id, shareProjectId: shareProjectId,
      });
    res.data.message && showToast(res.data.message, res.data.status ? "error" : "success");
    if (res.data.status) {
      e.target.value = oldFile.name;
      this.setState(_.assign(state, { rename: false, renameFileKey: null }))
      setLoading && setLoading(false);
      return false;
    }
    setLoading && setLoading(false);
    cloud[newFileKey] = _.cloneDeep(res.data.content);
    fileNamesJson[newFileKey] = res.data.content;
    removeItem(getFileNamesKey() + '.' + newFileKey);
    removeItem(getFileNamesKey() + '.' + oldFileKey);
    let newFile = fileNamesJson[newFileKey];
    if (openEditor && openEditor.isCommon()) {
      this.removeFileCache(oldFileKey);
      newFile.selected = true;
      await this.loadNewFile(content, newFile, DEFAULT_VER);
      this.setState(_.assign(state, {
        activeFileKey: newFileKey, fileVersion: DEFAULT_VER, selectType: SelectType.file,
        rename: false, renameFileKey: null, fileNamesJson: this.setFileNamesJsonItem(), cloud: cloud
      }), () => {
        openEditor.focusFolder();
      })
    } else {
      this.removeFileCache(oldFileKey);
      if (openEditor) {
        await this.switchFileTagEditor(newFile, DEFAULT_VER, content, undefined, false, openEditor);
      }
      this.setState(_.assign(state, {
        rename: false, renameFileKey: null, fileNamesJson: this.setFileNamesJsonItem(), cloud: cloud
      }))
    }
  };

  /**
   * 
   * @param {*} e 
   * @param {string} oldFolderKey 
   * @param {string} referFileKey 
   * @param {*} setLoading 
   */
  renameOrMoveFolderFunc = async (e, oldFolderKey, referFileKey, setLoading) => {
    const { activeFolderKey, activeFileKey, linkedFilesMap, sharedFilesMap,
      fileNamesJson, uploadUri, cloud, userId, projectId, shareProjectId } = this.state;
    const referFileKeyTmp = referFileKey === undefined ?
      oldFolderKey.substring(0, oldFolderKey.length - 1)
      : referFileKey;
    const oldFolder = fileNamesJson[oldFolderKey];
    const newFolderName = e.target.value;
    const newFolderKey = getFolderKey(referFileKeyTmp, newFolderName);
    if (oldFolderKey === newFolderKey) {
      e.target.value = oldFolder.name;
      this.setState({ rename: false, renameFileKey: null }, () => {
        setLoading && setLoading(false);
        showToast('No change, can not operate.', "error");
      })
      return false;
    }
    if (fileNamesJson[newFolderKey]) {
      setLoading && setLoading(false);
      showToast(MSG.FOLDER_NAME_REPEATED, "error");
      return;
    }
    if (fileNamesJson[newFolderKey.substring(0, newFolderKey.length - 1)]) {
      setLoading && setLoading(false);
      showToast(MSG.FILE_NAME_THE_SAME, "error");
      return;
    }
    if (!newFolderName.match(folderNameReg)) {
      e.target.value = oldFolder.name;
      this.setState({ rename: false, renameFileKey: null }, () => {
        setLoading && setLoading(false);
        showToast(MSG.FOLDER_NAME_BREAK_THE_RULE, "error");
      })
      return false;
    }
    if (referFileKey !== undefined &&
      !await showConfirm(<span>Are you sure you want to move <span className="text-primary">{oldFolderKey}</span> to <span className="text-success">{newFolderKey}</span>?</span>)) {
      setLoading && setLoading(false);
      return false;
    }
    let state = {};
    await this.uploadAllFiles(undefined, state, false, oldFolderKey);
    let promiseList = [];
    _.keys(fileNamesJson).filter(fileKey => {
      if (fileKey.startsWith(oldFolderKey) && fileKey !== oldFolderKey) {
        if (linkedFilesMap[fileKey]) {
          promiseList.push(this.handleRemoveLinkedToGraphXR(fileKey, state));
        }
        if (sharedFilesMap[fileKey]) {
          promiseList.push(this.removeShareds(fileKey, state));
        }
      }
    })
    await Promise.all(promiseList);
    let res = await axios.post(`/api/grove/renameFolder`,
      {
        oldFolderKey: oldFolderKey, newFolderKey: newFolderKey, userId: userId,
        projectId: projectId, shareProjectId: shareProjectId,
      });
    res.data.message && showToast(res.data.message, res.data.status ? "error" : "success");
    if (res.data.status) {
      setLoading && setLoading(false);
      e.target.value = oldFolder.name;
      this.setState({ rename: false, renameFileKey: null })
      return false;
    }
    setLoading && setLoading(false);
    _.each(_.keys(fileNamesJson), (key, index) => {
      if (key.startsWith(oldFolderKey)) {
        delete cloud[key];
        delete fileNamesJson[key];
      }
    })
    _.map(res.data.content, (value, key) => {
      cloud[key] = _.cloneDeep(value);
      fileNamesJson[key] = value;
    })
    this.setFileNamesJsonItem();
    if (activeFileKey && activeFileKey.startsWith(oldFolderKey)) {
      await editor.clearEditor(undefined, true, true, state);
    }
    let closeTagEditors = [];
    for (let index = 0, tEditor; tEditor = window.tagEditors[index], index < window.tagEditors.length; index++) {
      if (tEditor.getFileData() && tEditor.getFileData().fileKey.startsWith(oldFolderKey)) {
        closeTagEditors.push(tEditor);
      }
    }
    await this.closeTagEditor(closeTagEditors);
    _.assign(state, {
      activeFolderKey: newFolderKey, selectType: SelectType.dir,
      rename: false, renameFileKey: null, fileNamesJson: this.setFileNamesJsonItem(), cloud: cloud
    });
    this.setState(state, () => {
      editor.focusFolder();
    })
  }

  /**
   * 
   * @param {string} fileKey
   * @param {object} state 
   * @param {function} cb 
   * @param {number} version 
   */
  switchFileByFileKey = async (fileKey, state, version = DEFAULT_VER) => {
    let target = this.ref.current.querySelector(`[data-link='${fileKey}']`)
    if (target) {
      await this.switchFile({ currentTarget: target }, fileKey, state, version);
    } else {
      showToast("Can't not find file:" + fileKey, "error")
    }
  }
  /**
   * 
   * @param {{ currentTarget: Element }} e 
   * @param {string} fileKey 
   * @param {object} stateTmp transfer state that setState later or right now setState in the function
   * @param {number} version 
   */
  switchFile = async (e, fileKey, stateTmp, version = DEFAULT_VER) => {
    const { activeFileKey, fileVersion, selectType, fileNamesJson, uploadUri, cloud } = this.state;
    let state = stateTmp || {};
    if (fileKey === activeFileKey && fileVersion === version) {
      if (selectType !== SelectType.file) {
        _.assign(state, { selectType: SelectType.file });
      }
      !stateTmp && this.setState(state, () => {
        this.focusDrawer();
        this.focusActive = true;
      });
      return;
    }
    let dataLink = e.currentTarget.parentNode.getAttribute("data-link");
    await editor.saveCurrentFile(state)
    Object.keys(fileNamesJson).forEach((tmpfileKey) => {
      if (fileKey !== tmpfileKey) {
        fileNamesJson[tmpfileKey].selected = false;
      } else {
        fileNamesJson[tmpfileKey].selected = true;
      }
    })
    this.setFileNamesJsonItem();
    await new Promise((resolve, reject) => {
      this.setWrapperLoading(true, async () => {
        let content = await this.getFileContent(fileNamesJson[fileKey], version)
        await this.loadNewFile(content, fileNamesJson[fileKey], version);
        _.assign(state, {
          activeFileKey: fileKey, fileVersion: version,
          selectType: SelectType.file, activeFolderKey: dataLink
        })
        this.setWrapperLoading(false)
        if (!stateTmp) {
          this.setState(state, () => {
            if (this.focusActive && !(!e.nativeEvent && (e.nativeEvent instanceof Event))) {
              this.focusDrawer();
            }
            this.focusActive = true;
            resolve(state);
          });
        } else {
          resolve(state);
        }
      })
    })
  }

  /**
   * 
   * @param {DefaultFileData} fileData 
   * @param {Object} state 
   * @param {number} version
   * 
   * @returns {ShapeChatInfo}
   */
  getChatData = (fileData, state, version = DEFAULT_VER) => {
    const projectShareUsers = (state && state.projectShareUsers) || this.state.projectShareUsers;
    const { notOwn, uploadUri, usersSharePermission, shareProjectId } = this.state;
    const { userId, userName, email, projectId, projectName } = this.props;
    if (version !== DEFAULT_VER) {
      return undefined;
    }
    if (notOwn) {
      return _.assign({}, ShapeChatInfo,
        {
          userId: PROJECT_USER_ID,
          userName: getPROJECT_USER_NAME(),
          email: getPROJECT_USER_EMAIL(),
          isMaster: true,
          userStatus: UserStatus.offline,
          tag: TAG_TYPE.projectShare,
          projectId,
          projectName,
          fileKey: fileData.fileKey,
          version,
          status: MaxCanOption["Super"],
          uploadUri,
        }
      );
    } else if (!_.isEmpty(projectShareUsers)) {
      return _.assign({}, ShapeChatInfo,
        {
          userId,
          userName,
          email,
          isMaster: true,
          userStatus: UserStatus.offline,
          tag: TAG_TYPE.projectShare,
          projectId,
          projectName,
          fileKey: fileData.fileKey,
          version,
          status: MaxCanOption["Super"],
          uploadUri,
        }
      );
    }
    /**@type Object.<string, Object.<string,ShapeShared>> */
    const sharedFilesMap = (state && state.sharedFilesMap) || this.state.sharedFilesMap;
    let obj = sharedFilesMap[fileData.fileKey];
    let shared = _.isEmpty(obj) ? undefined : _.values(obj)[0];
    if (shared) {
      return _.assign({}, ShapeChatInfo, shared,
        {
          userId: shared.userId._id,
          userName: `${shared.userId.firstName} ${shared.userId.lastName}`,
          email: shared.userId.email,
          isMaster: true,
          tag: TAG_TYPE.folder,
          status: MaxCanOption["Super"],
        }
      )
    }
  }

  /**
   * display file content in editor
   * @param {String} content 
   * @param {DefaultFileData} fileData 
   * @param {number} version 
   * @param {ShapeChatInfo} chatData 
   */
  loadNewFile = async (content, fileData, version, chatData) => {
    try {
      if (!fileData) {
        return;
      }
      const { uploadUri } = this.state;
      content = wrapContent(content, fileData, uploadUri);
      editor.setFileData(fileData, version, chatData || this.getChatData(fileData, undefined, version));
      await editor.value(content, ContentType.File, version);
    } catch (e) {
      console.error(e.stack);
    }
  }

  switchFileVersion = async (version) => {
    const { activeFileKey, fileNamesJson } = this.state;
    if (!activeFileKey) {
      showToast(MSG.NOT_SELETE_A_FILE, "error");
      return;
    }
    let fileData = fileNamesJson[activeFileKey];
    let content = await this.getRealFileContent(fileData, version);
    editor.setFileData(fileData, version, this.getChatData(fileData, undefined, version));
    await editor.value(content, ContentType.File, version);
    this.setState({ fileVersion: version })
  }

  toggleFunc = (e) => {
    const { activeFolderKey, selectType } = this.state;
    const dataLink = e.target.getAttribute("data-link");
    if (activeFolderKey !== dataLink) {
      this.setState({ activeFolderKey: dataLink, selectType: SelectType.dir });
    } else if (selectType !== SelectType.dir) {
      this.setState({ selectType: SelectType.dir })
    }
  }

  newFolderFunc = (e) => {
    const { activeFileKey, activeFolderKey, selectType, fileNamesJson } = this.state;
    const folderName = e.target.value;
    e.target.value = "";
    if (!folderName.match(folderNameReg)) {
      showToast(MSG.FOLDER_NAME_BREAK_THE_RULE, "warning");
      return;
    }
    if ("" === folderName) {
      showToast("Forder name can not be empty!", "warning")
      return;
    }
    let folderKey = getFolderKey(selectType === SelectType.dir ? activeFolderKey : activeFileKey, folderName);
    if (fileNamesJson[folderKey]) {
      showToast("Forder name repeated!")
      return;
    }
    if (fileNamesJson[folderKey.substring(0, folderKey.length - 1)]) {
      showToast("A file name the same as it!")
      return;
    }
    let state = { newFolder: false };
    let folder = Object.assign({}, DefaultFolderData, {
      folderKey: folderKey, name: folderName
    });
    this.setWrapperLoading(true, async () => {
      let ret = await this.uploadFolderFunc(folder, undefined, state);
      if (ret) {
        _.assign(state, { activeFolderKey: folderKey, selectType: SelectType.dir });
      }
      !_.isEmpty(state) && this.setState(state, () => {
        this.setWrapperLoading(false);
        this.activeIntoView(undefined, true);
      });
    })
  }

  /**
   * 
   * @param {{target:{value:{string}}}} e
   * @param {*} contentTmp Blob if image or string or undefined
   */
  newFileFunc = async (e, contentTmp, referFileKeyTmp, load = true, stateTmp) => {
    const { activeFileKey, activeFolderKey, selectType, fileNamesJson, uploadUri, cloud } = this.state;
    const { mdfiles_transfer: { recordHistory } } = this.props;
    const fileName = e.target.value;
    e.target.value = "";
    if (!fileName.match(fileNameReg)) {
      showToast(MSG.FILE_NAME_BREAK_THE_RULE, "warning");
      return false;
    }
    if ("" === fileName) {
      showToast(MSG.FILE_NAME_CAN_NOT_EMPLTY, "warning")
      return false;
    }
    let referFileKey = referFileKeyTmp === undefined ?
      (selectType === SelectType.dir ? activeFolderKey : activeFileKey) :
      referFileKeyTmp;
    let fileKey = getFileKey(referFileKey, fileName);
    if (fileNamesJson[fileKey + fileSeparator]) {
      showToast(MSG.FOLDER_NAME_THE_SAME, "warning")
      return false;
    }
    if (fileNamesJson[fileKey] !== undefined && !await showConfirm(`Overwrite ${fileKey}?`)) {
      return false;
    }
    let file = {
      fileKey: fileKey, name: fileName, selected: false, type: getFileType(fileKey), mtimeMs: new Date().getTime()
    };
    const state = stateTmp || {};
    if (load && activeFileKey) {
      await editor.saveCurrentFile(state);
    }
    let content;
    if (contentTmp !== undefined) {
      if (isFileNoEdit(file) && (contentTmp instanceof Blob)) {
        content = await readAsDataURL(contentTmp);
      } else {
        content = contentTmp;
      }
    } else if (isFileCsv(file)) {
      content = "";
    } else if (isFileJs(file)) {
      content = "";
    } else if (isFileJson(file)) {
      content = "{}";
    } else if (isFileHtml(file)) {
      content = "<html></html>";
    } else if (isFileEditorJs(file)) {
      content = getUndefinedContent();
      setItem(getUndefinedKey(), DefaultData);
    } else {
      showToast(MSG.FILE_NAME_FILE_TYPE_NOT_SUPPORT, "warning");
      return false;
    }
    setItem(getFileNamesKey() + '.' + fileKey, content);
    fileNamesJson[fileKey] = Object.assign({}, DefaultFileData, file);
    Object.keys(fileNamesJson).forEach((tmpfileKey) => {
      if (fileKey !== tmpfileKey) {
        fileNamesJson[tmpfileKey].selected = false;
      } else {
        fileNamesJson[tmpfileKey].selected = true;
      }
    })
    this.setFileNamesJsonItem();
    load && editor.setInitEdit(true);
    load && await this.loadNewFile(content, fileNamesJson[fileKey]);
    return new Promise((resolve, reject) => {
      this.setWrapperLoading(true, async () => {
        await this.uploadMdFile(fileNamesJson[fileKey], this.setWrapperLoading, undefined, state);
        load && _.assign(state, {
          activeFileKey: fileKey, selectType: SelectType.file,
          rename: false, renameFileKey: null, newFile: false
        });
        this.setState(_.assign(state, {
        }), async () => {
          if (load) {
            await this.switchFileTagEditor(fileKey);
            editor.focusFolder();
            recordHistory(this.getInfo({ fileKey, version: DEFAULT_VER }))
          }
          resolve(true);
        });
      })
    })
  }

  /**
   * 
   * @param {*} e 
   * @param {*} setLoading 
   * @param {string} fileKeyTmp delete activeFile if undefined
   */
  removeFileFunc = async (e, setLoading, fileKeyTmp) => {
    const { activeFileKey, notOwn, userId, projectId } = this.state;
    /**@type {string} */
    const fileKey = fileKeyTmp || activeFileKey;
    if (notOwn && fileKey === SETTINGS_NAME) {
      setLoading && setLoading(false);
      return showToast(MSG.CAN_NOT_MODIFY_SETTINGS, "warning");
    }
    if (!await showConfirm(<span>Are you sure remove <span className="text-primary">{fileKey}</span> ?</span>, undefined, undefined, { okType: 'danger', okText: "Remove" })) {
      setLoading && setLoading(false);
      return;
    }
    if (fileKey === null) {
      setLoading && setLoading(false);
      return;
    }
    const state = {}
    await this.handleRemoveLinkedToGraphXR(fileKey, state);
    await this.removeShareds(fileKey, state);
    const { fileNamesJson, cloud } = this.state;
    const openEditor = this.getOpenEditor(fileKey);
    openEditor && (await openEditor.saveCurrentFile(state));
    let _id = fileNamesJson[fileKey].shareData && fileNamesJson[fileKey].shareData._id
    let res = await axios.post(`/api/grove/removeFile`, {
      fileKey: fileKey, userId: userId,
      projectId: projectId, _id: _id, shareProjectId: this.state.shareProjectId,
    });
    if (!res.data.status) {
      this.removeFileCache(fileKey);
      _.assign(state, {
        fileNamesJson: this.setFileNamesJsonItem(), cloud: cloud,
      });
      if (openEditor) {
        if (openEditor.isCommon()) {
          await openEditor.clearEditor(undefined, true, true, state);
        } else {
          await this.closeTagEditor(openEditor);
        }
      }
    }
    !_.isEmpty(state) && this.setState(state);
    res.data.message && showToast(res.data.message, res.data.status ? "error" : "success");
    setLoading && setLoading(false);
    if (window.currentEditor.getFileData() && fileKey.startsWith(getFolderKey(window.currentEditor.getFileData().fileKey, FILESDIR_NAME))) {
      actions.variable(actions.types.REFRESH_FILE_ATTACHMENTS, [], () => { })
    }
  }

  /**@returns {CommonEditor} current open tab editor */
  getTagEditor = (fileKey) => {
    return _.filter(window.tagEditors, /** @param {CommonEditor} tagEditor */(tagEditor) => {
      return tagEditor.getFileData() && tagEditor.getFileData().fileKey === fileKey
    })[0]
  }
  /**@returns {CommonEditor} current open editor */
  getOpenEditor = (fileKey) => {
    let openEditor = this.getTagEditor(fileKey);
    if (editor.getFileData() && editor.getFileData().fileKey === fileKey) {
      if (openEditor) { showToast("open file repeated!", 'error') }
      openEditor = editor;
    }
    return openEditor;
  }
  /**@returns {CommonEditor[]} */
  getTagEditors = (folderKey) => {
    return _.filter(window.tagEditors, /** @param {CommonEditor} tagEditor */(tagEditor) => {
      return tagEditor.getFileData() && tagEditor.getFileData().fileKey.startsWith(folderKey);
    })
  }


  /**
   * 
   * @param {typeof DefaultFileData} file 
   * @param {number} version 
   */
  duplicateFileFunc = async (file, version) => {
    const { fileNamesJson, selectType, activeFolderKey, activeFileKey } = this.state;
    let fileName;
    for (let index = 0; index < 100; index++) {
      fileName = ~file.name.lastIndexOf(".") ?
        `${file.name.substring(0, file.name.lastIndexOf("."))}_copy${index > 0 ? index : ''}${file.name.substring(file.name.lastIndexOf("."))}` :
        `${file.name}_copy${index > 0 ? index : ''}`
      let fileKey = getFileKey(selectType === SelectType.dir ? activeFolderKey : activeFileKey, fileName);
      if (fileNamesJson[fileKey + fileSeparator]) {
        continue;
      }
      if (fileNamesJson[fileKey] !== undefined) {
        continue;
      }
      break;
    }
    let content = await this.getRealFileContent(file, version);
    await this.newFileFunc({ target: { value: fileName } }, content);
  }

  /**
   * clear editor , display no file page
   * @param {Function} cb 
   * @param {boolean} initData 
   * @param {boolean} clearSelected 
   */
  clearEditor = async (cb, initData = false, clearSelected = false, stateTmp, refreshWindowState = true) => {
    let state = stateTmp || {};
    _.assign(state, {
      selectType: SelectType.dir, newFolder: false,
      newFile: false, rename: false, renameFileKey: null,
      activeFileKey: null
    })
    editor.getLinkData() && editor.setLinkData(undefined);
    editor.getFileData() && editor.setFileData(undefined);
    if (clearSelected) {
      const { fileNamesJson } = this.state;
      Object.keys(fileNamesJson).forEach((tmpfileKey) => {
        fileNamesJson[tmpfileKey].selected = false;
      });
      this.setFileNamesJsonItem();
      _.assign(state, { fileNamesJson });
    }
    refreshWindowState && setWindowState(this.getInfo({}));
    initData && await editor.value(getUndefinedContent(), ContentType.File);
    !stateTmp && this.setState(state, cb);
  }

  /**
   * 
   * @param {*} e 
   * @param {*} setLoading 
   * @param {*} folderKeyTmp delete activeFolder if undefined
   */
  removeFolderFunc = async (e, setLoading, folderKeyTmp) => {
    const { activeFileKey, fileNamesJson, cloud, activeFolderKey, linkedFilesMap,
      sharedFilesMap, notOwn, userId, projectId } = this.state;
    const folderKey = folderKeyTmp || activeFolderKey;
    let removeFileKeys = _.keys(fileNamesJson).filter(fileKey => {
      return fileKey.startsWith(folderKey);
    })
    if (notOwn && ~removeFileKeys.indexOf(SETTINGS_NAME)) {
      setLoading && setLoading(false);
      return showToast(MSG.NO_FILES_REMOVED, "warning");
    }
    if (~removeFileKeys.indexOf(SETTINGS_NAME) && !await showConfirm(MSG.CONFIRM_REMOVE_SETTINGS, undefined, undefined, { okType: 'danger', okText: "Remove" })) {
      setLoading && setLoading(false);
      return;
    }
    if (removeFileKeys.length === 0) {
      setLoading && setLoading(false);
      return showToast(MSG.NO_FILES_REMOVED, "warning");
    }
    let _shareProjectIds = [];
    _.each(_.keys(fileNamesJson), (fileKey) => {
      fileKey.startsWith(folderKey) &&
        fileNamesJson[fileKey].shareData &&
        _shareProjectIds.push(fileNamesJson[fileKey].shareData._id);
    })
    if (!await showConfirm(<div><div>Are you sure remove <span className="text-primary">{folderKey}</span>:</div>{_.map(removeFileKeys, (v, index, colls) => {
      return <div className="text-primary f7" key={v}>{v.substring(folderKey.length)}{index !== colls.length - 1 && ","}</div>;
    })
    } ?</div>, undefined, undefined, { okType: 'danger', okText: "Remove" })) {
      setLoading && setLoading(false);
      return;
    }
    if (folderKey === null) {
      setLoading && setLoading(false);
      return;
    }
    let state = {};
    let promiseList = [];
    _.keys(fileNamesJson).filter(fileKey => {
      if (fileKey.startsWith(folderKey) && fileKey !== folderKey) {
        if (linkedFilesMap[fileKey]) {
          promiseList.push(this.handleRemoveLinkedToGraphXR(fileKey, state));
        }
        if (sharedFilesMap[fileKey]) {
          promiseList.push(this.removeShareds(fileKey, state));
        }
      }
    })
    await Promise.all(promiseList);
    let res = await axios.post(`/api/grove/removeFolder`, {
      folderKey: folderKey, userId: userId,
      projectId: projectId, _ids: _shareProjectIds, shareProjectId: this.state.shareProjectId,
    });
    if (!res.data.status) {
      await this.removeFolderSuccessFunc(folderKey, state);
    }
    !_.isEmpty(state) && this.setState(state);
    res.data.message && showToast(res.data.message, res.data.status ? "error" : "success");
    setLoading && setLoading(false);
  }

  removeFolderSuccessFunc = async (folderKey, state) => {
    const { activeFileKey, fileNamesJson, cloud, activeFolderKey } = this.state;
    let removeFileKeys = _.keys(fileNamesJson).filter(fileKey => {
      return fileKey.startsWith(folderKey);
    })
    if (activeFileKey && activeFileKey.startsWith(folderKey)) {
      await editor.saveCurrentFile(state);
      this.removeFileCache(activeFileKey)
      removeFileKeys.splice(removeFileKeys.indexOf(activeFileKey), 1);
      await editor.clearEditor(undefined, true, true, state);
    }
    const tagEditors = this.getTagEditors(folderKey)
    const promiseList = [];
    _.each(tagEditors, (tagEditor) => {
      let fileKey = tagEditor.getFileData().fileKey;
      this.removeFileCache(fileKey);
      removeFileKeys.splice(removeFileKeys.indexOf(fileKey), 1);
      promiseList.push(this.closeTagEditor(tagEditor));
    })
    await Promise.all(promiseList);
    _.each(removeFileKeys, fileKey => {
      this.removeFileCache(fileKey);
    });
    (folderKey === activeFolderKey) && _.assign(state, { activeFolderKey: rootDir.folderKey });
    _.assign(state, { fileNamesJson: this.setFileNamesJsonItem(), cloud: cloud });
  }

  /**
   * 
   * @param {string[]} fileKeys 
   */
  removeFilesFunc = async (fileKeys) => {
    const { fileNamesJson } = this.state;
    for (let index = 0; index < fileKeys.length; index++) {
      let file = fileNamesJson[fileKeys[index]];
      if (!file) {
        continue;
      }
      if (isDir(file)) {
        await this.removeFolderFunc(undefined, undefined, file.folderKey)
      } else {
        await this.removeFileFunc(undefined, undefined, file.fileKey)
      }
    }
  }
  /**
   * remove file cache, storage, imagecache
   * @param {string} fileKey 
   */
  removeFileCache = (fileKey) => {
    if (!fileKey) {
      return;
    }
    const { cloud, fileNamesJson, uploadUri } = this.state;
    if (isFile(fileNamesJson[fileKey])) {
      removeItem(getFileNamesKey() + '.' + fileKey);
      if (isFileNoEdit(fileNamesJson[fileKey])) {
        delete ImageCache[getUri(uploadUri, fileKey)];
      }
    }
    delete cloud[fileKey];
    delete fileNamesJson[fileKey];
    disposeModule(fileKey, uploadUri);
    removeModule(fileKey, uploadUri);
  }

  revertFileFunc = async (fileKey) => {
    if (!await showConfirm(<div>Are you sure revert <span className="text-primary">{fileKey}</span> ?</div>)) {
      return;
    }
    this.setWrapperLoading(true, async () => {
      const { cloud, fileNamesJson, activeFileKey } = this.state;
      if (fileNamesJson[fileKey] === undefined || isFileNoEdit(fileNamesJson[fileKey])) {
        this.setWrapperLoading(false);
        showToast("revert Fail!", 'error');
        return;
      }
      removeItem(getFileNamesKey() + '.' + fileKey);
      fileNamesJson[fileKey] = _.cloneDeep(cloud[fileKey]);
      let openEditor = this.getOpenEditor(fileKey);
      if (openEditor) {
        if (openEditor.isCommon()) {
          let content = await this.getFileContent(fileNamesJson[fileKey])
          await this.loadNewFile(content, fileNamesJson[fileKey]);
        } else {
          await this.switchFileTagEditor(fileNamesJson[fileKey], DEFAULT_VER, undefined, undefined, true);
        }
      }
      this.setState({
        fileNamesJson
      }, () => {
        this.setWrapperLoading(false);
        showToast("revert Success!");
        editor.focusFolder();
      });
    })
  }
  /**
   * 
   * @param {typeof DefaultFileData} file 
   */
  unpublish = async (file) => {
    if (!file.shareData) {
      showToast("Not published, can not operate!", "error")
      return;
    }
    let res = await axios.post(`/api/grove/unpublish`, {
      _id: file.shareData._id,
    });
    if (!res.data.status) {
      const { fileVersion } = this.state;
      file.shareData = null;
      editor.setFileData(file, fileVersion, this.getChatData(file, undefined, fileVersion));
      this.setState({ fileNamesJson: this.setFileNamesJsonItem() })
    }
    res.data.message && showToast(res.data.message, res.data.status ? "error" : "success");
  }
  /**
   * 
   * @param {DefaultFileData} file 
   */
  publish = async (file, setLoading, status = ShareProjectCanOptions["View"], message) => {
    if (!isFileEditorJs(file)) {
      showToast("Can not publish!", "error")
      return;
    }
    if (!!file.shareData) {
      showToast("Already published!", "warning")
      return;
    }
    const { userId, projectId } = this.state;
    let state = {};
    let fileKey = file.fileKey;
    let openEditor = this.getOpenEditor(file.fileKey);
    if (openEditor) {
      await this.switchFileTagEditor(openEditor.getFileData());
    } else {
      _.assign(state, { fileVersion: DEFAULT_VER })
      await this.switchFileByFileKey(fileKey, state, state.fileVersion);
      openEditor = editor;
    }
    if (undefined !== openEditor.itemKey() && openEditor.getFileData().fileKey === fileKey
      && isFileEditorJs(file) && !file.previewImageUpload) {
      await this.screenshotAsPreviewImage(state);
    }
    let res = await axios.post(`/api/grove/publish`, {
      projectId: projectId,
      projectName: this.props.projectName,
      projectUser: userId,
      fileKey, status, message, title: openEditor.getFileDataTitle()
    });
    if (!res.data.status) {
      _.merge(file, res.data.content);
      if (openEditor.isCommon()) {
        openEditor.setFileData(file, state.fileVersion, this.getChatData(file, undefined, state.fileVersion));
        _.assign(state, { fileNamesJson: this.setFileNamesJsonItem() });
      }
    }
    this.setState(state, () => {
      if (openEditor.isCommon()) {
        openDirs(fileKey);
        openEditor.focusFolder();
      }
    });
    setLoading && setLoading(false);
    res.data.message && showToast(res.data.message, res.data.status ? "error" : "success");
  }

  /**
   * 
   * @param {typeof DefaultFileData} file 
   */
  republish = async (file, message) => {
    if (!isFileEditorJs(file)) {
      showToast("Can not publish!", "warning")
      return;
    }
    if (!file.shareData) {
      showToast("Not published!", "error")
      return;
    }
    const { userId, projectId } = this.state;
    let state = {};
    let fileKey = file.fileKey;
    let openEditor = this.getOpenEditor(file.fileKey);
    if (openEditor) {
      await this.switchFileTagEditor(openEditor.getFileData());
    } else {
      _.assign(state, { fileVersion: DEFAULT_VER })
      await this.switchFileByFileKey(fileKey, state, state.fileVersion);
      openEditor = editor;
    }
    if (undefined !== openEditor.itemKey() && openEditor.fileData.fileKey === fileKey
      && isFileEditorJs(file) && !file.previewImageUpload) {
      await this.screenshotAsPreviewImage(state);
    }
    let res = await axios.post(`/api/grove/republish`, {
      _id: file.shareData._id,
      projectId: projectId,
      projectName: this.props.projectName,
      projectUser: userId,
      fileKey, status: 1, message,
      title: openEditor.getFileDataTitle()
    });
    if (!res.data.status) {
      _.merge(file, res.data.content);
      if (openEditor.isCommon()) {
        openEditor.setFileData(file, state.fileVersion, this.getChatData(file, undefined, state.fileVersion));
        _.assign(state, { fileNamesJson: this.setFileNamesJsonItem() });
      }
    }
    this.setState(state, () => {
      if (openEditor.isCommon()) {
        openDirs(fileKey);
        openEditor.focusFolder();
      }
    });
    res.data.message && showToast(res.data.message, res.data.status ? "error" : "success");
  }
  /**
   * 
   * @param {DefaultFileData} file
   * @param {string} message 
   */
  tagVersion = async (file, message) => {
    if (!isFileEditorJs(file)) {
      showToast("Can not tag version!", "error")
      return;
    }
    const { userId, projectId } = this.state;
    let state = {};
    let fileKey = file.fileKey;
    let openEditor = this.getOpenEditor(fileKey);
    if (openEditor) {
      await this.switchFileTagEditor(openEditor.getFileData(), state.fileVersion);
    } else {
      _.assign(state, { fileVersion: DEFAULT_VER })
      await this.switchFileByFileKey(file.fileKey, state, state.fileVersion);
      openEditor = editor;
    }
    let res = await axios.post(`/api/grove/tagVersion`, {
      projectId: projectId,
      projectName: this.props.projectName,
      projectUser: userId,
      fileKey, message
    });
    if (!res.data.status) {
      _.merge(file, res.data.content);
      if (openEditor.isCommon()) {
        openEditor.setFileData(file, state.fileVersion, this.getChatData(file, undefined, state.fileVersion));
        _.assign(state, { fileNamesJson: this.setFileNamesJsonItem() });
      }
    }
    this.setState(state, () => {
      if (openEditor.isCommon()) {
        openDirs(fileKey);
        openEditor.focusFolder();
      }
    });
    res.data.message && showToast(res.data.message, res.data.status ? "error" : "success");
  }

  /**
   * 
   * @param {DefaultFileData} file
   */
  setPreviewImageFunc = async (file) => {
    if (!isFileEditorJs(file)) {
      showToast("Can not set preview Image!", "error")
      return;
    }
    let openEditor = this.getOpenEditor(file.fileKey);
    let state = {
      modalType: ModalType.SetPreviewImage, sourceFile: file, previewImg: null,
      modalData: {}
    };
    if (openEditor) {
      await this.switchFileTagEditor(openEditor.getFileData());
    } else {
      _.assign(state, { fileVersion: DEFAULT_VER })
      await this.switchFileByFileKey(file.fileKey, state, state.fileVersion);
    }
    this.setState(state);
  }

  /**
   * 
   * @param {DefaultFileData} file 
   */
  versionControlFunc = async (file) => {
    if (!file || !isFileEditorJs(file)) {
      showToast("Can not control version!", "error")
      return;
    }
    let state = {
      modalType: ModalType.VersionControl, sourceFile: file, previewImg: null,
      modalData: {}
    };
    let openEditor = this.getOpenEditor(file.fileKey);
    if (openEditor) {
      await this.switchFileTagEditor(openEditor.getFileData());
    } else {
      _.assign(state, { fileVersion: DEFAULT_VER });
      await this.switchFileByFileKey(file.fileKey, state, state.fileVersion);
    }
    this.setState(state);
  }
  multipleChoiceFunc = (prevValue) => {
    if (this.state.multipleChoice === prevValue) {
      this.ref.current.querySelectorAll(".form-check-input").forEach((checkboxInput, index) => {
        checkboxInput.checked = false;
      });
      this.setState({ multipleChoice: !prevValue })
    }
  }
  /**
   * 
   * @param {DefaultFileData} file 
   */
  publishFunc = async (file) => {
    if (!file || !isFileEditorJs(file)) {
      showToast("Can not publish!", "error")
      return;
    }
    let state = {
      modalType: ModalType.Publish, sourceFile: file, previewImg: null,
      modalData: {}
    };
    let openEditor = this.getOpenEditor(file.fileKey);
    if (openEditor) {
      await this.switchFileTagEditor(openEditor.getFileData());
    } else {
      _.assign(state, { fileVersion: DEFAULT_VER });
      await this.switchFileByFileKey(file.fileKey, state, state.fileVersion);
    }
    this.setState(state);
  }

  openDuplicateDrawer = () => {
    const { activeFileKey, activeFolderKey, selectType, fileNamesJson } = this.state;
    if (activeFileKey && selectType === SelectType.file) {
      this.setState({
        modalType: ModalType.DuplicateFile,
        treeSelectType: SelectType.dir,
        sourceFile: fileNamesJson[activeFileKey],
        modalData: {},
      })
    } else if (activeFolderKey && selectType === SelectType.dir) {
      this.setState({
        modalType: ModalType.DuplicateFile,
        treeSelectType: SelectType.dir,
        sourceFile: fileNamesJson[activeFolderKey],
        modalData: {},
      })
    }
  }

  /**
   * 
   * @param {typeof DefaultFileData} file 
   */
  openChanges = async (file) => {
    if (isFileNoEdit(file)) return;
    const { uploadUri, cloud, fileNamesJson, activeFileKey } = this.state;
    let state = {};
    if (file.fileKey === activeFileKey) {
      await editor.saveCurrentFile(state);
    }
    let oldText = await getMdFile(uploadUri, cloud, file, true);
    let text = await getMdFile(uploadUri, cloud, file);
    let ms_start = (new Date).getTime();
    let dmp = new diff_match_patch();
    let diffFunc = function (old_content, new_content) {
      if (typeof old_content === 'string' && typeof new_content === 'string') {
        let diff = dmp.diff_main(old_content, new_content);
        if (diff.length > 2) {
          dmp.diff_cleanupSemantic(diff);
        }
        let patch_list = dmp.patch_make(diff);
        let patch_text = dmp.patch_toText(patch_list);
        return patch_text;
      } else {
        old_content = typeof old_content === 'object' ? old_content : {}
        new_content = typeof new_content === 'object' ? new_content : {}
        return _.reduce(old_content, (prev, old_v, k) => {
          let new_v = new_content[k] || "";
          if (old_v !== new_v) {
            let diff = dmp.diff_main(old_v, new_v);
            if (diff.length > 2) {
              dmp.diff_cleanupSemantic(diff);
            }
            let patch_list = dmp.patch_make(diff);
            let patch_text = dmp.patch_toText(patch_list);
            prev[k] = patch_text
          }
          return prev;
        }, _.reduce(new_content, (prev, new_v, k) => {
          let old_v = old_content[k] || "";
          if (old_v === "") {
            let diff = dmp.diff_main(old_v, new_v);
            if (diff.length > 2) {
              dmp.diff_cleanupSemantic(diff);
            }
            let patch_list = dmp.patch_make(diff);
            let patch_text = dmp.patch_toText(patch_list);
            prev[k] = patch_text
          }
          return prev;
        }, {}))
      }
    }
    let diffStrings;
    if (isFileEditorJs(file)) {
      /**@type {[]} */
      let newBlocks = JSON.parse(text).blocks;
      let olds = JSON.parse(oldText).blocks;
      /**@type {[]} */
      let oldBlocks = _.clone(olds);
      let textBlocks = []
      let otherToContent = function (block) {
        let text;
        if (block.type === 'paragraph') {
          text = block.data.text
        } else if (block.type === 'table') {
          text = JSON.stringify(block.data.content)
        } else {
          text = JSON.stringify(block.data)
        }
        return text;
      }
      let specialTypes = ['codeTool', 'plotChart', 'antChart'];
      //check out removed blocks
      diffStrings = _.reduce(olds, (prev, block, index) => {
        let new_content = undefined, old_content, oldIndex = index;
        if (~specialTypes.indexOf(block.type)) {
          let toContent = toContentByType[block.type];
          old_content = toContent.func(block);
          if (block.type === 'codeTool') {
            let dname = block.data.codeData.dname;
            let news = toContent.extract(newBlocks, dname)
            if (!news.length) {
              oldBlocks.splice(oldBlocks.indexOf(block), 1);
              new_content = "";
            }
          } else if (block.type === 'plotChart') {
            let dname = block.data.dname;
            let news = toContent.extract(newBlocks, dname);
            if (!news.length) {
              oldBlocks.splice(oldBlocks.indexOf(block), 1);
              new_content = "";
            }
          } else if (block.type === 'antChart') {
            let dname = block.data.dname;
            let news = toContent.extract(newBlocks, dname);
            if (!news.length) {
              old_content = JSON.stringify(old_content);
              oldBlocks.splice(oldBlocks.indexOf(block), 1);
              new_content = "";
            }
          }
          if (new_content === "") {
            prev.push({ type: block.type, oldLine: oldIndex, line: "NULL", text: diffFunc(old_content, new_content) });
          }
        }
        return prev;
      }, []);
      diffStrings = _.reduce(newBlocks, (prev, block, index, newBlocks) => {
        let new_content, old_content, oldIndex, oldBlock;
        if (~specialTypes.indexOf(block.type)) {
          let toContent = toContentByType[block.type];
          new_content = toContent.func(block);
          if (block.type === 'codeTool') {
            let dname = block.data.codeData.dname;
            oldBlock = toContent.extract(oldBlocks, dname)[0];
            if (oldBlock) {
              old_content = toContent.func(oldBlock);
              oldIndex = olds.indexOf(oldBlock)
            } else {
              old_content = ""
            }
          } else if (block.type === 'plotChart') {
            let dname = block.data.dname;
            oldBlock = toContent.extract(oldBlocks, dname)[0];
            if (oldBlock) {
              old_content = toContent.func(oldBlock);
              oldIndex = olds.indexOf(oldBlock)
            } else {
              old_content = ""
            }
          } else if (block.type === 'antChart') {
            let dname = block.data.dname;
            oldBlock = toContent.extract(oldBlocks, dname)[0];
            if (oldBlock) {
              old_content = toContent.func(oldBlock);
              oldIndex = olds.indexOf(oldBlock)
            } else {
              new_content = JSON.stringify(new_content);
              old_content = ""
            }
          }
        } else {
          let text = otherToContent(block);
          textBlocks.push({ line: index, text: text });
          if (newBlocks.length - 1 !== index) {
            return prev;
          }
        }
        if (textBlocks.length) {
          let new_text_content = _.reduce(textBlocks, (prev, textBlock, index) => {
            prev.push(textBlock.text);
            return prev;
          }, []).join("\n")
          let lines = _.reduce(textBlocks, (prev, textBlock, index) => {
            prev.push(textBlock.line);
            return prev;
          }, []).join(",")
          textBlocks = [];
          let oldLen = 0;
          _.each(oldBlocks, (block, index, oldBlocks) => {
            if (!!~specialTypes.indexOf(block.type)) {
              oldLen = index;
              return false;
            } else if (index === oldBlocks.length - 1) {
              oldLen = oldBlocks.length;
            }
          })
          let old_text_content;
          let old_text_index;
          if (oldLen > 0) {
            let dels = oldBlocks.splice(0, oldLen);
            old_text_content = _.reduce(dels, (prev, block, index) => {
              prev.push(otherToContent(block))
              return prev;
            }, []).join("\n");
            old_text_index = _.reduce(dels, (prev, block, index) => {
              prev.push(olds.indexOf(block))
              return prev;
            }, []).join(",");
          } else {
            old_text_content = "";
            old_text_index = "NULL"
          }
          if (old_text_content !== new_text_content) {
            prev.push({ type: "others", oldLine: old_text_index, line: lines, text: diffFunc(old_text_content, new_text_content) });
          }
        }
        if (~specialTypes.indexOf(block.type)) {
          if (!_.isEqual(old_content, new_content)) {
            prev.push({ type: block.type, oldLine: oldIndex, line: index, text: diffFunc(old_content, new_content) });
          }
          if (oldBlock) {
            oldBlocks.splice(oldBlocks.indexOf(oldBlock), 1);
          }
        }
        return prev;
      }, diffStrings);
    } else {
      diffStrings = [{ oldLine: "all", line: "all", text: diffFunc(oldText, text) }];
    }

    let ms_end = (new Date).getTime();
    this.setState(_.assign(state,
      {
        modalType: ModalType.OpenChanges,
        diffStrings: diffStrings,
        // source: patch_text + '\nTime: ' + (ms_end - ms_start) / 1000 + 's',
        sourceFile: file,
        modalData: {}
      }));
  }

  menuClickFunc = (info) => {
    this.setState({ rightMenuVisible: false }, () => {
      this.menuClickHandler(info, this.rightMenuFile)
    })
  }

  /**
   * handle right menu
   * @param {*} info 
   * @param {DefaultFileData} hoverFile
   */
  menuClickHandler = async (info, hoverFile) => {
    const { fileNamesJson, uploadUri, projectId, userId, activeFileKey, fileVersion, activeFolderKey, selectType, } = this.state;
    switch (info.key) {
      case "absolutePath":
        copyContent((uploadUri.startsWith(ROOT_URI) ? uploadUri.replace(ROOT_URI, "%") : "") + (isDir(hoverFile) ? hoverFile.folderKey : hoverFile.fileKey));
        break;
      case "relativePath":
        copyContent((isDir(hoverFile) ? hoverFile.folderKey : hoverFile.fileKey));
        break;
      case "removeFile":
        if (isDir(hoverFile)) {
          this.removeFolderFunc(undefined, undefined, hoverFile.folderKey)
        } else {
          this.removeFileFunc(undefined, undefined, hoverFile.fileKey)
        }
        break;
      case "removeFiles":
        this.removeFilesFunc(_.reduce(this.ref.current.querySelectorAll(".form-check-input:checked"), (prev, curr) => {
          prev.push(curr.value);
          return prev;
        }, []))
        break;
      case "duplicateFile":
        if (isFile(hoverFile)) {
          this.setState({
            modalType: ModalType.DuplicateFile,
            treeSelectType: SelectType.dir,
            sourceFile: hoverFile,
            modalData: {},
          })
        } else if (isDir(hoverFile)) {
          this.setState({
            modalType: ModalType.DuplicateFile,
            treeSelectType: SelectType.dir,
            sourceFile: hoverFile,
            modalData: {},
          })
        }
        break;
      case "downloadGrove":
        if (isDir(hoverFile)) {
          this.downloadZip(hoverFile.folderKey, [], DEFAULT_VER, ExportType.GROVE, undefined, `${hoverFile.name}.zip`)
        } else {
          if (hoverFile.fileKey === activeFileKey) {
            this.onExport(ExportType.GROVE)
          } else {
            this.exportFile(hoverFile.fileKey, undefined, ExportType.GROVE);
          }
        }
        break;
      case "convertToDoc":
        function Export2Word(html = "", filename = '') {
          let preHtml = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'>";
          html = html.replace(/\<HTML[^\<\>]{0,}\>/i, preHtml);
          let blob = new Blob(['\ufeff', html], {
            type: 'application/msword'
          });
          // Specify link url
          let url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);
          // Specify file name
          filename = filename ? filename + '.doc' : 'document.doc';
          // Create download link element
          let downloadLink = document.createElement("a");
          document.body.appendChild(downloadLink);
          if (navigator.msSaveOrOpenBlob) {
            navigator.msSaveOrOpenBlob(blob, filename);
          } else {
            // Create a link to the file
            downloadLink.href = url;
            // Setting the file name
            downloadLink.download = filename;
            //triggering the function
            downloadLink.click();
          }
          document.body.removeChild(downloadLink);
        }
        let markdown = await this.getRealFileContent(hoverFile, DEFAULT_VER);
        Export2Word(markdown, hoverFile.name);
        break;
      case "downloadMd":
        if (isDir(hoverFile)) {
          this.downloadZip(hoverFile.folderKey, [], 0, ExportType.MD, undefined, `${hoverFile.name}.zip`)
        } else {
          if (hoverFile.fileKey === activeFileKey) {
            this.onExport(ExportType.MD)
          } else {
            this.exportFile(hoverFile.fileKey, undefined, ExportType.MD);
          }
        }
        break;
      case "downloadSelectedGrove":
        this.downloadZip(undefined,
          _.reduce(this.ref.current.querySelectorAll(".form-check-input:checked"), (prev, curr) => {
            prev.push(curr.value);
            return prev;
          }, []), undefined, ExportType.GROVE);
        break;
      case "downloadSelectedMd":
        this.downloadZip(undefined,
          _.reduce(this.ref.current.querySelectorAll(".form-check-input:checked"), (prev, curr) => {
            prev.push(curr.value);
            return prev;
          }, []), undefined, ExportType.MD);
        break;
      case "DisplaySource":
        let content = await this.getTextContent(hoverFile);
        this.setState({
          modalType: ModalType.DisplaySource, source: content, sourceFile: hoverFile,
          modalData: {}
        })
        break;
      case "SetPreviewImage":
        this.setPreviewImageFunc(hoverFile);
        break;
      case "VersionControl":
        this.versionControlFunc(hoverFile);
        break;
      case "multipleChoice":
        this.multipleChoiceFunc(false);
        break;
      case "cancelMultipleChoice":
        this.multipleChoiceFunc(true);
        break;
      case "revertFile":
        this.revertFileFunc(hoverFile.fileKey);
        break;
      case "OpenChanges":
        this.openChanges(hoverFile);
        break;
      case "newFolder":
        this.setState({
          selectType: SelectType.dir, activeFolderKey: hoverFile.folderKey,
          rename: false, renameFileKey: null, newFile: false, newFolder: true
        }, () => {
          const { selectType, activeFolderKey, activeFileKey } = this.state;
          let activeKey = selectType === SelectType.dir ? activeFolderKey : activeFileKey;
          openDirs(activeKey);
          document.querySelector(".new-folder.active input").focus();
          this.activeIntoView();
        })
        break;
      case "renameFile":
      case "renameFolder":
        actions.variable(actions.types.DRAWER_TYPE, [], () => {
          return {
            drawerObj: DrawerType.Folder,
            cb: () => {
              actions.variable(actions.types.RENAME, [], () => {
                return { file: hoverFile }
              })
            },
            only: true,
          }
        })
        break;
      case "tagVersion":
        this.tagVersion(hoverFile);
        break;
      case "publish":
        this.publishFunc(hoverFile);
        break;
      case "republish":
        this.republish(hoverFile);
        break;
      case "upload":
        this.uploadMdFile(hoverFile, undefined, undefined, undefined,
          hoverFile.fileKey === activeFileKey ? fileVersion : DEFAULT_VER);
        break;
      case "unpublish":
        this.unpublish(hoverFile);
        break;
      case "decompression":
        this.decompression(hoverFile);
        break;
    }
  }

  /**
   * 
   * @param {DefaultFileData} file 
   * @param {{}} stateTmp 
   */
  decompression = async (file, stateTmp) => {
    this.setWrapperLoading(true, async () => {
      let fd = new FormData();
      fd.append("fileKey", file.fileKey);
      fd.append("fileName", file.name);
      const { userId, projectId } = this.state;
      fd.append('userId', userId);
      fd.append('projectId', projectId);
      this.state.shareProjectId && fd.append('shareProjectId', this.state.shareProjectId);
      let shareData = this.getChatData(file, undefined, DEFAULT_VER);
      shareData && fd.append('roomId', `${this.state.uploadUri}${file.fileKey}`);
      let res = await axios.post(`/api/grove/unzip`, fd);
      if (!res.data.status) {
        const { cloud, fileNamesJson } = this.state;
        if (res.data.gzip) {
          _.assign(cloud, _.cloneDeep(res.data.gzip));
          _.assign(fileNamesJson, _.cloneDeep(res.data.gzip));
        }
        let state = stateTmp || {};
        _.assign(state, { cloud: cloud, fileNamesJson: this.setFileNamesJsonItem() })
        !stateTmp && this.setState(state);
        this.setWrapperLoading(false);
      }
      res.data.message && showToast(res.data.message, res.data.status ? "error" : "success");
    })
  }
  /**
   * 
   * @param {DefaultFileData} hoverFile 
   */
  getTextContent = async (hoverFile) => {
    let content = await this.getFileContent(hoverFile)
    const { uploadUri } = this.state;
    if (isFileNoEdit(hoverFile)) {
      if (isFileImage(hoverFile)) {
        content = JSON.stringify(imageDataFunc(content, hoverFile.name), null, 2);
      } else if (isFileGzip(hoverFile)) {
        content = JSON.stringify(gzipDataFunc(content, hoverFile.name), null, 2);
      } else if (isFileZip(hoverFile)) {
        content = JSON.stringify(zipDataFunc(content, hoverFile.name), null, 2);
      } else if (isFileDb(hoverFile)) {
        content = JSON.stringify(dbDataFunc(content, hoverFile.name), null, 2);
      } else if (isFileExcel(hoverFile)) {
        content = JSON.stringify(excelDataFunc(uploadUri + hoverFile.fileKey, hoverFile.name), null, 2);
      } else if (isFileArrow(hoverFile)) {
        content = JSON.stringify(arrowDataFunc(uploadUri + hoverFile.fileKey, hoverFile.name), null, 2);
      } else if (isFileAudio(hoverFile)) {
        content = JSON.stringify(audioDataFunc(uploadUri + hoverFile.fileKey, hoverFile.name), null, 2);
      } else if (isFileVideo(hoverFile)) {
        content = JSON.stringify(videoDataFunc(uploadUri + hoverFile.fileKey, hoverFile.name), null, 2);
      } else if (isFileGraphxr(hoverFile)) {
        content = JSON.stringify(graphxrDataFunc(uploadUri + hoverFile.fileKey, hoverFile.name), null, 2);
      }
    } else if (isFileJs(hoverFile) || isFileHtml(hoverFile)) {
      content = content
    } else if (isFileSvg(hoverFile)) {
      content = content
    } else if (isFileCsv(hoverFile)) {
      content = content
    } else {
      content = JSON.stringify(JSON.parse(content), null, 2)
    }
    return content;
  }

  renameFileOrFolder = (hoverFile) => {
    const { activeFileKey, activeFolderKey, selectType, fileNamesJson } = this.state;
    if (!hoverFile) {
      if (selectType === SelectType.file) {
        hoverFile = fileNamesJson[activeFileKey];
      } else {
        hoverFile = fileNamesJson[activeFolderKey];
      }
      if (!hoverFile) {
        return;
      }
    }
    if (isFile(hoverFile)) {
      let state = { newFile: false, rename: true, renameFileKey: hoverFile.fileKey };
      this.setState(state, () => {
        openDirs(hoverFile.fileKey);
        this.ref.current.querySelector(".file-name.rename .file-name-rename").focus();
      })
    } else if (isDir(hoverFile)) {
      this.setState({
        newFile: false, rename: true, renameFileKey: null,
        selectType: SelectType.dir, activeFolderKey: hoverFile.folderKey
      }, () => {
        this.ref.current.querySelector(".folder-name-tag.rename .folder-name-rename").focus();
      })
    }
  }

  activeIntoView = (tabs, folder = false) => {
    setTimeout(() => {
      let target = !folder && this.ref.current.querySelector(`.file${tabs ? ".tabs-active" : ".active.deep"}`) ||
        this.ref.current.querySelector(`details.active.deep`);
      target && !isInViewport(target, 155) && target.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" })
    });
  }

  handleSetImageOk = async (blob) => {
    const { previewImg } = this.state;
    let activeFile = window.currentEditor.getFileData();
    if (!activeFile) {
      return showToast("can not find active File", "error");
    }
    let imageFileName = previewImg.name || previewImg.path;
    if (!imageTypeReg.test(imageFileName.toLowerCase())) {
      return showToast("not image file image type not right", "error");
    }
    let fileName = `${activeFile.name}${ImageType}`;
    let fileKey = getFileKey(getFileKey(activeFile.fileKey, FILESDIR), fileName);
    let file = {
      fileKey: fileKey, name: fileName, selected: false, type: getFileType(fileKey), mtimeMs: new Date().getTime()
    };
    let state = { previewImgVersion: this.state.previewImgVersion + 1 };
    this.setWrapperLoading(true, async () => {
      if (!activeFile.previewImageUpload) {
        await this.updateFileSettings(activeFile, undefined, true, state);
      }
      await this.uploadMdFile(file, undefined, await readAsDataURL(blob || previewImg), state);
      this.setState(state);
      this.setWrapperLoading(false);
    })
  }

  /**
   * 
   * @param {*} stateTmp 
   */
  screenshotAsPreviewImage = async (stateTmp) => {
    let activeFile = window.currentEditor.getFileData();
    if (!activeFile) {
      return showToast("can not find active File", "error");
    }
    let editorjsEle = document.querySelector(`.editorjs-wrapper.${window.currentEditor.getTransferKey()}>.editorjs`);
    editorjsEle.classList.add("print");
    let canvas = await html2canvas(editorjsEle, {
      width: 390, height: 400, backgroundColor: getComputedStyle(document.body).getPropertyValue('--background-color')
    });
    if (!canvas) {
      editorjsEle.classList.remove("print");
      return showToast("screenshot fail!", "error");
    }
    editorjsEle.classList.remove("print");
    let imgData = canvas.toDataURL(`image/${ImageType.substring(1)}`, 1.0);
    let fileName = `${activeFile.name}${ImageType}`;
    let fileKey = getFileKey(getFileKey(activeFile.fileKey, FILESDIR), fileName);
    let file = {
      fileKey: fileKey, name: fileName, selected: false, type: getFileType(fileKey), mtimeMs: new Date().getTime()
    };
    let state = stateTmp || { previewImgVersion: this.state.previewImgVersion + 1 };
    if (activeFile.previewImageUpload) {
      await this.updateFileSettings(activeFile, undefined, false, state);
    }
    await this.uploadMdFile(file, undefined, imgData, state);
    !stateTmp && this.setState(state);
  }

  getNoOperateFileSystem = () => {
    const { notOwn, projectShareUsers } = this.state;
    return notOwn && projectShareUsers[USER_ID] && projectShareUsers[USER_ID].status < ShareProjectCanOptions["Operate file system"];
  }

  /**
   * 
   * @param {DefaultFileData} hoverFile 
   */
  OuterMenus = (hoverFile, props) => {
    if (!hoverFile) {
      return <Menu className={`data-html2canvas-ignore import-export-menu normal-icon tabs-menu`} onClick={(info) => {
        this.menuClickHandler(info, hoverFile)
      }}></Menu>
    }
    const { notOwn } = this.state;
    const noOperateFileSystem = this.getNoOperateFileSystem();
    return <Menu className={`data-html2canvas-ignore import-export-menu normal-icon tabs-menu`} onClick={(info) => {
      this.menuClickHandler(info, hoverFile)
    }}>
      {!noOperateFileSystem && <Menu.Item key="removeFile"><i className="icon fas fa-trash"></i>Remove File</Menu.Item>}
      {!noOperateFileSystem && <Menu.Item key="duplicateFile" ><i className="icon fas fa-copy"></i>Duplicate File</Menu.Item>}
      {isFileEditorJs(hoverFile) ?
        <SubMenu key="downloadFile" popupOffset={[0, -18]}
          title={<span><i className="icon fas fa-file-download"></i>Download File</span>}>
          <Menu.Item key="downloadGrove">.grove</Menu.Item>
          <Menu.Item key="downloadMd">.md</Menu.Item>
        </SubMenu> :
        <Menu.Item key="downloadGrove"><i className="icon fas fa-file-download"></i>Download File</Menu.Item>
      }
      <Menu.Item key="DisplaySource" ><i className="icon fas fa-code"></i>Display source</Menu.Item>
      {this.isNeedUpload(hoverFile) && !noOperateFileSystem &&
        <Menu.Item key="revertFile"><i className="icon fas fa-copy"></i>Revert File</Menu.Item>}
      {!noOperateFileSystem &&
        <Menu.Item key="renameFile"><i className="icon fas fa-i-cursor"></i>Rename File</Menu.Item>}
      {!isFileNoEdit(hoverFile) &&
        <Menu.Item key="OpenChanges" ><i className="icon fas fa-not-equal"></i>Open Changes</Menu.Item>}
      {isFileEditorJs(hoverFile) && !hoverFile.shareData && !noOperateFileSystem &&
        (this.isNeedUpload(hoverFile) ?
          <Menu.Item key="upload"><i className="icon fas fa-cloud-upload-alt"></i>Upload</Menu.Item> :
          <Menu.Item key="publish"><i className="icon fas fa-globe"></i>Publish</Menu.Item>)}
      {isFileEditorJs(hoverFile) &&
        <Menu.Item key="VersionControl" ><i className="icon fas fa-code-branch"></i>Version Control</Menu.Item>}
      {isFileEditorJs(hoverFile) && !!hoverFile.shareData && hoverFile.versionMtimeMs !== hoverFile.mtimeMs && !noOperateFileSystem &&
        (this.isNeedUpload(hoverFile) ?
          <Menu.Item key="upload"><i className="icon fas fa-cloud-upload-alt"></i>Upload</Menu.Item> :
          <Menu.Item key="republish"><i className="icon fas fa-globe"></i>Republish</Menu.Item>)}
      {isFileEditorJs(hoverFile) && !!hoverFile.shareData && !noOperateFileSystem &&
        <Menu.Item key="unpublish" ><i className="icon fas fa-lock"></i>Unpublish</Menu.Item>}
      {isFileEditorJs(hoverFile) && !noOperateFileSystem && <Menu.Item key="SetPreviewImage"><i className="icon far fa-image"></i>Set Preview Image</Menu.Item>}
      {(isFileGzip(hoverFile) || isFileZip(hoverFile)) && !noOperateFileSystem &&
        <Menu.Item key="decompression"><i className="icon fas fa-box-open"></i>Decompression</Menu.Item>}
      {!noOperateFileSystem &&
        <Menu.Item key="absolutePath"><i className="icon fas fa-map-marker-alt"></i>Copy Absolute Path</Menu.Item>}
    </Menu>
  }

  focusDrawer = () => {
    const { currentEditor } = this.state;
    if (!currentEditor || !currentEditor.getFileData()) {
      return;
    }
    let cb = (fileKey, tabs = false) => {
      if (fileKey) {
        openDirs(fileKey);
        currentEditor.focusFolder(() => {
          this.activeIntoView(tabs);
          if (currentEditor.isCommon() && this.state.selectType === SelectType.dir) {
            this.setState({ selectType: SelectType.file });
          }
        }, tabs);
      }
    }
    if (currentEditor.isCommon()) {
      const { activeFileKey } = this.state;
      cb(activeFileKey);
    } else {
      cb(currentEditor.getFileData().fileKey, true)
    }
  }

  InnerMenus = (props) => {
    const { noOperateFileSystem } = props;
    const { rightMenuVisible, multipleChoice } = this.state;
    this.rightMenuFile = this.hoverFile;
    let hoverFile = this.rightMenuFile;
    return multipleChoice ?
      <Menu className={`right-menu-menu`} onClick={this.menuClickFunc}>
        <SubMenu key="downloadSelectedFiles" className={rightMenuVisible ? "aaa" : "hide"} popupOffset={[0, -18]}
          title={<span><i className="icon fas fa-download"></i>Download selected files</span>}>
          <Menu.Item key="downloadSelectedGrove">All .grove</Menu.Item>
          <Menu.Item key="downloadSelectedMd">All .md</Menu.Item>
        </SubMenu>
        <Menu.Item key="cancelMultipleChoice" ><i className="icon far fa-square"></i>Cancel multiple choice</Menu.Item>
        {!noOperateFileSystem &&
          <Menu.Item key="removeFiles" ><i className="icon fas fa-trash"></i>Remove Files</Menu.Item>}
      </Menu> : ((hoverFile && isFile(hoverFile)) ?
        <Menu className={`right-menu-menu`} onClick={this.menuClickFunc}>
          <Menu.Item key="file" className="right-menu-title" title={hoverFile.fileKey} disabled >{hoverFile.name}</Menu.Item>
          <Menu.Divider />
          {!noOperateFileSystem &&
            <Menu.Item key="removeFile"><i className="icon fas fa-trash"></i>Remove File</Menu.Item>}
          {!noOperateFileSystem &&
            <Menu.Item key="duplicateFile"  ><i className="icon fas fa-copy"></i>Duplicate File</Menu.Item>}
          {isFileEditorJs(hoverFile) ?
            <SubMenu key="downloadFile" popupOffset={[0, -18]}
              title={<span><i className="icon fas fa-file-download"></i>Download File</span>}>
              <Menu.Item key="downloadGrove">.grove</Menu.Item>
              <Menu.Item key="downloadMd">.md</Menu.Item>
            </SubMenu> :
            <Menu.Item key="downloadGrove"><i className="icon fas fa-file-download"></i>Download File</Menu.Item>
          }
          {isFileHtml(hoverFile) && <Menu.Item key="convertToDoc"><i className="icon fas fa-file-download"></i>convert To Doc</Menu.Item>}
          <Menu.Item key="DisplaySource" ><i className="icon fas fa-code"></i>Display source</Menu.Item>
          {this.isNeedUpload(hoverFile) && !noOperateFileSystem &&
            <Menu.Item key="revertFile"><i className="icon fas fa-copy"></i>Revert File</Menu.Item>}
          {!noOperateFileSystem &&
            <Menu.Item key="renameFile"><i className="icon fas fa-i-cursor"></i>Rename File</Menu.Item>}
          {!isFileNoEdit(hoverFile) &&
            <Menu.Item key="OpenChanges" ><i className="icon fas fa-not-equal"></i>Open Changes</Menu.Item>}
          {isFileEditorJs(hoverFile) && !hoverFile.shareData && !noOperateFileSystem &&
            (this.isNeedUpload(hoverFile) ?
              <Menu.Item key="upload"><i className="icon fas fa-cloud-upload-alt"></i>Upload</Menu.Item> :
              <Menu.Item key="publish"><i className="icon fas fa-globe"></i>Publish</Menu.Item>)}
          {isFileEditorJs(hoverFile) &&
            <Menu.Item key="VersionControl"><i className="icon fas fa-code-branch"></i>Version Control</Menu.Item>}
          {isFileEditorJs(hoverFile) && !!hoverFile.shareData && hoverFile.versionMtimeMs !== hoverFile.mtimeMs && !noOperateFileSystem &&
            (this.isNeedUpload(hoverFile) ?
              <Menu.Item key="upload"><i className="icon fas fa-cloud-upload-alt"></i>Upload</Menu.Item> :
              <Menu.Item key="republish"><i className="icon fas fa-globe"></i>Republish</Menu.Item>)}
          {isFileEditorJs(hoverFile) && !!hoverFile.shareData && !noOperateFileSystem &&
            <Menu.Item key="unpublish"><i className="icon fas fa-lock"></i>Unpublish</Menu.Item>}
          {isFileEditorJs(hoverFile) && !noOperateFileSystem &&
            <Menu.Item key="SetPreviewImage"><i className="icon far fa-image"></i>Set Preview Image</Menu.Item>}
          <Menu.Item key="multipleChoice" ><i className="icon far fa-check-square"></i>Multiple choice</Menu.Item>
          {(isFileGzip(hoverFile) || isFileZip(hoverFile)) && !noOperateFileSystem && <Menu.Item key="decompression" ><i className="icon fas fa-box-open"></i>Decompression</Menu.Item>}
          {!noOperateFileSystem && <Menu.Item key="absolutePath"><i className="icon fas fa-map-marker-alt"></i>Copy Absolute Path</Menu.Item>}
        </Menu> :
        (hoverFile ? <Menu className={`right-menu-menu`} onClick={this.menuClickFunc}>
          <Menu.Item key="file" className="right-menu-title" disabled >{hoverFile.folderKey}</Menu.Item>
          <Menu.Divider />
          {!noOperateFileSystem && <Menu.Item key="removeFile"><i className="icon fas fa-trash"></i>Remove Folder</Menu.Item>}
          {!noOperateFileSystem && <Menu.Item key="duplicateFile"><i className="icon fas fa-copy"></i>Duplicate Folder</Menu.Item>}
          <SubMenu key="downloadFolder" className={rightMenuVisible ? "" : "hide"} popupOffset={[0, -18]}
            title={<span><i className="icon fas fa-download"></i>Download Folder</span>}>
            <Menu.Item key="downloadGrove">All .grove</Menu.Item>
            <Menu.Item key="downloadMd">All .md</Menu.Item>
          </SubMenu>
          {!noOperateFileSystem && <Menu.Item key="renameFolder"><i className="icon fas fa-i-cursor"></i>Rename Folder</Menu.Item>}
          {!noOperateFileSystem && <Menu.Item key="newFolder"><i className="icon fas fa-folder-plus"></i>New Folder</Menu.Item>}
          <Menu.Item key="multipleChoice" ><i className="icon far fa-check-square"></i>Multiple choice</Menu.Item>
          {!noOperateFileSystem && <Menu.Item key="absolutePath"><i className="icon fas fa-map-marker-alt"></i>Copy Absolute Path</Menu.Item>}
        </Menu> : <Menu className={`right-menu-menu`} onClick={this.menuClickFunc}>
        </Menu>))
      ;
  }

  render() {
    const { activeFileKey, fileVersion, activeFolderKey, selectType, currentEditor,
      rename, renameFileKey, newFile, newFolder, fileNamesJson, className, cloud,
      rightMenuVisible, uploadUri, multipleChoice, initData, filter, notOwn, width } = this.state;
    const noOperateFileSystem = this.getNoOperateFileSystem();
    // if (!initData) {
    //   return <div>Loading...</div>
    // }
    const displayObj = getDisplayDirObj(fileNamesJson);
    window.displayObj = displayObj
    window.DirObj = DirObj;
    window.FileObj = FileObj;
    const rootObj = new DirObj(null);
    window.rootObj = rootObj;
    rootObj.files["root"] = displayObj;
    let disabled = (activeFileKey && selectType === SelectType.file ||
      selectType === SelectType.dir) ? false : true;
    let maxIconN = Math.floor((width - 56) / (28.8 + 8));
    maxIconN = maxIconN <= 0 ? 1 : maxIconN;
    let flexIcons = [];
    if (!noOperateFileSystem) {
      flexIcons = flexIcons.concat(<i key="file" className="icon fas fa-file" disabled={disabled}
        title={ShortcutTitle(file_actions_keys['New File'])}
        onClick={() => {
          const { selectType, activeFolderKey, activeFileKey } = this.state;
          let activeKey = selectType === SelectType.dir ? activeFolderKey : activeFileKey;
          openDirs(activeKey)
          this.setState({ rename: false, renameFileKey: null, newFile: true, newFolder: false }, () => {
            document.querySelector(".new-file.active input").focus();
          })
        }}></i>,
        <i key="folder" className="icon fas fa-folder-plus" disabled={disabled} title={`New Folder`}
          onClick={(e) => {
            const { selectType, activeFolderKey, activeFileKey } = this.state;
            let activeKey = selectType === SelectType.dir ? activeFolderKey : activeFileKey;
            openDirs(activeKey);
            this.setState({ rename: false, renameFileKey: null, newFile: false, newFolder: true }, () => {
              document.querySelector(".new-folder.active input").focus();
            })
          }}></i>,
        <Spinner key="trash" type="i" className="icon fas fa-trash"
          disabled={disabled}
          title={ShortcutTitle(file_actions_keys['Remove Selected File/Folder'])}
          onClick={(e, setLoading) => {
            if (activeFileKey && selectType === SelectType.file) {
              this.removeFileFunc(e, setLoading);
            } else if (selectType === SelectType.dir) {
              this.removeFolderFunc(e, setLoading);
            } else {
              setLoading && setLoading(false);
            }
          }}></Spinner>,
        <i key="copy" className="icon fas fa-copy"
          disabled={(activeFileKey && selectType === SelectType.file ||
            activeFolderKey && selectType === SelectType.dir)
            ? false : true}
          title={ShortcutTitle(file_actions_keys["Duplicate Selected File/Folder"])}
          onClick={() => {
            this.openDuplicateDrawer();
          }} ></i>)
    }
    flexIcons = flexIcons.concat(<i key="export" className="icon fas fa-file-export"
      disabled={activeFileKey && selectType === SelectType.file ? false : true}
      title={ShortcutTitle(file_actions_keys["Download Current File"])}
      onClick={() => this.onExport(ExportType.GROVE)} ></i>,
      <i key="filter" className={`icon fas fa-filter ${filter ? "active" : ""}`} title="Filter Files" onClick={() => {
        this.setState((state) => { return { filter: !state.filter } })
      }}></i>,
      <i key="link" className="icon fas fa-exchange-alt" title="Link With Editor" onClick={() => {
        this.focusDrawer();
      }}></i>,
      <i key="collapse" className="icon fas fa-minus-square" title="Collapse All" onClick={() => {
        document.querySelector(".folder").querySelectorAll("details").forEach((details) => {
          if (!details.getAttribute("data-link")) {
            return;
          }
          details.removeAttribute("open");
        })
      }}></i>)
    let dropIcons = undefined;
    if (maxIconN > 0 && maxIconN < flexIcons.length) {
      let rms = flexIcons.splice(maxIconN - 1, flexIcons.length - (maxIconN - 1));
      dropIcons = <Menu forceSubMenuRender={true} className="data-html2canvas-ignore import-export-menu normal-icon">
        {_.map(rms, (curr, index) => {
          return <Menu.Item key={"key" + index}>{curr}</Menu.Item>
        })}
      </Menu>
    }
    return (
      <React.Fragment>
        <div className={`folder ${className || ''}`} ref={this.ref}>
          <Affix className={'folder-affix'} target={() => document.querySelector("#folder .ant-drawer-body")} offsetTop={0}>
            <div>
              <div className="header d-flex justify-content-start">
                <div className=" d-flex flex-rows">
                  {flexIcons}
                  {dropIcons && <Dropdown overlay={dropIcons} placement="bottomCenter" trigger={['click']}>
                    <EllipsisOutlined className="icon" title="more" />
                  </Dropdown>}
                </div>
              </div>
            </div>
          </Affix>
          {filter && <Filter react_component={this}></Filter>}
          <MyDropzone className="dropzone-div editor-dropzone" handleFilesFunc={(acceptedFiles) => {
            importFilesAction(acceptedFiles)
          }} accept={AllAccept}>
            <Dropdown overlayClassName="right-menu" visible={rightMenuVisible} onVisibleChange={(visible) => {
              this.setState({ rightMenuVisible: visible })
            }} arrow={true} overlay={this.InnerMenus({ noOperateFileSystem })} getPopupContainer={() => this.ref.current} trigger={['contextMenu']}>
              <div>
                <this.RecursionLocal local={rootObj}></this.RecursionLocal>
              </div>
            </Dropdown>
          </MyDropzone>
        </div>
        <ModalComp react_component={this}></ModalComp>
      </React.Fragment >
    );
  }

  /**
   * drag drop function
   * @param {*} e 
   */
  onDropFunc = (e) => {
    const { fileNamesJson } = this.state;
    e.preventDefault();
    let dragFileKey = e.dataTransfer.getData("dragFileKey");
    if ("" !== dragFileKey) {
      let dragFile = fileNamesJson[dragFileKey];
      if (!dragFile) {
        return;
      }
      let referFileKey = e.currentTarget.getAttribute("data-link");
      this.renameOrMoveFileFunc({ target: { value: dragFile.name } }, dragFileKey, referFileKey);
      return;
    }
    let dragFolderKey = e.dataTransfer.getData("dragFolderKey");
    if ("" !== dragFolderKey) {
      let dragFile = fileNamesJson[dragFolderKey];
      if (!dragFile) {
        return;
      }
      let referFileKey = e.currentTarget.getAttribute("data-link");
      this.renameOrMoveFolderFunc({ target: { value: dragFile.name } }, dragFolderKey, referFileKey);
      return;
    }
    // alert("drag drop exception")
  }

  /**
   * get under dir need upload files
   * @param {DirObj} dirObj dir
   * @param {FileObj[]} arr store need upload file
   */
  getNeedUploadArr = (dirObj, arr) => {
    const _self = this;
    _.each(dirObj.files, (v) => {
      if (v instanceof DirObj) {
        _self.getNeedUploadArr(v, arr);
      } else if (v instanceof FileObj) {
        const file = v.file;
        if (this.isNeedUpload(file)) {
          arr.push(v);
        }
      } else {
        console.error("error");
      }
    });
    return arr;
  }

  /**
   * 
   * @param {DefaultFileData} exceptFile 
   * @param {object} stateTmp transfer state that setState later or right now setState in the function
   * @param {boolean} largeCache
   * @param {string} specifyPath
   */
  uploadAllFiles = async (exceptFile, stateTmp, largeCache = false, specifyPath = undefined) => {
    const { fileNamesJson } = this.state;
    let state = stateTmp || {};
    let promiseList = _.reduce(fileNamesJson, (arr, file, fileKey) => {
      if ((!exceptFile || file.fileKey !== exceptFile.fileKey) &&
        file.type !== DefaultFolderData.type && this.isNeedUpload(file)) {
        if (!specifyPath || file.fileKey.startsWith(specifyPath)) {
          arr.push(this.uploadMdFile(file, undefined, undefined, state, DEFAULT_VER, largeCache));
        }
      }
      return arr;
    }, []);
    promiseList.length && await Promise.all(promiseList);
    !stateTmp && !_.isEmpty(state) && this.setState(state);
  }

  isNeedUpload = (file) => {
    if (!file) {
      return false;
    }
    const { cloud, activeFileKey, fileVersion } = this.state;
    const fileKey = file.fileKey;
    const ver = {
      "untracked": !cloud[fileKey],
      "modified": !!cloud[fileKey] && cloud[fileKey].mtimeMs !== file.mtimeMs
    };
    return (ver.untracked || ver.modified) &&
      ((file.fileKey === activeFileKey) ? (fileVersion === DEFAULT_VER) : true)
  }

  NewFileFolder = () => {
    let { newFile, newFolder } = this.state;
    return <div>
      <div className={cx(`new-file`,
        { "active": newFile }
      )}> &nbsp;<input type="text" className="form-control"
        onKeyDown={(e) => {
          if (e.key === 'Escape') {
            this.setState({ newFile: false })
            e.stopPropagation();
          } else if (e.key === "Enter") {
            let newFileName = e.target.value;
            e.target.value = "";
            if (!newFileName.match(fileNameReg)) {
              showToast(MSG.FILE_NAME_BREAK_THE_RULE, "warning");
              return false;
            }
            if ("" === newFileName) {
              showToast(MSG.FILE_NAME_CAN_NOT_EMPLTY, "warning")
              return false;
            }
            if (!isFileEditorJs({ type: getFileType(newFileName) })) {
              this.newFileFunc({ target: { value: newFileName } })
            } else {
              const { activeFileKey, activeFolderKey, selectType } = this.state;
              let referFileKey = selectType === SelectType.dir ? activeFolderKey : activeFileKey;
              let newFileKey = getFileKey(referFileKey, newFileName);
              this.setState({
                modalType: ModalType.NewNotebook,
                modalData: {
                  newFileKey: newFileKey,
                  newFileName: newFileName,
                  cb: (content, state) => {
                    this.newFileFunc({ target: { value: newFileName } }, content, undefined, true, state)
                  }
                }
              });
            }

          }
        }}
        onBlur={(e) => {
          e.target.value = '';
          this.setState({ newFile: false })
        }}
      ></input></div>
      <div className={cx(`new-folder`,
        { "active": newFolder }
      )}> &nbsp;<input type="text" className="form-control"
        onKeyDown={(e) => {
          if (e.key === 'Escape') {
            e.target.value = '';
            this.setState({ newFolder: false })
            e.stopPropagation();
          } else if (e.key === "Enter") {
            this.newFolderFunc(e);
          }
        }}
        onBlur={(e) => {
          e.target.value = '';
          this.setState({ newFolder: false })
        }}
      ></input></div>
    </div>
  }

  /**
   * 
   * @param {DirObj} folder 
   */
  folderHideFunc = (folder) => {
    const { linkedFilesMap, sharedFilesMap, filterType, filter } = this.state;
    let folderKey = folder.dir.folderKey;
    if (!filter || folder.dir.folderKey === rootDir.folderKey) {
      return false;
    }
    switch (filterType) {
      case FILTER_TYPE.links:
        return _.filter(linkedFilesMap, (value, key) => { return key.startsWith(folderKey) }).length === 0;
      case FILTER_TYPE.published:
        /**
         * 
         * @param {DirObj} fd 
         */
        function needDisplay(fd) {
          let ret = false;
          _.each(fd.files, (ff, fileName) => {
            if (ff instanceof DirObj) {
              if (needDisplay(ff)) {
                ret = true;
                return false;
              }
            } else if (ff.file.shareData) {
              ret = true;
              return false;
            }
          });
          return ret;
        }
        return !needDisplay(folder);
      case FILTER_TYPE.shared:
        return _.filter(sharedFilesMap, (value, key) => { return key.startsWith(folderKey) }).length === 0;
    }
  }

  /**
   * 
   * @param {DefaultFileData} file 
   */
  fileHideFunc = (file) => {
    let fileKey = file.fileKey;
    const { linkedFilesMap, sharedFilesMap, filterType, filter } = this.state;
    if (!filter) {
      return false;
    }
    switch (filterType) {
      case FILTER_TYPE.links:
        return !linkedFilesMap[fileKey];
      case FILTER_TYPE.published:
        return !file.shareData;
      case FILTER_TYPE.shared:
        return !sharedFilesMap[fileKey];
    }
  }

  RecursionLocal = (props = { local: {} }) => {
    let _self = this;
    /**@type DirObj */
    let local = props.local;
    let { currentEditor, activeFileKey, fileVersion, activeFolderKey, selectType, hoverFile,
      linkedFilesMap, filter, sharedFilesMap, rename, renameFileKey, uploadUri,
      cloud, fileNamesJson, multipleChoice, notOwn, userId, projectId, shareProjectId, sharedVisible } = this.state;
    const noOperateFileSystem = this.getNoOperateFileSystem();
    let activeKey = selectType === SelectType.dir ? activeFolderKey : activeFileKey;
    if (!local.files) {
      alert("dead while");
      return <div>dead while</div>;
    }
    return _.map(sortDirFileByKey(local.files), (value, key, kv) => {
      if (value instanceof DirObj) {
        let needUploadArr = this.getNeedUploadArr(value, []);
        let folderName = key;
        let dref = React.createRef();
        let folderKey = value.dir.folderKey;
        return <details key={folderKey} ref={dref}
          data-link={folderKey} onToggle={this.toggleFunc}
          className={cx(
            { "local": folderKey === rootDir.folderKey },
            { "active": activeFolderKey === folderKey },
            { "deep": activeFolderKey === folderKey && selectType === SelectType.dir },
            { "hide": this.folderHideFunc(value) }
          )}>
          {/* <Tooltip zIndex={1049} title={<div className="folder-name">
            <div>{folderKey || "root"}</div>
            <div>{value.files ? _.keys(value.files).length : 0} files</div>
          </div>}
            placement="top"
            arrowPointAtCenter
            mouseLeaveDelay={0}
            mouseEnterDelay={MouseEnterDelay}
            getPopupContainer={() => this.ref.current} > */}
          <summary title={decodeURI([`${folderKey || "root"}`, `${value.files ? _.keys(value.files).length : 0} files`].join("%0A"))} data-link={folderKey}
            draggable={noOperateFileSystem ? "false" : "true"}
            onMouseOver={() => {
              this.hoverFile !== value.dir && (this.hoverFile = value.dir);
            }}
            onDrop={this.onDropFunc}
            onDragOver={(e) => {
              e.preventDefault();
            }}
            onDragStart={(e) => {
              e.dataTransfer.setData("dragFolderKey", e.currentTarget.getAttribute("data-link"))
            }}>
            <div className="form-check-inline">
              <label className="form-check-label">
                <input onChange={(e) => {
                  let details = dref.current;
                  if (e.target.checked) {
                    details.querySelectorAll(".form-check-input").forEach((check) => {
                      check.checked = true;
                    })
                  } else {
                    details.querySelectorAll(".form-check-input").forEach((check) => {
                      check.checked = false;
                    })
                  }
                  loopParentChecked(details, e.target.checked);
                }}
                  onClick={(e) => { e.stopPropagation() }}
                  type="checkbox"
                  className={`form-check-input ${!multipleChoice ? "hide" : ""}`}
                  value={folderKey} />
              </label>
            </div>
            <div className={cx(`folder-name-tag`,
              { "rename": activeFolderKey === folderKey && selectType === SelectType.dir && rename && !renameFileKey },
            )}>
              <input className="form-control folder-name-rename" type="text" defaultValue={folderName}
                onKeyDown={(e) => {
                  if (e.key === "Escape") {
                    e.target.value = folderName;
                    this.setState({ rename: false })
                    e.stopPropagation();
                  } else if (e.key === "Enter") {
                    this.renameOrMoveFolderFunc(e, folderKey);
                  }
                }}
                onBlur={(e) => { e.target.value = folderName; this.setState({ rename: false }) }}
              ></input>
            </div>
            {folderName}
            <label className="icons">
              <Spinner type="i"
                className={cx(`icon fas fa-cloud-upload-alt`,
                  { "hide": (needUploadArr.length === 0) })}
                title={`Upload ${needUploadArr.length} files`}
                onClick={(e, setLoading) => {
                  let promiseList = [];
                  _.each(needUploadArr, v => {
                    promiseList.push(this.uploadMdFile(v.file));
                  })
                  if (promiseList.length === 0) {
                    setLoading && setLoading(false);
                    return;
                  }
                  Promise.all(promiseList).then(values => {
                    setLoading && setLoading(false)
                  });
                  e.stopPropagation();
                }}></Spinner>
              <i className="f7" title={`${needUploadArr.length} files modified`}>
                {`${needUploadArr.length === 0 ? "" : "M" + needUploadArr.length}`}
              </i>
            </label>
            {/* </div> */}
          </summary>
          {/* </Tooltip> */}
          {activeKey === folderKey && <_self.NewFileFolder></_self.NewFileFolder>}
          {value.files && _.keys(value.files).length > 0 && <_self.RecursionLocal local={value}></_self.RecursionLocal>}
        </details >
      } else if (value instanceof FileObj) {
        /**
         * @type DefaultFileData 
         */
        let file = value.file;
        let fileName = file.name;
        let fileKey = file.fileKey;
        let mtimeMs = file.mtimeMs;
        let ver = {
          "untracked": !cloud[fileKey],
          "modified": !!cloud[fileKey] && cloud[fileKey].mtimeMs !== file.mtimeMs
        };
        let published = (fileKey === activeFileKey ? fileVersion === DEFAULT_VER : true) && file.shareData;
        let shared = (fileKey === activeFileKey ? fileVersion === DEFAULT_VER : true) && sharedFilesMap[fileKey];
        return <div key={fileKey}
          data-link={fileKey}
          className={cx("file",
            {
              "tabs-active": currentEditor && !currentEditor.isCommon() && currentEditor.getFileData()
                && currentEditor.getFileData().fileKey === fileKey
            },
            { "active": activeFileKey === fileKey },
            { "deep": activeFileKey === fileKey && selectType === SelectType.file },
            { "hide": this.fileHideFunc(file) }
          )}
          draggable={noOperateFileSystem ? "false" : "true"}
          onMouseOver={() => {
            this.hoverFile !== file && (this.hoverFile = file);
          }}
          onDrop={this.onDropFunc}
          onDragOver={(e) => {
            e.preventDefault();
          }}
          onDragStart={(e) => {
            e.dataTransfer.setData("dragFileKey", e.currentTarget.getAttribute("data-link"))
          }}
          onClick={async (e) => {
            if (e.target.tagName === 'I') return;
            // if (this.state.activeFileKey === fileKey) {
            //   if (this.state.selectType !== SelectType.file) {
            //     this.setState({ selectType: SelectType.file })
            //   }
            //   return;
            // }
            if (macMetaOrCtrl(e) && !e.shiftKey && !e.altKey) {
              await this.switchFileTagEditor(fileKey)
            } else {
              this.focusActive = false;
              let obj = { fileKey, version: DEFAULT_VER };
              const { notOwn } = this.state;
              if (notOwn) {
                _.assign(obj, {
                  tag: TAG_TYPE.projectShare,
                  mtimeMs: file.mtimeMs,
                  cache: this.isNeedUpload(file) ? {} : { mtimeMs: file.mtimeMs },
                });
              }
              await this.handleSearch(this.getInfo(obj), false);
            }
          }}>
          {/* <Tooltip zIndex={1049} title={<div className="file-title">
            <div>{fileKey}</div>
            <div>{formatTime(file.mtimeMs)}</div>
            {ver.untracked && <div>~untracked</div>}
            {ver.modified && <div>~modified</div>}
            {file.size !== undefined && <div>{formatSizeUnits(file.size)}</div>}
            {file.version !== undefined && isFileEditorJs(file) && <div>version:{file.version}
              {file.messages && file.messages[file.version] && `(${file.messages[file.version]})` || ""}
            </div>}
            {sharedFilesMap[fileKey] && <div>{`Shared ${_.keys(sharedFilesMap[fileKey]).length} users`}</div>} */}
          {/* {file.final !== undefined && isFileEditorJs(file) && <div>final:{file.final}</div>} */}
          {/* </div>}
            placement="top"
            arrowPointAtCenter
            mouseLeaveDelay={0}
            mouseEnterDelay={MouseEnterDelay}
            getPopupContainer={() => this.ref.current} > */}
          <div title={decodeURI(
            [`${fileKey}%0A`,
            `${formatTime(file.mtimeMs)}%0A`,
            `${ver.untracked && "~untracked%0A" || ""}`,
            `${ver.modified && "~modified%0A" || ""}`,
            `${file.size !== undefined && `${formatSizeUnits(file.size)}%0A` || ""}`,
            `${file.version !== undefined && isFileEditorJs(file) && `version:${file.version}${file.messages && file.messages[file.version] && `(${file.messages[file.version]})` || ""}%0A` || ""}`,
            `${sharedFilesMap[fileKey] && `Shared ${_.keys(sharedFilesMap[fileKey]).length} users%0A` || ""}`].join(""))}>
            <div className={cx(`file-name`,
              { "active": activeFileKey === fileKey },
              { "rename": rename && renameFileKey === fileKey },
              ver
            )}>
              <div className="form-check-inline">
                <label className="form-check-label">
                  <input onClick={(e) => { e.stopPropagation() }}
                    type="checkbox"
                    className={`form-check-input ${!multipleChoice ? "hide" : ""}`}
                    value={fileKey}
                    onChange={(e) => {
                      loopParentChecked(e.target, e.target.checked);
                    }} />{fileName}{activeFileKey === fileKey ? `@${fileVersion}` : ""}
                </label>
              </div>
              <input className="form-control file-name-rename" type="text" defaultValue={fileName}
                onKeyDown={(e) => {
                  if (e.key === "Escape") {
                    e.target.value = fileName;
                    this.setState({ rename: false, renameFileKey: null })
                    e.stopPropagation();
                  } else if (e.key === "Enter") {
                    this.renameOrMoveFileFunc(e, fileKey);
                  }
                }}
                onBlur={(e) => { e.target.value = fileName; this.setState({ rename: false, renameFileKey: null }) }}
              ></input>
              <div className={`icons`} onClick={e => e.stopPropagation()}>
                {isFileEditorJs(file) && !noOperateFileSystem && <Share projectId={projectId}
                  className={cx({ "icon-hover": !shared })}
                  title={shared ? `Shared ${_.keys(sharedFilesMap[fileKey]).length} users` : "Share to user"}
                  shareProjectId={shareProjectId}
                  visible={sharedVisible}
                  projectName={this.props.projectName}
                  userId={userId}
                  value={value}
                  refreshShareds={(shareds, cb) => {
                    this.refreshShareds(fileKey, shareds, cb);
                  }}
                ></Share>}
                { /* For Unlink file*/
                  !linkedFilesMap[fileKey] && !noOperateFileSystem && <i title={`Link GraphXR`}
                    className={cx("f7 icon fas fa-link", { "icon-hover": true })}
                    onClick={() => {
                      this.handleAddLinkedToGraphXR(this.getInfo({ fileKey }));
                    }} />
                }
                { /* For Linked file*/
                  linkedFilesMap[fileKey] && !noOperateFileSystem &&
                  <i title={`Un-link GraphXR`}
                    className={cx("f7 icon fas fa-unlink")}
                    onClick={() => {
                      this.handleRemoveLinkedToGraphXR(fileKey);
                    }} />
                }
                <Spinner type="i"
                  className={cx(`f7 icon fas fa-cloud-upload-alt`,
                    { "hide": !(ver.untracked || ver.modified) })}
                  title="Upload to server"
                  onClick={(e, setLoading) => {
                    this.uploadMdFile(file, setLoading);
                    e.stopPropagation();
                  }}></Spinner>
                {ver.modified && <i className="f7 icon" title={`Modified`}>
                  {`M`}</i>}
                {ver.untracked && <i className="f7 icon" title={`Untracked`}>
                  {`U`}</i>}
                {isFileEditorJs(file) && !noOperateFileSystem && <Spinner type="i" title={published ? `Published ${codeTime(new Date().getTime(), file.versionMtimeMs)}` : "Publish"}
                  className={cx(`f7 icon fas fa-globe`, { "icon-hover": !published })}
                  onClick={async (e, setLoading) => {
                    await this.publishFunc(file)
                    setLoading(false);
                  }}></Spinner>}
              </div>
            </div>
          </div>
          {/* </Tooltip> */}
          {activeKey === fileKey && <_self.NewFileFolder></_self.NewFileFolder>}
        </div>
      } else {
        console.error("error value!");
        return;
      }
    })
  }
}