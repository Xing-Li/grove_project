import axios from 'axios';
import _ from "lodash";
import PropTypes from "prop-types";
import React from "react";
import { io } from "socket.io-client";
import { ContentType, showConfirm, showToast, ReadWriteOptions } from "../util/utils";
import actions from "../define/Actions";
import editor, { CommonEditor } from "../commonEditor";
import Common from '../util/Common';
import { DEBUG_CHAT } from 'util/helpers';
const { ShapeChatData, ShapeChatInfo, ShapeShared, USER_ID,
    USER_NAME, DefaultFileData, DefaultShareData, getSkey, getUploadUri, getMasterUserId, COMMON, noSharedProjectIds,
} = require("../util/localstorage");
const {
    cm_login_project_room, sm_login_project_room,
    cm_leave_project_room, sm_leave_project_room,
    cm_login_room, sm_login_room,
    cm_leave_room, sm_leave_room,
    cm_kickout_room, sm_kickout_room,
    cm_apply_edit, sm_apply_edit,
    cast_user_status, cast_file_status, cast_project_user_status,
    UserStatus, Msg_UserStatus,
    SHAREDS_TYPE, Msg_Error, ErrorId, FileStatus,
    Msg_FileStatus, cast_project_status, ProjectStatus, ShareProjectCanOptions, DefaultShareProjectCanOption
} = require("../util/MsgType");
const ShapeSocket = {
    userId: "String",
    userName: "String",
    projectRoomId: "String",
    roomId: "String",
    pro_user_status: 0,
    status: 0,
    lineId: 0
}
export default class Chat extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        info: PropTypes.object,
        linkData: PropTypes.object,
        fileData: PropTypes.object,
        currentEditor: PropTypes.object,
    };
    constructor(props) {
        super(props);
        const { info, linkData, fileData, currentEditor } = this.props;
        this.state = {
            /**
             * chat info, undefined means not shared to others
             * @type ShapeChatInfo
             */
            info,
            /**@type DefaultShareData */
            linkData,
            /**@type DefaultFileData */
            fileData,
            /**all users chat data in room
             * <userId, chat data>
             * @type Object.<string, ShapeChatData> 
             */
            users: {},
            /**all users chat data in project room
             * <userId, chat data>
             * @type Object.<string, ShapeChatData> 
             */
            projectShareUsers: {},
            trigger: null,
            view_code: false,
            fileStatus: FileStatus.idle,
            /**@type Object.<string,ShareProjectCanOptions> */
            usersSharePermission: editor.getSettings().getUsersSharePermission(),
            swProject: {
                /** @type {string} current project Id*/skey: getSkey(),
                /** @type {string} current project master user id*/masterUserId: getMasterUserId()
            },
            /**@type {CommonEditor} */
            currentEditor,
        }
        this.triggerElement = document.createElement("div");
        this.view_codeElement = document.createElement("div");
        this.usersSharePermissionElement = document.createElement("div");
        this.swProjectElement = document.createElement("div");
        this.projectShareUsersElement = document.createElement("div");
        this.isHaveMe = false;
        this.haveEverLoginProject = false;
    }

    async componentDidUpdate(prevProps, prevState) {
        let state = {};
        if (prevProps.className !== this.props.className) {
            _.assign(state, { className: this.props.className });
        }
        if (prevProps.linkData !== this.props.linkData) {
            _.assign(state, { linkData: this.props.linkData });
        }
        if (prevProps.fileData !== this.props.fileData) {
            _.assign(state, { fileData: this.props.fileData });
        }
        if (prevProps.currentEditor !== this.props.currentEditor) {
            await this.clearEditorCache();
            _.assign(state, { currentEditor: this.props.currentEditor });
        }
        if (prevProps.info !== this.props.info ||
            !_.isEqual(prevProps.info, this.props.info)) {
            this.handleLeaveRoom();
            _.assign(state, { info: this.props.info, fileStatus: FileStatus.idle });
        }
        if (!_.isEmpty(state)) {
            this.setState(state, async () => {
                if (~_.keys(state).indexOf("info")) {
                    /**@type ShapeChatInfo */
                    let info = state.info;
                    if (info) {
                        await this.getAllShareds();
                        !this.haveEverLoginProject && this.handleLoginProjectRoom();
                        this.handleLoginRoom();
                    }
                }
            });
        }
    }

    async componentWillUnmount() {
        actions.deleteCache(this.triggerElement);
        actions.deleteCache(this.view_codeElement);
        actions.deleteCache(this.usersSharePermissionElement);
        actions.deleteCache(this.swProjectElement);
        actions.deleteCache(this.projectShareUsersElement);
        await this.clearEditorCache();
        this.handleLeaveRoom();
        this.handleLeaveProjectRoom();
    }

    async clearEditorCache() {
        const { currentEditor, info } = this.state;
        if (currentEditor.get()) {
            if (currentEditor.getContentType() == ContentType.Link) {
                currentEditor.setNeedRefresh(true);
            }
            if (info && !currentEditor.getReadOnly()) {
                currentEditor.setInitEdit(true);
                // await currentEditor.setReadOnly(true);
            }
        }
    }

    /**
     * notice : shareds overwrite shareProject
     */
    getAllShareds = async () => {
        const { info, info: { userId, fileKey, projectId }, usersSharePermission } = this.state;
        info.isMaster = true;
        let users = {};
        let res;
        if (!noSharedProjectIds(projectId)) {
            res = await axios.post("/api/grove/getProjectShareUsers", {
                projectId: projectId
            });
            if (!res.data.status) {
                _.reduce(res.data.content || [], (prev, item, index) => {
                    prev[item.userId._id] = _.assign({}, ShapeChatData, {
                        userId: item.userId._id, userName: `${item.userId.firstName} ${item.userId.lastName}`,
                        email: item.userId.email, isMaster: false, userStatus: UserStatus.offline,
                        status: usersSharePermission[item.userId._id] || DefaultShareProjectCanOption,
                    })
                    return prev;
                }, users)
            }
        }
        let condition = {
            userId, fileKey, projectId,
            type: SHAREDS_TYPE.User_Shared_By_FileKey
        }
        res = await axios.post(`/api/grove/getAllShareds`, condition);
        if (!res.data.status) {
            _.reduce(res.data.content, /**@param {ShapeShared} curr*/(prev, curr, index) => {
                /**@type {ShapeChatData} */
                let info2 = _.cloneDeep(curr);
                info2.userId = info2.toUserId._id;
                info2.userName = `${info2.toUserId.firstName} ${info2.toUserId.lastName}`;
                info2.email = info2.toUserId.email;
                info2.isMaster = false;
                info2.userStatus = UserStatus.offline;
                prev[info2.userId] = info2;
                return prev;
            }, users);
        }
        users[userId] = _.cloneDeep(info)
        this.isHaveMe = users[USER_ID] != null;
        this.setState({ users }, () => {
            actions.variable(actions.types.USERS, [], () => {
                return users;
            });
        });
    }

    handleLoginProjectRoom = () => {
        const { swProject: { skey, masterUserId } } = this.state;
        if (!skey || !masterUserId || noSharedProjectIds(skey) || !this.isHaveMe) {
            return;
        }
        this.haveEverLoginProject = true;
        window._socket &&
            window._socket.emit(cm_login_project_room, {
                projectRoomId: getUploadUri(skey, masterUserId),
                userId: USER_ID,
                userName: USER_NAME,
            });
    }

    handleLeaveProjectRoom = () => {
        const { swProject: { skey, masterUserId } } = this.state;
        if (!skey || !masterUserId || noSharedProjectIds(skey) || !this.isHaveMe) {
            return;
        }
        window._socket &&
            window._socket.emit(cm_leave_project_room, {
                projectRoomId: getUploadUri(skey, masterUserId),
            });
    }

    handleLoginRoom = () => {
        if (!this.state.info || !this.isHaveMe) {
            return;
        }
        const { info: { userId, fileKey, projectId, uploadUri } } = this.state;
        window._socket &&
            window._socket.emit(cm_login_room, {
                roomId: `${uploadUri}${fileKey}`,
                userId: USER_ID,
                userName: USER_NAME,
            });
    };
    handleLeaveRoom = () => {
        if (!this.state.info || !this.isHaveMe) {
            return;
        }
        const { info: { userId, fileKey, projectId, uploadUri } } = this.state;
        if (window._socket) {
            window._socket.emit(cm_leave_room, {
                roomId: `${uploadUri}${fileKey}`
            });
        }
    };
    async componentDidMount() {
        const { info, swProject } = this.state;
        if (swProject.skey) {

        }
        if (info) {
            await this.getAllShareds();
        }
        if (!window._socket) {
            showToast("Connecting to room...");
            window._socket = io("/groveChat/", { path: Common.prefixAliasPath("/socket.io"), 'pingInterval': 5000, 'pingTimeout': 15000 });
        } else {
            this.handleLoginRoom();
        }
        window._socket.on("connect", () => {
            this.handleLoginProjectRoom();
            this.handleLoginRoom();
            showToast("Connected to room 🙌");
        });
        window._socket.on("disconnect", (reason) => {
            if (reason === "io server disconnect") {
                // the disconnection was initiated by the server, you need to reconnect manually
                window._socket.connect();
            }
            // else the socket will automatically try to reconnect
            showToast(reason, "error");
            const { users } = this.state;
            let youUser = _.filter(_.values(users), (user, index) => {
                return user.userId === USER_ID;
            })[0];
            if (youUser) {
                youUser.userStatus = UserStatus.offline;
                this.setState({ users }, () => {
                    actions.variable(actions.types.USERS, [], () => {
                        return users;
                    });
                });
            }
        });
        window._socket.on("connect_error", (error) => {
            showToast("Connection error: " + error);
        });
        window._socket.on("reconnect", () => {
            this.handleLoginProjectRoom();
            this.handleLoginRoom();
            showToast("Reconnecting...")
        });
        window._socket.on(sm_login_room, ({ errorId, roomId, userList }) => {
            if (errorId) {
                if (Msg_Error[errorId] === Msg_Error[102]) {
                    this.handleLoginRoom();
                } else {
                    showConfirm(`Error: "${Msg_Error[errorId]}". Continue login?`, () => {
                        this.handleLoginRoom();
                    })
                }
                return false;
            }
            const { users } = this.state;
            _.map(userList, /**@param { ShapeSocket} user*/(user) => {
                _.each(_.values(users), (u) => {
                    if (user.userId === u.userId) {
                        u.userStatus = user.status;
                    }
                })
            })
            this.setState({ users }, () => {
                actions.variable(actions.types.USERS, [], () => {
                    return users;
                });
                const { currentEditor } = this.state;
                if (currentEditor.getInitEdit()) {
                    currentEditor.setInitEdit(false);
                    this.applyEdit(true);
                }
            });
            DEBUG_CHAT && console.log(`roomId:${roomId}, login room successful`);
        });
        window._socket.on(sm_login_project_room, ({ errorId, projectRoomId, userList }) => {
            if (errorId) {
                if (Msg_Error[errorId] === Msg_Error[100]) {
                    this.handleLoginProjectRoom();
                } else {
                    showConfirm(`Error: "${Msg_Error[errorId]}". Continue login?`, () => {
                        this.handleLoginProjectRoom();
                    })
                }
                return false;
            }
            const { projectShareUsers } = this.state;
            _.map(userList, /**@param { ShapeSocket} user*/(user) => {
                _.each(_.values(projectShareUsers), (u) => {
                    if (user.userId === u.userId) {
                        u.userStatus = user.pro_user_status;
                    }
                })
            })
            this.setState({ projectShareUsers }, () => {
                actions.variable(actions.types.PROJECT_SHARE_USERS, [], () => {
                    return projectShareUsers;
                });
            });
            DEBUG_CHAT && console.log(`projectRoomId:${projectRoomId},login project room successful`);
        });
        window._socket.on(cast_user_status, ({ errorId, roomId, userId, userName, status, lineId, content }) => {
            if (errorId) {
                return showToast(Msg_Error[errorId], "error")
            }
            const { users } = this.state;
            _.each(_.values(users), (u) => {
                if (userId === u.userId) {
                    u.userStatus = status;
                    return false;
                }
            })
            this.setState({ users }, () => {
                actions.variable(actions.types.USERS, [], () => {
                    return users;
                });
            });
            showToast(`${userName} ${Msg_UserStatus[status]}`);
            if (userId === USER_ID && status === UserStatus.kickout) {
                showToast("You have been kicked out of this room.", "error");
            }
        });
        window._socket.on(cast_project_user_status, ({ errorId, projectRoomId, userId, userName, pro_user_status }) => {
            if (errorId) {
                return showToast(Msg_Error[errorId], "error")
            }
            const { projectShareUsers } = this.state;
            _.each(_.values(projectShareUsers), (u) => {
                if (userId === u.userId) {
                    u.userStatus = pro_user_status;
                    return false;
                }
            })
            this.setState({ projectShareUsers }, () => {
                actions.variable(actions.types.PROJECT_SHARE_USERS, [], () => {
                    return projectShareUsers;
                });
            });
            showToast(`${userName} ${Msg_UserStatus[pro_user_status]} project:${projectRoomId}`);
            if (userId === USER_ID && pro_user_status === UserStatus.kickout) {
                showConfirm("This Grovebook was opened in another window. Continue in this window?", () => {
                    this.handleLoginProjectRoom();
                });
            }
        });
        window._socket.on(cast_file_status, ({ errorId, roomId, status }) => {
            if (errorId) {
                return showToast(Msg_Error[errorId], "error")
            }
            const { fileStatus, currentEditor } = this.state;
            if (status === FileStatus.updated) {
                currentEditor.setNeedRefresh(true);
            }
            fileStatus !== status && this.setState({ fileStatus: status }, () => {
                showToast(`${roomId} ${Msg_FileStatus[status]}`);
            });
        });
        window._socket.on(cast_project_status, ({ errorId, projectRoomId, status, data }) => {
            if (errorId) {
                return showToast(Msg_Error[errorId], "error")
            }
            const { } = this.state;
            switch (status) {
                case ProjectStatus.removeFile:
                    editor.msgHandler.removeFile(data.fileKey);
                    break;
                case ProjectStatus.renameFile:
                    editor.msgHandler.renameOrMoveFile(data.oldFileKey, data.newFileData);
                    break;
                case ProjectStatus.uploadFile:
                    editor.msgHandler.uploadFile(data.fileData, data.gzip);
                    break;
                case ProjectStatus.removeFolder:
                    editor.msgHandler.removeFolder(data.removeFolderKey);
                    break;
                case ProjectStatus.renameFolder:
                    editor.msgHandler.renameOrMoveFolder(data.oldFolderKey, data.newFolderKey, data.ret);
                    break;
                case ProjectStatus.uploadFolder:
                    editor.msgHandler.uploadFolder(data.folderData);
                    break;
                case ProjectStatus.copyFolder:
                    editor.msgHandler.copyFolderUpdate(data.ret);
                    break;
                case ProjectStatus.addShared:
                    editor.msgHandler.addShared(data.ret);
                    break;
                case ProjectStatus.removeShared:
                    editor.msgHandler.removeShared(data.shared);
                    break;
            }
        });
        window._socket.on(sm_leave_room, ({ errorId, roomId }) => {
            if (errorId) {
                return showToast(Msg_Error[errorId], "error")
            }
            let users = {};
            this.setState({ users }, () => {
                actions.variable(actions.types.USERS, [], () => {
                    return users;
                });
            });
            DEBUG_CHAT && console.log(`roomId:${roomId},leave room successful`);
        });
        window._socket.on(sm_leave_project_room, ({ errorId, projectRoomId }) => {
            if (errorId) {
                return showToast(Msg_Error[errorId], "error")
            }
            const { projectShareUsers } = this.state;
            _.each(_.values(projectShareUsers), (u) => {
                u.userStatus = UserStatus.offline;
            })
            this.setState({ projectShareUsers }, () => {
                actions.variable(actions.types.PROJECT_SHARE_USERS, [], () => {
                    return projectShareUsers;
                });
            });
            DEBUG_CHAT && console.log(`projectRoomId:${projectRoomId},leave project room successful`);
        });
        window._socket.on(sm_kickout_room, ({ errorId, roomId, outUserId }) => {
            if (errorId) {
                return showToast(Msg_Error[errorId], "error")
            }
            const { users } = this.state;
            delete users[outUserId];
            this.setState({ users }, () => {
                actions.variable(actions.types.USERS, [], () => {
                    return users;
                });
            });
            DEBUG_CHAT && console.log(`roomId:${roomId},outUserId:${outUserId},kickout room successful`);
        });
        window._socket.on(sm_apply_edit, this.smApplyEdit);
        actions.inspector(this.triggerElement, [actions.types.TRIGGER],
            (trigger) => {
                this.setState({ trigger }, () => {
                    const { msgId, params, roomId } = trigger;
                    if (!this.state.info) {
                        return showToast("Data error!");
                    }
                    const { info: { userId, fileKey, projectId, uploadUri } } = this.state;
                    switch (msgId) {
                        case cm_kickout_room:
                            if (roomId === `${uploadUri}${fileKey}`) {
                                window._socket &&
                                    window._socket.emit(cm_kickout_room, {
                                        roomId: `${uploadUri}${fileKey}`,
                                        ...params
                                    });
                            }
                            break;
                        case cm_apply_edit:
                            window._socket &&
                                window._socket.emit(cm_apply_edit, {
                                    roomId: `${uploadUri}${fileKey}`,
                                    ...params
                                });
                            break;
                    }
                });
            })
        actions.inspector(this.view_codeElement, [actions.types.VIEW_CODE], (view_code) => {
            this.setState({ view_code })
        });
        actions.inspector(this.usersSharePermissionElement, [actions.types.USERS_SHARE_PERMISSION], (usersSharePermission) => {
            const { users, projectShareUsers } = this.state;
            _.each(users, (user, userId) => {
                (undefined !== usersSharePermission[userId]) && (user.status = usersSharePermission[userId]);
            })
            _.each(projectShareUsers, (projectUser, userId) => {
                (undefined !== usersSharePermission[userId]) && (projectUser.status = usersSharePermission[userId]);
            })
            this.setState({ usersSharePermission, users, projectShareUsers });
        });
        actions.inspector(this.swProjectElement, [actions.types.SW_PROJECT], (swProject) => {
            if (_.isEqual(swProject, this.state.swProject)) {
                return;
            }
            this.handleLeaveProjectRoom();
            this.setState({ swProject }, () => {
                this.handleLoginProjectRoom();
            });
        });
        actions.inspector(this.projectShareUsersElement, [actions.types.PROJECT_SHARE_USERS],
            (projectShareUsers) => {
                this.setState({ projectShareUsers });
            })
    }

    viewCodeComp = () => {
        const { view_code } = this.state;
        return <i
            onClick={() => {
                const { view_code } = this.state;
                let view_code_var = !view_code;
                let readwrite_mode_var = !view_code ? ReadWriteOptions[2] : ReadWriteOptions[1]
                actions.variable(actions.types.VIEW_CODE, [], () => {
                    return view_code_var;
                })
                actions.variable(actions.types.READWRITE_MODE, [], () => {
                    return readwrite_mode_var;
                })
                editor.getSettings().setReadwriteMode(readwrite_mode_var);
                editor.saveSettings(true);
            }}
            className={`icon-bordered icon fas fa-code ${!view_code ? "icon-muted" : ""
                } `} title={`${!view_code ? "Show" : "Hide"} All Code`}></i>;
    }

    smApplyEdit = async ({ errorId, edit }, ignore = false) => {
        const { currentEditor, users } = this.state;
        let youUser = _.filter(_.values(users), (user, index) => {
            return user.userId === USER_ID;
        })[0];
        if (errorId) {
            if (currentEditor.getReadOnly() == !edit) {
                edit = !edit;
            } else {
                currentEditor.setWrapperLoading(false);
                return !ignore && showToast(Msg_Error[errorId], "error")
            }
        }
        youUser.userStatus = edit ? UserStatus.edit : UserStatus.online;
        if (currentEditor.getReadOnly() !== !edit) {
            await currentEditor.setReadOnly(!edit);
        }
        this.setState({ users }, () => {
            actions.variable(actions.types.USERS, [], () => {
                return users;
            });
        });
        currentEditor.setWrapperLoading(false);
    }

    applyEdit = async (repaint = false) => {
        const { currentEditor, users } = this.state;
        let youUser = _.filter(_.values(users), (user, index) => {
            return user.userId === USER_ID;
        })[0];
        let editUser = _.filter(_.values(users), (user, index) => {
            return user.userId !== USER_ID && user.userStatus === UserStatus.edit;
        })[0];
        let edit = youUser.userStatus !== UserStatus.edit ? true : false;
        if (editUser) {
            showToast(`${editUser.userName} is editing`, "info")
            if (repaint) {
                currentEditor.setWrapperLoading(true);
                await this.smApplyEdit({ errorId: ErrorId["Someone is editing"], edit }, true)
            }
            return;
        }
        currentEditor.setWrapperLoading(true);
        if (!edit) {
            await currentEditor.upload();
        }
        actions.variable(actions.types.TRIGGER, [], () => {
            return {
                msgId: cm_apply_edit,
                params: { edit }
            };
        })
    }

    render() {
        const { users, info, className, fileStatus, usersSharePermission, linkData, fileData } = this.state;
        if (!info) {
            if (fileData) {
                return <React.Fragment>
                    <this.viewCodeComp></this.viewCodeComp>
                </React.Fragment>
            } else if (linkData) {
                return <React.Fragment>
                    {linkData.status >= ShareProjectCanOptions['View code'] &&
                        <this.viewCodeComp></this.viewCodeComp>}
                </React.Fragment>
            } else {
                return <React.Fragment>
                </React.Fragment>
            }
        }
        let youUser = _.filter(_.values(users), (user, index) => {
            return user.userId === USER_ID;
        })[0];
        let editUser = _.filter(_.values(users), (user, index) => {
            return user.userId !== USER_ID && user.userStatus === UserStatus.edit;
        })[0];
        return <React.Fragment>
            {youUser && (youUser.status >= ShareProjectCanOptions["View code"] ||
                youUser.isMaster
            ) && <this.viewCodeComp></this.viewCodeComp>
            }
            {(fileStatus !== FileStatus.updated) && youUser && (youUser.status >= ShareProjectCanOptions["Edit"] ||
                youUser.isMaster
            ) && <i
                onClick={this.applyEdit}
                className={`icon-bordered icon ${(youUser.userStatus === UserStatus.edit || !editUser) ?
                    "fas fa-pen" : "text-warning fas fa-signature"}
                 ${youUser.userStatus !== UserStatus.edit ? "icon-inactive" : ""
                    } `} title={`${editUser ? `${editUser.userName} is editing` :
                        ((youUser.userStatus !== UserStatus.edit) ? "Start Editing" : "Stop Editing")}`}></i>
            }
            {fileStatus === FileStatus.updated && <i
                onClick={async () => {
                    const { currentEditor } = this.state;
                    await currentEditor.refreshFile();
                    this.setState({ fileStatus: FileStatus.idle })
                }} className="icon-bordered icon fas fa-sync"
                title={`fetch updated file`}></i>}
        </React.Fragment>
    }
}