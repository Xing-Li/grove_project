import _ from 'lodash';
import React from "react";
import ReactDOM from "react-dom";
import editor from 'commonEditor';
import { v4 as uuidv4, NIL as NIL_UUID } from 'uuid';
import Code from "./Code";
const { CodeMode, linkKeys, cell_shortcuts_keys } = require('../../util/utils');
const { unescapeHtml, createElemnt } = require('../../util/helpers');

export default class CodeTool {
  constructor({ data, config, api, readOnly }) {
    this.api = api;
    window.editorjs = api;
    this.readOnly = readOnly;
    this.placeholder = this.api.i18n.t(config.placeholder || CodeTool.DEFAULT_PLACEHOLDER);
    /**@type{import ('../../tagEditor').default} */
    this.editor = (!config.editorjsKey || config.editorjsKey === editor.getTransferKey()) ? editor :
      _.filter(window.tagEditors, (tagEditor) => { return tagEditor.getTransferKey() === config.editorjsKey })[0];
    this.renderTask = this.editor.renderTask;
    this.CSS = {
      baseClass: this.api.styles.block,
      input: this.api.styles.input,
      wrapper: 'ce-code',
      textarea: 'ce-code__textarea',
    };
    if (data.codeData) {
      data.codeData.value = data.codeData.value ? unescapeHtml(data.codeData.value) : data.codeData.value;
    }
    this.data = {
      codeData: _.assign({}, {
        value: "",
        pinCode: true,
        dname: uuidv4(),
        codeMode: CodeMode.javascript2,
      }, data.codeData || {}),
    };
    if (!this.data.codeData.dname || this.data.codeData.dname.length < NIL_UUID.length) {
      this.data.codeData.dname = uuidv4();
    }
    this.ref = React.createRef();
    this.initSettingWrapper();
  }


  initSettingWrapper = () => {
    this.settingWrapper = document.createElement('div')
    const settings = [
      {
        name: 'edit-code',
        icon: `<i class="fas fa-pen"></i>`,
        func: "editCode",
        title: "Edit Code",
        className: 'btn-no-fill',
      },
      {
        name: 'run-code',
        icon: `<i class="fas fa-play"></i>`,
        func: "runCode",
        title: createElemnt(`<div>Run Code</div>
        <div class="ce-toolbar__plus-shortcut">${linkKeys(cell_shortcuts_keys["Run Current Cell"])}</div>`),
        className: 'btn-fill',
      },
      {
        name: 'copy-link',
        icon: `<i class="iconfont icon icon-icon-link"></i>`,
        func: "copyLink",
        title: createElemnt(`<div>Copy Link</div>`, "div", "Copy a link to this cell"),
        className: 'btn-fill',
      },
      {
        name: 'hide-code',
        icon: `<i class="fas fa-eye-slash"></i>`,
        func: "hideCode",
        title: createElemnt(`<div>Hide Block</div>
        <div> When Shared</div>`),
        className: 'btn-hide',
      },
      {
        name: 'sync-csv-file',
        icon: `<i class="fas fa-box"></i>`,
        func: "syncCsvFile",
        title: createElemnt(`<div>Proxy Array</div>`),
        className: 'btn-hide',
      }
    ];
    settings.forEach(tune => {
      let button = document.createElement('div');
      this.api.tooltip.onHover(button, tune.title, { placement: "top" });
      button.classList.add(tune.name, 'cdx-settings-button', tune.className);
      button.innerHTML = tune.icon;
      button.addEventListener('click', (domEvent) => {
        this.ref.current[tune.func](...(tune.args ? tune.args() : []));
        // button.classList.toggle('cdx-settings-button--active');
      });
      this.settingWrapper.appendChild(button);
    });
  }

  drawView() {
    this.viewWrapper = document.createElement('div');
    this.viewWrapper.classList.add(this.CSS.baseClass, this.CSS.wrapper);
    this.renderTask.submitTask((callback) => {
      ReactDOM.render(<Code
        ref={this.ref}
        editor={this.editor}
        readOnly={this.readOnly}
        api={this.api}
        data={this.data.codeData}
        saveData={(data) => {
          this.data.codeData = data;
          this.editor.setNeedSave(true);
        }}
        settingWrapper={this.settingWrapper}
      ></Code>, this.viewWrapper, () => {
        callback(this.ref.current);
      });
    })
    return this.viewWrapper;
  }

  render() {
    return this.drawView();
  }

  renderSettings() {
    return this.settingWrapper;
  }

  save(codeWrapper) {
    return {
      codeData: this.data.codeData,
    };
  }

  onPaste(event) {
    const content = event.detail.data;
    this.data = {
      codeData: content.textContent,
    };
  }

  beforeRemoved(params) {
    if (this.ref.current) {
      let data = this.viewWrapper.querySelector(".code-edit-result > .data");
      this.editor.deleteCacheV(data);
      let sync_data = this.viewWrapper.querySelector(".sync-file > .data");
      this.editor.deleteCacheV(sync_data);
      if (!params || params.type !== 'all') {
        this.editor.refreshAnchors();
        this.editor.refreshAttaches(this.viewWrapper.closest('.ce-block'));
        this.editor.refreshRuns(this.viewWrapper.closest('.ce-block'));
      }
      ReactDOM.unmountComponentAtNode(this.viewWrapper)
    }
  }

  removed() {

  }

  static get toolbox() {
    return {
      icon: '<svg width="14" height="14" viewBox="0 -1 14 14" xmlns="http://www.w3.org/2000/svg" > <path d="M3.177 6.852c.205.253.347.572.427.954.078.372.117.844.117 1.417 0 .418.01.725.03.92.02.18.057.314.107.396.046.075.093.117.14.134.075.027.218.056.42.083a.855.855 0 0 1 .56.297c.145.167.215.38.215.636 0 .612-.432.934-1.216.934-.457 0-.87-.087-1.233-.262a1.995 1.995 0 0 1-.853-.751 2.09 2.09 0 0 1-.305-1.097c-.014-.648-.029-1.168-.043-1.56-.013-.383-.034-.631-.06-.733-.064-.263-.158-.455-.276-.578a2.163 2.163 0 0 0-.505-.376c-.238-.134-.41-.256-.519-.371C.058 6.76 0 6.567 0 6.315c0-.37.166-.657.493-.846.329-.186.56-.342.693-.466a.942.942 0 0 0 .26-.447c.056-.2.088-.42.097-.658.01-.25.024-.85.043-1.802.015-.629.239-1.14.672-1.522C2.691.19 3.268 0 3.977 0c.783 0 1.216.317 1.216.921 0 .264-.069.48-.211.643a.858.858 0 0 1-.563.29c-.249.03-.417.076-.498.126-.062.04-.112.134-.139.291-.031.187-.052.562-.061 1.119a8.828 8.828 0 0 1-.112 1.378 2.24 2.24 0 0 1-.404.963c-.159.212-.373.406-.64.583.25.163.454.342.612.538zm7.34 0c.157-.196.362-.375.612-.538a2.544 2.544 0 0 1-.641-.583 2.24 2.24 0 0 1-.404-.963 8.828 8.828 0 0 1-.112-1.378c-.009-.557-.03-.932-.061-1.119-.027-.157-.077-.251-.14-.29-.08-.051-.248-.096-.496-.127a.858.858 0 0 1-.564-.29C8.57 1.401 8.5 1.185 8.5.921 8.5.317 8.933 0 9.716 0c.71 0 1.286.19 1.72.574.432.382.656.893.671 1.522.02.952.033 1.553.043 1.802.009.238.041.458.097.658a.942.942 0 0 0 .26.447c.133.124.364.28.693.466a.926.926 0 0 1 .493.846c0 .252-.058.446-.183.58-.109.115-.281.237-.52.371-.21.118-.377.244-.504.376-.118.123-.212.315-.277.578-.025.102-.045.35-.06.733-.013.392-.027.912-.042 1.56a2.09 2.09 0 0 1-.305 1.097c-.2.323-.486.574-.853.75a2.811 2.811 0 0 1-1.233.263c-.784 0-1.216-.322-1.216-.934 0-.256.07-.47.214-.636a.855.855 0 0 1 .562-.297c.201-.027.344-.056.418-.083.048-.017.096-.06.14-.134a.996.996 0 0 0 .107-.396c.02-.195.031-.502.031-.92 0-.573.039-1.045.117-1.417.08-.382.222-.701.427-.954z" /> </svg>',
      title: 'Code',
    };
  }

  static get DEFAULT_PLACEHOLDER() {
    return 'Enter a code';
  }

  static get pasteConfig() {
    return {
      tags: ['pre'],
    };
  }

  static get sanitize() {
    return {
      code: true, // Allow HTML tags
    };
  }

  static get isReadOnlySupported() {
    return true;
  }

  /**
   * Allow to press Enter inside the CommonCode textarea
   *
   * @returns {boolean}
   * @public
   */
  static get enableLineBreaks() {
    return true;
  }
}