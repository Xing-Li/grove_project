
const {
  cm_login_project_room, sm_login_project_room,
  cm_leave_project_room, sm_leave_project_room,
  cm_login_room, sm_login_room,
  cm_leave_room, sm_leave_room,
  cm_kickout_room, sm_kickout_room,
  cm_apply_edit, sm_apply_edit,
  cm_edit, sm_edit,
  cm_publish, sm_publish,
  cm_close_edit, sm_close_edit,
  ErrorId, cast_user_status, cast_project_status,
  cast_file_status, cast_project_user_status,
  UserStatus, ProjectStatus,
} = require('./MsgType');
const _ = require('lodash')
const ShapeSocket = _.assign({
  userId: "",
  userName: "",
  projectRoomId: "",
  pro_user_status: UserStatus,
  roomId: "",
  status: UserStatus,
  lineId: 0
}, { "WebSocket": "WebSocket" })

/**
 * <projectRoomId, <userId,socket>>
 * 
 * @type Object.<string, Object.<string, ShapeSocket>>
 */
const GSocketProjectRoomIdMap = {};
/**
 * <roomId, <userId,socket>>
 * @type Object.<string, Object.<string, ShapeSocket>>
 */
const GSocketRoomIdMap = {};

function castProjectUserOnlineStatus(projectRoomId, userId, userName, pro_user_status) {
  _.map(GSocketProjectRoomIdMap[projectRoomId], (socket, userIdKey) => {
    if (userIdKey !== userId) {
      socket.emit(cast_project_user_status, {
        projectRoomId,
        userId,
        userName,
        pro_user_status,
      });
    }
  })
}

function castUserOnlineStatus(roomId, userId, userName, status) {
  _.map(GSocketRoomIdMap[roomId], (socket, userIdKey) => {
    if (userIdKey !== userId) {
      socket.emit(cast_user_status, {
        userId,
        userName,
        roomId,
        status,
      });
    }
  })
}

function sendProjectStatus(projectRoomId, userId, status, data) {
  _.map(GSocketProjectRoomIdMap[projectRoomId], (socket, userIdKey) => {
    if (userIdKey !== userId) {
      socket.emit(cast_project_status, {
        projectRoomId,
        status,
        data
      });
    }
  })
}

function sendFileStatus(roomId, userId, status) {
  _.map(GSocketRoomIdMap[roomId], (socket, userIdKey) => {
    if (userIdKey !== userId) {
      socket.emit(cast_file_status, {
        roomId,
        status,
      });
    }
  })
}

function GroveChatSocket(io) {
  let ioChat = io.of('/groveChat/');

  ioChat.on('connection',/**@param {ShapeSocket} socket */ function (socket) {
    socket.on(cm_login_project_room, function ({ userName, userId, projectRoomId }) {
      console.log(`loginProjectRoom userName : ${userName},
      userId : ${userId},
      projectRoomId : ${projectRoomId}`);
      socket.projectRoomId = projectRoomId;
      socket.userId = userId;
      socket.userName = userName;
      socket.pro_user_status = UserStatus.online;
      if (!GSocketProjectRoomIdMap[projectRoomId]) {
        GSocketProjectRoomIdMap[projectRoomId] = {};
      }
      if (GSocketProjectRoomIdMap[projectRoomId][userId]) {
        socket.emit(sm_login_project_room, {
          errorId: ErrorId["Already logged in the project in other place"], projectRoomId
        });
        kickoutProjectRoom(projectRoomId, userId);
        return;
      }
      GSocketProjectRoomIdMap[projectRoomId][userId] = socket;
      castProjectUserOnlineStatus(projectRoomId, userId, userName, socket.pro_user_status);
      socket.emit(sm_login_project_room, {
        projectRoomId, userList: _.reduce(GSocketProjectRoomIdMap[projectRoomId], (prev, socket) => {
          prev.push({
            userId: socket.userId,
            userName: socket.userName,
            pro_user_status: socket.pro_user_status,
          })
          return prev;
        }, [])
      });
    });

    socket.on(cm_login_room, function ({ userName, userId, roomId }) {
      console.log(`loginRoom userName : ${userName},
      userId : ${userId},
      roomId : ${roomId}`);
      socket.userId = userId;
      socket.userName = userName;
      socket.roomId = roomId;
      socket.status = UserStatus.online;
      socket.lineId = -1;
      if (!GSocketRoomIdMap[socket.roomId]) {
        GSocketRoomIdMap[socket.roomId] = {};
      }
      if (GSocketRoomIdMap[socket.roomId][userId]) {
        socket.emit(sm_login_room, {
          errorId: ErrorId["Already logged in the room in other place"], roomId
        });
        kickoutRoom(roomId, userId);
        return;
      }
      GSocketRoomIdMap[roomId][userId] = socket;
      castUserOnlineStatus(roomId, userId, userName, socket.status);
      socket.emit(sm_login_room, {
        roomId, userList: _.reduce(GSocketRoomIdMap[roomId], (prev, socket) => {
          prev.push({
            userId: socket.userId,
            userName: socket.userName,
            status: socket.status,
            lineId: socket.lineId,
          })
          return prev;
        }, [])
      });
    });

    socket.on(cm_leave_project_room, function ({ }) {
      const { userName, userId, projectRoomId } = socket;
      console.log(`leaveProjectRoom userName : ${userName},
      userId : ${userId},
      projectRoomId : ${projectRoomId}`);
      _.unset(GSocketProjectRoomIdMap, [projectRoomId, userId]);
      if (_.size(GSocketProjectRoomIdMap[projectRoomId]) === 0) {
        _.unset(GSocketProjectRoomIdMap, [projectRoomId]);
      }
      castProjectUserOnlineStatus(projectRoomId, userId, userName, UserStatus.offline);
      socket.projectRoomId = undefined;
      socket.pro_user_status = undefined;
      socket.emit(sm_leave_project_room, {
        projectRoomId
      });
    });

    socket.on(cm_leave_room, function ({ }) {
      const { userName, userId, roomId } = socket;
      console.log(`leaveRoom userName : ${userName},
      userId : ${userId},
      roomId : ${roomId}`);
      _.unset(GSocketRoomIdMap, [roomId, userId]);
      if (_.size(GSocketRoomIdMap[roomId]) === 0) {
        _.unset(GSocketRoomIdMap, [roomId]);
      }
      castUserOnlineStatus(roomId, userId, userName, UserStatus.offline);
      socket.roomId = undefined;
      socket.status = undefined;
      socket.emit(sm_leave_room, {
        roomId
      });
    });

    function kickoutProjectRoom(projectRoomId, outUserId) {
      const outSocket = _.get(GSocketProjectRoomIdMap, [projectRoomId, outUserId]);
      if (!outSocket) {
        console.error("not need kickout project room!");
        return;
      }
      if (outSocket.projectRoomId !== projectRoomId) {
        console.error("error projectRoomId!")
        return;
      }
      const { userName, roomId } = outSocket;
      roomId && kickoutRoom(roomId, outUserId);
      console.log(`kickoutProjectRoom userName : ${userName},
      outUserId : ${outUserId},
      projectRoomId : ${projectRoomId}`);
      _.unset(GSocketProjectRoomIdMap, [projectRoomId, outUserId]);
      if (_.size(GSocketProjectRoomIdMap[projectRoomId]) === 0) {
        _.unset(GSocketProjectRoomIdMap, `${projectRoomId}`);
      }
      outSocket.projectRoomId = undefined;
      outSocket.emit(cast_project_user_status, {
        projectRoomId,
        userId: outUserId,
        userName,
        pro_user_status: UserStatus.kickout,
      });
      castProjectUserOnlineStatus(projectRoomId, outUserId, userName, UserStatus.kickout);
      outSocket.pro_user_status = undefined;
    }

    function kickoutRoom(roomId, outUserId) {
      const outSocket = _.get(GSocketRoomIdMap, [roomId, outUserId]);
      if (!outSocket) {
        console.error("not need kickout!")
        return;
      }
      if (outSocket.roomId !== roomId) {
        console.error("error roomId!")
        return;
      }
      const { userName } = outSocket;
      console.log(`kickoutRoom userName : ${userName},
      outUserId : ${outUserId},
      roomId : ${roomId}`);
      _.unset(GSocketRoomIdMap, [roomId, outUserId]);
      if (_.size(GSocketRoomIdMap[roomId]) === 0) {
        _.unset(GSocketRoomIdMap, `${roomId}`);
      }
      outSocket.roomId = undefined;
      outSocket.userId = undefined;
      outSocket.userName = undefined;
      outSocket.status = undefined;
      outSocket.lineId = undefined;
      outSocket.emit(cast_user_status, {
        roomId,
        userId: outUserId,
        userName,
        status: UserStatus.kickout,
      });
      castUserOnlineStatus(roomId, outUserId, userName, UserStatus.kickout);
    }

    socket.on(cm_kickout_room, function ({ roomId, outUserId }) {
      const outSocket = _.get(GSocketRoomIdMap, [roomId, outUserId]);
      if (!outSocket) {
        socket.emit(sm_kickout_room, {
          roomId,
          outUserId
        });
        return;
      }
      if (socket.roomId !== roomId || outSocket.roomId !== roomId) {
        socket.emit(sm_kickout_room, {
          errorId: ErrorId["Not in a room"]
        });
        return;
      }
      const { userName } = outSocket;
      console.log(`kickoutRoom userName : ${userName},
      outUserId : ${outUserId},
      roomId : ${roomId}`);
      _.unset(GSocketRoomIdMap, [roomId, outUserId]);
      if (_.size(GSocketRoomIdMap[roomId]) === 0) {
        _.unset(GSocketRoomIdMap, [roomId]);
      }
      outSocket.roomId = undefined;
      outSocket.status = undefined;
      outSocket.lineId = undefined;
      outSocket.emit(cast_user_status, {
        roomId,
        userId: outUserId,
        userName,
        status: UserStatus.kickout,
      });
      castUserOnlineStatus(roomId, outUserId, userName, UserStatus.kickout);
      socket.emit(sm_kickout_room, {
        roomId,
        outUserId
      });
    });

    socket.on(cm_apply_edit, function ({ edit }) {
      const { userName, userId, roomId } = socket;
      console.log(`applyEdit userName : ${userName},
      userId : ${userId},
      roomId : ${roomId}`);
      if (edit) {
        if (_.filter(GSocketRoomIdMap[roomId], (socket) => {
          if (socket.status === UserStatus.edit) {
            return socket;
          }
        }).length > 0) {
          socket.emit(sm_apply_edit, {
            errorId: ErrorId["Someone is editing"],
            edit
          });
          return;
        }
      }
      let nstate = edit ? UserStatus.edit : UserStatus.online;
      if (nstate === socket.status) {
        socket.emit(sm_apply_edit, {
          errorId: ErrorId["Someone is editing"],
          edit
        });
        return;
      }
      socket.status = nstate;
      castUserOnlineStatus(roomId, userId, userName, socket.status);
      socket.emit(sm_apply_edit, {
        roomId,
        edit
      });
    });

    // Leaving chat message
    socket.on('disconnect', function () {
      if (!socket.pro_user_status || socket.pro_user_status === UserStatus.offline) {
        console.log(`disconnect`);
        return;
      }
      const { userName, userId, projectRoomId, roomId } = socket;
      console.log(`disconnect userName: ${userName},
      userId: ${userId}`)
      if (projectRoomId) {
        _.unset(GSocketProjectRoomIdMap, [projectRoomId, userId]);
        if (_.size(GSocketProjectRoomIdMap[projectRoomId]) === 0) {
          _.unset(GSocketProjectRoomIdMap, [projectRoomId]);
        }
        castProjectUserOnlineStatus(projectRoomId, userId, userName, UserStatus.offline);
      }
      if (roomId) {
        _.unset(GSocketRoomIdMap, [roomId, userId]);
        if (_.size(GSocketRoomIdMap[roomId]) === 0) {
          _.unset(GSocketRoomIdMap, [roomId]);
        }
        castUserOnlineStatus(roomId, userId, userName, UserStatus.offline);
      }
      socket.projectRoomId = undefined;
      socket.pro_user_status = undefined;
      socket.roomId = undefined;
      socket.status = undefined;
      socket.lineId = undefined;
    });
  })
  return io

}

module.exports = { GroveChatSocket, castUserOnlineStatus, sendFileStatus, sendProjectStatus };