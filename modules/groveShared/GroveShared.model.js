const mongoose = require('mongoose');
const schemaCRUD = require('../schemaCRUD/schemaCRUD');

const Schema = mongoose.Schema;

const GroveShared = new Schema({
    userId: { type: Schema.Types.ObjectId, ref: 'users' },
    toUserId: { type: Schema.Types.ObjectId, ref: 'users' },
    projectId: String,
    projectName: String,
    fileKey: String,
    version: Number,
    status: Number,
    createTime: { type: Date, default: Date.now },
});

GroveShared.plugin(schemaCRUD.CRUD);

GroveShared.plugin(function (schema) {

    schema.statics.addShared = function (addInfo, cb) {
        return schemaCRUD.add(this, addInfo, function (err, result) {
            return cb(err, result);
        }, false);
    }

    schema.statics.getAllShareds = function (condition, cb) {
        this.find(condition).populate({
            path: 'toUserId',
            select: 'email  lastName firstName'
        }).populate({
            path: 'userId',
            select: 'email  lastName firstName'
        }).select({}).sort({ "createTime": -1 })
            .exec(function (err, projects) {
                return cb(err, projects)
            });
    }

    schema.statics.getShared = function (id, cb) {
        this.findOne({ "_id": id }).exec(function (err, item) {
            cb(err, item)
        });
    }

    schema.statics.updateSharedById = function (id, updateInfo, cb) {
        this.findOneAndUpdate(
            { "_id": id },
            { "$set": updateInfo },
            { upsert: false },
            function (err, item) {
                cb(err, item)
            })
    }

    schema.statics.deleteSharedById = function (id, cb) {
        this.deleteOne({ '_id': id }, cb)
    }

    schema.statics.deleteSharedsByIds = function (ids, cb) {
        this.deleteMany({
            _id: {
                $in: ids
            }
        }, cb)
    }
});



module.exports = mongoose.model('GroveShared', GroveShared);