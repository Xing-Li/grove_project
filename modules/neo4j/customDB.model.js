const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const schemaCRUD = require('../schemaCRUD/schemaCRUD');

const cryptoUtil = require('../util/cryptoUtil');

const DefaultSearchKey = 's'; //for tour
const defaultOpenMultiLan = AppConfig.companyName && AppConfig.companyName == 'tuke'
const CustomDB = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'users' },
    shareInfo: String,
    hostname: String,//change host to hostname
    port: Number,
    boltPort: Number,
    projectName: String,
    username: String, //update userName to username
    password: String,
    note: String,
    //Only for neo4j v4 multi dbs instance
    currentNeo4jDB: { type: String, default: "neo4j"},
    isDemo: { type: Boolean, default: false },

    /**
     * isLocal
     * false -> Neo4j proxies through the server
     * true -> user's browser connects directly to Neo4j
     */
    isLocal: { type: Boolean, default: false },

    /**
     * isRoot
     * When disableRootPath is false, we create a default root project for the admin user.
     * The root project has isRoot=true */
    isRoot: { type: Boolean, default: false },

    isShare: { type: Boolean, default: true },
    //For Neo4j or GraphDB
    queryCollections: [
    ],
    //For MSSQL
    sqlCollections: [
    ],
    mysqlCollections: [
    ],
    postgresqlCollections:[
    ],
    forceLayoutSettings:{
        linkStrength:{ type: Number, default: 1.0 },
        linkDistance:{ type: Number, default: 0.5 },
        charge:{ type: Number, default: -0.01 },
        gravity:{ type: Number, default:0.1 },
        friction:{ type: Number, default: 0.3 },
        heightCompress:{ type: Number, default: 1.0 },
        collision:{ type: Number, default: 0.02 }
    },
    projectSettings: {
        loadInnerRelationship: { type: Boolean, default: true },
        autoShowImage: { type: Boolean, default: false },
        autoShowCaption: { type: Boolean, default: false },
        disableInfoPanel: { type: Boolean, default: true },
        quickInfo: { type: Boolean, default: false },
        hideArrow: { type: Boolean, default: false },
        showRelationshipName:{ type: Boolean, default:false},
        useDash:{ type: Boolean, default:false},
        nodeCaptionPosition:{ type: String, default: "right"},
        iconMode:{ type: Number, default: 1.0},
        useCurve: { type: Boolean, default: true },
        hidePin: { type: Boolean, default: false },
        edgeWidthScale: { type: Number, default: 1.0 },
        nodeSizeScale: { type: Number, default: 1.0 },
        captionSizeScale: { type: Number, default: 1.0 },
        captionMaxLength: {type: Number, default: 100 },
        fogDensity: { type: Number, default: 0.0 },
        showSnapshot: { type: Boolean, default: false },
        captionMultiLangua: { type: Boolean, default: defaultOpenMultiLan },
        blendEdges: { type: Boolean, default: false },
        theme: { type: String, default: 'dark' },
        mode: { type: String, default: '3D' },
        iconConfig:{

        } 
    },
    searchKey: { type: String },
    searchIndex: { type: String, default: "graphxrIndex" },
    labels: [{
        // caption_property: { type: String, default: "" },
        captions: [],
        avatar_property: { type: String, default: "" },
        size_property: { type: String, default: "" },
        label: { type: String, default: "" },
        background_color: { type: String, default: "" },
        icon: { },
        caption_color: { type: String, default: "" },
        properties: {},
        template: { type: String, default: "" },
        visible: { type: Boolean, default: true },
        //Only hidden in table and info panel
        hidden:{},
        //Will Show the info with the order in info pane / table 
        order: []
    }],
    relationships: [{
        relationship: { type: String, default: "" },
        color: { type: String, default: "" },
        size: { type: Number, default: 1 },
        width_property: { type: String, default: "" },
        visible: { type: Boolean, default: true },
        captions:[],
        properties: {},
        //Only hidden in table and info panel
        hidden:{},
        //Will Show the info with the order in info pane / table 
        order: []
    }],
    createTime: {
        type: Date,
        default: Date.now
    },
    UISetting: {},
    labelConfig: {},
    relationshipConfig: {},
    propertyConfig: {},
    lastActiveTime: {
        type: Number,
        default: 0
    },
    advancedNeo4jFilter: {
        type: String,
        default: ""
    },
});

CustomDB.plugin(schemaCRUD.CRUD);

CustomDB.plugin(function (schema) {

    schema.statics.addProject = function (userId, dbInfo, cb) {
        let self = this;
        let id = dbInfo._id;
        let condition = { "user": userId, 'projectName': dbInfo.projectName, isDemo: dbInfo.isDemo };
        if (id) {
            condition = { '_id': id };
        }
        let dbItem = {
            user: userId,
            hostname: dbInfo.hostname,
            port: dbInfo.port || 0,
            boltPort: dbInfo.boltPort || 7687,
            username: cryptoUtil.encrypt(dbInfo.username),
            password: cryptoUtil.encrypt(dbInfo.password),
            isDemo: dbInfo.isDemo,
            isLocal: dbInfo.isLocal,
            isRoot: dbInfo.isRoot,
            projectName: dbInfo.projectName,
            currentNeo4jDB: dbInfo.currentNeo4jDB || 'neo4j',
            advancedNeo4jFilter: dbInfo.advancedNeo4jFilter || '',
        }
        return this.findOne(condition)
            .select({})
            .exec(function (err, oldItem) {
                if (!oldItem) {
                    return schemaCRUD.add(self, dbItem, (err,newItem) =>{
                        cb(err,newItem || {},{});
                    });
                    //is edit 
                } else if (id && oldItem) {
                    return schemaCRUD.edit(self, id, dbItem, (err,newItem) =>{
                        cb(err,newItem || {},oldItem);
                    });
                } else {
                    cb(new Error(dbInfo.projectName + ' is already exist'),{},{})
                }
            })
    }

    schema.statics.cloneProject = function (userId, dbInfo, cb) {
        let self = this;
        dbInfo.user = userId;
        dbInfo.lastActiveTime = new Date().getTime();
        return schemaCRUD.add(self, dbInfo, cb);
    }

    schema.statics.updateSearchKey = function (id, searchKey, cb) {
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }
        return this.findOneAndUpdate(
            { "_id": id },
            { "$set": { "searchKey": searchKey } },
            { upsert: false },
            function (err, item) {
                cb(err, item);
            })
    }

    schema.statics.updateField = function (id, fieldName, fieldValue, cb) {
         if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }
        if(!fieldName || fieldValue === undefined){
            return cb(new Error("Miss the fieldName or field value"));
        }
        return this.findOneAndUpdate(
            { "_id": id },
            { "$set": { [fieldName]: fieldValue } },
            { upsert: false },
            function (err, item) {
                cb(err, item);
            })
    }

    schema.statics.updateSearchIndex = function (id, searchIndex, cb) {
        let self = this;
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }
        return this.findOneAndUpdate(
            { '_id': id },
            { "$set": { "searchIndex": searchIndex } },
            { upsert: false },
            function (err, item) {
                cb(err, item);
            })
    }


    schema.statics.addQuery = function (id, query, cb) {
        let self = this;
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }
        return this.findOne({ "_id": id })
            .select({})
            .exec(function (err, item) {
                if (item) {
                    item = JSON.parse(JSON.stringify(item));
                    item.queryCollections = item.queryCollections || [];
                    if (!item.queryCollections.includes(query)) {
                        item.queryCollections.push(query)
                    }
                    let id = item._id;
                    delete item._id;
                    return schemaCRUD.edit(self, id, item, cb);
                } else {
                    cb(err)
                }
            })
    }

    schema.statics.addSQLQuery = function (id, collectionsName , query, cb) {
        let self = this;
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }
        return this.findOne({ "_id": id })
            .select({})
            .exec(function (err, item) {
                if (item) {
                    item = JSON.parse(JSON.stringify(item));
                    let collections =  item[collectionsName || 'mysqlCollections']
                    collections = collections?collections:[];
                    if (!collections.includes(query)) {
                        collections.push(query)
                    }
                    let id = item._id;

                    delete item._id;
                    return schemaCRUD.edit(self, id, item, cb);
                } else {
                    cb(err)
                }
            })
    }

    schema.statics.deleteQuery = function (id, query, cb) {
        let self = this;
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }
        return this.findOne({ "_id": id })
            .select({})
            .exec(function (err, item) {
                if (item) {
                    item = JSON.parse(JSON.stringify(item));
                    item.queryCollections = item.queryCollections || [];
                    let index = -1
                    if(typeof query == 'object')
                    {
                        console.log(Object.keys[query])
                        index = item.queryCollections.findIndex(i => typeof i == 'object' &&Object.keys(i)[0] === Object.keys(query)[0] );
                        console.log("index:"+index)
                    }else{
                        index = item.queryCollections.indexOf(query)
                    }
                    
                    if (index > -1) {
                        item.queryCollections.splice(index, 1)
                    }
                    let id = item._id;
                    delete item._id;
                    return schemaCRUD.edit(self, id, item, cb);
                } else {
                    cb(err)
                }
            })
    }

    schema.statics.deleteSQLQuery = function (id, collectionsName , query, cb) {
        let self = this;
        collectionsName = collectionsName || 'mysqlCollections' 
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }
        return this.findOne({ "_id": id })
            .select({})
            .exec(function (err, item) {
                if (item) {
                    item = JSON.parse(JSON.stringify(item));
                    let collections=item[collectionsName]
                    collections = collections?collections:[];
                    let index = collections.indexOf(query)
                    if (index > -1) {
                        collections.splice(index, 1)
                    }
                    let id = item._id;
                    item[collectionsName] = collections
                    delete item._id;
                    return schemaCRUD.edit(self, id, item, cb);
                } else {
                    cb(err)
                }
            })
    }

    schema.statics.getQueryList = function (id, cb) {
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }

        return this.findOne({ "_id": id })
            .select({})
            .exec(function (err, item) {
                if (item) {
                    item = JSON.parse(JSON.stringify(item));
                }
                cb(err, item && item.queryCollections ? item.queryCollections : [])
            })
    }

    schema.statics.deleteProject = function (userId, projectId, cb) {
        return this.deleteOne({ "user": userId, "_id": projectId }, function (err) {
            cb(err)
        })
    }

    schema.statics.shareProject = function (projectId, isShare, cb) {
        return this.findOneAndUpdate(
            { "_id": projectId },
            { "$set": { isShare: isShare ? true : false } },
            { upsert: false , returnOriginal:true},
            function (err, item) {
                cb(err, item || {})
            })
    }

    schema.statics.editNote = function (projectId, noteContent, cb) {
        return this.findOneAndUpdate(
            { "_id": projectId },
            { "$set": { note: noteContent ? noteContent : "" } },
            { upsert: false },
            function (err, item) {
                cb(err, item)
            })
    }

    schema.statics.getInfo = function (id, cb) {
        if (id === '/') {
            // See disableRootPath setting in config.js, and the isRoot project attribute
            return this.getRootProject(cb)
        }

        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }

        return this.findOne({ "_id": id })
            // .select("hostname username password port boltPort isDemo isShare isLocal projectName user note ")
            .exec(function (err, item) {
                // if (item && !item.get("isDemo") && !item.get("isShare")) {
                //     item.set("password", cryptoUtil.decrypt(item.get("password")));
                //     item.set("username", cryptoUtil.decrypt(item.get("username")))
                // }
                cb(err, item)
            })
    }

    schema.statics.getRootProject = function (cb) {
        return this.findOne({ "isRoot": true })
            .exec(function (err, item) {
                cb(err, item)
            })
    }


    schema.statics.listProject = function (userId, cb) {

        return this.find({ "user": userId })
            .select('currentNeo4jDB hostname port boltPort username password isDemo isShare isLocal note projectName advancedNeo4jFilter lastActiveTime')
            .exec(function (err, items) {
                if (items && items.length > 0) {
                    items.forEach(item => {
                        if (!item.get("isDemo")) {
                            item.set("username", cryptoUtil.decrypt(item.get("username")));
                            item.set("password", cryptoUtil.decrypt(item.get("password")));
                        }
                    })
                }
                cb(err, items)
            })

    }

    schema.statics.getForceLayoutSettings = function (id, cb) {
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }
        return this.findOne({ "_id": id })
            .select("forceLayoutSettings")
            .exec(function (err, item) {
                cb(err, item)
            })
    }

    schema.statics.updateForceLayoutSettings = function (id, forceLayoutSettings, cb) {
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }
        return this.findOneAndUpdate(
            { "_id": id },
            { "$set": { forceLayoutSettings } },
            { upsert: false },
            cb);
    }

    schema.statics.getProjectSettings = function (id, cb) {
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }
        return this.findOne({ "_id": id })
            .select("projectSettings")
            .exec(function (err, item) {
                cb(err, item)
            })
    }

    schema.statics.updateProjectSettings = function (id, projectSettings, cb) {
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }
        return this.findOneAndUpdate(
            { "_id": id },
            { "$set": { projectSettings } },
            { upsert: false },
            cb);
    }

    schema.statics.updateDemoToNoneNeo4j = function (
        demoHostName,
        demoBoltPort,
        currentNeo4jDB,
        cb
    ) {
        return this.updateMany(
            {
                hostname: demoHostName,
                boltPort: demoBoltPort,
                currentNeo4jDB,
                isDemo: true,
            },
            {
                $set: {
                    hostname: "",
                    boltPort: "",
                }
            },
            { upsert: false },
            cb
        );
    };

    schema.statics.getUISetting = function (id, cb) {
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }
        return this.findOne({ "_id": id })
            .select("UISetting")
            .exec(function (err, item) {
                cb(err, item)
            })
    }
    schema.statics.updateUISetting = function (id, UISetting, cb) {
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }
        return this.findOneAndUpdate(
            { "_id": id },
            { "$set": { UISetting } },
            { upsert: false, returnOriginal:true },
            (err, res) =>{
                cb(err, res || {})
            });//just update one item
    }

    schema.statics.updateLabels = function (id, labels, cb) {
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }
        return this.findOneAndUpdate(
            { "_id": id },
            { "$set": { labels } },
            { upsert: false, returnOriginal:true },
            (err, res) =>{
                cb(err, res || {})
            });//just update one item
    }

    schema.statics.updateRelationships = function (id, relationships, cb) {
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }
        return this.findOneAndUpdate(
            { "_id": id },
            { "$set": { relationships } },
            { upsert: false, returnOriginal:true },
            (err, res) =>{
                cb(err, res || {})
            });//just update one item
    }

    //@jack the function is not clean
    // schema.statics.updateItems = function (id, config, cb) {
    //     if (!(/[0-9a-z]{22,}/ig).test(id)) {
    //         return cb(new Error("Can't handle the project with id:" + id))
    //     }
    //     return this.findOneAndUpdate(
    //         { "_id": id },
    //         { "$set": config },
    //         { upsert: false },
    //         cb);//just update one item
    // }

    //@sean @jacob need use object replace the array;
    schema.statics.updateLabelConfig = function (id, labelConfig, cb) {

        let self = this;
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }
        return this.findOne({ "_id": id })
            .select({})
            .exec(function (err, item) {
                if (item) {
                    item = JSON.parse(JSON.stringify(item));
                    let exsitLabel = null;

                    item.labels = item.labels || [];
                    for (let i = 0; i < item.labels.length; i++) {
                        if (item.labels[i] && item.labels[i].label == labelConfig.label) {
                            exsitLabel = item.labels[i];
                            break;
                        }
                    }

                    if (exsitLabel) {
                        Object.assign(exsitLabel, labelConfig);
                    } else {
                        item.labels.push(labelConfig);
                    }

                    let id = item._id;
                    delete item._id;

                    return schemaCRUD.edit(self, id, item, cb);
                } else {
                    cb(new Error("Can't found the project"));
                }
            });
    }

    schema.statics.deleteLabelConfig = function (id, labelConfig, cb) {
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }

       return this.findOneAndUpdate(
            {'_id':id},
            {'$pull':{"labels":{"label":labelConfig.label}}}, 
            {'upsert':false, returnOriginal:true},
             function (err, oldDoc) {
                cb(err,oldDoc)
             })
    }

    //@sean @jacob need use object replace the array;
    schema.statics.updateRelationshipConfig = function (id, relationshipConfig, cb) {
        let self = this;
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }
        return this.findOne({ "_id": id })
            .select({})
            .exec(function (err, item) {
                //1. update the labelconfig
                if (item) {
                    item = JSON.parse(JSON.stringify(item));
                    let exsitRelationship = null;

                    item.relationships = item.relationships || [];

                    for (let i = 0; i < item.relationships.length; i++) {
                        if (item.relationships[i] && item.relationships[i].relationship == relationshipConfig.relationship) {
                            exsitRelationship = item.relationships[i];
                            break;
                        }
                    }

                    if (exsitRelationship) {
                        Object.assign(exsitRelationship, relationshipConfig);
                    } else {
                        item.relationships.push(relationshipConfig);
                    }

                    let id = item._id;
                    delete item._id;

                    return schemaCRUD.edit(self, id, item, cb);
                } else {
                    cb(new Error("Can't found the project"));
                }
            });
    }

    // lastActiveTime
    schema.statics.updateLastActiveTime = function (id, cb) {
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }
        return this.findOneAndUpdate(
            { "_id": id },
            { "$set": { lastActiveTime: new Date().getTime() } },
            { upsert: false }
            , function (err) {
                cb(err);
            });
    }

    schema.statics.findDBConfigs = function (id, cb) {
        if (!(/[0-9a-z]{22,}/ig).test(id)) {
            return cb(new Error("Can't handle the project with id:" + id))
        }
        return this.findOne({ "_id": id })
            .select({})
            .exec(function (err, item) {
                cb(err, item)
            })
    }

    schema.statics.updateStruct = function (cb) {
        let self = this;
        //add tour searchKey 2019-01-24
        this.updateMany(
            { searchKey: { "$exists": false } },
            { "$set": { searchKey: DefaultSearchKey } },
            { upsert: false },
            (err) => {
                if (err) {
                    console.error(err);
                }
            })

        //@v2.2.1 Captions support
        this.find({ "labels.caption_property": { "$exists": true } })
            .select({})
            .exec(function (err, items) {
                if (!items) {
                    return
                }
                items.forEach(item => {
                    item = item.toJSON();
                    item.labels.forEach(labelItem => {
                        let caption_property = labelItem['caption_property']
                        if (caption_property && caption_property !== '') {
                            labelItem.captions = [labelItem['caption_property']];
                        }
                        delete labelItem.caption_property;
                    })

                    schemaCRUD.edit(self, item._id, item, (err) => {
                        if (err) {
                            console.error(err);
                        }
                    });
                })
            });

            //@v2.14 change icon data structure from string to object
            this.find({ "labels.icon": { "$exists": true } })
            .select({})
            .exec(function (err, items) {
                if (!items) {
                    return
                }
                items.forEach(item => {
                    item = item.toJSON();
                    item.labels.forEach(labelItem => {
                        let icon = labelItem['icon']
                        if (typeof icon == "string") {
                            labelItem.icon = {
                                "name":icon,
                                "category":"all",
                                "version":1,
                                "unicode":""
                            }
                        }else{
                            labelItem.icon = icon
                        }
                    })

                    schemaCRUD.edit(self, item._id, item, (err) => {
                        if (err) {
                            console.error(err);
                        }
                    });
                })
            });

            //@v2.15 add advancedNeo4jFilter
            this.updateMany(
                { advancedNeo4jFilter: { "$exists": false } },
                { "$set": { advancedNeo4jFilter: "" } },
                { upsert: false },
                (err) => {
                    if (err) {
                        console.error(err);
                    }
                })

            //@v2.16 add hidden & order to labels & relationships
            this.find({ "labels.order": { "$exists": false } })
            .select({})
            .exec(function (err, items) {
                if (!items) {
                    return
                }
                items.forEach(item => {
                    item = item.toJSON();
                    item.labels.forEach(subItem => {
                        subItem.hidden = {};
                        subItem.order = [];
                    })
                    item.relationships.forEach(subItem => {
                        subItem.hidden = {};
                        subItem.order = [];
                    })
                    schemaCRUD.edit(self, item._id, item, (err) => {
                        if (err) {
                            console.error(err);
                        }
                    });
                })
            });

            //@2.16.2 add postgresql support
            this.updateMany(
                { postgresqlCollections: { "$exists": false } },
                { "$set": { postgresqlCollections: [] } },
                { upsert: false },
                (err) => {
                    if (err) {
                        console.error(err);
                    }
                })
        cb();
    };

});


module.exports = mongoose.model('customDB', CustomDB);