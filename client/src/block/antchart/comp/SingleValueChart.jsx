
import React from 'react';
import { renderValueByType } from '../util/ReportRecordProcessing';
import { evaluateRulesOnNeo4jRecord } from '../config/StyleRuleEvaluator';

/**
 * Renders Neo4j records as their JSON representation.
 */
const NeoSingleValueChart = (props) => {
    const { result } = props;
    if (!result || result instanceof Array) {
        return <div>Not Legal Data!</div>
    }
    if (typeof result !== 'object' || result.error) {
        return <div>Not Legal Data! {typeof result !== 'object' ? "not object result!" : JSON.stringify(result.error)}</div>
    }
    const records = result.records;
    const fontSize = props.settings && props.settings.fontSize ? props.settings.fontSize : 64;
    const color = props.settings && props.settings.color ? props.settings.color : window.getComputedStyle(document.body).color;
    const verticalAlign = props.settings && props.settings.verticalAlign ? props.settings.verticalAlign : "top";
    const alignItems = ({ top: "start", middle: "center", bottom: "end" })[verticalAlign]
    const value = (records && records[0] && records[0]["_fields"] && records[0]["_fields"][0]) ? records[0]["_fields"][0] : "";
    const displayValue = renderValueByType(value);
    return <div style={{
        height: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: alignItems
    }}>
        <span style={{
            display: "inline-block",
            width: "100%",
            fontSize: fontSize,
            color: evaluateRulesOnNeo4jRecord(records[0], "text color", color)
        }}>{displayValue}</span>
    </div>;
}

export default NeoSingleValueChart;