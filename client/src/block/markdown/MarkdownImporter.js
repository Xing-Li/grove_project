import * as remark from 'remark';
import { mdTransferToEditorjs } from './FileHandler';

/**
 * Markdown Import class
 */
export default class MarkdownImporter {
  /**
   * creates the Importer plugin
   * {editorData, api functions} - necessary to interact with the editor
   */
  constructor({ data, api }) {
    this.data = data;
    this.api = api;
  }

  /**
   * @return Plugin data such as title and icon
   */
  static get toolbox() {
    return {
      title: 'Import Markdown',
      icon: '<i class="fas fa-file-import"></i>',
    };
  }

  /**
  * @return invinsible file upload form
  */
  render() {
    const doc = document.createElement('input');
    doc.setAttribute('id', 'file-upload');
    doc.setAttribute('type', 'file');
    doc.setAttribute('style', 'display: none');
    doc.setAttribute('name', 'files[]');
    doc.setAttribute('onload', this.parseMarkdown());

    return doc;
  }

  /**
  * Function which parses markdown file to JSON which correspons the the editor structure
  * @return Parsed markdown in JSON format
  */
  async parseMarkdown() {
    const a = {};
    const data = await this.api.saver.save();
    a.content = data.blocks;

    const fileUpload = document.getElementById('file-upload');

    fileUpload.onchange = (e) => {
      const file = e.target.files[0];

      const reader = new FileReader();
      reader.readAsText(file, 'UTF-8');

      reader.onload = (readerEvent) => {
        const content = readerEvent.target.result;
        // clear the editor
        this.api.blocks.clear();
        // render the editor with imported markdown data
        this.api.blocks.render({
          blocks: mdTransferToEditorjs(content)
        });

        return remark().parse(content);
      };
    };

    fileUpload.click();
  }

  save() {
    return {
      message: 'Uploading Markdown',
    };
  }

  validate(savedData) {
    return false;
  }
  static get isReadOnlySupported() {
    return true;
  }
}
