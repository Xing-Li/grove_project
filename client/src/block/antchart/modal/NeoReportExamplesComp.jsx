import React from 'react';
import CypherEditor from '../comp/CypherEditor';
import { Row, Col, List, Card } from 'antd';
import NeoReport from '../report/Report';
import { Section, SectionTitle, SectionContent } from './ModalUtils';
import EXAMPLE_REPORTS from '../examples/ExampleConfig';
import { EXAMPLE_ADVANCED_REPORTS } from '../examples/AdvancedChartsExampleConfig';
import { copyContent } from '../../../util/utils';
import Meta from 'antd/lib/card/Meta';
import { RenderType } from 'util/databaseApi';
import { barDefault, multiRadarDefault, radarDefault } from 'util/chartConfigUtil';
import { ChartTypes, extractOptions, getRenderTypeOptions } from '../comp/OptionsTypes';
import { getDefaultOptionsByType } from '../comp/CheckChartTypes';

export const NeoReportExamplesComp = (props) => {
  const examples = [...EXAMPLE_REPORTS, ...EXAMPLE_ADVANCED_REPORTS];
  const database = 'neo4j';
  return <List
    grid={{
      gutter: 16
    }}
    dataSource={examples}
    renderItem={ /** @param {ShapeShared} item */ (example) => {
      let defaultSettings;
      switch (example.type) {
        case ChartTypes['Basic Bar']:
        case ChartTypes['Basic Pie']:
          defaultSettings = getDefaultOptionsByType(example.type);
          break;
        default:
          defaultSettings = getRenderTypeOptions(example.type);
          break;
      }
      let settings = extractOptions(_.assign(_.cloneDeep(defaultSettings), example.settings));
      return <List.Item>
        <Card className="card-example" hoverable
          cover={<div className="image-wrap" onClick={() => {
            //this.openFunc(tag, item)
          }}>
            <SectionTitle>{example.title}</SectionTitle>
            <div className='n-col-span-3'>{example.description}</div>
            <div
              style={{
                height: '355px',
                overflow: 'hidden',
                border: '1px solid lightgrey',
              }}
            >
              <NeoReport
                id={example.type}
                query={example.syntheticQuery}
                database={database}
                selection={example.selection}
                parameters={example.globalParameters}
                settings={settings}
                fields={example.fields}
                dimensions={example.dimensions}
                chartType={example.chartType}
                type={example.type}
              />
            </div>
            <CypherEditor items={[
              {
                value: example.syntheticQuery,
                label: "Copy Test Data Cypher Query",
              },
              {
                value: example.exampleQuery,
                label: 'Copy Template Cypher Query',
              },
            ]} value={example.exampleQuery} onValueChange={(value) => {
              copyContent(value)
            }}></CypherEditor>
          </div>}
        >
        </Card>
      </List.Item>
    }}
  />
};

export default NeoReportExamplesComp;
