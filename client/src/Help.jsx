import React from 'react';
import { AutoComplete, Button, Input, Modal, Collapse } from 'antd';
import { file_actions_keys, code_editor_keys, cell_shortcuts_keys, window_shortcuts_keys, ShortcutTitle, osKeys, HelpType } from 'util/utils';
import PropTypes from "prop-types";
import { Link } from "./stdlib/link";
import { osfunction, OSType } from 'util/helpers';
import { GUIDE_LINK_URI } from 'util/localstorage';
import NeoReportExamplesComp from 'block/antchart/modal/NeoReportExamplesComp';
import { ModalType } from './util/utils';

const { Panel } = Collapse;

const DisplayType = {
    doc: "doc",
    collapse: "collapse"
}
export default class Help extends React.Component {
    static propTypes = {
        className: PropTypes.string
    };
    static defaultProps = {};

    constructor(props) {
        super(props);
        this.state = {
            className: this.props.className,
            tabIndex: "collapse",
            defaultActiveKey: [props.defaultActiveKey || HelpType['Keyboard shortcuts']],
            displayType: DisplayType.collapse,
        }
    }

    componentDidMount() {
    }

    getGrid = (w) => {
        if (w >= 1600) {
            return "xxl"
        } else if (w >= 1200) {
            return "xl";
        } else if (w >= 992) {
            return "lg"
        } else if (w >= 768) {
            return "md"
        } else if (w >= 576) {
            return "sm"
        } else {
            return "xs"
        }
    }

    componentDidUpdate(preProps, preState) {
        if (preProps.className !== this.props.className) {
            this.setState({ className: this.props.className })
        }
        if (this.props.defaultActiveKey && preProps.defaultActiveKey !== this.props.defaultActiveKey) {
            this.setState({ defaultActiveKey: this.props.defaultActiveKey })
        }
    }

    render() {
        const { className, defaultActiveKey, displayType } = this.state;
        let text = "not finish";
        //         const tabs = {
        //             "Getting started": {
        //                 "Create a notebook": <React.Fragment>
        //                     <p>To create a notebook, click <b>
        //                         <i className="icon fas fa-file" title={ShortcutTitle(file_actions_keys['New File'])}></i>
        //                     </b> in the header. If desired, choose from one of our {Link({ fileKey: `${GUIDE_LINK_URI}user-manual/user-manual`, text: "User Manual" })}. Or don’t, and you’ll get a blank notebook. Then click <b>Create notebook</b> or press <b title="Enter">Enter</b>. You can also double-click a template to create a notebook.</p>
        //                     <p>After creating a notebook, your changes will be saved automatically as you edit. Have fun!</p>
        //                 </React.Fragment>,
        //                 "Create a cell": <React.Fragment>
        //                     <p>Notebooks are composed of cells, all of which are live and editable. We encourage tinkering—it’s a great way to learn and experiment. Don’t be afraid to try something and see what happens. If it breaks, you can always undo!</p>
        //                     <p>Insert a new cell by clicking <svg viewBox="0 0 16 16" width="16" height="16" stroke="currentColor" stroke-width="2" class="nb1"><path d="M3 8h10M8 3v10"></path></svg> in the left margin, or by pressing <b title="Ctrl-Enter">Ctrl-Enter</b> when editing a cell. You can also split cells with <b title="Alt-Enter">Alt-Enter</b>.</p>
        //                     <p>A cell’s code is shown in gray (or blue when focused) below the cell’s output. If a cell’s code is currently hidden, you can click the left margin adjacent to the cell to open it and focus the editor.</p>
        //                     <p>You can write in multiple languages. Cells are <b><svg width="16" height="16" viewBox="0 0 16 16" fill="none" class="nb1"><path fill-rule="evenodd" clip-rule="evenodd" d="M12.9009 13.0686C12.3333 13.6581 11.5224 14 10.5556 14H9.5V12H10.5556C11.0331 12 11.3056 11.8419 11.4602 11.6814C11.6267 11.5085 11.7222 11.2642 11.7222 11V10.5C11.7222 10.2276 11.7301 9.79053 12.14 9C12.5 8.39782 13.25 8 13.25 8C13.25 8 12.4702 7.69361 12.14 7.05676C11.7301 6.26622 11.7222 5.77243 11.7222 5.5V5C11.7222 4.73583 11.6267 4.49147 11.4602 4.31864C11.3056 4.1581 11.0331 4 10.5556 4H9.5L9.5 2H10.5556C11.5224 2 12.3333 2.3419 12.9009 2.93136C13.4567 3.50853 13.7222 4.26417 13.7222 5V5.5C13.7222 5.72757 13.7421 6.14272 13.9155 6.47718C13.9933 6.6271 14.0935 6.74371 14.2303 6.82779C14.3663 6.91136 14.5987 7 15 7V9C14.5987 9 14.3663 9.08864 14.2303 9.17221C14.0935 9.25629 13.9933 9.3729 13.9155 9.52282C13.7421 9.85728 13.7222 10.2724 13.7222 10.5V11C13.7222 11.7358 13.4567 12.4915 12.9009 13.0686Z" fill="currentColor"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M3.09912 13.0686C3.66675 13.6581 4.47757 14 5.44444 14H6.5V12H5.44444C4.96688 12 4.69436 11.8419 4.53976 11.6814C4.37334 11.5085 4.27778 11.2642 4.27778 11V10.5C4.27778 10.2276 4.26988 9.79053 3.85998 9C3.5 8.39782 2.75 8 2.75 8C2.75 8 3.52976 7.69361 3.85998 7.05676C4.26988 6.26622 4.27778 5.77243 4.27778 5.5V5C4.27778 4.73583 4.37334 4.49147 4.53976 4.31864C4.69436 4.1581 4.96688 4 5.44444 4H6.5L6.5 2H5.44444C4.47757 2 3.66675 2.3419 3.09912 2.93136C2.54333 3.50853 2.27778 4.26417 2.27778 5V5.5C2.27778 5.72757 2.25789 6.14272 2.08447 6.47718C2.00673 6.6271 1.90646 6.74371 1.7697 6.82779C1.63375 6.91136 1.40134 7 1 7L1 9C1.40134 9 1.63375 9.08864 1.7697 9.17221C1.90646 9.25629 2.00673 9.3729 2.08447 9.52282C2.25789 9.85728 2.27778 10.2724 2.27778 10.5V11C2.27778 11.7358 2.54333 12.4915 3.09912 13.0686Z" fill="currentColor"></path></svg> JavaScript</b> by default. When the focused cell is empty, change its language by pressing <b title="Left">←</b> or <b title="Right">→</b>. When the cell isn’t empty, use the language menu to the left of the editor.</p>
        //                     <p>After editing code, click <b><svg width="16" height="16" fill="none" stroke="currentColor" stroke-linejoin="round" stroke-width="2" viewBox="0 0 16 16" class="nb1"><path d="M4 12.4788V3.52116C4 2.23802 5.50646 1.54723 6.47878 2.38451L11.68 6.86335C12.375 7.46178 12.375 8.53822 11.68 9.13665L6.47878 13.6155C5.50646 14.4528 4 13.762 4 12.4788Z"></path></svg> Play</b> to run it or press <b title="Shift-Enter">Shift-Enter</b>. Your changes will also run automatically if you close the cell or focus another cell. Any cells that reference the edited cell’s value will run automatically, too.</p>
        //                     <p>To move cells, click and drag the <svg viewBox="0 0 16 16" width="16" height="16" fill="currentColor" class="nb1"><circle r="1.5" cx="8" cy="2.5"></circle><circle r="1.5" cx="8" cy="7.5"></circle><circle r="1.5" cx="8" cy="12.5"></circle></svg> cell menu. Or with a cell focused, click <svg width="16" height="16" viewBox="0 0 16 16" fill="none" class="nb1"><rect x="7" y="4" width="2" height="10" fill="currentColor"></rect><path d="M4 7L8 3L12 7" stroke="currentColor" stroke-width="2" stroke-linejoin="round"></path></svg> or <svg width="16" height="16" viewBox="0 0 16 16" fill="none" class="nb1"><rect x="9" y="12" width="2" height="10" transform="rotate(-180 9 12)" fill="currentColor"></rect><path d="M12 9L8 13L4 9" stroke="currentColor" stroke-width="2" stroke-linejoin="round"></path></svg> in the toolbar or press <b title="Ctrl-Shift-Up">Ctrl-Shift-↑</b> or <b title="Ctrl-Shift-Down">Ctrl-Shift-↓</b>. To delete a cell, double-click <b><svg width="16" height="16" viewBox="0 0 16 16" fill="none" class="nb1"><path d="M5 3V5H11V3L5 3ZM3 3V5H1V7H3V13C3 14.1046 3.89543 15 5 15H11C12.1046 15 13 14.1046 13 13V7H15V5H13V3C13 1.89543 12.1046 1 11 1H5C3.89543 1 3 1.89543 3 3ZM5 13V7H7V13H5ZM9 13H11V7H9L9 13Z" fill="currentColor" stroke-width="2" fill-rule="evenodd" clip-rule="evenodd"></path></svg> Delete</b> in the cell menu or toolbar. You can also delete an empty cell by pressing <b title="Alt-Backspace">Alt-Backspace</b>.</p>
        //                     <p>On your own notebooks, changes are saved automatically as you edit. If you edit others’ notebooks, only you can see your changes. Your changes will be discarded if you leave, so <a role="link" tabindex="0" class="blue pointer underline-hover">fork</a> if you wish to save them! Your changes will then be applied to the fork.</p>
        //                 </React.Fragment>,
        //                 "Write JavaScript": <React.Fragment>
        //                     <p>JavaScript cells can contain expressions, blocks, functions, classes, or <a role="link" tabindex="0" class="blue pointer underline-hover">imports</a>. If a cell contains multiple statements, it must be wrapped in curly braces as a block. The returned value then defines the cell’s value.</p>
        //                     <div class="jsx-1134405157 snippet relative"><pre class="jsx-1134405157 f7 br2 pa2 overflow-x-auto bg-near-white">2 * 3 * 7</pre><button title="Copy code" class="jsx-1134405157 button absolute top-0 right-0 bg-near-white hover-bg-white pointer ba b--near-white br2 flex pa1 ma1"><svg width="16" height="16" viewBox="0 0 16 16" fill="none"><path d="M2 6C2 5.44772 2.44772 5 3 5H10C10.5523 5 11 5.44772 11 6V13C11 13.5523 10.5523 14 10 14H3C2.44772 14 2 13.5523 2 13V6Z" stroke="currentColor" stroke-width="2"></path><path d="M4 2.00004L12 2.00001C13.1046 2 14 2.89544 14 4.00001V12" stroke="currentColor" stroke-width="2"></path></svg></button></div>
        //                     <div class="jsx-1134405157 snippet relative"><pre class="jsx-1134405157 f7 br2 pa2 overflow-x-auto bg-near-white">{`function add(x, y) {
        //   return x + y;
        // }`}</pre><button title="Copy code" class="jsx-1134405157 button absolute top-0 right-0 bg-near-white hover-bg-white pointer ba b--near-white br2 flex pa1 ma1"><svg width="16" height="16" viewBox="0 0 16 16" fill="none"><path d="M2 6C2 5.44772 2.44772 5 3 5H10C10.5523 5 11 5.44772 11 6V13C11 13.5523 10.5523 14 10 14H3C2.44772 14 2 13.5523 2 13V6Z" stroke="currentColor" stroke-width="2"></path><path d="M4 2.00004L12 2.00001C13.1046 2 14 2.89544 14 4.00001V12" stroke="currentColor" stroke-width="2"></path></svg></button></div>
        //                     <div class="jsx-1134405157 snippet relative"><pre class="jsx-1134405157 f7 br2 pa2 overflow-x-auto bg-near-white">{`xy = {
        //                         let x = 1;
        //   let y = 2;
        //   return x * y;
        // }`}</pre><button title="Copy code" class="jsx-1134405157 button absolute top-0 right-0 bg-near-white hover-bg-white pointer ba b--near-white br2 flex pa1 ma1"><svg width="16" height="16" viewBox="0 0 16 16" fill="none"><path d="M2 6C2 5.44772 2.44772 5 3 5H10C10.5523 5 11 5.44772 11 6V13C11 13.5523 10.5523 14 10 14H3C2.44772 14 2 13.5523 2 13V6Z" stroke="currentColor" stroke-width="2"></path><path d="M4 2.00004L12 2.00001C13.1046 2 14 2.89544 14 4.00001V12" stroke="currentColor" stroke-width="2"></path></svg></button></div>
        //                     <p>Local variables within a block (such as <i>x</i> and <i>y</i> above) are not accessible by other cells. But cells can be named (such as <i>xy</i>), allowing you to refer to the cell’s return value from another cell. Expression cells can be named, too.</p>
        //                     <div class="jsx-1134405157 snippet relative"><pre class="jsx-1134405157 f7 br2 pa2 overflow-x-auto bg-near-white">xyz = xy * 3</pre><button title="Copy code" class="jsx-1134405157 button absolute top-0 right-0 bg-near-white hover-bg-white pointer ba b--near-white br2 flex pa1 ma1"><svg width="16" height="16" viewBox="0 0 16 16" fill="none"><path d="M2 6C2 5.44772 2.44772 5 3 5H10C10.5523 5 11 5.44772 11 6V13C11 13.5523 10.5523 14 10 14H3C2.44772 14 2 13.5523 2 13V6Z" stroke="currentColor" stroke-width="2"></path><path d="M4 2.00004L12 2.00001C13.1046 2 14 2.89544 14 4.00001V12" stroke="currentColor" stroke-width="2"></path></svg></button></div>
        //                     <p>When you refer to another cell, code will run automatically whenever the referenced cell updates. Hence, the order of cells in the notebook does not affect the order in which code runs; rearrange cells in whatever order you find most readable! See our <a target="_blank" class="blue" rel="noopener noreferrer" href="/@observablehq/observables-not-javascript">guide to Observable JavaScript</a> and our <a target="_blank" class="blue" rel="noopener noreferrer" href="/@observablehq/how-observable-runs">guide to the Observable runtime</a> for more.</p>

        //                 </React.Fragment>,
        //                 "Write Markdown": <React.Fragment></React.Fragment>
        //             },
        //             "Loading data": `Attach a file
        //             Query a database
        //             Fetch from a URL
        //             Use a secret key`,
        //             "Exploring data": `Add a table
        //             Add a chart
        //             Add an interactive input`,
        //             "Reusing code": `Load a JavaScript library
        //             Import a cell from a notebook
        //             Search for a notebook
        //             Fork a notebook`,
        //             "Collaborating": `Invite people to a notebook
        //             Comment on a notebook
        //             Publish a notebook
        //             Send a suggestion
        //             Merge changes from a fork
        //             Export or embed a notebook
        //             Create a team`,
        //             "Troubleshooting": `View and revert changes
        //             Use the debugger
        //             Fix a stuck notebook`,
        //         }
        //         const uri = `${GUIDE_LINK_URI}`;
        //         let links = [`Grove’s not JavaScript
        //         Grove Inputs
        //         Grove Plot
        //         Learn D3
        //         Browse tutorials
        //         Ask a question
        //         Email support
        //         `]
        let datas = [{
            //     key: "Getting started",
            //     content: <div>
            //         <div><a className="link clicker">Create a notebook</a></div>
            //     </div>,
            // },
            // {
            //     key: "Loading data",
            //     content: text,
            // },
            // {
            //     key: "Exploring data",
            //     content: text,
            // },
            // {
            //     key: "Reusing code",
            //     content: text,
            // },
            // {
            //     key: "Collaborating",
            //     content: text,
            // },
            // {
            //     key: "Troubleshooting",
            //     content: text,
            // },
            // {
            key: "Links",
            content: <div>
                <div className='pb2'>{Link({ fileKey: `${GUIDE_LINK_URI}user-manual/user-manual`, text: "User Manual" })}</div>
                <div className='pb2'>
                    <a className='link cliker' onClick={() => {
                        window.currentEditor.react_component.setState({
                            modalType: ModalType.NeoDashDashboardGallery,
                            modalData: {
                            }
                        });
                    }}>
                        NeoDash Dashboard Gallery
                    </a>
                </div>
            </div>,
        },
        {
            key: HelpType['Keyboard shortcuts'],
            content: <div>
                <div className="pb2 bb b--silver">
                    <div className="ph3 mt2 mb1 f7 fw5 mid-gray lh-f7">Press {window_shortcuts_keys['Help?'].keys.join("+")} to toggle this panel, then Esc to hide them again.</div>
                </div>
                <div className="ph3">
                    <div className="fw6">File actions</div>
                    <div className="mt2 mb3 f7 fw5 mid-gray">Shortcuts for operating on the file system.</div>
                    {_.map(file_actions_keys, (value, key) => {
                        return <div key={value.title} className="d-flex justify-content-between bt b--light-silver">
                            <div className="flex-auto pv2">{value.title}</div>
                            <div className="nowrap pv2">
                                {_.map(osKeys(value), (key) => {
                                    return <span key={key} className="keyboard-key ml2 ph1">{key}</span>
                                })}
                            </div>
                        </div>
                    })}
                </div>
                <div className="ph3">
                    <div className="fw6">Editing code</div>
                    <div className="mt2 mb3 f7 fw5 mid-gray">Use keyboard for auto-complete, auto-identation, commenting and more.</div>
                    {_.map(code_editor_keys, (value, key) => {
                        return <div key={value.title} className="d-flex justify-content-between bt b--light-silver">
                            <div className="flex-auto pv2">{value.title}</div>
                            <div className="nowrap pv2">
                                {_.map(osKeys(value), (key) => {
                                    return <span key={key} className="keyboard-key ml2 ph1">{key}</span>
                                })}
                            </div>
                        </div>
                    })}
                </div>
                <div className="ph3">
                    <div className="fw6">Cell Shortcuts</div>
                    <div className="mt2 mb3 f7 fw5 mid-gray">Commands operating on the cell itself.</div>
                    {_.map(cell_shortcuts_keys, (value, key) => {
                        return <div key={value.title} className="d-flex justify-content-between bt b--light-silver">
                            <div className="flex-auto pv2">{value.title}</div>
                            <div className="nowrap pv2">
                                {_.map(osKeys(value), (key) => {
                                    return <span key={key} className="keyboard-key ml2 ph1">{key}</span>
                                })}
                            </div>
                        </div>
                    })}
                </div>
                <div className="ph3">
                    <div className="fw6">Window Shortcuts</div>
                    <div className="mt2 mb3 f7 fw5 mid-gray">Commands operating on the windows.</div>
                    {_.map(window_shortcuts_keys, (value, key) => {
                        return <div key={value.title} className="d-flex justify-content-between bt b--light-silver">
                            <div className="flex-auto pv2">{value.title}</div>
                            <div className="nowrap pv2">
                                {_.map(osKeys(value), (key) => {
                                    return <span key={key} className="keyboard-key ml2 ph1">{key}</span>
                                })}
                            </div>
                        </div>
                    })}
                </div>
                <div className="ph3">
                    <div className="fw6">Multiple tabs</div>
                    <div className="mt2 mb3 f7 fw5 mid-gray">Operating on the tabs.</div>
                    <div key={"clickOpen"} className="d-flex justify-content-between bt b--light-silver">
                        <div className="flex-auto pv2">Click open file in new tab</div>
                        <div className="nowrap pv2">
                            <span className="keyboard-key ml2 ph1">{osfunction() === OSType.MacOS ? "⌘" : "Ctrl"}</span>
                            <span className="keyboard-key ml2 ph1">Click the file</span>
                        </div>
                    </div>
                    <div key={"dragOpen"} className="d-flex justify-content-between bt b--light-silver">
                        <div className="flex-auto pv2">Drag open file in new tab</div>
                        <div className="nowrap pv2">
                            <span className="keyboard-key ml2 ph1">Drag the file drop to editor</span>
                        </div>
                    </div>
                    <div key={"clickImports"} className="d-flex justify-content-between bt b--light-silver">
                        <div className="flex-auto pv2">Click open file in current tab</div>
                        <div className="nowrap pv2">
                            <span className="keyboard-key ml2 ph1">Click the link of imports</span>
                        </div>
                    </div>
                    <div key={"newTabClickImports"} className="d-flex justify-content-between bt b--light-silver">
                        <div className="flex-auto pv2">Click open file in new tab</div>
                        <div className="nowrap pv2 shortcut-plus">
                            <span className="keyboard-key ml2 ph1">{osfunction() === OSType.MacOS ? "⌘" : "Ctrl"}</span>
                            <span className="keyboard-key ml2 ph1">Click the link of imports</span>
                        </div>
                    </div>
                </div>
            </div>

        }, {
            key: HelpType['Neo4j Report Examples'],
            content: <NeoReportExamplesComp></NeoReportExamplesComp>,
        },]
        return <div className={`help ${className || ''}`}>
            <div>
                <div>Welcome! Grove is a place for everyone to work together with data.</div>
                <Collapse className={`${displayType === DisplayType.collapse ? "" : "hide"}`} accordion activeKey={defaultActiveKey} ghost onChange={(key) => {
                    this.setState({ defaultActiveKey: key })
                }}>
                    {
                        _.map(datas, (data, index) => {
                            return <Panel header={<span className="fw6">{data.key}</span>} key={data.key}>
                                {data.content}
                            </Panel>
                        })
                    }
                </Collapse>
                {/* <Doc className={`${displayType === DisplayType.doc ? "" : "hide"}`}></Doc> */}
            </div>
        </div>
    }
}