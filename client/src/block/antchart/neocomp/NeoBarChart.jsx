import React, { useEffect } from 'react';
import { NoDrawableDataErrorMessage } from './CodeViewerComponent';
import { convertRecordObjectToString, recordToNative } from '../util/ChartUtils';
import DrawPlot from '../draw/DrawPlot'
import { getChartConfig } from '../comp/CheckChartTypes';
import { ChartTypes, extractDatas, extractOptions, getKeys } from '../comp/OptionsTypes';
import { extractColumnDatas } from '../comp/TableComp';
import { autoObjectsType } from 'util/helpers';

/**
 * Embeds a BarReport (from Nivo) into NeoDash.
 *  This visualization was extracted from https://github.com/neo4j-labs/charts.
 * TODO: There is a regression here with nivo > 0.73 causing the bar chart to have a very slow re-render.
 */
const NeoBarChart = (props) => {

  const { records, selection } = props;

  const settings = props.settings ? props.settings : {};

  if (!selection || props.records == null || props.records.length == 0 || props.records[0].keys == null) {
    return <NoDrawableDataErrorMessage />;
  }

  /**
   * The code fragment below is a workaround for a bug in nivo > 0.73 causing bar charts to re-render very slowly.
   */
  const [loading, setLoading] = React.useState(false);
  useEffect(() => {
    setLoading(true);
    const timeOutId = setTimeout(() => {
      setLoading(false);
    }, 1);
    return () => clearTimeout(timeOutId);
  }, [props.selection]);

  const [keys, setKeys] = React.useState({});
  const [data, setData] = React.useState([]);

  useEffect(() => {
    let newKeys = {};
    let newData = records
      .reduce((data, row) => {
        try {
          if (!selection || !selection.index || !selection.value) {
            return data;
          }
          const index = convertRecordObjectToString(row.get(selection.index));
          const idx = data.findIndex((item) => item.index === index);

          const key = selection.key !== '(none)' ? recordToNative(row.get(selection.key)) : selection.value;
          const rawValue = recordToNative(row.get(selection.value));
          const value = rawValue !== null ? rawValue : 0.0000001;
          if (isNaN(value)) {
            return data;
          }
          newKeys[key] = true;

          if (idx > -1) {
            data[idx][key] = value;
          } else {
            data.push({ index, [key]: value });
          }
          return data;
        } catch (e) {
          // eslint-disable-next-line no-console
          console.error(e);
          return [];
        }
      }, [])
      .map((row) => {
        Object.keys(newKeys).forEach((key) => {
          // eslint-disable-next-line no-prototype-builtins
          if (!row.hasOwnProperty(key)) {
            row[key] = 0;
          }
        });
        return row;
      });

    setKeys(newKeys);
    setData(newData);
  }, [selection]);

  if (loading) {
    return <></>;
  }

  const { columns, rows } = extractColumnDatas(false, records, true, true);

  let exOptions = _.assign({}, settings,
    {
      data: autoObjectsType(rows, false)
    }
  );
  exOptions.yField = selection.index
  exOptions.xField = selection.value
  if (selection.key !== '(none)') {
    exOptions.seriesField = selection.key
    exOptions.isGroup = false;
    exOptions.isStack = true;
    exOptions.legend = false;
  }
  const darkMode = document.body.classList.contains("darkmode");

  const chart = (
    <DrawPlot type={exOptions.seriesField ? ChartTypes['Multi Bar'] : ChartTypes['Basic Bar']} exOptions={exOptions} darkMode={darkMode}></DrawPlot>
  );

  return chart;
};

export default NeoBarChart;
