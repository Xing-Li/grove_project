import { Button, Select, TreeSelect, Input, Row, Col, Form, Checkbox, Switch } from 'antd';
import { FolderOutlined } from '@ant-design/icons';
import Text from 'antd/lib/typography/Text';
import axios from 'axios';
import _ from 'lodash';
import PropTypes from "prop-types";
import React from 'react';
import editor from '../commonEditor.js';
import { InitSettings } from '../editorSettings';
import MdFiles from './MdFiles';
import Modal from 'antd/lib/modal/Modal';
import { WIDTH, Z_INDEX, Title, assembleFileData, getTreeData, getFileTreeData, fileNameReg, readAsDataURL, fileSeparator, getFolderKey, getFileKey, getFileName } from './fileUtils.js';
import { ModalType, showConfirm, showToast } from '../util/utils.js';
import { DefaultFileType, DefaultOptions, USER_ID } from '../util/localstorage.js';
import { isFileGzip, isFile, isDir, isFileZip, SelectType, isFileCsv } from '../util/helpers.js';

const { Option } = Select;
export default class SyncFile extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        width: PropTypes.number,
        react_component: PropTypes.object,
        refreshType: PropTypes.number,
    };
    static defaultProps = {};

    constructor(props) {
        super(props);
        this.state = {
            /**
             * @type {{ projectId, userId, folderKey: dir,
             * name: arr[index], shareProjectId }}
            */
            value: undefined,
            treeData: undefined,
            syncFileName: undefined,
            variantName: undefined,
            initRead: false,
            /**@type Object.<string,{
                userId, projectName, projectId,shareProjectId, fileNamesJson
            }> */
            cache: {},
            valuesChange: false,
        }
        this.formRef = React.createRef();
    }

    onChange = value => {
        let state = { value: value }
        _.assign(state, { syncFileName: this.getSyncFileName(value) });
        this.setState(state, () => {
            this.formRef.current.setFieldsValue(this.state);
        });
        console.log(value);
    };

    componentDidMount() {
    }

    getSyncFileName(value) {
        const { projectId, userId, shareProjectId, folderKey, name, fileKey } = JSON.parse(value);
        let fileName = (!folderKey && fileKey) ? getFileName(fileKey) : "";
        return fileName.toLowerCase().endsWith(".csv") ? fileName.substring(0, fileName.length - ".csv".length) : ""
    }

    refreshTreeData = async () => {
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
        const { projectId, modalType, treeSelectType, modalData: { syncFileKey, variantName } } = react_component.state;
        let onlyCurrentProject = true;
        let cache = {};
        let options = _.cloneDeep(DefaultOptions());
        let currentDataFunc = () => {
            const { fileNamesJson, projectId, userId, shareProjectId } = react_component.state;
            cache[projectId] = {
                userId, projectName: react_component.props.projectName, projectId,
                shareProjectId, fileNamesJson
            }
            return getTreeData(fileNamesJson, projectId, react_component.props.projectName, userId, shareProjectId, treeSelectType);
        }
        let treeData = currentDataFunc();
        let defaultValue = treeData.length > 0 ? ((syncFileKey && _.filter(treeData, (curr) => { return JSON.parse(curr.value).fileKey === syncFileKey })[0] || {}).id || treeData[0].id) : undefined;
        if (!onlyCurrentProject) {
            let leftOptions = _.filter(options, (value, index) => {
                return value.projectId !== projectId;
            })
            for (let index = 0; index < leftOptions.length; index++) {
                let option = leftOptions[index];
                const { userId, projectName, projectId } = option;
                let res = await axios.post(`/api/grove/load`, {
                    userId: userId, settings: InitSettings, projectId: projectId
                });
                let fileNamesJson = res.data.content;
                let shareProjectId = userId === USER_ID ? undefined : projectId;
                treeData.push(...getTreeData(fileNamesJson, projectId, projectName, userId, shareProjectId, treeSelectType));
                cache[projectId] = {
                    userId, projectName, projectId,
                    shareProjectId, fileNamesJson
                }
            }
        }
        this.setState({
            treeData,
            value: defaultValue,
            syncFileName: defaultValue ? this.getSyncFileName(defaultValue) : "",
            variantName,
            cache,
            valuesChange: false,
        }, () => {
            this.formRef.current.setFieldsValue(this.state);
            console.log("refresh data")
        });

    }

    componentDidUpdate(prevProps, prevState) {
        if ((prevProps.refreshType !== this.props.refreshType) && (this.props.refreshType === 1)) {
            this.refreshTreeData();
        }
    }

    handleOk = async () => {
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
        const { modalType } = react_component.state;
        react_component.setWrapperLoading(true);
        if (modalType === ModalType.SyncFile) {
            await this.syncFile();
        }
        react_component.setWrapperLoading(false);
    };

    syncFile = async () => {
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
        const { modalData: { cbFunc, content } } = react_component.state;
        const { syncFileName, cache, variantName, initRead } = this.state;
        const { projectId, userId, shareProjectId, folderKey, fileKey } = JSON.parse(this.state.value);
        if (syncFileName) {
            let newFileKey = `${folderKey ? `${folderKey}${syncFileName}` : getFileKey(fileKey, syncFileName)}.csv`;
            if (!react_component.state.fileNamesJson[newFileKey]) {
                await react_component.newFileFunc({ target: { value: `${syncFileName}.csv` } }, content, newFileKey, false)
            }
            cbFunc({ syncFileKey: newFileKey, variantName, initRead });
        } else {
            cbFunc({ syncFileKey: undefined, variantName, initRead });
        }
        // const currentProjectId = react_component.state.projectId;
        // if (projectId === currentProjectId) {
        //     const { fileNamesJson } = react_component.state;
        //     if (fileNamesJson[newFileKey] && !await showConfirm(`File exists, Overwrite ${newFileKey}?`)) {
        //         return;
        //     }
        //     if (await react_component.uploadMdFile(_.assign(_.cloneDeep(sourceFile), {
        //         fileKey: newFileKey,
        //         name: newName
        //     }), undefined, content, state, fileVersion, false)) {
        //         showToast("Copy File Successful", "info");
        //     }
        // } else if (cache[projectId]) {
        //     const { fileNamesJson } = cache[projectId];
        //     if (fileNamesJson[newFileKey] && !await showConfirm(`File exists, Overwrite ${newFileKey}?`)) {
        //         return;
        //     }
        //     await react_component.copyFile(_.assign(_.cloneDeep(sourceFile), {
        //         fileKey: newFileKey,
        //         name: newName
        //     }), userId, projectId, shareProjectId,
        //         undefined, content, fileVersion, false);
        // }
    }

    render() {
        const { className, width } = this.props;
        const { treeData, value, cache, syncFileName, variantName, valuesChange } = this.state;
        if (!treeData) {
            return <div></div>;
        }
        const { projectId, userId, shareProjectId, folderKey, name, fileKey } = JSON.parse(this.state.value);
        let projectName = cache[projectId].projectName;
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
        const { modalType, modalData: { } } = react_component.state;
        const closeFunc = () => {
            react_component.setState({
                modalType: ModalType.None,
            });
        };
        const onFinish = (values) => {
            console.log('Success:', values);
            this.handleOk();
            closeFunc();
        };
        const onFinishFailed = (errorInfo) => {
            showToast(_.reduce(errorInfo.errorFields, (prev, curr, index) => {
                prev += curr.errors.join(",")
                return prev + "; ";
            }, ""), 'error')
            console.log('Failed:', errorInfo);
        };
        return <Modal zIndex={Z_INDEX}
            className={`modal-comp data-html2canvas-ignore ${className}`}
            width={width || WIDTH}
            title={<Title modalType={`${modalType}`}></Title>}
            visible={~[ModalType.SyncFile].indexOf(modalType)}
            onCancel={closeFunc}
            footer={null}
        >
            <Form layout={"horizontal"}
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 18 }}
                ref={this.formRef}
                initialValues={this.state}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                onValuesChange={() => { this.setState({ valuesChange: true }) }}
            >
                <Form.Item label={"Variant Name"} name="variantName" rules={[
                    {
                        required: true,
                    },
                ]}>
                    <Input onChange={(e) => {
                        if (!e.target.value.match(fileNameReg)) {
                            e.target.value = variantName;
                            return;
                        }
                        this.setState({ variantName: e.target.value });
                    }} />
                </Form.Item>
                <Form.Item label={"Select Save File's Position"} name="value" rules={[
                ]}>
                    <TreeSelect
                        treeDataSimpleMode
                        treeLine={{ showLeaf: false }}
                        dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                        placeholder={"Please Select File Position"}
                        onChange={this.onChange}
                        // loadData={this.onLoadData}
                        treeData={treeData}
                    />
                </Form.Item>
                <Form.Item label={`File Name`} >
                    <Row gutter={8}>
                        <Col span={12}>
                            <Form.Item name="syncFileName" rules={[
                            ]}>

                                <Input onChange={(e) => {
                                    if (!e.target.value.match(fileNameReg)) {
                                        e.target.value = syncFileName;
                                        return;
                                    }
                                    this.setState({ syncFileName: e.target.value });
                                }} />
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            <span className="ant-form-text">.csv</span>
                            <span><Button key="open" onClick={() => {
                                const { folderKey, fileKey } = JSON.parse(this.state.value);
                                let newFileKey = `${folderKey ? `${folderKey}${syncFileName}` : getFileKey(fileKey, syncFileName)}.csv`;
                                if (react_component.state.fileNamesJson[newFileKey]) {
                                    react_component.switchFileTagEditor(newFileKey)
                                }
                            }}>
                                Open
                            </Button></span>
                        </Col>
                    </Row>
                </Form.Item>
                <Form.Item label="Init Read" name="initRead" valuePropName="checked" rules={[
                    {
                        required: true,
                    },
                ]}>
                    <Switch onChange={(checked) => {
                        this.setState({ initRead: checked })
                    }} />
                </Form.Item>
                <Form.Item key="back"  >
                    <div className="d-flex justify-content-end">
                        <Button key="back" onClick={closeFunc}>
                            Cancel
                        </Button>&nbsp;&nbsp;
                        <Button key="addNew" disabled={!variantName} type="primary" htmlType="submit" disabled={!valuesChange}>
                            Confirm
                        </Button>
                    </div>
                </Form.Item>
            </Form>
        </Modal>
    }
}
