import * as remark from 'remark';
import { parseHeaderToMarkdown, parseMarkdownToHeader } from './BlockTypeParsers/HeaderTypeParser';
import { parseParagraphToMarkdown, parseTableToMarkdown, parseMarkdownToParagraph } from './BlockTypeParsers/ParagraphTypeParser';
import { parseListToMarkdown, parseMarkdownToList } from './BlockTypeParsers/ListTypeParser';
import { parseDelimiterToMarkdown, parseMarkdownToDelimiter } from './BlockTypeParsers/DelimiterTypeParser';
import { parseImageToMarkdown } from './BlockTypeParsers/ImageTypeParser';
import { parseSimpleImageToMarkdown } from './BlockTypeParsers/SimpleImageTypeParser';
import { parseCheckboxToMarkdown } from './BlockTypeParsers/CheckboxTypeParser';
import { parseQuoteToMarkdown, parseMarkdownToQuote } from './BlockTypeParsers/QuoteTypeParser';
import { parseCodeToMarkdown, parseCodeToolToMarkdown, parseMarkdownToCode } from './BlockTypeParsers/CodeTypeParser';
/**
 * Function which takes data and creates a markdown file
 * @param content of the file
 * @param filename
 *
 */
export function fileDownloadHandler(content, fileName) {
  const file = new File([content], { type: 'text/markdown', endings: 'transparent' });
  const url = URL.createObjectURL(file);

  const element = document.createElement('a');
  document.body.appendChild(element);
  element.href = url;
  element.download = fileName;
  element.click();
  window.URL.revokeObjectURL(url);
  document.body.removeChild(element);
}

/**
 * 
 * @param {{blocks:[]}} data 
 * @returns {string}
 */
export function editorjsTransferToMd(data) {
  const initialData = {};
  initialData.content = data.blocks;
  const parsedData = initialData.content.map((item) => {
    // iterate through editor data and parse the single blocks to markdown syntax
    switch (item.type) {
      case 'header':
        return parseHeaderToMarkdown(item.data);
      case 'paragraph':
        return parseParagraphToMarkdown(item.data);
      case 'list':
        return parseListToMarkdown(item.data);
      case 'delimiter':
        return parseDelimiterToMarkdown(item);
      case 'antChart':
        return parseDelimiterToMarkdown(item);
      case 'image':
        return parseImageToMarkdown(item.data);
      case 'simpleImage':
        return parseSimpleImageToMarkdown(item.data);
      case 'quote':
        return parseQuoteToMarkdown(item.data);
      case 'checkbox':
        return parseCheckboxToMarkdown(item.data);
      case 'code':
        return parseCodeToMarkdown(item.data);
      case 'codeTool':
        return parseCodeToolToMarkdown(item.data);
      case 'checklist':
        return parseCheckboxToMarkdown(item.data);
      case 'table':
        return parseTableToMarkdown(item.data);
      default:
        break;
    }
  });
  return parsedData.join('\n');
}
export const editorData = [];

/**
 * 
 * @param {string | ArrayBuffer} content 
 * @returns {[]}
 */
export function mdTransferToEditorjs(content) {
  // empty the array before running the function again
  editorData.length = 0;
  const parsedMarkdown = remark().parse(content);
  // iterating over the pared remarkjs syntax tree and executing the json parsers
  parsedMarkdown.children.forEach((item, index) => {
    switch (item.type) {
      case 'heading':
        return editorData.push(parseMarkdownToHeader(item));
      case 'paragraph':
        return editorData.push(parseMarkdownToParagraph(item));
      case 'list':
        return editorData.push(parseMarkdownToList(item));
      case 'thematicBreak':
        return editorData.push(parseMarkdownToDelimiter());
      case 'code':
        return editorData.push(parseMarkdownToCode(item));
      case 'blockquote':
        return editorData.push(parseMarkdownToQuote(item));
      default:
        break;
    }
  });
  return editorData.filter((value) => Object.keys(value).length !== 0); // filter through array and remove empty objects
}
