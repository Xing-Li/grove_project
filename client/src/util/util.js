
import _ from "lodash";
import Common from "./Common";

const Util = function () {};

Util.loadJS = function (jsURL) {
  return new Promise((resolve, reject) => {
    if (
      _.find(document.getElementsByTagName("script"), (e) => e.src === jsURL)
    ) {
      resolve("done");
    } else {
      let jsDom = document.createElement("script");
      jsDom.type = "text/javascript";
      jsDom.async = true;
      jsDom.src = jsURL;
      jsDom.onload = function () {
        resolve("done");
      };
      let s = document.getElementsByTagName("script")[0];
      s.parentNode.insertBefore(jsDom, s);
    }
  });
};

Util.sleep = (m) => new Promise((r) => setTimeout(r, m));

Util.getFileName = (pathStr, isPath = false) => {
  let tempPathStr = String(pathStr);
  let tmpIndex = tempPathStr.lastIndexOf("/");
  if (!isPath) {
    return tempPathStr.substr(tmpIndex + 1);
  } else {
    return tempPathStr.substr(0, tmpIndex);
  }
};

Util.getProjectId = function getProjectId() {
  let pathName = window.opener.location.pathname;
  let regex = /\/[a-z-]+\/([a-z0-9]{24,})\/.+/gi;
  let projectId = regex.exec(pathName)
  if (/\/simulation\/.+/gi.test(window.location.pathname)) {
    projectId = "configDemoHost";
  } else if (/\/[a-z-]+\/Neo4jDesktop\//gi.test(window.location.pathname)) {
    projectId = "Neo4jDesktop";
  } else if (/\/[a-z-]+\/default\//gi.test(window.location.pathname)) {
    projectId = "default";
  }
  return projectId[1];
};

Util.sendRequest = function sendRequest(
  apiURL,
  params = null,
  method = "POST"
) {
  let apiFinalURL = `${apiURL}`;
  let condition = {
    method: method,
    mode: "cors",
    credentials: "include",
  };

  if (params) {
    condition.body = JSON.stringify(params);
  }

  return fetch(new Request(Common.prefixAliasPath(apiFinalURL), condition))
    .then((res) => {
      if (res.status !== 200) {
        throw new Error(res.message || "Something went wrong on api server!");
      } else {
        return res.json();
      }
    })
    .then((resJSON) => {
      if (resJSON.status === 0) {
        return resJSON;
      } else {
        console.error("apiURL:", apiURL, resJSON.message);
        throw new Error(resJSON.message);
      }
    });
};

export default Util;
