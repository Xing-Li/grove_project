const Nodemailer = require('nodemailer')
const EJS = require('ejs')
const FS = require('fs')
const PATH = require('path')
const Util = require('./util')
const { ROOT_PATH ,TEMP_PATH } = require('../../Constant');

const EmailUtil = {
  // SettingConfigPath: PATH.join(ROOT_PATH, './setting.config.json'),
  // DefaultSettingConfigPath: PATH.join(ROOT_PATH, './setting.config.example.json'),
  sendEmail: function (toEmail, subject, content) {

    return new Promise((resolve, reject) => {
      if (!AppConfig.email || !AppConfig.email.stmp) {
        return reject(new Error("Miss stmp info, please email stmp to config.js"))
      }
      // Open a SMTP connection pool
      let transporter = Nodemailer.createTransport(AppConfig.email.stmp);

      // setting content
      let mailOptions = {
        from: AppConfig.email.form, // Email address
        to: toEmail, // Collection list
        subject: subject, //
        html: content //content
      };
      // send email
      transporter.sendMail(mailOptions, function (error, response) {
        if (error) {
          console.error(error);
          resolve(error)
        } else {
          resolve(response)
        }
        transporter.close(); // colse
      });

    })


    /*end send  email*/
  },
  sendEmailWithTemplate: function (toEmail, subject, templateName, contentData, req) {


    let templateEmailHtml = FS.readFileSync(PATH.join(ROOT_PATH, './views/emailTemplates/' + templateName), 'utf8');

    contentData.host = Util.getHost(req);
    contentData.toEmail = toEmail;

    let emailCotent = EJS.compile(templateEmailHtml)(contentData);


    return new Promise((resolve, reject) => {

      this.sendEmail(toEmail, subject, emailCotent).then(emailRes => {
        resolve(emailCotent)
      }).catch(err => {
        reject(err)
      })


    })



  },
  readJSONFile: function readJSONFile(JOSNPath, cb) {
    if (FS.existsSync(JOSNPath) && FS.accessSync(JOSNPath, FS.R_OK | FS.W_OK) == undefined) {
      FS.readFile(JOSNPath, "utf-8", function (err, JSONData) {
        if (JSONData) {
          try {
            JSONData = JSON.parse(JSONData);
          } catch (error) {
            err = error
          }
        }
        cb(err, JSONData)
      })
    } else {
      cb(new Error("Can't read the file :", JOSNPath));
    }
  },

  updateJSONFile: function updateJSONFile(JOSNPath, key, value, cb) {

    EmailUtil.readJSONFile(JOSNPath, function (err, JSONData) {

      let writeStream = FS.createWriteStream(JOSNPath);
      if (!JSONData) {
        JSONData = {};
      }
      JSONData[key] = value;
      writeStream.write(JSON.stringify(JSONData));
      writeStream.end(cb);
    })
  }

}

module.exports = EmailUtil;
