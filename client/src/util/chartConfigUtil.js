export const ShapeType = ["circle", "point", "square", "diamond", "hexagon", "triangle", "cross"]
export const fontAlignTypes = "left|center|right".split("|");
export const groupSeparators = ['_'];
export const positions = ["top-left", "top-center", "top-right",
    "right-top", "right-center", "right-bottom",
    "bottom-left", "bottom-center", "bottom-right",
    "left-top", "left-center", "left-bottom",]

const titleSizes = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']
const RENDERERS = ['canvas', 'svg'];
const HEAT_MAP_TYPES = ['polygon', 'density'];
const BubbleShapeType = "circle, square, bowtie, diamond, hexagon, triangle,triangle-down, hollow-circle, hollow-square, hollow-bowtie,hollow-diamond, hollow-hexagon, hollow-triangle, hollow-triangle-down, cross, tick, plus, hyphen, line".split(", ")
const HeatmapShapeType = "rect,square,circle".split(",");
const StepType = " | hv | vh | hvh | vhv".split(" | ");
const groupHeightModes = ['auto', 'fixed', 'fitItems'];
const tmItemTypes = ['box', 'point', 'range'];
const zoomKeys = ['', 'ctrlKey', 'altKey', 'shiftKey', 'metaKey'];
const legendPositions = ['top', 'top-left', 'top-right',
    'left', 'left-top', 'left-bottom',
    'right', 'right-top', 'right-bottom',
    'bottom', 'bottom-left', 'bottom-right']
const tickMethods = ['cat', 'time-cat', 'wilkinson-extended', 'r-pretty', 'time', 'time-pretty', 'log', 'pow', 'quantile', 'd3-linear']
const pieLableTypes = "inner|outer|spider".split("|");
const titleAligns = ['left', 'center', 'right']
const labelTypes = ["point", "line"]
const pointShapes = ["point", "line"]

export const ShowType = {
    Off: "Off",
    On: "On",
    Remove: "Remove",
    Stable: "Stable",
    Extract: "Extract",
    Select: "Select",
    Boolean: "Boolean",
    Text: "Text",
    JsText: "JsText",
    Number: "Number",
    Color: "Color",
    Padding: "Padding"
}
//////////////color util//////////////
function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

let multiColors = [{
    displayConfig: { type: ShowType.Color },
    value: "#5B8FF9"
},
{
    displayConfig: { type: ShowType.Color },
    value: "#5AD8A6"
},
{
    displayConfig: { type: ShowType.Color },
    value: "#5D7092"
},
{
    displayConfig: { type: ShowType.Color },
    value: "#F6BD16"
},
{
    displayConfig: { type: ShowType.Color },
    value: "#E8684A"
},]

export const getMultiColors = function (num) {
    for (let i = 0; i < num; i++) {
        if (!multiColors[i]) {
            let color = getRandomColor();
            if (_.filter(multiColors, (multiColor, index) => { return multiColor === color }).length > 0) {
                i--;
                continue;
            }
            multiColors[i] = {
                displayConfig: { type: ShowType.Color },
                value: color
            }
        }
    }
    return {
        displayConfig: {
            type: ShowType.Off, onValue: multiColors.slice(0, num)
        },
    }
}


export const loadMultiColors = function (num) {
    for (let i = 0; i < num; i++) {
        if (!multiColors[i]) {
            let color = getRandomColor();
            if (_.filter(multiColors, (multiColor, index) => { return multiColor === color }).length > 0) {
                i--;
                continue;
            }
            multiColors[i] = {
                displayConfig: { type: ShowType.Color },
                value: color
            }
        }
    }
    return multiColors
}

const DEFAULT_COLOR = {
    displayConfig: {
        type: ShowType.Off, onValue: {
            displayConfig: { type: ShowType.Color },
            value: "#5B8FF9"
        }
    },
}

const DEFAULT_MULTI_COLOR = getMultiColors(0)


const ROTATE_CONFIG = {
    displayConfig: { type: ShowType.Number },
    minV: 0.0,
    maxV: 10.0,
    step: 0.01,
    value: 0,
}

export const TitleConfig = {
    // align: {
    //     displayConfig: { type: ShowType.Select },
    //     selector: titleAligns,
    //     value: titleAligns[1],
    // },
    size: {
        displayConfig: { type: ShowType.Select },
        selector: titleSizes,
        value: titleSizes[5],
    }
}

const LableStyleConfig = {
    displayConfig: { type: ShowType.Off },
    fontSize: 10,
    textAlign: {
        displayConfig: { type: ShowType.Select },
        selector: fontAlignTypes,
        value: 'center'
    },
}

export const DefaultChartConfig = {
    // width: 560,
    // height: 376,
    title: {
        text: "line chart"
    },
    description: {
        text: "a simple line chart"
    },
    autoFit: true,
    renderer: {
        displayConfig: { type: ShowType.Select },
        selector: RENDERERS,
        value: RENDERERS[0]
    },
    smooth: false,
    lineStyle: {
        lineWidth: 2,//line size
    },
    label: {
        offset: 10,//entire offset
        offsetX: 0,
        offsetY: 0,
        style: _.cloneDeep(LableStyleConfig),
    },
    point: {
        size: 3,
        shape: 'circle'
    },
    tooltip: {
        showContent: true,
    },
    xAxis: {
        title: {
            text: "",
            autoRotate: true,
        },
        label: {
            offset: 10,//entire offset
            offsetX: 0,
            offsetY: 0,
            rotate: ROTATE_CONFIG,
            autoRotate: true,
            autoHide: false,
            style: _.cloneDeep(LableStyleConfig),
        },
        tickLine: {
            length: 5
        },
        grid: {
            displayConfig: { type: ShowType.Off, offValue: null },
            line: {
                style: {
                    lineWidth: 1,
                }
            }
        },
    },
    yAxis: {
        title: {
            text: "",
            autoRotate: true,
        },
        label: {
            offset: 10,//entire offset
            offsetX: 0,
            offsetY: 0,
            rotate: ROTATE_CONFIG,
            autoRotate: true,
            autoHide: false,
            style: _.cloneDeep(LableStyleConfig),
        },
        tickLine: {
            length: 5
        },
        grid: {
            displayConfig: { type: ShowType.On, offValue: null },
            line: {
                style: {
                    lineWidth: 1,
                }
            }
        },
    }
}

////////////notice that: configuration can only add root property can not alter old property's inner property, these may won't effect
export const timelineDefault = {
    startField: 'year',
    contentField: 'value',
    type: {
        displayConfig: { type: ShowType.Select },
        selector: tmItemTypes,
        value: tmItemTypes[1],
    },
    title: _.assign({
        text: "timeline chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a simple timeline chart"
    },
    maxHeight: 400,
    linkMain: true,
    stack: true,
    verticalScroll: true,
    timeSpacing: {
        displayConfig: { type: ShowType.Number },
        minV: -1,
        maxV: 100,
        step: 1,
        value: -1,
    },
    hideLabel: true,
    zoomKey: {
        displayConfig: { type: ShowType.Select },
        selector: zoomKeys,
        value: zoomKeys[1],
    },
}
export const rangeTimelineDefault = _.merge(_.cloneDeep(timelineDefault), {
    endField: "endTime",
    type: {
        displayConfig: { type: ShowType.Select },
        selector: tmItemTypes,
        value: 'range',
    },
    title: _.assign({
        text: "range timeline chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a simple range timeline chart"
    },
})
export const groupsTimelineDefault = {
    startField: 'year',
    groupField: 'groupBy',
    contentField: 'value',
    type: {
        displayConfig: { type: ShowType.Select },
        selector: tmItemTypes,
        value: 'point',
    },
    title: _.assign({
        text: "groups timeline chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a simple groups timeline chart"
    },
    maxHeight: 400,
    linkMain: true,
    groupHeightMode: {
        displayConfig: { type: ShowType.Select },
        selector: groupHeightModes,
        value: groupHeightModes[0],
    },
    stack: false,
    verticalScroll: true,
    timeSpacing: {
        displayConfig: { type: ShowType.Number },
        minV: -1,
        maxV: 100,
        step: 1,
        value: -1,
    },
    hideLabel: true,
    zoomKey: {
        displayConfig: { type: ShowType.Select },
        selector: zoomKeys,
        value: zoomKeys[1],
    },
}
export const groupsRangeTimelineDefault = _.merge(_.cloneDeep(groupsTimelineDefault), {
    endField: "endTime",
    type: {
        displayConfig: { type: ShowType.Select },
        selector: tmItemTypes,
        value: 'range',
    },
    title: _.assign({
        text: "groups range timeline chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a simple groups range timeline chart"
    },
})
export const subGroupsTimelineDefault = {
    startField: 'year',
    groupField: 'groupBy',
    contentField: 'value',
    type: {
        displayConfig: { type: ShowType.Select },
        selector: tmItemTypes,
        value: 'point',
    },
    title: _.assign({
        text: "subgroups timeline chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a simple subgroups timeline chart"
    },
    maxHeight: 400,
    linkMain: true,
    groupHeightMode: {
        displayConfig: { type: ShowType.Select },
        selector: groupHeightModes,
        value: groupHeightModes[0],
    },
    stack: false,
    verticalScroll: true,
    timeSpacing: {
        displayConfig: { type: ShowType.Number },
        minV: -1,
        maxV: 100,
        step: 1,
        value: -1,
    },
    hideLabel: true,
    zoomKey: {
        displayConfig: { type: ShowType.Select },
        selector: zoomKeys,
        value: zoomKeys[1],
    },
    groupSeparator: {
        displayConfig: { type: ShowType.Select },
        selector: groupSeparators,
        value: groupSeparators[0],
    },
}
export const subGroupsRangeTimelineDefault = _.merge(_.cloneDeep(subGroupsTimelineDefault), {
    endField: "endTime",
    type: {
        displayConfig: { type: ShowType.Select },
        selector: tmItemTypes,
        value: 'range',
    },
    title: _.assign({
        text: "subgroups range timeline chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a simple subgroups range timeline chart"
    },
})
/**
 * displayConfig: config the current parent object 
 */
export const lineDefault = {
    xField: 'year',
    yField: 'value',
    title: _.assign({
        text: "line chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a simple line chart"
    },
    padding: {
        displayConfig: { type: ShowType.Padding },
        value: 'auto',
    },
    autoFit: true,
    renderer: {
        displayConfig: { type: ShowType.Select },
        selector: RENDERERS,
        value: RENDERERS[0]
    },
    smooth: false,
    legend: {
        displayConfig: { type: ShowType.Remove, offValue: false },
        position: {
            displayConfig: { type: ShowType.Select },
            selector: legendPositions,
            value: 'top-left',
        },
        flipPage: true,
        offsetX: 0,
        offsetY: 0,
    },
    stepType: {
        displayConfig: { type: ShowType.Select },
        selector: StepType,
        value: '',
    },
    lineStyle: {
        displayConfig: { type: ShowType.On },
        lineWidth: 2,//line size
    },
    label: {
        displayConfig: { type: ShowType.Off },
        offset: 10,//entire offset
        offsetX: 0,
        offsetY: 0,
        style: _.cloneDeep(LableStyleConfig),
    },
    point: {
        displayConfig: { type: ShowType.Off },
        size: 5,
        shape: {
            displayConfig: { type: ShowType.Select },
            selector: ShapeType,
            value: 'circle',
        }
    },
    tooltip: {
        displayConfig: { type: ShowType.Off, offValue: { showContent: false, shared: false } },
        showContent: true,
        offset: 0,
    },
    xAxis: {
        title: {
            displayConfig: { type: ShowType.Off },
            text: "",
            autoRotate: true,
        },
        label: {
            offset: 10,
            offsetX: 0,
            offsetY: 0,
            rotate: ROTATE_CONFIG,
            autoRotate: false,
            autoHide: true,
            style: _.cloneDeep(LableStyleConfig),
        },
        tickLine: {
            displayConfig: { type: ShowType.Off },
            length: 5
        },
        grid: {
            displayConfig: { type: ShowType.Off, offValue: null },
            line: {
                style: {
                    lineWidth: 1,
                }
            }
        },
        min: {
            displayConfig: {
                type: ShowType.Off, offValue: false, onValue: 0
            }
        },
        tickInterval: {
            displayConfig: {
                type: ShowType.Off, offValue: false, onValue: 5
            }
        },
    },
    yAxis: {
        title: {
            displayConfig: { type: ShowType.Off },
            text: "",
            autoRotate: true,
        },
        label: {
            offset: 10,
            offsetX: 0,
            offsetY: 0,
            rotate: ROTATE_CONFIG,
            autoRotate: false,
            autoHide: true,
            style: _.cloneDeep(LableStyleConfig),
        },
        tickLine: {
            displayConfig: { type: ShowType.Off },
            length: 5
        },
        grid: {
            displayConfig: { type: ShowType.On, offValue: null },
            line: {
                style: {
                    lineWidth: 1,
                }
            }
        },
        min: {
            displayConfig: {
                type: ShowType.On, offValue: false, onValue: 0
            }
        },
        tickInterval: {
            displayConfig: {
                type: ShowType.Off, offValue: false, onValue: 5
            }
        },
        // tickMethod: {
        //     displayConfig: {
        //         type: ShowType.Off, offValue: false, onValue: {
        //             displayConfig: { type: ShowType.Select },
        //             selector: tickMethods,
        //             value: 'cat',
        //         }
        //     }
        // },
    },
    color: DEFAULT_COLOR
}

export const multiLineDefault = _.merge(_.cloneDeep(lineDefault), {
    title: {
        text: "multi line chart"
    },
    description: {
        text: "a simple multi line chart"
    },
    legend: {
        displayConfig: { type: ShowType.On },
    },
    tooltip: {
        shared: true,
    },
    color: DEFAULT_MULTI_COLOR
});

export const areaDefault = _.merge(_.cloneDeep(lineDefault), {
    title: _.assign({
        text: "area chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a simple area chart"
    }
});

export const multiAreaDefault = _.merge(_.cloneDeep(areaDefault), {
    title: {
        text: "multi area chart"
    },
    description: {
        text: "a simple multi area chart"
    },
    legend: {
        displayConfig: { type: ShowType.On },
    },
    isPercent: false,
    color: DEFAULT_MULTI_COLOR
});
const columnLabelPositions = "top' | 'bottom' | 'middle' | 'left' | 'right".split("' | '")
export const barDefault = {
    xField: 'year',
    yField: 'value',
    title: _.assign({
        text: "bar chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a simple bar chart"
    },
    padding: {
        displayConfig: { type: ShowType.Padding },
        value: 'auto',
    },
    autoFit: true,
    renderer: {
        displayConfig: { type: ShowType.Select },
        selector: RENDERERS,
        value: RENDERERS[0]
    },
    barWidthRatio: {
        displayConfig: { type: ShowType.Number },
        minV: 0.1,
        maxV: 1.0,
        step: 0.1,
        value: 0.5,
    },
    // marginRatio: {
    //     displayConfig: { type: ShowType.Remove },
    //     minV: 0.1,
    //     maxV: 1.0,
    //     step: 0.1,
    //     value: 0.5,
    // },
    legend: {
        displayConfig: { type: ShowType.Remove, offValue: false },
        position: {
            displayConfig: { type: ShowType.Select },
            selector: legendPositions,
            value: 'top-left',
        },
        flipPage: true,
        offsetX: 0,
        offsetY: 0,
    },
    label: {
        displayConfig: { type: ShowType.Off },
        position: {
            displayConfig: { type: ShowType.Select },
            selector: columnLabelPositions,
            value: 'right',
        },
        offset: 0,//entire offset
        offsetX: 0,
        offsetY: 0,
        style: _.cloneDeep(LableStyleConfig),
    },
    point: {
        displayConfig: { type: ShowType.Off },
        size: 5,
        shape: {
            displayConfig: { type: ShowType.Select },
            selector: ShapeType,
            value: 'circle',
        }
    },
    tooltip: {
        displayConfig: { type: ShowType.On, offValue: { showContent: false, shared: false } },
        showContent: true,
        offset: 0,
    },
    xAxis: {
        title: {
            displayConfig: { type: ShowType.Off },
            text: "",
            autoRotate: true,
        },
        label: {
            offset: 10,
            offsetX: 0,
            offsetY: 0,
            rotate: ROTATE_CONFIG,
            autoRotate: false,
            autoHide: true,
            style: _.cloneDeep(LableStyleConfig),
        },
        tickLine: {
            displayConfig: { type: ShowType.Off },
            length: 5
        },
        grid: {
            displayConfig: { type: ShowType.On, offValue: null },
            line: {
                style: {
                    lineWidth: 1,
                }
            }
        },
        min: {
            displayConfig: {
                type: ShowType.Off, offValue: false, onValue: 0
            }
        },
        tickInterval: {
            displayConfig: {
                type: ShowType.Off, offValue: false, onValue: 5
            }
        },
    },
    yAxis: {
        title: {
            displayConfig: { type: ShowType.Off },
            text: "",
            autoRotate: true,
        },
        label: {
            offset: 10,
            offsetX: 0,
            offsetY: 0,
            rotate: ROTATE_CONFIG,
            autoRotate: false,
            autoHide: true,
            style: _.cloneDeep(LableStyleConfig),
        },
        tickLine: {
            displayConfig: { type: ShowType.Off },
            length: 5
        },
        grid: {
            displayConfig: { type: ShowType.Off, offValue: null },
            line: {
                style: {
                    lineWidth: 1,
                }
            }
        },
        min: {
            displayConfig: {
                type: ShowType.On, offValue: false, onValue: 0
            }
        },
        tickInterval: {
            displayConfig: {
                type: ShowType.Off, offValue: false, onValue: 5
            }
        },
    },
    color: DEFAULT_COLOR
}

export const multiBarDefault = _.merge(_.cloneDeep(barDefault), {
    title: {
        text: "multi bar chart"
    },
    description: {
        text: "a simple multi bar chart"
    },
    // marginRatio: {
    //     displayConfig: { type: ShowType.Off },
    // },
    legend: {
        displayConfig: { type: ShowType.Off },
    },
    isStack: false,
    isGroup: true,
    isRange: false,
    isPercent: false,
    color: DEFAULT_MULTI_COLOR
});
export const columnDefault = {
    // width: 560,
    // height: 376,
    xField: 'year',
    yField: 'value',
    title: _.assign({
        text: "column chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a simple column chart"
    },
    padding: {
        displayConfig: { type: ShowType.Padding },
        value: 'auto',
    },
    autoFit: true,
    renderer: {
        displayConfig: { type: ShowType.Select },
        selector: RENDERERS,
        value: RENDERERS[0]
    },
    columnWidthRatio: {
        displayConfig: { type: ShowType.Number },
        minV: 0.1,
        maxV: 1.0,
        step: 0.1,
        value: 0.5,
    },
    // marginRatio: {
    //     displayConfig: { type: ShowType.Remove },
    //     minV: 0.1,
    //     maxV: 1.0,
    //     step: 0.1,
    //     value: 0.5,
    // },
    legend: {
        displayConfig: { type: ShowType.Remove, offValue: false },
        position: {
            displayConfig: { type: ShowType.Select },
            selector: legendPositions,
            value: 'top-left',
        },
        flipPage: true,
        offsetX: 0,
        offsetY: 0,
    },
    label: {
        displayConfig: { type: ShowType.On },
        position: {
            displayConfig: { type: ShowType.Select },
            selector: columnLabelPositions,
            value: 'top',
        },
        offset: 0,//entire offset
        offsetX: 0,
        offsetY: 0,
        style: _.cloneDeep(LableStyleConfig),
    },
    // point: {
    //     displayConfig: { type: ShowType.Off },
    //     size: 5,
    //     shape: {
    //         displayConfig: { type: ShowType.Select },
    //         selector: ShapeType,
    //         value: 'circle',
    //     }
    // },
    tooltip: {
        displayConfig: { type: ShowType.Off, offValue: { showContent: false, shared: false } },
        showContent: true,
        offset: 0,
    },
    xAxis: {
        title: {
            displayConfig: { type: ShowType.Off },
            text: "",
            autoRotate: true,
        },
        label: {
            offset: 10,
            offsetX: 0,
            offsetY: 0,
            rotate: ROTATE_CONFIG,
            autoRotate: false,
            autoHide: true,
            style: _.cloneDeep(LableStyleConfig),
        },
        tickLine: {
            displayConfig: { type: ShowType.Off },
            length: 5
        },
        grid: {
            displayConfig: { type: ShowType.Off, offValue: null },
            line: {
                style: {
                    lineWidth: 1,
                }
            }
        },
        min: {
            displayConfig: {
                type: ShowType.Off, offValue: false, onValue: 0
            }
        },
        tickInterval: {
            displayConfig: {
                type: ShowType.Off, offValue: false, onValue: 5
            }
        },
    },
    yAxis: {
        title: {
            displayConfig: { type: ShowType.Off },
            text: "",
            autoRotate: true,
        },
        label: {
            offset: 10,
            offsetX: 0,
            offsetY: 0,
            rotate: ROTATE_CONFIG,
            autoRotate: false,
            autoHide: true,
            style: _.cloneDeep(LableStyleConfig),
        },
        tickLine: {
            displayConfig: { type: ShowType.Off },
            length: 5
        },
        grid: {
            displayConfig: { type: ShowType.On, offValue: null },
            line: {
                style: {
                    lineWidth: 1,
                }
            }
        },
        min: {
            displayConfig: {
                type: ShowType.On, offValue: false, onValue: 0
            }
        },
        tickInterval: {
            displayConfig: {
                type: ShowType.Off, offValue: false, onValue: 5
            }
        },
    },
    color: DEFAULT_COLOR
};

export const multiColumnDefault = _.merge(_.cloneDeep(columnDefault), {
    title: {
        text: "multi column chart"
    },
    description: {
        text: "a simple multi column chart"
    },
    // marginRatio: {
    //     displayConfig: { type: ShowType.Off },
    // },
    legend: {
        displayConfig: { type: ShowType.On },
    },
    isStack: false,
    isGroup: true,
    isRange: false,
    isPercent: false,
    color: DEFAULT_MULTI_COLOR
});

export const waterfallDefault = {
    // width: 560,
    // height: 376,
    xField: 'year',
    yField: 'value',
    title: _.assign({
        text: "waterfall chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a simple waterfall chart"
    },
    padding: {
        displayConfig: { type: ShowType.Padding },
        value: 'auto',
    },
    autoFit: true,
    renderer: {
        displayConfig: { type: ShowType.Select },
        selector: RENDERERS,
        value: RENDERERS[0]
    },
    risingFill: {
        displayConfig: { type: ShowType.Color },
        value: "#f4664a"
    },
    fallingFill: {
        displayConfig: { type: ShowType.Color },
        value: "#30bf78"
    },
    total: {
        displayConfig: { type: ShowType.On, offValue: false },
        style: {
            label: "Total",
            fill: {
                displayConfig: { type: ShowType.Color },
                value: "#AAA"
            }
        }
    },
    label: {
        displayConfig: { type: ShowType.On },
        position: {
            displayConfig: { type: ShowType.Select },
            selector: columnLabelPositions,
            value: 'top',
        },
        offset: 0,//entire offset
        offsetX: 0,
        offsetY: 0,
        style: _.cloneDeep(LableStyleConfig),
    },
    leaderLine: {
        displayConfig: { type: ShowType.Off, offValue: false },
        style: {
            stroke: {
                displayConfig: { type: ShowType.Color },
                value: "#8c8c8c"
            },
            lineWidth: 1,
            lineDash: {
                displayConfig: {
                    type: ShowType.Off, onValue: [4, 2]
                },
            },
        }
    },
    tooltip: {
        displayConfig: { type: ShowType.Off, offValue: { showContent: false, shared: false } },
        showContent: true,
        offset: 0,
    },
    xAxis: {
        title: {
            displayConfig: { type: ShowType.Off },
            text: "",
            autoRotate: true,
        },
        label: {
            offset: 10,
            offsetX: 0,
            offsetY: 0,
            rotate: ROTATE_CONFIG,
            autoRotate: false,
            autoHide: true,
            style: _.cloneDeep(LableStyleConfig),
        },
        tickLine: {
            displayConfig: { type: ShowType.Off },
            length: 5
        },
        grid: {
            displayConfig: { type: ShowType.Off, offValue: null },
            line: {
                style: {
                    lineWidth: 1,
                }
            }
        },
        min: {
            displayConfig: {
                type: ShowType.Off, offValue: false, onValue: 0
            }
        },
        tickInterval: {
            displayConfig: {
                type: ShowType.Off, offValue: false, onValue: 5
            }
        },
    },
    yAxis: {
        title: {
            displayConfig: { type: ShowType.Off },
            text: "",
            autoRotate: true,
        },
        label: {
            offset: 10,
            offsetX: 0,
            offsetY: 0,
            rotate: ROTATE_CONFIG,
            autoRotate: false,
            autoHide: true,
            style: _.cloneDeep(LableStyleConfig),
        },
        tickLine: {
            displayConfig: { type: ShowType.Off },
            length: 5
        },
        grid: {
            displayConfig: { type: ShowType.On, offValue: null },
            line: {
                style: {
                    lineWidth: 1,
                }
            }
        },
        min: {
            displayConfig: {
                type: ShowType.On, offValue: false, onValue: 0
            }
        },
        tickInterval: {
            displayConfig: {
                type: ShowType.Off, offValue: false, onValue: 5
            }
        },
    }
};

export const liquidDefault = {
    percent: 0.25,
    title: _.assign({
        text: "liquid chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a simple liquid chart"
    },
    padding: {
        displayConfig: { type: ShowType.Padding },
        value: 'auto',
    },
    autoFit: true,
    renderer: {
        displayConfig: { type: ShowType.Select },
        selector: RENDERERS,
        value: RENDERERS[0]
    },
    outline: {
        border: {
            displayConfig: { type: ShowType.Number },
            minV: 0,
            maxV: 100,
            step: 1,
            value: 4,
        },
        distance: {
            displayConfig: { type: ShowType.Number },
            minV: 0,
            maxV: 100,
            step: 1,
            value: 8,
        },
    },
    wave: {
        length: {
            displayConfig: { type: ShowType.Number },
            minV: 1,
            maxV: 1000,
            step: 1,
            value: 128,
        },
    },
};

export const pieDefault = {
    // width: 560,
    // height: 376,
    appendPadding: 0,
    angleField: 'value',
    colorField: 'type',
    title: _.assign({
        text: "pie chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a simple pie chart"
    },
    autoFit: true,
    renderer: {
        displayConfig: { type: ShowType.Select },
        selector: RENDERERS,
        value: RENDERERS[0]
    },
    radius: {
        displayConfig: { type: ShowType.Number },
        minV: 0.1,
        maxV: 1.0,
        step: 0.1,
        value: 1.0,
    },
    innerRadius: {
        displayConfig: {
            type: ShowType.Off, onValue: {
                displayConfig: { type: ShowType.Number },
                minV: 0.1,
                maxV: 1.0,
                step: 0.1,
                value: 0.3,
            }
        }
    },
    startAngle: {
        displayConfig: {
            type: ShowType.Off, onValue: {
                displayConfig: { type: ShowType.Number },
                minV: 0.00,
                maxV: Math.PI * 2,
                step: 0.01,
                value: 0.0,
            }
        }
    },
    endAngle: {
        displayConfig: {
            type: ShowType.Off, onValue: {
                displayConfig: { type: ShowType.Number },
                minV: 0.00,
                maxV: Math.PI * 2,
                step: 0.01,
                value: Math.PI * 1,
            }
        }
    },
    label: {
        type: {
            displayConfig: { type: ShowType.Select },
            selector: pieLableTypes,
            value: 'inner'
        },
        content: { displayConfig: { type: ShowType.Off, onValue: '{name}\t{percentage}' } },
        style: {
            fontSize: 14,
            textAlign: {
                displayConfig: { type: ShowType.Select },
                selector: fontAlignTypes,
                value: 'center'
            },
            style: _.cloneDeep(LableStyleConfig),
        },
    },
    legend: {
        displayConfig: { type: ShowType.On, offValue: false },
        layout: {
            displayConfig: { type: ShowType.Select },
            selector: ['horizontal', 'vertical'],
            value: undefined,
            props: { allowClear: true },
        },
        position: {
            displayConfig: { type: ShowType.Select },
            selector: ['top', 'top-left', 'top-right', 'left', 'left-top', 'left-bottom', 'right', 'right-top', 'right-bottom', 'bottom', 'bottom-left', 'bottom-right'],
            value: 'right',
            props: { allowClear: true },
        },
    },
    color: DEFAULT_MULTI_COLOR,
    interactions: [{ type: 'element-active' }],
}

export const roseDefault = {
    // width: 560,
    // height: 376,
    xField: 'year',
    yField: 'value',
    title: _.assign({
        text: "rose chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a simple rose chart"
    },
    radius: {
        displayConfig: { type: ShowType.Number },
        minV: 0.1,
        maxV: 1.0,
        step: 0.1,
        value: 1.0,
    },
    autoFit: true,
    padding: {
        displayConfig: { type: ShowType.Padding },
        value: 'auto',
    },
    renderer: {
        displayConfig: { type: ShowType.Select },
        selector: RENDERERS,
        value: 'canvas'
    },
    tooltip: {
        displayConfig: { type: ShowType.Off },
        showContent: true,
    },
    color: DEFAULT_COLOR
}

export const multiRoseDefault = _.merge(_.cloneDeep(roseDefault), {
    title: {
        text: "multi rose chart"
    },
    description: {
        text: "a simple multi rose chart"
    },
    legend: {
        displayConfig: { type: ShowType.On },
    },
    isGroup: true,
    isStack: false,
    color: DEFAULT_MULTI_COLOR
});

export const scatterDefault = {
    // width: 560,
    // height: 376,
    xField: 'year',
    yField: 'value',
    title: _.assign({
        text: "scatter chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a simple scatter chart"
    },
    autoFit: true,
    smooth: false,
    label: {
        displayConfig: { type: ShowType.Off },
        offset: 10,//entire offset
        offsetX: 0,
        offsetY: 0,
        style: _.cloneDeep(LableStyleConfig),
    },
    // point: {
    //     displayConfig: { type: ShowType.Off },
    //     size: 3,
    //     shape: {
    //         displayConfig: { type: ShowType.Select },
    //         selector: BubbleShapeType,
    //         value: 'circle'
    //     }
    // },
    size: 3,
    shape: {
        displayConfig: { type: ShowType.Select },
        selector: BubbleShapeType,
        value: 'circle'
    },
    tooltip: {
        displayConfig: { type: ShowType.Off },
        showContent: true,
    },
    xAxis: {
        title: {
            text: "",
            autoRotate: true,
        },
        label: {
            offset: 10,
            offsetX: 0,
            offsetY: 0,
            rotate: ROTATE_CONFIG,
            autoRotate: false,
            autoHide: true,
            style: _.cloneDeep(LableStyleConfig),
        },
        tickLine: {
            displayConfig: { type: ShowType.Off },
            length: 5
        },
        grid: {
            displayConfig: { type: ShowType.Off, offValue: null },
            line: {
                style: {
                    lineWidth: 1,
                }
            }
        },
        min: {
            displayConfig: {
                type: ShowType.Off, offValue: false, onValue: 0
            }
        },
        tickInterval: {
            displayConfig: {
                type: ShowType.Off, offValue: false, onValue: 5
            }
        },
    },
    yAxis: {
        title: {
            text: "",
            autoRotate: true,
        },
        label: {
            offset: 10,
            offsetX: 0,
            offsetY: 0,
            rotate: ROTATE_CONFIG,
            autoRotate: false,
            autoHide: true,
            style: _.cloneDeep(LableStyleConfig),
        },
        tickLine: {
            displayConfig: { type: ShowType.Off },
            length: 5
        },
        grid: {
            displayConfig: { type: ShowType.On, offValue: null },
            line: {
                style: {
                    lineWidth: 1,
                }
            }
        },
        min: {
            displayConfig: {
                type: ShowType.On, offValue: false, onValue: 0
            }
        },
        tickInterval: {
            displayConfig: {
                type: ShowType.Off, offValue: false, onValue: 5
            }
        },
    },
    regressionLine: {
        displayConfig: { type: ShowType.Off },
        type: {
            displayConfig: { type: ShowType.Select },
            selector: "linear, exp, loess, log, poly, pow, quad".split(", "),
            value: 'linear'
        },
        style: {
            displayConfig: { type: ShowType.Off },
            fill: {
                displayConfig: {
                    type: ShowType.Off, onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "red"
                    }
                }
            },
            fillOpacity: {
                displayConfig: {
                    type: ShowType.Off, onValue: {
                        displayConfig: { type: ShowType.Number },
                        minV: 0.0,
                        maxV: 1.0,
                        step: 0.1,
                        value: 0.5,
                    }
                }
            },
            stroke: {
                displayConfig: { type: ShowType.Color },
                value: "black"
            },
            lineWidth: 1,
            lineDash: {
                displayConfig: {
                    type: ShowType.Off, onValue: [4, 5]
                },
            },
            strokeOpacity: {
                displayConfig: { type: ShowType.Number },
                minV: 0.0,
                maxV: 1.0,
                step: 0.1,
                value: 0.7,
            },
            shadowColor: {
                displayConfig: {
                    type: ShowType.Off, onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "black"
                    }
                }
            },
            shadowBlur: {
                displayConfig: {
                    type: ShowType.Off, onValue: 10
                }
            },
            shadowOffsetX: {
                displayConfig: {
                    type: ShowType.Off, onValue: 5
                }
            },
            shadowOffsetY: {
                displayConfig: {
                    type: ShowType.Off, onValue: 5
                }
            },
            // cursor: 'pointer'
        }
    },
    size: 4,
}
export const bubbleDefault = _.merge(_.cloneDeep(scatterDefault), {
    title: _.assign({
        text: "bubble chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a simple bubble chart"
    },
    size: [12, 30],
    shape: {
        displayConfig: { type: ShowType.Select },
        selector: BubbleShapeType,
        value: 'circle'
    },
});

export const multiBubbleDefault = _.merge(_.cloneDeep(bubbleDefault), {
    title: {
        text: "multi bubble chart"
    },
    description: {
        text: "a simple multi bubble chart"
    },
    legend: {
        displayConfig: { type: ShowType.On },
    },
    tooltip: {
        shared: true,
    },
});

export const wordCloudDefault = {
    // width: 560,
    // height: 376,
    wordField: 'year',
    weightField: 'value',
    colorField: undefined,
    title: _.assign({
        text: "word cloud chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a simple word cloud chart"
    },
    autoFit: true,
    padding: {
        displayConfig: { type: ShowType.Padding },
        value: 'auto',
    },
    renderer: {
        displayConfig: { type: ShowType.Select },
        selector: RENDERERS,
        value: RENDERERS[0]
    },
    wordStyle: {
        fontFamily: 'Verdana',
        fontSize: [8, 32],
    },
    // 设置交互类型
    interactions: [{ type: 'element-active' }],
    state: {
        active: {
            // 这里可以设置 active 时的样式
            style: {
                lineWidth: 3,
            },
        },
    },
    imageMask: {
        displayConfig: {
            type: ShowType.Off, onValue: 'https://gw.alipayobjects.com/mdn/rms_2274c3/afts/img/A*07tdTIOmvlYAAAAAAAAAAABkARQnAQ'
        },
    }
    // color: {
    //     displayConfig: {
    //         type: ShowType.Off, onValue: [{
    //             displayConfig: { type: ShowType.Color },
    //             value: "#d62728"
    //         },
    //         {
    //             displayConfig: { type: ShowType.Color },
    //             value: "#2ca02c"
    //         },
    //         {
    //             displayConfig: { type: ShowType.Color },
    //             value: "#000000"
    //         }]
    //     },
    // },
}

export const continuousHeatmapDefault = {
    // width: 560,
    // height: 376,
    xField: 'year',
    yField: 'value',
    colorField: undefined,
    sizeField: undefined,
    title: _.assign({
        text: "continuous heatmap chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a continuous heatmap chart"
    },
    legend: {
        displayConfig: { type: ShowType.Off, offValue: false },
        position: {
            displayConfig: { type: ShowType.Select },
            selector: legendPositions,
            value: 'top-left',
        },
        offsetX: 0,
        offsetY: 0,
    },
    autoFit: true,
    // padding: 0,
    renderer: {
        displayConfig: { type: ShowType.Select },
        selector: RENDERERS,
        value: RENDERERS[0]
    },
    type: {
        displayConfig: { type: ShowType.Select },
        selector: HEAT_MAP_TYPES,
        value: HEAT_MAP_TYPES[1]
    },
    color: {
        displayConfig: {
            type: ShowType.Off, onValue: [{
                displayConfig: { type: ShowType.Color },
                value: "#d62728"
            },
            {
                displayConfig: { type: ShowType.Color },
                value: "#2ca02c"
            },
            {
                displayConfig: { type: ShowType.Color },
                value: "#000000"
            }]
        },
    },
    xAxis: {
        title: {
            text: "",
            autoRotate: true,
        },
        label: {
            offset: 10,//entire offset
            offsetX: 0,
            offsetY: 0,
            rotate: ROTATE_CONFIG,
            autoRotate: true,
            autoHide: false,
            style: _.cloneDeep(LableStyleConfig),
        },
        tickLine: {
            length: 5
        },
        grid: {
            displayConfig: { type: ShowType.Off, offValue: null },
            line: {
                style: {
                    lineWidth: 1,
                }
            }
        },
        min: {
            displayConfig: {
                type: ShowType.Off, offValue: false, onValue: 0
            }
        },
        tickInterval: {
            displayConfig: {
                type: ShowType.Off, offValue: false, onValue: 5
            }
        },
    },
    yAxis: {
        title: {
            text: "",
            autoRotate: true,
        },
        label: {
            offset: 10,//entire offset
            offsetX: 0,
            offsetY: 0,
            rotate: ROTATE_CONFIG,
            autoRotate: true,
            autoHide: false,
            style: _.cloneDeep(LableStyleConfig),
        },
        tickLine: {
            length: 5
        },
        grid: {
            displayConfig: { type: ShowType.On, offValue: null },
            line: {
                style: {
                    lineWidth: 1,
                }
            }
        },
        min: {
            displayConfig: {
                type: ShowType.On, offValue: false, onValue: 0
            }
        },
        tickInterval: {
            displayConfig: {
                type: ShowType.Off, offValue: false, onValue: 5
            }
        },
    }
}
export const heatmapDefault = _.merge(_.cloneDeep(continuousHeatmapDefault), {
    title: _.assign({
        text: "heatmap chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a heatmap chart"
    },
    label: {
        displayConfig: { type: ShowType.Off },
        offset: 10,//entire offset
        offsetX: 0,
        offsetY: 0,
        style: _.cloneDeep(LableStyleConfig),
    },
    type: {
        displayConfig: { type: ShowType.Select },
        selector: HEAT_MAP_TYPES,
        value: HEAT_MAP_TYPES[0]
    },
    shape: {
        displayConfig: { type: ShowType.Select },
        selector: HeatmapShapeType,
        value: HeatmapShapeType[0]
    },
});
export const unevenHeatmapDefault = _.merge(_.cloneDeep(continuousHeatmapDefault), {
    title: _.assign({
        text: "uneven heatmap chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a uneven heatmap chart"
    },
    label: {
        displayConfig: { type: ShowType.Off },
        offset: 10,//entire offset
        offsetX: 0,
        offsetY: 0,
        style: _.cloneDeep(LableStyleConfig),
    },
    type: {
        displayConfig: { type: ShowType.Select },
        selector: HEAT_MAP_TYPES,
        value: HEAT_MAP_TYPES[0]
    },
    shape: {
        displayConfig: { type: ShowType.Select },
        selector: HeatmapShapeType,
        value: HeatmapShapeType[0]
    },
});

export const radarDefault = {
    // width: 560,
    // height: 376,
    xField: 'year',
    yField: 'value',
    title: _.assign({
        text: "radar chart"
    }, _.cloneDeep(TitleConfig)),
    description: {
        text: "a simple radar chart"
    },
    radius: {
        displayConfig: { type: ShowType.Number },
        minV: 0.1,
        maxV: 1.0,
        step: 0.1,
        value: 1.0,
    },
    autoFit: true,
    smooth: false,
    area: {
        displayConfig: { type: ShowType.On },
    },
    point: {
        displayConfig: { type: ShowType.Off },
        size: 3,
        shape: {
            displayConfig: { type: ShowType.Select },
            selector: ShapeType,
            value: 'circle'
        }
    },
    tooltip: {
        displayConfig: { type: ShowType.Off },
        showContent: true,
    },
    xAxis: {
        tickLine: {
            displayConfig: { type: ShowType.Off },
            length: 5
        },
        label: {
            offset: 10,//entire offset
            offsetX: 0,
            offsetY: 0,
            style: _.cloneDeep(LableStyleConfig),
        },
    },
    yAxis: {
        title: {
            text: "",
            autoRotate: true,
        },
        tickLine: {
            displayConfig: { type: ShowType.Off },
            length: 5
        },
        label: {
            offset: 10,//entire offset
            offsetX: 0,
            offsetY: 0,
            style: _.cloneDeep(LableStyleConfig),
        },
        min: {
            displayConfig: {
                type: ShowType.On, offValue: false, onValue: 0
            }
        },
        tickInterval: {
            displayConfig: {
                type: ShowType.Off, offValue: false, onValue: 5
            }
        },
    },
    color: DEFAULT_COLOR
}

export const multiRadarDefault = _.merge(_.cloneDeep(radarDefault), {
    title: {
        text: "multi radar chart"
    },
    description: {
        text: "a simple multi radar chart"
    },
    legend: {
        displayConfig: { type: ShowType.On },
    },
    tooltip: {
        shared: true,
    },
    color: DEFAULT_MULTI_COLOR
});

