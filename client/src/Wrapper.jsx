import { CaretDownOutlined, EditOutlined, EllipsisOutlined, UpCircleOutlined, UserOutlined, CloseOutlined } from '@ant-design/icons';
import isGraphXrDarkMode from './util/isGraphXrDarkMode';
import {
  Affix,
  Alert, Avatar, BackTop,
  Button, Card, Drawer, Dropdown,
  Modal, Menu, Tag,
  Select, Spin, Tooltip
} from 'antd';
import axios from 'axios';
import _, { debounce } from 'lodash';
import PropTypes from "prop-types";
import React from 'react';
import Draggable from 'react-draggable';
import { TweenOneGroup } from 'rc-tween-one';
// import './fonts/simhei-bold';
import { Link } from 'stdlib/link';
import BottomTools from './BottomTools';
import AntChartEditor from './block/antchart/AntChartEditor';
import AntChartSetting from './block/antchart/AntChartSetting';
import { DefaultAntChartConfig } from './block/antchart/comp/OptionsTypes';
import commonData from './common/CommonData';
import editor from './commonEditor';
import ToolsSelect from './ToolsSelect';
import { DrawerType, InitSettings, verifyW } from './editorSettings';
import Chat from './components/Chat';
import actions from './define/Actions';
import { fileSeparator, getContent, getFileKey, getFileName, getFileType, verFileTag, wrapContent } from './file/fileUtils';
import MdFiles, { importFilesAction } from './file/MdFiles';
import MyDropzone from './file/MyDropzone';
import FileAttachments from './FileAttachments';
import ImportModules from './ImportModules';
import Help from './Help';
import FindAndReplace from './FindAndReplace';
import Panels from './Panels';
import Settings from './Settings';
import TagEditor, { ShapeTagEditorInfo } from "./tagEditor";
import { DefaultInitData } from 'block/data';
import { disposeModule, removeModule } from 'util/hqUtils';
import { addAllGlobalScope } from 'util/hintUtil';
import EditorAbs from 'editorAbs';
import { GraphxrApiSearchModal } from './components/graphxrApiSearchModal/GraphxrApiSearchModal';
import * as graphxrApi from './stdlib/graphxrApi';
import { SafeModeIcon } from './block/code/Icon';
import DrawerEx from 'components/DrawerEx';

const { Msg_UserStatus, UserStatus } = require("./util/MsgType");
const { showToast, saveLink, SelectPropsDef, IsKey, copyContent,
  window_shortcuts_keys, file_actions_keys, code_editor_keys, cell_shortcuts_keys, ShortcutTitle, ContentType, MSG, ModalType } = require("./util/utils");
const { UNKOWN, codeTime, formatSizeUnits, checkIsMobile, isFileImage, getFileIcon,
  SelectType, isFileEditorJs, ExcpetImgAccept, AllAccept, DEBUG_EDITOR_JS, DEBUG_DRAG } = require("./util/helpers");
const { setWindowState, DefaultShareData, DefaultFileData, ShapeFileChange, ShapeLinkChange,
  getFileNamesKey, getItem, DEFAULT_VER, setItem, getFileNamesJson, TAG_TYPE,
  DefaultOptions, getSkey, setSkey, isNotLogin, ShapeChatInfo, SYSTEM_USER,
  ShapeChatData, USER_ID, USER_EMAIL, refreshItems, SYSTEM_USER_URI, PROJECT_ID,
  getUploadUri, COMMON, getMasterUserId, noSharedProjectIds, noSharedProjectName, USER_NAME, FILESDIR, GUIDE_LINK_URI, getUndefinedContent, getFormatUndefinedContent, getTaginfosKey,
} = require("./util/localstorage");

const { Meta } = Card;
const { Option } = Select;
const Def = DrawerType.None;
const colors = ['bg-success text-white', 'bg-danger text-white', "bg-warning text-dark",
  'bg-info text-white', 'bg-white text-dark'];
const MAX_COUNT = 3;
const MAX_STYLE = { color: '#f56a00', backgroundColor: '#fde3cf' };
const usersCompareFunc = /**
   * @param {ShapeChatData} u1 
   * @param {ShapeChatData} u2  */ (u1, u2) => { return u2.userStatus - u1.userStatus }
const Grays = [UserStatus.offline, UserStatus.kickout];
const GrayColors = ["bg-secondary text-white", "bg-dark text-white"];
const TAG_SELECTED = '#108ee9';
const TAG_CLOSED = 'default';
const MAX_SCALE = 100;
/**@type {import('html2canvas').default} */
const html2canvas = window.html2canvas;

export default class Wrapper extends React.Component {
  static propTypes = {
    className: PropTypes.string,
  };

  static defaultProps = {};
  constructor(props) {
    super(props);
    /** @type [DefaultShareData] */
    this.infos = [];
    this.infosIndex = -1;
    refreshItems();
    let query = new URLSearchParams(
      window.location.href.substring(window.location.href.indexOf("?") + 1)
    );
    let defaultFileKey = query.get("fileKey") || "";
    let defaultUserId = query.get("userId") || undefined;
    let defaultProjectId = query.get("projectId") || undefined;
    let defaultUploadUri = query.get("uploadUri") || ((defaultUserId && defaultProjectId) ? getUploadUri(defaultProjectId, defaultUserId) : undefined) || undefined;
    /** @type DefaultShareData */
    let info = {
      userId: defaultUserId,
      userName: query.get("userName") || undefined,
      email: query.get("email") || undefined,
      tag: query.get("tag") || undefined,
      projectId: defaultProjectId,
      projectName: query.get("projectName") || noSharedProjectName(defaultProjectId) || undefined,
      fileKey: defaultFileKey,
      version: parseInt(query.get("version") || "0"),
      status: query.get("status") || undefined,
      sharedId: query.get("sharedId") || undefined,
      uploadUri: defaultUploadUri,
    };
    let options = _.cloneDeep(DefaultOptions());
    let cacheInfo = undefined;
    if (!info.userId || !info.projectId) {
      info = options[0];
    } else if (info.userId === USER_ID) {
      let arr = _.filter(options, (value) => { return value.projectId === info.projectId });
      arr.forEach(item => {
        item.userId = defaultUserId;
      })
      if (arr.length === 0) {
        options.push(info);
      } else {
        _.assign(arr[0], info);
        info = arr[0];
      }
    } else {//link
      if (isNotLogin()) {//May be a share page,need not login.
        let currentProjectFolder = options[0];
        if (info.projectId === currentProjectFolder.projectId && info.userId === currentProjectFolder.userId) {
          info = currentProjectFolder;
          if (defaultFileKey) {
            info['fileKey'] = defaultFileKey;
          }
          if (defaultUploadUri) {
            info['uploadUri'] = defaultUploadUri;
          }
        }
      } else {
        cacheInfo = info.fileKey ? info : undefined;
        info = options[0];
      }
    }

    info && this.recordHistory(info, {});

    const { userId, userName, email, projectId, projectName, fileKey } = this.infos[0] || {};
    setSkey(projectId, userId);

    const queryReadonly = query.get("readonly")
    if (queryReadonly) {
      commonData.setReadOnlyMode(queryReadonly === "true")
    }

    const graphxrApiVersion = graphxrApi.getGraphxrApiVersion();
    if (!graphxrApiVersion) {
      graphxrApi.executeOnLoad(() => {
        this.setState({
          graphxrApiVersion: graphxrApi.getGraphxrApiVersion()
        })
      })
    }
    this.state = {
      ready: false,
      mask: true,
      file_version_selected: DEFAULT_VER,
      mdfiles_transfer: {
        help: false,
        settings: null,
        fileNamesJson: getFileNamesJson(),
        cloud: {},
        uploadUri: null,
        // setLoading: (loading, cb) => {
        //   this.setState({ loading }, cb);
        // },
        handleSearch: this.handleSearch,
        switchFileTagEditor: this.switchFileTagEditor,
        switchLinkTagEditor: this.switchLinkTagEditor,
        loadTaginfos: this.loadTaginfos,
        closeTagEditor: this.closeTagEditor,
        recordHistory: this.recordHistory,
        cacheInfo: cacheInfo,
      },
      drawObj: Def,
      drawRefreshs: 0,
      initWidth: verifyW(Def.w),
      /**@type DefaultFileData */
      fileData: null,
      /**@type {Object.<string,Set>} */
      filesAttaches: {},
      /**@type DefaultShareData */
      linkData: null,
      toggleOn: false,
      options,
      initData: false,
      loading: true,
      loadingClosable: false,
      userId: userId || USER_ID,
      userName,
      email,
      projectId,
      projectName,
      fileKey: fileKey || "",
      /**@type ShapeChatInfo file shared to others*/
      shared: undefined,
      infos: this.infos,
      infosIndex: this.infosIndex,
      needUpload: false,
      /**@type {Object.<string,boolean>} */
      needSaveFiles: {},
      resize: { widthScale: 0, heightScale: 0 },
      /**@type Object.<string, ShapeChatData> */
      users: {},
      safeMode: commonData.getSafeMode(),
      readOnlyMode: commonData.getReadOnlyMode(),
      editCompDrop: false,
      fullScreen: query.get("fullscreen") === "true",
      dFullScreen: false,
      drawerWidth: 100,
      /**@type Object.<string, ShapeChatData> */
      projectShareUsers: {},
      editorReadOnly: true,
      /**@type Array.<TagEditor> */
      tagEditors: window.tagEditors,
      /**@type {TagEditor} */
      tagedEditor: undefined,
      position: {
        top: 0,
        left: 0,
      },
      cb: undefined,
      /**props need transport to Drawer */
      dprops: {},
      isGraphxrApiSearchVisible: false,
      graphxrApiVersion,
      graphxrApiSearchBlockIndex: undefined,
    };
    this.fileInputRef = React.createRef();
    this.mdfilesRef = React.createRef();
    this.graphApiElement = document.createElement("div");
    this.fileChangeElement = document.createElement("div");
    this.linkChangeElement = document.createElement("div");
    this.maskElement = document.createElement("div");
    this.readyElement = document.createElement("div");
    this.needSaveElement = document.createElement("div");
    this.resizeElement = document.createElement("div");
    this.usersElement = document.createElement("div");
    this.loadingElement = document.createElement("div");
    this.attachesElement = document.createElement("div");
    this.safeModeElement = document.createElement("div");
    this.readOnlyModeElement = document.createElement("div");
    this.hintElement = document.createElement("div");
    this.fullScreenElement = document.createElement("div");
    this.projectShareUsersElement = document.createElement("div");
    this.currentEditorReadOnlyElement = document.createElement("div");
    this.currentEditorElement = document.createElement("div");
    this.drawerTypeElement = document.createElement("div");
    this.menusPopElement = document.createElement("div");
  }

  /**
   * 
   * @param {EditorAbs} editor 
   * @param {*} editorReadOnly 
   */
  EditComp = (editor, editorReadOnly) => {
    const { safeMode, readOnlyMode, editCompDrop } = this.state;
    const menu = <Menu className="data-html2canvas-ignore normal-icon" onClick={(info) => {
      const { fileData, linkData } = this.state;
      const filename = (fileData && fileData.name) || (linkData && getFileName(linkData.fileKey)) || "notDefineFileName";
      switch (info.key) {
        case "safeMode":
          this.safeModeFunc();
          break;
        case "readOnlyMode":
          this.readOnlyModeFunc();
          break;
        case "unpin":
          editor.pinCodes(false);
          break;
        case "pin":
          editor.pinCodes(true);
          break;
        case "dependencies":
          this.openFunc(DrawerType.ImportModules);
          break;
        case "openDataPane":
          this.openFunc(DrawerType.FileAttachments);
          break;
        case "screenshot":
          this.getEditor().setWrapperLoading(true, async () => {
            let editorjsEle = document.querySelector(`.editorjs-wrapper.${this.getEditor().getTransferKey()}>.editorjs`);
            editorjsEle.classList.add("print");
            await html2canvas(editorjsEle, { backgroundColor: getComputedStyle(document.body).getPropertyValue('--background-color') }).then(function (canvas) {
              editorjsEle.classList.remove("print");
              let imgData = canvas.toDataURL("image/png", 1.0);
              saveLink(imgData, filename);
            }, (e) => {
              showToast("screenshot fail!", "error");
              editorjsEle.classList.remove("print");
            });
            this.getEditor().setWrapperLoading(false);
          }, true)
          break;
        case "print":
          this.pdfPrint();
          break;
        case "export2Word":
          this.getEditor().setWrapperLoading(true, async () => {
            let scrollY = window.pageYOffset || window.scrollY || document.body.scrollTop;
            let scrollX = window.pageXOffset || window.scrollX || document.body.scrollLeft;
            window.scrollTo(0, 0);
            await this.getEditor().toDoc();
            window.scrollTo(scrollX, scrollY);
            this.getEditor().setWrapperLoading(false);
          }, true)
          break;
      }
    }}>
      <Menu.Item key="safeMode" title={`${safeMode ? "Off" : "On"} Safe Mode`}>
        <i className={`icon ${!safeMode ? "icon-inactive" : ""} far fa-life-ring`}></i>
        Safe Mode
      </Menu.Item>
      {!readOnlyMode && <Menu.Item key="readOnlyMode" title={`${readOnlyMode ? "Off" : "On"} Read Mode`}>
        <i className={`icon ${!readOnlyMode ? "icon-inactive" : ""} fas fa-book-open`}></i>
        Read Mode
      </Menu.Item>}
      {!editorReadOnly && <Menu.Item key="unpin" title={`Unpin Code`}>
        <i className="iconfont icon icon-unpin"></i>Unpin Codes
      </Menu.Item>}
      {!editorReadOnly && <Menu.Item key="pin" title={`Pin Code`}>
        <i className="icon fas fa-thumbtack"></i>Pin Codes
      </Menu.Item>}
      <Menu.Item key="dependencies" title={`Open Dependencies Pane`}>
        <i className="icon fas fa-atom"></i>Dependencies
      </Menu.Item>
      <Menu.Item key="openDataPane" title={ShortcutTitle(window_shortcuts_keys['Open Data Pane'])}>
        <i className="icon fas fa-paperclip"></i>Open Data Pane
      </Menu.Item>
      <Menu.Item key="screenshot" title={`Screenshot`}>
        <i className="icon fas fa-camera"></i>Screenshot
      </Menu.Item>
      <Menu.Item key="print" title={ShortcutTitle(window_shortcuts_keys['PDF Print'])}>
        <i className="icon fas fa-print"></i>Print
      </Menu.Item>
      <Menu.Item key="export2Word" title={`Word`}>
        <i className="icon far fa-file-word"></i>Word
      </Menu.Item>
    </Menu>
    return <span><Dropdown overlay={menu} onVisibleChange={(visible) => { this.setState({ editCompDrop: visible }) }} placement="bottomCenter">
      <CaretDownOutlined className={`icon-bordered icon icon-mouseover transition-icon ${editCompDrop ? "transition-icon-hover" : ""}`}></CaretDownOutlined>
    </Dropdown></span>
  }

  setCurrentEditor = () => {
    let currentEditorTmp = this.getEditor();
    if (window.currentEditor === currentEditorTmp) {
      return window.currentEditor;
    }
    window.currentEditor = currentEditorTmp;
    window.requestAnimationFrame(() => {
      window.scrollTo(0, window.currentEditor.getScrollPos());
    })
    window.currentEditor.getNeedRefresh() && window.currentEditor.refreshFile();
    window.currentEditor.focusCurrentFileData();
    actions.variable(actions.types.CURRENT_EDITOR, [], () => { return window.currentEditor })
    actions.variable(actions.types.CHART_EDITOR, [], () => {
      return undefined;
    })
    return window.currentEditor;
  }

  onDropFunc = (e) => {
    e.preventDefault();
    let dragFileKey = e.dataTransfer.getData("dragFileKey");
    if ("" !== dragFileKey) {
      this.switchFileTagEditor(dragFileKey);
    }
  }
  /**
   * 
   * @param {DefaultFileData[]} taginfos 
   */
  loadTaginfos = async (taginfos) => {
    for (let i = 0; i < taginfos.length; i++) {
      let taginfo = taginfos[i];
      if (!taginfo) {
        return;
      }
      if (taginfo.projectId) {
        await this.switchLinkTagEditor(taginfo, false);
      } else {
        await this.switchFileTagEditor(taginfo, taginfo.selectedVersion || DEFAULT_VER, undefined, false);
      }
    }
  }
  /**
   * 
   * @param {DefaultShareData} linkData 
   */
  switchLinkTagEditor = async (linkData, refresh = false, replaceEditor) => {
    let state = {};
    if (!linkData) {
      showToast("Cannot find link", "error")
      return;
    }
    const { tagEditors } = this.state;
    /**@type {ShapeChatInfo} */
    let shared = undefined;
    if (linkData && (~[TAG_TYPE.shared, TAG_TYPE.projectShare].indexOf(linkData.tag)) &&
      linkData.userId && linkData.userId !== "undefined") {
      shared = _.assign({}, ShapeChatInfo, linkData);
      shared.isMaster = true;
    }
    let uploadUri = linkData.uploadUri;
    let key = uploadUri + linkData.fileKey;
    if (editor.getKey() === key) {
      _.assign(state, { tagedEditor: undefined });
      this.setState(state, this.setCurrentEditor)
      return;
    }
    let filterEditors = _.filter(tagEditors, (tagEditor) => {
      return tagEditor.getKey() === key;
    });
    if (filterEditors.length === 0) {
      let indexOf = -1;
      if (replaceEditor && ~tagEditors.indexOf(replaceEditor)) {
        indexOf = tagEditors.indexOf(replaceEditor);
        await this.closeTagEditor(replaceEditor, state);
      }
      let tagEditor = new TagEditor(_.assign({}, ShapeTagEditorInfo, {
        fileData: undefined,
        linkData: linkData,
        shared: shared,
        contentType: ContentType.Link,
        version: linkData.version,
        react_component: editor.react_component,
        key,
        uploadUri: uploadUri,
      }));
      if (~indexOf) {
        tagEditors.splice(indexOf, 0, tagEditor);
      } else {
        tagEditors.push(tagEditor);
      }
      await new Promise((resolve, reject) => {
        _.assign(state, { tagEditors, tagedEditor: tagEditor });
        let cb = async () => {
          let uri = linkData.uploadUri + `${linkData.fileKey}${linkData.version ?
            `${verFileTag}${fileSeparator}${linkData.version}` : ""}`;
          let content = await getContent(linkData, uri);
          content = wrapContent(content, linkData, uploadUri);
          tagEditor.init({
            data: DefaultInitData,
          });
          tagEditor.get().isReady
            .then(async (v) => {
              await tagEditor.value(content, linkData.version)
              this.setCurrentEditor();
              DEBUG_EDITOR_JS && console.log('Tag Editor.js is ready to work!');
              resolve(linkData.fileKey);
            }).catch((reason) => {
              console.log(`Tag Editor.js initialization failed because of ${reason}`);
              showToast(reason, 'error');
              reject(`Tag Editor.js initialization failed because of ${reason}`)
            });
          this.saveTaginfos();
        };
        this.setState(state, cb);
      })
    } else {
      let tagEditor = filterEditors[0];
      if (!refresh) {
        _.assign(state, { tagedEditor: tagEditor })
        this.setState(state, this.setCurrentEditor);
        return;
      }
      await new Promise((resolve, reject) => {
        _.assign(state, { tagedEditor: tagEditor })
        let cb = async () => {
          let uri = linkData.uploadUri + `${linkData.fileKey}${linkData.version ?
            `${verFileTag}${fileSeparator}${linkData.version}` : ""}`;
          let content = await getContent(linkData, uri);
          content = wrapContent(content, linkData, uploadUri);
          await tagEditor.value(content, linkData.version);
          this.setCurrentEditor();
          resolve(linkData.fileKey);
        };
        this.setState(state, cb);
      })
    }
  }

  switchFileTagEditor = async (fileKeyOrData, fileVersion = DEFAULT_VER, content, shared, refresh = false, replaceEditor) => {
    let state = {};
    const { tagEditors } = this.state;
    const { uploadUri, fileNamesJson } = editor.react_component.state;
    /**@type {DefaultFileData} */
    let fileData = typeof fileKeyOrData === 'string' ? fileNamesJson[fileKeyOrData] : fileKeyOrData;
    if (!fileData) {
      showToast("Cannot find file: " + fileKeyOrData, "error")
      return;
    }
    let key = uploadUri + fileData.fileKey;
    if (editor.getKey() === key) {
      await editor.switchFile(fileData.fileKey, fileVersion);
      _.assign(state, { tagedEditor: undefined });
      this.setState(state, this.setCurrentEditor)
      return;
    }
    let filterEditors = _.filter(tagEditors, (tagEditor) => {
      return tagEditor.getKey() === key
    });
    if (filterEditors.length === 0) {
      let indexOf = -1;
      if (replaceEditor && ~tagEditors.indexOf(replaceEditor)) {
        indexOf = tagEditors.indexOf(replaceEditor);
        await this.closeTagEditor(replaceEditor, state);
      }
      let tagEditor = new TagEditor(_.assign({}, ShapeTagEditorInfo, {
        fileData: fileData,
        linkData: undefined,
        shared: shared || editor.react_component.getChatData(fileData, undefined, fileVersion),
        contentType: ContentType.File,
        version: fileVersion,
        react_component: editor.react_component,
        key,
        uploadUri: uploadUri,
        settings: editor.getSettings(),
      }));
      if (~indexOf) {
        tagEditors.splice(indexOf, 0, tagEditor);
      } else {
        tagEditors.push(tagEditor);
      }
      await new Promise((resolve, reject) => {
        _.assign(state, { tagEditors, tagedEditor: tagEditor });
        let cb = async () => {
          content = content || (await editor.react_component.getFileContent(fileData, fileVersion));
          content = wrapContent(content, fileData, uploadUri);
          tagEditor.init({
            data: DefaultInitData,
          });
          tagEditor.get().isReady
            .then(async (v) => {
              DEBUG_EDITOR_JS && console.log('Tag Editor.js is ready to work!');
              await tagEditor.value(content, fileVersion);
              this.setCurrentEditor();
              resolve(fileData.fileKey);
            }).catch((reason) => {
              console.log(`Tag Editor.js initialization failed because of ${reason}`);
              showToast(reason, 'error');
              reject(`Tag Editor.js initialization failed because of ${reason}`)
            });
          this.saveTaginfos();
        };
        this.setState(state, cb);
      })
    } else {
      /**@type {TagEditor} */
      let tagEditor = filterEditors[0];
      if (tagEditor.version === fileVersion && !refresh) {
        await new Promise((resolve, reject) => {
          _.assign(state, { tagedEditor: tagEditor });
          let cb = () => {
            this.setCurrentEditor();
            resolve(fileData.fileKey);
          };
          this.setState(state, cb);
        })
      } else {
        await new Promise((resolve, reject) => {
          _.assign(state, { tagedEditor: tagEditor })
          let cb = async () => {
            content = content || (await editor.react_component.getFileContent(fileData, fileVersion));
            content = wrapContent(content, fileData, uploadUri);
            await tagEditor.value(content, fileVersion);
            this.setCurrentEditor();
            tagEditor.refreshFileData(tagEditor.getFileData(), fileData)
            resolve(fileData.fileKey);
          };
          this.setState(state, cb);
        })
      }
    }
  }

  componentDidMount() {
    actions.variable(actions.types.REFRESH_GRAPH, [], () => {
      return window.graphxrApi;
    });
    document.addEventListener("keydown", this.keydownFunc, false);
    document.getElementById("folder") && document.getElementById("folder").addEventListener("keydown", this.drawerKeydownFunc, false);
    /** count seconds */
    this.count = 0;
    /** use Observable as use React cause Endless Loop //TODO need check */
    actions.inspector(this.graphApiElement, ["graphApi"],
      (graphApi) => {
        return graphApi;
      })
    actions.inspector(this.fileChangeElement, [actions.types.FILE_CHANGE],
       /** @param {ShapeFileChange} fileChangeData */(fileChangeData) => {
        const { resetLinkData, file, version, shared, needUpload } = fileChangeData;
        let state = {};
        if (resetLinkData) {
          _.assign(state, { linkData: undefined });
        }
        if (!_.isEqual(this.state.fileData, file)) {
          _.assign(state, { fileData: file })
        }
        if (!_.isEqual(this.state.fileData, file)) {
          _.assign(state, { fileData: file })
        }
        if (!_.isEqual(this.state.file_version_selected, version || DEFAULT_VER)) {
          _.assign(state, { file_version_selected: version || DEFAULT_VER })
        }
        if (!_.isEqual(this.state.shared, shared)) {
          _.assign(state, { shared: shared })
        }
        if (!_.isEqual(this.state.needUpload, needUpload)) {
          _.assign(state, { needUpload: needUpload })
        }
        !_.isEmpty(state) && this.setState(state);
      })
    actions.inspector(this.linkChangeElement, [actions.types.LINK_CHANGE],
        /** @param {ShapeLinkChange} linkChangeData */(linkChangeData) => {
        const { resetFileData, file, shared, needUpload } = linkChangeData;
        let state = {};
        if (resetFileData) {
          _.assign(state, { fileData: undefined });
        }
        this.setState(_.assign(state, {
          linkData: file,
          file_version_selected: file ? file.version : DEFAULT_VER,
          shared: shared,
          needUpload: needUpload,
        }));
      })
    actions.inspector(this.maskElement, [actions.types.MASK],
      (mask) => {
        this.setState({ mask })
      })
    actions.inspector(this.readyElement, [actions.types.READY],
      (ready) => {
        this.setState({ ready })
      })
    actions.inspector(this.needSaveElement, [actions.types.NEED_SAVE_FILES],
      (needSaveFiles) => {
        this.setState({ needSaveFiles })
      })
    actions.inspector(this.resizeElement, [actions.types.RESIZE_WINDOW],
      (resize) => { this.setState({ resize }); })
    actions.inspector(this.usersElement, [actions.types.USERS],
      (users) => {
        this.setState({ users });
      })
    actions.inspector(this.loadingElement, [actions.types.LOADING],
      (loadingData) => {
        const { loading } = this.state;
        if (loadingData.loading === loading) {
          console.warn("loading state conflict!", loadingData.loading)
        }
        this.setState({ loading: loadingData.loading, loadingClosable: loadingData.loadingClosable || false }, loadingData.cb ? (() => { setTimeout(loadingData.cb) }) : undefined);
        if (!loadingData.loading) {
          actions.variable(actions.types.SW_PROJECT, [], () => {
            return { skey: getSkey(), masterUserId: getMasterUserId() };
          })
        }
      })
    actions.inspector(this.attachesElement, [actions.types.FILES_ATTACHES],
      (filesAttaches) => {
        this.setState({ filesAttaches });
      })
    actions.inspector(this.safeModeElement, [actions.types.SAFE_MODE],
      (safeMode) => {
        if (this.state.safeMode === safeMode) {
          return;
        }
        this.setState({ safeMode, loading: true, loadingClosable: false }, async () => {
          editor.setNeedRefresh(true);
          const { tagEditors } = this.state;
          _.each(tagEditors, (tagEditor) => {
            tagEditor.setNeedRefresh(true);
          });
          window.currentEditor && window.currentEditor.getNeedRefresh() && (await window.currentEditor.refreshFile());
          this.setState({ loading: false });
        });
      })
    actions.inspector(this.readOnlyModeElement, [actions.types.READ_ONLY_MODE],
      async (readOnlyMode) => {
        editor.getReadOnly() !== readOnlyMode && !editor.getShared() && editor.setNeedRefresh(true);
        const { tagEditors } = this.state;
        _.each(tagEditors, (tagEditor) => {
          tagEditor.getReadOnly() !== readOnlyMode && !tagEditor.getShared() && tagEditor.setNeedRefresh(true);
        });
        window.currentEditor && window.currentEditor.getNeedRefresh() && (await window.currentEditor.refreshFile());
        this.setState((state) => {
          let ret = { readOnlyMode };
          let cReadOnly = window.currentEditor && window.currentEditor.getReadOnly();
          if (state.editorReadOnly !== cReadOnly) {
            _.assign(ret, { editorReadOnly: cReadOnly });
          }
          return ret;
        });
      })
    actions.inspector(this.hintElement, [actions.types.HINT],
      (hint) => {
        return true;
      })
    actions.inspector(this.fullScreenElement, [actions.types.FULL_SCREEN],
      (fullScreen) => {
        this.setState({ fullScreen });
      })
    actions.inspector(this.projectShareUsersElement, [actions.types.PROJECT_SHARE_USERS],
      (projectShareUsers) => {
        this.setState({ projectShareUsers });
      })
    actions.inspector(this.currentEditorReadOnlyElement, [actions.types.CURRENT_EDITOR_READ_ONLY], (currentEditorReadOnly) => {
      this.setState({ editorReadOnly: currentEditorReadOnly })
    });
    actions.inspector(this.currentEditorElement, [actions.types.CURRENT_EDITOR], (currentEditor) => {
      this.setState({ editorReadOnly: currentEditor.getReadOnly() })
    });
    actions.inspector(this.drawerTypeElement, [actions.types.DRAWER_TYPE],
      (drawerType) => { this.openFunc(drawerType.drawerObj, drawerType.cb, drawerType.only, undefined, drawerType.dprops) })
    actions.inspector(this.menusPopElement, [actions.types.MENUS_POP],
      ({ position, cb }) => { this.setState({ position, cb }) })
    this.timerID = setTimeout(this.tick, 1000);
    this.requestData();
    actions.variable(actions.types.HINT, [
      "Inputs", "SQLite", "htl", "html", "d3"],
      (Inputs, SQLite, htl, html, d3) => {
        let obj = {};
        obj.Inputs = Inputs;
        obj.SQLite = SQLite;
        obj.htl = htl;
        obj.html = html;
        obj.d3 = d3;
        // obj._ = _.reduce(_.keys(_), (prev, curr, index) => {
        //   prev[curr] = undefined;
        //   return prev;
        // }, {});
        addAllGlobalScope(obj);
        return true;
      })

    // Synchronize Grove's light/dark mode setting with GraphXR's
    const darkMode = isGraphXrDarkMode()
    if (darkMode !== undefined) {
      console.log('setting darkMode to ', darkMode)
      actions.variable(actions.types.DARK_MODE, [], () => darkMode)
    }
    let oldtitle = document.head.querySelector("title").innerHTML;
    window.onbeforeprint = function (event) {
      const filename = document.querySelector(".page-head .ant-tag-has-color .tag-style") && document.querySelector(".page-head .ant-tag-has-color .tag-style").innerHTML || "notDefineFileName";
      document.body.classList.add("print");
      document.head.querySelector("title").innerHTML = filename;
    };
    window.onafterprint = (event) => {
      document.body.classList.remove("print");
      document.head.querySelector("title").innerHTML = oldtitle;
    };
  }

  componentWillUnmount() {
    actions.deleteCache(this.graphApiElement);
    actions.deleteCache(this.fileChangeElement);
    actions.deleteCache(this.linkChangeElement);
    actions.deleteCache(this.maskElement);
    actions.deleteCache(this.readyElement);
    actions.deleteCache(this.needSaveElement);
    actions.deleteCache(this.resizeElement);
    actions.deleteCache(this.usersElement);
    actions.deleteCache(this.loadingElement);
    actions.deleteCache(this.attachesElement);
    actions.deleteCache(this.safeModeElement);
    actions.deleteCache(this.readOnlyModeElement);
    actions.deleteCache(this.hintElement);
    actions.deleteCache(this.fullScreenElement);
    actions.deleteCache(this.projectShareUsersElement);
    actions.deleteCache(this.currentEditorReadOnlyElement);
    actions.deleteCache(this.currentEditorElement);
    actions.deleteCache(this.drawerTypeElement);
    actions.deleteCache(this.menusPopElement);
    document.removeEventListener("keydown", this.keydownFunc, false);
    document.getElementById("folder") && document.getElementById("folder").removeEventListener("keydown", this.drawerKeydownFunc, false);
    document.getElementById("drawer").innerHTML = ""
    clearTimeout(this.timerID);
  }

  /** 
    * @param {DefaultShareData} info
    */
  recordHistory = (info, stateTmp) => {
    if (this.infosIndex !== this.infos.length - 1) {
      this.infos.splice(this.infosIndex + 1, this.infos.length);
    }
    setWindowState(info);
    this.infos.push(_.cloneDeep(info));
    this.infosIndex = this.infos.length - 1;
    // console.error(JSON.stringify(this.infos, null, 1))
    let state = stateTmp || {};
    _.assign(state, { infos: this.infos, infosIndex: this.infosIndex });
    !stateTmp && this.setState(state);
  }

  /** 
   * @param {DefaultShareData} info
   * @param {boolean} history is it visit navigator history ?
   */
  handleSearch = async (info, history) => {
    const { options, tagEditors } = this.state;
    !info.name && (info.name = getFileName(info.fileKey));
    let key = info.uploadUri + info.fileKey;
    let filterEditors = _.filter(tagEditors, (tagEditor) => {
      return tagEditor.getKey() === key
    });
    if (filterEditors.length) {
      if (filterEditors.length !== 1) {
        showToast("open tab repeated!", 'error');
        return;
      }
      this.setState({ tagedEditor: filterEditors[0] }, this.setCurrentEditor)
      return;
    }
    let state = { tagedEditor: undefined };
    if (!info) {
      alert("Can not visit!")
      return;
    }
    if (!history) {
      if (!info || !info.userId || !info.projectId) {
        alert("Can not visit!")
        return;
      } else if (info.userId === USER_ID) {
        let arr = _.filter(options, (value) => { return value.projectId === info.projectId })
        if (arr.length === 0) {
          options.push(info);
        } else {
          if (arr[0] !== info) {
            delete info.projectName;
            _.assign(arr[0], info);
            info = arr[0];
          }
        }
        _.assign(state, { options });
      } else {
      }
      this.recordHistory(info, state);
    } else {
      setWindowState(info);
    }
    if (info.userId === USER_ID || info.projectId === PROJECT_ID) {//my project or shared project
      const { userId, userName, email, projectId, projectName, fileKey, version, uploadUri } = info;
      if (getSkey() === projectId) {//already opened project
        if (fileKey) {
          await editor.switchFile(fileKey, version);
        }
        _.assign(state, { infos: this.infos, infosIndex: this.infosIndex });
        !_.isEmpty(state) && this.setState(state, this.setCurrentEditor);
      } else {//not opened project
        setSkey(projectId, userId);
        await editor.clearEditor(async () => {
          const { mdfiles_transfer } = this.state;
          _.assign(mdfiles_transfer, { fileNamesJson: getFileNamesJson() });
          await this.requestData(_.assign(state, {
            mdfiles_transfer, userId, userName, email, projectId, projectName, fileKey,
            file_version_selected: version
          }));
          this.setCurrentEditor();
        }, false, false, undefined, false);
      }
    } else if (undefined !== info.uploadUri) {//share to me file
      if (info.userId !== SYSTEM_USER) {
        let res = await axios.post(`/api/grove/linkTo`, info);
        if (!res.data.status) {
          if (res.data.content instanceof Object) {
            info.attachments = res.data.content;
          }
          info.userName = res.data.userName;
          info.email = res.data.email;
        }
      }
      const { fileKey, version, projectId, projectName, userId, userName, email, uploadUri } = info;
      info.type = getFileType(fileKey);
      let mstate = {};
      if (projectId === PROJECT_ID) {
        /* let arr = _.filter(options, (value) => { return value.projectId === info.projectId })
        if (arr.length === 0) {
          options.push(info);
        } else {
          if (arr[0] !== info) {
            delete info.projectName;
            _.assign(arr[0], info);
            info = arr[0];
          }
        }
        _.assign(state, { options });
        if (getSkey() === projectId) {
          _.assign(mstate, { activeFileKey: fileKey, selectType: SelectType.file });
        } else {
          setSkey(projectId, userId);
          await editor.clearEditor(async () => {
            const { mdfiles_transfer } = this.state;
            _.assign(mdfiles_transfer, { fileNamesJson: getFileNamesJson() });
            await this.requestData(_.assign(state, {
              mdfiles_transfer, userId, userName, email, projectId, projectName, fileKey,
              file_version_selected: version
            }), false, false, undefined, false);
            this.setCurrentEditor();
          })
          if (checkIsMobile()) {
            this.openFunc(DrawerType.None)
          }
          return;
        } */
        return showToast('big error', 'error');
      } else if (!editor.linkData) {
        _.assign(mstate, { activeFileKey: undefined, selectType: SelectType.dir });
      }
      !_.isEmpty(mstate) && editor.react_component.setState(mstate);
      editor.setLinkData(info);
      await editor.linkTo(fileKey, version);
      _.assign(state, { infos: this.infos, infosIndex: this.infosIndex });
      !_.isEmpty(state) && this.setState(state, this.setCurrentEditor);
    } else {
      alert("error!")
    }
    if (checkIsMobile()) {
      this.openFunc(DrawerType.None)
    }
  }

  ChatComponent = () => {
    const { shared, linkData, fileData, editorReadOnly, ready, fullScreen, tagedEditor, needUpload, readOnlyMode } = this.state;
    if (!ready) {
      return <div className="blabla document-title">loading...</div>
    }
    return <div className={`d-flex chat normal-icon ${fullScreen ? "full-screen-head" : ""}`}>
      <div className="space-normal">
        {!editorReadOnly && needUpload && <i className="icon fas fa-cloud-upload-alt"
          title={ShortcutTitle(file_actions_keys['Upload Current File'])} onClick={() => {
            this.getEditor().upload();
          }}></i>}
        {this.EditComp(this.getEditor(), editorReadOnly)}
        <span>
          {readOnlyMode && <i
            onClick={() => {
              this.readOnlyModeFunc();
            }}
            className={`icon-bordered icon ${!readOnlyMode ? "icon-inactive" : ""} fas fa-book-open`} title={`${readOnlyMode ? "Off" : "On"} Read Mode`}></i>}
          <Chat currentEditor={this.getEditor()} info={shared} linkData={linkData} fileData={fileData}></Chat>
        </span>
        <i className={`icon-bordered icon ${!fullScreen ? "icon-inactive" : ""} fas fa-expand-alt`}
          title={ShortcutTitle(window_shortcuts_keys['Full Screen'])} onClick={async () => {
            const { fullScreen } = this.state;
            actions.variable(actions.types.FULL_SCREEN, [], () => {
              return !fullScreen
            })
          }}></i>
      </div>
    </div>;
  }

  UsersComp = () => {
    const { shared, users, fileData, linkData, userId, email, userName } = this.state;
    if (!shared) {
      if (fileData) {
        let userOutlineTitle = userId !== USER_ID ? email : USER_EMAIL;
        let userOutlineName = userId !== USER_ID ? userName : `You`;
        return <Avatar className="bg-primary text-white" ><i title={userOutlineTitle}> {userOutlineName} </i></Avatar>;
      } else if (linkData) {
        return <Avatar className={colors[0]} >
          <i title={linkData.email}> {linkData.userName} </i></Avatar>;
      } else {
        return <div></div>;
      }
    }
    let youUser = _.filter(_.values(users), (user, index) => {
      return user.userId === USER_ID;
    })[0];
    return <Avatar.Group maxCount={MAX_COUNT} maxStyle={MAX_STYLE}>
      {_.map(_.values(users).sort(usersCompareFunc), (user, index) => {
        let you = user === youUser;
        let userName = user.userName;
        let isMaster = user.isMaster;
        let userStatus = user.userStatus;
        let style;
        if (!~Grays.indexOf(userStatus)) {
          style = !isMaster ?
            colors[index % colors.length] :
            "bg-primary text-white"
        } else {
          style = GrayColors[Grays.indexOf(userStatus)];
        }
        return <Tooltip key={user.userId}
          title={<div>
            <div>{isMaster ? "File Master" : ""}</div>
            <div>{Msg_UserStatus[user.userStatus]}</div>
            <div>{you ? `${userName}(You)` : userName}</div>
            <div>{you ? `${USER_EMAIL}(You)` : (user.email || "")}</div>
          </div>} placement="bottomRight">
          <Avatar className={style} >
            {you ? `(You)` : userName}
          </Avatar>
        </Tooltip>
      })}
    </Avatar.Group>
  }
  /**
   * @typedef {object} LinkMenusData
   * @property {TagEditor} editor - editor
   */
  /**
   * @param {LinkMenusData} props
   * @returns 
   */
  linkFileMenus = (props = { editor: editor, linkData: DefaultShareData }) => {
    const { editor, linkData } = props;
    if (editor.isLinkDataReadOnly()) {
      return null;
    }
    return <Menu className="data-html2canvas-ignore import-export-menu normal-icon" onClick={(info) => {
      switch (info.key) {
        case "ForkFile":
          editor.fork(linkData, editor.getReadOnly() ? editor.option.data : editor.latestText())
          break;
      }
    }}>
      <Menu.Item key="ForkFile"><i className="icon fas fa-code"></i>Fork File</Menu.Item>
    </Menu>
  }
  /**
   */
  newfileMenus = () => {
    return <Menu className="data-html2canvas-ignore import-export-menu normal-icon" onClick={(info) => {
      switch (info.key) {
        case "DisplaySource":
          let content = getFormatUndefinedContent();
          editor.react_component.setState({
            modalType: ModalType.DisplaySource, source: content, sourceFile: null,
            modalData: {}
          })
          break;
      }
    }}>
      <Menu.Item key="DisplaySource"><i className="icon fas fa-code"></i>Display Source</Menu.Item>
    </Menu>
  }
  TitleComp = () => {
    const { needSaveFiles, ready, tagedEditor } = this.state;
    const { fileData, linkData } = editor;
    const clickFunc = (e) => {
      const { tagedEditor } = this.state;
      if (tagedEditor) {
        this.setState({ tagedEditor: undefined }, this.setCurrentEditor);
      }
    }
    if (!ready) {
      return <div className="blabla document-title">loading...</div>
    }
    if (fileData || linkData) {
      return <Dropdown
        overlayStyle={{ width: "fit-content", minWidth: "unset", maxWidth: "unset" }}
        overlay={fileData ? editor.react_component.OuterMenus(fileData) : <this.linkFileMenus editor={editor} linkData={linkData}></this.linkFileMenus>}
        placement="bottomLeft" trigger={['click']} disabled={tagedEditor ? true : false}>
        <Tag className="d-flex align-items-center clicker" title={`${(fileData || linkData).fileKey}${!!editor.getVersion() ? `@${editor.getVersion()}` : ""}`} onClick={clickFunc} icon={<i className={`icon ${getFileIcon(fileData || linkData)} icon-with-text`}></i>} color={tagedEditor ? TAG_CLOSED : TAG_SELECTED}>
          <span className={"tag-style"}>{(fileData || linkData).name}{!!editor.getVersion() ? `@${editor.getVersion()}` : ""}</span>
        </Tag>
      </Dropdown>;
    } else {
      return <Dropdown
        overlayStyle={{ width: "fit-content", minWidth: "unset", maxWidth: "unset" }}
        overlay={<this.newfileMenus></this.newfileMenus>}
        placement="bottomLeft" trigger={['click']} disabled={tagedEditor ? true : false}>
        <Tag className="d-flex align-items-center" onClick={clickFunc} icon={<i className="icon fas fa-file icon-with-text clicker"></i>} color={tagedEditor ? TAG_CLOSED : TAG_SELECTED}>
          <span className={"tag-style"}><span>unsaved file</span>
            {/* <span>{formatSizeUnits(editor.option ? editor.option.data.length : 0)}</span> */}
            <span>{needSaveFiles[editor.getTransferKey()] ? "*" : ""}</span></span>
        </Tag>
      </Dropdown>
    }
  };

  /**
   * @returns {EditorAbs}
   */
  getEditor = () => {
    const { tagedEditor } = this.state;
    return (tagedEditor || editor);
  }

  TickFunc = () => {
    const { fileData, linkData, shared, file_version_selected, fullScreen, tagedEditor,
      projectId, projectName, needSaveFiles, needUpload, ready, filesAttaches, email, userId, userName, editorReadOnly } = this.state;
    let nowTime = new Date().getTime();
    let Description, HoverMe;
    if (!this.getEditor().isReady()) {
      return <div className="blabla document-title">loading...</div>
    }
    if (fileData) {
      let userOutlineTitle = userId !== USER_ID ? email : USER_EMAIL;
      let userOutlineName = userId !== USER_ID ? userName : `You`;
      Description = <div className="space-normal header-file-description">
        <UserOutlined title={userOutlineTitle} className="icon icon-muted icon-xs icon-with-text" />{userOutlineName}&nbsp;
        {fileData && isFileEditorJs(fileData) && <span className="clicker" onClick={() => {
          fileData && this.getEditor().react_component.versionControlFunc(fileData);
        }}><i title={`version ${file_version_selected}${fileData.messages && fileData.messages[file_version_selected] && `(${fileData.messages[file_version_selected]})` || ""}`} className="icon icon-inactive icon-xs fas fa-code-branch icon-with-text"></i>{file_version_selected}&nbsp;</span>}
        {fileData && fileData.shareData && <span className="clicker" onClick={() => {
          this.getEditor().react_component.publishFunc(fileData);
        }}><i title="published time" className="icon icon-inactive icon-xs fas fa-globe icon-with-text"></i>{codeTime(nowTime, new Date(fileData.shareData.createTime).getTime())}&nbsp;</span>}
        {fileData && isFileEditorJs(fileData) && <span className="clicker" onClick={() => {
          this.openFunc(DrawerType.FileAttachments);
        }}><i title={ShortcutTitle(window_shortcuts_keys['Open Data Pane'])} className="icon icon-inactive icon-xs fas fa-paperclip icon-with-text"></i>{(filesAttaches[this.getEditor().getTransferKey()] || new Set()).size}&nbsp;</span>}
        {needSaveFiles[this.getEditor().getTransferKey()] ? <EditOutlined title="modified time" className="icon icon-muted icon-xs icon-with-text" />
          : <i title="published time" className="icon icon-muted icon-xs far fa-edit"></i>}
        {codeTime(nowTime, fileData.mtimeMs)}&nbsp;
        <i title="project name" className="icon icon-muted icon-xs far fa-folder icon-with-text"></i>{projectName}&nbsp;
        <span className="light-gray" title="file size"> • </span>{formatSizeUnits(fileData.size)}&nbsp;
      </div>;
    } else if (linkData) {
      /**@type {DefaultFileData} */
      let cache = linkData.cache || { size: this.getEditor().option.data.length };
      Description = <div className="space-normal header-file-description">
        <UserOutlined title={linkData.email} className="icon icon-muted icon-xs icon-with-text" />{linkData.userName}&nbsp;
        <i title="version" className="icon icon-muted icon-xs fas fa-code-branch icon-with-text"></i>{linkData.version}&nbsp;
        <i title="project name" className="icon icon-muted icon-xs far fa-folder icon-with-text"></i>{linkData.projectName}&nbsp;
        {(cache.mtimeMs || linkData.mtimeMs) && <span>{needSaveFiles[this.getEditor().getTransferKey()] ? <EditOutlined title="modified time" className="icon icon-muted icon-xs icon-with-text" />
          : <i title="modified time" className="icon icon-muted icon-xs far fa-edit"></i>}<span className="text-warning icon-with-text">{codeTime(nowTime, (cache.mtimeMs || linkData.mtimeMs))}</span>&nbsp;</span>}
        {linkData.tag === TAG_TYPE.shared && <span><i title="shared time" className="icon icon-muted icon-xs fas fa-share-alt-square icon-with-text"></i>{codeTime(nowTime, new Date(linkData.createTime).getTime())}&nbsp;</span>}
        {linkData.tag === TAG_TYPE.published && <span><i title="published time" className="icon icon-muted icon-xs fas fa-globe icon-with-text"></i>{codeTime(nowTime, new Date(linkData.createTime).getTime())}&nbsp;</span>}
        <span className="light-gray" title="file size"> • </span>{formatSizeUnits(cache.size)}&nbsp;
      </div>;
    } else {
      Description = <div></div>
    }
    return <div><div className={`${(fullScreen || editorReadOnly) ? "hide" : ""} d-flex justify-content-start normal-icon `}>{Description}{this.UsersComp()}</div></div>
  };
  /**
   * @param {EditorAbs} tagEditor 
   */
  Menus = (tagEditor) => {
    return <Menu className="data-html2canvas-ignore import-export-menu normal-icon" onClick={(info) => {
      this.menuClickHandler(info, tagEditor)
    }}>
      <Menu.Item key="closeTab">Close Tab</Menu.Item>
      <Menu.Item key="closeOtherTabs"> Close Other Tabs</Menu.Item>
      <Menu.Item key="closeTabsToTheRight"> Close Tabs to the Right</Menu.Item>
      <Menu.Item key="closeAllTabs"> Close All Tabs</Menu.Item>
    </Menu>
  }
  /**
   * @param {*} info 
   * @param {EditorAbs} tagEditor 
   */
  menuClickHandler = async (info, tagEditor) => {
    const { tagEditors } = this.state;
    let tagEditorsTmp = _.clone(tagEditors);
    switch (info.key) {
      case "closeTab":
        tagEditor.setWrapperLoading(true, async () => {
          await this.closeTagEditor(tagEditor);
          tagEditor.setWrapperLoading(false);
        })
        break;
      case "closeOtherTabs":
        tagEditor.setWrapperLoading(true, async () => {
          let state = {};
          for (let index = 0, tEditor; tEditor = tagEditorsTmp[index], index < tagEditorsTmp.length; index++) {
            if (tEditor !== tagEditor) {
              await this.closeTagEditor(tEditor, state);
            }
          }
          this.setState(state, this.setCurrentEditor);
          tagEditor.setWrapperLoading(false);
        })
        break;
      case "closeTabsToTheRight":
        tagEditor.setWrapperLoading(true, async () => {
          let state = {};
          let sliceArr = tagEditorsTmp.slice(tagEditorsTmp.indexOf(tagEditor) + 1);
          for (let index = 0, tEditor; tEditor = sliceArr[index], index < sliceArr.length; index++) {
            await this.closeTagEditor(tEditor, state);
          }
          this.setState(state, this.setCurrentEditor);
          tagEditor.setWrapperLoading(false);
        })
        break;
      case "closeAllTabs":
        tagEditor.setWrapperLoading(true, async () => {
          let state = {};
          for (let index = 0, tEditor; tEditor = tagEditorsTmp[index], index < tagEditorsTmp.length; index++) {
            await this.closeTagEditor(tEditor, state);
          }
          this.setState(state, this.setCurrentEditor);
          tagEditor.setWrapperLoading(false);
        })
        break;
    }
  }

  /**
   * 
   * @param {TagEditor} tagEditor 
   */
  forMap = (tagEditor, index) => {
    const { tagedEditor } = this.state;
    return <span style={{ display: 'inline-block' }} key={tagEditor.getTransferKey()}>
      <Dropdown
        overlayStyle={{ width: "fit-content", minWidth: "unset", maxWidth: "unset" }}
        overlay={this.Menus(tagEditor)}
        placement="bottomLeft" trigger={['contextMenu']}>
        <span style={{ display: 'inline-block' }}>
          <Dropdown
            overlayStyle={{ width: "fit-content", minWidth: "unset", maxWidth: "unset" }}
            overlay={tagEditor.getFileData() ? tagEditor.react_component.OuterMenus(tagEditor.getFileData()) : <this.linkFileMenus editor={tagEditor} linkData={tagEditor.getLinkData()}></this.linkFileMenus>}
            placement="bottomLeft" trigger={['click']} disabled={tagEditor !== tagedEditor ? true : false}>
            <Tag className="d-flex align-items-center clicker" title={`${tagEditor.getData().fileKey}${!!tagEditor.getVersion() ? `@${tagEditor.getVersion()}` : ""}`} icon={<i className={`icon ${getFileIcon(tagEditor.getData())} icon-with-text`}></i>} closable onClose={e => {
              tagEditor.setWrapperLoading(true, async () => {
                await this.closeTagEditor(tagEditor);
                tagEditor.setWrapperLoading(false);
              })
              e.preventDefault();
            }} onClick={(e) => {
              const { tagedEditor } = this.state;
              if (tagEditor !== tagedEditor) {
                this.setState({
                  tagedEditor: tagEditor
                }, this.setCurrentEditor)
              }
              e.preventDefault();
            }} color={tagedEditor === tagEditor ? TAG_SELECTED : TAG_CLOSED}>
              <span className={"tag-style"}>{tagEditor.getData().name}{!!tagEditor.getVersion() ? `@${tagEditor.getVersion()}` : ""}</span>
            </Tag>
          </Dropdown>
        </span>
      </Dropdown>
    </span >
  };
  /**
   * 
   * @param {TagEditor|TagEditor[]} removedTagEditors 
   */
  closeTagEditor = async (removedTagEditors, stateTmp) => {
    let state = stateTmp || {};
    if (!removedTagEditors) {
      return;
    }
    if (!(removedTagEditors instanceof Array)) {
      removedTagEditors = [removedTagEditors]
    }
    const { tagEditors, tagedEditor } = this.state;
    for (let index = 0; index < removedTagEditors.length; index++) {
      let removedTagEditor = removedTagEditors[index];
      tagEditors.splice(tagEditors.indexOf(removedTagEditor), 1);
      let _delSelfModule = false;
      _.each(removedTagEditor.getModuleNames(), (moduleName) => {
        if (_.filter([editor, ...tagEditors], (tagEditor) => {
          return ~tagEditor.getModuleNames().indexOf(moduleName)
        }).length === 0) {
          if (moduleName === removedTagEditor.getSelfModuleName()) {
            _delSelfModule = true;
          }
          disposeModule(moduleName);
          removeModule(moduleName);
        }
      });
      await removedTagEditor.clear(_delSelfModule);
      if (removedTagEditor === tagedEditor) {
        _.assign(state, { tagedEditor: undefined });
      }
    }
    _.assign(state, { tagEditors });
    !stateTmp && this.setState(state, this.setCurrentEditor);
    this.saveTaginfos();
  };


  FileMenu = () => {
    const { infos, infosIndex, fileData, linkData, needUpload, tagedEditor, tagEditors } = this.state;
    const navMenu = (
      <Menu style={{ maxHeight: "200px", overflowY: "auto" }}>
        {
          _.reverse(_.map(infos, (info, index, infos) => {
            return <Menu.Item className={`menu-text ${infosIndex === index ? "text-primary" : "text-default"}`} onClick={() => {
              this.infosIndex = index;
              this.handleSearch(info, true);
            }} key={`${info.userId} ${info.projectId} ${info.fileKey}@${info.version} ${index}`}>
              {`${info.userName} ${info.projectName} ${info.fileKey}@${info.version}`}
            </Menu.Item>
          }))
        }
      </Menu>
    );
    const tagChild = tagEditors.map(this.forMap);
    let maxIconN = Math.floor((window.innerWidth - 16 * 2 - 196) / (155));
    maxIconN = maxIconN <= 0 ? 1 : maxIconN;
    let dropIcons = undefined;
    if (maxIconN > 0 && maxIconN < tagChild.length) {
      let rms = tagChild.splice(maxIconN - 1, tagChild.length - (maxIconN - 1));
      dropIcons = <Menu forceSubMenuRender={true} className="data-html2canvas-ignore import-export-menu normal-icon">
        {_.map(rms, (curr, index) => {
          return <Menu.Item key={"key" + index}>{curr}</Menu.Item>
        })}
      </Menu>
    }
    return <div className="file-menu">
      <div className="d-flex align-items-center">
        <span className="normal-icon">
          <i className={`icon ${!editor.getData() ? "icon-muted" : "icon-mouseover"} far fa-times-circle`}
            title="close file" onClick={() => {
              if (editor.getData()) {
                editor.clearEditor(undefined, true, true);
              }
            }}></i>
          <Dropdown placement="bottomRight" overlay={navMenu} disabled={!infos.length} >
            <i className={`icon ${infos.length ? "icon-mouseover" : "icon-muted"} fas fa-history`} title="history"></i>
          </Dropdown>
        </span>
        <TweenOneGroup
          enter={{
            scale: 0.8,
            opacity: 0,
            type: 'from',
            duration: 100,
          }}
          onEnd={e => {
            if (e.type === 'appear' || e.type === 'enter') {
              e.target.style = 'display: inline-block';
            }
          }}
          leave={{ opacity: 0, width: 0, scale: 0, duration: 200 }}
          appear={false}
        >
          {tagChild}
        </TweenOneGroup>
        {dropIcons && <Dropdown overlay={dropIcons} placement="bottomCenter" trigger={['click']}>
          <EllipsisOutlined className="icon" title="more" />
        </Dropdown>}
      </div>
    </div>
  }

  closeDrawer = () => {
    this.openFunc(DrawerType.None)
  };

  DrawerComp = () => {
    const { drawObj, initWidth, mdfiles_transfer, mdfiles_transfer: { fileNamesJson }, options,
      userId, userName, email, projectId, projectName, projectShareUsers, drawRefreshs, dprops,
      fileKey, file_version_selected, toggleOn, initData, mask, ready, resize, fileData, linkData, shared, filesAttaches, dFullScreen } = this.state;
    if (!initData) {
      return <div></div>;
    }
    let type = _.keys(DrawerType)[drawObj.index].toLowerCase();
    let width = dFullScreen ? window.innerWidth : drawObj.w;
    let youUser = _.filter(_.values(projectShareUsers), (user, index) => {
      return user.userId === USER_ID;
    })[0];
    return <DrawerEx drawerKey={drawObj.index} className={`${ready ? "" : "hide"} data-html2canvas-ignore drawer-div drawer-${type}`}
      mask={mask} keyboard={true} title={<div>
        <Select dropdownClassName="normal-icon" className={`normal-icon ${drawObj === DrawerType.Folder ? "" : "hide"}`}
          {...SelectPropsDef} style={{ width: "98%" }}
          value={`${fileKey}#${projectId}`} onChange={async (value) => {
            let vs = value.split("#");
            let info = _.filter(options, (v) => { return v.fileKey === vs[0] && v.projectId === vs[1] })[0]
            if (!info) {
              showToast("can not find!", 'error');
              return;
            }
            const { userName, email } = info;
            if (!userName || UNKOWN === userName || !email || UNKOWN === email) {
              let res = await axios.post(`/api/grove/linkTo`, info);
              if (!res.data.status) {
                _.assign(info, {
                  userName: res.data.userName,
                  email: res.data.email
                })
              }
            }
            this.handleSearch(info, false);
          }} placeholder="Select Folder">
          {_.map(options, (option, index) => {
            let key = `${option.fileKey}#${option.projectId}`;
            let label = `${option.projectName}#${option.fileKey}`;
            return <Option title={label}
              key={key}
              value={key}>
              <div className="d-inline-flex align-items-center">
                {option.userId === USER_ID ? "" : <i className="f7 icon icon-xs fas fa-share-alt-square"></i>}{label}
                {!_.isEmpty(projectShareUsers) && !noSharedProjectIds(option.projectId) &&
                  <Avatar.Group maxCount={MAX_COUNT} maxStyle={MAX_STYLE} size="small">
                    {_.map(_.values(projectShareUsers).sort(usersCompareFunc), (user, index) => {
                      let you = user === youUser;
                      let userName = user.userName;
                      let isMaster = user.isMaster;
                      let userStatus = user.userStatus;
                      let style;
                      if (!~Grays.indexOf(userStatus)) {
                        style = !isMaster ?
                          colors[index % colors.length] :
                          "bg-primary text-white"
                      } else {
                        style = GrayColors[Grays.indexOf(userStatus)];
                      }
                      return <Tooltip key={user.userId}
                        title={<div>
                          <div>{isMaster ? "Project Master" : ""}</div>
                          <div>{Msg_UserStatus[user.userStatus]}</div>
                          <div>{you ? `${userName}(You)` : userName}</div>
                          <div>{you ? `${USER_EMAIL}(You)` : (user.email || "")}</div>
                        </div>} placement="bottomRight">
                        <Avatar className={style}  >
                          {you ? `(You)` : userName}
                        </Avatar>
                      </Tooltip>
                    })}
                  </Avatar.Group>}</div>
            </Option>
          })}
        </Select>
        <div className={`${drawObj === DrawerType.Help ? "" : "hide"}`}>Help</div>
        <div className={`${drawObj === DrawerType.FindAndReplace ? "" : "hide"}`}>Find and replace</div>
        <div className={`${drawObj === DrawerType.Settings ? "" : "hide"}`}>Project [{projectName}] Settings</div>
        <div className={`${drawObj === DrawerType.Panels ? "" : "hide"}`}>
          <div className="d-flex justify-content-between"><div>Notebooks</div></div></div>
        <div className={`${drawObj === DrawerType.FileAttachments ? "" : "hide"}`}>
          <div className="d-flex justify-content-between"><div>Data</div></div></div>
        <div className={`${drawObj === DrawerType.ImportModules ? "" : "hide"}`}>
          <div className="d-flex justify-content-between"><div>Dependency versions</div></div></div>
      </div>}
      getContainer={document.getElementById("folder")}
      destroyOnClose={false}
      direction={'right'}
      onClose={this.closeDrawer}
      visible={!ready || drawObj !== DrawerType.None}
      bodyStyle={{ paddingBottom: 12 }}
      footer={<div>
        {drawObj === DrawerType.Panels && <div className="d-flex justify-content-end">{/* <Button type="link" onClick={(e) => {
          editor.handleSearch(_.assign({}, DefaultShareData, {
            userId: SYSTEM_USER,
            projectId: "systemProjectId",
            fileKey: "five-minute-introduction",
            uploadUri: SYSTEM_USER_URI,
          }), false)
        }}>User Manual</Button> */}
          <Link fileKey={`${GUIDE_LINK_URI}user-manual/user-manual`} text="User Manual"></Link>
        </div>}
        {drawObj === DrawerType.Settings && <div className="settings-bottom">Width scale: {(new Number(resize.widthScale)).toFixed(0)}%,&nbsp;&nbsp;
          Height scale: {(new Number(resize.heightScale)).toFixed(0)}%</div>}
      </div>
      }
      closeIcon={<CloseOutlined title='Close (Esc)' />}
      initWidth={initWidth}
      dragCallback={(deltaPosition) => {
        const { drawObj, initWidth } = this.state;
        drawObj.w = initWidth - deltaPosition.x.toFixed(0);
        editor.saveSettings();
        DEBUG_DRAG && console.log("drag")
        if (drawObj === DrawerType.Folder) {
          this.mdfilesRef.current && debounce(() => {
            let width = this.state.dFullScreen ? window.innerWidth : drawObj.w;
            this.mdfilesRef.current.state.width !== width && this.mdfilesRef.current.setState({ width: width })
          }, 500)();
        }
      }}
    >
      <div className={`drawer-type`}>
        <MdFiles ref={this.mdfilesRef} userId={userId} userName={userName} email={email}
          projectId={projectId} projectName={projectName} fileKey={fileKey}
          className={`${drawObj === DrawerType.Folder ? "" : "hide"}`}
          mdfiles_transfer={mdfiles_transfer} version={file_version_selected} width={width}>
        </MdFiles>
        {<Help className={`${drawObj === DrawerType.Help ? "" : "hide"}`} {...dprops}></Help>}
        {drawObj === DrawerType.FindAndReplace && <FindAndReplace drawRefreshs={drawRefreshs} info={fileData || linkData} editor={this.getEditor()} ></FindAndReplace>}
        <Settings editor={this.getEditor()} projectId={projectId} projectName={projectName} userId={userId}
          className={`${drawObj === DrawerType.Settings ? "" : "hide"}`} {...dprops} ></Settings>
        {drawObj === DrawerType.Panels && <Panels
          mdfiles_transfer={mdfiles_transfer}></Panels>}
        {drawObj === DrawerType.FileAttachments && fileData && <FileAttachments editor={this.getEditor()}
          attaches={filesAttaches[this.getEditor().getTransferKey()] || new Set()}
          fileData={fileData} fileNamesJson={fileNamesJson}></FileAttachments>}
        {<ImportModules editor={this.getEditor()}
          className={`${drawObj === DrawerType.ImportModules ? "" : "hide"}`}
          info={fileData || linkData}></ImportModules>}
        <BackTop target={() =>
          document.querySelector(".drawer-div .ant-drawer-body")
        }
          visibilityHeight={200}>
          <UpCircleOutlined className="ant-back-top-icon" />
        </BackTop>
      </div>
    </DrawerEx>
  }

  /**
   * 
   * @param {KeyboardEvent} e 
   */
  keydownFunc = async (e) => {
    let folderClass = ".folder";
    e = e || window.event;
    let key = String.fromCharCode(e.keyCode).toLowerCase();
    //filter the number & char key
    if (!(/^[0-9a-z]$/g).test(key) || e.key === e.code) {
      key = e.key;
    }
    //support backspace for delete
    // if (key === 'Backspace') {
    //   key = 'Delete'
    // }

    const { fullScreen } = this.state;
    // console.log("keydown func:",e, key);
    // console.error(e.ctrlKey ? "ctrl" : "", e.metaKey ? "meta" : "", e.altKey ? "alt" : "", e.shiftKey ? "shift" : "", key);
    const notOwn = !editor.react_component || editor.react_component.getNoOperateFileSystem();
    if (IsKey(e, key, file_actions_keys['Rename Selected File'])) {
      if (!notOwn) {
        actions.variable(actions.types.DRAWER_TYPE, [], () => {
          return {
            drawerObj: DrawerType.Folder,
            cb: () => {
              actions.variable(actions.types.RENAME, [], () => {
                return {}
              })
            },
            only: true,
          }
        })
      }
    } else if (IsKey(e, key, file_actions_keys['New File'])) {
      if (!notOwn) {
        actions.variable(actions.types.DRAWER_TYPE, [], () => {
          return {
            drawerObj: DrawerType.Folder,
            cb: () => {
              document.querySelector(`${folderClass} .icon.fas.fa-file`) &&
                document.querySelector(`${folderClass} .icon.fas.fa-file`).click();
            },
            only: true,
          }
        })
      }

    } else if (IsKey(e, key, file_actions_keys['Import Files'])) {
      !notOwn && this.fileInputRef.current.click();
    } else if (IsKey(e, key, file_actions_keys['Download Current File'])) {
      actions.variable(actions.types.DOWNLOAD_FILE, [], () => {
        return true;
      })
    } else if (IsKey(e, key, file_actions_keys['Upload Current File'])) {
      window.currentEditor.upload();
    } else if (IsKey(e, key, file_actions_keys['Duplicate Selected File/Folder'])) {
      actions.variable(actions.types.DUPLICATE, [], () => {
        return true;
      })
    } else if (IsKey(e, key, file_actions_keys['Download All Files'])) {
      actions.variable(actions.types.DOWNLOAD, [], () => {
        return true;
      })
    } else if (IsKey(e, key, file_actions_keys['Remove Selected File/Folder'])
      && document.querySelector(`.ant-drawer.drawer-folder`) &&
      document.querySelector(`.ant-drawer.drawer-folder`).contains(document.activeElement) &&
      document.activeElement.tagName !== "input") {
      !notOwn && document.querySelector(`${folderClass} .icon.fas.fa-trash`) &&
        document.querySelector(`${folderClass} .icon.fas.fa-trash`).click();
    } else if (IsKey(e, key, cell_shortcuts_keys['Delete Current Cell'])) {
      !notOwn && window.currentEditor.deleteCurrentBlock(e);
    } else if (IsKey(e, key, window_shortcuts_keys['Open File Explorer'])) {
      this.openFunc(DrawerType.Folder)
    } else if (IsKey(e, key, window_shortcuts_keys['Open Explore Pane'])) {
      this.openFunc(DrawerType.Panels)
    } else if (IsKey(e, key, window_shortcuts_keys['Open Settings Pane'])) {
      this.openFunc(DrawerType.Settings)
    } else if (IsKey(e, key, window_shortcuts_keys['Help?'])) {
      this.openFunc(DrawerType.Help)
    } else if (IsKey(e, key, code_editor_keys['Find and replace'])) {
      this.getEditor().setSearch(window.getSelection().toString());
      this.openFunc(DrawerType.FindAndReplace, undefined, !!this.getEditor().getSearchText(), true)
    } else if (IsKey(e, key, window_shortcuts_keys['Open Data Pane'])) {
      this.openFunc(DrawerType.FileAttachments)
    } else if (IsKey(e, key, window_shortcuts_keys['PDF Print'])) {
      this.pdfPrint();
    } else if (IsKey(e, key, window_shortcuts_keys['Full Screen'])) {
      actions.variable(actions.types.FULL_SCREEN, [], () => {
        return !fullScreen;
      })
      e.preventDefault();
    } else if (IsKey(e, key, window_shortcuts_keys['Escape Full Screen'])) {
      fullScreen && actions.variable(actions.types.FULL_SCREEN, [], () => {
        return false
      })
      if (this.getEditor().isOpenSelect()) {
        actions.variable(actions.types.OPEN_SELECT, [], () => { return { editorKey: this.getEditor().getTransferKey(), openSelect: false } });
      }
      this.getEditor().multipleChoiceFunc(true);
      e.preventDefault();
    } else if (IsKey(e, key, window_shortcuts_keys['Search GraphXR API Docs'])) {
      this.setState({ graphxrApiSearchBlockIndex: this.getEditor().getCurrentBlockIndex(), isGraphxrApiSearchVisible: true })
    } else if (IsKey(e, key, cell_shortcuts_keys['Go Back Editing Cell'])) {
      let data = this.getEditor().prevPosition();
      data && data.closest(".code-edit") && setTimeout(() => data.closest(".code-edit").editCode(true));
    } else if (IsKey(e, key, cell_shortcuts_keys['Go Forward Editing Cell'])) {
      let data = this.getEditor().nextPosition();
      data && data.closest(".code-edit") && setTimeout(() => data.closest(".code-edit").editCode(true));
    } else if (IsKey(e, key, cell_shortcuts_keys['Pin/Unpin Cell'])) {
      this.getEditor().pinCode();
      e.preventDefault();
    } else if (IsKey(e, key, cell_shortcuts_keys['Move Cell Down'])) {
      this.getEditor().moveCellDown();
    } else if (IsKey(e, key, cell_shortcuts_keys['Move Cell Up'])) {
      this.getEditor().moveCellUp();
    } else {
      this.getEditor().currentBlockKeydownFunc(e);
    }
  }

  pdfPrint = () => {
    this.getEditor().setWrapperLoading(true, async () => {
      /**@type {import('jspdf').jsPDF} */
      let doc = new jsPDF();
      let oldBgColor = document.querySelector(`.editorjs-wrapper.${this.getEditor().getTransferKey()}`).style.backgroundColor;
      document.querySelector(`.editorjs-wrapper.${this.getEditor().getTransferKey()}`).style.backgroundColor = window.getComputedStyle(document.body).backgroundColor;
      document.body.classList.add("print");
      // doc.setFont("simhei");
      await doc.html(document.querySelector(`.editorjs-wrapper.${this.getEditor().getTransferKey()}`), {
        x: 0,
        y: 0,
        width: 210,
        windowWidth: window.screen.width,
        margin: 0,
        html2canvas: {
          // backgroundColor: null
        }
      });
      const filename = document.querySelector(".page-head .ant-tag-has-color .tag-style") && document.querySelector(".page-head .ant-tag-has-color .tag-style").innerHTML || "notDefineFileName";
      await doc.save(filename, { returnPromise: true });
      document.querySelector(`.editorjs-wrapper.${this.getEditor().getTransferKey()}`).style.backgroundColor = oldBgColor;
      document.body.classList.remove("print");
      this.getEditor().setWrapperLoading(false);
    }, true)
  }

  drawerFullScreenFunc = () => {
    const { dFullScreen } = this.state;
    this.setState({ dFullScreen: !dFullScreen });
  }

  drawerKeydownFunc = async (e) => {
    e = e || window.event;
    let key = String.fromCharCode(e.keyCode).toLowerCase();
    if (!(/^[0-9a-z]$/g).test(key) || e.key === e.code) {
      key = e.key;
    }
    if (IsKey(e, key, window_shortcuts_keys['Drawer Full Screen'])) {
      this.drawerFullScreenFunc();
      e.preventDefault();
    }
  }

  saveTaginfos = () => {
    const { tagEditors } = this.state;
    let taginfos = _.reduce(tagEditors, (prev, tagEditor) => {
      if (tagEditor.getData()) {
        prev.push(tagEditor.getData());
      }
      return prev;
    }, []);
    setItem(getTaginfosKey(), JSON.stringify(taginfos));
  }

  requestData = async (state) => {
    state = state || {};
    const drawObj = state.drawObj || this.state.drawObj;
    const userId = state.userId || this.state.userId;
    const projectId = state.projectId || this.state.projectId;
    const fileKey = state.fileKey || this.state.fileKey;
    const mdfiles_transfer = state.mdfiles_transfer || this.state.mdfiles_transfer;
    const fileNamesJson = mdfiles_transfer.fileNamesJson;
    let userName = state.userName || this.state.userName;
    let email = state.email || this.state.email;
    const { tagEditors } = this.state;
    const tagEditorsTmp = _.clone(tagEditors);
    for (let index = 0, tEditor; tEditor = tagEditorsTmp[index], index < tagEditorsTmp.length; index++) {
      if (tEditor.getData() && tEditor.getData().projectId !== projectId) {
        await this.closeTagEditor(tEditor, state);
      }
    }
    if (!userName || UNKOWN === userName || !email || UNKOWN === email) {
      let res = await axios.post(`/api/grove/linkTo`, { fileKey, projectId, userId });
      if (!res.data.status) {
        _.assign(state, {
          userName: res.data.userName,
          email: res.data.email
        })
      }
    }
    let res = {};
    if (isNotLogin()) {
      res = await axios.post(`/api/grove/loadShare`, {
        userId: userId,
        projectId: projectId,
        fileKey,
      });
      if (!res.data.status && res.data.content instanceof Object) {
        let res2 = await axios.post(`/api/grove/linkTo`, {
          userId: userId,
          projectId: projectId,
          fileKey
        });
        if (!res2.data.status && res2.data.content instanceof Object) {
          _.assign(res.data.content, res2.data.content);
        }
      }
    } else {
      res = await axios.post(`/api/grove/load`, {
        userId: userId, settings: InitSettings, projectId: projectId
      });
    }
    if (!res.data.status && res.data.content instanceof Object) {
      if (typeof (res.data.settings) !== 'object') {
        res.data.settings = InitSettings;
      }
      _.each(fileNamesJson, (value, key) => {
        if (fileNamesJson[key] && !res.data.content[key] &&
          getItem(getFileNamesKey() + '.' + key) === undefined) {
          console.error(`local file not exist! ${key}`);
          delete fileNamesJson[key];
        }
      })
      _.each(fileNamesJson, (value, key) => {
        if (fileNamesJson[key] && res.data.content[key] &&
          getItem(getFileNamesKey() + '.' + key) === undefined &&
          fileNamesJson[key].mtimeMs !== res.data.content[key].mtimeMs) {
          console.error(`someone remote have modified! ${key}`);
          delete fileNamesJson[key];
        }
      })
      _.merge(fileNamesJson, _.reduce(res.data.content, (result, value, key) => {
        if (!fileNamesJson[key]) {
          result[key] = _.cloneDeep(value);
        } else {
          _.merge(fileNamesJson[key], value);
        }
        return result;
      }, {}));
      _.each(fileNamesJson, (value, key) => {///fix prev bug
        if (getItem(getFileNamesKey() + '.' + key) !== undefined && res.data.content[key] &&
          fileNamesJson[key].mtimeMs === res.data.content[key].mtimeMs) {
          fileNamesJson[key].mtimeMs = (new Date()).getTime()
        }
      })
      setItem(getFileNamesKey(), JSON.stringify(fileNamesJson));
      _.assign(mdfiles_transfer, {
        fileNamesJson: fileNamesJson,
        cloud: res.data.content,
        uploadUri: res.data.uploadUri,
        settings: res.data.settings
      });
      let drawerType = res.data.settings.drawerType || InitSettings.drawerType;
      this.setState(_.assign(state, {
        mdfiles_transfer,
        initWidth: verifyW(drawerType[_.keys(drawerType)[drawObj.index]].w),
        initData: true,
        loading: true,
        loadingClosable: false
      }), () => {
      });
    }
    if (res.data.message && res.data.status) {
      showToast(res.data.message, "error");
    }
  }

  tick = async () => {
    this.count++;
    const { tagEditors } = this.state;
    let promiseList = [];
    _.each([editor, ...tagEditors], (editor) => {
      promiseList.push(editor.needSaveCurrentFile());
    })
    await Promise.all(promiseList);
    // actions.variable(actions.types.NOW_TIME, [], () => {
    //   return new Date().getTime()
    // })
    this.timerID = setTimeout(this.tick, 1000);
  }

  /**
   * 
   * @param {DrawerType} drawObj 
   * @param {Function} cb 
   * @param {boolean} only only open it or switch on/off
   * @param {boolean} refresh drawRefreshs add
   * @param {{}} dprops
   */
  openFunc = (drawObj, cb, only = false, refresh = false, dprops = {}) => {
    drawObj = drawObj === undefined ? Def : drawObj;
    if (only && !refresh && this.state.drawObj === drawObj) {
      return cb && cb();
    }
    this.setState((state) => {
      let ret = {
      }
      if (!only || state.drawObj !== drawObj) {
        _.assign(ret, {
          drawObj: state.drawObj !== drawObj ? drawObj : DrawerType.None,
          initWidth: verifyW(drawObj.w),
          dprops: dprops || {}
        })
      }
      if (refresh) {
        _.assign(ret, { drawRefreshs: 1 + state.drawRefreshs })
      }
      return ret;
    }, () => {
      this.clearSearch();
      cb && cb();
    });
  };

  clearSearch = () => {
    const { drawObj } = this.state;
    if (drawObj === DrawerType.None) {
      this.getEditor().clearSearch();
    }
  }

  safeModeFunc = async () => {
    const { safeMode } = this.state;
    commonData.setSafeMode(!safeMode);
  }

  readOnlyModeFunc = async () => {
    const { readOnlyMode } = this.state;
    commonData.setReadOnlyMode(!readOnlyMode);
  }

  selectFileFunc = (uploadFileCb) => {
    this.uploadFileCb = uploadFileCb;
    this.selectPlace = false;
    this.referFileKey = getFileKey(this.getEditor().getData().fileKey, FILESDIR);
    this.load = false;
    this.fileInputRef.current.click();
  }

  render() {
    const { drawObj, loading, loadingClosable, position, cb,
      safeMode, fullScreen, tagedEditor, tagEditors,
      isGraphxrApiSearchVisible, editorReadOnly,
      graphxrApiSearchBlockIndex
    } = this.state;
    const menu = <Menu forceSubMenuRender={true} className="data-html2canvas-ignore import-export-menu normal-icon">
      <Menu.Item key="file-import">
        <i className="icon fas fa-file-import"
          title={ShortcutTitle(file_actions_keys['Import Files'])} onClick={() => {
            this.fileInputRef.current.click();
          }}></i>
      </Menu.Item>
      <Menu.Item key="export-all">
        <i className="icon fas fa-download"
          title={ShortcutTitle(file_actions_keys['Download All Files'])} onClick={() => {
            actions.variable(actions.types.DOWNLOAD, [], () => {
              return true;
            })
          }}></i>
      </Menu.Item>
      <Menu.Item key="help">
        <i className={"icon far fa-question-circle"} title={ShortcutTitle(window_shortcuts_keys['Help?'])} onClick={() => {
          this.openFunc(DrawerType.Help)
        }}></i>
      </Menu.Item>
      <Menu.Item key="find-and-replace">
        <i className={"icon fas fa-search"} title={ShortcutTitle(code_editor_keys['Find and replace'])} onClick={() => {
          this.getEditor().setSearch(window.getSelection().toString());
          this.openFunc(DrawerType.FindAndReplace)
        }}></i>
      </Menu.Item>
      <Menu.Item key="open-settings">
        <i className={"icon fas fa-cog"} title={ShortcutTitle(window_shortcuts_keys['Open Settings Pane'])} onClick={() => {
          this.openFunc(DrawerType.Settings)
        }}></i>
      </Menu.Item>
    </Menu>
    return (
      <React.Fragment>
        <GraphxrApiSearchModal
          onCancel={() => {
            // Focus the block we were inside before opening the modal
            this.getEditor().focusBlockByIndex(graphxrApiSearchBlockIndex);
            this.setState({ graphxrApiSearchBlockIndex: undefined, isGraphxrApiSearchVisible: false })
          }}
          visible={isGraphxrApiSearchVisible}
        />
        <MyDropzone className="dropzone-div editor-dropzone" handleFilesFunc={(acceptedFiles) => {
          importFilesAction(acceptedFiles, true);
        }} accept={ExcpetImgAccept}>
          <ToolsSelect position={position} cb={cb} editor={this.getEditor()} selectFileFunc={this.selectFileFunc}></ToolsSelect>
          {<div className={`loading-top normal-icon ${loading ? "" : "hide"}`}>
            <div className='btns d-flex align-items-center'>
              <Button className={`${!safeMode ? "text-secondary" : "text-primary"}`} onClick={() => {
                this.setState(state => { return { safeMode: !state.safeMode } }, () => {
                  const { safeMode } = this.state;
                  commonData.setSafeMode(safeMode);
                })
                // window.location.href = window.location.href
              }} type="link" icon={<SafeModeIcon safeMode={safeMode} title={"Safe Mode"}></SafeModeIcon>}>Safe Mode</Button>
              {loadingClosable && <CloseOutlined onClick={() => {
                this.state.loading && this.setState({ loading: !this.state.loading });
              }} />}
            </div>
            <Spin spinning={true} size="large" tip="Loading...">
              <div className="full-div"></div>
            </Spin>
          </div>}
          <div className={`wrapper ${fullScreen ? "wrapper-full-screen" : ""}`}>
            <Affix className="top-affix" offsetTop={0}>
              <div>
                <div className="data-html2canvas-ignore">
                  {safeMode && <Alert
                    message={<div>
                      <span className="font-weight-bold"><i className="icon icon-xs far fa-life-ring"></i>&nbsp;
                        Safe mode enabled.</span>
                      <span className="d-none d-md-inline">
                        &nbsp;Use this code-only view to fix issues that might crash or hang the normal editor.
                      </span>
                    </div>
                    }
                    type="warning"
                    closable
                    onClose={this.safeModeFunc}
                  />}
                </div>
                <div className={`${!fullScreen ? "" : "page-head-full-screen"} page-head data-html2canvas-ignore`}>
                  <div className={`${!fullScreen ? "" : "hide"} d-flex justify-content-between align-items-center`}>
                    <div className="d-flex align-items-center">
                      <this.TitleComp />
                      {this.FileMenu()}
                    </div>
                    <div className="import-export normal-icon">
                      <div className="d-flex flex-rows">
                        <i className={`icon icon-bordered ${drawObj !== DrawerType.Panels ? "icon-inactive" : ""} fas fa-solar-panel`} title={ShortcutTitle(window_shortcuts_keys['Open Explore Pane'])} onClick={() => {
                          this.openFunc(DrawerType.Panels)
                        }}></i>
                        {!isNotLogin() && <i className={`icon icon-bordered ${drawObj !== DrawerType.Folder ? "icon-inactive" : ""} far ${drawObj !== DrawerType.Folder ? "fa-folder" : "fa-folder-open"}`}
                          title={ShortcutTitle(window_shortcuts_keys['Open File Explorer'])}
                          onClick={() => { this.openFunc(DrawerType.Folder) }}></i>}
                        <Dropdown overlay={menu} placement="bottomCenter" trigger={['click']}>
                          <EllipsisOutlined className="icon icon-bordered icon-mouseover" title="more" />
                        </Dropdown>
                      </div>
                    </div>
                  </div>
                  <div className="d-flex justify-content-between align-items-center">
                    <this.TickFunc />
                    <this.ChatComponent></this.ChatComponent>
                  </div>
                </div>
              </div>
            </Affix>
            <BottomTools editor={this.getEditor()} editorReadOnly={editorReadOnly} selectFileFunc={this.selectFileFunc}></BottomTools>
            <BackTop className="data-html2canvas-ignore ant-back-top-div" >
              <UpCircleOutlined className="ant-back-top-icon" />
            </BackTop>
            <div onDrop={this.onDropFunc}>
              <div className={`editorjs-wrapper ${editor.getTransferKey()} ${tagedEditor ? "hide" : ""}`}>
                <div className={`editorjs-anchor${/**!fullScreen ? "" : "-full-screen"*/""} data-html2canvas-ignore`}></div>
                <div className={`editorjs googoose-wrapper ${tagedEditor ? "hide" : ""}`}></div>
              </div>
              {_.map(tagEditors, (tagEditor) => {
                return <div key={tagEditor.getTransferKey()} className={`editorjs-wrapper ${tagEditor.getTransferKey()} ${tagedEditor === tagEditor ? "" : "hide"}`}>
                  <div className="editorjs-anchor data-html2canvas-ignore"></div>
                  <div className={`editorjs googoose-wrapper ${tagedEditor === tagEditor ? "" : "hide"}`}></div>
                </div>
              })}
            </div>
          </div>
        </MyDropzone>
        <this.DrawerComp></this.DrawerComp>
        <div className="file_import hide">
          <input ref={this.fileInputRef}
            className="file_input" multiple
            type="file"
            title="import files"
            accept={AllAccept}
            onChange={(e) => {
              let acceptedFiles = e.target.files;
              importFilesAction(acceptedFiles, false, this.selectPlace, this.referFileKey, this.load, (fileNames) => {
                this.uploadFileCb && this.uploadFileCb(fileNames);
                e.target.value = "";
                this.selectPlace = true;
                this.referFileKey = undefined;
                this.load = undefined;
                this.uploadFileCb = undefined;
              });
            }}
          >
          </input>
        </div>
      </React.Fragment >
    );
  }
}
