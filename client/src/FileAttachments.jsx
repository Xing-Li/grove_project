import { EllipsisOutlined } from '@ant-design/icons';
import { Card, Dropdown, List, Menu, Skeleton } from 'antd';
import PropTypes from "prop-types";
import React from 'react';
import { InsertIcon, ReplaceIcon } from "./block/code/Icon";
import EditorAbs from "./editorAbs";
import actions from "./define/Actions";
import { fileSeparator, getFolderKey, getFileAttachmentContent } from "./file/fileUtils";
import MyDropzone from './file/MyDropzone.jsx';
import { copyContent, ModalType, saveLink } from "./util/utils";
const { Meta } = Card;
const { DefaultFileData, DEFAULT_VER, FILESDIR_NAME } = require("./util/localstorage");
const { AllAccept, codeTime, formatSizeUnits, checkIsMobile, isFileImage, getFileIcon, isFileJson, ImageCache } = require("./util/helpers");

export default class FileAttachments extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        fileNamesJson: PropTypes.object.isRequired,
        fileData: PropTypes.object,
        attaches: PropTypes.object,
        editor: PropTypes.object,
    };
    static defaultProps = {};
    constructor(props) {
        super(props);
        /**@type {EditorAbs} */
        const editor = this.props.editor;
        const { className, inuseList, attaches } = this.props;
        /**@type {DefaultFileData} */
        const fileData = this.props.fileData;
        /**@type {Object.<string, DefaultFileData>} */
        const fileNamesJson = this.props.fileNamesJson;
        let folderKey = getFolderKey(fileData.fileKey, FILESDIR_NAME);
        const list = [];
        _.keys(fileNamesJson).filter(fileKey => {
            if (fileKey.startsWith(folderKey) && fileKey !== folderKey
                && !~fileKey.substring(folderKey.length).indexOf(fileSeparator)) {
                list.push(fileNamesJson[fileKey]);
            }
        })
        this.state = {
            editor,
            /**@type {Object.<string, DefaultFileData>} */
            fileNamesJson,
            fileData,
            /**@type {Array.<DefaultFileData>} */
            list,
            /**@type {Set} */
            attaches,
            editorReadOnly: editor.getReadOnly(),
        }
        this.ref = React.createRef();
        this.fileInputRef = React.createRef();
        this.currentEditorElement = document.createElement("div");
        this.currentEditorReadOnlyElement = document.createElement("div");
    }

    componentDidMount() {
        actions.inspector(this.currentEditorElement, [actions.types.CURRENT_EDITOR], /**@param {CommonEditor} currentEditor */(currentEditor) => {
            this.setState({ editorReadOnly: currentEditor.getReadOnly() });
        })
        actions.inspector(this.currentEditorReadOnlyElement, [actions.types.CURRENT_EDITOR_READ_ONLY], (currentEditorReadOnly) => {
            this.setState({ editorReadOnly: currentEditorReadOnly })
        });
    }

    componentWillUnmount() {
        actions.deleteCache(this.currentEditorElement);
        actions.deleteCache(this.currentEditorReadOnlyElement);
    }

    componentDidUpdate(preProps, preState) {
        let state = {};
        if (this.props.attaches !== preProps.attaches) {
            _.assign(state, { attaches: this.props.attaches });
        }
        if (this.props.fileNamesJson !== preProps.fileNamesJson) {
            _.assign(state, { fileNamesJson: this.props.fileNamesJson });
        }
        if (this.props.fileData !== preProps.fileData) {
            _.assign(state, { fileData: this.props.fileData });
        }
        if (preProps.editor !== this.props.editor) {
            /**@type {EditorAbs} */
            const editor = this.props.editor;
            _.assign(state, { editor });
        }
        !_.isEmpty(state) && this.setState(state, () => {
            if (state.fileData || (state.editor && state.editor.fileData)) {
                this.refreshList()
            }
        });
    }

    /**
     * 
     * @param {DefaultFileData} transItem 
     */
    insert = (transItem) => {
        const { editor } = this.state;
        let api = editor.editorInstance;
        let index = api.blocks.getCurrentBlockIndex();
        api.blocks.insert("codeTool", {
            "codeData": {
                "pinCode": true,
                "value": getFileAttachmentContent(editor, transItem)
            }
        }, undefined, index + 1, true);
        api.caret.setToBlock(index + 1, 'start');
        api.toolbar.rightOpen();
    }

    /**
     * 
     * @param {*} info 
     * @param {DefaultFileData} transItem 
     */
    menuClickHandler = async (info, transItem) => {
        const { editor } = this.state;
        switch (info.key) {
            case "insert":
                this.insert(transItem);
                break;
            case "copy":
                copyContent(getFileAttachmentContent(editor, transItem));
                break;
            case "remove":
                await editor.react_component.removeFileFunc(undefined,
                    editor.react_component.setWrapperLoading, transItem.fileKey);
                this.refreshList();
                window.currentEditor.removeFile(transItem.name);
                break;
        }
    }

    refreshList = () => {
        setTimeout(() => {
            const { fileData, fileNamesJson } = this.state;
            let folderKey = getFolderKey(fileData.fileKey, FILESDIR_NAME);
            const list = [];
            _.keys(fileNamesJson).filter(fileKey => {
                if (fileKey.startsWith(folderKey) && fileKey !== folderKey
                    && !~fileKey.substring(folderKey.length).indexOf(fileSeparator)) {
                    list.push(fileNamesJson[fileKey]);
                }
            })
            this.setState({ list })
        })
    }

    render() {
        const { list, attaches, fileData, editorReadOnly, editor } = this.state;
        const MenuFunc = /** @param {DefaultFileData} item */(item) => (<Dropdown arrow={true} overlay={
            <Menu onClick={(info) => {
                this.menuClickHandler(info, item)
            }}>
                {<Menu.Item key="insert" disabled={editorReadOnly}>
                    <InsertIcon className="icon"></InsertIcon>Insert into notebook
                </Menu.Item>}
                {<Menu.Item key="copy">
                    <i className={`icon fas fa-copy`}></i>Copy usage code
                </Menu.Item>}
                {<Menu.Item key="remove">
                    <i className="icon fas fa-trash"></i>Remove file
                </Menu.Item>}
            </Menu>
        } getPopupContainer={() => this.ref.current} trigger={['click']}>
            <EllipsisOutlined
                onClick={e => e.preventDefault()} className="icon" title="more" />
        </Dropdown>)
        return <div ref={this.ref} >
            <MyDropzone className="dropzone-div" input={false} multiple={true} handleFilesFunc={(acceptedFiles) => {
                const { editor } = this.state;
                editor.onImportFiles(acceptedFiles).then((values) => {
                    this.refreshList();
                })
            }}>
                <List.Item className="sub-title"
                    actions={[
                        <div onClick={() => {
                            this.fileInputRef.current.click()
                        }}>
                            <i title="import files" className="icon fas fa-plus"></i>
                        </div>]}
                >
                    <List.Item.Meta
                        avatar={<i className={`icon icon-inactive fas fa-paperclip`}></i>}
                        title={<div><div>File attachments</div><div className="small mid-gray">{getFolderKey(fileData.fileKey, FILESDIR_NAME)}</div></div>}
                    />
                </List.Item>
                <List
                    className="demo-loadmore-list"
                    itemLayout="horizontal"
                    dataSource={list.sort((a, b) => { return a.name > b.name ? -1 : 0 }).sort((a, b) => {
                        return attaches.has(a.name) ? -1 : (attaches.has(b.name) ? 1 : -1)
                    })}
                    renderItem={/**@param {DefaultFileData} item*/item => (
                        <List.Item
                            actions={[
                                attaches.has(item.name) ? <ReplaceIcon disabled={editorReadOnly}
                                    className="icon"
                                    title="Replace file"
                                    onClick={() => {
                                        const { editor } = this.state;
                                        editor.react_component.setState({
                                            modalType: ModalType.ReplaceFile, sourceFile: item, previewImg: null,
                                            modalData: {}
                                        })
                                    }}></ReplaceIcon> :
                                    <InsertIcon disabled={editorReadOnly} className="icon"
                                        title="Insert into notebook"
                                        onClick={() => {
                                            this.insert(item);
                                        }}></InsertIcon>,
                                <React.Fragment>{MenuFunc(item)}</React.Fragment>]}
                        >
                            <Skeleton avatar title={false} loading={item.loading} active>
                                <List.Item.Meta
                                    avatar={
                                        <i className={`icon ${getFileIcon(item)}`} onClick={() => {
                                            const { editor } = this.state;
                                            editor.switchFileTagEditor(item.fileKey);
                                        }} title={`${item.fileKey}`}></i>
                                    }
                                    title={<div className="cursorp"
                                        onClick={() => {
                                            const { editor } = this.state;
                                            editor.downloadFile(item.name);
                                        }}
                                        title={`download`}>{item.name}</div>}
                                    description={<div>{attaches.has(item.name) ? <span className="text-primary" title="Jump to defining cell" onClick={() => {
                                        const { editor } = this.state;
                                        editor.focusFileAttach(item.name);
                                    }}>In use</span> : "Unused"} • {formatSizeUnits(item.size)} • {codeTime(undefined, new Date(item.mtimeMs).getTime())}</div>}
                                />
                                <div></div>
                            </Skeleton>
                        </List.Item>
                    )}
                />
                <div className="file_import hide">
                    <input ref={this.fileInputRef}
                        className="file_input" multiple
                        type="file"
                        title="import files"
                        accept={AllAccept}
                        onChange={(e) => {
                            const { editor } = this.state;
                            let acceptedFiles = e.currentTarget.files;
                            if (0 === acceptedFiles.length) {
                                return;
                            }
                            editor.onImportFiles(acceptedFiles).then((values) => {
                                this.refreshList();
                            })
                        }}
                    >
                    </input>
                </div>
            </MyDropzone>
        </div>
    }
}