/* eslint-disable no-console */

import { createInterface } from "readline";
import open from "open";
import {
  rawConfig,
} from "./config";
import server  from "./server";
import { exit } from "./errors";
import _ from 'lodash';

export function start() {
  server();
}