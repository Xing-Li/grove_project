export default class DbClientVariable {

    constructor(editorjsKey, variantName, cb) {
        this.editorjsKey = editorjsKey;
        this.variantName = variantName;
        this.cb = (cb && cb.bind(this.array)) || (() => {
            let editor = this.editorjsKey === window.editor.getTransferKey() ? window.editor :
                _.filter(window.tagEditors, (tagEditor) => { return tagEditor.getTransferKey() === this.editorjsKey })[0];
            const { variables } = editor.getMain()._cachedata;
            let name = this.variantName
            let variable = variables[name]
            variable = !variable.redefines ? variable :
                _.assign({}, variable, variable.redefines[variable.redefines.length - 1])
            if (!variable.binded) {
                variable.func = variable.func.bind(variable.varia._value);
                variable.binded = true;
            }
            variable.varia.define(variable.name, variable.args, variable.func);
        });
    }

    /**
     * @returns {Array}
     */
    get array() {
        let editor = this.editorjsKey === window.editor.getTransferKey() ? window.editor :
            _.filter(window.tagEditors, (tagEditor) => { return tagEditor.getTransferKey() === this.editorjsKey })[0];
        const { variables } = editor.getMain()._cachedata;
        let name = this.variantName
        let variable = variables[name]
        variable = !variable.redefines ? variable :
            _.assign({}, variable, variable.redefines[variable.redefines.length - 1])
        if (undefined === variable.varia._value) {
            throw new Error("array not defined!");
        }
        return variable.varia._value;
    }
}