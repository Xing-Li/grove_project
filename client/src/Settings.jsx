import { EllipsisOutlined } from '@ant-design/icons';
import { Button, Cascader, Dropdown, List, Menu, Select, Switch, Table, Tabs } from 'antd';
import axios from 'axios';
import { InsertIcon } from 'block/code/Icon';
import moment from 'moment';
import { Buffer } from 'buffer';
import PropTypes from "prop-types";
import React from 'react';
import IntegerStep from './components/IntegerStep';
import Modes from './components/Modes';
import actions from './define/Actions';
import EditorAbs from './editorAbs';
import { Link } from './stdlib/link';
import { GUIDE_LINK_URI, USER_ID, USER_NAME } from './util/localstorage';
import { v4 as uuidv4, NIL as NIL_UUID } from 'uuid';
// import { createHmac, timingSafeEqual } from "crypto";
import databaseClient from 'stdlib/databaseClient';

const { ShareProjectCanOptions } = require('./util/MsgType');
const { getTestSql, showToast, copyContent, ModalType, showConfirm, NoticeOptions, ConnectionHostedByType, showMessage, ShortDatabaseType, ReadWriteOptions } = require("./util/utils");
const { DefaultDatabase, DefaultSecret } = require("./util/databaseApi");
const { Option } = Select;
const { TabPane } = Tabs;
const Options = ["default", "3024-day", "3024-night", "abcdef", "ambiance", "ayu-dark", "ayu-mirage", "base16-dark", "base16-light", "bespin", "blackboard", "cobalt", "colorforth", "darcula", "dracula", "duotone-dark", "duotone-light", "eclipse", "elegant", "erlang-dark", "gruvbox-dark"]
export default class Settings extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        projectId: PropTypes.string,
        projectName: PropTypes.string,
        userId: PropTypes.string,
        editor: PropTypes.object,
    };
    static defaultProps = {};

    constructor(props) {
        super(props);
        /**@type {EditorAbs} */
        const editor = this.props.editor;
        this.state = {
            editor,
            className: this.props.className,
            projectId: this.props.projectId,
            projectName: this.props.projectName,
            tabActiveKey: this.props.tabActiveKey || "General",
            userId: this.props.userId,
            theme: editor.getSettings().getTheme(),
            currentFileReadOnly: editor.getReadOnly(),
            readwrite_mode: editor.getSettings().getReadwrite_mode(),
            autoSaveInSecond: editor.getSettings().getAutoSaveInSecond(),
            autoSave: editor.getSettings().getAutoSave(),
            vimEnabled: editor.getSettings().getVimEnabled(),
            darkMode: editor.getSettings().getDarkMode(),
            autoFormat: editor.getSettings().getAutoFormat(),
            mask: editor.getSettings().getMask(),
            /**@type {Array.<DefaultDatabase>} */
            databases: editor.getSettings().getDatabases(),
            /**@type {Array.<DefaultSecret>} */
            secrets: editor.getSettings().getSecrets(),
            notice_mode: editor.getSettings().getNotice_mode(),
            usersSharePermission: editor.getSettings().getUsersSharePermission(),
            /**@type Object.<string, ShapeChatData> */
            projectShareUsers: {},
        }
        this.ref = React.createRef();
        this.themeElement = document.createElement("div");
        this.currentEditorElement = document.createElement("div");
        this.currentEditorReadOnlyElement = document.createElement("div");
        this.readwrite_modeElement = document.createElement("div");
        this.autoSaveInSecondElement = document.createElement("div");
        this.autoSaveElement = document.createElement("div");
        this.vimEnabledElement = document.createElement("div");
        this.darkModeElement = document.createElement("div");
        this.autoFormatElement = document.createElement("div");
        this.maskElement = document.createElement("div");
        this.databasesElement = document.createElement("div");
        this.secretsElement = document.createElement("div");
        this.notice_modeElement = document.createElement("div");
        this.sharePermissionElement = document.createElement("div");
        this.projectShareUsersElement = document.createElement("div");
    }

    componentDidMount() {
        actions.inspector(this.themeElement, [actions.types.THEME],
            (theme) => { this.setState({ theme }); })
        actions.inspector(this.currentEditorElement, [actions.types.CURRENT_EDITOR],  /**@param {CommonEditor} currentEditor */(currentEditor) => {
            this.setState({ currentFileReadOnly: currentEditor.getReadOnly() });
        })
        actions.inspector(this.currentEditorReadOnlyElement, [actions.types.CURRENT_EDITOR_READ_ONLY], (currentEditorReadOnly) => {
            this.setState({ currentFileReadOnly: currentEditorReadOnly })
        });
        actions.inspector(this.readwrite_modeElement, [actions.types.READWRITE_MODE],
            (readwrite_mode) => { this.setState({ readwrite_mode }); })
        actions.inspector(this.autoSaveInSecondElement, [actions.types.AUTO_SAVE_IN_SECOND],
            (autoSaveInSecond) => { this.setState({ autoSaveInSecond }); })
        actions.inspector(this.autoSaveElement, [actions.types.AUTO_SAVE],
            (autoSave) => { this.setState({ autoSave }); })
        actions.inspector(this.vimEnabledElement, [actions.types.VIM_ENABLED],
            (vimEnabled) => { this.setState({ vimEnabled }); })
        actions.inspector(this.darkModeElement, [actions.types.DARK_MODE],
            (darkMode) => {
                const { editor } = this.state;
                this.setState({ darkMode });
                editor.getSettings().setDarkMode(darkMode);
                // It's impossible to persist the code theme if we overwrite it here
            })
        actions.inspector(this.autoFormatElement, [actions.types.AUTO_FORMAT],
            (autoFormat) => { this.setState({ autoFormat }); })
        actions.inspector(this.maskElement, [actions.types.MASK],
            (mask) => { this.setState({ mask }); })
        actions.inspector(this.databasesElement, [actions.types.DATABASES],
            (databases) => { this.setState({ databases }); })
        actions.inspector(this.secretsElement, [actions.types.SECRETS],
            (secrets) => { this.setState({ secrets }); })
        actions.inspector(this.notice_modeElement, [actions.types.NOTICE_MODE],
            (notice_mode) => { this.setState({ notice_mode }); })
        actions.inspector(this.sharePermissionElement, [actions.types.USERS_SHARE_PERMISSION],
            (usersSharePermission) => { this.setState({ usersSharePermission }); })
        actions.inspector(this.projectShareUsersElement, [actions.types.PROJECT_SHARE_USERS],
            (projectShareUsers) => { this.setState({ projectShareUsers }); })
    }

    componentWillUnmount() {
        actions.deleteCache(this.themeElement);
        actions.deleteCache(this.currentEditorElement);
        actions.deleteCache(this.currentEditorReadOnlyElement);
        actions.deleteCache(this.readwrite_modeElement);
        actions.deleteCache(this.autoSaveInSecondElement);
        actions.deleteCache(this.autoSaveElement);
        actions.deleteCache(this.vimEnabledElement);
        actions.deleteCache(this.darkModeElement);
        actions.deleteCache(this.autoFormatElement);
        actions.deleteCache(this.maskElement);
        actions.deleteCache(this.databasesElement);
        actions.deleteCache(this.secretsElement);
        actions.deleteCache(this.notice_modeElement);
        actions.deleteCache(this.sharePermissionElement);
        actions.deleteCache(this.projectShareUsersElement);
    }

    componentDidUpdate(preProps, preState) {
        let state = {};
        if (preProps.className !== this.props.className) {
            _.assign(state, { className: this.props.className });
        }
        if (preProps.projectId !== this.props.projectId) {
            _.assign(state, { projectId: this.props.projectId });
        }
        if (preProps.projectName !== this.props.projectName) {
            _.assign(state, { projectName: this.props.projectName });
        }
        if (preProps.tabActiveKey !== this.props.tabActiveKey) {
            _.assign(state, { tabActiveKey: this.props.tabActiveKey });
        }
        if (preProps.userId !== this.props.userId) {
            _.assign(state, { userId: this.props.userId });
        }
        if (preProps.editor !== this.props.editor) {
            /**@type {EditorAbs} */
            const editor = this.props.editor;
            _.assign(state, { editor });
        }
        !_.isEmpty(state) && this.setState(state);
    }

    handleChange = (value) => {
        actions.variable(actions.types.THEME, [], () => {
            return value;
        })
        const { editor } = this.state;
        editor.getSettings().setTheme(value);
        editor.saveSettings(true);
    }

    General = () => {
        const { className, projectName, userId, theme, currentFileReadOnly, readwrite_mode,
            notice_mode, usersSharePermission, autoSave, autoSaveInSecond, darkMode, vimEnabled,
            autoFormat, mask, projectShareUsers, editor } = this.state;
        const defaultValue = [];
        const options =
            _.reduce(projectShareUsers, (prev, user, userId, obj) => {
                if (USER_ID === userId) {
                    defaultValue.push(userId);
                }
                let entry = {
                    value: userId,
                    label: `${USER_ID === userId ? "You" : user.userName}`,
                    children: (_.reduce(ShareProjectCanOptions, (prev, curr, key) => {
                        if (USER_ID === userId && usersSharePermission[userId] === curr) {
                            defaultValue.push(curr);
                        }
                        let entry = {
                            value: curr,
                            label: (usersSharePermission[userId] === curr) ? `•${key}` : ` ${key}`,
                        }
                        prev.push(entry);
                        return prev;
                    }, [])).sort((a, b) => { return a.value - b.value }),
                }
                prev.push(entry);
                return prev;
            }, [])
        return <div>
            <div className="small mid-gray">Settings shared across current Project [{projectName}].</div>
            <ul className="list-group list-group-flush">
                <li className="list-group-item">
                    <div className="d-flex justify-content-between align-items-center"><div>Code Theme:</div><Select getPopupContainer={() => this.ref.current} value={theme} style={{ width: 120 }}
                        onChange={this.handleChange}>
                        {
                            _.map(Options, (option, index) => {
                                return <Option key={option} value={option}>{option}</Option>
                            })
                        }
                    </Select></div>
                </li>
                <li className="list-group-item">
                    <div className="d-flex justify-content-between align-items-center"><div>Dark Mode</div><Modes size="small" onChange={(checked) => {
                        actions.variable(actions.types.DARK_MODE, [], () => {
                            return checked;
                        })
                    }} checkedChildren="ON" unCheckedChildren="OFF" checked={darkMode} />
                    </div>
                </li>
                <li className="list-group-item">
                    <div className="d-flex justify-content-between align-items-center"><div>AutoSave:</div><Switch size="small" onChange={(checked, e) => {
                        actions.variable(actions.types.AUTO_SAVE, [], () => {
                            return checked;
                        })
                        if (checked && editor.getFileData() && editor.react_component.isNeedUpload(editor.getFileData())) {
                            editor.setNeedSave(true);
                        }
                        editor.getSettings().setAutoSave(checked);
                        editor.saveSettings(true);
                    }} checkedChildren="ON" unCheckedChildren="OFF" checked={autoSave} />
                    </div>
                    <div>
                        Auto Save Period(sec): <IntegerStep min={5} max={60} inputValue={autoSaveInSecond} changeFunc={(value) => {
                            actions.variable(actions.types.AUTO_SAVE_IN_SECOND, [], () => {
                                return parseInt(value);
                            })
                            editor.setCountSecond(0);
                            editor.getSettings().setAutoSaveInSecond(parseInt(value));
                            editor.saveSettings(true);
                        }}></IntegerStep>
                    </div>
                </li>
                <li className="list-group-item">
                    <div className="d-flex justify-content-between align-items-center">
                        <div>Block Display:</div>
                        <Select getPopupContainer={() => this.ref.current} value={readwrite_mode} style={{ width: 120 }}
                            onChange={(value) => {
                                actions.variable(actions.types.READWRITE_MODE, [], () => {
                                    return value;
                                })
                                editor.getSettings().setReadwriteMode(value);
                                editor.refreshViewCode();
                                editor.saveSettings(true);
                            }}>
                            {
                                _.map(ReadWriteOptions, (option, index) => {
                                    return <Option key={option} value={option}>{option}</Option>
                                })
                            }
                        </Select>
                    </div>
                </li>
                <li className="list-group-item">
                    <div className="d-flex justify-content-between align-items-center"><div>Auto Format</div><Switch size="small" onChange={(checked, e) => {
                        actions.variable(actions.types.AUTO_FORMAT, [], () => {
                            return checked;
                        })
                        editor.getSettings().setAutoFormat(checked);
                        editor.saveSettings(true);
                    }} checkedChildren="ON" unCheckedChildren="OFF" checked={autoFormat} />
                    </div>
                </li>
                <li className="list-group-item">
                    <div className="d-flex justify-content-between align-items-center"><div>VIM</div><Switch size="small" onChange={(checked, e) => {
                        actions.variable(actions.types.VIM_ENABLED, [], () => {
                            return checked;
                        })
                        editor.getSettings().setVimEnabled(checked);
                        editor.saveSettings(true);
                    }} checkedChildren="ON" unCheckedChildren="OFF" checked={vimEnabled} />
                    </div>
                </li>
                <li className="list-group-item">
                    <div className="d-flex justify-content-between align-items-center"><div>Mask</div><Switch size="small" onChange={(checked, e) => {
                        actions.variable(actions.types.MASK, [], () => {
                            return checked;
                        })
                        editor.getSettings().setMask(checked);
                        editor.saveSettings(true);
                    }} checkedChildren="ON" unCheckedChildren="OFF" checked={mask} />
                    </div>
                </li>
                <li className="list-group-item">
                    <div className="d-flex justify-content-between align-items-center">
                        <div>Notice Level:</div>
                        <Select getPopupContainer={() => this.ref.current} value={notice_mode} style={{ width: 120 }}
                            onChange={(value) => {
                                actions.variable(actions.types.NOTICE_MODE, [], () => {
                                    return value;
                                })
                                editor.getSettings().setNotice_mode(value);
                                editor.saveSettings(true);
                            }}>
                            {
                                _.map(NoticeOptions, (option, index) => {
                                    return <Option key={option} value={option}>{option}</Option>
                                })
                            }
                        </Select>
                    </div>
                </li>
                {options.length > 0 && <li className="list-group-item">
                    <div className="d-flex justify-content-between align-items-center">
                        <div>Share Permission Level:</div>
                        <Cascader key={defaultValue.toString()} disabled={userId !== USER_ID}
                            defaultValue={defaultValue}
                            options={options}
                            expandTrigger="hover"
                            onChange={(value) => {
                                if (value.length !== 2) {
                                    return;
                                }
                                const { usersSharePermission } = this.state;
                                usersSharePermission[value[0]] = value[1];
                                actions.variable(actions.types.USERS_SHARE_PERMISSION, [], () => {
                                    return usersSharePermission;
                                })
                                editor.getSettings().setUsersSharePermission(usersSharePermission);
                                editor.saveSettings(true);
                            }}
                        />
                    </div>
                </li>}
            </ul>
        </div >
    }

    /**
     * 
     * @param {DefaultDatabase|DefaultSecret} transItem 
     */
    insert = (transItem) => {
        const { editor } = this.state;
        let api = editor.editorInstance;
        let index = api.blocks.getCurrentBlockIndex();
        api.blocks.insert("codeTool", {
            "codeData": {
                "pinCode": true,
                "value": this.getContent(transItem)
            }
        }, undefined, index + 1, true);
        api.caret.setToBlock(index + 1, 'start');
        api.toolbar.rightOpen();
    }

    /**
     * 
     * @param {DefaultDatabase|DefaultSecret} transItem  
     */
    getContent = (transItem) => {
        if (undefined !== transItem.type) {
            return `DatabaseClient(\"${transItem.name}\")`;
        } else {
            return `Secret(\"${transItem.name}\")`;
        }
    }
    /**
       * 
       * @param {DefaultDatabase} transItem 
       */
    genSecret(transItem) {
        let secret = {
            "name": transItem.name,
            "origin": location.origin,
            "type": ShortDatabaseType[transItem.type],
            "host": transItem.host,
            "port": transItem.port,
            "ssl": transItem.sslTls ? "required" : "disabled",
            "secret": uuidv4()
        };
        return Buffer.from(JSON.stringify(secret)).toString("base64");
    }

    /**
     * 
     * @param {DefaultDatabase} transItem 
     */
    resetSecret(transItem) {
        transItem.secret = this.genSecret(transItem);
        copyContent(transItem.secret, () => {
            showMessage("reset secret success!", "success")
        });
    }

    /**
     * 
     * @param {*} info 
     * @param {DefaultDatabase} transItem 
     */
    menuClickHandler = async (info, transItem) => {
        const { editor } = this.state;
        switch (info.key) {
            case "insert":
                this.insert(transItem);
                break;
            case "copy":
                copyContent(this.getContent(transItem));
                break;
            case "edit":
                editor.react_component.setState({
                    modalType: ModalType.NewDatabase,
                    modalData: {
                        database: transItem,
                        databases: editor.getSettings().getDatabases(),
                        cb:
                            /**
                             * 
                             * @param {DefaultDatabase} newDatabase 
                             */
                            (newDatabase) => {
                                let databases = editor.getSettings().getDatabases();
                                let target = _.filter(databases, (database) => {
                                    return database.name === newDatabase.name
                                })[0];
                                if (!target) {
                                    showToast("Name Can Not Find!", 'error');
                                }
                                if (_.isEqual(newDatabase, target)) {
                                    return;
                                }
                                databases.splice(databases.indexOf(target), 1);
                                if (newDatabase.connectionHostedBy === ConnectionHostedByType['Self-hosted proxy']) {
                                    this.resetSecret(newDatabase);
                                }
                                databases.push(newDatabase);
                                actions.variable(actions.types.DATABASES, [], () => {
                                    return databases;
                                })
                                editor.getSettings().setDatabases(databases)
                                editor.saveSettings(true);
                            }
                    },
                })
                break;
            case "remove":
                showConfirm(<div>Are you sure you want to remove <span className="text-primary">{transItem.name}</span>?</div>,
                    () => {
                        /**@type{[]} */
                        let databases = editor.getSettings().getDatabases();
                        databases.splice(databases.indexOf(transItem), 1);
                        actions.variable(actions.types.DATABASES, [], () => {
                            return databases;
                        })
                        editor.getSettings().setDatabases(databases)
                        editor.saveSettings(true);
                    }, undefined, { okType: 'danger', okText: "Remove" }
                );
                break;
            case "test":
                this.onTest(transItem);
                break;
            case "copySecret":
                copyContent(transItem.secret)
                break;
            case "resetSecret":
                let databases = editor.getSettings().getDatabases();
                this.resetSecret(transItem);
                actions.variable(actions.types.DATABASES, [], () => {
                    return databases;
                })
                editor.getSettings().setDatabases(databases)
                editor.saveSettings(true);
                break;
        }
    }
    /**
     * 
     * @param {*} info 
     * @param {DefaultSecret} transItem 
     */
    secretMenuClickHandler = async (info, transItem) => {
        const { editor } = this.state;
        switch (info.key) {
            case "insert":
                this.insert(transItem);
                break;
            case "copy":
                copyContent(this.getContent(transItem));
                break;
            case "edit":
                editor.react_component.setState({
                    modalType: ModalType.NewSecret,
                    modalData: {
                        secret: transItem,
                        cb:
                            /**
                             * 
                             * @param {DefaultSecret} newSecret 
                             */
                            (newSecret) => {
                                let secrets = editor.getSettings().getSecrets();
                                let target = _.filter(secrets, (database) => {
                                    return database.name === newSecret.name
                                })[0];
                                if (!target) {
                                    return showToast("Name Can Not Find!", 'error');
                                }
                                secrets.splice(secrets.indexOf(target), 1);
                                secrets.push(newSecret);
                                actions.variable(actions.types.SECRETS, [], () => {
                                    return secrets;
                                })
                                editor.getSettings().setSecrets(secrets)
                                editor.saveSettings(true);
                            }
                    },
                })
                break;
            case "remove":
                showConfirm(<div>Are you sure you want to remove <span className="text-primary">{transItem.name}</span>?</div>,
                    () => {
                        /**@type{[]} */
                        let secrets = editor.getSettings().getSecrets();
                        secrets.splice(secrets.indexOf(transItem), 1);
                        actions.variable(actions.types.SECRETS, [], () => {
                            return secrets;
                        })
                        editor.getSettings().setSecrets(secrets)
                        editor.saveSettings(true);
                    }, undefined, { okType: 'danger', okText: "Remove" }
                );
                break;
        }
    }

    /**
     * 
     * @param {DefaultDatabase} dbInfo 
     */
    async onTest(dbInfo) {
        if (dbInfo.connectionHostedBy === ConnectionHostedByType["Self-hosted proxy"]) {
            // let payload = Buffer.from(JSON.stringify({ owner: USER_NAME, name: dbInfo.name, type: ShortDatabaseType[dbInfo.type], origin: location.origin, exp: new Date().getTime() + 60 * 60 * 1000 }))
            if (!dbInfo.secret) {
                return showToast("Init Error", 'warning')
            }
            // let secret = JSON.parse(Buffer.from(dbInfo.secret, "base64")).secret;
            // let hmac = createHmac("sha256", Buffer.from(secret, "hex"))
            //     .update(payload)
            //     .digest()
            let headers = {} //{ Authorization: `Bearer ${payload.toString("base64")}.${hmac.toString("base64")}` }
            let result = await axios.post(`${dbInfo.sslTls ? "https" : "http"}://${dbInfo.proxyHost}:${dbInfo.port}${dbInfo.location || ""}`, { sql: getTestSql(dbInfo) }, { headers: headers })
                .then(
                    (res) => {
                        return res;
                    },
                    (e) => {
                        console.error(e)
                        return { status: 1, error: e };
                    }
                );
            if (result.status == 200) {
                if (!result.data || !result.data.data) {
                    return showMessage(result.data && result.data.error || result.data || "Have no results!", 'warning');
                }
                showMessage("Connection Success", 'success')
            } else {
                showToast(result.error && JSON.stringify(result.error) || "Connection Failed", 'warning')
            }
        } else if (dbInfo.connectionHostedBy === ConnectionHostedByType.Grove) {
            let params = { ...dbInfo };
            params["sql"] = getTestSql(dbInfo);
            let result = await axios.post(`/api/${ShortDatabaseType[dbInfo.type]}/query`, params).then(
                (res) => {
                    return res.data;
                },
                (e) => {
                    console.error(e)
                    return { status: 1, error: e };
                }
            );

            if (result.status == 0) {
                if (!result.content || !result.content.results) {
                    return showMessage(result.content && result.content.error || result.content || "Have no results!", 'warning');
                }
                showMessage("Connection Success", 'success')
            } else {
                showToast(result.error && JSON.stringify(result.error) || "Connection Failed", 'warning')
            }
        } else {
            let client = databaseClient(dbInfo.name);
            let result = await client.query(getTestSql(dbInfo)).then(
                (res) => {
                    return { status: 0, result: res };
                },
                (e) => {
                    console.error(e)
                    return { status: 1, error: e };
                }
            );
            if (result.status == 0) {
                showMessage("Connection Success", 'success')
            } else {
                showToast(result.error && JSON.stringify(result.error) || "Connection Failed", 'warning')
            }
        }
    }

    Databases = () => {
        const { databases, currentFileReadOnly, projectName, editor } = this.state;
        const MenuFunc = /** @param {DefaultDatabase} item */(item) => (<Dropdown arrow={true} overlay={
            <Menu onClick={(info) => {
                this.menuClickHandler(info, item)
            }}>
                {<Menu.Item key="insert" disabled={currentFileReadOnly}>
                    <InsertIcon className="icon"></InsertIcon>Insert into notebook
                </Menu.Item>}
                {<Menu.Item key="test">
                    <i className="iconfont icon icon-icon-link"></i>Test connection
                </Menu.Item>}
                {<Menu.Item key="copy">
                    <i className="icon fas fa-copy"></i>Copy usage code
                </Menu.Item>}
                {<Menu.Item key="edit">
                    <i className="icon fas fa-pen"></i>Edit
                </Menu.Item>}
                {<Menu.Item key="remove">
                    <i className="icon fas fa-trash"></i>Remove
                </Menu.Item>}
                {item.connectionHostedBy === ConnectionHostedByType['Self-hosted proxy'] && <Menu.Item key="resetSecret">
                    <i className="icon fas fa-redo"></i>Reset secret
                </Menu.Item>}
                {item.connectionHostedBy === ConnectionHostedByType['Self-hosted proxy'] && <Menu.Item key="copySecret">
                    <i className="icon fas fa-copy"></i>Copy secret
                </Menu.Item>}
            </Menu>
        } getPopupContainer={() => this.ref.current} trigger={['click']}>
            <EllipsisOutlined
                onClick={e => e.preventDefault()} className="icon" title="more" />
        </Dropdown>)
        const columns = [
            {
                title: 'Name',
                dataIndex: 'name',
                // width: 150,
                ellipsis: true,
            },
            {
                title: 'Type',
                dataIndex: 'type',
                sorter: (a, b) => a > b,
            },
            {
                title: 'Action',
                dataIndex: 'action',
            },
        ];

        const data = _.map(databases, (value, index) => {
            return {
                key: value.name,
                name: <div>
                    <div> {value.name} </div>
                    <small className="small-limit"> {value.connectionHostedBy === ConnectionHostedByType['Self-hosted proxy'] ?
                        (value.proxyHost ? `${value.sslTls ? "https" : "http"}://${value.proxyHost}:${value.port}${value.location || ""}` : "")
                        : (value.host ? `${value.user}@${value.host}:${value.port}${`/${value.database || value.schema || ''}`}` : "")} </small>
                </div>
                ,
                type: <div>
                    <div>{value.type}</div>
                    <div>{value.connectionHostedBy}</div>
                </div>,
                action: <div><InsertIcon disabled={currentFileReadOnly} className="icon"
                    title="Insert into notebook"
                    onClick={() => {
                        this.insert(value)
                    }}></InsertIcon>
                    {MenuFunc(value)}</div>,
            }
        });
        return <div>
            <div className="small mid-gray">You can query PostgreSQL, MySQL, BigQuery and Snowflake databases from current project [{projectName}]'s notebooks with DatabaseClient("dbname").</div>
            <List.Item className="sub-title"
                actions={[
                    <Button onClick={() => {
                        editor.react_component.setState({
                            modalType: ModalType.NewDatabase,
                            modalData: {
                                databases: editor.getSettings().getDatabases(),
                                cb:
                                    /**
                                     * 
                                     * @param {DefaultDatabase} newDatabase 
                                     */
                                    (newDatabase) => {
                                        let databases = editor.getSettings().getDatabases();
                                        if (_.filter(databases, (database) => {
                                            return database.name === newDatabase.name
                                        }).length > 0) {
                                            return showToast("Name Can Not Be Repeated!", 'error');
                                        }
                                        newDatabase.secret = this.genSecret(newDatabase);
                                        databases.push(newDatabase);
                                        actions.variable(actions.types.DATABASES, [], () => {
                                            return databases;
                                        })
                                        editor.getSettings().setDatabases(databases)
                                        editor.saveSettings(true);
                                    }
                            },
                        })
                    }}>New Database</Button>
                ]}
            >
                <List.Item.Meta
                    avatar={<i className={`icon icon-inactive fas fa-database`}></i>}
                />
            </List.Item>
            <Table size='small' columns={columns} dataSource={data} onChange={(pagination, filters, sorter, extra) => {
                // console.log('params', pagination, filters, sorter, extra);
            }} />
            <i className="icon fas fa-info-circle"></i><Link fileKey={`${GUIDE_LINK_URI}databases/databases`} text="Learn more about databases"></Link>
        </div>
    }

    Secrets = () => {
        const { secrets, currentFileReadOnly, projectName, editor } = this.state;
        const MenuFunc = /** @param {DefaultSecret} item */(item) => (<Dropdown arrow={true} overlay={
            <Menu onClick={(info) => {
                this.secretMenuClickHandler(info, item)
            }}>
                {<Menu.Item key="insert" disabled={currentFileReadOnly}>
                    <InsertIcon className="icon"></InsertIcon>Insert into notebook
                </Menu.Item>}
                {<Menu.Item key="copy">
                    <i className="icon fas fa-copy"></i>Copy usage code
                </Menu.Item>}
                {<Menu.Item key="edit">
                    <i className="icon fas fa-pen"></i>Edit
                </Menu.Item>}
                {<Menu.Item key="remove">
                    <i className="icon fas fa-trash"></i>Remove
                </Menu.Item>}
            </Menu>
        } getPopupContainer={() => this.ref.current} trigger={['click']}>
            <EllipsisOutlined
                onClick={e => e.preventDefault()} className="icon" title="more" />
        </Dropdown>)
        const columns = [
            {
                title: 'SECRET NAME',
                dataIndex: 'name',
                sorter: (a, b) => a > b,
            },
            {
                title: 'DATE',
                dataIndex: 'createTime',
            },
            {
                title: 'Action',
                dataIndex: 'action',
            },
        ];

        const data = _.map(secrets, (value, index) => {
            return {
                key: value.name,
                name: value.name,
                createTime: <div>{value.createTime && moment(new Date(value.createTime)).format("MMM D,YYYY")}</div>,
                action: <div><InsertIcon disabled={currentFileReadOnly} className="icon"
                    title="Insert into notebook"
                    onClick={() => {
                        this.insert(value)
                    }}></InsertIcon>
                    {MenuFunc(value)}</div>,
            }
        });
        return <div>
            <div className="small mid-gray">Secrets can be accessed by current project [{projectName}]'s notebooks using the Secret("KEY_NAME") function. Use secrets to store API keys and other sensitive values you don’t want to be public.</div>
            <List.Item className="sub-title"
                actions={[
                    <Button onClick={() => {
                        editor.react_component.setState({
                            modalType: ModalType.NewSecret,
                            modalData: {
                                cb:
                                    /**
                                     * 
                                     * @param {DefaultSecret} newSecret 
                                     */
                                    (newSecret) => {
                                        let secrets = editor.getSettings().getSecrets();
                                        if (_.filter(secrets, (database) => {
                                            return database.name === newSecret.name
                                        }).length > 0) {
                                            return showToast("Name Can Not Be Repeated!", 'error');
                                        }
                                        newSecret.createTime = new Date().getTime();
                                        secrets.push(newSecret);
                                        actions.variable(actions.types.SECRETS, [], () => {
                                            return secrets;
                                        })
                                        editor.getSettings().setSecrets(secrets)
                                        editor.saveSettings(true);
                                    }
                            },
                        })
                    }}>New Secret</Button>
                ]}
            >
                <List.Item.Meta
                    avatar={<i className={`icon icon-inactive fas fa-key`}></i>}
                />
            </List.Item>
            <Table size='small' columns={columns} dataSource={data} onChange={(pagination, filters, sorter, extra) => {
                // console.log('params', pagination, filters, sorter, extra);
            }} />
            <i className="icon fas fa-info-circle"></i><Link fileKey={`${GUIDE_LINK_URI}secrets/secrets`} text="Learn more about secrets"></Link>
        </div>
    }

    render() {
        const { className, tabActiveKey } = this.state;
        return <div ref={this.ref} className={`settings ${className || ''}`} style={{ paddingTop: "32px" }}>
            <Tabs activeKey={tabActiveKey} tabPosition={"top"} style={{ height: "auto" }} onChange={(activeKey) => {
                this.setState({ tabActiveKey: activeKey });
            }}>
                <TabPane tab={`General`} key={`General`}>
                    <this.General></this.General>
                </TabPane>
                <TabPane tab={`Databases`} key={`Databases`}>
                    <this.Databases></this.Databases>
                </TabPane>
                <TabPane tab={`Secrets`} key={`Secrets`}>
                    <this.Secrets></this.Secrets>
                </TabPane>
            </Tabs>
            <div className="ph3 mt4">
                <div className="fw6">About</div>
                <div className="mt2 mb3 f6 fw5 mid-gray">Beta Version:&nbsp;&nbsp;{process.env.REACT_APP_VERSION}</div>
            </div>
        </div >
    }
}