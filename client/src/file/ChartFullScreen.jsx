import {
    Button, Select, Space, Form,
    Input,
    Radio,
    Cascader,
    DatePicker,
    InputNumber,
    TreeSelect,
    Switch,
    Row,
    Col,
} from 'antd';
import Text from 'antd/lib/typography/Text';
import axios from 'axios';
import _ from 'lodash';
import PropTypes from "prop-types";
import React from 'react';
import Modal from 'antd/lib/modal/Modal';
import { WIDTH, Z_INDEX, Title } from './fileUtils.js';
import { ModalType, showToast } from '../util/utils.js';
import AntChartApart from 'block/antchart/AntChartApart';

const { Option } = Select;
export default class ChartFullScreen extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        react_component: PropTypes.object,
    };
    static defaultProps = {};

    constructor(props) {
        super(props);
        /**@type {import('./MdFiles').default}*/
        const react_component = this.props.react_component;
        const { chartConfig, chartEditor } = react_component.state.modalData;
        this.state = {
            modalName: chartConfig.options.title ? chartConfig.options.title.text : "    ",
        };
    }

    componentDidMount() {
    }

    componentDidUpdate(prevProps, prevState) {
    }

    render() {
        const { className } = this.props;
        const { name, value, modalName } = this.state;
        /**@type {import('./MdFiles').default}*/
        const react_component = this.props.react_component;
        const { modalType } = react_component.state;
        const { chartConfig, chartEditor, result, fields } = react_component.state.modalData;
        const closeFunc = () => {
            react_component.setState({
                modalType: ModalType.None,
            });
        };
        return <Modal zIndex={Z_INDEX}
            className={`modal-comp data-html2canvas-ignore ${className}`}
            width={"100vw"}
            title={null}
            visible={modalType !== ModalType.None}
            onCancel={closeFunc}
            footer={null}
        >
            <div style={{
                width: "100%",
                height: "80vh",
                overflowY: 'auto'
            }}>
                <AntChartApart result={result} fields={fields} darkMode={window.editor && window.editor.getSettings().getDarkMode()} reload={false} chartEditor={chartEditor} chartConfig={chartConfig} currentDataGrid={{ w: 12, h: 12 }} fullscreen={true}></AntChartApart>
            </div>
        </Modal>
    }
}
