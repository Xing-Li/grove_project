import { groveComponent } from "./groveComponent";
import { Td } from "./Td";
import { Th } from "./Th";

export const Tr = groveComponent({ name: 'tr', children: [Td, Th] })