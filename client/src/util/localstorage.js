import _ from 'lodash';
import { DefaultData } from '../block/data';
import { message } from 'antd';
import { SelectType } from './helpers';
const { UserStatus, ShareProjectCanOptions } = require('./MsgType');

/**current user's email */
export const USER_EMAIL = window.opener.globalVariable.email;
/**admin's email */
export const ADMIN_EMAIL = window.opener.globalVariable.adminEmail;
/**check if is admin */
export const IS_ADMIN = USER_EMAIL && ADMIN_EMAIL === USER_EMAIL;
/**current user's name */
export const USER_NAME = `${window.opener.globalVariable.firstName} ${window.opener.globalVariable.lastName}`;
/**current user's id */
export const USER_ID = window.opener.globalVariable._id;
/**current project user's id */
export const PROJECT_USER_ID = window.opener.globalVariable.project.user;
/**current project user's name */
export const getPROJECT_USER_NAME = () => window.project_user_name;
/**current project user's email */
export const getPROJECT_USER_EMAIL = () => window.project_user_email;
/**current project id */
export const PROJECT_ID = window.opener.globalVariable.project._id;
/**current project name */
export const PROJECT_NAME = window.opener.globalVariable.project.projectName;

// aliasPath is a GraphXR feature which prefixes all URL paths with a constant string. e.g. /graphxr
export const ALIAS_PATH = window.opener.globalVariable.aliasPath ?? '';

export const CONFIG_GUIDE = {
  localhost: "%projects/userGuideProjectId/",
  "graphxrdev.kineviz.com": "%projects/63d85b927f9808a869153944/",//xing,second_item
  "graphxrnext.kineviz.com": "%5fc51b103da6df006283d67b/",
  "graphxr.kineviz.com": "%6151ed5e782c1800529db20a/",
}
export const GUIDE_LINK_URI = CONFIG_GUIDE[location.hostname] || CONFIG_GUIDE.localhost;
export const EXPECT_PROJECT_IDS = ["userGuideProjectId"];
export const EXPECT_PROJECT_NAMES = ["User Guide"];
export const SYSTEM_USER = "systemUser";
export const SYSTEM_USER_URI = "/usermanual/";
export const COMMON = "common";
export const COMMON_NAME = "Personal Folder";
export const DEFAULT_VER = 0;
/** Directory "files/" name */
export const COMMON_EDITOR_KEY = "common";
export const FILESDIR_NAME = "files";
export const FILESDIR = `${FILESDIR_NAME}/`;
let skey;
let masterUserId;
const storage = window.localStorage;
export const FILE_NAMES = ".fileNames";
export const TAGINFOS = ".taginfos";

export const noSharedProjectName = (projectId) => {
  if (projectId === COMMON) {
    return COMMON_NAME;
  } else if (~EXPECT_PROJECT_IDS.indexOf(projectId)) {
    return EXPECT_PROJECT_NAMES[EXPECT_PROJECT_IDS.indexOf(projectId)]
  } else {
    return undefined;
  }
}
/**
 * check is projectId can be shared
 * 
 * @returns {boolean}
 */
export const noSharedProjectIds = (projectId) => {
  return undefined !== noSharedProjectName(projectId)
}
/**
 * Enum for file origin.
 * @readonly
 * @enum {string}
 */
export const TAG_TYPE = {
  published: "published",
  shared: "shared",
  projectShare: "projectShare",
  folder: "folder"
}
export const DefaultFileType = "text/plain";
export const DefaultFileSettings = { version: 0, final: 0 };
export const ShapeShared = {
  _id: "",
  projectId: "",
  projectName: "",
  fileKey: "",
  version: 0,
  status: 0,
  toUserId: {
    _id: "",
    firstName: "",
    lastName: "",
    firstName: "",
    email: ""
  },
  userId: {
    _id: "",
    firstName: "",
    lastName: "",
    firstName: "",
    email: ""
  },
  uploadUri: "",
}
export const ShapeProjectShareUser = {
  userId: "", userName: "", email: "", isMaster: false, userStatus: UserStatus.offline,
}
export const ShapeChatData = {
  userId: "", userName: "", email: "", isMaster: false, userStatus: UserStatus.offline,
  status: ShareProjectCanOptions["View"]
}

export const ShapeChatInfo = {
  userId: "", userName: "", email: "", isMaster: false, userStatus: UserStatus.offline,
  tag: TAG_TYPE, projectId: "", projectName: "", fileKey: "", version: 0,
  status: ShareProjectCanOptions["View"], uploadUri: "",
}

export const DefaultFileData = {
  fileKey: "fileKey", name: "fileName", selected: false, type: DefaultFileType, size: 0, mtimeMs: 0,
  version: 0, final: 0, versionMtimeMs: 0, previewImageUpload: false, messages: null,
  /**
   * @type {null | "DefaultShareData"}
   */
  shareData: null
}
export const DefaultShareData = {
  userId: "",
  userName: "",
  email: "",
  /**@type {TAG_TYPE} */
  tag: "",
  projectId: "",
  projectName: "",
  fileKey: "",
  name: "",
  type: "",
  version: 0,
  status: ShareProjectCanOptions["View"],
  createTime: "",
  mtimeMs: "",
  sharedId: "",
  uploadUri: "",
  attachments: [DefaultFileData],
}
export const DefaultFolderData = {
  folderKey: "folderKey", name: "folderName", selected: false, type: SelectType.dir
}

export const ShapeFileChange = { resetLinkData: false, file: DefaultFileData, version: 0, shared: ShapeChatInfo };

export const ShapeLinkChange = { resetFileData: false, file: DefaultShareData, shared: ShapeChatInfo };

export const DefaultOptions = function () {
  if (isNotLogin()) {
    return [_.assign({}, DefaultShareData, {
      userId: PROJECT_USER_ID,
      userName: getPROJECT_USER_NAME(),
      email: getPROJECT_USER_EMAIL(),
      projectId: PROJECT_ID,
      projectName: PROJECT_NAME,
      fileKey: ""
    })];
  }
  let arr = [];
  if (USER_ID === PROJECT_USER_ID) {
    arr.push(_.assign({}, DefaultShareData, {
      userId: USER_ID,
      userName: USER_NAME,
      email: USER_EMAIL,
      projectId: PROJECT_ID,
      projectName: PROJECT_NAME,
      fileKey: ""
    }))
  } else {
    arr.push(_.assign({}, DefaultShareData, {
      userId: PROJECT_USER_ID,
      userName: getPROJECT_USER_NAME(),
      email: getPROJECT_USER_EMAIL(),
      projectId: PROJECT_ID,
      projectName: PROJECT_NAME,
      fileKey: ""
    }))
  }
  if (IS_ADMIN) {
    arr.push(_.assign({}, DefaultShareData, {
      userId: USER_ID,
      userName: USER_NAME,
      email: USER_EMAIL,
      projectId: EXPECT_PROJECT_IDS[0],
      projectName: EXPECT_PROJECT_NAMES[0],
      fileKey: ""
    }))
  }
  arr.push(_.assign({}, DefaultShareData, {
    userId: USER_ID,
    userName: USER_NAME,
    email: USER_EMAIL,
    projectId: COMMON,
    projectName: COMMON_NAME,
    fileKey: ""
  }))
  return arr;
}

export const setWindowState = /** @param {DefaultShareData} info */ function (info) {
  let params = new URLSearchParams();
  _.each(_.keys(DefaultShareData), (key, index) => {
    undefined !== info[key] && params.append(key, info[key])
  })
  window.history.replaceState({}, null, `${location.pathname}?${params.toString()}`);
}

/**
 * @returns {Object.<string,DefaultFileData|DefaultFolderData>}
 */
export function getFileNamesJson() {
  let fileNamesData = getItem(getFileNamesKey());
  let fileNamesJson = fileNamesData ? JSON.parse(fileNamesData) : {};
  let change = false;
  _.each(_.values(fileNamesJson), (file) => {
    file.name && (file.type !== DefaultFolderData.type) && !file.fileKey && (file.fileKey = file.name) && (change = true);
  })
  if (change) {
    setItem(getFileNamesKey(), JSON.stringify(fileNamesJson));
  }
  return fileNamesJson;
}
export function getTaginfosKey() {
  return getKey() + TAGINFOS;
}

export function getTaginfosJson() {
  let taginfos = getItem(getTaginfosKey());
  let taginfosJson = taginfos ? JSON.parse(taginfos) : [];
  return taginfosJson;
}

/**
 * get storage file names key
 * @returns `graphxr.observable.${PROJECT_ID}.fileNames`
 */
export function getFileNamesKey() {
  return getKey() + FILE_NAMES;
}
/** no file edit save item key */
export function getUndefinedKey() {
  return getFileNamesKey() + "." + "$undefined";
}
/** no file edit save item content */
export function getUndefinedContent() {
  return getItem(getUndefinedKey()) || DefaultData;
}
export function getFormatUndefinedContent() {
  return JSON.stringify(JSON.parse(getUndefinedContent()), null, 2);
}
/**is not login */
export function isNotLogin() {
  return USER_ID === null;
}
/**is login and not own project */
export function notMyOwnProject() {
  return !isNotLogin() && (getSkey() === PROJECT_ID) && USER_ID !== PROJECT_USER_ID;
}

/**
 * fix version updated problem
 */
export function refreshItems() {
  let oldFileNamesKey = `graphxr.observable` + FILE_NAMES;
  let item = getItem(oldFileNamesKey);
  if (undefined !== item) {
    let fileNamesJson = JSON.parse(item);
    let oldFiles = {};
    _.each(fileNamesJson, (value, key) => {
      let fileContent = getItem(oldFileNamesKey + '.' + key);
      if (fileContent !== undefined) {
        oldFiles[oldFileNamesKey + '.' + key] = {
          newKey: `graphxr.observable.${USER_ID}` + FILE_NAMES + '.' + key,
          content: fileContent
        };
      }
    })
    if (!_.isEmpty(oldFiles)) {
      _.each(oldFiles, (value, key) => {
        setItem(value.newKey, value.content);
        removeItem(key);
      })
    }
    setItem(`graphxr.observable.${USER_ID}` + FILE_NAMES, item);
    removeItem(oldFileNamesKey);
    message.warn(`version updated, existing old version cache:\n
    ${_.keys(oldFiles).join("\n")}
    have been changed to your own projects, no worry!`);
  }
}

function getSafeModeKey() {
  return `graphxr.observable.safeMode`
}

export function getSafeMode() {
  return (getItem(getSafeModeKey()) === "true") ? true : false;
}

export function setSafeMode(safeMode) {
  return setItem(getSafeModeKey(), safeMode ? "true" : "false");
}

function getReadOnlyModeKey() {
  return `graphxr.observable.readOnlyMode`
}
export function getReadOnlyMode() {
  return (getItem(getReadOnlyModeKey()) === "true") ? true : false;
}

export function setReadOnlyMode(readOnlyMode) {
  return setItem(getReadOnlyModeKey(), readOnlyMode ? "true" : "false");
}

function getKey() {
  if (isNotLogin()) {
    return `graphxr.observable.share`
  }
  return skey !== COMMON ? `graphxr.observable.${skey}` : `graphxr.observable.${USER_ID}`;
}
export function setSkey(tmp, masterUserIdTmp) {
  skey = tmp;
  masterUserId = masterUserIdTmp;
}
/**
 * current selected projectId
 * @returns "common" | projectId
 */
export function getSkey() {
  return skey;
}

export function getMasterUserId() {
  return masterUserId;
}

export function getCurrentUploadUri() {
  return getUploadUri(skey, masterUserId);
}

export function getUploadUri(projectId, userId) {
  if (!projectId || !userId) {
    throw new Error("error!");
  }
  let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [userId];
  return `/tmp/observable/${rootDirName.join('/')}/`
}

/**
 * get storage content of key
 * @param {*} key 
 * @returns undefined if haven't
 */
export function getItem(key) {
  try {
    const serializedVal = storage.getItem(key)
    if (serializedVal === null) return undefined
    return serializedVal
  } catch (e) {
    return undefined
  }
}
/**
 * save to localstorage
 * @param {*} key 
 * @param {*} val 
 */
export function setItem(key, val) {
  try {
    const serializedVal = val
    storage.setItem(key, serializedVal)
    return true
  } catch (e) {
    return false
  }
}

export function removeItem(key) {
  try {
    storage.removeItem(key)
    return true
  } catch (e) {
    return false
  }
}