import React, { useEffect, useState, useRef } from "react";
import { Switch } from "antd";
const d3 = require('d3');

export default function Selection(props) {
    const { width, height, data, maxNum, className, setFilterValues, startEnd } = props;
    const extractData = _.reduce(data, (prev, curr, index) => {
        const { start, end, num, rows } = curr;
        prev["" + start] = { num, rows };
        if (index === data.length - 1) {
            prev["" + end] = undefined;
        }
        return prev;
    }, {})
    const ty = 20;
    let ref = useRef(null);
    useEffect(() => {
        if (!ref.current) {
            return;
        }

        const x = d3.scalePoint()
            .domain(_.keys(extractData))
            .range([0, width])
        // .padding(0.5)

        // const xAxis = g => g
        //     .attr("transform", `translate(0,${height - ty})`)
        //     .call(d3.axisBottom(x))

        const svg = d3.select(ref.current)
            .attr("viewBox", [0, 0, width, height]);


        if (startEnd && startEnd.selection) {
            function dragstarted(event, d) {
                d3.select(this).raise().attr("stroke", "black");
            }

            function dragged(event, d) {
                let nx = parseFloat(d3.select(this).attr("x")) + event.dx;
                nx = nx < 0 ? 0 : nx;
                nx = nx + parseFloat(d3.select(this).attr("width")) > width ? (width - parseFloat(d3.select(this).attr("width"))) : nx;
                if (nx >= 0 && nx + parseFloat(d3.select(this).attr("width")) <= width) {
                    d3.select(this).attr("x", nx);
                }
            }

            function dragended(event, d) {
                d3.select(this).attr("stroke", null);
                let selection = [parseFloat(d3.select(this).attr("x")), parseFloat(d3.select(this).attr("x")) + parseFloat(d3.select(this).attr("width"))];
                let start = data[0].start + selection[0] / width * (data[data.length - 1].end - data[0].start);
                let end = data[0].start + selection[1] / width * (data[data.length - 1].end - data[0].start);
                setFilterValues({ start, end, selection });
            }

            let drag = d3.drag()
                .on("start", dragstarted)
                .on("drag", dragged)
                .on("end", dragended);
            svg
                .append("g")
                .attr("fill", "orange")
                .attr("fill-opacity", "0.2")
                .append("rect")
                .attr("x", startEnd.selection[0])
                .attr("y", 0)
                .attr("height", height)
                .attr("width", startEnd.selection[1] - startEnd.selection[0])
                .style("cursor", "move")
                .call(drag)
                ;
        } else {
            const brush = d3.brushX()
                .on("start brush end", brushed)
                .on("end.snap", brushended);

            // const defaultColor = "none";
            // const bar = svg
            //     .append("g")
            //     .attr("fill", defaultColor)
            //     .selectAll("rect")
            //     .data(x.domain())
            //     .join("rect")
            //     .attr("x", (d) => x(d))
            //     .attr("y", 0)
            //     .attr("height", height - ty)
            //     .attr("width", x.step());
            // svg.append("g")
            //     .attr("font-family", "var(--sans-serif)")
            //     .attr("text-anchor", "middle")
            //     .attr("transform", `translate(${x.bandwidth() / 2},${height / 2})`)
            //     .selectAll("text")
            //     .data(x.domain())
            //     .join("text")
            //     .attr("x", d => x(d))
            //     .attr("dy", "0.35em")
            // .text(d => d);
            // const bar = svg
            //     .append("g")
            //     .attr("fill", defaultColor)
            //     .selectAll("rect")
            //     .data(x.domain())
            //     .join("rect")
            //     .attr("x", (d) => x(d))
            //     .attr("y", (d) => {
            //         return extractData[d] ? (1 - extractData[d].num / maxNum) * (height - ty) : 0
            //     })
            //     .attr("height", (d) => {
            //         return extractData[d] ? extractData[d].num / maxNum * (height - ty) : 0
            //     })
            //     .attr("width", x.step());

            svg.append("g")
                .call(brush);
            // svg.append("g")
            //     .call(xAxis);
            function brushed({ selection }) {
                if (selection) {
                    const range = x.domain().map(x);
                    const i0 = d3.bisectCenter(range, selection[0]);
                    const i1 = d3.bisectCenter(range, selection[1]);
                    // bar.attr("fill", (d, i) => (i0 <= i && i < i1 ? "orange" : defaultColor));
                    svg.property("value", x.domain().slice(i0, i1)).dispatch("input");
                } else {
                    // bar.attr("fill", defaultColor);
                    svg.property("value", []).dispatch("input");
                }
            }

            function brushended({ selection, sourceEvent }) {
                if (!sourceEvent || !selection) return;
                // const range = x.domain().map(x),
                //     dx = x.step();
                // const x0 = range[d3.bisectCenter(range, selection[0])];
                // const x1 = range[d3.bisectCenter(range, selection[1])];
                // d3.select(this)
                //     .transition()
                //     .call(brush.move, x1 > x0 ? [x0, x1] : null);
                if (selection[0] < selection[1] && setFilterValues) {
                    if (selection[0] === 0 && selection[1] === width) {
                        setFilterValues(null);
                    } else {
                        let start = data[0].start + selection[0] / width * (data[data.length - 1].end - data[0].start);
                        let end = data[0].start + selection[1] / width * (data[data.length - 1].end - data[0].start);
                        setFilterValues({ start, end, selection });
                    }
                }
            }
        }
    }, [ref.current]);
    return <svg className={className} width={width} height={height} ref={ref} />;
}