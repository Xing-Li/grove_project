const RecordingUtil = require('./../util/RecordingUtil');
// const clients = [];


const recordSocket = function (io) {
	io.of('/records')
		.on("connection", function (socket) {

			console.log("socketID:" + socket.id);
			// clients[socket.id] = socket;

			socket.itemsCount = 0;
			//will be overrited  
			socket.tmpName = socket.id.replace(/[#\/]/g, "");

			socket.on('data', function (data) {
				if (!data || !data.data || !data.type) {
					// socket.emit("reply", {
					// 	type:data.type,
					// 	event: "data",
					// 	status: 1,
					// 	message: "Miss parameters \"data\" or \"type\"  "
					// });
					return;
				}

				if (socket.tmpName != data.fileName) {
					console.warn("Socket maybe broken and retry to connect");
					socket.tmpName = data.fileName;
				}

				if (data.type == "image") {
					RecordingUtil.saveImage(socket.tmpName, data.index, data.data);
				} else if (data.type == "video") {
					RecordingUtil.saveVedio(socket.tmpName, data.index, data.data);
				}

				socket.emit("reply", {
					type: data.type,
					event: "data",
					index: data.index ,
					message: "success"
				});

				console.log("Receive the data with index :", data.index);
				socket.itemsCount++;
			});

			socket.on('start', function (data) {
				socket.itemsCount = 1;
				socket.lastIndex = 0;

				//overrite the tempName
				socket.tmpName = data.fileName || socket.tmpName;

				RecordingUtil.clearnData(socket.tmpName, function (err) {
					if (err) {
						console.log(err);
						socket.emit("reply", {
							type: data.type,
							event: "start",
							status: 1,
							message: err.message
						});
					} else {
						socket.emit("reply", {
							type: data.type,
							event: "start",
							status: 0,
							message: "Ready, Will Start"
						});
					}


				});

			});

			socket.on('stop', function (data) {



				if (!data || !data.lastIndex || !(parseInt(data.lastIndex) > 0)) {
					socket.emit("reply", {
						event: "stop",
						status: 1,
						message: "Miss parameters \"lastIndex\", and please sure lastIndex is number type."
					});
				} else {

					socket.lastIndex = data.lastIndex;

					if (socket.tmpName != data.fileName) {
						console.warn("Socket maybe broken and retry to connect");
						socket.tmpName = data.fileName;
					}

					let timer = null;
					let checkCount = 0;

					function checkItems() {
						if ((socket.itemsCount + 5) >= parseInt(data.lastIndex) && data.type == "image"
							|| data.type == "video" && socket.itemsCount == parseInt(data.lastIndex)) {

							let handleGenerateMP4 = function (err, fileName) {
								console.log("handle Generate MP4 finish.");
								if (err) {
									console.error(err);
									socket.emit("reply",
										{
											type: data.type,
											event: "stop",
											status: 1,
											message: err.message
										});
								} else {
									socket.emit("reply",
										{
											type: data.type,
											event: "stop",
											status: 0,
											message: "Generate MP4 Success",
											content: (fileName || socket.tmpName) + '.mp4'
										});
								}
							};

							setTimeout(function () {
								if (data.type == "image") {
									RecordingUtil.generateMP4FromImages(socket.tmpName, handleGenerateMP4, data.videoType == '360');
								} else if (data.type == "video") {
									RecordingUtil.generateMP4FromStream(socket.tmpName, handleGenerateMP4, data.videoType == '360');
								}
							}, 4000)

						} else {
							timer = setTimeout(function () {
								clearTimeout(timer);
								timer = null;
								checkCount++;
								checkItems();
							}, 1000);
						}
					}

					checkItems();
				}
			});

			socket.on('disconnect', function () {
				console.log('a user disconnected, will remove client:' + socket.id);
				// delete clients[socket.id];
			});

		});

}


module.exports = recordSocket;