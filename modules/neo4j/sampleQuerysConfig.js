module.exports =  [{
	needLabels: ['Publication', 'Events'],
	querys: [
    	'MATCH (n:Publication)-[r]-(p) WHERE p.name =~ ".*(?i)fauci.*" RETURN n,r,p LIMIT 200',
		'MATCH (n:Events) WHERE n.title =~ ".*(?i)Genomics.*" or n.title =~ ".*(?i)Genomics.*" RETURN n LIMIT 200'
	],
	icon: '/public/default_icons/spritesheet.png',
	iconMap: {},
	indexableKeys: ['name'],
	searchKey:'hiv'
},
{
	needLabels: ['fb_page'],
	querys: [
		'MATCH (n:fb_page) RETURN n LIMIT 25',
		'MATCH p=()-[r:RSVP]->() RETURN p LIMIT 25',
		'MATCH p=()-[r:liked]->() RETURN p LIMIT 25'
	],
	icon: '/public/default_icons/spritesheet3.png',
	iconMap: {},
	indexableKeys: ['event_name', 'name', 'fb_guest_name', 'fb_guest_name']
},
{
	needLabels: ['Hashtag', 'Tweet', 'Troll', 'User', 'URL'],
	querys: [
		"MATCH (n:Hashtag {tag: 'islamkills'})-[r]-(a:Tweet)-[b]-(m:Troll) RETURN * LIMIT 100",
		"MATCH (h:Hashtag {tag:'pjnet'} )-[r]-(t:Tweet) optional MATCH(t)-[r0]-(u:URL) OPTIONAL MATCH (t)-[p]-(u0:User) RETURN * LIMIT 100",
		"MATCH (h:Hashtag {tag:'neverhillary'} )-[r]-(t:Tweet) OPTIONAL MATCH(t)-[r0]-(u:URL) OPTIONAL MATCH (t)-[p]-(u0:User) RETURN * LIMIT 100",
		"MATCH (t:Troll)-[po:POSTED]->(tw:Tweet) WHERE tw.text CONTAINS 'fraud' OPTIONAL MATCH p=(tw) - [:HAS_TAG | HAS_LINK | MENTIONS | IN_REPLY_TO]-(a) RETURN * LIMIT 100"
	],
	icon: '/public/default_icons/spritesheet.png',
	iconMap: {
		// 'User:Troll': 14
	},
	indexableKeys: ['name', 'text', 'tag']
},
{
	needLabels: ['Address', 'Entity', 'Intermediary', 'Officer'],
	querys: [
		"MATCH p=allShortestPaths((a)-[:OFFICER_OF|:INTERMEDIARY_OF |:REGISTERED_ADDRESS*..10]-(b)) WHERE a.name CONTAINS 'Ross, Jr'  AND b.name CONTAINS 'Grant' RETURN p LIMIT 100",
		"MATCH p=()-[r:INTERMEDIARY_OF]->() RETURN p LIMIT 100",
		"MATCH p=()-[r:CONNECTED_TO]->() RETURN p LIMIT 25"
	],
	icon: '/public/default_icons/spritesheet.png',
	iconMap: {},
	indexableKeys: ['name']
},
{
	needLabels: ['Airport', 'Flight'],
	icon: '/public/default_icons/spritesheet.png',
	iconMap: {},
	querys: [
		'MATCH (da:Airport)-[d:DESTINATION]-(n:Flight)-[o:ORIGIN]-(oa:Airport) RETURN * LIMIT 100',
		'MATCH (o:Airport)-[c:CONNECTED_TO]->(d:Airport) return * limit 100',
		`Match (f:Flight {flight_num:1710})-[:CARRIER]-(car {abbr: 'WN'}) 
match (da:Airport)-[d:DESTINATION]-(f)-[o:ORIGIN]-(oa:Airport) return *`,
		`MATCH (ms:Airport { abbr: 'SCC' }),(cs:Airport { abbr: 'SJU' }), p = shortestPath((ms)-[:CONNECTED_TO*]-(cs)) RETURN p limit 100`,
		`MATCH p=(ms:Airport { abbr: 'SCC' })-[r:CONNECTED_TO*4]-(cs:Airport { abbr: 'SJU' }) RETURN p limit 100`
	],
	indexableKeys: ['name']
},
{
	needLabels: ['VersionableObject', 'Electromotor'],
	icon: '/public/default_icons/spritesheet2.png',
	iconMap: {},
	querys: [
		'MATCH p=()-[r:childOf]->() RETURN p LIMIT 25',
	],
	indexableKeys: ['name'],
	useModel: true,
	models: [{
		label: 'VersionableObject:Electromotor',
		file: 'static/stl/Boris/3EGH489017-8096.stl',
		scale: 0.001,
	}, {
		label: 'VersionableObject:Part',
		file: 'static/stl/Boris/500244928_A.STL',
		scale: 0.001,
	}, {
		label: 'VersionableObject:Module',
		file: 'static/stl/Boris/500381254_B.STL',
		scale: 0.0003,
	}, {
		label: 'VersionableObject:Breakes',
		file: 'static/stl/Boris/Abrissbirne-V03.stl',
		scale: 0.008,
	}, {
		label: 'VersionableObject:Gearbox:PlanetaryGearbox',
		file: 'static/stl/Boris/Chizzle-V03.stl',
		scale: 0.008,
	}, {
		label: 'VersionableObject:Gearbox:WormGearbox',
		file: 'static/stl/Boris/GripperArm3.stl',
		scale: 0.001,
	}, {
		label: 'Electromotor',
		file: 'static/stl/Boris/Schaufel-V03.stl',
		scale: 0.004,
	}, {
		label: 'Electromotor',
		file: 'static/stl/Boris/Schaufelaufnahme.stl',
		scale: 0.03,
	}]
},

]