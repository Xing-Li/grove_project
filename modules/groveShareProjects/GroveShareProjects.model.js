const mongoose = require('mongoose');
const schemaCRUD = require('../schemaCRUD/schemaCRUD');

const Schema = mongoose.Schema;

const GroveShareProjects = new Schema({
    userId: { type: Schema.Types.ObjectId, ref: 'users' },
    projectId: String,
    projectName: String,
    fileKey: String,
    version: Number,
    status: Number,
    createTime: { type: Date, default: Date.now },
    title: String,
    fileName: String,
});

GroveShareProjects.plugin(schemaCRUD.CRUD);

GroveShareProjects.plugin(function (schema) {

    schema.statics.addShareProject = function (addInfo, cb) {
        return schemaCRUD.add(this, addInfo, function (err, result) {
            return cb(err, result);
        }, false);
    }

    schema.statics.getAllShareProjects = function (condition, cb) {
        schemaCRUD.queryAll(this, condition, function (err, projects) {
            return cb(err, projects)
        }, {
            path: 'userId',
            select: 'email  lastName firstName'
        }, false, {})
        // this.find(condition).populate({
        //     path: 'userId',
        //     select: 'email  lastName firstName'
        // }).select({}).sort({ "createTime": -1 })
        //     .exec(function (err, projects) {
        //         return cb(err, projects)
        //     });
    }

    schema.statics.getShareProject = function (id, cb) {
        this.findOne({ "_id": id }).exec(function (err, item) {
            cb(err, item)
        });
    }

    schema.statics.updateShareProjectById = function (id, updateInfo, cb) {
        this.findOneAndUpdate(
            { "_id": id },
            { "$set": updateInfo },
            { upsert: false },
            function (err, item) {
                cb(err, item)
            })
    }

    schema.statics.deleteShareProjectById = function (id, cb) {
        this.deleteOne({ '_id': id }, cb)
    }

    schema.statics.deleteShareProjectByIds = function (ids, cb) {
        this.deleteMany({
            _id: {
                $in: ids
            }
        }, cb)
    }
});



module.exports = mongoose.model('GroveShareProjects', GroveShareProjects);