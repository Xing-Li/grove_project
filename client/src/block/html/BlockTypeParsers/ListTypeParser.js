export function parseListToHtml(blocks) {
  let items = {};
  switch (blocks.style) {
    case 'unordered':
      items = blocks.items.map((item) => (`<li>${item}</li>`));

      return `<ul>${items.join("\n")}</ul>`;
    case 'ordered':
      items = blocks.items.map((item, index) => (`<li>${index + 1} ${item}</li>`));

      return `<ol>${items.join("\n")}</ol>`;
    default:
      break;
  }
}

export function parseMarkdownToList(blocks) {
  let listData = {};
  const itemData = [];

  blocks.children.forEach((items) => {
    items.children.forEach((listItem) => {
      listItem.children.forEach((listEntry) => {
        itemData.push(listEntry.value);
      });
    });
  });

  listData = {
    data: {
      items: itemData,
      style: blocks.ordered ? 'ordered' : 'unordered',
    },
    type: 'list',
  };

  return listData;
}
