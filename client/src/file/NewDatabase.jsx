import { InboxOutlined, UploadOutlined } from '@ant-design/icons';
import {
    Button, Select, Space, Form,
    Input,
    Radio,
    Cascader,
    DatePicker,
    InputNumber,
    TreeSelect,
    Switch,
    Row,
    Col,
    Checkbox,
    Upload,
} from 'antd';
import Text from 'antd/lib/typography/Text';
import axios from 'axios';
import _ from 'lodash';
import PropTypes from "prop-types";
import React from 'react';
import MdFiles from './MdFiles';
import Modal from 'antd/lib/modal/Modal';
import { WIDTH, Z_INDEX, Title } from './fileUtils.js';
import { DatabaseType, ModalType, showToast, DatabaseFields as Fields, ConnectionHostedByType, DatabaseCreateTypes, showMessage } from '../util/utils.js';
import { Link } from 'stdlib/link';
import { GUIDE_LINK_URI } from '../util/localstorage';
import MyDropzone from './MyDropzone';
import { dsv, parse } from 'util/helpers';

const { DefaultDatabase, awsRegions } = require("util/databaseApi");
const { Option } = Select;
export default class NewDatabase extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        width: PropTypes.number,
        react_component: PropTypes.object,
    };
    static defaultProps = {};

    constructor(props) {
        super(props);
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
        /**@type {DefaultDatabase} */
        let database = react_component.state.modalData.database;
        let newState = () => {
            let type = DatabaseType.Mysql;
            let connectionHostedBy = ConnectionHostedByType['Self-hosted proxy'];
            let fields = _.reduce(Fields, (prev, curr, key) => {
                _.each(_.keys(curr), (key) => {
                    _.merge(prev, curr[key]);
                })
                return prev;
            }, {})
            _.merge(fields, _.cloneDeep(Fields[type][connectionHostedBy]))
            return {
                acceptedFile: "",
                modalName: ModalType.NewDatabase,
                name: "",
                type: type,
                connectionHostedBy: connectionHostedBy,
                sslTls: false,
                ...fields,
            }
        }
        this.state = database ? { acceptedFile: { path: database.file, size: 0 }, modalName: "Edit Database", ...database } : newState();
        this.oldState = _.cloneDeep(this.state);
        this.formRef = React.createRef();
    }

    componentDidMount() {
    }

    componentDidUpdate(prevProps, prevState) {
    }

    handleOk = () => {
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
    };

    onFormLayoutChange = ({ size }) => {
        setComponentSize(size);
    };

    render() {
        const { className, width } = this.props;
        const { name, type, modalName, connectionHostedBy, acceptedFile } = this.state;
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
        const { modalType, modalData: { databases } } = react_component.state;
        const closeFunc = () => {
            react_component.setState({
                modalType: ModalType.None,
            });
        };
        let pairs = _.reduce(_.keys(Fields[type][connectionHostedBy]), (prev, key) => {
            if (~(Fields[type].InvisibleFields || []).indexOf(key)) {
                return prev;
            }
            if (prev.length && prev[prev.length - 1].length === 1) {
                prev[prev.length - 1].push(key);
            } else {
                prev.push([key]);
            }
            return prev;
        }, []);
        const onFinish = (vs) => {
            const values = _.cloneDeep(vs);
            console.log('Success:', values);
            let addChecks = [...(Fields[type].InvisibleFields || []), "file"];
            for (let index = 0; index < addChecks.length; index++) {
                let checkField = addChecks[index];
                if (undefined !== Fields[type][connectionHostedBy][checkField] && this.state[checkField] === "") {
                    return showMessage(`${checkField} Can Not Be Empty!`, 'error');
                }
                if (undefined !== this.state[checkField]) {
                    values[checkField] = this.state[checkField];
                }
            }

            const { react_component } = this.props;
            const { modalData: { cb } } = react_component.state;
            if (modalName === ModalType.NewDatabase) {
                if (_.filter(databases, (database) => {
                    return database.name === values.name
                }).length > 0) {
                    return showToast("Name Can Not Be Repeated!", 'error');
                }
            }
            if (!_.isEqual(values, _.pick(this.oldState, _.keys(values)))) {
                cb(values);
            }
            closeFunc();
        };

        const onFinishFailed = (errorInfo) => {
            showToast(_.reduce(errorInfo.errorFields, (prev, curr, index) => {
                prev += curr.errors.join(",")
                return prev + "; ";
            }, ""), 'error')
            console.log('Failed:', errorInfo);
        };
        return <Modal zIndex={Z_INDEX}
            className={`modal-comp data-html2canvas-ignore ${className}`}
            width={width || WIDTH}
            title={<div><h4>{modalName}</h4></div>}
            visible={modalType !== ModalType.None}
            onCancel={closeFunc}
            footer={null}
        >
            <Form ref={this.formRef} key={type} layout={"vertical"}
                initialValues={this.state}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                    <Col className="gutter-row" span={{ md: 24, lg: 12 }}>
                        <Form.Item label="Name" name="name" rules={[
                            {
                                required: true,
                            },
                            ({ getFieldValue }) => ({
                                validator(rule, value) {
                                    if (!value) {
                                        return Promise.resolve();
                                    }
                                    if (modalName === ModalType.NewDatabase) {
                                        if (value === "GraphxrNeo4j") {
                                            return Promise.reject(new Error("Can Not Use The Name!"));
                                        }
                                        if (_.filter(databases, (database) => {
                                            return database.name === value
                                        }).length > 0) {
                                            return Promise.reject(new Error("Name Can Not Be Repeated!"));
                                        }
                                    }
                                    return Promise.resolve();
                                },
                            }),
                        ]}>
                            <Input readOnly={modalName === ModalType.NewDatabase ? false : true}
                                onChange={(e) => {
                                    this.setState({ name: e.target.value })
                                }} />
                        </Form.Item>
                    </Col>
                    <Col className="gutter-row" span={{ md: 24, lg: 12 }}>
                        <Form.Item label="Type" name="type" rules={[
                            {
                                required: true,
                            },
                        ]}>
                            <Select
                                style={{ width: 120 }}
                                onChange={(ntype) => {
                                    console.log(ntype)
                                    this.setState({
                                        type: ntype,
                                        ..._.cloneDeep(Fields[ntype][connectionHostedBy]),
                                    })
                                }}
                            >{_.map(_.filter(DatabaseType, (value) => { return Fields[value][connectionHostedBy] }), (key) => {
                                return <Option key={key} value={key}>{key}</Option>
                            })}
                            </Select>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                    <Col className="gutter-row" span={24}>
                        <Form.Item label="Connection hosted by" name="connectionHostedBy" rules={[
                            {
                                required: true,
                            },
                        ]} extra={connectionHostedBy === ConnectionHostedByType["Self-hosted proxy"] && <div>Learn more about setting up a self-hosted database proxy <Link fileKey={`${GUIDE_LINK_URI}self-hosted-database-proxies/self-hosted-database-proxies`} text="here"></Link>.</div>}>
                            <Select style={{ width: "100%" }}
                                onChange={(key) => {
                                    console.log(key)
                                    this.setState({
                                        connectionHostedBy: key,
                                        ..._.cloneDeep(Fields[type][key]),
                                    })
                                }}
                            >{_.map(_.filter(_.keys(Fields[type]), (k) => ~_.keys(ConnectionHostedByType).indexOf(k)), (key) => {
                                return <Option key={key} value={key}>{key}</Option>
                            })}
                            </Select>
                        </Form.Item>
                    </Col>
                </Row>
                {
                    _.map(pairs,
                        /**
                         * 
                         * @param {[]} fieldPair 
                         * @param {*} index 
                         */
                        (fieldPair, index) => {
                            return <Row key={fieldPair.join("_")} gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                {_.map(fieldPair, (field, index) => {
                                    let Template = Fields[type][connectionHostedBy];
                                    let labelName = field.camelPeakToBlankSplit().firstUpperCase();
                                    let Comp;
                                    if (typeof Template[field] === 'boolean') {
                                        Comp = <Switch onChange={(e) => {
                                            let state = {}; state[field] = e;
                                            this.setState(state);
                                        }} />;
                                    } else if (typeof Template[field] === 'number') {
                                        Comp = <InputNumber onChange={(e) => {
                                            let state = {}; state[field] = e;
                                            this.setState(state);
                                        }} />;
                                    } else if (field === 'protocal') {
                                        Comp = <Select onChange={(e) => {
                                            let state = {}; state[field] = e;
                                            this.setState(state);
                                        }} >{_.map(["neo4j", "neo4j+s", "neo4j+ssc", "bolt", "bolt+s", "bolt+ssc"], (key) => {
                                            return <Option key={key} value={key}>{key}</Option>
                                        })}</Select>;
                                    } else if (field === 'proxyHost' && connectionHostedBy === ConnectionHostedByType["Self-hosted proxy"]) {
                                        labelName += "(or paste url here)"
                                        Comp = <Input type={!~(Fields[type].HideFields || []).indexOf(field) ? 'text' : 'password'} onChange={(e) => {
                                            let match = e.target.value.trim().match(/^(http|https):\/\/([\w|\.]+)(:\d+)?(\S+)?$/)
                                            if (null !== match && match.length === 5) {
                                                let state = {}; state.sslTls = match[1] === 'https';
                                                state[field] = match[2];
                                                state.port = match[3] ? parseInt(match[3].substring(1)) : (state.sslTls ? 443 : 80);
                                                state.location = match[4];
                                                this.setState(state, () => {
                                                    this.formRef.current.setFieldsValue(this.state)
                                                });
                                            } else {
                                                let state = {}; state[field] = e.target.value;
                                                this.setState(state);
                                            }
                                        }} />;
                                    } else if (field === 'region' && type === DatabaseType.DynamoDB) {
                                        Comp = <Select
                                            style={{ width: 120 }}
                                            onChange={(ntype) => {
                                                let state = {}; state[field] = ntype;
                                                this.setState(state);
                                            }}
                                        >{_.map(awsRegions, (Region, index) => {
                                            return <Option key={Region} value={Region}>{Region}</Option>
                                        })}
                                        </Select>;
                                    } else if (field === 'file') {
                                        if (type === DatabaseType.BigQuery) {

                                        } else if (type === DatabaseType.DynamoDB) {
                                            return <Col key={field} className="gutter-row" span={{ md: 24, lg: 12 }}>
                                                <MyDropzone input={true}
                                                    multiple={false}
                                                    accept=".csv"
                                                    handleFilesFunc={async (acceptedFiles) => {
                                                        if (acceptedFiles.length >= 1) {
                                                            let acceptedFile = acceptedFiles[0];
                                                            let text = await parse(acceptedFile);
                                                            console.log(text);
                                                            let config = dsv(text, ",");
                                                            if (config.length !== 1) {
                                                                throw new Error("csv file error!");
                                                            }
                                                            let state = { acceptedFile }; state[field] = acceptedFile.name;
                                                            _.assign(state, {
                                                                accessKeyId: config[0]['Access key ID'],
                                                                secretAccessKey: config[0]['Secret access key'],
                                                            })
                                                            this.setState(state, () => {
                                                                this.formRef.current.setFieldsValue(this.state)
                                                            });
                                                        }
                                                    }}>
                                                    <div className="text-center">
                                                        <div>Drag and drop access key file to this panel or</div>
                                                        <Button type='default'>Select a file</Button>
                                                        {acceptedFile && (
                                                            <ul>
                                                                <li key={acceptedFile.path}>
                                                                    {acceptedFile.path} - {acceptedFile.size} bytes
                                                                </li>
                                                            </ul>
                                                        )}
                                                    </div>
                                                </MyDropzone>
                                                <p>A CSV key file belonging to a <a className="fw5" target="_blank" rel="noopener noreferrer" href='https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction.html'>IAM</a> users, with the ‘AmazonDynamoDBReadOnlyAccess’ and ‘AmazonDynamoDBFullAccess’ permissions, is required to access DynamoDB. See the documentation for <a className="fw5" target="_blank" rel="noopener noreferrer" href="https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Introduction.html">What is Amazon DynamoDB?</a> and <a className="fw5" target="_blank" rel="noopener noreferrer" href="https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html?icmpid=docs_iam_console#Using_CreateAccessKey">Managing access keys for IAM users</a> for more information.</p>
                                            </Col>
                                        }
                                    } else {
                                        Comp = <Input type={!~(Fields[type].HideFields || []).indexOf(field) ? 'text' : 'password'} onChange={(e) => {
                                            let state = {}; state[field] = e.target.value;
                                            this.setState(state);
                                        }} />;
                                    }
                                    return <Col key={field} className="gutter-row" span={{ md: 24, lg: 12 }}>
                                        <Form.Item valuePropName={typeof Template[field] === 'boolean' ? "checked" : "value"} label={labelName}
                                            name={field}
                                            rules={[
                                                {
                                                    required: ~(Fields[type].OptionFields || []).indexOf(field) ? false : true,
                                                },
                                            ]}
                                        >
                                            {Comp}
                                        </Form.Item>
                                    </Col>
                                })}
                            </Row>
                        })
                }
                <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                    <Col className="gutter-row" span={24}>
                        <Form.Item
                            name="sslTls"
                            valuePropName="checked"
                            rules={[
                                {
                                    required: true,
                                },
                            ]}
                        >
                            <Checkbox onChange={(e) => {
                                let state = {}; state.sslTls = e.target.checked;
                                this.setState(state);
                            }}>
                                Require SSL/TLS
                            </Checkbox>
                        </Form.Item>
                    </Col>
                </Row>
                <Form.Item key="back"  >
                    <div className="d-flex justify-content-end">
                        <Button key="back" onClick={closeFunc}>
                            Cancel
                        </Button>&nbsp;&nbsp;
                        <Button key="addNew" type="primary" htmlType="submit" disabled={_.isEqual(this.state, this.oldState)}>
                            {modalName === ModalType.NewDatabase ? "Add New" : "Save Changes"}
                        </Button>
                    </div>
                </Form.Item>
            </Form>
        </Modal>
    }
}
