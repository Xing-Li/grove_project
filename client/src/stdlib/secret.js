export default function secret(name) {
    let secrets = _.filter(window.editor.getSettings().getSecrets(), (secret, index) => {
        return secret.name === name;
    });
    if (secrets.length !== 1) {
        return new Error("Secret not found");
    }
    let secret = secrets[0];
    return secret.value;
} 