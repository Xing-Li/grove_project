const util = require('./../util/util');
const schemaCRUD = function () { };
//del
schemaCRUD.del = function (schema, id, cb, cbOld = false) {
  console.log(schema.modelName + " >>>  : remove id = " + id);
  function handleDel(oldItem = {}) {
    return schema.deleteOne({ _id: id }, function (err) {
      return cb(err,  oldItem || {});
    });
  }
  if(cbOld){
    return schema
    .findOne({ _id: id })
    .select({})
    .exec(function (err, oldItem) {
      return handleDel(oldItem ?  oldItem.toJSON() : {});
    })
  }else{
    return handleDel();
  }
}

//clear
schemaCRUD.clear = function (schema, cb) {
  console.log(schema.modelName + " >>>  : Clear all Data");
  return schema.deleteMany({}, function (err) {
    return cb(err);
  });
}

//get edit
schemaCRUD.edit = function (schema, id, infoData, cb, uniqueName) {

  if(infoData && infoData['_id']){
    delete infoData['_id']
  }
  function updateData() {
    return schema.findOneAndUpdate(
      {'_id':id},
      {'$set':infoData } , 
      {'upsert':false, returnOriginal:true},
       function (err, oldDoc) {
      return cb(err, oldDoc ? Object.assign( oldDoc.toJSON(),infoData,{_id:id}) : null, oldDoc || {});
    });
  }

  if (uniqueName && infoData[uniqueName]) {
    let condition = {};
    condition[uniqueName] = infoData[uniqueName];

    return schema
      .findOne(condition)
      .select('_id ')
      .exec(function (error, existItem) {

        if ((existItem && id == existItem._id) || (!error && !existItem)) {
          return updateData();
        } else {
          return cb(new Error("Already exist record " + condition[uniqueName]), null, {});
        }

      });
  } else {
    return updateData();
  }
}

//get add
schemaCRUD.add = function (schema, infoData, cb, debug, uniqueName) {

  if (!infoData) {
    return cb(new Error("You must add paramaters to data"), null, {});
  };

  function saveData() {
    let temObject = new schema(infoData);

    if (debug) {
      return cb(null, infoData);
    } else {
      return temObject.save(function(err, item){
        if(err){
          console.error(item, {});
        }
        cb(err, item, {});
      });
    }
  }

  if (uniqueName && infoData[uniqueName]) {
    let condition = {};
    condition[uniqueName] = infoData[uniqueName];

    return schema
      .findOne(condition)
      .select('_id')
      .exec(function (error, result) {

        if (result) {
          return cb(new Error("Already exist record " + condition[uniqueName]), null, {});
        } else {
          return saveData();
        }
      });
  } else {
    return saveData();
  }
}

//get add
schemaCRUD.forceAdd = function (schema, infoData, cb, debug, uniqueName) {

  let self = this;

  if (!infoData) {
    return cb(new Error("You must add paramaters to data"), null);
  };

  function saveData() {
    let temObject = new schema(infoData);

    if (debug) {
      return cb(null, infoData);
    } else {
      return temObject.save(cb);
    }
  }

  if (uniqueName && infoData[uniqueName]) {
    let condition = {};
    condition[uniqueName] = infoData[uniqueName];

    return schema
      .findOne(condition)
      .select('_id')
      .exec(function (error, result) {

        if (result) {
          return self.edit(schema, result._id, infoData, cb);
        } else {
          return saveData();
        }

      });
  } else {
    return saveData();
  }
}

//getAll
schemaCRUD.queryAll = function (schema, condition, cb, populates, isCount, ignoreFields) {

  let findCondition = {};
  let optionArray = [];

  if (util.isNullOrEmpty(condition)) {
    condition = {};
    condition.search = {};//search
    condition.option = {};//and
    condition.multiOptions = {};//or
  }

  condition.page = parseInt(condition.page) || 0;
  condition.items = parseInt(condition.items) || 1000;

  if (util.isNullOrEmpty(condition.sort)) {
    condition.sort = { createTime: 1 };
  }

  //for search
  let searchArray = [];
  if (!util.isNullOrEmpty(condition.search)) {

    for (let searckKey in condition.search) {
      if (!util.isNullOrEmpty(condition.search[searckKey])) {
        let keyValue = {};
        keyValue[searckKey] = new RegExp(condition.search[searckKey], "i");
        searchArray.push(keyValue);
      }
    }

  }

  //for multiOptions
  if (!util.isNullOrEmpty(condition.multiOptions)) {

    for (let optionKey in condition.multiOptions) {
      if (!util.isNullOrEmpty(condition.multiOptions[optionKey])) {
        let keyValue = {};
        keyValue[optionKey] = condition.multiOptions[optionKey];
        searchArray.push(keyValue);
      }
    }
  }

  if (searchArray.length > 1) {
    optionArray.push({ $or: searchArray });
  } else if (searchArray.length == 1) {
    optionArray = searchArray;
  }

  //for option
  if (!util.isNullOrEmpty(condition.option)) {

    for (let optionKey in condition.option) {
      if (!util.isNullOrEmpty(condition.option[optionKey])) {
        let keyValue = {};
        keyValue[optionKey] = condition.option[optionKey];
        optionArray.push(keyValue);
      }
    }
  }

  //for timeRange
  if (!util.isNullOrEmpty(condition.timeRange)) {


    condition.timeRange = condition.timeRange.map(d => new Date(parseInt(d) || new Date()));

    let maxDate = Math.max.apply(null, condition.timeRange);
    //end this day
    // maxDate = maxDate+24*60*60*1000;
    if (!condition.timeRangeName) {
      condition.timeRangeName = "createTime";
    }

    let timeRangeMin = {};
    let timeRangeMax = {};
    timeRangeMin[condition.timeRangeName] = { $lt: maxDate };
    timeRangeMax[condition.timeRangeName] = { $gt: Math.min.apply(null, condition.timeRange) };

    optionArray.push(timeRangeMin);
    optionArray.push(timeRangeMax);
  }

  if (condition.updateTime && condition.updateTime > 0) {
    optionArray.push({ updateTime: { $gt: condition.updateTime } });
  }

  if (optionArray.length > 1) {
    findCondition = { $and: optionArray };
  } else if (optionArray.length == 1) {
    findCondition = optionArray[0];
  }

  // console.log("getAll condition :",JSON.stringify(findCondition))
  //add count
  if (isCount) {
    return schema.countDocuments(findCondition, cb);
  }

  //query
  let query =
    schema.find(findCondition, ignoreFields ? ignoreFields : {});
  //support populates one or more
  if (populates) {
    if (Array.isArray(populates)) {
      for (let tempIndex in populates) {
        query.populate(populates[tempIndex]);
      }
    } else {
      query.populate(populates);
    }
  }
  query.sort(condition.sort)
    .limit(condition.items)
    .skip(condition.page * condition.items)
    .select({});
  return query.exec(cb);
}


schemaCRUD.getAll = function (schema) {
  schema.statics.getAll = function (condition, cb, isCount, populates) {
    let currentSchema = this;
    return schemaCRUD.queryAll(currentSchema, condition, cb, populates, isCount);
  };
}

schemaCRUD.CRUD = function (schema) {

  //del
  schema.statics.del = function (id, cb, cbOld) {
    return schemaCRUD.del(this, id, cb, cbOld);
  };

  //add
  schema.statics.add = function (objectInfo, cb, uniqueName) {
    return schemaCRUD.add(this, objectInfo, cb, false, uniqueName);
  };

  //forceAdd
  schema.statics.forceAdd = function (objectInfo, cb, uniqueName) {
    return schemaCRUD.forceAdd(this, objectInfo, cb, false, uniqueName);
  };

  //edit
  schema.statics.edit = function (id, objectInfo, cb, uniqueName) {
    return schemaCRUD.edit(this, id, objectInfo, cb, uniqueName);
  };

  //getAll
  schemaCRUD.getAll(schema);

}


module.exports = schemaCRUD;