import html from "@observablehq/stdlib/src/html";
import { groveComponent } from "./groveComponent";
import classNames from "classnames";

export const Icon = groveComponent(({className, id, style=''}) => html`<span style="${style}" class="${classNames(`fa fa-${id}`, className)}" />`)