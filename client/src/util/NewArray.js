export default class NewArray extends Array {

    setCb(cb) {
        this.cb = (cb && cb.bind(this)) || (() => { });
    }

    push(...items) {
        let len = super.push(...items);
        this.cb();
        return len;
    }


    pop() {
        let any = super.pop()
        this.cb();
        return any;
    }

    splice() {
        let removes;
        removes = super.splice(...arguments);
        this.cb();
        return removes;
    }

    concat(...items) {
        let n = super.concat(...items)
        this.cb();
        return n;
    }

    fill() {
        let n = super.fill(...arguments);
        this.cb();
        return n;
    }

    reverse() {
        let n = super.reverse();
        this.cb();
        return n;
    }

    shift() {
        let n = super.shift();
        this.cb();
        return n;
    }

    unshift() {
        let n = super.unshift(...arguments);
        this.cb();
        return n;
    }

    sort() {
        let n = super.sort(...arguments);
        this.cb();
        return n;
    }

    clear() {
        this.length = 0;
        this.cb();
        return this.length;
    }

}