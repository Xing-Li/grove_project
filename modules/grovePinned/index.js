const path = require("path");
const express = require("express");
const async = require("async");
const _ = require("lodash");

const GrovePinned = require("./GrovePinned.model");

const Auth = require("./../util/auth");
const Util = require("./../util/util");
const Logger = require("./../logger");

const GLogger = Logger.getLogger(__filename);
const GAuditProjectFields = ["_id", "user", "projectId", "fileName", "fileKey"];

const routes = express.Router();

routes.all("/add", Auth.protected, function (req, res) {
  let grovePinnedItem = req.parameters.item || {};
  async.waterfall(
    [
      (callback) => {
        if (grovePinnedItem._id) {
          GrovePinned.getInfo(grovePinnedItem._id, callback);
        } else {
          callback(null, {});
        }
      },
      (oldItem, callback) => {
        GrovePinned.add(req.parameters.projectId, req.parameters.userId, grovePinnedItem, function (err, item) {
          callback(
            err,
            item || {
              _id: grovePinnedItem._id,
              projectId: req.parameters.projectId,
              name: grovePinnedItem.name,
            },
            oldItem
          );
        });
      },
    ],
    (err, item, oldItem) => {
      GLogger.audit(
        req,
        `Pinned grove "${grovePinnedItem.fileName}" to project "${req.parameters.projectId}" `,
        grovePinnedItem._id ? Logger.AuditActionTypes.GrovePinnedModified : Logger.AuditActionTypes.GrovePinnedAdd,
        err ? Logger.AuditStatusTypes.Failed : Logger.AuditStatusTypes.Successful,
        item || grovePinnedItem,
        oldItem,
        GAuditProjectFields
      );
      res.send({
        status: err ? 1 : 0,
        message: err ? "tips.failed": "tips.successful",
        content: item,
      });
    }
  );
});

// routes.all("/getInfo", Auth.protectedShare, function (req, res) {
//   GrovePinned.getInfo(req.parameters.id, function (err, item) {
//     res.send({
//       status: err ? 1 : 0,
//       message: err ? err.message : "Successful.",
//       content: item,
//     });
//   });
// });

routes.all("/remove", Auth.protected, function (req, res) {
  GrovePinned.del(
    req.parameters.id,
    function (err, oldItem) {
      GLogger.audit(
        req,
        `Un-pinned grove "${oldItem.fileName}" from project "${oldItem.projectId}" `,
        Logger.AuditActionTypes.GrovePinnedDelete,
        err ? Logger.AuditStatusTypes.Failed : Logger.AuditStatusTypes.Successful,
        {},
        oldItem,
        GAuditProjectFields
      );
      res.send({
        status: err ? 1 : 0,
        message: err ? err.message : "Successful.",
      });
    },
    true
  );
});

routes.all("/getAll", Auth.protectedManager, function (req, res) {
  res.send({
    status: 0,
    message:  "tips.successful",
  });
});

module.exports = routes;
