const express = require("express");
const _ = require("lodash");
const async = require("async");
const boltService = require("../bolt/boltService");
const Neo4jDBModel = require("../neo4jDB/neo4jDB.model");
const { duplicateProjectFolder } = require("../../routes/api/grove");
const CustomDB = require("./customDB.model");
const readUserDBInfoSession = require("./readUserDBInfoSession");
const getProjectWithIdOrHost = require("./getProjectWithIdOrHost");
const writeUserDBInfoSession = require("./writeUserDBInfoSession");
const excuteCommandWithProjectIdAndCommand = require("./excuteCommandWithProjectIdAndCommand");
const Perspective = require("../perspectives/perspective.model");
const CryptoUtil = require("../util/cryptoUtil");
const Auth = require("../util/auth");
const Util = require("../util/util");
const UIConfig = require("../config/moduleConfig");
const Logger = require("../logger");
const AppCorsConfig = require('AppConfig').cors || {  };
const GLogger = Logger.getLogger(__filename);
const ConvertedUIConfig = Util.convertUIConfig(UIConfig);
const SampleQueryConfig = require("./sampleQuerysConfig");
const router = express.Router();
const GAuditProjectFields = ["_id", "projectName", "hostname", "boltPort", "isShare", "isDemo", "isLocal"];

function removeUserDBInfoSession(req, projectId) {
  let dbs = req.session.dbs || {};
  if (dbs[projectId]) {
    delete dbs[projectId];
    req.session.dbs = dbs;
  }
}

router.all("/updateLastActiveTime", Auth.protected, function (req, res) {
  CustomDB.updateLastActiveTime(req.parameters.id, (err, item) => {
    res.send({
      status: 0,
      message: "tips.actionTips.updatedSuccessfully",
      content: item,
    });
  });
});

//only for neo4j
router.all(
  "/excuteCommand",
  (req, res, next) => {
    const QUERY_ID_PREFIX = "queryId:";
    // Expect req.parameters.command to look like "queryId:<queryId>", where
    // queryId is the id of a known Cypher query in this project's queryCollections.
    // If there's no queryId, and the user isn't authenticated and the query isn't whitelisted, reject the request.
    if (!req.parameters.command.startsWith(QUERY_ID_PREFIX)) {
      // Command is raw Cypher, like MATCH (n) RETURN n
      // Let authenticated users through
      if (req.isAuthenticated() || AppCorsConfig.allowAnonymousQuery) {
        return next();
      }

      // Let known legacy queries through
      if (
        [
          /* TbRNAT */
          /^\s*WITH {\s+nodeIds:\[.*\]\s+} as params\s+MATCH \(a\)-\[r\]-\(b\)\s+WHERE\s+id\(a\) IN params\.nodeIds\s+AND id\(b\) IN params\.nodeIds\s+RETURN r\s*$/,
          /^\s*match \(g1:TbRNAT\)-\[r:TBREGNET\]-\(g2:TbRNAT\) where g1\.ncbiGeneLocus =~'\(\?i\)\.\*\w+\.\*' OR g1\.ncbiGeneDescription =~'\(\?i\)\.\*\w+\.\*' OR g1\.ncbiGeneSymbol =~'\(\?i\)\.\*\w+\.\*' return \*\s*$/,
          /^\s*MATCH\s*\(n:Events\)\s*where\s*n\.title\s*=~\s*"\(\?i\).*Tuberculosis.*"\s*RETURN\s*n\.endDate\s*as\seventenddate,\s*n\.title\s*as\seventtitle,\s*n\.location\s*as\seventlocation,\s*n\.type\s*as\seventtype,\s*n\.startDate\s*as\seventstartdate,\s*n\.url\s*as\seventurl\s*$/,
          /^\s*MATCH\s*\(n:Publication\)\s*where\s*n\.title\s*=~\s*"\(\?i\).*Tuberculosis.*"\s*RETURN\s*n\.journal\s*as\spubjournal,\s*n\.pm_date\s*as\spubdate,\s*n\.title\s*as\spubtitle,\s*n\.pmid\s*as\spubmedID\s*ORDER\s*BY\s*n\.pm_timestamp\s*DESC\s*$/,
          /^\s*MATCH\s*\(n:Applications\)\s*where\s*n\.keywords\s*=~\s*"\(\?i\).*tuberculosis.*"\s*RETURN\s*n\.appName\s*as\s*appname,\s*n\.appShortdescription\s*as\s*appdesc,\s*n\.appUrl\s*as\s*appurl\s*$/,

          /* GraphXR */
          /^\s*CALL db.schema.visualization\(\)\s*$/,
          /^\s*CALL\s+db\.labels\(\)\s+YIELD\s+label\s+RETURN\s+\{name:'labels',\s+data:COLLECT\(label\)\[..1000\]\}\s+as\s+result\s+UNION\s+ALL\s+CALL\s+db\.relationshipTypes\(\)\s+YIELD\s+relationshipType\s+RETURN\s+\{name:'relationships',\s+data:COLLECT\(relationshipType\)\[..1000\]\}\s+as\s+result\s*$/,
          /^\s*MATCH .* RETURN '.+' as label, keys\(n\) as props LIMIT 1\s*$/,
          /RETURN type\(r\) as relationship, keys\(r\) as props LIMIT 1/,
          /^\s*CALL apoc\.meta\.data\(\) yield label, property, index, type, array, other, elementType\s*$/,

          /* Full text search */
          /^\s*SHOW INDEXES\s+yield name ,labelsOrTypes as label, properties as props, entityType , type\s+where type = "FULLTEXT" and entityType = "NODE"\s+return name as indexName, label as labels, props\s*$/,
          /^\s*CALL db.index.fulltext.queryNodes\(\$searchIndexKeyName, \$searchRegStr\) YIELD node, score RETURN node LIMIT 3000\s*$/,

          /* Allow pull from category and relationship  refer https://regexr.com/7jkjt*/
          /^\s*MATCH\s+([a-z]+=)?\([a-z:`[\]_\-><()]*\)\s+RETURN\s+[a-z,*]+\s+limit\s+\d+\s*?$/ig,

          /* Cypher query panel */
          // Probably don't need to allow SHOW PROCEDURES
          ///^\s*SHOW PROCEDURES YIELD name, signature, mode WHERE mode <> "WRITE" and mode <> 'DBMS' RETURN {2}name, signature\s*$/

          // Probably shouldn't allow SHOW DATABASES. The Project Settings and Data tab use it.
          ///^\s*SHOW DATABASES\s*$/,
        ].some((r) => r.test(req.parameters.command))
      ) {
        return next();
      }

      GLogger.error("No queryId provided. You need to provide a queryId or be authenticated to run Cypher.");
      GLogger.error(req.parameters.command);
      return res.send({
        status: 1,
        message: "No queryId provided. You need to provide a queryId or be authenticated to run Cypher.",
        content: null,
      });
    }

    // Lookup query by id from the database.
    // If it exists, replace req.parameters.command with the query.
    // If it doesn't exist, return an error.
    const queryId = req.parameters.command.substring(QUERY_ID_PREFIX.length);
    CustomDB.getInfo(req.parameters.projectId, (err, project) => {
      err = err || (!project ? new Error("Can not find the project") : null);
      if (err) {
        return res.send({
          status: 1,
          message: err.message,
          content: err,
        });
      }

      /*
       * Example project
       * {
       * 		...,
       *		"queryCollections": [
       *			{
       *				"queryId": "MATCH (n:Person) RETURN n LIMIT 25"
       *			}
       *		]
       *	}
       */
      const command = project.queryCollections.find((q) => queryId in q);
      if (!command) {
        return res.send({
          status: 1,
          message: "Cannot find the query",
          content: null,
        });
      }

      // Replace req.parameters.command with the query so that the next middleware can run it.
      req.parameters.command = command[queryId];
      GLogger.log('command is', req.parameters.command);
      GLogger.log('params is', req.parameters.params);
      return next();
    });
  },
  function (req, res) {
    let _id = req.parameters.projectId;
    excuteCommandWithProjectIdAndCommand(
      req,
      req.parameters.projectId,
      req.parameters.command,
      req.parameters.queryLimit,
      req.parameters.options,
      req.parameters.params
    )
      .then((data) => {
        GLogger.audit(
          req,
          `Run neo4j Query ${req.parameters.command}`,
          Logger.AuditActionTypes.Neo4jQuery,
          Logger.AuditStatusTypes.Successful,
          { _id },
          { _id }
        );

        return res.send({
          status: 0,
          message: "tips.actionTips.querySuccessfully",
          content: data,
        });
      })
      .catch((err) => {
        GLogger.audit(
          req,
          `Run neo4j Query ${req.parameters.command}`,
          Logger.AuditActionTypes.Neo4jQuery,
          Logger.AuditStatusTypes.Failed,
          { _id },
          { _id }
        );

        return res.send({
          status: 1,
          message: err.message,
          content: null,
        });
      });
  }
);

router.all("/neo4jBaseInfo/SampleQuery", function (req, res) {
  let labels = req.parameters.labels || [];
  let neo4jBaseInfo = {
    indexableKeys: [],
    icon: "",
    iconMap: {},
    models: [],
    useModel: false,
  };
  if (labels.length > 0) {
    let matchSampleQuery = _.find(SampleQueryConfig, (item) => {
      return item.needLabels.filter((lable) => labels.includes(lable)).length == item.needLabels.length;
    });

    if (matchSampleQuery) {
      Object.assign(neo4jBaseInfo, matchSampleQuery);
      neo4jBaseInfo.models = neo4jBaseInfo.models || [];
    }
  }
  return res.send({
    status: 0,
    message: "tips.successful",
    content: neo4jBaseInfo || {},
  });
});

router.all("/neo4jBaseInfo", function (req, res) {
  let projectId = req.parameters.projectId;

  async.waterfall(
    [
      //0. ready struct
      function (callback) {
        let neo4jBaseInfo = {
          querys: [],
          sqlCollections: [],
          mysqlCollections: [],
          labelConfigs: [],
          relationshipConfigs: [],
          searchKey: "man",
          searchIndex: "graphxrIndex",
        };
        callback(null, neo4jBaseInfo);
      },

      // //1. get sampleQuery config info, refer neo4jBaseInfo/SampleQuery
      // function (neo4jBaseInfo, callback) {

      // 	if (labels.length > 0) {
      // 		let matchSampleQuery = _.find(SampleQueryConfig, (item) => {
      // 			return item.needLabels.filter(lable => labels.includes(lable)).length == item.needLabels.length;
      // 		})

      // 		if (matchSampleQuery) {
      // 			Object.assign(neo4jBaseInfo, matchSampleQuery);
      // 			neo4jBaseInfo.models = neo4jBaseInfo.models || [];
      // 		}
      // 	}

      // 	callback(null, neo4jBaseInfo);
      // },

      //2. get project config
      function (neo4jBaseInfo, callback) {
        CustomDB.findDBConfigs(projectId, (err, resProject) => {
          neo4jBaseInfo.labelConfigs = !err && resProject ? resProject.labels : [];
          neo4jBaseInfo.relationshipConfigs = !err && resProject ? resProject.relationships : [];
          neo4jBaseInfo.searchKey =
            !err && resProject && resProject.searchKey ? resProject.searchKey : neo4jBaseInfo.searchKey;
          neo4jBaseInfo.searchIndex =
            !err && resProject && resProject.searchIndex ? resProject.searchIndex : neo4jBaseInfo.searchIndex;

          neo4jBaseInfo.customQuery = !err && resProject ? resProject.queryCollections : [];
          neo4jBaseInfo.sqlCollections = !err && resProject ? resProject.sqlCollections : [];
          neo4jBaseInfo.mysqlCollections = !err && resProject ? resProject.mysqlCollections : [];
          neo4jBaseInfo.postgresqlCollections = !err && resProject ? resProject.postgresqlCollections : [];

          callback(err, neo4jBaseInfo);
        });
      },
    ],
    function (err, neo4jBaseInfo) {
      //allow access /p/Neo4jDesktop|configDemoHost|default/xxx
      if (!/^\/p\/(Neo4jDesktop|configDemoHost|default)\/.+/gi.test(req.originalUrl)) {
        return res.send({
          status: 0,
          message: "tips.successful",
          content: neo4jBaseInfo || {},
        });
      } else if(err){
        return res.send({
          status: 0,
          message: err && err.message ? err.message : "tips.failed",
          content: neo4jBaseInfo || {},
        });
      } else {
        return res.send({
          status: 0,
          message: "tips.successful",
          content: neo4jBaseInfo || {},
        });
      }
    }
  );
});

router.post("/label/:labelname/properties/", Auth.protected, function (req, res) {
  CustomDB.updateLabelConfig(req.parameters.projectId, req.parameters.labelConfig, (err, item) => {
    res.send({
      status: 0,
      message: "tips.actionTips.updatedSuccessfully",
      content: item,
    });
  });
});

router.post("/label/:labelname/delete/", Auth.protected, function (req, res) {
  CustomDB.deleteLabelConfig(req.parameters.projectId, req.parameters.labelConfig, (err, item) => {
    res.send({
      status: 0,
      message: "tips.actionTips.deletedSuccessfully",
      content: item,
    });
  });
});

router.post("/relationship/:relationshipName/", Auth.protected, function (req, res) {
  CustomDB.updateRelationshipConfig(req.parameters.projectId, req.parameters.relationshipConfig, (err, item) => {
    res.send({
      status: 0,
      message: "tips.actionTips.updatedSuccessfully",
      content: item,
    });
  });
});

router.post("/addQuery", Auth.protected, function (req, res) {
  CustomDB.addQuery(req.parameters.projectId, req.parameters.query, (err, item) => {
    res.send({
      status: 0,
      message: "tips.actionTips.addedSuccessfully",
      content: item,
    });
  });
});

router.post("/deleteQuery", Auth.protected, function (req, res) {
  CustomDB.deleteQuery(req.parameters.projectId, req.parameters.query, (err, item) => {
    res.send({
      status: 0,
      message: "tips.actionTips.deletedSuccessfully",
      content: item,
    });
  });
});

router.post("/addSQLQuery", Auth.protected, function (req, res) {
  CustomDB.addSQLQuery(req.parameters.projectId, req.parameters.collectionsName, req.parameters.query, (err, item) => {
    res.send({
      status: 0,
      message: "tips.actionTips.addedSuccessfully",
      content: item,
    });
  });
});
router.post("/deleteSQLCollection", Auth.protected, function (req, res) {
  CustomDB.deleteSQLQuery(req.parameters.projectId, req.parameters.collectionsName, req.parameters.query, (err, item) => {
    res.send({
      status: 0,
      message: "tips.actionTips.deletedSuccessfully",
      content: item,
    });
  });
});

router.all("/getDemoHosts", Auth.protected, function (req, res) {
  Neo4jDBModel.getActiveDBs(req.parameters.userId, req.isManager(), (err, dbs) => {
    res.send({
      status: err ? 1 : 0,
      message: err ? "tips.failed" : "tips.successful",
      content: _.uniqBy(
        (dbs || []).map(function (dbItem) {
          return {
            name: dbItem.get("name"),
            host: `${dbItem.get("neo4j.hostname")}:${dbItem.get("neo4j.boltPort")}:${dbItem.get("neo4j.currentNeo4jDB")}`,
          };
        }),
        "host"
      ),
    });
  });
});

//@sean need improve the configDemoHost api
router.all("/configDemoHost", Auth.protected, function (req, res) {
  let host = req.parameters.host;
  let id = req.parameters.id;
  async.waterfall(
    [
      //1. try got the project
      function (callback) {
        getProjectWithIdOrHost(host, true)
          .then((project) => {
            callback(null, project);
          })
          .catch(() => {
            callback(null, null);
          });
      },
      //2. try use custom project
      function (project, callback) {
        if (!project) {
          project = {
            dbname: "configDemoHost",
            hostname: host.split(":")[0],
            host: host,
            httpPort: 0,
            boltPort: host.split(":")[1],
            username: "neo4j",
            password: "",
            isDemo: false,
            isLocal: false,
          };

          boltService.checkConnect(project.hostname, project.boltPort, project.username, project.password, (err) => {
            callback(err, project);
          });
        } else {
          callback(null, project);
        }
      },
      //3. write the project to session;
      function (project, callback) {
        project._id = id;
        project.isShare = true;
        writeUserDBInfoSession(req, project);
        callback(null, project);
      },
    ],
    (err) => {
      res.send({
        status: err ? 1 : 0,
        message: err ? "tips.failed" : "tips.successful",
      });
    }
  );
});

/**---CustomDB---*/

router.post("/project/checkConnect", Auth.protectedShare, function (req, res) {
  let dbinfo = req.parameters.dbinfo || {};
  dbinfo.boltPort = parseInt(dbinfo.boltPort) || 7687;

  boltService.checkConnect(
    dbinfo.hostname,
    dbinfo.boltPort,
    dbinfo.username,
    dbinfo.password,
    function (err, resData) {
      return res.send({
        status: err ? 1 : 0,
        message: err ? err.message : "tips.successful",
        content: resData,
      });
    },
    true
  );
});

router.post("/project/tourSearchKey", Auth.protected, function (req, res) {
  let searchkey = req.parameters.searchKey || "s";
  let projectId = req.parameters.projectId;

  CustomDB.updateSearchKey(projectId, searchkey, (err) => {
    res.send({
      status: err ? 1 : 0,
      message: err ? err.message : "tips.successful",
    });
  });
});

router.post("/project/searchIndex", Auth.protected, function (req, res) {
  let searchIndex = req.parameters.searchIndex || "graphxrIndex";
  let projectId = req.parameters.projectId;

  CustomDB.updateSearchIndex(projectId, searchIndex, (err) => {
    res.send({
      status: err ? 1 : 0,
      message: err ? err.message : "tips.successful",
    });
  });
});

router.post("/project/add", Auth.protected, function (req, res) {
  let dbinfo = req.parameters.dbinfo || {};
  let dbId = dbinfo._id;
  if (dbinfo.hostname !== "") {
    dbinfo.boltPort = parseInt(dbinfo.boltPort) || 7687;
    boltService.cleanCacheNeo4jDrivers(`${dbinfo.hostname}:${dbinfo.boltPort}`);
    if (dbId) {
      removeUserDBInfoSession(req, dbId);
    }
  }

  async.waterfall(
    [
      //1. chech host
      function (callback) {
        //1.1 just save for local Neo4j db

        if (dbinfo.isLocal) {
          callback(null);

          //1.2 Check the connect for neo4j
        } else if (dbinfo.hostname) {
          boltService.checkConnect(dbinfo.hostname, dbinfo.boltPort, dbinfo.username, dbinfo.password, function (err) {
            callback(err);
          });
        } else {
          callback(null);
        }
      },
      //2. save to db
      function (callback) {
        CustomDB.addProject(req.parameters.userId, dbinfo, (err, item, oldItem) => {
          callback(err, item, oldItem);
        });
      },
    ],
    function (err, newItem, oldItem) {
      GLogger.audit(
        req,
        `${dbId ? "Edit" : "Add"} project "${dbinfo.projectName}"`,
        !dbId ? Logger.AuditActionTypes.ProjectAdded : Logger.AuditActionTypes.ProjectModified,
        err ? Logger.AuditStatusTypes.Failed : Logger.AuditStatusTypes.Successful,
        newItem,
        oldItem,
        GAuditProjectFields
      );

      res.send({
        status: err ? 1 : 0,
        message: err ? "tips.failed" : "tips.successful",
        content: newItem,
      });
    }
  );
});

router.post("/project/addDemo", Auth.protected, function (req, res) {
  let host = req.parameters.host;
  async.waterfall(
    [
      //1. got the project for host
      function (callback) {
        getProjectWithIdOrHost(host, true)
          .then((project) => {
            callback(null, project);
          })
          .catch((err) => {
            callback(err);
          });
      },
      //2. save the project as project
      function (project, callback) {
        boltService.cleanCacheNeo4jDrivers(`${project.hostname}:${project.boltPort}`);

        CustomDB.addProject(
          req.parameters.userId,
          {
            isDemo: true,
            username: project.username,
            password: "",
            hostname: project.hostname,
            boltPort: project.boltPort,
            currentNeo4jDB: project.currentNeo4jDB,
            projectName: project.projectName,
          },
          callback
        );
      },
    ],
    (err, newItem, oldItem) => {
      newItem = newItem || {};
      oldItem = oldItem || {};
      GLogger.audit(
        req,
        `Add project "${newItem.projectName}"`,
        Logger.AuditActionTypes.ProjectAdded,
        err ? Logger.AuditStatusTypes.Failed : Logger.AuditStatusTypes.Successful,
        newItem,
        oldItem,
        GAuditProjectFields
      );

      res.send({
        status: err ? 1 : 0,
        message: err ? "tips.actionTips.addedFailed" : "tips.actionTips.addedSuccessfully",
        content: newItem,
      });
    }
  );
});

router.post("/project/delete", Auth.protected, function (req, res) {
  let projectId = req.parameters.projectId;
  Perspective.removePerspectives(req.parameters.userId, req.parameters.id, projectId, true, function (err) {
    if (err) GLogger.error("Clean perspective failed", err);
  });

  getProjectWithIdOrHost(projectId)
    .then((project) => {
      boltService.cleanCacheNeo4jDrivers(`${project.hostname}:${project.boltPort}`);
      if (project._id) {
        removeUserDBInfoSession(req, project._id);
      }

      CustomDB.deleteProject(req.parameters.userId, projectId, (err) => {
        res.send({
          status: err ? 1 : 0,
          message: err ? "tips.actionTips.deletedFailed" : "tips.actionTips.deletedSuccessfully",
        });
      });
      GLogger.audit(
        req,
        `Delete the project "${project.projectName}"`,
        Logger.AuditActionTypes.ProjectDeleted,
        Logger.AuditStatusTypes.Successful,
        {},
        project,
        GAuditProjectFields
      );
    })
    .catch(() => {
      GLogger.audit(
        req,
        `Delete the project "${projectId}"`,
        Logger.AuditActionTypes.ProjectDeleted,
        Logger.AuditStatusTypes.Failed,
        {},
        { _id: projectId }
      );
      res.send({
        status: 1,
        message: "tips.actionTips.deletedFailed",
      });
    });
});

router.post("/project/clone", Auth.protected, function (req, res) {
  async.waterfall(
    [
      (callback) => {
        CustomDB.getInfo(req.parameters.projectId, (err, project) => {
          err = err || (!project ? new Error("Can not found the project") : null);
          callback(err, project);
        });
      },
      (project, callback) => {
        project = JSON.parse(JSON.stringify(project));
        if (!req.isManager() && project.user !== req.parameters.userId) {
          return callback(new Error("tips.limitTips.onlyCloneYourProject"));
        }

        let dbInfo = Object.assign({}, project, {
          projectName: req.parameters.newProjectName || `${project.projectName}_copy`,
        });
        delete dbInfo._id;

        CustomDB.cloneProject(req.parameters.userId, dbInfo, (err, item) => {
          callback(err, item || dbInfo);
        });
      },
    ],
    (err, resData) => {
      let newItem = resData || { _id: req.parameters.projectId };
      GLogger.audit(
        req,
        `Clone project "${newItem.projectName || newItem._id}"`,
        Logger.AuditActionTypes.ProjectAdded,
        err ? Logger.AuditStatusTypes.Failed : Logger.AuditStatusTypes.Successful,
        newItem,
        {},
        GAuditProjectFields
      );
      let fromProjectId = req.parameters.projectId;
      let toProjectId = newItem._id.toString();
      let ret = req.parameters.notCloneGrove ? undefined : duplicateProjectFolder(fromProjectId, toProjectId);
      if (ret) {
        return res.send({
          status: ret ? 1 : 0,
          message: ret ? ret.message || "tips.actionTips.cloneFailed" : "tips.actionTips.cloneSuccessfully",
          content: newItem,
        });
      }
      res.send({
        status: err ? 1 : 0,
        message: err ? err.message || "tips.actionTips.cloneFailed" : "tips.actionTips.cloneSuccessfully",
        content: newItem,
      });
    }
  );
});

router.post("/project/updateCurrentNeo4jDB", Auth.protected, function (req, res) {
  const currentNeo4jDB = req.parameters.currentNeo4jDB || "";
  const projectId = req.parameters.projectId;

  if (projectId) {
    removeUserDBInfoSession(req, projectId);
  }

  CustomDB.updateField(projectId, "currentNeo4jDB", currentNeo4jDB, (err) => {
    res.send({
      status: err ? 1 : 0,
      message: err ? err.message : "tips.actionTips.updatedSuccessfully",
    });
  });
});

router.post("/project/share", Auth.protected, function (req, res) {
  CustomDB.shareProject(req.parameters.projectId, req.parameters.isShare, (err, oldProject) => {
    let newItem = { _id: req.parameters.projectId, isShare: req.parameters.isShare };
    let oldItem = { _id: req.parameters.projectId, isShare: !req.parameters.isShare };

    GLogger.audit(
      req,
      `The project "${oldProject.projectName || newItem._id}" ${newItem.isShare ? "enable share" : "disable share"} `,
      req.parameters.isShare ? Logger.AuditActionTypes.ProjectShareOpened : Logger.AuditActionTypes.ProjectShareClosed,
      err ? Logger.AuditStatusTypes.Failed : Logger.AuditStatusTypes.Successful,
      newItem,
      oldItem
    );

    res.send({
      status: err ? 1 : 0,
      message: err ? "tips.actionTips.shareFailed" : "tips.actionTips.shareSuccessfully",
    });
  });
});

router.post("/project/note", Auth.protected, function (req, res) {
  CustomDB.editNote(req.parameters.projectId, req.parameters.note, (err, item) => {
    res.send({
      status: err ? 1 : 0,
      message: err ? "tips.failed" : "tips.successful",
      content: item,
    });
  });
});

router.all("/project/list", Auth.protected, function (req, res) {
  CustomDB.listProject(req.parameters.userId, (err, items) => {
    res.send({
      status: err ? 1 : 0,
      message: err ? "tips.failed" : "tips.successful",
      content: items,
    });
  });
});

router.all("/project/getInfo", function (req, res) {
  //always clean the DB session.

  if (req.parameters.id == "configDemoHost") {
    return res.send({
      status: 0,
      message: "tips.successful",
      content: readUserDBInfoSession(req, req.parameters.id),
    });
  }
  removeUserDBInfoSession(req, req.parameters.id);

  getProjectWithIdOrHost(req.parameters.id)
    .then((project) => {
      //project the share item
      if (project && !project.isShare && !req.isAuthenticated()) {
        res.send({
          status: 401,
          message: "tips.successful",
        });
      } else {
        let isRequireDecrypt = req.isManager() || req.parameters["userId"] == project.user;
        if (isRequireDecrypt && project.isLocal) {
          project.password = CryptoUtil.decrypt(project.password);
          project.username = CryptoUtil.decrypt(project.username);
        } else {
          delete project.password;
        }
        res.send({
          status: 0,
          message: "tips.successful",
          content: project,
        });
      }
    })
    .catch(() => {
      res.send({
        status: 1,
        message: "tips.failed",
      });
    });
});

router.all("/project/connect", Auth.protectedShare, function (req, res) {
  //1. already auth
  if (readUserDBInfoSession(req, req.parameters.id)) {
    let project = readUserDBInfoSession(req, req.parameters.id);
    return boltService.checkConnect(
      project.hostname,
      project.boltPort,
      project.username,
      project.password,
      function (err, resData) {
        res.send({
          status: 0,
          message: "tips.successful",
          content: resData,
        });
      },
      true
    );
  }

  //2. auth new item to project;
  getProjectWithIdOrHost(req.parameters.id, true)
    .then((project) => {
      return new Promise((reslove, reject) => {
        boltService.checkConnect(
          project.hostname,
          project.boltPort,
          project.username,
          project.password,
          function (err, resData) {
            err
              ? reject(err)
              : reslove({
                  project,
                  resData,
                });
          },
          true
        );
      });
    })
    .then(({ project, resData }) => {
      writeUserDBInfoSession(req, project);
      res.send({
        status: 0,
        message: "tips.successful",
        content: resData,
      });
    })
    .catch((err) => {
      GLogger.error("/connect :", err);
      res.send({
        status: 1,
        message: "tips.failed",
      });
    });
});

router.post(["/project/updateForceLayoutSettings"], Auth.protected, function (req, res) {
  CustomDB.updateForceLayoutSettings(req.parameters.projectId, req.parameters.forceLayoutSettings, (err) => {
    res.send({
      status: err ? 1 : 0,
      message: err ? err.message : "tips.actionTips.updatedSuccessfully",
    });
  });
});

router.all("/project/getForceLayoutSettings", function (req, res) {
  CustomDB.getForceLayoutSettings(req.parameters.projectId, (err, item) => {
    let forceLayoutSettings =
      item && item.forceLayoutSettings ? JSON.parse(JSON.stringify(item.forceLayoutSettings)) : {};
    res.send({
      status: err ? 1 : 0,
      message: err ? err.message : "tips.successful",
      content: forceLayoutSettings,
    });
  });
});

router.post(["/project/updateProjectSettings"], Auth.protected, function (req, res) {
  CustomDB.updateProjectSettings(req.parameters.projectId, req.parameters.projectSettings, (err) => {
    res.send({
      status: err ? 1 : 0,
      message: err ? err.message : "tips.actionTips.updatedSuccessfully",
    });
  });
});

router.all("/project/getProjectSettings", function (req, res) {
  CustomDB.getProjectSettings(req.parameters.projectId, (err, item) => {
    let projectSettings = item && item.projectSettings ? JSON.parse(JSON.stringify(item.projectSettings)) : {};
    res.send({
      status: 0,
      message: "tips.successful",
      content: projectSettings,
    });
  });
});

router.post(["/project/updateLabels"], Auth.protected, function (req, res) {
  CustomDB.updateLabels(req.parameters.projectId, req.parameters.labels, (err) => {
    res.send({
      status: err ? 1 : 0,
      message: err ? err.message : "tips.actionTips.updatedSuccessfully",
    });
  });
});

router.post(["/project/updateRelationships"], Auth.protected, function (req, res) {
  CustomDB.updateRelationships(req.parameters.projectId, req.parameters.relationships, (err) => {
    res.send({
      status: err ? 1 : 0,
      message: err ? err.message : "tips.actionTips.updatedSuccessfully",
    });
  });
});

router.post(["/project/updateUISetting"], Auth.protected, function (req, res) {
  let newItem = {
    _id: req.parameters.projectId,
    UISetting: req.parameters.UISetting,
  };
  CustomDB.updateUISetting(newItem._id, newItem.UISetting, (err, oldItem) => {
    GLogger.audit(
      req,
      `Config the project(${oldItem.projectName || newItem._id}) Share UI`,
      Logger.AuditActionTypes.ProjectShareUIModified,
      err ? Logger.AuditStatusTypes.Failed : Logger.AuditStatusTypes.Successful,
      newItem,
      {
        _id: req.parameters.projectId,
        UISetting: (oldItem || {}).UISetting || UIConfig,
      }
    );
    res.send({
      status: err ? 1 : 0,
      message: err ? err.message : "tips.actionTips.updatedSuccessfully",
    });
  });
});

router.all("/project/getUISetting", Auth.protectedShare, function (req, res) {
  CustomDB.getUISetting(req.parameters.projectId, (err, item) => {
    // convert any legacy and deep copy
    let tempUIConfig = item && item.UISetting ? Util.convertUIConfig(item.UISetting) : {};

    // mask against latest UIConfig
    tempUIConfig = Util.deepAssign(tempUIConfig, ConvertedUIConfig, { extensions: true });

    if (err) {
      GLogger.error("getUISetting:", err.message);
    }

    res.send({
      status: 0,
      message: "tips.successful",
      content: tempUIConfig,
    });
  });
});
module.exports = router;
