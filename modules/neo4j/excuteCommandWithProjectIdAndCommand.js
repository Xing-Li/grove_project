const async = require('async');
const Logger = require('../logger')
const GLogger = Logger.getLogger(__filename);
const boltService = require('../bolt/boltService');
const readUserDBInfoSession = require("./readUserDBInfoSession")
const getProjectWithIdOrHost = require("./getProjectWithIdOrHost")
const writeUserDBInfoSession = require("./writeUserDBInfoSession")
const ONE_DAY = 24 * 60 * 60 * 1000;

function toNumberIfPossible(str) {
	const trimmed = str.trim();
	return isNaN(trimmed) ? str : Number(trimmed);
    }

function handleArrayParams(params) {
	if (!params) {
		return null;
	}
	const arrayRegex = /\[([^\]]+)\]/;
	let myArrays = {};
	for (const key in params) {
		if (Object.hasOwnProperty.call(params, key)) {
			if (typeof params[key] === 'string') { 
				let value = params[key]
				let test = value.match(arrayRegex);
				GLogger.log(`Match result for key '${key}':`, test);
				if (test) {
					myArrays[key] = test[1].split(',').map(toNumberIfPossible);
				}
				else{
					myArrays[key] = params[key]
				}
			} else {
				myArrays[key] = params[key]
			}
		}
	}
	return myArrays;
}


function excuteCommandWithProjectIdAndCommand(req, projectId, command, limit, options, params) {
	projectId = projectId || "default";
	params = handleArrayParams(params)
	return new Promise((reslove, reject) => {

		async.waterfall([
			//0 check the parameters
			function (callback) {
				callback(!projectId || !command ? new Error("Missing parameters projectId and command.") : null)
			},

			//1. got the project infor
			//1.1. try got the project from sessions
			function (callback) {
				callback(null, readUserDBInfoSession(req, projectId));
			},

			//1.2. try got the project item from customDB.model  
			function (project, callback) {
				if (!project) {
					getProjectWithIdOrHost(projectId, true).then(projectItem => {
						project = projectItem;
						callback(null, project)
					}).catch(err => {
						callback(err, null)
					})
				} else {
					callback(null, project);
				}

			},
			//1.3 check the project and  try rewrite the project to sessions
			function (project, callback) {
				let currentTime = new Date().getTime();
				if (!project) {
					return callback(new Error("Cannot get project information"))
				}

				if (!project.lastTime || currentTime - project.lastTime > ONE_DAY) {
					project.lastTime = currentTime;
					writeUserDBInfoSession(req, project);
				}
				callback(null, project)

			},

			//2. try run the command
			function (project, callback) {
				console.log("xxxxxxxx", params)
				//   Only the owner user can write he/she project DB, Only manager can write the Demo DB,
				const userId = req.parameters?.userId ?? req.params?.userId;
				let enableWritePermission = (!project.isDemo && project.user === userId) || (project.isDemo && req.isManager()) || false;
				project.options = options;
				boltService.excuteCommand(req.sessionID, command, project, (err, data) => {
					callback(err, data)
				}, !enableWritePermission, limit, params)
			},

		], function (err, data) {
			if (err) {
				reject(err);
			} else {
				reslove(data);
			}


		})
	})

}

module.exports = excuteCommandWithProjectIdAndCommand;