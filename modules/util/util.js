const path = require('path')
const _ = require('lodash')
const fs = require('fs')
const http = require('http')
const zlib = require('zlib')
const Moment = require('moment')
const readline = require('readline')
const packageData = require('./../../package.json');
const { ROOT_PATH ,TEMP_PATH } = require('../../Constant');

let ShortMonths = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

Date.prototype.format = function (fmt, isShortMonth) {
    if (isShortMonth && fmt.indexOf('M') >= 0) {
        fmt = fmt.replace(/M+/g, "M");
    }
    let o = {
        "M+": !isShortMonth ? (this.getMonth() + 1) : ShortMonths[this.getMonth() + 1],
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3), //quarter (of a year) ;
        "S": this.getMilliseconds()
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (let k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1)
                ? (o[k])
                : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }
    return fmt;
};


Object.toType = (function toType(global) {
    return function (obj) {
        if (obj === global) {
            return "global";
        }
        return ({}).toString.call(obj).match(/\s([a-z|A-Z]+)/)[1].toLowerCase();
    }
})(this)

let util = function () { };
util.currentTime = () =>{
    return new Date().format("yyyyMMddhhmmss")
};
util.toDiffLog = function(obj){
    obj = obj || {};
    return Object.keys(obj).map(k => `${k}=${ typeof(obj[k]) !== 'object' ? obj[k] : '[object]' }`).join(';')
};
util.objectToURLQuery = function(params = {}){
    return Object.keys(params).map((key) => {
        return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&');
}
util.siteURLToPath = function (url = '') {
    if (!(/http(s)?:\/\//ig).test(url)) {
        return url;
    }
    if (/http(s)?:\/\/([a-z0-9-\.]+):(\d+)?/ig.test(url)) {
        return String(url).replace(/http(s)?:\/\/([a-z0-9-\.]+):(\d+)?/ig, 'http$1/$2/$3');
    } else {
        return String(url).replace(/http(s)?:\/\//ig, 'http$1/');
    }
}
util.pathToSiteURL = function (path = '') {
    if (!(/http(s)?\//ig).test(path)) {
        return path;
    }
    if (/http(s)?\/([a-z0-9-\.]+)\/(\d+)/ig.test(path)) {
        return String(path).replace(/http(s)?\/([a-z0-9-\.]+)\/(\d+)?/ig, "http$1://$2:$3");
    } else {
        return String(path).replace(/http(s)?\//ig, "http$1://");
    }
}

//Use cacheVersion to improve the  getVersion performance
let _cacheVersion = null;
util.getVersion = function () {
    if(_cacheVersion){
        return _cacheVersion;
    }

    // if .git/HEAD doesn't exist, it might be a git submodule
    if (!fs.existsSync(path.join(ROOT_PATH, './.git/HEAD'))) {
        return {
            version: packageData.version,
            released: packageData.released,
            branch: 'unknown',
            commit: 'unknown',
        }
    }

    //.git/HEAD
    //.git/refs/heads/
    let gitHead = fs.readFileSync(path.join(ROOT_PATH, './.git/HEAD')).toString().trim();
    let gitCommit = (/[a-z0-9]{40,}/ig).test(gitHead) ? gitHead : fs.readFileSync(path.join(ROOT_PATH, `./.git/${gitHead.replace(/^ref[ :]+/ig, '')}`)).toString().trim();
    let gitBranch = gitHead.replace(/^ref[ :]+refs\/heads\//ig, '');

    _cacheVersion = {
        version: packageData.version,
        released: packageData.released,
        branch: gitBranch,
        //Top 8 chars as commit ID. Do not need full commit ID
        commit: String(gitCommit).slice(0,8), 
    };

    return _cacheVersion;
}


util.numberFormat = function numberFormat(numberValue, n, x) {
    let re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return parseFloat(numberValue).toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
};

util.isDate = function check(date) {
    if (typeof (date) == 'string' || date.length == 10) {
        //yyyy-MM-dd yyyy/MM/dd    MM-dd-yyyy MM/dd/yyyy
        return (/^\d{4}[-\/](0?[1-9]|1[0-2])[-\/](0?[1-9]|[1-2]\d|3[0-1])$/).test(date) ||
            (/^(0?[1-9]|[1-2]\d|3[0-1])[-\/](0?[1-9]|1[0-2])[-\/]\d{4}$/).test(date);
    } else {
        return false;
    }
}

util.formatSize = function formatSize(formattedSize) {
  var sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
  const parts = formattedSize.split(" ");
  const bytes = parts[0];
  const unit = parts[1];
  const l = sizes.indexOf(unit);
  return bytes * Math.pow(1024, l);
};

util.formatBytes = function formatBytes(bytes, decimals, unit) {
  if (bytes == 0) return "0 Byte";
  let k = 1024;
  let dm = decimals >= 0 ? decimals : 2;
  let sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
  let i = Math.floor(Math.log(bytes) / Math.log(k));
  if (sizes.indexOf(unit) > 0) {
    i = sizes.indexOf(unit);
  } else if (i >= sizes.length - 1) {
    i = sizes.length - 1;
  }
  //auto force keep 2 point when more TB
  if (i >= sizes.indexOf("TB") && sizes.indexOf(unit) < 0 && decimals == 0) {
    dm = 2;
  }

  return (
    util.numberFormat(parseFloat((bytes / Math.pow(k, i)).toFixed(dm)), dm) +
    " " +
    sizes[i]
  );
};

util.formatMoney = function formatMoney(moneys, decimals, unit) {
    if (moneys == 0) return '0';
    let k = 1000;
    let dm = decimals >= 0 ? decimals : 2;
    let sizes = ['', 'K', 'M', 'G', 'T', 'P', 'E'];//Kilo（10^3）、Mega（10^6）、Giga（10^9）、Tera（10^12）、Peta（10^15）、Exa（10^18） 
    let i = Math.floor(Math.log(moneys) / Math.log(k));
    if (sizes.indexOf(unit) > 0) {
        i = sizes.indexOf(unit);
    } else if (i >= sizes.length - 1) {
        i = sizes.length - 1;
    }
    return util.numberFormat(parseFloat((moneys / Math.pow(k, i)).toFixed(dm)), dm) + '' + sizes[i];
}

util.inArray = function (array, elm) {
    for (let key in array) {
        if (elm.toLowerCase() == array[key].toLowerCase()) {
            return true;
        }
    }
    return false;
}

util.trim = function (strObject) {
    let valueType = (typeof strObject).toLowerCase();
    if (valueType == 'string') {
        return strObject.trim();
    } else if (valueType == 'number') {
        return strObject;
    } else if (valueType == 'object') {
        for (let tempKey in strObject) {
            let tempValue = util.trim(strObject[tempKey]);
            if (tempKey != util.trim(tempKey)) {
                delete strObject[tempKey]; //remove old key/value include space
            }

            strObject[util.trim(tempKey)] = tempValue;

        }
        return strObject;
    } else {
        return "";
    }
}


util.getHost = function (req) {
    //get poxy hostName
    let hostName = req.headers['x-forwarded-host'] ? req.headers['x-forwarded-host'] : req.headers['x-forwarded-server'];
    hostName = hostName || (req.headers['origin'] ||  req.headers['host']).replace(/https|http/ig,'');
    const proto = (
    (/^https/ig).test(req.headers['referer'])
    || (/^https/ig).test(req.headers['origin']) 
    || req.headers['x-forwarded-proto'] === 'https' 
    || req.protocol ==='https'
    ) ? 'https' : 'http';
      return `${proto}://${(hostName.split(','))[0]}`.replace(/(\:\/\/)+/ig,'://');
}

util._getIP = function _getIP(req) {
    if(req.headers?.['x-real-ip']) {
        return req.headers?.['x-real-ip'];
    }else if(req.headers?.['x-forwarded-for']){
        return req.headers?.['x-forwarded-for']
    }else if(req.headers?.['x-client-ip']) {
        return req.headers?.['x-client-ip'];
    }else if(req.connection && req.connection.remoteAddress){
        return req.connection.remoteAddress;
    }else if(req.connection && req.connection.socket && req.connection.socket.remoteAddres ){
        return req.connection.socket.remoteAddress;
    }else if(req.socket && req.socket.remoteAddress){
        return req.socket.remoteAddress;
    }else if(req.ip){
        return req.ip
    }else if(req.ips && req.ips[0]){
        return req.ips[0]
    }else{
        return 'un-know'
    }
}

util.getIP = function getIP(req) {
    return util._getIP(req).replace(/^::ffff:/ig,'');
}

util.parseRawJSONData = function (req, res, next) {

    let contentType = req.headers['content-type'] || ''
        , mime = contentType.split(';')[0];

    if (mime != 'text/plain') {
        return next();
    }

    let data = '';
    req.setEncoding('utf8');
    req.on('data', function (chunk) {
        data += chunk;
    });
    req.on('end', function () {

        if (data.length > 0) {
            try {
                req.rawBody = JSON.parse(data);
            } catch (err) {
                console.log(err);
            }
        }
        return next();
    });
}

util.mergeReqParams = function (req, res, next) {
    req.parameters = {};

    //support raw body JSON Data
    if (req.rawBody) {
        for (let key in req.rawBody) {
            req.parameters[key] = req.rawBody[key];
        }
    }

    //Merge  req.query
    for (let queryKey in req.query) {
        req.parameters[queryKey] = req.query[queryKey];
    }

    //Merge  req.params
    for (let key in req.params) {
        req.parameters[key] = req.params[key];
    }

    //Merge  req.body
    for (let bodyKey in req.body) {
        req.parameters[bodyKey] = req.body[bodyKey];
    }

    //add session userId
    if (req.user && req.user._id) {
        req.parameters['userId'] = req.user.get("id");
    } else {
        req.parameters['userId'] = req.session && req.session.passport && req.session.passport.uId ? req.session.passport.uId : null;
    }

    return next();
}

util.mergeJSONKeyValue = function (req, res, next) {

    let JOSNKeyValue = req.parameters['json'];

    if (JOSNKeyValue && typeof (JOSNKeyValue) == "string") {
        try {
            let tempJSONObject = JSON.parse(JOSNKeyValue);
            if (tempJSONObject) {
                for (let jsonKey in tempJSONObject) {
                    req.parameters[jsonKey] = tempJSONObject[jsonKey];
                }
            }
        } catch (err) {
            console.log(err);
        }
    }
    next();
}

util.filterURL = function (req, res, next) {

    //has the content contain URL, eg <a href="https://www.google.com/">
    if (req.query.url && (/["'](http:\/\/)|(https:\/\/)/ig).test(req.query.url)) {
        // console.log("start URL:", req.query.url);
        //match the URL
        let matchURLs = req.query.url.match(/["'](http(s?):\/\/[\w\d\.\/\-\?\=\&\%]+)["']/ig);
        req.query.url = matchURLs && matchURLs.length > 0 ? matchURLs[0].replace(/["']/ig, '') : req.query.url;

        //fixed miss the query params
        Object.keys(req.query).forEach(qk => {
            if (qk !== 'url' && qk !== 'proxy') {
                req.query.url += `&${qk}=${req.query[qk]}`
            }
        })
        console.log("filter URL end:", req.query.url);
    }
    next();
}

util.randomAlphaNum = function (len) {
    let value = "";
    let i = 0;
    let charactors = "ab1cd2ef3gh4ij5kl6mn7opq8rst9uvw0xyz";
    for (let j = 1; j <= len; j++) {
        i = parseInt(35 * Math.random());
        value = value + charactors.charAt(i);
    }
    return value;
}

util.isNullOrEmpty = function (strVal) {

    if (typeof strVal == 'object') {
        return util.isEmptyObject(strVal);
    }
    if (strVal === '' || strVal === null || strVal === 'null' || strVal === undefined || strVal === 'undefined') {
        return true;
    } else {
        return false;
    }
}

util.isEmptyObject = function (O) {
    if (typeof O != 'object') {
        return true;
    }
    for (let x in O) {
        return false;
    }
    return true;
}

util.deepCopy = function (source) {
    let result;
    if (Array.isArray(source)) {
        result = [];
    } else if ((typeof source).toLowerCase() == 'object') {
        result = {};
    } else {
        return source;
    }
    for (let key in source) {
        result[key] = this.deepCopy(source[key]);
    }
    return result;
}

util.convertKeyValueStringToObject = function (keyValueString) {
    //Support value is key=value|key=value|key=value . auto convert to json.
    if ((typeof keyValueString).toLowerCase() == "string" && keyValueString.indexOf("=") > 0) {
        let tempObject = {};
        keyValueString.split("|").filter(function (s) { return s.length > 0 })
            .map(function (s) {//key=value|key=value|key=value
                let t = util.trim(s).split("=");
                //maybe have bug, will override old data
                tempObject[util.trim(t[0])] = util.trim(t[1]);
            });
        //replace key=value|key=value|key=value  to object
        return tempObject;
    }
    return keyValueString;
}

util.httpRequest = function (options, data, callback) {
    let postData = JSON.stringify(data),
        optionsDefault = {
            hostname: 'localhost',
            port: 80,
            path: '',
            method: 'POST',
            headers: {}
        };

    options = Object.assign(optionsDefault, options || {});

    _.forEach(options, function (v, k) {
        if (typeof (v) == 'string') {
            options[k] = v.trim();
        }
    })

    let req = http.request(options, function (res) {
        let str = '';

        res.setEncoding('utf-8');
        res.on('data', function (chunk) {
            str += chunk;
        });
        res.on('end', function () {
            callback(null, str);
        })
    })

    req.on('error', function (err) {
        callback(err);
    });
    req.write(postData);
    req.end();
}



util.rmDir = function (dirPath) {
    let files = [];
    try {
        files = fs.readdirSync(dirPath) || [];
    }
    catch (e) { return; }
    if (files && files.length > 0) {
        for (let i = 0; i < files.length; i++) {
            let filePath = dirPath + '/' + files[i];
            console.log('del:' + filePath)
            if (fs.existsSync(filePath) && fs.statSync(filePath).isFile()) {
                fs.unlinkSync(filePath);
            } else {
                this.rmDir(filePath);
            }
        }
    }

    if (fs.existsSync(dirPath)) {
        fs.rmdirSync(dirPath);
    }
};
util.realPath = function (filePath) {
    if(!fs.existsSync(filePath)){
        console.error(`util.realPath > ${filePath} no found`)
        return filePath;
    }
    let isLink = fs.statSync(filePath).isSymbolicLink();
    let linkPath = isLink ? fs.readlinkSync(filePath) : '';
    if (linkPath) {
        return path.resolve(linkPath)
    } else {
        return filePath
    }
}

util.rmFile = function (filePath) {
    if (fs.existsSync(filePath) && fs.statSync(filePath).isFile())
        fs.unlinkSync(filePath);
}
util.readFile = function (filePath, cb) {
    if (fs.existsSync(filePath) && fs.statSync(filePath).isFile()) {
        fs.readFile(filePath, { encoding: "utf8", flag: "r" }, cb);
    } else {
        cb(new Error("Not found the file"));
    }
}

util.readLinesWithGZOrCSV = function (filePath, lines, handleCallback) {
    let filename = filePath;
    const gzFileInput = fs.createReadStream(filename);
    const gunzip = filename.indexOf(".gz") >= 0 ? zlib.createGunzip() : gzFileInput;
    let countLines = 0;
    const rl = readline.createInterface({
        input: gunzip,
    }).on('line', line => {
        handleCallback(null, { type: "line", content: line });
        countLines++;
        if (lines > 0 && countLines == lines) {
            // handleCallback(null, {type:"done"});
            rl.close();
        }

    }).on('close', () => {
        handleCallback(null, { type: "done" });
    });

    if (filename.indexOf(".gz") >= 0) {
        gzFileInput.on('data', function (data) {
            gunzip.write(data);
        });
        gzFileInput.on('end', function () {
            gunzip.end();
        });
    }
}

util.readLinesWithCSV = function (filePath, lines, handleCallback) {

    if (!fs.existsSync(filePath) || !fs.statSync(filePath).isFile()) {
        handleCallback(1, 'no file found')
        return
    }
    let rd = readline.createInterface({
        input: fs.createReadStream(filePath),
    });
    let lineNumber = 0
    let linesContent = []
    rd.on('line', function (line) {
        lineNumber++
        if (lineNumber > lines) {
            rd.close()
        }
        else {
            linesContent.push(line)
        }

    });

    rd.on('close', function () {
        console.log(linesContent)
        handleCallback(0, linesContent)
    });
}


util.writeFile = function (filePath, content, cb) {
    let writeStream = fs.createWriteStream(filePath, {
        encoding: 'utf8'
    });
    content.forEach(e => {
        writeStream.write(e.join(',') + '\n');
    })
    writeStream.end(function () {
        cb()
    });
}

util.printMemoryUsage = function printMemoryUsage(startTime, bytes) {
    let info = process.memoryUsage();

    let useTime = !startTime ? null :
        "useTime=" + Moment().startOf('day')
            .seconds((new Date().getTime() - startTime.getTime()) / 1000)
            .format('HH:mm:ss');

    function mb(v) {
        return (v / MB).toFixed(2) + 'MB';
    }

    console.log(
        useTime, ' rss=', mb(info.rss), ' heapTotal=', mb(info.heapTotal), '  heapUsed=', mb(info.heapUsed)
    );
}

util.autoPrintMemory = function autoPrintMemory(props) {
    props = props || {};
    //stop
    if (props.timer) {
        clearInterval(props.timer);
        props.timer = null;
        this.printMemoryUsage(props.startTime);
    } else {
        props.timer = setInterval(() => {
            this.printMemoryUsage(props.startTime);
        }, 1000);
    }
    return props.timer;
}

/**
 *@param targetObject JSON Object, auto complate the attribute with templateObject
 *@param templateObject JSON template Object
 *  */
util.deepAssign = function deepAssign(targetObject, templateObject, keep = {}) {

    if (Object.toType(templateObject) !== 'array' && Object.toType(templateObject) !== 'object') {

        if (targetObject !== undefined) {
            return targetObject
        } else {
            return templateObject;
        }
    }

    if (Object.toType(targetObject) !== Object.toType(templateObject)) {
        targetObject = Object.toType(templateObject) === 'array' ? [] : {};
    }

    for (let key in templateObject) {
        targetObject[key] = this.deepAssign(targetObject[key], templateObject[key], keep);
    }

    //Clean the un-use prop;
    for (let key in targetObject) {
        if (templateObject[key] === undefined && !keep[key]) {
            delete targetObject[key];
        }
    }

    return targetObject;
}
/**
 *  compareVersions('1.0.1','1.0.100')
 */
util.compareVersions = function compareVersions(v1, v2) {

    function split(v) {
        let c = String(v).replace(/^v/, '').replace(/\+.*$/, '');
        let patchIndex = c.indexOf('-') === -1 ? c.length : c.indexOf('-');
        let arr = c.substring(0, patchIndex).split('.');
        arr.push(c.substring(patchIndex + 1));
        return arr;
    }

    function tryParse(v) {
        return isNaN(Number(v)) ? v : Number(v);
    }

    let s1 = split(v1)
    let s2 = split(v2)

    for (let i = 0; i < Math.max(s1.length - 1, s2.length - 1); i++) {
        let n1 = parseInt(s1[i] || 0, 10);
        let n2 = parseInt(s2[i] || 0, 10);

        if (n1 > n2) return 1;
        if (n2 > n1) return -1;
    }

    let sp1 = s1[s1.length - 1];
    let sp2 = s2[s2.length - 1];

    if (sp1 && sp2) {
        let p1 = sp1.split('.').map(tryParse);
        let p2 = sp2.split('.').map(tryParse);

        for (let i = 0; i < Math.max(p1.length, p2.length); i++) {
            if (p1[i] === undefined || typeof p2[i] === 'string' && typeof p1[i] === 'number') return -1;
            if (p2[i] === undefined || typeof p1[i] === 'string' && typeof p2[i] === 'number') return 1;

            if (p1[i] > p2[i]) return 1;
            if (p2[i] > p1[i]) return -1;
        }
    } else if (sp1 || sp2) {
        return sp1 ? -1 : 1;
    }

    return 0;
};

/**
 * @param {json} jsonNew {"a":{"b":[1,2]}}
 * @param {json} jsonOld {"a":{"b":[2,1]},"b":"c"}
 * @param {any} prevKey default undefined
 * @returns {b:{_new:'b',_old:'c'}}
 * 
 * e.g. util.JSONDiff({"a":{"b":[1,2]},"b":"b"},{"a":{"b":[2,1]},"b":"c"})
 */

util.JSONDiff = function JSONDiff(jsonNew, jsonOld, prevKey = undefined) {
  let _newOld = {};
  if (_.isEmpty(jsonNew) && _.isEmpty(jsonOld)) {
    return {};
  } else if (_.isEmpty(jsonOld)) {
    return { _new: jsonNew };
  } else if (_.isEmpty(jsonNew)) {
    return { _old: jsonOld };
  }

  if (Array.isArray(jsonNew)) {
    jsonNew = _.uniq(jsonNew).sort();
  }

  if (Array.isArray(jsonOld)) {
    jsonOld = _.uniq(jsonOld).sort();
  }

  _.uniq(Object.keys(jsonNew).concat(Object.keys(jsonOld)))
    .filter((p) => jsonNew[p] != jsonOld[p])
    .forEach((key) => {
      let _currentKey = `${prevKey !== undefined ? prevKey + "." : ""}${key}`;
      let newVal = jsonNew[key];
      let oldVal = jsonOld[key];
      if (typeof newVal === "object") {
        Object.assign(_newOld, util.JSONDiff(newVal, oldVal, _currentKey));
      } else if (newVal === undefined && oldVal !== undefined) {
        _newOld[_currentKey] = { _old: oldVal };
      } else if (newVal !== undefined && oldVal === undefined) {
        _newOld[_currentKey] = { _new: newVal };
      } else if (newVal !== undefined && oldVal !== undefined) {
        _newOld[_currentKey] = { _new: newVal, _old: oldVal };
      }
    });
  return _newOld;
};

util.hiddenPassword = function hiddenPassword(password) {
    return password ? `${String(password).slice(0,2)}****${String(password).slice(-2)}` : '';
}

util.clearObjectProps =function clearObjectProps(obj,ignoreFields) {
    if(typeof(obj) === "object" && obj.password){
        obj.password = util.hiddenPassword(obj.password)
    }
    if(typeof(obj) === "object" && !Array.isArray(obj)  && Array.isArray(ignoreFields)  && ignoreFields.length > 0){
        Object.keys(obj).filter(p => ignoreFields.includes(p)).forEach(p =>{
            delete obj[p];
        })
    }else{
        return obj;
    }
}

util.JSONDiffNewOld = function JSONDiffNewOld(jsonNew, jsonOld, ignoreFields) {
    ignoreFields = Object.assign(ignoreFields || {},['hash','salt','__v']);
    let _new = util.clearObjectProps(jsonNew, ignoreFields);
    let _old = util.clearObjectProps(jsonOld, ignoreFields);

    if(_.isEmpty(_new) || _.isEmpty(_old)){
        return {_new,_old};
    }
    
    if(Array.isArray(_new)){
        _new = _.uniq(_new).sort();
    }

    if(Array.isArray(_old)){
        _old = _.uniq(_old).sort();
    }
    let newVal = {};
    let oldVal = {};
    _.uniq(Object.keys(_new).concat(Object.keys(_old)))
    .filter(p => _new[p] !== _old[p])
    .forEach(p =>{
        if(typeof(_new[p]) === typeof(_old[p]) && _new[p] && typeof(_old[p]) === 'object'){
          let newOldProp =  util.JSONDiffNewOld(_new[p], _old[p], ignoreFields);
          if(Object.keys(newOldProp._new).length > 0){
            newVal[p] = newOldProp._new; 
          } 
          if(Object.keys(newOldProp._old).length > 0){
            oldVal[p] = newOldProp._old;
          } 
        }else if(_new[p] !== undefined && _old[p] === undefined){
            newVal[p] = _new[p];
        }else if(_new[p] === undefined && _old[p] !== undefined){
            oldVal[p] = _old[p];
        }else{
            newVal[p] = _new[p];
            oldVal[p] = _old[p];
        }

    })

    return {
          _new:newVal,
          _old:oldVal
    };
}

/**
 * If aliasPath is "/graphxr" and path is "/Projects", makeUrl returns "/graphxr/Projects".
 * If aliasPath is undefined or empty, makeUrl returns "/Projects".
 * Not intended for URLs that don't start with "/".
 * If path is already prefixed, just return path.
 */
util.prefixAliasPath = (path) => {
    if (!AppConfig.aliasPath) return path
    if (!path.startsWith('/')) return path;
    if (path.startsWith(AppConfig.aliasPath)) return path
    return AppConfig.aliasPath + path;
}

util.convertUIConfig = (config) => {

    const converted = JSON.parse(JSON.stringify(config));

    Object.keys(converted).forEach(itemName => {
        const item = converted[itemName];

        if (item.subModule) {
            Object.keys(item.subModule).forEach(subName => {
                const sub = item.subModule[subName];
                sub.visibility = sub.visibility || (sub.visible ? "public" : "project"); // defined in Visibility.js on the web side
                delete sub.visible;
            });
            delete item.visible;
        } else {
            item.visibility = item.visibility || (item.visible ? "public" : "project");
            delete item.visible;
        }
    });

    return converted;
}

module.exports = util;
