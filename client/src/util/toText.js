export const toContentByType = {
    codeTool: {
        func: function (block) {
            let arr = [];
            arr.push(block.data.codeData.value);
            arr.push(
                _.reduce(block.data.codeData, (p, curr, key) => {
                    if (key !== "value") {
                        p.push(`${key}:${curr}`);
                    }
                    return p;
                }, []).join(",")
            );
            return arr.join("\n");
        },
        extract: function (blocks, dname) {
            return _.filter(blocks, (block, index) => {
                return block.type === 'codeTool' && block.data.codeData.dname === dname;
            })
        }
    },
    plotChart: {
        func: function (block) {
            return _.reduce(block.data, (p, curr, key) => {
                p.push(`${key}:${JSON.stringify(curr)}`);
                return p;
            }, []).join("\n")
        },
        extract: function (blocks, dname) {
            return _.filter(blocks, (block, index) => {
                return block.type === 'plotChart' && block.data.dname === dname;
            })
        }
    },
    antChart: {
        func: function (block) {
            return _.reduce(block.data.chartConfigs, (p, curr, key) => {
                p[key] = JSON.stringify(curr);
                return p;
            }, {
                // layouts: JSON.stringify(block.data.layouts) 
            })
        },
        extract: function (blocks, dname) {
            return _.filter(blocks, (block, index) => {
                return block.type === 'antChart' && block.data.dname === dname;
            })
        }
    }
}