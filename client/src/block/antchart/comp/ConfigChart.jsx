import { CarryOutOutlined } from '@ant-design/icons';
import { Affix, AutoComplete, Button, Checkbox, Col, Input, InputNumber, Row, Select, Switch, Tabs, Tooltip, Tree } from 'antd';
import _ from 'lodash';
import PropTypes from "prop-types";
import React from "react";
import { Sticky, StickyContainer } from 'react-sticky';
import { ShowType, positions } from 'util/chartConfigUtil'
import { isChartRenderType, RenderType } from 'util/databaseApi';
import { confirm_key, copyContent, IsKey, showMessage } from 'util/utils';
import AntChartSetting from '../AntChartSetting';
import { DataComp, StepTypes } from '../antchartUtils';
import SketchColorPicker from '../util/SketchColorPicker';
import { cloneDeepDefaultOptions, getDefaultOptionsByType } from './CheckChartTypes';
import { RunCodeIcon } from '../../code/Icon';
import { Default_Display_Config, extractOptions, extractOption, ChartTypeTemplate, extractDatas, Fields, ChartTypes, isTimelineType } from './OptionsTypes';
import RenderObject from './RenderObject';
import { RawJSONComp } from './TableComp';
import { useState } from 'react';
const { TabPane } = Tabs;
const { Option } = Select;

const renderTabBar = (props, DefaultTabBar) => (
    <Sticky bottomOffset={80}>
        {({ style }) => (
            <DefaultTabBar {...props} className="site-custom-tab-bar" style={{ ...style }} />
        )}
    </Sticky>
);
const commonClazz = "d-flex justify-content-between align-items-center";
const extractKey = (key) => (~key.lastIndexOf('.') ? key.substring(key.lastIndexOf('.') + 1) : key);
const PaddingComp = function ({ alias, KEY = '', obj, onChange }) {
    let [value, setValue] = useState(typeof obj === 'object' ? obj.value : obj);
    const [allSame, setAllSame] = useState(typeof obj === 'number' || typeof obj === 'string');
    return <div className={"d-flex justify-content-between"}>
        <div>{(alias || extractKey(KEY)).camelPeakToBlankSplit().firstUpperCase()}:</div>
        <div>
            <div className='d-flex align-items-center justify-content-between'>All same:<Switch checked={allSame}
                onChange={(checked) => {
                    let v;
                    if (checked) {
                        v = 'auto';
                    } else {
                        v = 0;
                    }
                    setAllSame(checked);
                    setValue(v);
                    onChange(typeof obj === 'object' ? `${KEY}.value` : KEY, v)
                }} >
            </Switch>
            </div>
            {_.map(['Top', 'Right', 'Bottom', 'Left'], (key, index) => {
                return <div className='d-flex align-items-center justify-content-between' key={key}>
                    <span>{key}:</span>
                    <AutoComplete
                        style={{
                            width: '100px',
                        }}
                        placeholder="Value"
                        options={allSame ? [
                            {
                                value: 'auto',
                            }
                        ] : []}
                        value={((typeof value === 'number' || typeof value === 'string') ? value || 0 : value[index] || 0).toString()}
                        onChange={(e) => {
                            let v;
                            if (allSame) {
                                if (e === 'auto' || parseInt(e) >= 0) {
                                    v = e === 'auto' ? e : parseInt(e);
                                } else {
                                    return;
                                }
                            } else {
                                if (parseInt(e) >= 0) {
                                    let tmp = _.cloneDeep(value);
                                    if (typeof tmp === 'number' || typeof tmp === 'string') {
                                        tmp = [0, 0, 0, 0]
                                    }
                                    tmp[index] = parseInt(e);
                                    v = tmp;
                                }
                            }
                            setValue(v)
                            onChange(typeof obj === 'object' ? `${KEY}.value` : KEY, v);
                        }}
                    />
                </div>
            })}
        </div>
    </div>
}
export default class ConfigChart extends React.Component {
    static propTypes = {
        settings: PropTypes.object,
        className: PropTypes.string,
    };

    static defaultProps = {};
    constructor(props) {
        super(props);
        this.state = {
            className: this.props.className,
        }
    }

    componentDidUpdate(preProps, preState) {
        const { className } = this.props;
        let state = {};
        if (preProps.className !== className) {
            _.assign(state, { className });
        }
        !_.isEmpty(state) && this.setState(state);
    }

    componentWillUnmount() {
    }

    render() {
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        const { chartConfig: { chartData, chartColumns, type, options, chartKey, renderType }, current } = settings.state;
        const { className } = this.state;
        return <div className={`config-chart ${className || ''} ${current === StepTypes.ConfigChart ? "" : "hide"}`}>
            <StickyContainer>
                <Tabs defaultActiveKey="All Configs" renderTabBar={renderTabBar}>
                    <TabPane tab="All Configs" key="All Configs">
                        <AllConfigs key={chartKey} settings={settings} needRefresh={current === StepTypes.ConfigChart} chartKey={chartKey} renderType={renderType} type={type} options={_.assign(options, { data: chartData })}></AllConfigs>
                    </TabPane>
                    {/* <TabPane tab="Common" key="Common" style={{ height: 200 }}>
                        Content of Tab Pane 1
                    </TabPane> */}
                </Tabs>
            </StickyContainer>
            <DataComp data={chartData} columns={chartColumns}></DataComp>
        </div>
    }
}
class AllConfigs extends React.Component {
    static propTypes = {
        needRefresh: PropTypes.bool,
        options: PropTypes.object,
        renderType: PropTypes.string,
        type: PropTypes.string,
        chartKey: PropTypes.string,
        settings: PropTypes.any
    }

    constructor(props) {
        super(props);
        const { renderType, type, chartKey, options, needRefresh } = this.props;
        let tmp = { needRefresh, chartKey, cacheState: undefined, alwaysRun: true, needRun: false };
        this.EXCEPT_KEYS = _.keys(tmp);
        _.assign(tmp, cloneDeepDefaultOptions(renderType, type), _.cloneDeep(options));
        this.state = tmp;
    }

    componentDidUpdate(preProps, preState) {
        const { renderType, type, chartKey, options, needRefresh } = this.props;
        const { cacheState } = this.state;
        let state = {};
        if (preProps.chartKey !== chartKey) {
            _.assign(state, { chartKey });
        }
        if (preProps.options !== options || !_.isEqual(preProps.options, options)) {
            let tmpObj = _.clone(this.state);
            _.each(tmpObj, (value, key, obj) => {
                if (!~this.EXCEPT_KEYS.indexOf(key)) {
                    obj[key] = undefined;
                }
            })
            if (!needRefresh) {
                _.assign(state, { cacheState: _.assign(tmpObj, _.cloneDeep(options)) });
            } else {
                _.assign(state, tmpObj, _.cloneDeep(options), { cacheState: undefined });
            }
        } else if (needRefresh && cacheState) {
            _.assign(state, cacheState, { cacheState: undefined });
        }
        !_.isEmpty(state) && this.setState(state);
    }

    onChange = (key = "", value) => {
        let state = {};
        let lastIndexOf = key.lastIndexOf(".");
        if (~lastIndexOf) {
            let rootKey = key.substring(0, key.indexOf("."));
            let rootValue = _.cloneDeep(_.get(this.state, rootKey));
            _.set(state, rootKey, rootValue);
            let parentKey = key.substring(0, lastIndexOf)
            let parentValue = _.cloneDeep(_.get(state, parentKey));
            parentValue[key.substring(lastIndexOf + 1)] = value;
            _.set(state, parentKey, parentValue);
        } else {
            _.set(state, key, value);
        }
        console.log(state);
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        const { chartConfig, chartEditor } = settings.state;
        const { needRefresh, chartKey, cacheState, alwaysRun, needRun, ...options } = this.state;
        settings.modifyChartConfig({ options: _.assign(options, state) }, () => {
            alwaysRun ? (chartEditor && chartEditor.run(() => { this.setState({ needRun: false }) }, false)) : this.setState({ needRun: true });
        })
    }

    inputNumberC = ({ alias, key = '', minV = 0, maxV = 100000, step = 1, children }) => {
        let obj = _.get(this.state, key);
        let hasConfig = typeof obj === 'object' && null !== obj && obj.displayConfig.type === ShowType.Number
        let v = hasConfig ? obj.value : obj;
        return {
            title: <div className={commonClazz}>
                <div>{(alias || extractKey(key)).camelPeakToBlankSplit().firstUpperCase()}:</div>
                {~['width', 'height'].indexOf(key.toLowerCase()) ? <InputNumber min={minV} max={maxV} value={v} controls={false}
                    onPressEnter={(e) => {
                        let value = Number.isInteger(step) ? parseInt(e.target.value) : parseFloat(e.target.value);
                        if (v !== value) {
                            if (hasConfig) {
                                this.onChange(`${key}.value`, value)
                            } else {
                                this.onChange(key, value)
                            }
                        }
                    }}
                    onBlur={(e) => {
                        let value = Number.isInteger(step) ? parseInt(e.target.value) : parseFloat(e.target.value);
                        if (v !== value) {
                            if (hasConfig) {
                                this.onChange(`${key}.value`, value)
                            } else {
                                this.onChange(key, value)
                            }
                        }
                    }} step={step}></InputNumber> : <InputNumber min={minV} max={maxV} defaultValue={v}
                        onPressEnter={(e) => {
                            let value = Number.isInteger(step) ? parseInt(e.target.value) : parseFloat(e.target.value);
                            if (v !== value) {
                                if (hasConfig) {
                                    this.onChange(`${key}.value`, value)
                                } else {
                                    this.onChange(key, value)
                                }
                            }
                        }}
                        onBlur={(e) => {
                            let value = Number.isInteger(step) ? parseInt(e.target.value) : parseFloat(e.target.value);
                            if (v !== value) {
                                if (hasConfig) {
                                    this.onChange(`${key}.value`, value)
                                } else {
                                    this.onChange(key, value)
                                }
                            }
                        }} step={step}></InputNumber>}
            </div>,
            key: `${key}_${this.state.chartKey}`,
            icon: <CarryOutOutlined />,
            children: children
        }
    }

    inputTextC = ({ alias, key = '', children }) => {
        let obj = _.get(this.state, key);
        let value = typeof obj === 'object' ? obj.value : obj;
        return {
            title: <div className={commonClazz}>
                <div>{(alias || extractKey(key)).camelPeakToBlankSplit().firstUpperCase()}:</div>
                <Input style={{ width: 120 }} defaultValue={value} allowClear
                    onChange={(e) => {
                        let key = String.fromCharCode(e.keyCode).toLowerCase();
                        //filter the number & char key
                        if (!(/^[0-9a-z]$/g).test(key) || e.key === e.code) {
                            key = e.key;
                        }
                        if (IsKey(e, key, confirm_key) && value !== e.target.value) {
                            this.onChange(typeof obj === 'object' ? `${key}.value` : key, e.target.value)
                        }
                    }} onBlur={(e) => {
                        if (value !== e.target.value) {
                            this.onChange(typeof obj === 'object' ? `${key}.value` : key, e.target.value)
                        }
                    }}></Input>
            </div>,
            key: `${key}_${this.state.chartKey}`,
            icon: <CarryOutOutlined />,
            children: children
        }
    }

    jsTextC = ({ alias, key = '', children }) => {
        let obj = _.get(this.state, key);
        let value = typeof obj === 'object' ? obj.value : obj;
        return {
            title: <div className={commonClazz}>
                <div>{(alias || extractKey(key)).camelPeakToBlankSplit().firstUpperCase()}:</div>
                <RawJSONComp renderType={RenderType.Code} style={{ width: 120 }} result={value}
                    onBlur={(cm, e) => {
                        if (value !== cm.getValue()) {
                            this.onChange(typeof obj === 'object' ? `${key}.value` : key, cm.getValue())
                        }
                    }}
                    run={(cm) => {
                        if (value !== cm.getValue()) {
                            this.onChange(typeof obj === 'object' ? `${key}.value` : key, cm.getValue())
                        }
                    }}></RawJSONComp>
            </div>,
            key: `${key}_${this.state.chartKey}`,
            icon: <CarryOutOutlined />,
            children: children
        }
    }

    inputSelectC = ({ alias, key = '', options = positions, children, props = {} }) => {
        let obj = _.get(this.state, key);
        return {
            title: <div className={commonClazz}>
                <div>{(alias || extractKey(key)).camelPeakToBlankSplit().firstUpperCase()}:</div>
                <Select {...props} style={{ width: 120 }} value={obj.value}
                    onChange={(value) => {
                        this.onChange(`${key}.value`, value)
                    }}>
                    {_.map(options, (position, index) => {
                        return <Option key={position} value={position}>{position}</Option>
                    })}
                </Select>
            </div>,
            key: key,
            icon: <CarryOutOutlined />,
            children: children
        }
    }

    colorPickerC = ({ alias, key = '', children, props = {} }) => {
        let obj = _.get(this.state, key);
        return {
            title: <div className={commonClazz}>
                <div>{(alias || extractKey(key)).camelPeakToBlankSplit().firstUpperCase()}:</div>
                <SketchColorPicker color={obj.value}
                    onChange={(value) => {
                        this.onChange(`${key}.value`, value)
                    }}>
                </SketchColorPicker>
            </div>,
            key: key,
            icon: <CarryOutOutlined />,
            children: children
        }
    }

    switchC = ({ alias, key = '', children, }) => {
        let obj = _.get(this.state, key);
        let displayConfig = (obj && obj.displayConfig) || Default_Display_Config;
        let checked = (typeof obj === 'object') ?
            (displayConfig.type === ShowType.Off ? false : (displayConfig.type === ShowType.Boolean ? obj.value : true)) : obj;
        return {
            title: <div className={commonClazz}>
                <div>{(alias || extractKey(key)).camelPeakToBlankSplit().firstUpperCase()}:</div>
                <Switch checked={checked}
                    onChange={(value) => {
                        if (typeof obj === 'boolean') {
                            this.onChange(key, value);
                        } else if (typeof _.get(this.state, key) === 'object') {
                            let newData = _.cloneDeep(obj);
                            ~[ShowType.Off, ShowType.On].indexOf(displayConfig.type) ?
                                _.set(newData, "displayConfig.type", (displayConfig.type === ShowType.Off ? ShowType.On : ShowType.Off)) :
                                (newData.value = value);
                            this.onChange(key, newData)
                        }
                    }}></Switch>
            </div>,
            key: key,
            icon: <CarryOutOutlined />,
            disabled: !checked,
            children: checked ? children : undefined
        }
    }

    stableC = ({ alias, key = '', children }) => {
        return {
            title: <div className={commonClazz}>
                <div>{(alias || extractKey(key)).camelPeakToBlankSplit().firstUpperCase()}:</div>
            </div>,
            key: key,
            icon: <CarryOutOutlined />,
            children: children
        }
    }

    renderTreeNodes = (key = '', cache = []) => {
        if (!key || key === "LABEL" || key.endsWith(".LABEL")) { return null; }
        let obj = _.get(this.state, key);
        if (typeof obj === 'boolean') {
            return this.switchC({ key });
        } else if (typeof obj === 'number' || null === obj) {
            return this.inputNumberC({ key, minV: 0 });
        } else if (typeof obj === 'string') {
            return this.inputTextC({ key })
        } else if (typeof obj === 'object') {
            if (!obj || key === "displayConfig" || key.endsWith('displayConfig')) {
                return null;
            }
            let alias = obj.LABEL;
            let displayConfig = obj.displayConfig || Default_Display_Config;
            switch (displayConfig.type) {
                case ShowType.Boolean:
                    return this.switchC({ alias, key });
                case ShowType.Text:
                    return this.inputTextC({ alias, key });
                case ShowType.JsText:
                    return this.jsTextC({ alias, key });
                case ShowType.Select:
                    return this.inputSelectC({ key, options: obj.selector, props: obj.props });
                case ShowType.Number:
                    return this.inputNumberC({
                        key,
                        minV: obj.minV || 0,
                        maxV: obj.maxV || 100000,
                        step: obj.step || 1
                    });
                case ShowType.Padding:
                    return {
                        title: <PaddingComp {...{ alias, KEY: key, onChange: this.onChange, obj: _.get(this.state, key) }}></PaddingComp>,
                        key: `${key}_${this.state.chartKey}`,
                        icon: <CarryOutOutlined />,
                        children: undefined
                    }
                case ShowType.Extract:
                    //TODO
                    return null;
                case ShowType.Remove:
                    return null;
                case ShowType.Color:
                    return this.colorPickerC({ alias, key })
                case ShowType.Off:
                    return this.switchC({ alias, key });
                case ShowType.On:
                    if (_.has(displayConfig, "onValue")) {
                        return this.switchC({
                            alias,
                            key,
                            children: (typeof displayConfig.onValue === 'object') ? (typeof extractOption(displayConfig.onValue) === 'object' ? _.reduce(_.keys(displayConfig.onValue), (prev, objKey, index) => {
                                let ele = this.renderTreeNodes(`${key}.displayConfig.onValue.${objKey}`, cache);
                                ele && prev.push(ele);
                                return prev;
                            }, []) : [this.renderTreeNodes(`${key}.displayConfig.onValue`, cache)]) : [this.renderTreeNodes(`${key}.displayConfig.onValue`, cache)]
                        });
                    } else {
                        return this.switchC({
                            alias,
                            key,
                            children: _.reduce(_.keys(obj), (prev, objKey, index) => {
                                let ele = this.renderTreeNodes(`${key}.${objKey}`, cache);
                                ele && prev.push(ele);
                                return prev;
                            }, [])
                        });
                    }
                case ShowType.Stable:
                    return this.stableC({
                        alias,
                        key,
                        children: _.reduce(_.keys(obj), (prev, objKey, index) => {
                            let ele = this.renderTreeNodes(`${key}.${objKey}`, cache);
                            ele && prev.push(ele);
                            return prev;
                        }, [])
                    })
            }
        }
    }

    treeData = () => {
        let cache = [];
        let { needRefresh, chartKey, cacheState, alwaysRun, needRun, ...options } = this.state;
        let keys = _.filter(_.keys(options), (e) => {
            return undefined === ChartTypeTemplate.config[e] &&
                undefined !== this.state[e]
        })
        let data = _.reduce(keys, (prev, objKey, index) => {
            let ele = this.renderTreeNodes(`${objKey}`, cache);
            ele && prev.push(ele);
            return prev;
        }, [])
        return data;
    }

    render() {
        let { needRefresh, chartKey, cacheState, alwaysRun, needRun, ...options } = this.state;
        options.data = extractDatas(options.data, _.values(_.pick(options, _.keys(Fields))))
        return <div className="">
            <Row gutter={16}>
                <Col md={12} span={24}>
                    <h4>{this.props.type}</h4>
                    <Tree key={chartKey} treeDefaultExpandedKeys={["title", "title.text"]}
                        treeData={this.treeData()}
                    />
                    <Affix target={() => document.querySelector("#drawer .ant-drawer-body")} offsetBottom={0}>
                        <div className={"d-flex justify-content-center align-items-center"}>
                            <Switch checkedChildren="Apply it now" unCheckedChildren="Click apply" checked={alwaysRun} onChange={(checked) => {
                                this.setState({ alwaysRun: checked });
                            }}></Switch>&nbsp;&nbsp;&nbsp;&nbsp;
                            <Button size='small' icon={<RunCodeIcon fill={needRun} />} onClick={() => {
                                /**@type {AntChartSetting} */
                                const settings = this.props.settings;
                                const { chartEditor } = settings.state;
                                chartEditor && chartEditor.run(() => { this.setState({ needRun: false }) }, false);
                            }}>Run</Button>&nbsp;&nbsp;&nbsp;&nbsp;
                            <div className="normal-icon">
                                {isTimelineType(this.props.type) && <i title={"Copy variant name of visible items"} className="icon-bordered icon fas fa-copy" onClick={
                                    async () => {
                                        copyContent(`_timeline_${chartKey.replaceNoWordToUnderline()}`, () => {
                                            showMessage("copy success!", "success")
                                        })
                                    }
                                }></i>}
                            </div>
                        </div>
                    </Affix>
                </Col>
                <Col md={12} span={24}>
                    <RenderObject key={chartKey} obj={extractOptions(options)}></RenderObject>
                </Col>
            </Row >
        </div >
    }
}