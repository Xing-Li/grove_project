import onSelect from "./onSelect"

export default function getSeletedEdges() {
    return onSelect('grove-selected-edges', event => window.gxr.edges(event.edgeIds))
}