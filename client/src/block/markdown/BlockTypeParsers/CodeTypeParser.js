export function parseCodeToMarkdown(blocks) {
  return `\`\`\`\n${blocks.code}\n\`\`\`\n`;
}
export function parseCodeToolToMarkdown(blocks) {
  return `\`\`\` js\n${blocks.codeData.value}\n\`\`\`\n`;
}

export function parseMarkdownToCode(blocks) {
  const codeData = blocks.lang === 'js' ? {
    data: {
      "codeData": {
        "value": blocks.value,
        "pinCode": true
      }
    },
    type: 'codeTool',
  } : {
      data: {
        code: blocks.value,
      },
      type: 'code',
    };

  return codeData;
}
