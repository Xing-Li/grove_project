import React from "react";
import _ from "lodash";
import moment from "moment";
import "moment/locale/zh-cn";
import "moment/locale/en-gb";
import { ALIAS_PATH } from "./localstorage";
require('./Polyfill')
import Pinyin from "util/pinyin";

const py = new Pinyin();

String.prototype.toCommonName = function () {
  let matches = this.matchAll(/[\u4e00-\u9fa5]/g);
  let tmp;
  let _self = this;
  while (!(tmp = matches.next()).done) {
    _self = _self.replace(tmp.value[0], py.getFullChars(tmp.value[0]))
  }
  return _self.replace(/\W/g, "$").replace(/(^\d)/g, "_$1");
};

String.prototype.firstUpperCase = function () {
  return this.charAt(0).toUpperCase() + this.slice(1);
};

String.prototype.replaceNoWordToUnderline = function () {
  return this.replace(/\W/g, "_")
}

String.prototype.camelPeakToBlankSplit = function () {
  let a = "";
  for (let index = 0; index < this.length; index++) {
    let lastch = a.length ? a.charAt(a.length - 1) : undefined;
    if (a.length > 0 && "A" <= this.charAt(index) && this.charAt(index) <= "Z" && (!lastch || !("A" <= lastch && lastch <= "Z"))) {
      a = a + " ";
    }
    a = a + this.charAt(index);
  }
  return a;
};

String.prototype.clipString = function (clipLength = 0) {
  if (clipLength > 0 && this.length > clipLength) {
    return `${this.substr(0, clipLength)} ...`;
  } else {
    return this;
  }
};

/**
 *  Sean Li to SL
  "Sean Li".shortWord();
  "SeanLi".shortWord();
 */
String.prototype.shortWord = function (length = 2) {
  let words = this.trim().split(" ");
  if (words.length === 1) {
    words = Array.from(this.match(/[A-Z]/g) || [this]);
  }
  if (words.length >= length) {
    return words
      .slice(0, length)
      .map((word) => word.charAt(0).toUpperCase())
      .join("");
  } else {
    return this.slice(0, length).toUpperCase();
  }
};

const URLQuery = (name) => new URLSearchParams(
  window.location.search
).get(name);

const Common = {
  dateFormatss: (dateStr) => {

    // const cn = window.location.search.indexOf("cn") !== -1;
    let createTime = moment(dateStr);
    // if (cn) {
    //   createTime.locale("zh-cn").utcOffset(8);
    // } else {
    //   createTime.locale("en-gb").utcOffset(0);
    // }
    return createTime.format("YYYY-MM-DD HH:mm:ss");
  },
  urlParam: (paramName) => URLQuery(paramName),
  urlBooleanParam: (paramName) => (/true/i).test(URLQuery(paramName)),
  recursionDefaultInput: (defaultInput, callback) => {
    _.forEach(defaultInput, (info, infoKey, defaultInput) => {
      let tmpTwo = true;
      _.forEach(info, (objs, index, arr) => {
        if (objs instanceof Array) {
          let tmp = true;
          _.forEach(objs, (obj, index2, arr2) => {
            if (!obj.ignore && callback(obj) === false) {
              tmp = false;
              tmpTwo = false;
              return false;
            }
          })
          if (!tmp) {
            return false;
          }
        } else {
          if (!objs.ignore && callback(objs) === false) {
            tmpTwo = false;
            return false;
          }
        }
      });
      if (!tmpTwo) {
        return false;
      }
    });
  },
  getUrlNextPath: (basePath) => {
    let tag =
      window.location.href.lastIndexOf(basePath) == -1
        ? null
        : window.location.href.substring(
          window.location.href.lastIndexOf(basePath) + basePath.length + 1
        );
    tag = tag.substring(
      0,
      tag.match(/[\?/]/g) ? tag.indexOf(tag.match(/[\?/]/g)[0]) : tag.length
    );
    return tag;
  },
  /**
   * NANP  replace \d{3} with [2-9]{1}\d{2}
   */
  isPhoneNumber: (v) => (/^[\d\-\+]+$/).test(v),
  // /^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/.test(v) ||
  // (v.match(/\d/g) && v.match(/\d/g).length === 10),
  /*
     Common.isEmail("a.b@demo.com"); 
     Common.isEmail("a@demo");
    */
  isEmail: (v) => /[a-z0-9\.\-\_]+@[a-z0-9\.\-\_]+\.[a-z]{2,}/gi.test(v),
  /* 
    Common.isEmail("demo.com");
    Common.isEmail("http://abc.xyz");
    Common.isEmail("abc-xyz");
    */
  isSite: (v) => /(http|https)?(:\/\/)?[a-z0-9\.\-\_]+\.[a-z]{2,}/gi.test(v),
  isName: (v) => v !== "" && /^[a-z]+[0-9]*$/gi.test(v),
  isShopName: (v) => v !== "" && /^[a-z][a-z0-9 ]*[0-9]*$/gi.test(v),
  isString: (v) => v !== "" && typeof (v) === 'string',
  isAddress: (v) => v !== "" && /^[a-z0-9\,\-\.]*/gi.test(v),
  isNumber: (v) => typeof v == "number" || (v !== "" && !isNaN(v)),
  getString: (v) => String(v),
  getNumber: (v) => Number(v) || 0,
  /**
    Common.checkNumber(NaN); 
   Common.checkNumber("NaN");
   Common.checkNumber(22); Common.checkNumber(2.2);  Common.checkNumber(8e+10);
   Common.checkNumber("22"); Common.checkNumber("2.2");  Common.checkNumber("8e+10");
   Common.checkNumber("ww22"); Common.checkNumber("2.2dd");  Common.checkNumber("e+ddd8e+10");
 **/
  checkNumber: function checkNumber(numberVal) {
    if (!isFinite(numberVal)) {
      // typeof(Infinity or NaN)   is number
      return String(numberVal);
    } else if (typeof numberVal === "number") {
      return numberVal;
    } else if (typeof numberVal !== "string") {
      return numberVal;
    }
    numberVal = numberVal.trim();
    if (/^(-)?[0-9]+$/.test(numberVal)) {
      return parseInt(numberVal);
    } else if (/^(-)?\d+\.(\d+)?(e\+\d+)?$/.test(numberVal)) {
      return parseFloat(numberVal);
    } else {
      return numberVal;
    }
  },

  checkMoney: function checkMoney(numberVal) {
    return Math.ceil(Common.checkNumber(numberVal) * 100) / 100;
  },
  dateFormat: function dateFormat(date, fmt) {
    date = new Date(date);
    let o = {
      "M+": date.getMonth() + 1,
      "d+": date.getDate(),
      "h+": date.getHours(),
      "m+": date.getMinutes(),
      "s+": date.getSeconds(),
      "q+": Math.floor((date.getMonth() + 3) / 3), //quarter (of a year) ;
      S: date.getMilliseconds(),
    };
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(
        RegExp.$1,
        (date.getFullYear() + "").substr(4 - RegExp.$1.length)
      );
    }
    for (let k in o) {
      if (new RegExp("(" + k + ")").test(fmt)) {
        fmt = fmt.replace(
          RegExp.$1,
          RegExp.$1.length === 1
            ? o[k]
            : ("00" + o[k]).substr(("" + o[k]).length)
        );
      }
    }
    return fmt;
  },
  //[[A,B],[a,b]] to [Aa,Ab,Ba,Bb]
  cartesian: function cartesian(...lists) {
    lists = (lists || []).filter(list => Array.isArray(list) && list.length > 0);
    if (lists.length === 0) {
      return [];
    }
    return lists.map((list) => list.map((item) => [item]))
      .reduce((listsA, listsB) =>
        listsA.reduce(
          (list, listA) =>
            list.concat(listsB.map((listB) => listA.concat(listB))),
          []
        )
      );
  },
  //add to Add
  capitalizeFirstChar: function (str) {
    str = String(str);
    return str[0].toUpperCase() + str.slice(1);
  },
  transferObject: function (defOptions) {
    return defOptions instanceof Array
      ? defOptions.reduce(function (allNames, name) {
        allNames[name] = name;
        return allNames;
      }, {})
      : (defOptions || {});
  },
  isClassComponent: function isClassComponent(component) {
    return typeof component === "function" &&
      !!component.prototype.isReactComponent
      ? true
      : false;
  },

  isFunctionComponent: function isFunctionComponent(component) {
    return typeof component === "function" &&
      String(component).includes('_react2["default"].createElement')
      ? true
      : false;
  },
  isReactComponent: function isReactComponent(component) {
    return this.isClassComponent(component) ||
      this.isFunctionComponent(component)
      ? true
      : false;
  },

  isElement: function isElement(element) {
    return React.isValidElement(element);
  },

  isDOMTypeElement: function isDOMTypeElement(element) {
    return this.isElement(element) && typeof element.type === "string";
  },

  isCompositeTypeElement: function isCompositeTypeElement(element) {
    return this.isElement(element) && typeof element.type === "function";
  },
  loadJS: function (jsURL) {
    return new Promise((resolve, reject) => {
      if (
        _.find(document.getElementsByTagName("script"), (e) => e.src == jsURL)
      ) {
        resolve("done");
      } else {
        let jsDom = document.createElement("script");
        jsDom.type = "text/javascript";
        jsDom.async = true;
        jsDom.src = jsURL;
        jsDom.onload = function () {
          resolve("done");
        };
        let s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(jsDom, s);
      }
    });
  },

  loadCSS: function (cssURL) {
    return new Promise((resolve, reject) => {
      if (
        _.find(document.getElementsByTagName("link"), (e) => e.href == cssURL)
      ) {
        resolve("done");
      } else {
        let cssDom = document.createElement("link");
        cssDom.type = "text/css";
        cssDom.rel = "stylesheet";
        cssDom.async = true;
        cssDom.href = cssURL;
        cssDom.onload = function () {
          resolve("done");
        };
        let s = document.getElementsByTagName("link")[0];
        s.parentNode.insertBefore(cssDom, s);
      }
    });
  },

  openURL: function openURL(url, target = "_blank") {
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      window.location.href = url;
      return;
    }

    let a = document.createElement("a");
    a.setAttribute("href", url);
    a.setAttribute("target", target);
    a.setAttribute("id", "openURL");
    a.innerHTML = "openURL";
    a.click();
  },

  CSVToArray: function (strData, strDelimiter) {
    // Check to see if the delimiter is defined. If not,
    // then default to comma.
    strDelimiter = strDelimiter || ",";
    // Create a regular expression to parse the CSV values.
    let objPattern = new RegExp(
      // Delimiters.
      "(\\" +
      strDelimiter +
      "|\\r?\\n|\\r|^)" +
      // Quoted fields.
      '(?:"([^"]*(?:""[^"]*)*)"|' +
      // Standard fields.
      '([^"\\' +
      strDelimiter +
      "\\r\\n]*))",
      "gi"
    );
    // Create an array to hold our data. Give the array
    // a default empty first row.
    let arrData = [[]];
    // Create an array to hold our individual pattern
    // matching groups.
    let arrMatches = objPattern.exec(strData);
    // Keep looping over the regular expression matches
    // until we can no longer find a match.
    while (arrMatches) {
      // Get the delimiter that was found.
      let strMatchedDelimiter = arrMatches[1];
      // Check to see if the given delimiter has a length
      // (is not the start of string) and if it matches
      // field delimiter. If id does not, then we know
      // that this delimiter is a row delimiter.
      if (strMatchedDelimiter.length && strMatchedDelimiter != strDelimiter) {
        // Since we have reached a new row of data,
        // add an empty row to our data array.
        arrData.push([]);
      }
      // Now that we have our delimiter out of the way,
      // let's check to see which kind of value we
      // captured (quoted or unquoted).
      let strMatchedValue = "";
      if (arrMatches[2]) {
        // We found a quoted value. When we capture
        // this value, unescape any double quotes.
        strMatchedValue = arrMatches[2].replace(new RegExp('""', "g"), '"');
      } else {
        // We found a non-quoted value.
        strMatchedValue = arrMatches[3];
      }
      // Now that we have our value string, let's add
      // it to the data array.
      arrData[arrData.length - 1].push(strMatchedValue);
      arrMatches = objPattern.exec(strData);
    }
    // Return the parsed data.
    return arrData;
  },

  convertCSVArrayToJSON: function (csvArray) {
    let jsonArray = [];
    csvArray[0] = csvArray[0].map((n) => String(n).trim());
    for (let i = 1; i < csvArray.length; i++) {
      let jsonRow = {};
      let countEmptyCol = 0;

      if (csvArray[0].length != csvArray[i].length) {
        continue;
      }

      for (let k = 0; k < csvArray[0].length; k++) {
        if (csvArray[i][k] == "") {
          countEmptyCol++;
        }

        jsonRow[csvArray[0][k]] = Common.checkNumber(csvArray[i][k]);
      }

      if (countEmptyCol != csvArray[0].length) {
        jsonArray.push(jsonRow);
      }
    }

    // let json = JSON.stringify(jsonArray);
    // let str = json.replace(/},/g, "},\r\n");

    return jsonArray;
  },

  convertCSV2JSON: function (csvString) {
    return this.convertCSVArrayToJSON(this.CSVToArray(csvString));
  },

  readCSVFileAsJSON: function readCSVFileAsJSON(file) {
    return new Promise((resolve, reject) => {
      let reader = new FileReader();
      reader.onload = function (evt) {
        try {
          let filecontent = evt.target.result;
          let productsObjs = Common.convertCSV2JSON(filecontent);
          resolve(productsObjs);
        } catch (e) {
          reject(e);
        }
      };
      reader.onerror = function (err) {
        reject(err);
      };
      reader.readAsText(file);
    });
  },

  /**
   * If basePath is "/graphxr" and path is "/Projects", makeUrl returns "/graphxr/Projects".
   * If basePath is undefined or empty, makeUrl returns "/Projects".
   * Not intended for URLs that don't start with "/".
   * If path is already prefixed, just return path.
   */
  prefixAliasPath: function (path) {
    if (!ALIAS_PATH) return path
    if (!path.startsWith('/')) return path;
    if (path.startsWith(ALIAS_PATH)) return path
    return ALIAS_PATH + path;
  },
  /**
   * Use this when serializing URLs to a database, for example. 
   */
  removeAliasPath: function (path) {
    if (!ALIAS_PATH) return path;
    if (!path.startsWith(ALIAS_PATH)) return path;
    return path.substring(ALIAS_PATH.length);
  },
  /**
   * Wrap the Fetch API resource url with the base url 
   */
  fetch: function (resource, ...args) {
    return fetch(this.prefixAliasPath(resource), ...args);
  }
};


Common.addClassToBody = (className = '', isRemove = false) => {
  let defaultClassName = String(document.body.className).replace(className, '').trim();
  if (!isRemove) {
    document.body.className = `${defaultClassName} ${className}`;
  } else {
    document.body.className = defaultClassName;
  }
}


export const ONE_MINUTE_SECONDS = 60,
  TEN_MINUTES_SECONDS = ONE_MINUTE_SECONDS * 10,
  HALF_MINUTES_SECONDS = ONE_MINUTE_SECONDS * 10,
  ONE_HOUR_SECONDS = ONE_MINUTE_SECONDS * 60,
  ONE_DAY_SECONDS = ONE_HOUR_SECONDS * 24,
  ONE_WEEK_SECONDS = ONE_DAY_SECONDS * 7;

Common.getFriendlyTime = (date) => {
  const secondsAgo = Math.round((new Date().getTime() - new Date(date).getTime()) / 1000);

  if (secondsAgo < ONE_MINUTE_SECONDS) {
    return ONE_MINUTE_SECONDS;
  } else if (secondsAgo < TEN_MINUTES_SECONDS) {
    return TEN_MINUTES_SECONDS;
  } else if (secondsAgo < HALF_MINUTES_SECONDS) {
    return HALF_MINUTES_SECONDS;
  } else if (secondsAgo < ONE_HOUR_SECONDS) {
    return ONE_HOUR_SECONDS;
  } else if (secondsAgo < ONE_DAY_SECONDS) {
    return ONE_DAY_SECONDS;
  } else {
    return Math.ceil(secondsAgo / ONE_DAY_SECONDS) * ONE_DAY_SECONDS
  }
}

export default Common;
