import * as stdlib from "@observablehq/stdlib";

export default function monitor(env, taskId) {
  if (!taskId) {
    return;
  }

  const { Generators } = new stdlib.Library();
  return Generators.observe((emit) => {
    let keepFetching = true;
    const unregister = () => {keepFetching = false};

    const getMessages = () => {
      return fetch(`${env.baseUrl}/tasks/${taskId}/log`).then((response) => response.json());
    }

    const messages = getMessages();
    emit(messages);

    const refetch = () => {
      setTimeout(() => {
        const messages = getMessages();
        emit(messages);

        if (keepFetching) {
          refetch();
        }
      }, 5000);
    }
    refetch();

    return unregister;
  });
}