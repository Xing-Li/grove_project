import * as remark from 'remark';
import { parseHeaderToHtml } from './BlockTypeParsers/HeaderTypeParser';
import { parseParagraphToHtml, parseTableToMarkdown, parseMarkdownToParagraph } from './BlockTypeParsers/ParagraphTypeParser';
import { parseListToHtml, parseMarkdownToList } from './BlockTypeParsers/ListTypeParser';
import { parseDelimiterToHtml, parseMarkdownToDelimiter } from './BlockTypeParsers/DelimiterTypeParser';
import { parseImageToHtml } from './BlockTypeParsers/ImageTypeParser';
import { parseSimpleImageToHtml } from './BlockTypeParsers/SimpleImageTypeParser';
import { parseCheckboxToHtml } from './BlockTypeParsers/CheckboxTypeParser';
import { parseQuoteToHtml, parseMarkdownToQuote } from './BlockTypeParsers/QuoteTypeParser';
import { parseCodeToHtml, parseCodeToolToMarkdown, parseMarkdownToCode } from './BlockTypeParsers/CodeTypeParser';
/**
 * Function which takes data and creates a markdown file
 * @param content of the file
 * @param filename
 *
 */
export function fileDownloadHandler(content, fileName) {
  const file = new File([content], { type: 'text/markdown', endings: 'transparent' });
  const url = URL.createObjectURL(file);

  const element = document.createElement('a');
  document.body.appendChild(element);
  element.href = url;
  element.download = fileName;
  element.click();
  window.URL.revokeObjectURL(url);
  document.body.removeChild(element);
}

/**
 * 
 * @param {{blocks:[]}} data 
 * @returns {string}
 */
export function editorjsTransferToHtml(data) {
  const initialData = {};
  initialData.content = data.blocks;
  const parsedData = initialData.content.map((item) => {
    // iterate through editor data and parse the single blocks to markdown syntax
    switch (item.type) {
      case 'header':
        return parseHeaderToHtml(item.data);
      case 'paragraph':
        return parseParagraphToHtml(item.data);
      case 'list':
        return parseListToHtml(item.data);
      case 'delimiter':
        return parseDelimiterToHtml(item);
      case 'image':
        return parseImageToHtml(item.data);
      case 'simpleImage':
        return parseSimpleImageToHtml(item.data);
      case 'quote':
        return parseQuoteToHtml(item.data);
      case 'checkbox':
        return parseCheckboxToHtml(item.data);
      case 'code':
        return parseCodeToHtml(item.data);
      case 'codeTool':
        return parseCodeToolToHtml(item.data);
      case 'checklist':
        return parseCheckboxToHtml(item.data);
      case 'table':
        return parseTableToHtml(item.data);
      default:
        break;
    }
  });
  return parsedData.join('\n');
}