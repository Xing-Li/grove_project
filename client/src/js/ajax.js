
export function createXHR() {
    var request = false;
    try {
        window.ActiveXObject && (request = new window.ActiveXObject('Msxml2.XMLHTTP'));
    }
    catch (err2) {
        try {
            window.ActiveXObject && (request = new window.ActiveXObject('Microsoft.XMLHTTP'));
        }
        catch (err3) {
            try {
                request = new XMLHttpRequest();
            }
            catch (err1) {
                request = false;
            }
        }
    }
    return request;
}

