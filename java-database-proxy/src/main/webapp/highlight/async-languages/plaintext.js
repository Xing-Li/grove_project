/*
Language: plaintext
Author: Egor Rogov (e.rogov@postgrespro.ru)
Description:
    Plain text without any highlighting.
*/

define([], (...$requiredLanguages) => function(hljs) {
        
          
          
    return {
        disableAutodetect: true
    };
}
)