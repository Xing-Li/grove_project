import JSZip from "jszip";

const DEFAULT_FILE_KEY = "blob";

export async function compressBlob(blob, options = { type: 'DEFLATE', level: 1 }) {
  const zip = new JSZip();
  zip.file(DEFAULT_FILE_KEY, blob);
  const compressedBlob = await zip.generateAsync({
    type: "blob",
    compression: options.type,
    compressionOptions: { level: options.level },
  });
  return compressedBlob;
}

export async function decompressBlob(blob) {
  const unzip = new JSZip();
  const unzipped = await unzip.loadAsync(blob);
  const unzippedBlob = await unzipped.file(DEFAULT_FILE_KEY).async("blob");
  return unzippedBlob
}