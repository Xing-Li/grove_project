import { Inspector } from '@observablehq/runtime';
import { addModuleScope, removeModuleScope } from 'util/hintUtil';
import NewArray from 'util/NewArray';
import { setGraphxrApi } from './graphxrApi';
import { CommonClient } from './databaseClient';
import { SQLiteDatabaseClient } from './sqlite';

export const DbClients = {};
window.DbClients = DbClients;
export const ShareData = {};
window.ShareData = ShareData;
const HINT_ARRAY = [];
export default class ExInspector extends Inspector {
    /**
     * 
     * @param {HTMLElement} node 
     * @param {String} tname hint name
     * @param {String} dname default name of block element
     * @param {String} moduleName
     * @param {Function} successFunc
     * @param {Function} failFunc
     * 
     */
    constructor(node, tname, dname, moduleName, successFunc, failFunc) {
        super(node);
        /**hint name of variant */
        this.tname = tname;
        /**default name of block element */
        this.dname = dname;
        this.moduleName = moduleName;
        /**@type {function} call when fulfilled */
        this.successFunc = successFunc;
        /**@type {function} call when rejected */
        this.failFunc = failFunc;
        /**@type {boolean} flag is deleted */
        this.del = false;
        this.shareDataKey = this.tname || this.dname;
    }

    /**
     * Inspects the specified value, replacing the contents of 
     * this inspector’s element as appropriate, and dispatching 
     * an update event. If the specified value is a DOM element 
     * or text node, and the value is not already attached to 
     * the DOM, it is inserted into this inspector’s element, replacing 
     * any existing contents. Otherwise, for other arbitrary values 
     * such as numbers, arrays, or objects, an expandable display of 
     * the specified value is generated into this inspector’s element. 
     * Applies the observablehq class to this inspector’s element, and 
     * for non-element values, the observablehq--inspect class.
     * @param {*} value 
     * @param {*} name 
     */
    fulfilled(value, name) {
        if (this.tname || this.dname) {
            const { _node } = this;

            /**
             * This value is GraphXR API.
             * We cache it so that other modules (GraphxrApiSearchModal) can see the loaded version
             */
            if (value != undefined && value._minimumGraphxrVersion) {
                console.log(`GraphXR API detected: v${value._version}`);
                setGraphxrApi(value);
            }

            if (value instanceof SVGElement) {
                if (!value.style.width && value.getAttribute("width") === null) {
                    let viewBox = value.getAttribute("viewBox");
                    if (viewBox !== null) {
                        viewBox = viewBox.indexOf(",") ? viewBox.replace(/(\s+)?,(\s+)?/g, " ") : viewBox.replace(/\s+/g, " ")
                        let splits = viewBox.split(" ");
                        value.style.width = splits[2] + "px";
                        value.style.height = splits[3] + "px";
                    }
                }
            }
            if (value instanceof HTMLElement && (
                value instanceof SVGElement ||
                value instanceof HTMLCanvasElement ||
                value instanceof HTMLIFrameElement ||
                value instanceof HTMLImageElement ||
                (
                    value.getElementsByTagName("svg").length > 0 ||
                    value.getElementsByTagName("canvas").length > 0 ||
                    value.getElementsByTagName("iframe").length > 0 ||
                    value.getElementsByTagName("img").length > 0
                )
            )) {
                _node.parentNode.querySelector(".declare") && (_node.parentNode.querySelector(".declare").innerHTML = "");
            }
            if (this.tname !== undefined) {
                if (value instanceof Array || window.opener && value instanceof window.opener.Array || value instanceof NewArray) {
                    this.hintValue = HINT_ARRAY;
                    addModuleScope(this.moduleName, this.tname, this.hintValue);
                } else {
                    this.hintValue = value;
                    addModuleScope(this.moduleName, this.tname, this.hintValue);
                }
            }
            if (value instanceof CommonClient || value instanceof SQLiteDatabaseClient) {
                if (this.shareDataKey && (!DbClients[this.moduleName] || value !== DbClients[this.moduleName][this.shareDataKey])) {
                    if (!DbClients[this.moduleName]) {
                        DbClients[this.moduleName] = {};
                    }
                    DbClients[this.moduleName][this.shareDataKey] = value;
                    if (!this.tname) {
                        _node.setAttribute("title", this.dname);
                    }
                    actions.variable(actions.types.DB_CLIENTS, [], () => {
                        return { DbClients, key: this.shareDataKey };
                    })
                }
            }
            if ((value instanceof Array || window.opener && value instanceof window.opener.Array || value instanceof NewArray) && this.shareDataKey) {
                let rightData = true;
                _.each(value,/**
                 * 
                 * @param {{}} v 
                 * @param {number} i 
                 * @param {[]} arr 
                 */ (v, i, arr) => {
                        if (typeof v !== 'object' || (_.filter(v, (value, key) => {
                            return value && (typeof value === 'object' && !(value instanceof Date || value instanceof window.opener.Date))
                        }).length > 0)) {
                            rightData = false;
                            return false;
                        }
                    });
                if (rightData && this.shareDataKey && (!ShareData[this.moduleName] || value !== ShareData[this.moduleName][this.shareDataKey])) {
                    if (!ShareData[this.moduleName]) {
                        ShareData[this.moduleName] = {};
                    }
                    ShareData[this.moduleName][this.shareDataKey] = value;
                    if (!this.tname) {
                        _node.setAttribute("title", this.dname);
                    }
                    actions.variable(actions.types.SHARE_DATA, [], () => {
                        return { ShareData, key: this.shareDataKey };
                    })
                }
            }
        }
        super.fulfilled(value, name);
        if (!this.del && this.successFunc !== undefined) {
            this.successFunc(value, name);
        }
    }

    /**
     * Inspects the specified error, replacing the contents 
     * of this inspector’s element as appropriate with the 
     * error’s description, and dispatching an error event. 
     * Applies the observablehq and observablehq--error class 
     * to this inspector’s element.
     * @param {*} error 
     * @param {*} name 
     */
    rejected(error, name) {
        const { _node } = this;
        super.rejected(error, name);
        if (!this.del && this.failFunc !== undefined) {
            this.failFunc(error, name);
        }
    }

    /**
     * set hint name of variant
     * 
     * @param {string} tname 
     */
    setTname(tname) {
        this.tname = tname;
        this.shareDataKey = this.tname || this.dname;
        this.del = false;
    }

    setFailFunc(failFunc) {
        this.failFunc = failFunc;
    }

    setSuccessFunc(successFunc) {
        this.successFunc = successFunc;
    }

    delete() {
        this.del = true;
        if (this.shareDataKey && ShareData[this.moduleName] && ShareData[this.moduleName][this.shareDataKey]) {
            delete ShareData[this.moduleName][this.shareDataKey];
            actions.variable(actions.types.SHARE_DATA, [], () => {
                return { ShareData, key: this.shareDataKey };
            })
        }
        if (this.shareDataKey && DbClients[this.moduleName] && DbClients[this.moduleName][this.shareDataKey]) {
            delete DbClients[this.moduleName][this.shareDataKey];
            actions.variable(actions.types.DB_CLIENTS, [], () => {
                return { DbClients, key: this.shareDataKey };
            })
        }
        if (this.tname !== undefined) {
            removeModuleScope(this.moduleName, this.tname, this.hintValue);
        }
    }

}
