import { Col, InputNumber, Row, Slider } from 'antd';
import PropTypes from "prop-types";
import React from 'react';

export default class IntegerStep extends React.Component {
    static propTypes = {
        min: PropTypes.number,
        max: PropTypes.number,
        inputValue: PropTypes.number,
        changeFunc: PropTypes.func
    };
    static defaultProps = {};


    constructor(props) {
        super(props);
        this.state = {
            inputValue: this.props.inputValue
        }
    }

    componentDidUpdate(prevProps, preState) {
        if (this.props.inputValue !== prevProps.inputValue) {
            this.setState({ inputValue: this.props.inputValue });
        }
    }

    onChange = value => {
        if (!value) {
            return;
        }
        this.setState({
            inputValue: value,
        }, () => {
            const { changeFunc } = this.props;
            const { inputValue } = this.state;
            changeFunc(inputValue);
        });
    };

    render() {
        const { min, max } = this.props;
        const { inputValue } = this.state;
        const marks = {};
        marks[min] = {
            label: <strong>{min}</strong>,
        }
        marks[max] = {
            style: {
                color: '#f50',
            },
            label: <strong>{max}</strong>,
        }
        return (
            <Row className="d-flex justify-content-between align-items-center">
                <Col span={12}>
                    <Slider
                        marks={marks}
                        min={min}
                        max={max}
                        onChange={this.onChange}
                        value={typeof inputValue === 'number' ? inputValue : 0}
                    />
                </Col>
                <InputNumber
                    min={min}
                    max={max}
                    // style={{ margin: '0 16px' }}
                    value={inputValue}
                    onChange={this.onChange}
                />
            </Row>
        );
    }
}