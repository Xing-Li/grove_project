
export const CARD_FOOTER_HEIGHT = 64;
export const CARD_HEADER_HEIGHT = 72;


export const SELECTION_TYPES = {
    NUMBER: 0,
    NUMBER_OR_DATETIME: 1,
    LIST: 2,
    TEXT: 3,
    MULTILINE_TEXT: 4,
    DICTIONARY: 5,
    COLOR: 6,
    NODE_PROPERTIES: 7
}

