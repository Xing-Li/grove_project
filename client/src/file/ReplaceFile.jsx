import { Button, Select } from 'antd';
import Modal from 'antd/lib/modal/Modal';
import Text from 'antd/lib/typography/Text';
import PropTypes from "prop-types";
import React from 'react';
import MyDropzone from '../file/MyDropzone.jsx';
import { ModalType } from '../util/utils.js';
import { Title, WIDTH, Z_INDEX } from './fileUtils.js';
import MdFiles from './MdFiles';
const { DefaultShareData, DefaultFileData, USER_ID, PROJECT_USER_ID, USER_EMAIL } = require("../util/localstorage")

const { Option } = Select;
export default class ReplaceFile extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        width: PropTypes.number,
        react_component: PropTypes.object,
    };
    static defaultProps = {};

    constructor(props) {
        super(props);
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
        const { modalType, sourceFile } = react_component.state;
        this.state = {
            /**@type{string} */
            fileName: sourceFile.name,
            acceptedFile: null,
        }
    }

    componentDidMount() {
    }

    componentDidUpdate(prevProps, prevState) {
    }

    handleOk = () => {
        const { acceptedFile, fileName } = this.state;
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
        if (acceptedFile) {
            window.currentEditor.onImportFiles([acceptedFile]).then((values) => {
                if (!values || values.length !== 1) {
                    this.setState({ acceptedFile: undefined });
                    return;
                }
                window.currentEditor.replaceFile(fileName, values[0]);
                react_component.setState({
                    modalType: ModalType.None,
                });
            });
        } else {
            this.setState({ acceptedFile: undefined });
        }
    };

    render() {
        const { className, width } = this.props;
        const { fileName, acceptedFile } = this.state;
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
        const { modalType, sourceFile } = react_component.state;
        const closeFunc = () => {
            react_component.setState({
                modalType: ModalType.None,
            });
        };
        const buttons = [
            <Button key="back" onClick={closeFunc}>
                Cancel
            </Button>,
            <Button key="replace" onClick={this.handleOk}>
                Upload and replace
            </Button>
        ];
        return <Modal zIndex={Z_INDEX}
            className={`modal-comp data-html2canvas-ignore ${className}`}
            width={width || WIDTH}
            title={<Title modalType={modalType} sourceFile={sourceFile}></Title>}
            visible={modalType !== ModalType.None}
            onCancel={closeFunc}
            footer={buttons}
        >
            <div>
                <Text type="secondary">All references to {fileName} in the notebook will be replaced with references to the new file.</Text>
                <MyDropzone input={true} multiple={false} handleFilesFunc={(acceptedFiles) => {
                    acceptedFiles.length >= 1 && this.setState({ acceptedFile: acceptedFiles[0] });
                }}>
                    <div className="text-center">
                        <div>Drag and drop files to this panel or</div>
                        <div className="btn btn-light">Select a file</div>
                        {acceptedFile && <ul>
                            <li key={acceptedFile.path}>
                                {acceptedFile.path} - {acceptedFile.size} bytes
                            </li>
                        </ul>}
                    </div>
                </MyDropzone>
                <small className="font-italic text-secondary"> Maximum file size: 50 MB </small>
            </div>
        </Modal>
    }
}
