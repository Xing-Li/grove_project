export const calcHalfWindowWidth = () => {
	return (window.innerWidth - 2 * 20) / 2;
};

export const calcWindowWidth = () => {
	return window.innerWidth - 2 * 20;
};
