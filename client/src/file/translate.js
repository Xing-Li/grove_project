const version = "2.19.1";
function codeDataFunc(code) {
    return {
        "type": "codeTool",
        "data": {
            "codeData": {
                "value": `${code}`,
                "pinCode": false
            }
        }
    }
}
function mdDataFunc(blocks) {
    return {
        blocks: blocks,
        "version": version
    };
}
const Runtime = function (dirKey) {
    this.dirKey = dirKey;
}
Runtime.prototype.module = function (moduleName) {
    return new Module(moduleName);
}
Runtime.prototype.fileAttachments = function (resolve) {
    return null;
}
Runtime.prototype.map = function () {
    const _self = this;
    return function (arrs) {
        _self.arrs = new Map(arrs);
    }
}
const Module = function (moduleName) {
    this.moduleName = moduleName;
    this.blocks = [];
}
Module.prototype.builtin = function (libraryName, library) {

}
/**
 * 
 * @param {*} name 
 * @param {Module} module 
 */
Module.prototype.import = function (remote, name, module) {
    if (arguments.length < 3) {
        module = name, name = remote;
        let moduleName = module.moduleName;
        this.blocks.push(codeDataFunc(`import {${remote}} from '${moduleName}'`))
    } else {
        let moduleName = module.moduleName;
        this.blocks.push(codeDataFunc(`import {${remote} as ${name}} from '${moduleName}'`))
    }

}
Module.prototype.variable = function (observerRet) {
    this.observerRet = observerRet;
    return this;
}
Module.prototype.define = function (name, inputs, definition) {
    switch (arguments.length) {
        case 1: {
            definition = name, name = inputs = null;
            break;
        }
        case 2: {
            definition = inputs;
            if (typeof name === "string") inputs = null;
            else inputs = name, name = null;
            break;
        }
    }
    if (typeof definition !== 'function') {
        return;
    }
    /** @type [string] */
    let splits = definition.toString().split('\n');
    if (splits.length < 2) {
        return;
    }
    let codBody = ~splits[0].indexOf('{') ?
        splits.slice(1, splits.length - 1).join("\n") :
        splits.slice(1, splits.length).join("\n");
    let code = `${this.observerRet ? `${this.observerRet} = ${splits[0].endsWith("(") ?
        `(${codBody})` :
        codBody
        }` : codBody}`;
    this.blocks.push(codeDataFunc(code))
}
const observer = function (arg) {
    return arg;
}

export {
    Runtime, Module, observer, mdDataFunc
}