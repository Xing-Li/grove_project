import PropTypes from "prop-types";
import React from 'react';
import actions from './define/Actions';
import EditorAbs from './editorAbs';
import { v4 as uuidv4, NIL as NIL_UUID } from 'uuid';
import { Tooltip } from "antd";
import { getFileAttachmentContent, getFileKey } from "file/fileUtils";
import { FILESDIR } from "util/localstorage";
import { debounce } from "lodash";

const { copyContent, CodeMode, getCodeModeIcon, ShortcutTitle, cell_shortcuts_keys } = require("./util/utils");

/**
 * bottom pop tools
 */
export default class BottomTools extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        editor: PropTypes.object,
        editorReadOnly: PropTypes.bool,
    };
    static defaultProps = {};
    constructor(props) {
        super(props);
        /**@type {EditorAbs} */
        const editor = this.props.editor;
        const { editorReadOnly, selectFileFunc } = this.props;
        this.state = {
            editor,
            editorReadOnly,
            openSelect: editor && editor.isOpenSelect(),
            savedData: [],
            blockIndex: -1,
            copied: false,
            selectFileFunc: selectFileFunc,
        }
        this.needConfirm = false;
        this.currentBlock = null;
        this.focusBlockChangeElement = document.createElement("div");
        this.openSelectElement = document.createElement("div");
    }

    componentWillUnmount() {
        actions.deleteCache(this.focusBlockChangeElement);
        actions.deleteCache(this.openSelectElement);
    }

    componentDidMount() {
        actions.inspector(this.focusBlockChangeElement, [actions.types.FOCUS_BLOCK_CHANGE],
            ({ index }) => {
                const { blockIndex, editor } = this.state;
                const api = editor && editor.editorInstance;
                let blockApi = editor && editor.isReady() && api.blocks.getBlockByIndex(index);
                if (blockIndex !== index || this.currentBlock !== blockApi) {
                    this.currentBlock = blockApi;
                    this.setState({ blockIndex: index })
                }
            })
        actions.inspector(this.openSelectElement, [actions.types.OPEN_SELECT], ({ editorKey, openSelect }) => {
            const { editor } = this.state;
            if (editor.getTransferKey() === editorKey) {
                this.setState({ openSelect });
                editor.selectBlocks(openSelect);
                if (openSelect) {
                    editor.selectBlock(blockIndex);
                }
            }
        })
    }

    componentDidUpdate(preProps, preState) {
        let state = {};
        if (preProps.editor !== this.props.editor) {
            /**@type {EditorAbs} */
            let editor = this.props.editor;
            _.assign(state, { editor: editor, openSelect: editor && editor.isOpenSelect() });
        }
        if (preProps.editorReadOnly !== this.props.editorReadOnly) {
            _.assign(state, { editorReadOnly: this.props.editorReadOnly });
        }
        if ((preState.editor !== this.state.editor || preState.blockIndex !== this.state.blockIndex) && this.state.copied) {
            _.assign(state, { copied: false });
        }
        !_.isEmpty(state) && this.setState(state, () => {
        });
    }

    render() {
        const { editor, blockIndex, editorReadOnly, openSelect, copied, selectFileFunc } = this.state;
        const api = editor && editor.editorInstance;
        const block = editor && editor.isReady() && !editorReadOnly && api.blocks.getBlockByIndex(blockIndex);
        if (!block) {
            return <div></div>;
        }
        const config = editor.editorInstance.tools.getAllToolsConfig(block.name);
        const indexs = openSelect ? editor.getIndexOfSelectBlocks() : [blockIndex];
        let runComp = false;
        let pinComp = false;
        let hideComp = false;
        let fileAttachComp = false;
        let codeModeIcon;
        let codeModeTitle;
        if (block.name === 'codeTool') {
            /**@type{HTMLElement} */
            let data = block.holder.querySelector(".code-edit-result > .data");
            if (data) {
                let { pinCode, codeMode, needRun, hide } = data.closest(".code-edit").state;
                codeModeTitle = codeMode.firstUpperCase();
                codeModeIcon = `<i class="${getCodeModeIcon(codeMode)}"></i>`;
                if (~[CodeMode.htmlmixed, CodeMode.javascript2, CodeMode.jsx, CodeMode.markdown, CodeMode.python, CodeMode.tex, CodeMode.javascript].indexOf(codeMode)) {
                    pinComp = <i title={ShortcutTitle(cell_shortcuts_keys["Pin/Unpin Cell"], `${!pinCode ? "Pin" : "Unpin"} ${openSelect ? "All Codes" : "Code"}`)} tabIndex={-1}
                        className={`icon-bordered icon fas fa-thumbtack ${pinCode ? "" : "icon-inactive"}`}
                        onClick={() => {
                            editor.pinCodeFunc();
                        }}></i>;
                    hideComp = <i title={`${hide ? "Show" : "Hide"} Block When Shared`} tabIndex={-1}
                        className={`icon-bordered icon fas fa-eye-slash ${hide ? "" : "icon-inactive"}`}
                        onClick={() => {
                            let hideCodeFunc = data.closest(".code-edit").hideCode;
                            hideCodeFunc();
                        }}></i>;
                }
                if (~[CodeMode.htmlmixed, CodeMode.javascript2, CodeMode.jsx, CodeMode.markdown, CodeMode.python, CodeMode.tex, CodeMode.sql].indexOf(codeMode)) {
                    runComp = openSelect ? false : <i title={ShortcutTitle(cell_shortcuts_keys["Run Current Cell"])} tabIndex={-1}
                        className={`icon-bordered icon fas fa-play ${needRun ? "" : "icon-inactive"}`}
                        onClick={() => {
                            let runCodeFunc = data.closest(".code-edit").runCode;
                            runCodeFunc();
                        }}></i>;
                    if (editor.getData()) {
                        let insertValueFunc = data.closest(".code-edit").insertValue;
                        fileAttachComp = openSelect ? false : <i title="Attach File" tabIndex={-1} className={`icon-bordered icon fas fa-paperclip`}
                            onClick={() => {
                                let uploadFileCb = (fileNames) => {
                                    if (fileNames && fileNames.length >= 1) {
                                        let fileName = fileNames[0];
                                        let fileData = editor.getFileDataByKey(getFileKey(getFileKey(editor.getData().fileKey, FILESDIR), fileName))
                                        let value = getFileAttachmentContent(editor, fileData);
                                        insertValueFunc(value);
                                    }
                                    api.caret.setToBlock(api.blocks.getCurrentBlockIndex());
                                }
                                selectFileFunc(uploadFileCb);
                            }}></i>;
                    }
                }
            }
        }
        if (!config) {
            return <div></div>
        }
        return <div>
            {block && <div className={`bottom-affix d-flex justify-content-between align-items-center normal-icon ${!!~blockIndex ? "" : "hide"}`}>
                <div className="d-flex justify-content-start align-items-center">
                    <i title={codeModeTitle || config.title} className={"icon"} dangerouslySetInnerHTML={{ __html: codeModeIcon || config.icon }} />
                    <i tabIndex={-1} title={`${openSelect ? ShortcutTitle(cell_shortcuts_keys["Clear Selection"]) : "Select Cells"}`} className={`icon-bordered icon ${!openSelect ? "fas fa-check" : "fas fa-check-square"} `} onClick={() => {
                        actions.variable(actions.types.OPEN_SELECT, [], () => { return { editorKey: editor.getTransferKey(), openSelect: !openSelect } });
                    }}></i>
                    {runComp}
                    {pinComp}
                    {hideComp}
                    <i tabIndex={-1} title={copied ? "Copied!" : "Copy Select Cells"} className={`icon-bordered icon fas fa-copy ${copied ? "" : "icon-inactive"}`} onClick={async () => {
                        const { openSelect } = this.state;
                        let savedData = openSelect ? await editor.copySelectBlocks() : [await block.save()];
                        this.setState({ savedData, copied: true }, () => {
                        })
                    }}></i>
                    <i tabIndex={-1} title="Paste Copied Cells" className="icon-bordered icon fas fa-paste" onClick={async () => {
                        const { savedData } = this.state;
                        let savedDataTmp = _.cloneDeep(savedData);
                        editor.insertPrepare(savedDataTmp);
                        editor.editorInstance.blocks.insertEditorJSData(savedDataTmp);
                    }}></i>
                    <i tabIndex={-1} title={ShortcutTitle(cell_shortcuts_keys["Move Cell Up"])} className={`icon-bordered ${indexs.length === 0 || indexs[0] === 0 ? "icon-inactive" : ""} icon fas fa-arrow-up`} onClick={() => {
                        const { editor, blockIndex } = this.state;
                        editor.moveCellUp();
                    }}></i>
                    <i tabIndex={-1} title={ShortcutTitle(cell_shortcuts_keys["Move Cell Down"])} className={`icon-bordered ${indexs.length === 0 || indexs[indexs.length - 1] >= api.blocks.getBlocksCount() - 1 ? "icon-inactive" : ""} icon fas fa-arrow-down`} onClick={() => {
                        const { editor, blockIndex } = this.state;
                        editor.moveCellDown();
                    }}></i>
                    {fileAttachComp}
                    <i tabIndex={-1} title={ShortcutTitle(cell_shortcuts_keys["Delete Current Cell"])} className={`icon-bordered icon fas fa-times`} onClick={(e) => {
                        if (!this.needConfirm) {
                            e.target.classList.add("transition-bg-icon");
                            this.needConfirm = true;
                        } else {
                            const { editor, blockIndex } = this.state;
                            const api = editor.editorInstance;
                            api.blocks.delete(blockIndex);
                        }
                    }} onMouseLeave={(e) => {
                        if (this.needConfirm) {
                            e.target.classList.remove("transition-bg-icon");
                            this.needConfirm = false;
                        }
                    }}></i>
                </div>
                <div className="mr4">
                    <i title="Close" className="icon far fa-times-circle" onClick={(e) => {
                        editor.editorInstance.blocks.blockManager().dropPointer()
                    }}></i>
                </div>
            </div>}
        </div>
    }
}