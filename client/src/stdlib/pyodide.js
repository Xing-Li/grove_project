import { require as requireDefault } from "./grove-require";
var pyodideVar = undefined;
export async function pyodide() {
  if (!pyodideVar) {
    pyodideVar = await loadPyodide();
  }
  return pyodideVar;
}

async function loadPyodide() {
  return requireDefault("https://cdn.jsdelivr.net/pyodide/v0.21.2/full/pyodide.js").then(function (pyodideV) {
    return pyodideV.loadPyodide();
  });
}

export default async function (strings, ...expressions) {
  if (!strings) {
    return
  }
  let globals = {};
  const code = strings.reduce((result, string, index) => {
    if (expressions[index]) {
      const name = `x${index}`;
      globals[name] = expressions[index];
      return result + string + name;
    }
    return result + string;
  }, '');
  let pyodideVar = await pyodide();
  await pyodideVar.loadPackagesFromImports(code);
  const result = await pyodideVar.pyodide_py.eval_code_async(
    code,
    pyodideVar.toPy(globals)
  );
  if (result?.toJs) return result.toJs();
  return result;
}


