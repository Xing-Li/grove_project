const _ = require('lodash');
let s_trackNeo4jSocketInstance = null;

let TrackNeo4jCommandSocket = function (io) {
    s_trackNeo4jSocketInstance = io.of('/neo4j/tracking')
        .on("connection", function (socket) {

            console.info("socketID:" + socket.id);
            // clients[socket.id] = socket;
            socket.itemsCount = 0;

            socket.on('disconnect', function () {
                console.info('a user disconnected, will remove client:' + socket.id);
                // delete clients[socket.id];
            });

        });


}


let tracking = function (sessionID, type, content) {
    if (s_trackNeo4jSocketInstance) {
        s_trackNeo4jSocketInstance.sockets.forEach(socket => {
            // console.log("connected cookie:",socket.handshake.headers.cookie, "ID:",socket.id , sessionID);
            if(new RegExp(sessionID, 'ig').test(socket.handshake.headers.cookie)){
                socket.emit('tracking', {
                    type,
                    content
                })
            };
        })
    }
}

module.exports = TrackNeo4jCommandSocket;
module.exports.tracking = tracking;