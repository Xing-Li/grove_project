import { CloseOutlined, Loading3QuartersOutlined, UpCircleOutlined } from '@ant-design/icons';
import { BackTop, Button, message, Spin, Steps } from 'antd';
import DrawerEx from 'components/DrawerEx.jsx';
import actions from 'define/Actions';
import _ from 'lodash';
import PropTypes from "prop-types";
import React from "react";
import { dynamo_Template, mongo_Template, neo_Template, reD_Template, RenderType } from 'util/databaseApi';
import { CategoryFromType, IsKey, ShortcutTitle, showToast, window_shortcuts_keys } from 'util/utils';
import AntChartEditor from './AntChartEditor';
import { cloneDeepDefaultOptions } from './comp/CheckChartTypes';
import { ChooseChart, ConfigChart, ExportFiles, UploadData } from './comp/index.jsx';
import { chartDataFunc, ChartTypes, ChartTypeTemplate, DefaultAntChartConfig } from './comp/OptionsTypes';
const { StepTypes, getColumnsData, extractColumnsData, getCatogeries, NextStepsOfRenderType } = require('./antchartUtils');
const { Step } = Steps;
/**
 * step by step config chart
 * 
 * Upload Data -> Choose Chart -> Config Chart -> Export Files
 */
export default class AntChartSetting extends React.Component {
  static propTypes = {
    chartConfig: PropTypes.object,
    chartEditor: PropTypes.object,
    className: PropTypes.string
  }

  static defaultProps = {};
  /**
   * Creates
   *
   * @constructor
   * @param {{chartConfig:DefaultAntChartConfig}} props
   */
  constructor(props) {
    super(props);
    this.props = props;
    const { chartConfig, chartConfig: { renderType } } = this.props;
    /** @type {AntChartEditor} chartEditor binded when open current Component*/
    const chartEditor = this.props.chartEditor;
    /** @type {DefaultAntChartConfig} */
    const chartConfigTmp = _.cloneDeep(chartConfig)
    chartConfig.chartData.keysArr && (chartConfigTmp.chartData.keysArr = chartConfig.chartData.keysArr)
    this.state = {
      /** @type {AntChartEditor} chartEditor binded when open current Component*/
      chartEditor,
      /** @type {DefaultAntChartConfig} */
      chartConfig: chartConfigTmp,
      /** @type {DefaultAntChartConfig} */
      oldChartConfig: chartConfig,
      visible: chartConfig !== DefaultAntChartConfig,
      steps: chartEditor && chartEditor.state.readOnly ? [StepTypes.ExportFiles] : [StepTypes.UploadData, ...NextStepsOfRenderType(renderType)],
      current: chartEditor && chartEditor.state.readOnly ? StepTypes.ExportFiles : StepTypes.UploadData,
      placement: 'bottom',
      /** data source properties */
      properties: [],
      /** data source columns */
      columns: [],
      /** data source data */
      data: [],
      /**loading state */
      editorLoading: false,
      dFullScreen: false,
    };
    this.refreshGraphElement = document.createElement("div");
    this.shareDataElement = document.createElement("div");
  }

  componentDidUpdate(prevProps, prevState) {
    const { chartEditor, chartConfig } = this.props
    let state = {};
    if (prevProps.chartEditor !== chartEditor) {
      _.assign(state, { chartEditor });
    }
    if (prevProps.chartConfig !== chartConfig || !_.isEqual(prevProps.chartConfig, chartConfig)) {
      const chartConfigTmp = _.cloneDeep(chartConfig);
      chartConfig.chartData.keysArr && (chartConfigTmp.chartData.keysArr = chartConfig.chartData.keysArr)
      const { current } = this.state;
      const { chartData, type, renderType, selectedCategory, selectedCategoryFrom } = chartConfig;
      if (chartEditor && chartEditor.state.readOnly) {
        _.assign(state, { current: StepTypes.ExportFiles });
      } else if (chartConfig === DefaultAntChartConfig) {
        _.assign(state, { current: StepTypes.UploadData });
      } else {
        let disabled = false;
        if (renderType === RenderType.Chart) {
          if (~[StepTypes.ChooseChart].indexOf(current)) {
            if (!chartData.length) {
              disabled = true;
            }
          }
          if (~[StepTypes.ConfigChart, StepTypes.ExportFiles].indexOf(current)) {
            if (!type) {
              disabled = true;
            }
          }
        } else if (!selectedCategory || !selectedCategoryFrom) {
          disabled = true;
        }
        disabled && _.assign(state, { current: StepTypes.UploadData });
      }
      _.assign(state, {
        chartConfig: chartConfigTmp,
        steps: chartEditor && chartEditor.state.readOnly ? [StepTypes.ExportFiles] : [StepTypes.UploadData, ...NextStepsOfRenderType(renderType)],
        visible: chartConfig !== DefaultAntChartConfig
      });
      if (chartConfig.chartKey !== prevProps.chartConfig.chartKey) {
        _.assign(state, { oldChartConfig: chartConfig });
      }
    }
    if (state.oldChartConfig) {
      _.assign(state, {
        properties: [],
        columns: [],
        data: []
      });
    }
    !_.isEmpty(state) && this.setState(state, async () => {
      if (state.oldChartConfig) {
        let nstate = {};
        await this.mainContainFunc(nstate);
        await this.shareDataContainFunc(nstate);
        await this.filesContainFunc(nstate);
        !_.isEmpty(nstate) && this.setState(nstate);
      }
    });
  }


  focusSelectedChartFunc = () => {
    const { chartEditor } = this.state;
    chartEditor && chartEditor.focus();
  }
  drawerFullScreenFunc = () => {
    const { dFullScreen } = this.state;
    this.setState({ dFullScreen: !dFullScreen });
  }

  drawerKeydownFunc = async (e) => {
    e = e || window.event;
    let key = String.fromCharCode(e.keyCode).toLowerCase();
    if (!(/^[0-9a-z]$/g).test(key) || e.key === e.code) {
      key = e.key;
    }
    if (IsKey(e, key, window_shortcuts_keys['Drawer Full Screen'])) {
      this.drawerFullScreenFunc();
      e.preventDefault();
    } else if (IsKey(e, key, window_shortcuts_keys['Focus Selected Chart'])) {
      this.focusSelectedChartFunc();
      e.preventDefault();
    }
  }

  componentWillUnmount() {
    actions.deleteCache(this.refreshGraphElement);
    actions.deleteCache(this.shareDataElement);
    document.getElementById("drawer") && document.getElementById("drawer").removeEventListener("keydown", this.drawerKeydownFunc, false);
  }

  /**
   * 
   * @param {boolean} editorLoading 
   */
  setEditorLoading(editorLoading) {
    this.setState({ editorLoading });
  }

  mainContainFunc = async (stateTmp) => {
    const { chartEditor } = this.state;
    if (chartEditor && chartEditor.state.readOnly) { return; }
    let state = stateTmp || {};
    const { chartConfig: { selectedCategoryFrom, selectedCategory, selectedProperties, emptyFills, castColumns, joinColumns } }
      = this.state;
    if (selectedCategoryFrom === CategoryFromType.main) {
      let isMainContain = false;
      getCatogeries(window.currentEditor, CategoryFromType.main).map((value) => {
        if (!isMainContain) {
          isMainContain = selectedCategory === value;
        }
      })
      if (isMainContain) {
        await this.onCategoryChanged({ selectedCategoryFrom, selectedCategory, emptyFills, castColumns, joinColumns }, state)
        this.onRowSelectionChanged(selectedProperties, state, emptyFills, castColumns, joinColumns, undefined)
      } else {
        _.assign(state, {
          properties: [],
          columns: [],
          data: []
        });
      }
    }
    !stateTmp && this.setState(state);
  }

  shareDataContainFunc = async (stateTmp) => {
    const { chartEditor } = this.state;
    if (chartEditor && chartEditor.state.readOnly) { return; }
    let state = stateTmp || {};
    const { chartConfig: { selectedCategoryFrom, selectedCategory, selectedProperties, emptyFills, castColumns, joinColumns } }
      = this.state;
    if (selectedCategoryFrom === CategoryFromType.shareData) {
      let isShareDataContain = false;
      getCatogeries(window.currentEditor, CategoryFromType.shareData).map((value) => {
        if (!isShareDataContain) {
          isShareDataContain = selectedCategory === value;
        }
      })
      if (isShareDataContain) {
        await this.onCategoryChanged({ selectedCategoryFrom, selectedCategory }, state)
        this.onRowSelectionChanged(selectedProperties, state, emptyFills, castColumns, joinColumns, undefined)
      } else {
        _.assign(state, {
          properties: [],
          columns: [],
          data: []
        });
      }
    }
    !stateTmp && this.setState(state);
  }
  filesContainFunc = async (stateTmp) => {
    const { chartEditor } = this.state;
    if (chartEditor && chartEditor.state.readOnly) { return; }
    let state = stateTmp || {};
    const { chartConfig: { selectedCategoryFrom, selectedCategory, selectedProperties, emptyFills, castColumns, joinColumns } }
      = this.state;
    if (selectedCategoryFrom === CategoryFromType.files) {
      let isFilesContain = false;
      getCatogeries(window.currentEditor, CategoryFromType.files).map((value) => {
        if (!isFilesContain) {
          isFilesContain = selectedCategory === value.fileKey;
        }
      })
      if (isFilesContain) {
        await this.onCategoryChanged({ selectedCategoryFrom, selectedCategory }, state)
        this.onRowSelectionChanged(selectedProperties, state, emptyFills, castColumns, joinColumns)
      } else {
        _.assign(state, {
          properties: [],
          columns: [],
          data: []
        });
      }
    }
    !stateTmp && this.setState(state);
  }

  componentDidMount() {
    actions.inspector(this.refreshGraphElement, [actions.types.REFRESH_GRAPH], (graphxrApi) => {
      this.mainContainFunc();
    })
    actions.inspector(this.shareDataElement, [actions.types.SHARE_DATA], ({ ShareData, key }) => {
      const { chartConfig: { selectedCategoryFrom, selectedCategory, selectedProperties } }
        = this.state;
      if (selectedCategoryFrom === CategoryFromType.shareData && selectedCategory === key) {
        this.shareDataContainFunc();
      }
    })
    document.getElementById("drawer") && document.getElementById("drawer").addEventListener("keydown", this.drawerKeydownFunc, false);
  }

  onCategoryChanged = async (props = {
    selectedCategoryFrom: "",
    selectedCategory: "",
    selectedClientType: undefined,
    type: ChartTypes['Base Timeline'],
    renderType: RenderType.Chart,
    options: cloneDeepDefaultOptions(RenderType.Chart, ChartTypes['Base Timeline'])
  }, stateTmp) => {
    if (!props) return;
    let state = stateTmp || {};
    const { chartConfig } = this.state;
    state.chartConfig = _.assign({}, chartConfig, state.chartConfig || {}, props);
    const { selectedCategoryFrom, selectedCategory, mongo, dynamo, reD, neo } = state.chartConfig;
    if (selectedCategoryFrom === CategoryFromType.mongodb && !mongo) {
      _.assign(state.chartConfig, { mongo: _.cloneDeep(mongo_Template) })
    } else if (selectedCategoryFrom === CategoryFromType.dynamodb && !dynamo) {
      _.assign(state.chartConfig, { dynamo: _.cloneDeep(dynamo_Template) })
    } else if (selectedCategoryFrom === CategoryFromType.database && !reD) {
      _.assign(state.chartConfig, { reD: _.cloneDeep(reD_Template) })
    } else if (selectedCategoryFrom === CategoryFromType.neo4j && !neo) {
      _.assign(state.chartConfig, { neo: _.cloneDeep(neo_Template) })
    }
    const columnsData = await getColumnsData(window.currentEditor.getSelfModuleName(), selectedCategoryFrom, selectedCategory);
    _.assign(state, columnsData);
    if (props.renderType) {
      _.assign(state, { steps: [StepTypes.UploadData, ...NextStepsOfRenderType(props.renderType)] })
    }
    !stateTmp && this.setState(state);
  };
  /**
   * 
   * @param {DefaultAntChartConfig} props
   * @param {function} cb 
   */
  modifyChartConfig = (props, cb = () => { }) => {
    if (!props) return;
    const { chartConfig } = this.state;
    let state = { chartConfig: _.assign({}, chartConfig, props) }
    if (props.renderType) {
      _.assign(state, { steps: [StepTypes.UploadData, ...NextStepsOfRenderType(props.renderType)] })
    }
    this.setState(state, cb);
  }

  setColumnsData = (nodes, reset = false, stateTmp) => {
    const { chartConfig, chartConfig: { selectedProperties } } = this.state;
    let columnsData = extractColumnsData(nodes);
    if (!reset && _.filter(selectedProperties, (p, index) => { return !~columnsData.properties.indexOf(p) }).length > 0) {
      reset = true;
    }
    let state = stateTmp || {};
    _.assign(state, reset ? {
      chartConfig: _.assign({}, chartConfig, {
        selectedProperties: [],
        emptyFills: {},
        castColumns: {},
        joinColumns: {},
        type: ChartTypeTemplate.type,
        chartData: [],
        chartColumns: [],
      })
    } : {});
    _.assign(state, {
      ...columnsData
    });
    !stateTmp && this.setState(state);
  }

  onRowSelectionChanged = (selectedProperties = [], stateTmp, emptyFills = {}, castColumns = {}, joinColumns = {}, more = {}) => {
    let state = stateTmp || {};
    const properties = state.properties || this.state.properties;
    const data = state.data || this.state.data;
    const chartConfig = state.chartConfig || this.state.chartConfig;
    let chartData = chartDataFunc(data, selectedProperties, emptyFills, castColumns, joinColumns)
    let chartColumns = _.reduce([...selectedProperties,
    ..._.reduce(joinColumns, (prev, v, k) => { if (k && v && v instanceof Array && v.length > 0) { prev.push(k) } return prev; }, [])], (prev, property, index) => {
      prev.push({
        key: property,
        title: property,
        dataIndex: property,
      });
      return prev;
    }, []);
    _.assign(state, { chartConfig: _.assign(chartConfig, { selectedProperties, chartData, chartColumns, emptyFills, castColumns, joinColumns }, more) });
    !stateTmp && this.setState(state);
  }

  prev = () => {
    const { current, steps } = this.state;
    this.setCurrent(steps[steps.indexOf(current) - 1]);
  };

  next = () => {
    const { current, steps } = this.state;
    this.setCurrent(steps[steps.indexOf(current) + 1]);
  };

  setCurrent = (current) => {
    const { chartConfig: { chartData, type, renderType } } = this.state;
    if (renderType === RenderType.Chart && ~[StepTypes.ChooseChart].indexOf(current)) {
      if (!chartData.length) {
        return showToast("Please select appropriate dataSource and fields!", "error");
      }
    }
    if (renderType === RenderType.Chart && ~[StepTypes.ConfigChart, StepTypes.ExportFiles].indexOf(current)) {
      if (!type) {
        return showToast("No chart selected. Please click to select one!", "error");
      }
    }
    this.setState({ current: current });
  }

  onChange = current => {
    const { steps } = this.state;
    this.setCurrent(steps[current]);
  };

  getStepComp = () => {
    const { current, chartConfig, chartEditor, oldChartConfig, placement } = this.state;
    switch (current) {
      case StepTypes.UploadData:
        return <UploadData key={oldChartConfig.chartKey} settings={this}></UploadData>
      case StepTypes.ChooseChart:
        return <ChooseChart key={oldChartConfig.chartKey} settings={this}></ChooseChart>
      case StepTypes.ConfigChart:
        return <ConfigChart key={oldChartConfig.chartKey} settings={this}></ConfigChart>
      case StepTypes.ExportFiles:
        return <ExportFiles key={oldChartConfig.chartKey} settings={this}></ExportFiles>
    }
  }

  render() {
    const { className } = this.props;
    const { current, chartConfig: { renderType, type }, visible, chartEditor, oldChartConfig, placement, editorLoading, steps, dFullScreen } = this.state;
    const currentIndex = steps.indexOf(current);
    return (
      <DrawerEx direction={placement}
        dragCallback={(deltaPosition) => {
        }}
        getContainer={document.getElementById("drawer")}
        destroyOnClose={false}
        className={"chart-setting-drawer"}
        title={<div className="d-flex align-items-center mr4">
          <Steps style={{ marginRight: "60px" }} current={currentIndex} onChange={this.onChange}>
            {steps.map((item, index) => {
              let disabled = false;
              const { chartConfig: { chartData, type } } = this.state;
              if (renderType === RenderType.Chart && index >= steps.indexOf(StepTypes.ChooseChart)) {
                if (!chartData.length) {
                  disabled = true;
                }
              }
              if (renderType === RenderType.Chart && index >= steps.indexOf(StepTypes.ConfigChart)) {
                if (!type) {
                  disabled = true;
                }
              }
              return <Step disabled={disabled} key={item} title={item.camelPeakToBlankSplit()} />
            })}
          </Steps>&nbsp;&nbsp;<i title={ShortcutTitle(window_shortcuts_keys['Focus Selected Chart'])} className="icon zoom-out-icon fas fa-crosshairs" onClick={this.focusSelectedChartFunc}></i>&nbsp;&nbsp;<i title={ShortcutTitle(window_shortcuts_keys['Drawer Full Screen'])} className="icon zoom-out-icon fas fa-expand-alt" onClick={this.drawerFullScreenFunc}></i>&nbsp;&nbsp;</div>}
        initHeight={window.innerHeight / 2}
        fullScreen={dFullScreen}
        mask={false}
        onClose={chartEditor && chartEditor.cancelEdit}
        visible={visible}
        bodyStyle={{ paddingBottom: 12 }}
        footer={
          <div className="d-flex flex-wrap justify-content-end btns">
            {currentIndex > 0 && (
              <Button type="button" className="btn btn-secondary" onClick={() => {
                this.prev()
              }}>
                Previous
              </Button>
            )}
            {currentIndex < steps.length - 1 && (
              <Button type="button" className="btn btn-secondary" onClick={() => {
                chartEditor && chartEditor.run()
                this.next()
              }}>
                Next
              </Button>
            )}
            {currentIndex === steps.length - 1 && (
              <Button type="button" className="btn btn-success" onClick={() => {
                chartEditor && chartEditor.publishEdit();
                message.success('Processing complete!');
              }}>
                Done
              </Button>
            )}
            <Button className="btn btn-secondary" onClick={chartEditor && chartEditor.cancelEdit}>
              Cancel
            </Button>
            <Button onClick={chartEditor && chartEditor.publishEdit} type="primary">
              Submit
            </Button>
          </div>
        }
      >
        <div className={`chart-settings ${className || ''}`} >
          <div className="steps-content">
            {this.getStepComp()}
            {editorLoading && <div className="code-loading-top">
              <Spin spinning={true} indicator={<span> <Loading3QuartersOutlined style={{ fontSize: 24, }} spin /><CloseOutlined onClick={() => {
                this.state.editorLoading && this.setState({ editorLoading: !this.state.editorLoading });
              }} /> </span>} >
                <div className="editor-full-div"></div>
              </Spin>
            </div>}
          </div>
        </div >
        <BackTop target={() =>
          document.querySelector(".chart-setting-drawer .ant-drawer-body")
        }
          visibilityHeight={200}>
          <UpCircleOutlined className="ant-back-top-icon" />
        </BackTop>
      </DrawerEx>
    );
  }
}