const Mongoose = require("mongoose");
const _ = require("lodash");
const SchemaCRUD = require("../schemaCRUD/schemaCRUD");

const Schema = Mongoose.Schema;
const GrovePinned = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "users" },
  projectId: { type: String },
  fileName: { type: String },
  fileKey: { type: String },
  accessPath: { type: String },
  updateTime: { type: Date, default: Date.now },
  createTime: { type: Date, default: Date.now },
});

GrovePinned.plugin(SchemaCRUD.CRUD);

GrovePinned.plugin(function (schema) {
  schema.statics.updateStruct = function (cb) {
    cb(null, null);
  };

  schema.statics.add = function (projectId, userId, itemData, cb) {
    if (!projectId) {
      return cb(new Error("Miss project ID"));
    }
    let item = Object.assign(
      {
        user: userId,
        projectId: projectId,
      },
      itemData
    );
    item.updateTime = new Date();
    if (item._id) {
      let id = item._id;
      delete item._id;
      return SchemaCRUD.edit(this, id, item, cb);
    } else {
      return SchemaCRUD.add(this, item, cb, false);
    }
  };

  schema.statics.getInfo = function (id, cb) {
    let condition = {
      _id: id,
    };
    return this.findOne(condition, cb);
  };

  schema.statics.getWithProjectId = function (projectId, userId, cb) {
    let condition = {
      option: {
        projectId: projectId,
      },
    };

    // if (userId) {
    //   condition.option["user"] = userId;
    // }

    return SchemaCRUD.queryAll(this, condition, cb, null);
  };

  schema.statics.getAll = function (condition, cb, isCount) {
    return SchemaCRUD.queryAll(this, condition, cb, null, isCount);
  };
});

module.exports = Mongoose.model("grovePinneds", GrovePinned);
