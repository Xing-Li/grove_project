import axios from 'axios';
// import { createHmac } from "crypto";
import _ from 'lodash';
import { USER_NAME } from 'util/localstorage';
import { autoSqliteObjectsType, calcSqliteAutoType, nanOrBlank, stringifyNanOrBlank } from '../util/helpers';
import { ConnectionHostedByType, DatabaseType, FailedResults, getTestSql, ShortDatabaseType, showToast, DatabaseFields, isFailedResults, DynamoOperationType, MongoOperationType, assembleErrMessage, extractDynamoDatas } from '../util/utils';
import { createInteger } from '../util/neoUtils'
import Neo4jClient from '../lib/neo/neo4j';
import DynamoClient from '../lib/dynamo/dynamo';
import { require as requireDefault } from "./grove-require";
import sqlite, { SQLiteDatabaseClient } from "./sqlite.js";
import { Workbook } from "./xlsx.js";
import { Integer, Node, Record, Relationship } from 'neo4j-driver';

const { DefaultDatabase } = require("util/databaseApi");
/**@type {Object.<string,DynamoClient>} */
const clientMap = {};
window.clientMap = clientMap;


export const extractConfig = function (config) {
    if (~["127.0.0.1", "localhost"].indexOf(config.host)) {
        switch (config.type) {
            case DatabaseType.Neo4j:
                switch (config.protocal) {
                    case 'bolt':
                        break
                    case 'bolt+s':
                        config.protocal = 'bolt'
                        break
                    case 'bolt+ssc':
                        config.protocal = 'bolt'
                        break
                    case 'neo4j':
                        break
                    case 'neo4j+s':
                        config.protocal = 'neo4j'
                        break
                    case 'neo4j+ssc':
                        config.protocal = 'neo4j'
                        break
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }
    return config;
}
export function createDatabaseClient(config) {
    let database = extractConfig(config);
    switch (database.type) {
        case DatabaseType.Mysql:
            return new MysqlDatabaseClient(database);
        case DatabaseType.PostgreSQL:
            return new PostgreSQLDatabaseClient(database);
        case DatabaseType.MSSQL:
            return new MSSQLDatabaseClient(database);
        case DatabaseType.MongoDB:
            return new MongoDBDatabaseClient(database);
        case DatabaseType.DynamoDB:
            return new DynamoDBDatabaseClient(database);
        case DatabaseType.Neo4j:
            return new Neo4jDatabaseClient(database);
        case DatabaseType.Snowflake:
            return new SnowflakeDatabaseClient(database);
        case DatabaseType.BigQuery:
            return new BigqueryDatabaseClient(database);
    }
}
/**
 * 
 * @param {string} name 
 * @returns {DatabaseClient}
 */
export default function databaseClient(name) {
    if (name === 'GraphxrNeo4j') {
        return new GraphxrNeo4jDatabaseClient();
    }
    let databases = _.filter(window.editor.getSettings().getDatabases(), (database, index) => {
        return database.name === name;
    });
    if (databases.length !== 1) {
        console.error("Database not found");
        return null;
    }
    return createDatabaseClient(databases[0]);
}
export function createClient(config) {
    let url = _.reduce(config, (prev, v, k) => {
        prev = prev.replace(`{${k}}`, v)
        return prev;
    }, DatabaseFields[config.type].template || "");
    switch (config.type) {
        case DatabaseType.Neo4j:
            return new Neo4jClient(url, config);
        case DatabaseType.DynamoDB:
            return new DynamoClient(url, config);
        default:
            return null;
    }
}
/**
 * 
 * @param {Workbook} wb 
 * @param {boolean} headers 
 */
export async function workbookToDb(wb, headers = true) {
    if (!wb || !(wb instanceof Workbook)) {
        return;
    }
    let datasArray = _.reduce(wb.sheetNames, (prev, sheetName, index) => {
        prev.push(sheetName.toCommonName());
        prev.push(wb.sheet(sheetName, { headers }));
        return prev;
    }, [])
    return await datasToDb(...datasArray);
}

/**
 * 
 * @param {string} tableName 
 * @param {Object[]} datas 
 */
export async function datasToDb() {
    if (arguments.length < 2) {
        return;
    }
    let create = "";
    for (let index = 1; index < arguments.length; index += 2) {
        let tableName = arguments[index - 1];
        let datas = arguments[index];
        if (typeof tableName !== 'string' || typeof datas !== 'object' || !(datas instanceof Array) || datas.length < 1) {
            continue;
        }
        let objTypes = calcSqliteAutoType(datas, false, true);
        create += `CREATE TABLE ${tableName} (` + _.reduce(objTypes, (prev, objType, columnName) => {
            if (prev !== "") {
                prev += ', '
            }
            prev += `${columnName.toCommonName().replace(/^(ABORT|ACTION|ADD|AFTER|ALL|ALTER|ALWAYS|ANALYZE|AND|AS|ASC|ATTACH|AUTOINCREMENT|BEFORE|BEGIN|BETWEEN|BY|CASCADE|CASE|CAST|CHECK|COLLATE|COLUMN|COMMIT|CONFLICT|CONSTRAINT|CREATE|CROSS|CURRENT|CURRENT_DATE|CURRENT_TIME|CURRENT_TIMESTAMP|DATABASE|DEFAULT|DEFERRABLE|DEFERRED|DELETE|DESC|DETACH|DISTINCT|DO|DROP|EACH|ELSE|END|ESCAPE|EXCEPT|EXCLUDE|EXCLUSIVE|EXISTS|EXPLAIN|FAIL|FILTER|FIRST|FOLLOWING|FOR|FOREIGN|FROM|FULL|GENERATED|GLOB|GROUP|GROUPS|HAVING|IF|IGNORE|IMMEDIATE|IN|INDEX|INDEXED|INITIALLY|INNER|INSERT|INSTEAD|INTERSECT|INTO|IS|ISNULL|JOIN|KEY|LAST|LEFT|LIKE|LIMIT|MATCH|MATERIALIZED|NATURAL|NO|NOT|NOTHING|NOTNULL|NULL|NULLS|OF|OFFSET|ON|OR|ORDER|OTHERS|OUTER|OVER|PARTITION|PLAN|PRAGMA|PRECEDING|PRIMARY|QUERY|RAISE|RANGE|RECURSIVE|REFERENCES|REGEXP|REINDEX|RELEASE|RENAME|REPLACE|RESTRICT|RETURNING|RIGHT|ROLLBACK|ROW|ROWS|SAVEPOINT|SELECT|SET|TABLE|TEMP|TEMPORARY|THEN|TIES|TO|TRANSACTION|TRIGGER|UNBOUNDED|UNION|UNIQUE|UPDATE|USING|VACUUM|VALUES|VIEW|VIRTUAL|WHEN|WHERE|WINDOW|WITH|WITHOUT)$/ig, "_$1")} ${objType.type}`;
            return prev;
        }, ``) + " );\n"

        create += _.reduce(autoSqliteObjectsType(datas), (prev, data, index2) => {
            let ss = _.reduce(objTypes, (prev, objType, k) => {
                let v = data[k];
                if (prev !== "") {
                    prev += ', '
                }
                switch (objType.type) {
                    case 'NULL':
                        prev += "NULL";
                        break;
                    case 'INTEGER':
                        prev += (nanOrBlank(v) ? "NULL" : v);
                        break;
                    case 'REAL':
                        prev += (nanOrBlank(v) ? "NULL" : v);
                        break;
                    case 'TEXT':
                        prev += `"${nanOrBlank(v) ? stringifyNanOrBlank(v) : v}"`;
                        break;
                    case 'BLOB':
                        prev += (nanOrBlank(v) ? "NULL" : v);
                        break;
                }
                return prev;
            }, '');
            prev += `INSERT INTO ${tableName} VALUES ( ${ss} );\n`;
            return prev;
        }, "")
    }
    const SQL = await sqlite(requireDefault)
    const db = new SQL.Database();
    db.run(create);
    return new SQLiteDatabaseClient(db);
}

export class CommonClient {
    /**
     * 
     * @param {DefaultDatabase} config 
     */
    constructor(config, filterObj = true) {
        this.config = config;
        this.filterObj = filterObj;
        this.location = undefined;
    }
    /**
     * table data need filter object cell, or can not display in table
     * 
     * @param {*} results 
     */
    filter(results) {
        if (!results || !(results instanceof Array)) {
            return;
        }
        _.each(results,/**@param {{}} row */(row) => {
            _.each(row, (value, key, row) => {
                if (typeof value === 'object' && this.filterObj) {
                    row[key] = value && value.toString();
                }
            })
        });
    }
    async proxyQuery(query, params) {
        // let payload = Buffer.from(JSON.stringify({
        //     owner: USER_NAME,
        //     name: this.config.name,
        //     type: ShortDatabaseType[this.config.type],
        //     origin: location.origin,
        //     exp: new Date().getTime() + 60 * 60 * 1000
        // }))
        if (!this.config.secret) {
            return showToast("Init Error", 'warning')
        }
        // let secret = JSON.parse(Buffer.from(this.config.secret, "base64")).secret;
        // let hmac = createHmac("sha256", Buffer.from(secret, "hex"))
        //     .update(payload)
        //     .digest()
        let headers = {} //{ Authorization: `Bearer ${payload.toString("base64")}.${hmac.toString("base64")}` }
        return await axios.post(`${this.config.sslTls ? "https" : "http"}://${this.config.proxyHost}:${this.config.port}${this.config.location || ""}`, { sql: query, params }, { headers: headers })
            .then(
                (res) => {
                    return res;
                },
                (e) => {
                    console.error(e)
                    return { status: 1, message: e.message };
                }
            );
    }
    async serverProxyQuery(query, params) {
        let data = { ...this.config };
        data["sql"] = query
        params && (data["data"] = params);
        return await axios.post(`/api/${ShortDatabaseType[this.config.type]}/query`, data).then(
            (res) => {
                return res.data;
            },
            (e) => {
                console.error(e)
                return { status: 1, message: e.message };
            }
        );
    }

    /**
     * 
     * @param {{error:string}} result 
     */
    assembleFailResults(result) {
        return assembleErrMessage(result);
    }

    /**
     * 
     * @param {string} query 
     * @param {{}} params 
     * @returns {Promise}
     */
    async query(query, params) {
        if (!query) {
            return this.assembleFailResults({ error: "Query is Empty!" });
        }
        if (undefined !== this.location) {
            if (params) {
                params.location = this.location;
            } else {
                params = { location: this.location }
            }
        }
        if (this.config.connectionHostedBy === ConnectionHostedByType["Self-hosted proxy"]) {
            let result = await this.proxyQuery(query, params);
            if (result && result.status == 200) {
                if (!result.data || !result.data.data) {
                    return this.assembleFailResults({ error: result.data && result.data.error || result.data || "Have no results!" });
                }
                this.filter(result.data.data);
                return result.data.data;
            } else {
                return this.assembleFailResults(result);
            }
        } else if (this.config.connectionHostedBy === ConnectionHostedByType.Grove) {
            let result = await this.serverProxyQuery(query, params);
            if (result.status == 0) {
                if (!result.content || !result.content.results) {
                    return this.assembleFailResults({ error: result.content && result.content.error || result.content || "Have no results!" });
                }
                let ret = result.content.results;
                if (ret.length === 2 && ret[0] instanceof Array && ret[1] instanceof Array) {
                    ret = ret[0]
                }
                this.filter(ret);
                return ret;
            } else {
                return this.assembleFailResults(result);
            }
        } else if (this.config.connectionHostedBy === ConnectionHostedByType.Client) {//if self client get the real result
            let client = clientMap[this.config.name];
            if (!client || !_.isEqual(client.proxyConfig, this.config)) {
                if (client) {
                    client.close();
                }
                client = createClient(_.cloneDeep(this.config));
                if (client) {
                    clientMap[this.config.name] = client;
                }
            }
            if (client) {
                let result = await client.query(query, params)
                return result;
            }
            return this.assembleFailResults({ error: "Can not find client!" });
        } else {
            return this.assembleFailResults({ error: "Not support connection type!" });
        }
    }
    get dialect() {
        return this.config && this.config.type || "database";
    }
}

class GraphxrNeo4jDatabaseClient extends CommonClient {
    constructor(config) {
        super(config, false)
    }

    assembleFailResults(result) {
        return { error: result };
    }

    /**
     * 
     * @param {string} query 
     * @param {{}} params 
     * @returns {Promise}
     */
    async query(query, params) {
        if (!window.graphxrApi) {
            this.assembleFailResults({ error: "NetworkError, please try again!" });
        }
        try {
            let result = window.graphxrApi && (await window.graphxrApi.neo4j(query, params));
            if (!result || !result._content || !result._content.data) {
                return {
                    records: [], summary: {}
                }
            }
            if (result._content.type === 'TABLE') {
                let keys = result._content.data[0];
                let fieldLookup = _.reduce(keys, (prev, curr, index) => {
                    prev[curr] = index;
                    return prev;
                }, {})
                let records = [];
                _.reduce(result._content.data.slice(1), (prev, curr, index) => {
                    let fields = curr;
                    prev.push(new Record(keys, fields, fieldLookup))
                    return prev;
                }, records)
                return {
                    records, summary: result._content.summary
                }
            } else if (result._content.type === 'GRAPH') {
                let keys = ["m", "n", "r"];
                let fieldLookup = _.reduce(keys, (prev, curr, index) => {
                    prev[curr] = index;
                    return prev;
                }, {})
                let records = [];
                _.reduce(result._content.data.relationships, (prev, rT, index) => {
                    let mT = result._content.data.nodes[index * 2]
                    let nT = result._content.data.nodes[index * 2 + 1]
                    let m = new Node(createInteger(parseInt(mT.id)), mT.labels, mT.properties, mT.id);
                    let n = new Node(createInteger(parseInt(nT.id)), nT.labels, nT.properties, nT.id);
                    let r = new Relationship(createInteger(parseInt(rT.id)), createInteger(parseInt(rT.startNodeId)), createInteger(parseInt(rT.endNodeId)), rT.type, rT.properties, rT.id, rT.startNodeId, rT.endNodeId);
                    prev.push(new Record(keys, [m, n, r], fieldLookup))
                    return prev;
                }, records)
                return {
                    records, summary: result._content.summary
                }
            }
        } catch (e) {
            console.error(e)
            return this.assembleFailResults({ error: e.message });
        }
    }
}

export class DatabaseClient extends CommonClient {
    /**
     * 
     * @param {DefaultDatabase} config 
     */
    constructor(config, filterObj = true) {
        super(config, filterObj);
    }
    async describeTables({ schema } = {}) {
        return (await this.query(getTestSql(this.config)));
    }
    async describe(object) {
        return (await this.query(getTestSql(this.config)));
    }
    async queryRow(query, params) {
        return (await this.query(query, params))[0] || null;
    }

    getSql(strings) {
        if (!(strings instanceof Array)) {
            return strings;
        }
        let string = strings[0],
            parts = [], part,
            root = null,
            node, nodes,
            walker,
            i, n, j, m, k = -1;

        // Concatenate the text using comments as placeholders.
        for (i = 1, n = arguments.length; i < n; ++i) {
            part = arguments[i];
            if (part instanceof Node) {
                parts[++k] = part;
                string += "<!--o:" + k + "-->";
            } else if (Array.isArray(part)) {
                for (j = 0, m = part.length; j < m; ++j) {
                    node = part[j];
                    if (node instanceof Node) {
                        if (root === null) {
                            parts[++k] = root = document.createDocumentFragment();
                            string += "<!--o:" + k + "-->";
                        }
                        root.appendChild(node);
                    } else {
                        root = null;
                        string += node;
                    }
                }
                root = null;
            } else {
                string += part;
            }
            string += strings[i];
        }
        return string;
    }

    async sql() {
        return (await this.query(this.getSql(...arguments)));
    }
    async describeColumns({ schema, table } = {}) {
    }
}
class MysqlDatabaseClient extends DatabaseClient {
    constructor(config) {
        super(config)
    }
    async describeColumns({ schema, table } = {}) {
        return (await this.query(`describe ${table}`));
    }
}

class BigqueryDatabaseClient extends DatabaseClient {
    constructor(config) {
        super(config)
    }

    async getDatasets() {
        return await this.query("getDatasets");
    }

    async describeTables({ schema } = {}) {
        return await this.query("getTables", { datasetId: schema });
    }

    async describeColumns({ schema, table } = {}) {
        return await this.query("getMetadata", { datasetId: schema, tableId: table });
    }
}

class PostgreSQLDatabaseClient extends DatabaseClient {
    constructor(config) {
        super(config)
    }
    async describeColumns({ schema, table } = {}) {
        let sql = `SELECT c.column_name, c.data_type, c.is_nullable
FROM information_schema.columns c
WHERE c.table_name = '${table}'  AND table_schema = '${schema}'
ORDER BY c.ordinal_position`
        return (await this.query(sql));
    }
}
class MSSQLDatabaseClient extends DatabaseClient {
    constructor(config) {
        super(config)
    }
    async describeColumns({ schema, table } = {}) {
        return (await this.query(`SELECT COLUMN_NAME, DATA_TYPE , IS_NULLABLE
         FROM INFORMATION_SCHEMA. COLUMNS WHERE TABLE_NAME = '${table}' and TABLE_SCHEMA='${schema}'`));
    }
}
class Neo4jDatabaseClient extends CommonClient {
    constructor(config) {
        super(config, false)
    }

    assembleFailResults(result) {
        return { error: result };
    }
}

class MongoDBDatabaseClient extends CommonClient {
    constructor(config) {
        super(config, false)
    }

    async describeTables() {
        return (await this.query(JSON.stringify({ operationType: MongoOperationType.listCollectionNames })));
    }

    async describeColumns(collectionName) {
        return (await this.query(JSON.stringify({ operationType: MongoOperationType.listFields, collectionName: collectionName })));
    }
}

class DynamoDBDatabaseClient extends DatabaseClient {
    constructor(config) {
        super(config, false)
    }

    async describeTables() {
        return (await this.query(JSON.stringify({ operationType: DynamoOperationType.ListTables })));
    }

    async describeColumns({ schema, table } = {}) {
        let keys = await this.query(JSON.stringify({ operationType: DynamoOperationType.DescribeTable, tableName: table }));
        if (!isFailedResults(keys) && keys.AttributeDefinitions && keys.KeySchema) {
            let items = await this.query(JSON.stringify({
                operationType: DynamoOperationType.ExecuteStatement, params: {
                    Statement: `SELECT * FROM ${table}`,
                    Limit: 1,
                    ConsistentRead: true,
                }
            }))
            let allKeys = _.reduce(keys.AttributeDefinitions, (prev, curr, index) => {
                prev.push({ column_name: curr.AttributeName, data_type: curr.AttributeType, key_type: _.filter(keys.KeySchema, (obj, index) => { return obj.AttributeName === curr.AttributeName })[0].KeyType })
                return prev;
            }, [])
            if (!isFailedResults(items) && items.Items && items.Items.length === 1) {
                return _.concat(allKeys, _.reduce(items.Items[0], (prev, curr, key) => {
                    if (_.filter(allKeys, (k, i) => { return k.column_name === key }).length === 0) {
                        prev.push({ column_name: key, data_type: _.keys(curr)[0] })
                    }
                    return prev;
                }, []));
            }
            return allKeys;
        }
        return keys;
    }
    async sql() {
        let sql = this.getSql(...arguments);
        sql = _.reduce(sql.trim().split("\n"), (prev, line, index) => {
            line = line.trim();
            if (line && !line.startsWith("--")) {
                prev += line;
            }
            return prev;
        }, "")
        if (undefined === this.limit) {
            if (/^select\s+/i.test(sql)) {
                let match = sql.match(/(\s+limit\s+(\d+))(\s+)?$/i)
                if (null !== match && match.length === 4) {
                    this.limit = parseInt(match[2])
                    sql = sql.substring(0, sql.length - match[0].length);
                } else {
                    this.limit = 100;
                }
            }
        }
        let items = await this.query(JSON.stringify({
            operationType: DynamoOperationType.ExecuteStatement,
            params: _.assign({
                Statement: sql,
                ConsistentRead: true,
            }, this.limit ? { Limit: this.limit } : {})
        }))
        delete this.limit;
        return extractDynamoDatas(items);
    }
}

class SnowflakeDatabaseClient extends DatabaseClient {
    constructor(config) {
        super(config)
    }
    async describeColumns({ schema, table } = {}) {
        return (await this.query(`SELECT COLUMN_NAME, DATA_TYPE , IS_NULLABLE
         FROM INFORMATION_SCHEMA. COLUMNS WHERE TABLE_NAME = '${table}' and TABLE_SCHEMA='${schema}'`));
    }
}