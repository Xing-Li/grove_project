import React from 'react';
import { ChartProps } from '../util/Neo4jChart';
import { ResponsiveChoropleth } from '@nivo/geo';
import { useState, useEffect } from 'react';
import { checkResultKeys, recordToNative } from '../util/ChartUtils';
import { NoDrawableDataErrorMessage } from './CodeViewerComponent';

/**
 * Embeds a NeoChoroplethMapChart (from Charts) into NeoDash.
 * @param {ChartProps} props
 */
const NeoChoroplethMapChart = (props) => {
    const txyFunc = props.txyFunc;
    const records = props.records;
    const selection = props.selection;

    if (!selection || props.records == null || props.records.length == 0 || props.records[0].keys == null) {
        return <NoDrawableDataErrorMessage />
    }

    //TODO Apply certain logic to determine different map features to display
    //const feature = globeFeature;
    const [feature, setFeature] = useState({ features: [] });
    // TODO Think of a way to make it configurable to fetch vector data.
    // It makes sense to ship this JSON with NeoDash deployments that are behind some firewall and can't access this site.
    useEffect(() => {
        const abortController = new AbortController();
        fetch("https://raw.githubusercontent.com/plouc/nivo/master/website/src/data/components/geo/world_countries.json", { signal: abortController.signal })
            .then((res) => res.json())
            .then((matched) => {
                setFeature(matched)
            });
        // returned function will be called on component unmount 
        return () => {
            abortController.abort();
        }
    }, []);

    const keys = {};

    let data = records.reduce(
        /**
         * 
         * @param {Record<string, any>} data 
         * @param {Record<string, any>} row 
         */
        (data, row) => {
            try {
                const index = recordToNative(row.get(selection['index']));
                const value = recordToNative(row.get(selection['value']));

                if (!index || index.length != 3 || isNaN(value)) {
                    return data;
                    //throw "Invalid selection for choropleth chart. Ensure a three letter country code is retrieved together with a value."
                }
                data.push({ "id": index, "value": value });
                return data;
            } catch (e) {
                console.error(e);
                return [];
            }
        }, []);

    let m = Math.max(...data.map(o => o.value));

    const settings = (props.settings) ? props.settings : {};
    const marginRight = (settings["marginRight"]) ? settings["marginRight"] : 24;
    const marginLeft = (settings["marginLeft"]) ? settings["marginLeft"] : 24;
    const marginTop = (settings["marginTop"]) ? settings["marginTop"] : 24;
    const marginBottom = (settings["marginBottom"]) ? settings["marginBottom"] : 40;
    const interactive = (settings["interactive"] !== undefined) ? settings["interactive"] : true;
    const borderWidth = (settings["borderWidth"]) ? settings["borderWidth"] : 0;
    const legend = (settings["legend"] !== undefined) ? settings["legend"] : true;
    const colorScheme = (settings["colors"] && settings["colors"] !== 'neodash') ? settings["colors"] : 'nivo';
    const projectionScale = (settings["projectionScale"]) ? settings["projectionScale"] : 100;
    const projectionTranslationX = (settings["projectionTranslationX"]) ? settings["projectionTranslationX"] : 0.5;
    const projectionTranslationY = (settings["projectionTranslationY"]) ? settings["projectionTranslationY"] : 0.5;
    const labelProperty = (settings["labelProperty"]) ? settings["labelProperty"] : "properties.name";


    if (!data || data.length == 0) {
        return <NoDrawableDataErrorMessage />;
    }
    let startDrag = false;
    let enableWheel = false;
    let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    return (
        <>
            <div style={{ position: "relative", overflow: "hidden", width: "100%", height: "100%" }}
                onDoubleClick={(e) => {
                    e = e || window.event;
                    let svgElement = e.currentTarget.querySelector("svg>g")
                    let currentTransform = svgElement.getAttribute("transform");
                    if (currentTransform) {
                        let arr = currentTransform.match(/scale\((-?\d*\.?\d*)\)/)
                        let scale;
                        if (arr && arr.length > 1) {
                            scale = parseFloat(arr[1])
                        } else {
                            scale = 1;
                        }
                        arr = currentTransform.match(/translate\((-?\d*\.?\d*)\,(-?\d*\.?\d*)\)/)
                        if (arr && arr.length > 2) {
                            let x = parseInt(arr[1]) - 24;
                            let y = parseInt(arr[2]) - 24;
                            let dx = x / svgElement.getBoundingClientRect().width / scale;
                            let dy = y / svgElement.getBoundingClientRect().height / scale;
                            if (txyFunc) {
                                svgElement.setAttribute("transform", "translate(24,24)");
                                txyFunc(dx, dy, scale);
                            }
                        }
                    }
                    e.preventDefault();
                    e.stopPropagation();
                }}
                onMouseDown={(e) => {
                    e = e || window.event;
                    e.preventDefault();
                    pos3 = e.clientX;
                    pos4 = e.clientY;
                    startDrag = true
                    e.target.focus && e.target.focus();
                }}
                onMouseUp={() => {
                    startDrag = false;
                }}
                onMouseEnter={(e) => {
                    if (enableWheel) {
                        return;
                    }
                    let onWheelFunc = (e) => {
                        e = e || window.event;
                        let svgElement = e.currentTarget.querySelector("svg>g")
                        let currentTransform = svgElement.getAttribute("transform");
                        if (currentTransform) {
                            let arr = currentTransform.match(/scale\((-?\d*\.?\d*)\)/)
                            let scale;
                            if (arr && arr.length > 1) {
                                scale = parseFloat(arr[1])
                            } else {
                                scale = 1;
                            }
                            let oldScale = scale;
                            scale += e.deltaY * -0.0005;
                            scale = Math.min(Math.max(0.125, scale), 4);
                            let newTransform = `scale(${scale})`;
                            if (arr && arr.length > 1) {
                                currentTransform = currentTransform.replace(arr[0], newTransform);
                            } else {
                                currentTransform = `${currentTransform} ${newTransform}`;
                            }
                            arr = currentTransform.match(/translate\((-?\d*\.?\d*)\,(-?\d*\.?\d*)\)/)
                            if (arr && arr.length > 2) {
                                let newTransform = `translate(${parseInt(arr[1] * (1 + (scale - oldScale)))},${parseInt(arr[2] * (1 + (scale - oldScale)))})`;
                                currentTransform = currentTransform.replace(arr[0], newTransform);
                            }
                            svgElement.setAttribute("transform", currentTransform);
                        }
                        e.preventDefault();
                        e.stopPropagation();
                        return false;
                    }
                    e.currentTarget.addEventListener('wheel', onWheelFunc, { passive: false });
                    enableWheel = true;
                }}
                onMouseOut={() => {
                    if (startDrag) {
                        startDrag = false;
                    }

                }}
                onMouseMove={(e) => {
                    if (startDrag) {
                        e = e || window.event;
                        e.preventDefault();
                        pos1 = pos3 - e.clientX;
                        pos2 = pos4 - e.clientY;
                        pos3 = e.clientX;
                        pos4 = e.clientY;
                        let svgElement = e.currentTarget.querySelector("svg>g")
                        let currentTransform = svgElement.getAttribute("transform");
                        if (currentTransform) {
                            let arr = currentTransform.match(/translate\((-?\d*\.?\d*)\,(-?\d*\.?\d*)\)/)
                            if (arr && arr.length > 2) {
                                let newTransform = `translate(${parseInt(arr[1] - pos1)},${parseInt(arr[2] - pos2)})`;
                                svgElement.setAttribute("transform", currentTransform.replace(arr[0], newTransform));
                            }
                        }
                    }
                }}

            >
                <ResponsiveChoropleth
                    data={data}
                    isInteractive={interactive}
                    features={feature.features}
                    domain={[0, m]}
                    margin={{ top: marginTop, right: marginRight, bottom: marginBottom, left: marginLeft }}
                    colors={colorScheme}
                    unknownColor="#666666"
                    label={labelProperty}
                    valueFormat=".2s"
                    projectionScale={projectionScale}
                    projectionTranslation={[projectionTranslationX, projectionTranslationY]}
                    projectionRotation={[0, 0, 0]}
                    enableGraticule={true}
                    graticuleLineColor="#dddddd"
                    borderWidth={borderWidth}
                    borderColor="#152538"
                    tooltip={(props) => {
                        return !!props.feature.label && <div className={`d-flex align-items-center theme`}>
                            <svg width="1rem" height="1rem">
                                <rect width="1rem" height="1rem" fill={props.feature.color}></rect>
                            </svg>  {props.feature.label}:{props.feature.value}</div>
                    }}
                    legends={legend ? [
                        {
                            anchor: 'bottom-left',
                            direction: 'column',
                            justify: true,
                            translateX: 20,
                            translateY: -100,
                            itemsSpacing: 0,
                            itemWidth: 94,
                            itemHeight: 18,
                            itemDirection: 'left-to-right',
                            itemTextColor: 'var(--text-color)',
                            itemOpacity: 0.85,
                            symbolSize: 18,
                            effects: [
                                {
                                    on: 'hover',
                                    style: {
                                        itemTextColor: 'var(--text-color-hover)',
                                        itemOpacity: 1
                                    }
                                }
                            ]
                        }
                    ] : []}
                />
            </div>
        </>)
}

export default NeoChoroplethMapChart;