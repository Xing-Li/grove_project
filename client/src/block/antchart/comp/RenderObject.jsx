import {
  Dropdown,
  Table,
  Menu
} from 'antd';
import _ from 'lodash';
import PropTypes from "prop-types";
import React from "react";
const { copyContent } = require("../../../util/utils");
const { showObject, stringifyNanOrBlank } = require('../../../util/helpers.js');





const formatProperty = (property) => {
  if (property.startsWith("http://") || property.startsWith("https://")) {
    return <a href={property}>{property}</a>;
  }
  return property;
}




export default class RenderObject extends React.Component {
  static propTypes = {
    obj: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    open: PropTypes.bool,
    table: PropTypes.bool,
    rows: PropTypes.bool,
    cellLen: PropTypes.number,
    pagination: PropTypes.oneOfType([PropTypes.object, PropTypes.bool])
  };

  static defaultProps = {};

  static stringRows = function (obj, cellLen) {
    if (!obj || _.isEmpty(obj)) {
      return `<div>0 Rows.</div>`
    }
    let lenR = {
      key: 0,
      v: 0,
    }
    let datas = _.map(obj, (v, key) => {
      lenR.key = lenR.key < key.length ? key.length : lenR.key;
      let vLen = (stringifyNanOrBlank(v)).toString().length;
      lenR.v = lenR.v < vLen ? vLen : lenR.v;
      return {
        key,
        v: v ? formatProperty((stringifyNanOrBlank(v)).toString()) : v
      }
    })
    let columns = _.reduce(_.keys(datas[0]), (prev, property, index) => {
      let width = lenR[property] * 14 < cellLen * 14 ? lenR[property] * 14 : cellLen * 14;
      prev.push({
        key: property,
        title: property,
        dataIndex: property,
        width: width,
        ellipsis: true,
        render: (text) => {
          return formatProperty((stringifyNanOrBlank(text)).toString())
        }
      });
      return prev;
    }, []);
    return `<div class="ant-dropdown-trigger code-edit-result">
      <div class="data">
        <div>
        </div>
        <div class="ant-table-wrapper">
          <div class="ant-spin-nested-loading">
            <div class="ant-spin-container">
              <div class="ant-table ant-table-small">
                <div class="ant-table-container">
                  <div class="ant-table-content">
                    <table style="table-layout:fixed">
                      <colgroup>
                        ${_.map(columns, (c) => {
      return `<col style="width:${c.width}px">`
    }).join("")}
                      </colgroup>
                      <tbody class="ant-table-tbody">
                        ${_.map(datas, (d) => {
      const { key, v } = d;
      return `<tr data-row-key="${key}" class="ant-table-row ant-table-row-level-0">
                            <td title="${key}" class="ant-table-cell ant-table-cell-ellipsis">
                            ${key}
                            </td>
                            <td title="${v}" class="ant-table-cell ant-table-cell-ellipsis">
                            ${v}
                            </td>
                            </tr>`
    }).join("")
      }
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>`
  }
  constructor(props) {
    super(props);
    this.state = {
      /**@type {Object.<string,any>} */
      obj: this.props.obj,
      open: this.props.open,
      table: this.props.table,
      rows: this.props.rows,
      cellLen: this.props.cellLen,
      pagination: this.props.pagination || false,
    }
    this.ref = React.createRef();
  }

  componentDidUpdate(preProps, preState) {
    const { obj } = this.props;
    let state = {};
    if (preProps.obj !== obj || !_.isEqual(preProps.obj, obj)) {
      _.assign(state, { obj });
    }
    !_.isEmpty(state) && this.setState(state, () => {
      this.refresh();
    });
  }

  refresh = () => {
    const { obj, open, table, rows } = this.state;
    this.ref.current.innerHTML = "";
    if (table || rows) {
      return;
    }
    showObject(obj, this.ref.current, undefined, open);
  }
  componentDidMount() {
    this.refresh();
  }

  TbComp = (props) => {
    const { obj, cellLen, pagination } = this.state;
    let properties = _.keys(obj);
    let columns = _.reduce(properties, (prev, property, index) => {
      let width = property.length * 14 < cellLen * 14 ? property.length * 14 : cellLen * 14;
      prev.push({
        key: property,
        title: property,
        dataIndex: property,
        width: width,
        ellipsis: true,
        render: (text) => {
          return formatProperty(text.toString())
        }
      });
      return prev;
    }, []);
    if (!columns.length) {
      return <div>0 Rows.</div>
    }
    let datas = [_.reduce(obj, (prev, v, k) => {
      prev[k] = (v && v.type) ? v.type : v;
      return prev;
    }, { key: "0" })];
    return <Table size="small" showSorterTooltip={false} columns={columns} dataSource={datas} pagination={undefined !== pagination ? pagination : (datas.length > 5 && {
      size: 'small', pageSizeOptions: _.reduce([5, 10, 20, 50, 100, 200, 500, 1000], (prev, curr) => { if (datas.length >= curr) { prev.push(curr); } return prev; }, []), defaultPageSize: 5
    })} />
  }

  RowsComp = (props) => {
    const { obj, cellLen } = this.state;
    if (!obj || _.isEmpty(obj)) {
      return <div>0 Rows.</div>
    }
    let lenR = {
      key: 0,
      v: 0,
    }
    let datas = _.map(obj, (v, key) => {
      lenR.key = lenR.key < key.length ? key.length : lenR.key;
      let vLen = (stringifyNanOrBlank(v)).toString().length;
      lenR.v = lenR.v < vLen ? vLen : lenR.v;
      return {
        key,
        v: v ? formatProperty((stringifyNanOrBlank(v)).toString()) : v
      }
    })
    let columns = _.reduce(_.keys(datas[0]), (prev, property, index) => {
      let width = lenR[property] * 14 < cellLen * 14 ? lenR[property] * 14 : cellLen * 14;
      prev.push({
        key: property,
        title: property,
        dataIndex: property,
        width: width,
        ellipsis: true,
        render: (text) => {
          return formatProperty((stringifyNanOrBlank(text)).toString())
        }
      });
      return prev;
    }, []);
    return <Table size="small" showSorterTooltip={false} columns={columns} dataSource={datas} showHeader={false} pagination={false}
    />
  }

  render() {
    const { obj, table, rows } = this.state;
    const menu = (
      <Menu onClick={async (info) => {
        switch (info.key) {
          case "copySource":
            copyContent(JSON.stringify(obj, undefined, 2));
            break;
        }
      }}>
        <Menu.Item key="copySource" >copy source</Menu.Item>
      </Menu>
    );
    return <Dropdown overlay={menu} trigger={['contextMenu']}>
      <div className="code-edit-result"  >
        <div className="data">
          <div ref={this.ref}>
          </div>
          {table && <this.TbComp></this.TbComp>}
          {rows && <this.RowsComp></this.RowsComp>}
        </div>
      </div>
    </Dropdown>
  }
}