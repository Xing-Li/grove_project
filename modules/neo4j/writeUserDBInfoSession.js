const _ = require('lodash');
const Logger = require('../logger')
const GLogger = Logger.getLogger(__filename);
const ONE_DAY = 24 * 60 * 60 * 1000;

function writeUserDBInfoSession(req, dbProject) {
	if (!req.session) {
		req.session = {};
	}

	let dbs = req.session.dbs || {};

	if (dbProject && dbProject._id) {

		//remove the space
		_.forEach(dbProject, function (v, k) {
			if (typeof (v) == 'string') {
				dbProject[k] = v.trim();
			}
		})
		const {_id , hostname, boltPort, username,  password, isDemo, isLocal, isShare, user}= dbProject;
		dbs[_id] = {_id , hostname, boltPort, username,  password, isDemo, isLocal, isShare, user };

		//add lastTime for cache 
		dbs[_id].lastTime = new Date().getTime();

		//clear the dbs expire cache in session.
		_.forEach(dbs, (item, itemKey) => {
			if (!item || !item.lastTime || new Date().getTime() - item.lastTime > ONE_DAY) {
				delete dbs[itemKey];
			}
		})

		req.session.dbs = dbs;

	} else {

		GLogger.warn("Miss dbInfo param, and the dbInfo must be contain _id");
	}

}

module.exports = writeUserDBInfoSession;
