const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose')
const crypto = require('crypto')
const async = require("async")
const _ = require('lodash')
const schemaCRUD = require('./../schemaCRUD/schemaCRUD')
const util = require('./../util/util')
const Logger = require('./../logger')

const GLogger = Logger.getLogger(__filename);
const userExtendFields = AppConfig?.userExtendFields || {}

const Schema = mongoose.Schema;

const adminEmail = AppConfig && AppConfig.adminEmail ? (AppConfig.adminEmail).trim().toLowerCase() : null;

const passportOptions = {
  saltlen: 32,
  iterations: 25000,
  keylen: 512,
  encoding: 'hex',
  digestAlgorithm: 'sha1',
  limitAttempts: true,
  lastLoginField: 'last',
  loginTimeField: 'loginTime',
  attemptsField: 'attempts',
  hashField: 'hash',
  saltField: 'salt',
  authCount: 'count',
}

const passportFields = {
  [passportOptions.lastLoginField]: passportOptions.lastLoginField,
  [passportOptions.loginTimeField]: passportOptions.loginTimeField,
  [passportOptions.attemptsField]: passportOptions.attemptsField,
  [passportOptions.hashField]: passportOptions.hashField,
  [passportOptions.saltField]: passportOptions.saltField,
  [passportOptions.authCount]: passportOptions.authCount,
}

const userSchema = {
  email: String,
  username: String,
  lastName: String,
  firstName: String,
  verifyToken: {
    key: String,
    createTime: {
      default: Date.now,
      type: Date,
    },
  },
  token: { //
    key: String,
    createTime: {
      default: Date.now,
      type: Date,
    },
  },
  city: String,
  last: {
    type: Date,
    default: Date.now
  },
  type: {
    type: Number,
    default: 0
  }, //0 normal user  1 admin  10 Api  
  access: {
    type: Number,
    default: 1
  }, // -1 freeze , 0 active , 1 wait email active, 2 please reset password

  planType: {
    type: Number,
    enum: [0, 1, 2],
    //{ name: 'Free', value: 0 },
    //{ name: 'Pro', value: 1 },
  },
  planDays: {
    type: Number,
    default: 0
  },
  createTime: {
    type: Date,
    default: Date.now
  },
  loginTime: {
    type: Date,
    default: Date.now
  },
  apiStatus: {
    type: Number,
    default: 0
  }, // 0 close , 1 open.
  apiAuth: {
    key: String,
    secret: String,
  },
  connectorAPIAuth: {
    type: Number,
    default: 0
  },// 0 close , 1 open.
  customDB: {
    type: Number,
    default: 0
  }, // 0 close , 1 open.
  recentProject: [],
  extend: {},
}

const UserSecretFields = {
  password: 1,
  token: 1,
  emailAuth: 1,
  verifyToken: 1,
  apiAuth: 1,
  [passportOptions.saltField]: 1,
  [passportOptions.hashField]: 1,
}
//Can return the data to the client from the server api
const ShowNormalUserFields = {
  _id: 1,
  username: 1,
  email: 1,
  firstName: 1,
  lastName: 1,
  city: 1,
  type: 1,
  access: 1,
  planType: 1,
  planDays: 1,
  connectorAPIAuth: 1,
  customDB: 1,
  createTime: 1,
  loginTime: 1,
  extend: 1,
  [passportOptions.lastLoginField]: 1,
  [passportOptions.loginTimeField]: 1,
  [passportOptions.attemptsField]: 1,
}

//Only for GraphXR system use the object in memory
const ShowSystemUserFields = {
  ...ShowNormalUserFields,
  ...UserSecretFields
}

if (AppConfig.inviteCode) {
  userSchema.inviteCode = String;
}

if (userExtendFields && Object.keys(userExtendFields).length > 0) {
  Object.keys(userExtendFields).forEach(function (key) {
    if (!userSchema[key] && !passportFields[key]) {
      userSchema.extend[key] = {
        type: userExtendFields[key].type === "Number" ? Number : String,
        default: userExtendFields[key].default || "",
      };
      if (userExtendFields[key].enum) {
        userSchema.extend[key].enum = userExtendFields[key].enum;
      }
    } else {
      GLogger.error(`user model field  extend.${key} is exist, please check your config userExtendFields`);
    }
  })
}

const User = new Schema(userSchema);


User.plugin(passportLocalMongoose, passportOptions);

const pbkdf2 = function (password, salt, cb) {
  if (crypto.pbkdf2.length >= 6) {
    return crypto.pbkdf2(password, salt, passportOptions.iterations, passportOptions.keylen, passportOptions.digestAlgorithm, cb);
  } else {
    return crypto.pbkdf2(password, salt, passportOptions.iterations, passportOptions.keylen, cb);
  }
};

const generateSalt = function (cb) {
  crypto.randomBytes(passportOptions.saltlen, function (err, salt) {
    if (err) {
      return cb(err);
    }
    cb(null, salt.toString(passportOptions.encoding));
  });
};

const generateHash = function (password, salt, cb) {
  pbkdf2(password, salt, function (err, hashRaw) {
    if (err) {
      return cb(err);
    }
    return cb(null, Buffer.from(hashRaw, "binary").toString(passportOptions.encoding));
  });
};

User.plugin(function (schema) {


  schema.statics.setPassword = function (password, user, cb) {
    async.waterfall(
      [
        function (callback) {
          if (!user[passportOptions.saltField]) {
            generateSalt(callback);
          } else {
            callback(null, user[passportOptions.saltField]);
          }
        },
        function (salt, callback) {
          user[passportOptions.saltField] = salt;
          generateHash(password, salt, function (err, hash) {
            if (err) {
              return callback(err);
            }
            user[passportOptions.hashField] = hash;
            callback(null, user);
          });
        },
      ],
      (err, passwordUser) => {
        cb(err, passwordUser);
      }
    );
  };
  //compare passWord
  schema.methods.verifyPassword = function (password, cb) {
    let self = this;

    return pbkdf2(password, self.get(passportOptions.saltField), function (err, hashRaw) {
      if (err) {
        return cb(err);
      }

      //force clean the plaintext password
      self.set('password', "");

      let hash = Buffer.from(hashRaw, 'binary').toString(passportOptions.encoding)
      let userSaveHash = self.get(passportOptions.hashField);
      let lastLoginTime = new Date(self.get(passportOptions.lastLoginField) || 0).getTime();
      let attempts = (self.get(passportOptions.attemptsField) || 0);

      if (attempts >= 10 && (Date.now() - lastLoginTime) < 60000) {
        return cb(null, false, {
          message: "Too many Try, please wait one minute",
          attempts: attempts
        });
      }

      if (hash && hash === userSaveHash) {
        if (passportOptions.limitAttempts) {
          self.set(passportOptions.lastLoginField, self.get(passportOptions.loginTimeField) || Date.now());
          self.set(passportOptions.loginTimeField, Date.now());
          self.set(passportOptions.attemptsField, 0);
          // self.set(options.authCount,(self.get(options.authCount) || 0)+1 );
          self.save();
        }
        return cb(null, self);
      } else {
        if (passportOptions.limitAttempts) {
          self.set(passportOptions.lastLoginField, Date.now());
          self.set(passportOptions.attemptsField, (self.get(passportOptions.attemptsField) || 0) + 1);
          self.save(function () {
            if (self.get(passportOptions.attemptsField) > 10) {
              console.log(self.get("username") + " try login fail > 10");
              return cb(null, false, {
                message: "Too many try, please use \"Forget Password\" to reset your password",
                attempts: self.get(passportOptions.attemptsField)
              });
            } else {
              return cb(null, false, {
                message: "Incorrect password.",
                attempts: self.get(passportOptions.attemptsField),
              });
            }
          });
        } else {
          return cb(null, false, {
            message: "Incorrect password."
          });
        }
      }
    });
  }


  //over write PassportLocalMongoose
  schema.statics.findByUsername = function (username, cb) {
    username = username.trim();
    let criteria = (username.indexOf('@') === -1) ? {
      username: username
    } : {
      email: String(username).toLowerCase()
    };
    let query = this.findOne(criteria);
    query.select(ShowSystemUserFields);
    query.exec(cb);
  };

  //over write PassportLocalMongoose
  schema.statics.deserializeUser = function () {

    return (username, cb) => {
      return this.findByUsername(username, function (err, user) {
        return cb(err, user);
      });
    }
  };

  schema.statics.findByToken = function (token, email, cb) {
    token = token.trim();
    let condition = { "token.key": token };
    //email is optional when the third system , Oauth profile API can return the email info
    if (email) {
      condition.email = email;
    }
    let query = this.findOne(condition);
    query.select(ShowSystemUserFields);
    query.exec(cb);
  };

  schema.statics.register = function (user, password, cb) {
    // Create an instance of this in case user isn't already an instance
    if (!(user instanceof this)) {
      user = new this(user);
    }

    if (adminEmail && adminEmail == user.get("email")) {
      user.set('type', 1);
      user.set('access', 0);
    }

    if (user.get('type') == 1) {
      user.customDB = 1;
    }

    user.email = String(user.email).toLowerCase();

    return this.findOne({
      $or: [{
        username: user.username
      }, {
        email: user.email
      }]
    }, function (err, existingUser) {

      if (err) {
        return cb(err);
      }

      if (existingUser) {
        return cb(null, existingUser);
      }

      return user.setPassword(password, function (err, user) {
        if (err) {
          return cb(err);
        }
        user.save(function (err, newUser) {
          return cb(err, newUser);
        });
      });
    });
  };

  schema.statics.checkUsernameAndEmail = function (userId, username, email, cb) {

    return this.find({
      $or: [{
        username: username
      }, {
        email: String(email).toLowerCase()
      }]
    })
      .select("username email")
      .limit(2)
      .exec(function (err, existingUsers) {

        let errMsg = err;
        let currentUser = null;

        if (!errMsg && existingUsers && existingUsers.length > 0) {

          existingUsers = JSON.parse(JSON.stringify(existingUsers));

          existingUsers.forEach(function (element) {
            if (element._id == userId) {
              currentUser = element;
            }
          });

          if (!currentUser || existingUsers.length > 1) {
            errMsg = new Error("already have username or email");
          }
        }
        return cb(errMsg, currentUser);

      });
  };

  schema.statics.updatePlanType = function (userId, planType, cb, orderId = null, planDays = 0) {

    let self = this;
    async.waterfall([
      //1. find user
      function (callback) {
        self.findOne({ "_id": userId }).exec(function (err, resUser) {
          callback(err || (!resUser ? new Error("Can't found user") : null), resUser);
        })
      },
      //2. update the planDays and planType
      function (resUser, callback) {
        let resPlanDays = resUser.get("planDays") || 0;
        resPlanDays += (planType == 0 ? -1 : 1) * planDays;
        resPlanDays = resPlanDays > 0 ? resPlanDays : 0;
        planType = planType > 1 ? planType : (resPlanDays > 0 ? 1 : 0);
        resUser.set("planDays", resPlanDays);
        resUser.set("planType", planType);
        resUser.save(function (err) {
          callback(err, planType);
        })
      }

    ], function (err, planType) {
      if (cb) {
        cb(err, planType)
      }
    })

  };

  schema.statics.planAutoReduceOneDay = function (cb) {
    let self = this;
    async.waterfall([
      function (callback) {
        self.updateMany(
          { planDays: { "$gt": 0 } },
          { "$inc": { planDays: -1 } },
          { upsert: false },
          callback
        )
      },
      function (res, callback) {
        self.updateMany(
          { planDays: 0, planType: { "$gt": 0 } },
          { "$set": { planType: 0 } },
          { upsert: false },
          callback
        )
      },
    ], cb);

  }

  schema.statics.updateType = function (userIds, type, cb) {

    if (!Array.isArray(userIds) || userIds.length == 0) {
      return cb(new Error("Miss userIds "))
    }
    let condition = {};

    if (adminEmail) {
      condition = {
        "$and": [{
          _id: {
            "$in": userIds
          }
        }, {
          "email": {
            "$ne": adminEmail
          }
        }]
      };
    } else {
      condition = {
        _id: {
          "$in": userIds
        }
      };
    }

    let updateField = {
      $set: {
        type: parseInt(type) || 0
      }
    };
    let option = {
      upsert: false
    };
    return this.updateMany(condition, updateField, option, function (err) {
      if (err) {
        console.error(err);
      }
      if (cb) {
        cb(err);
      }
    });
  };


  //getInfo
  schema.statics.getInfo = function (id, username, cb, optionFields) {
    let self = this;
    let findCondition = {};
    if (id) {
      findCondition["_id"] = id;
    } else if (username) {
      findCondition["username"] = username.replace(/==-==|--/g, "/").replace(/%20|==_==|__/g, " ");
    } else {
      return cb(new Error("please check id or username"), null);
    }

    if (!optionFields) {
      optionFields = {};
    }

    _.forEach(UserSecretFields, function (value, key) {
      optionFields[key] = 0;
    })

    return self.find(findCondition, optionFields)
      .limit(1)
      .select({})
      .exec(function (err, users) {
        let tempUser = null;
        if (users && users.length > 0) {
          tempUser = users[0];
        }
        return cb(err, tempUser);
      });
  }


  schema.statics.getUserByKeyword = function (keyword, exitedEmail = [], cb) {

    keyword = keyword.toString().trim()
    return this.find({
      '$or': [{
        'username': { $regex: `${keyword}` }
      }, {
        'email': { $regex: `${keyword}` }
      }],
      'email': {
        '$nin': exitedEmail
      }
    })
      .limit(2)
      .select("_id email username")
      .exec(function (err, users) {
        return cb(err, users);
      });
  }

  schema.statics.modifyAccess = function (userId, access, cb) {
    let self = this;
    let updateData = {
      $set: {
        access: access
      }
    };

    return self.findOneAndUpdate(
      { "_id": userId },
      updateData,
      { upsert: false },
      cb);
  }

  schema.statics.verifyEmail = function (verifyToken, cb) {

    this.findOne({ "verifyToken.key": verifyToken })
      .select({})
      .exec(function (err, user) {
        if (user && user.get("access") == 1) {
          user.set('access', 0);
          user.save(function (err) {
            cb(err, user.get("email"));
          })
        } else if (user && user.get("access") == -1) {
          cb(new Error(`Sorry this account is inactive, please get in touch with us at ${adminEmail} if you wish to make it active  `))
        } else if (user && user.get("access") == 0) {
          //already Validation
          cb(null, user.get("email"));
        } else {
          cb(new Error(`Validation failed`))
        }
      })
  }

  //forgot password
  schema.statics.forgotPassword = function (email, cb) {
    let token = {
      key: util.randomAlphaNum(32),
      createTime: (new Date()).getTime()
    };
    return this.findOneAndUpdate(
      { "email": String(email).toLowerCase() },
      { "$set": { token } },
      { upsert: false },
      function (err, user) {
        if (!err && user) {
          user.token = token;
        }
        cb(err, user);
      });
  }

  //resetPassword
  schema.statics.resetPassword = function (token, password, email, cb) {
    let self = this;
    let expiredDate = new Date();
    expiredDate.setDate(expiredDate.getDate() - 2);

    let findCondition = {
      $and: [{
        'email': String(email).toLowerCase()
      }, {
        'token.key': token
      }, {
        'token.createTime': {
          $gt: expiredDate.getTime()
        }
      },]
    };

    return self.find(findCondition)
      .limit(1)
      .select({})
      .exec(function (err, users) {
        if (err || !users || users.length == 0) {
          return cb({
            message: "The Token in the URL has expired."
          }, null);
        } else {
          let user = users[0];
          return user.setPassword(password, function (err, user) {
            if (err) {
              console.error(err);
              return cb(err);
            } else {
              user.token.createTime = expiredDate;
              if (user.access == 1 || user.access == 2) { //remove email active status.  always keep the freeze status
                user.access = 0;
              }
              if (adminEmail && user.get("email") == adminEmail) {
                user.set("type", 1);
                user.set("access", 0);
              }
              return user.save(function (err) {
                cb(err, user);
              });
            }
          });
        }
      });
  }

  //changePassword
  schema.statics.changePassword = function (uid, oldPassword, newPassword, cb) {
    let self = this;
    let query = self.findOne({
      "_id": uid
    });
    query.select('_id salt hash username email firstName lastName access userType');
    return query.exec(function (err, user) {
      if (err || !user) {
        return cb({
          message: "The Token in the URL has expired."
        }, null);
      }

      return user.verifyPassword(oldPassword, function (err, user) {
        if (err || !user) {
          return cb({
            message: "Incorrect password."
          }, null);
        }

        return user.setPassword(newPassword, function (err, user) {
          if (err || !user) {
            return cb(err);
          } else {

            if (adminEmail && user.get("email") == adminEmail) {
              user.set("type", 1);
            }
            return user.save(function (err) {
              cb(err, user);
            });
          }
        });
      });
    });
  }

  //save 
  schema.statics.edit = function (id, user, cb) {
    let self = this;

    if (user.type == 1) {
      user.customDB = 1;
    }

    if (!user) {
      return cb(new Error("Your miss parameter"), null);
    }

    if (user.email) {
      user.email = String(user.email).toLowerCase();
    }

    let updateOptions = {
      "$set": user
    };

    return self.findOneAndUpdate(
      { '_id': id },
      updateOptions,
      { upsert: false, returnOriginal: true },
      function (err, oldUser) {
        return cb(err, oldUser ? Object.assign(oldUser.toJSON(), user) : null, oldUser || {});
      });
  }

  schema.statics.getAll = function (condition, cb, isCount) {
    let currentSchema = this;

    let ignoreFields = { ...ShowNormalUserFields }
    if (condition.fields && condition.fields.length > 0) {
      condition.fields.forEach(function (element) {
        ignoreFields[element] = 1;
      }, this);
    }
    return schemaCRUD.queryAll(currentSchema, condition, cb, null, isCount, ignoreFields);
  };

  //copy passWord
  schema.statics.copyPassword = function (sourceUser, targetUser) {
    targetUser.salt = sourceUser.salt;
    targetUser.hash = sourceUser.hash;
    return targetUser;
  }

  schema.statics.modifyStatus = function (access, userIds, cb) {
    let self = this;
    let condition = {
      _id: {
        $in: userIds
      }
    };
    let updateCondition = {
      $set: {
        access: access
      }
    };

    return self.updateMany(condition, updateCondition, {
      upsert: false
    }, cb);
  }



  //generate apiAuth
  schema.statics.apiAuthGenerate = function (id, cb) {
    const self = this;
    const apiAuth = {
      key: util.randomAlphaNum(32),
      secret: util.randomAlphaNum(64)
    };
    return self.findOneAndUpdate({ "_id": id }, { apiAuth: apiAuth }, function (err, user) {
      if (!err && user) {
        user.apiAuth = apiAuth;
      }
      cb(err, user);
    });
  }

  //apiAuth
  schema.statics.apiAuth = function (apiKey, apiSecret, cb) {

    const query = this.findOne({ "apiAuth.key": apiKey, "apiAuth.secret": apiSecret });
    query.select(ShowSystemUserFields);
    return query.exec(function (err, user) {
      return cb(err, user);
    });
  }


  //remove inactive users
  schema.statics.removeInactiveUsers = function (cb) {
    return this.deleteMany({ access: 1 }, function (err, user) {
      return cb(err, user);
    });
  }


  schema.statics.updateStruct = function (cb) {

    let condition = {};

    //1. for admin config, always keep for restart app
    if (adminEmail) {
      condition["$or"] = [{
        "type": {
          "$exists": false
        }
      }, {
        "email": adminEmail
      }];
    } else {
      condition["type"] = {
        "$exists": false
      };
    }

    this.find(condition)
      .select({})
      .exec(function (err, users) {
        //update fields
        if (users && users.length > 0) {

          for (let tempIndex in users) {
            let tempUser = users[tempIndex];
            if (adminEmail && adminEmail == tempUser.email) {
              tempUser.type = 1;
              tempUser.access = 0;
            } else {
              tempUser.type = 0;
            }
            tempUser.save(function (err, updateRes) {
              if (err) {
                console.log(new Date() + ">>> update user struct fail ", err, updateRes);
              }
            });
          }
        }
      });

    this.find({
      '$or': [{
        'customDB': {
          "$exists": false
        }
      }, {
        'type': 1
      }]
    }).select({})
      .exec(function (err, users) {
        //update fields
        if (users && users.length > 0) {

          for (let tempIndex in users) {
            let tempUser = users[tempIndex];
            if (tempUser.type == 1 || (adminEmail && adminEmail == tempUser.email)) {
              tempUser.customDB = 1;
            } else {
              tempUser.customDB = 0;
            }

            tempUser.save(function (err, updateRes) {
              if (err) {
                console.log(new Date() + ">>> update user struct fail ", err, updateRes);
              }
            });
          }
        }
      });


    //update planType struct
    this.find({
      "$or": [
        {
          'planType': {
            "$exists": false
          }
        },
        {
          'planDays': {
            "$exists": false
          }
        }
      ]

    })
      .select({})
      .exec(function (err, users) {
        //update fields
        if (users && users.length > 0) {

          for (let tempIndex in users) {
            let tempUser = users[tempIndex];
            tempUser.set('planType', 0);
            tempUser.set('planDays', 0);
            tempUser.save(function (err, updateRes) {
              if (err) {
                console.log(new Date() + ">>> update user struct fail ", err, updateRes);
              }
            });
          }
        }
      });

    //force email to toLowerCase
    this.find({ email: /[A-Z]/ })
      .select({})
      .exec(function (err, users) {
        //update fields
        if (users && users.length > 0) {

          for (let tempIndex in users) {
            let tempUser = users[tempIndex];
            tempUser.set('email', String(tempUser.get("email")).toLowerCase());
            tempUser.save(function (err, updateRes) {
              if (err) {
                console.log(new Date() + ">>> update user struct fail ", err, updateRes);
              }
            });
          }
        }
      });


    return cb(null);
  };

  // update recent project
  schema.statics.updateRecentProject = function (id, recentProject, cb) {
    let self = this;
    return self.findOneAndUpdate(
      { "_id": id },
      { "$set": { recentProject: recentProject } },
      { upsert: false },
      function (err) {
        cb(err);
      });
  }


})


module.exports = mongoose.model('users', User);

let DefaultPlanPriceStruct = {
  0: {
    name: "Free Account",
    unit: "Free",
    price: 0,
  },
  1: {
    name: "Pro Account",
    unit: "Month",
    price: 120,//$
  },
  2: {
    name: "Pro Account",
    unit: "Year",
    price: 1320,//$
  },
}

module.exports.planPrice = Object.assign(DefaultPlanPriceStruct, AppConfig.planPrice);