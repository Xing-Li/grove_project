import { groveComponent } from "./groveComponent";
import { Tr } from "./Tr";

export const Table = groveComponent({ name: 'table', children: [Tr], className: 'table' })