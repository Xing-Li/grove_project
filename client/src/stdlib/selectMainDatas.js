import observe from "@observablehq/stdlib/src/generators/observe.js";
import { getMainColumnsData } from "../block/antchart/antchartUtils";
import actions from "../define/Actions";

export default function () {
  return observe(function (change) {
    const refreshGraphElement = document.createElement("div");
    change(getMainColumnsData);
    actions.inspector(refreshGraphElement, [actions.types.REFRESH_GRAPH], (graphxrApi) => {
      change(getMainColumnsData);
    });
    return function () {
      actions.deleteCache(refreshGraphElement);
    };
  });
}
