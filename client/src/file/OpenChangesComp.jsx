import React from 'react';

export default class OpenChangesComp extends React.Component {

    constructor(props) {
        super(props);
        this.ref = React.createRef();
    }

    componentDidMount() {

    }

    render() {
        const react_component = this.props.react_component;
        const { diffStrings } = react_component.state;
        let configuration = {
            drawFileList: false,
            fileListToggle: false,
            fileListStartVisible: false,
            fileContentToggle: false,
            matching: 'lines',
            outputFormat: 'line-by-line',
            synchronisedScroll: true,
            highlight: true,
            renderNothingWhenEmpty: false,
            // diffStyle: 'char',
            colorScheme: 'dark'
        };
        return _.map(diffStrings, (diffString, index) => {
            const { type, oldLine, line, text } = diffString;
            return <div key={index}> {_.map(typeof text === 'string' ? { single: text } : text, (df, id) => {
                let oldKey = id === 'single' ? oldLine : `chart-${id}`;
                let newKey = id === 'single' ? line : `chart-${id}`;
                return <div key={id} dangerouslySetInnerHTML={{
                    __html: Diff2Html.html(`diff --git old:${oldKey} block:${newKey}
index 0000001..0ddf2ba
--- old:${oldKey}
+++ block:${newKey}
${decodeURIComponent(df)}
`, configuration)
                }}
                    onClick={() => {
                        if ("NULL" !== line && !window.currentEditor) {
                            return;
                        }
                        let lineNum = parseInt((typeof line === 'number') ? line : line.split(",")[0])
                        let api = window.currentEditor.get();
                        let block = api.blocks.getBlockByIndex(lineNum);
                        if (block) {
                            if (type === 'antChart' && id !== "layouts") {
                                let element = document.querySelector(`.chart-${id}`);
                                element && element.scrollIntoView({ behavior: "auto", block: "center", inline: "nearest" });
                            } else {
                                let element = block.holder;
                                element && element.scrollIntoView({ behavior: "auto", block: "center", inline: "nearest" });
                            }
                            api.caret.setToBlock(lineNum, "start", 0);
                        }
                    }}
                />
            })}</div>
        })
    }
}