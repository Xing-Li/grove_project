import { Button, Select } from 'antd';
import Modal from 'antd/lib/modal/Modal';
import axios from 'axios';
import _ from 'lodash';
import PropTypes from "prop-types";
import React from 'react';
import { Title, WIDTH, Z_INDEX } from './fileUtils.js';
import MdFiles from './MdFiles';
const { showToast, ModalType } = require("../util/utils");
const { DefaultShareData, DefaultFileData } = require("../util/localstorage")
const { PublishOptions, ShareProjectCanOptions, Msg_CanOptions } = require("../util/MsgType");

const { Option } = Select;
export default class Publish extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        fileData: PropTypes.object,
        react_component: PropTypes.object,
        width: PropTypes.number,
    };
    static defaultProps = {};

    constructor(props) {
        super(props);
        const { fileData } = this.props;
        this.state = {
            /**@type{DefaultFileData} */
            fileData: fileData,
            status: ShareProjectCanOptions["View"]
        }
    }

    componentDidMount() {
    }

    componentDidUpdate(prevProps, prevState) {
    }

    /**
     * 
     * @param {DefaultShareData} item
     * @param {*} value 
     */
    handleItemChange = async (value) => {
        const { fileData: { shareData }, fileData } = this.state;
        let res = await axios.post(`/api/grove/setShareProjectStatus`, {
            _id: shareData._id,
            status: value
        });
        if (!res.data.status) {
            shareData.status = res.data.content.status;
            this.setState({ fileData })
        }
        res.data.message && showToast(res.data.message, res.data.status ? "error" : "success");
    }

    render() {
        const { fileData: { shareData }, fileData, status } = this.state;
        /** @type {MdFiles} */
        const react_component = this.props.react_component;
        const { modalType, sourceFile } = react_component.state;
        const { width } = this.props;
        const closeFunc = () => {
            react_component.setState({
                modalType: ModalType.None,
            });
        };
        const buttons = [
            <Button key="back" onClick={closeFunc}>
                Cancel
            </Button>
        ];
        if (shareData) {
            fileData.versionMtimeMs !== fileData.mtimeMs && !react_component.isNeedUpload(fileData) &&
                buttons.push(<Button key="Republish" onClick={() => {
                    react_component.setWrapperLoading(true, async () => {
                        await react_component.republish(fileData);
                        react_component.setWrapperLoading(false);
                    })
                }}>Republish</Button>);
            buttons.push(<Button key="Unpublish" onClick={() => {
                react_component.setWrapperLoading(true, async () => {
                    await react_component.unpublish(fileData);
                    react_component.setWrapperLoading(false);
                })
            }}>Unpublish</Button>);
        } else {
            buttons.push(<Button key="Publish" onClick={() => {
                react_component.setWrapperLoading(true, async () => {
                    await react_component.publish(fileData, undefined, status);
                    react_component.setWrapperLoading(false);
                })
            }}>Publish</Button>)
        }
        return <Modal zIndex={Z_INDEX}
            className={"modal-comp data-html2canvas-ignore"}
            width={width || WIDTH}
            title={<Title modalType={modalType} sourceFile={sourceFile}></Title>}
            visible={modalType !== ModalType.None}
            onCancel={closeFunc}
            footer={buttons}
        >
            <div>
                {shareData ? <div className="d-flex justify-content-between">
                    <div> Permission level:&nbsp;&nbsp;<Select defaultValue={shareData.status} onChange={(value) => {
                        if (!Msg_CanOptions[value]) {
                            showToast("error")
                            return;
                        }
                        react_component.setWrapperLoading(true, async () => {
                            await this.handleItemChange(value)
                            react_component.setWrapperLoading(false);
                        })
                    }}>
                        {
                            _.map(PublishOptions, (value, key) => {
                                return <Option key={value} value={value}>{key}</Option>
                            })
                        }
                    </Select></div>
                </div> : <div className="d-flex justify-content-between">
                        <div>Permission Level:&nbsp;&nbsp;<Select style={{ width: 120 }} defaultValue={status} onChange={(value) => {
                            if (!Msg_CanOptions[value]) {
                                showToast("error")
                                return;
                            }
                            this.setState({ status: value })
                        }}>
                            {
                                _.map(PublishOptions, (value, key) => {
                                    return <Option key={value} value={value}>{key}</Option>
                                })
                            }
                        </Select></div>
                    </div>}
            </div>
        </Modal>
    }
}
