import React, { useEffect } from 'react';
import { NoDrawableDataErrorMessage } from './CodeViewerComponent';
import { convertRecordObjectToString, recordToNative } from '../util/ChartUtils';
import DrawPlot from '../draw/DrawPlot'
import { getChartConfig } from '../comp/CheckChartTypes';
import { extractDatas, extractOptions, Fields } from '../comp/OptionsTypes';
import { extractColumnDatas } from '../comp/TableComp';
import { autoObjectsType } from 'util/helpers';

/**
 * Embeds a PieChart (from Nivo) into NeoDash.
 */
const NeoPieChart = (props) => {
  const { records } = props;
  const { selection } = props;

  if (!selection || props.records == null || props.records.length == 0 || props.records[0].keys == null) {
    return <NoDrawableDataErrorMessage />;
  }

  const buildFromRecords = (records) => {
    let keys = {};
    let dataRaw = records
      .reduce((dataR, row) => {
        try {
          if (!selection || !selection.index || !selection.value) {
            return dataR;
          }

          const index = convertRecordObjectToString(row.get(selection.index));
          const idx = dataR.findIndex((item) => item.index === index);
          const key = selection.key !== '(none)' ? recordToNative(row.get(selection.key)) : selection.value;
          const value = recordToNative(row.get(selection.value));

          if (isNaN(value)) {
            return dataR;
          }
          keys[key] = true;

          if (idx > -1) {
            data[idx][key] = value;
          } else {
            data.push({ id: index, label: index, value: value });
          }

          return data;
        } catch (e) {
          // eslint-disable-next-line no-console
          console.error(e);
          return [];
        }
      }, [])
      .map((row) => {
        Object.keys(keys).forEach((key) => {
          // eslint-disable-next-line no-prototype-builtins
          if (!row.hasOwnProperty(key)) {
            row[key] = 0;
          }
        });

        return row;
      });
    return dataRaw;
  };

  const [data, setData] = React.useState([]);

  useEffect(() => {
    setData(buildFromRecords(records));
  }, [selection, records]);

  const settings = props.settings ? props.settings : {};

  if (data.length == 0) {
    return <NoDrawableDataErrorMessage />;
  }

  const { columns, rows } = extractColumnDatas(false, records, true, true);
  let exOptions = _.assign({}, settings,
    {
      data: autoObjectsType(rows, false)
    }
  );
  exOptions.colorField = selection.index
  exOptions.angleField = selection.value
  const darkMode = document.body.classList.contains("darkmode");

  const chart = (
    <DrawPlot type={props.chartType} exOptions={exOptions} darkMode={darkMode}></DrawPlot>
  );

  return chart;
};

export default NeoPieChart;
