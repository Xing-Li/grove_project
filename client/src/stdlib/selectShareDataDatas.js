import observe from "@observablehq/stdlib/src/generators/observe.js";
import { getShareDataColumnsData } from "../block/antchart/antchartUtils";
import actions from "../define/Actions";

export default function () {
  return observe(function (change) {
    const shareDataElement = document.createElement("div");
    change(getShareDataColumnsData);
    actions.inspector(shareDataElement, [actions.types.SHARE_DATA], ({ ShareData, key }) => {
      change(getShareDataColumnsData);
    });
    return function () {
      actions.deleteCache(shareDataElement);
    };
  });
}
