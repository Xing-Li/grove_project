/**
 * Title : toDoc.Js - JavaScript Library to create create a Word document with Text/HTML
 * Author : Abs1337
 * Version : 2.1, 11/4/2019
 * License : MIT
 * @namespace toDoc
 */

/** 
 * @public
 * @returns {object} Library object with global functions
 * @memberof toDoc
 */
var ToDoc = function () {
    this.ADD_CSS = `code {
        font-family: Consolas,"courier new";
        color: crimson;
        background-color: #f1f1f1;
        padding: 2px;
        font-size: 105%;
      }
      .cdx-marker {
        background: rgba(245,235,111,0.29);
        padding: 3px 0;
      }
      .inline-code {
      background: rgba(250, 239, 240, 0.78);
      color: #b44437;
      padding: 3px 4px;
      border-radius: 5px;
      margin: 0 1px;
      font-family: inherit;
      font-size: 0.86em;
      font-weight: 500;
      letter-spacing: 0.3px;
      }

      .cdx-warning {
      position: relative;
      }
      .cdx-warning [contentEditable=true][data-placeholder]::before{
      position: absolute;
      content: attr(data-placeholder);
      color: #707684;
      font-weight: normal;
      opacity: 0;
      }
      .cdx-warning [contentEditable=true][data-placeholder]:empty::before {
      opacity: 1;
      }
      .cdx-warning [contentEditable=true][data-placeholder]:empty:focus::before {
      opacity: 0;
      }
      .cdx-warning::before {
      content: '';
      background-image: url("data:image/svg+xml,%3Csvg width='16' height='17' viewBox='0 0 320 294' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'%3E%3Cpath fill='%237B7E89' d='M160.5 97c12.426 0 22.5 10.074 22.5 22.5v28c0 12.426-10.074 22.5-22.5 22.5S138 159.926 138 147.5v-28c0-12.426 10.074-22.5 22.5-22.5zm0 83c14.636 0 26.5 11.864 26.5 26.5S175.136 233 160.5 233 134 221.136 134 206.5s11.864-26.5 26.5-26.5zm-.02-135c-6.102 0-14.05 8.427-23.842 25.28l-74.73 127.605c-12.713 21.444-17.806 35.025-15.28 40.742 2.527 5.717 8.519 9.175 17.974 10.373h197.255c5.932-1.214 10.051-4.671 12.357-10.373 2.307-5.702-1.812-16.903-12.357-33.603L184.555 70.281C174.608 53.427 166.583 45 160.48 45zm154.61 165.418c2.216 6.027 3.735 11.967 4.393 18.103.963 8.977.067 18.035-3.552 26.98-7.933 19.612-24.283 33.336-45.054 37.586l-4.464.913H61.763l-2.817-.357c-10.267-1.3-19.764-4.163-28.422-9.16-11.051-6.377-19.82-15.823-25.055-27.664-4.432-10.03-5.235-19.952-3.914-29.887.821-6.175 2.486-12.239 4.864-18.58 3.616-9.64 9.159-20.55 16.718-33.309L97.77 47.603c6.469-11.125 12.743-20.061 19.436-27.158 4.62-4.899 9.562-9.07 15.206-12.456C140.712 3.01 150.091 0 160.481 0c10.358 0 19.703 2.99 27.989 7.933 5.625 3.356 10.563 7.492 15.193 12.354 6.735 7.072 13.08 15.997 19.645 27.12l.142.24 76.986 134.194c6.553 10.46 11.425 19.799 14.654 28.577z'/%3E%3C/svg%3E");
      width: 18px;
      height: 18px;
      background-size: 18px 18px;
      position: absolute;
      margin-top: 12px;
      left: -30px;
      }
      @media all and (max-width: 735px) {
      .cdx-warning::before {
          display: none;
      }
      }
      .cdx-warning__message {
      min-height: 85px;
      }
      .cdx-warning__title {
      margin-bottom: 6px;
      }

h1,h2,h3,h4,h5,h6{margin-top:0;margin-bottom:.5rem}
.h1,.h2,.h3,.h4,.h5,.h6,h1,h2,h3,h4,h5,h6{margin-bottom:.5rem;font-weight:500;line-height:1.2}.h1,h1{font-size:2.5rem}.h2,h2{font-size:2rem}.h3,h3{font-size:1.75rem}.h4,h4{font-size:1.5rem}.h5,h5{font-size:1.25rem}.h6,h6{font-size:1rem}

:root{--syntax_normal:#1b1e23;--syntax_comment:#a9b0bc;--syntax_number:#20a5ba;--syntax_keyword:#c30771;--syntax_atom:#10a778;--syntax_string:#008ec4;--syntax_error:#ffbedc;--syntax_unknown_variable:#838383;--syntax_known_variable:#005f87;--syntax_matchbracket:#20bbfc;--syntax_key:#6636b4;--mono_fonts:82%/1.5 Menlo,Consolas,monospace}.observablehq--collapsed,.observablehq--expanded,.observablehq--function,.observablehq--gray,.observablehq--import,.observablehq--string:after,.observablehq--string:before{color:var(--syntax_normal)}.observablehq--collapsed,.observablehq--inspect a{cursor:pointer}.observablehq--field{text-indent:-1em;margin-left:1em}.observablehq--empty{color:var(--syntax_comment)}.observablehq--blue,.observablehq--keyword{color:#3182bd}.observablehq--forbidden,.observablehq--pink{color:#e377c2}.observablehq--orange{color:#e6550d}.observablehq--boolean,.observablehq--null,.observablehq--undefined{color:var(--syntax_atom)}.observablehq--bigint,.observablehq--date,.observablehq--green,.observablehq--number,.observablehq--regexp,.observablehq--symbol{color:var(--syntax_number)}.observablehq--index,.observablehq--key{color:var(--syntax_key)}.observablehq--prototype-key{color:#aaa}.observablehq--empty{font-style:oblique}.observablehq--purple,.observablehq--string{color:var(--syntax_string)}.observablehq--error,.observablehq--red{color:#e7040f}.observablehq--inspect{font:var(--mono_fonts);overflow-x:auto;display:block;white-space:pre}.observablehq--error .observablehq--inspect{word-break:break-all;white-space:pre-wrap}
      `
    /**
     * Store default document settings
     * @private
     * @memberof toDoc
     */
    this.oDocSettings = {
        "pageSizeX": "8.5in",
        "pageSizeY": "11in",
        "marginTop": "1in",
        "marginBottom": "1in",
        "marginLeft": "1in",
        "marginRight": "1in",
        "headerMargin": "0.5in",
        "footerMargin": "0.5in",
    };

    /** 
     * Store Paragraph, Page and Section data
     * @private
     * @memberof toDoc
     */
    this.oData = {
        // Header
        aHeader: [],
        // Footer
        aFooter: [],
        // Page / Paragraph
        aContent: [],

        // HTML tags and markup
        break: "<br/>",
        pageBreak: "<br clear='all' style='page-break-before:always' />",
        paraStart: "<p>",
        paraEnd: "</p>",
        fontStart: "<font size='1'>",
        fontEnd: "</font>",
        pageNum_1: " <span style='mso-field-code: PAGE '></span>",
        pageNum_2: " Page <span style='mso-field-code: PAGE '></span> of <span style='mso-field-code: NUMPAGES '></span>",
    };
}
/** 
 * Creates a Header or a Footer Section in the document
 * @public
 * @param {string} sectionType - Specifies whether to create content for the header or footer of the document <br/> Accepts : "header" &#124; "footer" <br/> Required
 * @param {string} contentType - Defines whether the content is Text or stringified HTML markup <br/> Accepts : "text" &#124; "html" <br/> Required
 * @param {string} content -  The content that will be inserted in the document <br/> Accepts : stringified text &#124; stringified HTML markup <br/> Required
 * @param {number} position - Specifies the content's position in the section  <br/> Header/Footer content will be sorted by positon during document generation <br/> Accepts : Numbers > 0 <br/> Default position : 0 <br/> Optional
 * @param {string} align - Specifies the content's alignemnt <br/> Accepts : "left" &#124; "center" &#124; "right" &#124; "justify" <br/> Default alignment : left <br/> Optional
 * @param {string} size - Specifies the content's font size <br/> Accepts : CSS font-size <br/> Default size : 12pt/16px/1em  <br/> Optional
 * @param {string} font - Specifies the content's font family <br/> Accepts : CSS font-family <br/> Default font : Times New Roman <br/> Optional
 * @memberof toDoc
 */
ToDoc.prototype.createSection = function (sectionType, contentType, content, position, align, size, font) {

    // Store section data and settings as an object
    let sectionObj = {
        "sContentType": "",
        "sContent": "",
        "sPosition": 0,
        "sAlign": "",
        "sSize": "",
        "sFont": "",
        "iAlign": "",
        "iWidth": "",
        "iHeight": "",
    };

    // Validate and set position
    if (!position) {
        sectionObj.sPosition = 0;
    } else if (position && Number.isInteger(position) && position > 0) {
        sectionObj.sPosition = position;
    } else {
        console.error(position + " is an invalid header position, expected number");
        return;
    }

    // Validate and set alignment
    if (!align) {
        sectionObj.sAlign = "left";
    } else if (align && (align === "left" || align === "center" || align === "right" || align === "justify")) {
        sectionObj.sAlign = align;
    } else {
        console.error(align + " is an invalid alignment for image, expected 'left', 'top', 'middle', 'left' or 'right'");
        return;
    }

    // Validate and set font size
    let fontSizeRegex = /(?:[^\d]\.| |^)((?:\d+\.)?\d+) *pt$|px$|em$/; // Regex for number followed by 'pt', 'px' or 'em'
    if (!size) {
        sectionObj.sSize = "16px";
    } else if (size && typeof (size) == "string" && fontSizeRegex.test(size)) {
        sectionObj.sSize = size;
    } else {
        console.error(size + " is an invalid header position, expected number");
        return;
    }

    // Validate and set font family
    if (!font) {
        sectionObj.sFont = "";
    } else if (font && typeof (font) == "string") {
        sectionObj.sFont = font;
    } else {
        console.error(font + " is an invalid header position, expected number");
        return;
    }

    // Crete section content based on type
    switch (contentType) {
        // Create Header Text
        case "text":
            if (content.length > 0 && typeof (content) == "string") {
                sectionObj.sContent = content;
                sectionObj.sContentType = contentType;
            } else {
                console.error(content + "is not an valid value for the parameter 'content', expected string");
                return;
            }
            break;
        // Create Header HTML Markup
        case "html":
            let htmlRegex = /<\/?[a-z][\s\S]*>/i;
            // Check for valid HTML
            if (content.length > 0 && htmlRegex.test(content)) {
                sectionObj.sContent = content;
                sectionObj.sContentType = contentType;
            } else {
                console.error("Invalid HTML markup");
                return;
            }
            break;
        // Error
        default:
            console.error(contentType + " is an invalid content type, expected 'string' or 'html'");
            return;
    }

    // Create section based on type - Header / Footer and push to respestive array
    if (sectionType.length > 0 && typeof (sectionType) == "string" && (sectionType === "header" || sectionType === "footer")) {
        if (sectionType === "header") {
            this.oData.aHeader.push(sectionObj);
        } else if (sectionType === "footer") {
            this.oData.aFooter.push(sectionObj);
        }
    } else {
        console.error(sectionType + " is an invalid section type, expected 'header' or 'footer'");
        return;
    }
};

/** 
 * Stitch sections together during document generation
 * @private
 * @returns {string} Returns a stringified markup of header and footer content
 * @memberof toDoc
 */
ToDoc.prototype.getSection = function (sectionArray) {
    let sections = sectionArray;
    let sectionString = "";
    // Sort by ascending position
    if (sections.length > 1) {
        sections.sort(function (a, b) {
            let posA = a["cPosition"];
            let posB = b["cPosition"];
            return (posA < posB) ? -1 : (posA > posB) ? 1 : 0;
        });
    }
    // Stich section elements together
    if (sections.length > 0) {
        sections.forEach((i) => {
            if (i.sContentType === "text") {
                sectionString = sectionString + "<p align='" + i.sAlign + "' style='font-size : " + i.sSize + "; font-family : " + i.sFont + ", Times New Roman;'>" + i.sContent + this.oData.paraEnd;
            } else if (i.sContentType === "image" || i.sContentType === "html") {
                sectionString = sectionString + i.sContent;
            }
        });
    }
    return sectionString;
};

/** 
 * Creates a Paragraph or Page in the document
 * @public
 * @param {string} type - Defines whether the content is a Paragraph or a Page <br/> Accepts : "paragraph" : Inserts a paragraph in the next line </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "page" : Inserts a paragraph in the next page <br/> <br/> Required
 * @param {string} content - The content that will be created in the document <br/> Accepts : Text &#124; Stringified HTML <br/> Required
 * @param {number} position - Specifies the content's position in the document <br/> Paragraphs and pages will be sorted by positon during document generation <br/> Accepts : Numbers > 0 <br/> Default position : 0 <br/> Required for pagagraphs and pages <br/> Optional if passing a whole HTML document
 * @param {string} align - Specifies the content's alignemnt <br/> Accepts : "left" &#124; "center" &#124; "right" &#124; "justify" <br/> Default alignment : left <br/> Optional
 * @param {string} size - Specifies the content's font size <br/> Accepts : CSS font-size <br/> Default size : 12pt/16px/1em  <br/> Optional
 * @param {string} font - Specifies the content's font family <br/> Accepts : CSS font-family <br/> Default font : Times New Roman <br/> Optional   

 * @memberof toDoc
 */
ToDoc.prototype.createContent = function (type, content, position, align, size, font) {

    let contentObj = {
        "cContent": "",
        "cPosition": 0,
        "cType": "",
        "cAlign": "",
        "cSize": "",
        "cFont": "",
        "iAlign": "",
        "iWidth": "",
        "iHeight": "",
    };

    // Validate and set position
    if (!position) {
        contentObj.sPosition = 0;
    } else if (position && Number.isInteger(position) && position > 0) {
        // Check for duplicate content position
        if (this.oData.aContent.length > 0) {
            this.oData.aContent.every(function (i) {
                if (i.hasOwnProperty("cPosition")) {
                    if (i.cPosition == position) {
                        console.error("Content with position: " + i.cPosition + " has already been defined, enter new position for " + content);
                        return false;
                    } else {
                        contentObj.sPosition = position;
                        return true;
                    }
                }
            });
        }
    } else {
        console.error(position + " is an invalid header position, expected number");
        return;
    }

    // Validate and set alignment
    if (!align) {
        contentObj.cAlign = "left";
    } else if (align && (align === "left" || align === "center" || align === "right" || align === "justify")) {
        contentObj.cAlign = align;
    } else {
        console.error(align + " is an invalid alignment for image, expected 'left', 'top', 'middle', 'left' or 'right'");
        return;
    }

    // Validate and set font size
    let fontSizeRegex = /(?:[^\d]\.| |^)((?:\d+\.)?\d+) *pt$|px$|em$/; // Regex for number followed by 'pt', 'px' or 'em'
    if (!size) {
        contentObj.cSize = "16px";
    } else if (size && typeof (size) == "string" && fontSizeRegex.test(size)) {
        contentObj.cSize = size;
    } else {
        console.error(size + " is an invalid header position, expected number");
        return;
    }

    // Validate and set font face
    if (!font) {
        contentObj.cFont = "";
    } else if (font && typeof (font) == "string") {
        contentObj.cFont = font;
    } else {
        console.error(font + " is an invalid header position, expected number");
        return;
    }

    switch (type) {

        // Create a Paragraph with text or HTML markup
        case "paragraph":
            if (typeof (content) == "string" && content.length > 0) {
                contentObj.cContent = content;
                // Check if content is Text or HTML markup
                let htmlRegex = /<\/?[a-z][\s\S]*>/i;
                if (htmlRegex.test(content)) {
                    contentObj.cType = "html";
                } else {
                    contentObj.cType = type;
                }
            } else {
                console.error(content + " is not an valid value for the parameter 'content', expected string");
                return;
            }
            break;

        // Create a page with HTML markup
        case "page":
            if (typeof (content) == "string" && content.length > 0) {
                contentObj.cContent = content;
                // Check for HTML markup
                let htmlRegex = /<\/?[a-z][\s\S]*>/i;
                if (htmlRegex.test(content)) {
                    contentObj.cType = "html_page";
                } else {
                    contentObj.cType = type;
                }
            } else {
                console.error(content + "is not an valid value for the parameter 'content', expected string");
                return;
            }
            break;

        // Error
        default:
            console.error(type + " is an invalid content type, expected 'image', 'string' or 'html'");
            return;
    }

    // Push to array with other contents
    this.oData.aContent.push(contentObj);
};

/** 
 * Stitch Page during document generation
 * @private
 * @returns {string} Returns a stringified markup of all pages
 * @memberof toDoc
 */
ToDoc.prototype.getContents = function () {
    let content = this.oData.aContent;

    // Sort by ascending position
    if (content.length > 1) {
        content.sort(function (a, b) {
            let posA = a["cPosition"];
            let posB = b["cPosition"];
            return (posA < posB) ? -1 : (posA > posB) ? 1 : 0;
        });
    }

    // Stitch content together
    if (content.length > 0) {
        let contentString = "";
        content.forEach((i, index) => {

            // Stitch markup based on content type
            if (i.cType === "paragraph") {

                contentString = contentString + "<p align='" + i.cAlign + "' style='font-size : " + i.cSize + "; font-family : " + i.cFont + ", Times New Roman;'>" + i.cContent + this.oData.paraEnd;

            } else if (i.cType === "page") {

                contentString = this.oData.pageBreak + contentString + i.cContent;

            } else if (i.cType === "image" || i.cType === "html") {

                contentString = contentString + i.cContent;

            } else if (i.cType === "html_page") {

                contentString = this.oData.pageBreak + contentString + i.cContent;

            }

            // Create content in next line
            if (i.contentNextLine) {
                contentString = contentString + this.oData.break;
            }

        });
        return contentString;
    } else {
        return "";
    }
};

/** 
 * Creates an image in the Header, Footer or Body  of the document
 * @public
 * @param {string} sectionType - Defines the section where the image will be created <br/> Accepts : "header" &#124; "footer" &#124; "body" <br/> Required
 * @param {string} imageURL - Specifies the image's URL <br/> Accepts : image URL &#124; base64 data URL <br/> Required
 * @param {number} position - Specifies the image's position in the document <br/> Images will be sorted by position along with Header/Footer/Paragraphs/Pages during document generation <br/> Accepts : Numbers > 0 <br/> Default position : 0 <br/> Optional
 * @param {string} align - Defines the image's alignemnt <br/> Accepts : "left" &#124; "center" &#124; "right" <br/> Default alignment : left <br/> Optional
 * @param {number} imageWidth - Specify a custom width for images <br/> Use only for resizing images <br/> Accepts : Numbers > 0 <br/> Optional
 * @param {number} imageHeight - Specify a custom height for images <br/> Use only for resizing images <br/> Accepts : Numbers > 0 <br/> Optional
 * @memberof toDoc
 */
ToDoc.prototype.createImage = function (sectionType, imageURL, position, align, imageWidth, imageHeight) {

    // Store section data and settings
    let sectionObj = {
        "sContentType": "",
        "sContent": "",
        "sPosition": 0,
        "sAlign": "",
        "sSize": "",
        "sFont": "",
        "iAlign": "",
        "iWidth": "",
        "iHeight": "",
    };

    let contentObj = {
        "cContent": "",
        "cPosition": 0,
        "cType": "",
        "cAlign": "",
        "cSize": "",
        "cFont": "",
        "iAlign": "",
        "iWidth": "",
        "iHeight": "",
    };

    let imageObj = {};

    // Create image for header/footer or body
    if (sectionType.length > 0 && typeof (sectionType) == "string" && (sectionType === "header" || sectionType === "footer" || sectionType === "body")) {
        if (sectionType === "header" || sectionType === "footer") {
            imageObj = sectionObj;
        } else if (sectionType === "body") {
            imageObj = contentObj;
        }
    } else {
        console.error(sectionType + " is an invalid section type, expected 'header' or 'footer'");
        return;
    }

    // Validate and set position
    if (!position) {
        imageObj.sPosition = 0;
    } else if (position && Number.isInteger(position) && position > 0) {
        imageObj.sPosition = position;
    } else {
        console.error(position + " is an invalid header position, expected number");
        return;
    }

    // Validate and set alignment
    if (!align) {
        imageObj.sAlign = "left";
        imageObj.iAlign = "left";
    } else if (align && (align === "left" || align === "center" || align === "right" || align === "justify")) {
        imageObj.sAlign = align;
        imageObj.iAlign = align;
    } else {
        console.error(align + " is an invalid alignment for image, expected 'left', 'top', 'middle', 'left' or 'right'");
        return;
    }

    // Validate and set image width
    if (!imageWidth) {
        imageObj.iWidth = "";
    } else if (imageWidth && Number.isInteger(imageWidth) && imageWidth > 0) {
        imageObj.iWidth = imageWidth;
    } else {
        console.error(imageWidth + " is an invalid header position, expected number");
        return;
    }

    // Validate and set image height
    if (!imageHeight) {
        imageObj.iHeight = "";
    } else if (imageHeight && Number.isInteger(imageHeight) && imageHeight > 0) {
        imageObj.iHeight = imageHeight;
    } else {
        console.error(imageHeight + " is an invalid header position, expected number");
        return;
    }

    // Validate URL and create image
    let urlRegex = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
    // Check for Data URL
    if (imageURL.indexOf("data") == 0) {
        let imgHTML = "<div align='" + imageObj.imgAlign + "'><img width='" + imageObj.imgWidth + "' height='" + imageObj.imgHeight + "' src='" + imageURL + "' crossOrigin='anonymous'></img>"
        imageObj.sContent = imgHTML;
        imageObj.cType = "image";
        imageObj.sContentType = "image";
    } else if (urlRegex.test(imageURL)) { // Check for URL
        let imgUrl = imageURL,
            imgAlign = imageObj.iAlign,
            imgWidth = imageObj.iHeight,
            imgHeight = imageObj.iWidth;
        // Get image as DATA URL
        getImage(imgUrl, imgAlign, imgWidth, imgHeight, function (imgMarkup) {
            imageObj.sContent = imgMarkup;
            imageObj.cType = "image";
            imageObj.sContentType = "image";
        });
    } else {
        console.error(imageURL + "is not an valid value for the parameter 'imageURL', expected image url/path");
        return;
    }

    // Create section based on type - Header / Footer / Body and push to respestive array
    if (sectionType === "header") {
        this.oData.aHeader.push(imageObj);
    } else if (sectionType === "footer") {
        this.oData.aFooter.push(imageObj);
    } else if (sectionType === "body") {
        this.oData.aContent.push(imageObj);
    }
};

/** 
 * Creates and stores an image as a base64 data URL from image's URL
 * @private
 * @param {string} url - URL or path of the image
 * @param {string} imgAlign - Specifies the image's alignment
 * @param {string} imgWidth - Defines the image's width
 * @param {string} imgHeight - Defines the image's height
 * @param {function} callBack - Callback function return data URL after image loads
 * @returns {string} Returns a stringified HTML markup of the image
 * @memberof toDoc
 */
ToDoc.prototype.getImage = function (url, imgAlign, imgWidth, imgHeight, callBack) {
    let image = new Image();
    image.crossOrigin = "anonymous"

    // Execute after image loads
    image.onload = function () {

        let nHeight = image.height;
        let nWidth = image.width;

        // Set image to canvas to get data URL
        if (image != null && (nWidth && nHeight)) {

            let imgCanvas = document.createElement("canvas"),
                imgContext = imgCanvas.getContext("2d");

            // Make sure canvas is as big as the picture
            imgCanvas.width = image.width;
            imgCanvas.height = image.height;

            // Draw image into canvas element
            imgContext.drawImage(image, 0, 0, image.width, image.height);

            // Get canvas contents as a data URL
            let imgAsDataURL = imgCanvas.toDataURL("image/png");

            let imgHTML = "<div align='" + imgAlign + "'><img width='" + imgWidth + "' height='" + imgHeight + "' src='" + imgAsDataURL + "' crossOrigin='anonymous'></img>"

            // Return data URL in HTML tags
            if (callBack) {
                callBack(imgHTML);
            } else {
                return imgHTML;
            }
        } else {
            console.error("cannot reach image url");
            return "";
        }
    }

    // Load image
    image.src = url;

    // Try Handling CORS error.
    /*let request = new XMLHttpRequest();
    request.open("GET", url);
    request.onload = request.onerror = function() {
        for (let responseText = request.responseText, responseTextLen = responseText.length, binary = "", i = 0; i < responseTextLen; ++i) {
          binary += String.fromCharCode(responseText.charCodeAt(i) & 255)
        }
        let src = 'data:image/jpeg;base64,'+ window.btoa(binary);

        let imgHTML = "<div align='" + imgAlign + "'><img width='" + imgWidth + "' height='" + imgHeight + "' src='" + src + "' crossOrigin='anonymous'></img>"

        // Return data URL in HTML tags
        if (callBack) {
            callBack(imgHTML);
        } else {
            return imgHTML;
        }
    };
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    request.send("");*/
};

/** 
 * Inserts a page number in specified section of the document
 * @public
 * @param {string} sectionType - Defines the section where the page number is inserted in the document <br/> Accepts : "header" &#124; "footer" <br/> Required
 * @param {number} format - Specifies the page number's format <br/> Accepts : 1 : Only page number </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2 : Page X of Y <br/> Default value : 1 <br/> Optional
 * @param {string} align - Specifies the page number's alignemnt <br/> Accepts : "left" &#124; "center" &#124; "right" <br/> Default alignment : left <br/> Optional
 * @param {string} size - Specifies the page number's font size <br/> Accepts : CSS font-size <br/> Default size : 12pt/16px/1em  <br/> Optional
 * @param {string} font - Specifies the page number's font family <br/> Accepts : CSS font-family <br/> Default font : Times New Roman <br/> Optional            
 * @memberof toDoc
 */
ToDoc.prototype.createPagenumber = function (sectionType, format, align, size, font) {

    // Store section data and settings
    let sectionObj = {
        "sContentType": "",
        "sContent": "",
        "sPosition": 0,
        "sAlign": "",
        "sSize": "",
        "sFont": "",
        "iAlign": "",
        "iWidth": "",
        "iHeight": "",
    };

    // Validate and set alignment
    if (!align) {
        sectionObj.sAlign = "left";
    } else if (align && (align === "left" || align === "center" || align === "right")) {
        sectionObj.sAlign = align;
    } else {
        console.error(align + " is an invalid alignment for image, expected 'left', 'center' or 'right'");
        return;
    }

    // Validate text's font size
    let fontSizeRegex = /(?:[^\d]\.| |^)((?:\d+\.)?\d+) *pt$|px$|em$/; // Regex for number followed by 'pt', 'px' or 'em'
    if (!size) {
        sectionObj.sSize = "16px";
    } else if (size && typeof (size) == "string" && fontSizeRegex.test(size)) {
        sectionObj.sSize = size;
    } else {
        console.error(size + " is an invalid header position, expected number");
        return;
    }

    // Validate text's font face
    if (!font) {
        sectionObj.sFont = "";
    } else if (font && typeof (font) == "string") {
        sectionObj.sFont = font;
    } else {
        console.error(font + " is an invalid font style");
        return;
    }

    // Format page number
    if (!format || format === 1) {
        sectionObj.sContent = "<p align='" + sectionObj.sAlign + "' style='font-size : '" + sectionObj.sSize + "; font-family : " + sectionObj.sFont + ", Times New Roman;'>" + this.oData.pageNum_1 + this.oData.paraEnd;
    } else if (format === 2) {
        sectionObj.sContent = "<p align='" + sectionObj.sAlign + "' style='font-size : '" + sectionObj.sSize + "; font-family : " + sectionObj.sFont + ", Times New Roman;'>" + this.oData.pageNum_2 + this.oData.paraEnd;
    } else {
        console.error(format + " is an invalid format for page numbers, expected number 1/2");
        return;
    }

    // Insert page number in specified section and push to respestive array
    if (sectionType.length > 0 && typeof (sectionType) == "string" && (sectionType === "header" || sectionType === "footer")) {
        if (sectionType === "header") {
            this.oData.aHeader.push(sectionObj);
        } else if (sectionType === "footer") {
            this.oData.aFooter.push(sectionObj);
        }
    } else {
        console.error(sectionType + " is an invalid section type, expected 'header' or 'footer'");
        return;
    }

};

/** 
 * Generates and saves a Word document
 * @public
 * @param {string} fileName - Specifies the name of the document <br/> Required
 * @param {object} params - Define custom parameters for the document's layout <br/> Accepts : JSON Object <br/> Optional
 * @memberof toDoc
 */
ToDoc.prototype.createDocument = function (fileName, params) {
    let docParams = {};

    // Validate object parameters
    if (params && !params instanceof Array) {

        Object.keys(params).forEach(function (i) {
            // Check for number followed by 'cm' or 'in'
            let unitRegex = /(?:[^\d]\.| |^)((?:\d+\.)?\d+) *cm$|in$/;
            if (!unitRegex.test(params[i])) {
                console.error(params[i] + " in an invalid value for parameter: " + i);
                return;
            }
        });
        docParams = params;
    } else {
        docParams = this.oDocSettings;
    }

    // HTML markup for Word Document
    let documentMarkup = "<html xmlns:o='urn:schemas-microsoft-com:office:office'" +
        "xmlns:w='urn:schemas-microsoft-com:office:word'" +
        "xmlns='http://www.w3.org/TR/REC-html40'>" +
        "<head>" +
        "<style>" +
        "p.MsoHeader, li.MsoHeader, div.MsoHeader{" +
        "margin:0in;" +
        "margin-top:.0001pt;" +
        "mso-pagination:widow-orphan;" +
        "tab-stops:center 3.0in right 6.0in;" +
        "}" +
        "p.MsoFooter, li.MsoFooter, div.MsoFooter{" +
        "margin:0in;" +
        "margin-bottom:.0001pt;" +
        "mso-pagination:widow-orphan;" +
        "tab-stops:center 3.0in right 6.0in;" +
        "font-size:18.0pt;" +
        "}" +
        "@page Section1{" +
        "size:" + docParams.pageSizeX + " " + docParams.pageSizeY + ";" +
        "margin:" + docParams.marginTop + " " + docParams.marginBottom + " " + docParams.marginLeft + " " + docParams.marginRight + ";" +
        "mso-header-margin:" + docParams.headerMargin + ";" +
        "mso-header:h1;" +
        "mso-footer:f1;" +
        "mso-footer-margin:" + docParams.footerMargin + ";" +
        "mso-paper-source:0;" +
        "}" +
        "div.Section1{" +
        "page:Section1;" +
        "}" +
        //Style to stop Header and Footer repeating
        "table#hrdftrtbl{" +
        "margin:0in 0in 0in 900in;" +
        "}" +
        this.ADD_CSS +
        "</style>" +
        "</head>" +
        "<body>" +
        //Content
        "<div class='Section1'>" + // Section1
        //Pages 
        this.getContents() +
        //Header and Footer
        "<table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>" +
        "<tr>" +
        "<td>" +
        //Header
        "<div style='mso-element:header' id='h1' >" +
        "<p class='MsoHeader'>" +
        "<table border='0' width='100%'>" +
        //"<tr>"+
        // Header Left Align
        //"<td width='30%'>"+
        this.getSection(this.oData.aHeader) +
        //"</td>"+
        // Header Center Align
        //"<td align='center' width='40%'>"+
        //    this.getSection(this.oData.aHeaderCenter) +
        //"</td>"+
        // Header Right Align
        //"<td align='right' width='30%'>"+
        //    this.getSection(this.oData.aHeaderRight) +
        //"</td>"+
        //"</tr>"+
        "</table>" +
        "</p>" +
        "</div>" +
        "</td>" +
        "<td>" +
        //Footer
        "<div style='mso-element:footer' id='f1'>" +
        "<p class='MsoFooter'>" +
        "<table width='100%' border='0' cellspacing='0' cellpadding='0'>" +
        //"<tr>"+
        // Footer Left Align
        //"<td width='30%'>"+
        this.getSection(this.oData.aFooter) +
        //"</td>"+
        // Footer Center Align
        //"<td align='center' width='40%'>"+
        //    this.getSection(this.oData.aFooterCenter) +
        //"</td>"+
        // Footer Right Align
        //"<td align='right' width='30%'>"+
        //    this.getSection(this.oData.aFooterRight) +
        //    "Page <span style='mso-field-code: PAGE '></span> of <span style='mso-field-code: NUMPAGES '></span>"+
        //"</td>"+
        //"</tr>"+
        "</table>" +
        "</p>" +
        "</div>" +
        "</td>" +
        "</tr>" +
        "</table>" +
        "</div>" +
        // End Section 1
        "</body>" +
        "</html>"

    let blob = new Blob(['\ufeff', documentMarkup], {
        type: 'application/msword'
    });

    // Get link URL
    let url = URL.createObjectURL(blob);

    // Set file name
    if (!fileName) {
        fileName = fileName ? fileName + '.doc' : 'document.doc';
    }

    // Create download link element
    let downloadLink = document.createElement("a");
    document.body.appendChild(downloadLink);

    // Check for IE 10/11
    if (navigator.msSaveOrOpenBlob) {
        navigator.msSaveOrOpenBlob(blob, fileName);
    } else {
        // Create a link to the file
        downloadLink.href = url;
        // Set the file name
        downloadLink.download = fileName;
        // Trigger the function
        downloadLink.click();
    }
    document.body.removeChild(downloadLink);

    // Clear previous document's data
    this.clearDocument();
};

/** 
 * Clears all created document data
 * @public
 * @memberof toDoc
 */
ToDoc.prototype.clearDocument = function () {
    // Header
    this.oData.aHeader = [];
    // Footer
    this.oData.aFooter = [];
    // Page / Paragraph
    this.oData.aContent = [];
};

const doc = new ToDoc();
window.doc = doc;
export default doc;