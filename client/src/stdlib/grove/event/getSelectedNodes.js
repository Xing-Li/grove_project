import onSelect from "./onSelect"

export default function getSelectedNodes() {
    return onSelect('grove-selected-nodes', event => window.gxr.nodes(event.nodeIds))
}