import { BackTop } from 'antd';
import React, { Component } from "react";
import editor from './commonEditor';
import Wrapper from "./Wrapper";
import ChartSettings from "./ChartSettings";
import * as ReactRouter from "react-router-dom";

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    return <ReactRouter.BrowserRouter>
      <React.Fragment>
        <Wrapper></Wrapper>
        <ChartSettings></ChartSettings>
      </React.Fragment>
    </ReactRouter.BrowserRouter>;
  }
}