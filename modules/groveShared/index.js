'use strict';

const express = require('express');
const _ = require('lodash');
const auth = require('../util/auth');

const GroveShared = require('./GroveShared.model');


let router = express.Router();

router.all('/addShared', auth.protected, function (req, res) {
    let addInfo = {
        userId: req.user.id,
        toUserId: req.parameters.addInfo.toUserId,
        projectId: req.parameters.addInfo.projectId,
        projectName: req.parameters.addInfo.projectName,
        fileKey: req.parameters.addInfo.fileKey,
        status: req.parameters.addInfo.status
    }
    GroveShared.addShared(addInfo, function (err) {
        if (err) {
            console.error(err);
        }
        return res.send({ 
            status: err ? 1 : 0, 
            message: err ? "tips.failed": "tips.successful",
         });
    });
});

router.all('/getAllShareds', auth.protected, function (req, res) {
    GroveShared.getAllShareds(function (err, users) {
        if (err) {
            console.error(err);
        }
        return res.send({ 
            status: err ? 1 : 0, 
            message: err ? "tips.failed": "tips.successful",
            content: users
        });
    });
});

router.all('/deleteSharedById', auth.protected, function (req, res) {
    let id = req.parameters.id
    GroveShared.deleteSharedById(id, function (err) {
        if (err) {
            console.error(err);
        }
        return res.send({ 
            status: err ? 1 : 0, 
            message: err ? "tips.failed": "tips.successful"
        });
    });
});


module.exports = router;