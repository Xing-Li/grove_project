export let extensionConfig = {};

const defaults = {
  noOnlineRequire: false,
}

export function setExtensionConfig(value) {
  extensionConfig = {...defaults, ...value};
}