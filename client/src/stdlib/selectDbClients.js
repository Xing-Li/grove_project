import observe from "@observablehq/stdlib/src/generators/observe.js";
import { getDbClients } from "../block/antchart/antchartUtils";
import actions from "../define/Actions";

export default function () {
  return observe(function (change) {
    const dbClientsElement = document.createElement("div");
    change(getDbClients);
    actions.inspector(dbClientsElement, [actions.types.DB_CLIENTS], ({ DbClients, key }) => {
      change(getDbClients);
    });
    return function () {
      actions.deleteCache(dbClientsElement);
    };
  });
}
