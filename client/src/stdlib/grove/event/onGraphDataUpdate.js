import * as stdlib from "@observablehq/stdlib";

export default function onGraphDataUpdate() {
  const { Generators } = new stdlib.Library();
  return Generators.observe((emit) => {
    window.gxr.onGraphDataUpdate(() => {
      emit(gxr.graph());
    });

    // Initial state
    emit(gxr.graph());

    return () => {
      console.log("I leak memory!")
    };
  });
}
