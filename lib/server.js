#!/usr/bin/env node
/* eslint-disable no-console */

import http from "http";
let formidable = require('formidable');
import https from "https";
import { readFileSync, existsSync, rename, copyFile, writeFile } from "fs";
import { homedir } from "os";
import { join } from "path";
import { json, run } from "micro";
import { createHmac, timingSafeEqual } from "crypto";
import _ from 'lodash';
const Mongoose = require('mongoose');
const schedule = require('node-schedule');
const async = require('async');
const { networkInterfaces } = require('os');
const { exec } = require('child_process');
const express = require('express');
const cors = require('cors')
const app = express();
import { notFound, unauthorized, exit } from "./errors";
import { RESP_STATUS_OK, RESP_STATUS_FAIL } from './constants';
const User = require('../modules/user/user.model');
const Logger = require('../modules/logger')
const API = require('../modules/index');
const GLogger = Logger.getLogger(__filename);

const sslcertFile = join("./java-database-proxy/src/main/java/resource", "certificate.crt");
const sslkeyFile = join("./java-database-proxy/src/main/java/resource", "privateKey.key");

const nets = networkInterfaces();
const results = Object.create(null); // Or just '{}', an empty object
const ips = ["localhost", "127.0.0.1"];

app.use(cors())
app.use(express.static(__dirname + "/../java-database-proxy/src/main/webapp/"));

let consoleFunc = function (oriLogFunc, depth) {
  return function () {
    if (!(arguments.length > 1 && typeof arguments[0] === 'string')) {
      let now = new Date();
      let $depth = depth || 2;
      let line = (new Error).stack.split("\n")[$depth].replace(/\s+at\s+/, "");
      let xx = line.indexOf("(");
      if (xx != -1) {
        line = line.substring(line.indexOf("("));
      } else {
        line = "(" + line + ")";
      }
      let msg = `${now.getHours()}:${now.getMinutes()}:${now.getSeconds()} ${line} ${_.join(_.map(arguments, (value, index) => { return value instanceof Object ? JSON.stringify(value) : value }), " ")}`;
      oriLogFunc.call(console, msg);
    } else {
      oriLogFunc.call(console, ...arguments);
    }
  }
};

const logFunc = console.log;
console.log = consoleFunc(logFunc);

for (const name of Object.keys(nets)) {
  for (const net of nets[name]) {
    // Skip over non-IPv4 and internal (i.e. 127.0.0.1) addresses
    // 'IPv4' is in Node <= 17, from 18 it's a number 4 or 6
    const familyV4Value = typeof net.family === 'string' ? 'IPv4' : 4
    if (net.family === familyV4Value && !net.internal) {
      if (!results[name]) {
        results[name] = [];
      }
      results[name].push(net.address);
      ips.push(net.address)
    }
  }
}

const createDB = function createDB() {
  // Connect to local db
  Mongoose.Promise = global.Promise;
  Mongoose.set('strictQuery', false);
  //If password contain @, please use 40% replace
  GLogger.log("Connecting to mongodb at " + AppConfig.mongodb);
  return Mongoose.connect(AppConfig.mongodb, {
    // uri_decode_auth: true
    useUnifiedTopology: true,//v5
    useNewUrlParser: true,//v5
    // useFindAndModify: false
  }).then(function () {
    //support cluster init, only init once 
    if (process.isAppMaster) {
      /**
       * Try to create a default admin user if ADMIN_EMAIL and ADMIN_PASSWORD are set
       */
      if (AppConfig.adminEmail && AppConfig.adminPassword) {
        GLogger.log('ADMIN_EMAIL and ADMIN_PASSWORD are set, creating the admin user if it does not exist', AppConfig.adminEmail);
        let user = new User({
          username: AppConfig.adminEmail,
          firstName: 'Admin',
          lastName: 'User',
          city: 'AdminCity',
          email: AppConfig.adminEmail,
          inviteCode: null,
          type: 1,
          access: 1,
        });

        User.register(user, AppConfig.adminPassword, function (err, account) {
          if (err || !account) {
            GLogger.log("Failed to register admin user:", err)
          }
          GLogger.log("Registered admin user, or it already exists!", AppConfig.adminEmail)
        })
      }
    }
    return Mongoose
  }).catch(function (err) {
    throw err;
  });
}
const createWebServer = function createWebServer(mongoose) {
  const development = process.env.NODE_ENV === "development";
  const developmentOrigin = "http://localhost:9000";

  const {
    ssl = "disabled",
    port = 2899,
    sslcert,
    sslkey,
  } = AppConfig;
  let server;
  const useSSL = ssl === "required";
  if (useSSL && (!(sslcert || existsSync(sslcertFile)) || !(sslkey || existsSync(sslkeyFile)))) {
    return exit(
      "SSL required, but no SSL certificate or private key configured"
    );
  }

  if (useSSL) {
    const sslcertData = readFileSync(sslcert || sslcertFile);
    const sslkeyData = readFileSync(sslkey || sslkeyFile);
    server = https.createServer({ cert: sslcertData, key: sslkeyData }, app);
  } else {
    server = http.createServer(app);
  }

  server.listen(port, () => {
    console.log(
      `Grove Server running at \n${_.map(ips, (host) => `http${useSSL ? `s` : ``}://${host}:${port}`).join("\n")}, CORS-enabled`
    );
  });

  function getHandler(req, res) {
    // CORS
    res.setHeader(
      "Access-Control-Allow-Headers",
      "Authorization, Content-Type"
    );
    res.setHeader("Access-Control-Allow-Methods", "GET, POST");
    res.setHeader("Access-Control-Max-Age", "86400");
    if (req.method === "OPTIONS") {
      // Don't have an authorization header to check yet, so be permissive
      res.setHeader("Access-Control-Allow-Origin", req.headers.origin || req.headers.host);
      return;
    }

    res.setHeader("Access-Control-Allow-Origin", req.headers.origin || req.headers.host);

    // Expose type
    if (req.method === "GET") {
      res.send(JSON.stringify({ error: { type: proxyConfig.type } }));
      return;
    }
    return handler;
  }

  app.post("/uploadFile", async function (req, res) {
    // const { fileName, data } = await json(req);
    let form = new formidable.IncomingForm();
    form.parse(req, (err, fields, files) => {
      let fileDir = join(__dirname, './../upload');
      let filePath = join(fileDir, fields.fileName);
      writeFile(filePath, fields.data, 'utf8', function (err) {
        if (err) {
          console.error("/uploadFile :", err);
          res.send({ status: RESP_STATUS_FAIL, error: `File Upload Fail!` });
        } else {
          res.send(JSON.stringify({ status: RESP_STATUS_OK, msg: `File Upload Success` }));
        }
      });
    })
  })
  app.get('/static', express.static(join(__dirname, '../upload')))

  let router = express.Router();
  router.use(API);

  app.use('/', router);
  
  // Open the default browser on Windows
  if (process.platform === 'win32') {
    exec(`start "" "http://${ips[0]}:${port}"`);
  }
}

let gMongoose = null;
export default function (cb) {
  async.waterfall([
    function (callback) {
      if (gMongoose) {
        callback(null, gMongoose);
      } else {
        createDB().then(function (mongoose) {
          gMongoose = mongoose;
          callback(null, gMongoose)
        }).catch(function (err) {
          callback(err);
        })
      }
    },
    function (mongoose, callback) {
      try {
        let httpServer = createWebServer(mongoose);
        callback(null, httpServer);
      } catch (error) {
        callback(error)
      }
    }
  ], function (err, httpServer) {
    cb && cb(err, httpServer);
  })
}
