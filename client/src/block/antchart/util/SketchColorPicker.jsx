import React from 'react'
import reactCSS from 'reactcss'
import { SketchPicker } from 'react-color'
import PropTypes from "prop-types";
import { Tooltip } from 'antd';

export default class SketchColorPicker extends React.Component {
    static propTypes = {
        onChange: PropTypes.func,
        color: PropTypes.string
    };

    static defaultProps = {};

    constructor(props) {
        super(props);
        let { color } = this.props;
        this.state = {
            color: color || "red",
        };
    }

    handleChange = (color) => {
        this.setState({ color: color.hex }, () => {
            const { onChange } = this.props;
            onChange && onChange(color.hex);
        })
    };

    render() {
        const styles = reactCSS({
            'default': {
                color: {
                    width: '36px',
                    height: '14px',
                    borderRadius: '2px',
                    background: `${this.state.color}`,
                },
                swatch: {
                    padding: '5px',
                    background: '#fff',
                    borderRadius: '1px',
                    boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
                    display: 'inline-block',
                    cursor: 'pointer',
                },
                popover: {
                    position: 'absolute',
                    zIndex: '2',
                },
                cover: {
                    position: 'fixed',
                    top: '0px',
                    right: '0px',
                    bottom: '0px',
                    left: '0px',
                },
            },
        });

        return (
            <div>
                <Tooltip trigger={'click'} color={this.state.color} placement="top" title={<SketchPicker color={this.state.color} onChange={this.handleChange} />}>
                    <div style={styles.swatch}>
                        <div style={styles.color} />
                    </div>
                </Tooltip>
            </div>
        )
    }
}
