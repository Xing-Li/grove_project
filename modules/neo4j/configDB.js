const AppNeo4jConfig = AppConfig.neo4j || { auth: {} };
const CryptoUtil = require('../util/cryptoUtil');
const boltPort = process.env["NEO4J_BOLT_PORT"] || AppNeo4jConfig.boltPort || 7687
const hostname = process.env["NEO4J_HOSTNAME"] || AppNeo4jConfig.hostname
module.exports = {
	_id: "default",
	dbname: "default",
	name: "default",
	hostname,
	host: `${hostname}:${boltPort}`,
	httpPort: process.env["NEO4J_HTTP_PORT"] || AppNeo4jConfig.httpPort || 7474,
	boltPort,
	protocol: process.env["PROTOCOL"] || 'bolt',
	username: CryptoUtil.encrypt(process.env["NEO4J_USERNAME"] || AppNeo4jConfig.auth.user || 'neo4j'),
	password: CryptoUtil.encrypt(process.env["NEO4J_PASSWORD"] || AppNeo4jConfig.auth.pass || 'neo4j'),
	isDemo: false,
	isLocal: false,
	isShare: true,
	currentNeo4jDB: "neo4j",
}