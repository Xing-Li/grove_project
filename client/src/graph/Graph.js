
import Node from './Node'
import Edge from './Edge'
export default class Graph {
    constructor() {
        this.nodes = []
        this.edges = []
    }

    cloneGraph(graph) {
        let nodes = graph.nodes
        this.nodes.length = 0;
        nodes.forEach(n => {
            let node = new Node()
            node.cloneFromNode(n)
            this.nodes.push(node)
        });

        let edges = graph.edges
        this.edges.length = 0;
        edges.forEach(e => {
            let edge = new Edge()
            edge.source = this.nodes.find(n => n.id == e.sourceId)
            edge.target = this.nodes.find(n => n.id == e.targetId)
            edge.cloneEdge(e)
            this.edges.push(edge)
        })

    }
}