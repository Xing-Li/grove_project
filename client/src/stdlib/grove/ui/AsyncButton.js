import html from "@observablehq/stdlib/src/html.js";

import { groveComponent } from "./groveComponent";

export const AsyncButton = groveComponent(({ className = "", children, label, onClick, tooltip }) => {
  const setLoading = (button) => {
    const buttonStyle = window.getComputedStyle(button);
    const contentWidth =
      button.clientWidth -
      (parseFloat(buttonStyle.paddingLeft) +
        parseFloat(buttonStyle.paddingRight));
    button.setAttribute("disabled", "");
    button.innerHTML = `<div style="width: ${contentWidth}px"><span class="fa fa-robot fa-spin class="m-auto"></span></div>`;
  }

  const setNotLoading = (button) => {
    button.removeAttribute("disabled");
    button.innerHTML = '';
    const inner = html`<div style="display:flex;align-items:center;gap:3px"></div>`
    children.forEach(child => inner.appendChild(child))
    if (label) {
      inner.appendChild(html`${label}`)
    }
    button.appendChild(inner)
  }

  const button = html`<button class="kv-button ${className}" type="button" title="${tooltip}"></button>`;
  setNotLoading(button)
  button.onclick = async () => {
    try {
      setLoading(button);
      await onClick();
    } catch (e) {
      console.error(e);
    } finally {
      setNotLoading(button);
    }
  };
  return button;
})