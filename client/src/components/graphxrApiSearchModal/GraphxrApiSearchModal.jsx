import React, { useEffect, useState } from "react";
import Fuse from "fuse.js";
import { Modal } from "antd";
import { fetchPackageFile } from "../../stdlib/grove-require";
import GraphxrApiSearchResults from "./GraphxrApiSearchResults";
import * as graphxrApi from "../../stdlib/graphxrApi";

/**
 * Duplicate some state values in a module level store because the keydown handler
 * exists outside the state lifecycle of this component. This works because this component
 * is only rendered in one location. See notes on the keydown handler for more context.
 */
const store = {
  results: [],
  selectedResult: -1,
  version: undefined,
};

/**
 * Store the keydown handler as a module global so we can remove it when the modal is closed.
 */
let onKeyDown;

export function GraphxrApiSearchModal({ onCancel, initialTerm, visible }) {
  const [init, setInit] = useState(false);
  const [term, setTerm] = useState("");
  const [results, setResults] = useState(undefined);
  const [lastSelectedResult, setLastSelectedResult] = useState(-1);
  const [selectedResult, setSelectedResult] = useState(-1);
  const [searchIndex, setSearchIndex] = useState(undefined);
  const [fuse, setFuse] = useState(undefined);
  const [version, setVersion] = useState(undefined);

  const packagePath = version ? `@kineviz/graphxr-api@${version}` : "@kineviz/graphxr-api";
  const searchIndexPath = `${packagePath}/docs/searchIndex.json`;
  const getSearchResult = (index) => (index >= 0 ? document.getElementById(`searchResult-${index}`) : undefined);
  const unhighlightResults = () => {
    for (const element of document.getElementsByClassName("selectedResult")) {
      element.classList.remove("selectedResult");
    }
  };
  const highlightResult = (index) => {
    if (index >= 0) {
      const searchResult = getSearchResult(index);
      searchResult.classList.add("selectedResult");
      if (!isInView(searchResult))
        searchResult.scrollIntoView({
          behavior: "instant",
          block: lastSelectedResult > selectedResult ? "start" : "end",
        });
    }
  };
  const isInView = (searchResult) => {
    const parent = searchResult.parentElement;
    const parentRect = parent.getBoundingClientRect();
    const myRect = searchResult.getBoundingClientRect();
    return myRect.top >= parentRect.top && myRect.bottom <= parentRect.bottom;
  };

  useEffect(() => {
    if (selectedResult >= 0) {
      unhighlightResults();
      highlightResult(selectedResult);
    }
  }, [selectedResult]);

  useEffect(() => {
    if (results) {
      const index = results.length > 0 ? 0 : -1;
      if (index !== selectedResult) {
        setSelectedResult(index);
      } else {
        unhighlightResults();
        highlightResult(selectedResult);
      }
    }
  }, [results]);

  useEffect(() => {
    if (fuse) {
      const results = fuse.search(term);
      setResults(results.map((results) => results.item));
    }
  }, [term]);

  useEffect(() => {
    setFuse(new Fuse(searchIndex, { keys: ["name", "description"] }));
    if (initialTerm) {
      setTerm(initialTerm);
    }
  }, [searchIndex]);

  useEffect(() => {
    fetchPackageFile(searchIndexPath)
      .then((file) => file.json())
      .then((searchIndex) => {
        setSearchIndex(searchIndex);
      });
  }, [version]);

  const close = () => {
    window.removeEventListener("keydown", onKeyDown, { capture: true });
    setInit(false);
    setTerm("");
    setResults([]);
    setLastSelectedResult(-1);
    setSelectedResult(-1);
    setVersion(undefined);
    onCancel();
  };

  /**
   * Update the module global store when this component's state changes.
   */
  useEffect(() => {
    if (results) store.results = results;
    if (selectedResult >= 0) store.selectedResult = selectedResult;
    if (version) store.version = version;
  }, [results, selectedResult, version]);

  useEffect(() => {
    if (!init && visible) {
      graphxrApi.executeOnLoad(() => {
        setVersion(graphxrApi.getGraphxrApiVersion());
      });

      /**
       * This is a workaround to use addEventListener with {capture: true}, so that this modal's
       * keydown handler will be called before Editor.js's keydown handler.
       */
      onKeyDown = (event) => {
        const keys = ["ArrowUp", "ArrowDown", "Enter", "Escape"];
        if (keys.includes(event.key)) {
          event.stopPropagation();
          event.preventDefault();
          if (event.key === "Enter") {
            if (store.results.length > 0) {
              window.open(graphxrApi.getDocUrl(store.results[store.selectedResult].name, version), "_blank");
              close();
            }
          } else if (event.key === "ArrowDown") {
            if (store.results.length > 0) {
              let newSelectedResult = store.selectedResult + 1;
              if (newSelectedResult >= store.results.length) newSelectedResult = 0;
              setLastSelectedResult(store.selectedResult);
              setSelectedResult(newSelectedResult);
            }
          } else if (event.key === "ArrowUp") {
            if (store.results.length > 0) {
              let newSelectedResult = store.selectedResult - 1;
              if (newSelectedResult < 0) newSelectedResult = store.results.length - 1;
              setLastSelectedResult(store.selectedResult);
              setSelectedResult(newSelectedResult);
            }
          } else if (event.key === "Escape") {
            close();
          }
        }
      };
      window.addEventListener("keydown", onKeyDown, { capture: true });

      setInit(true);
    }
  }, [visible]);

  return (
    <Modal
      bodyStyle={{
        padding: "0",
      }}
      destroyOnClose={true}
      keyboard={false}
      title={`Search GraphXR API Docs ${version ? `(v${version})` : "(latest)"}`}
      visible={visible}
      onCancel={close}
      closable={true}
      footer={null}
    >
      <div className="graphxrApiSearchModal">
        <input
          autoComplete="off"
          className="searchInput"
          id="graphxrApiSearchInput"
          type="text"
          placeholder="Enter search term..."
          value={term}
          autoFocus={true}
          onChange={() => {
            setTerm(event.target.value);
          }}
        ></input>
        {!searchIndex && <p className="noResults">The search index is loading or unavailable.</p>}
        {searchIndex && (!results || (results && results.length === 0)) && (
          <p className="noResults">No results found.</p>
        )}
        {searchIndex && results && results.length > 0 && (
          <GraphxrApiSearchResults version={version} results={results} />
        )}
      </div>
    </Modal>
  );
}
