import axios from 'axios';
import isGraphXrDarkMode from './util/isGraphXrDarkMode';
import 'codemirror';
import 'codemirror/addon/comment/comment';
import 'codemirror/addon/display/placeholder.js';
import 'codemirror/addon/fold/foldcode';
import 'codemirror/addon/fold/foldgutter';
import 'codemirror/addon/fold/brace-fold';
import 'codemirror/addon/fold/xml-fold';
import 'codemirror/addon/fold/indent-fold';
import 'codemirror/addon/fold/markdown-fold';
import 'codemirror/addon/fold/comment-fold';
import "codemirror/addon/lint/lint";
import 'codemirror/addon/hint/show-hint';
import 'codemirror/addon/display/fullscreen';
import 'codemirror/addon/selection/active-line';
import 'javascript2/hint/javascript-hint';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/mode/jsx/jsx';
import 'codemirror/mode/htmlmixed/htmlmixed';
import 'codemirror/mode/markdown/markdown';
import 'codemirror/mode/sql/sql';
import 'codemirror/mode/powershell/powershell';
import 'javascript2/javascript2';
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./App.scss";
import actions from "./define/Actions";
import Common from "./util/Common";
import { UNKOWN } from "./util/helpers";
import { showConfirm } from "./util/utils";
import NewArray from 'util/NewArray';
import ProxyArray from 'util/ProxyArray';
import _ from 'lodash';
import { ThemeSwitcherProvider } from "react-css-theme-switcher";

// See GraphXR config.js for information about aliasPath
// e.g. aliasPath: "/graphxr"
const aliasPath = window.opener?.globalVariable?.aliasPath ?? ''
const themes = {
  light: `${process.env.PUBLIC_URL}${aliasPath}/antd.css`,
  dark: `${process.env.PUBLIC_URL}${aliasPath}/antd.dark.css`,
};

const darkModeElement = document.createElement('div');

let consoleFunc = function (oriLogFunc, depth) {
  return function () {
    if (!(arguments.length > 1 && typeof arguments[0] === 'string')) {
      let now = new Date();
      let $depth = depth || 2;
      let line = (new Error).stack.split("\n")[$depth].replace(/\s+at\s+/, "");
      let xx = line.indexOf("(");
      if (xx != -1) {
        line = line.substring(line.indexOf("("));
      } else {
        line = "(" + line + ")";
      }
      let msg = `${now.getHours()}:${now.getMinutes()}:${now.getSeconds()} ${_.join(_.map(arguments, (value, index) => { return value instanceof Object ? JSON.stringify(value) : value }), " ")} ${line}`;
      oriLogFunc.call(console, msg);
    } else {
      oriLogFunc.call(console, ...arguments);
    }
  }
};
window.NewArray = NewArray;
window.ProxyArray = ProxyArray;
window.showConfirm = showConfirm;


const rootElement = document.getElementById("root");

actions.inspector(darkModeElement, [actions.types.DARK_MODE], (darkMode) => {
  if (darkMode) {
    document.body.classList.add("darkmode");
  } else {
    document.body.classList.remove("darkmode");
  }
})
async function startReady() {
  ReactDOM.render(<ThemeSwitcherProvider themeMap={themes} defaultTheme={"light"}>
    <App />
  </ThemeSwitcherProvider>, rootElement);
}
startReady();