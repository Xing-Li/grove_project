'use strict';

const express = require('express');
const async = require('async');
const https = require('https')
const passport = require('passport');
const _ = require('lodash');
const crypto = require('crypto');
const svgCaptcha = require('svg-captcha');
const signature = require('cookie-signature')

const Util = require('./../util/util');

const auth = require('./../util/auth');
const EmailUtil = require('./../util/emailUtil');
const UserLDAP = require('./user.ldap');
const User = require('./user.model');
const Auth = require('./../util/auth');
const util = require('./../util/util');
const Logger = require('./../logger')

const GLogger = Logger.getLogger(__filename);
const GAuditUserFields =  ['_id','email','lastName','firstName','type','access'];

const adminEmail = AppConfig && AppConfig.adminEmail ? (AppConfig.adminEmail).trim().toLowerCase() : null;

let router = express.Router();


let verifyEmail = function (req, res) {
    User.verifyEmail(req.parameters.token, function (err, email) {
        if (!err && email) {
            res.redirect(`/login?email=${email}&success=1&msg=${encodeURI("tips.actionTips.verificationSuccessfully")}`)
        } else {
            GLogger.error(err);
            res.redirect(`/login?msg=${encodeURI('tips.actionTips.verificationFailed')}`)
        }
    });
}
router.all('/verifyEmail/:token', verifyEmail);

router.all('/updateStruct', auth.protectedManager, function (req, res) {
    User.updateStruct(function (err) {
        if (err) {
            GLogger.error(err);
        }
        return res.send({ status: err ? 1 : 0, message: err ? err.message : "tips.successful" });

    });

});
router.all('/captcha',function(req,res){
    let captcha = svgCaptcha.create({width:132,height:42});
    req.session.captcha = captcha.text;
    res.type('svg');
    res.status(200).send(captcha.data);
})

router.all('/captcha/check',function(req,res){
     res.send({
        status:0,
        captchaTime: req.session.captcha ? true : false
    })
})
//login 
router.post("/login", function (req, res) {
    if(AppConfig.ldap && AppConfig.ldap.server){
        return res.redirect(307,"/api/user/ldap/login");
    }

    if (!req.parameters.username  || !req.parameters.password ) {
        GLogger.audit(req,"tips.auth.usernameOrPasswordEmpty",Logger.AuditActionTypes.Login, Logger.AuditStatusTypes.Failed);
        return res.send({ status: 1, message: 'tips.auth.usernameOrPasswordEmpty' });
    }
    req.body.username = req.parameters.username 
    req.body.password = req.parameters.password 
    if(req.session.captcha && String(req.session.captcha).toLowerCase() !== String(req.parameters.captcha).toLowerCase()){
        return res.send({
            status:-1,
            message: "tips.auth.captchaNotRight",
            captchaTime: true
        })
    }

    passport.authenticate('local', function (err, user, passworderr) {
        let errorMessage = null;
        let status = 0;
        if (err || passworderr) {
            status = -1;
            errorMessage = passworderr && passworderr.message ? passworderr.message : 'tips.auth.incorrectUsernameOrPassword';
        } else if (user.access == -1) {
            status = -1;
            errorMessage ="tips.auth.accountInactive"; 
        } else if (user.access == 1) {//wait email active
            status = 120;
            errorMessage = "tips.auth.accountWaitingEmailActive"; 
         } else if (user.access == 2) {//please reset password
            status = 120;
            errorMessage = "tips.auth.accountWaitingReset";
        }

        if(status === -1){
            GLogger.log("login failed :", passworderr);
            GLogger.audit(req,errorMessage,Logger.AuditActionTypes.Login, Logger.AuditStatusTypes.Failed);
            req.logOut(()=>{
                return res.send({
                    status,
                    message:errorMessage || "tips.auth.loginFailed",
                    captchaTime: passworderr && passworderr.attempts >= 5
                })
            });
            return;
        }

        GLogger.audit(req,"Login success",Logger.AuditActionTypes.Login, Logger.AuditStatusTypes.Successful);
        
        //remove the captcha when login success
        req.session.captcha = null;

        req.query.username = user.email;
        req.login(user, (err) => {
            if (err) {
                GLogger.error(err)
            }
            let tempUser = Object.assign({}, user.toJSON ? user.toJSON() : user);
            delete tempUser.hash;
            delete tempUser.salt;

            if ('forwardSid' in req.body) {
                // Required to forward credentials to an external app (SightXR). Check LoginPage.js `handleLoginSubmit`
                const sid = 's:' + signature.sign(req.sessionID, AppConfig.session.secret);
                tempUser.sid = sid;
            }
    
            return res.send({
                 status: 0,
                 message: "tips.auth.loginSuccessfully",
                  content: tempUser
                 });
        })
   
    })(req, res);
});

//azure-ad login 
router.all("/azure-ad/login",
    passport.authenticate('azuread-openidconnect', {
        scope: ['email', 'profile'],
        session: false
    })
);

router.all(["/azure-ad/auth", "/azure-ad/callback"],(req, res, next) =>{
    passport.authenticate('azuread-openidconnect', function(err, user){
        if(err || !user){
            GLogger.audit(req,"Azure AD",Logger.AuditActionTypes.Login, Logger.AuditStatusTypes.Failed);
            req.session.azureAD = 0;
            res.redirect("/Login")
        }else{
            req.session.azureAD = 1;
            req.query.username = user.email;
            req.login(user, (err) => {
                if (err) {
                    res.redirect("/Login")
                }else{
                    GLogger.audit(req,"Azure AD",Logger.AuditActionTypes.Login, Logger.AuditStatusTypes.Successful);
                    res.redirect("/projects")
                }
            })
        }

    })(req,res,next)

})
   

router.all("/azure-ad/auth/logout",
    function (req, res) {
        GLogger.audit(req,"Azure AD",Logger.AuditActionTypes.Logout, Logger.AuditStatusTypes.Successful);
        req.session.azureAD = 0;
        res.redirect("/login")
    });

router.get(['/google/login'],
    passport.authenticate('google', {
        scope: ['email', 'profile'],
        session: false
    })
);

router.all(['/google/onetap'], function (req, res, next) {
    let credential = req.parameters['credential'];
    let g_csrf_token = req.parameters['g_csrf_token'];
    async.waterfall([
        function (callback) {
            let hasHackerInjection = !g_csrf_token || g_csrf_token !== req.cookies["g_csrf_token"];
            callback(hasHackerInjection ? new Error("has hacker") : null)
        },
        function (callback) {
            https.get(
                `https://oauth2.googleapis.com/tokeninfo?id_token=${credential}`,
                function (googleRes) {
                    let body = '';
                    googleRes.on('data', function (chunk) {
                        body += chunk;
                    });
                    googleRes.on('end', function () {
                        callback(null, body);
                    });
                }).on('error', function (e) {
                    callback(e);
                });
        },
        function (googleProfileResBody, callback) {
            try {
                let googleProfile = JSON.parse(googleProfileResBody);
                auth.handleGoogleLogin(googleProfile, callback);
            } catch (err) {
                callback(err)
            }
        },
        function (googleUser, callback) {
            req.query.username = googleUser.email;
            req.login(googleUser, function(err){
                callback(err, googleUser);
            })
        }
    ], function (err, user) {
        if (err) {
            GLogger.audit(req,"Google Onetap",Logger.AuditActionTypes.Login, Logger.AuditStatusTypes.Failed);
            return res.redirect(`/api/user/google/login`);
        }
        req.query.username = user.email;
        GLogger.audit(req,"Google Onetap",Logger.AuditActionTypes.Login, Logger.AuditStatusTypes.Successful);
        return res.redirect(`/projects`);

    })

});

router.post(['/ldap/login'], function(req, res) {
 
    if (!req.parameters.username  || !req.parameters.password ) {
        GLogger.audit(req,"LDAP",Logger.AuditActionTypes.Login, Logger.AuditStatusTypes.Failed);
        return res.send({ status: 1, message: 'Username or  Password is empty' });
    }
    req.body.username = req.parameters.username 
    req.body.password = req.parameters.password 

    passport.authenticate('ldapauth', {session: false, cache:false}, function (err, user, passworderr) {
        let errorMessage = null;
        let status = 0;
        if (err || passworderr) {
            GLogger.error("Auth Problem:", err, passworderr);
            status = -1;
            errorMessage ='tips.auth.incorrectUsernameOrPassword';
        } else if (user.access == -1) {
            status = -1;
            errorMessage = `tips.auth.accountInactive`; 
        } else if (user.access == 1) {//wait email active
            status = 120;
            errorMessage = `tips.auth.accountWaitingEmailActive`; 
         } else if (user.access == 2) {//please reset password
            status = 120;
            errorMessage = `tips.auth.accountWaitingReset`; 
        }

        if(status === -1){
            GLogger.log("login failed");
            GLogger.audit(req,"LDAP",Logger.AuditActionTypes.Login, Logger.AuditStatusTypes.Failed);
            req.logOut(() =>{
                return res.send({
                    status,
                    message:errorMessage || "tips.auth.loginFailed"
                })
            });
            return ;
        }

        GLogger.audit(req,"LDAP",Logger.AuditActionTypes.Login, Logger.AuditStatusTypes.Successful);
       
        req.login(user, (err) => {
            if (err) {
                GLogger.error(err)
            }
        })

        let tempUser = Object.assign({},  user.toJSON ? user.toJSON() : user);
        delete tempUser.hash;
        delete tempUser.salt;

        return res.send({ 
            status: 0, 
            message: "tips.auth.loginSuccessfully", 
            content: tempUser 
        });
    })(req, res);

  });

 

//logout
router.all(["/logout"], function (req, res,next) {
    let azureAD = req.session.azureAD;
    let googleAuth = req.session.google;
    GLogger.audit(req,"logout",Logger.AuditActionTypes.Logout, Logger.AuditStatusTypes.Successful)
    req.session.azureAD = 0;
    req.session.google = 0;
    //Do not need handle logout error and redirect to login page
    req.logout(function (err) {
        // if (err) {
        //   return next(err);
        // }
        req.session.destroy();
        // res.redirect("/login");
      });
    // req.session.destroy();
    if (AppConfig.azureAD && azureAD) {
        let logoutURL = encodeURIComponent(`${util.getHost(req)}/api/user/azure-ad/auth/logout`);
        let azureADLogoutURL = `https://login.windows.net/${AppConfig.azureAD.tenantID}/oauth2/logout?post_logout_redirect_uri=${logoutURL}`;
        return res.send({
            redirect: azureADLogoutURL,
            status: 0,
            message: 'tips.successful'
        })
    } else {
        return res.send({
            status: 0,
            message: 'tips.failed'
        });
    }
});

router.post('/register', function (req, res) {
    if(AppConfig.ldap){
        return res.send({ 
            status: 1, 
            message: `tips.auth.LDAPNotAllowRegister` 
        });
    }
    if (!req.parameters.email || !(/\S+@\S+\.\S+/.test(req.parameters.email))) {
        return res.send({ 
            status: 1,
            message: "tips.auth.noValidEmail" 
        });
    }
    req.query.username = req.parameters.username ? req.parameters.username : req.parameters.email;
    let verifyToken = {
        key: crypto.createHash('md5').update(`${req.query.username}:${new Date().getTime()}:${req.parameters.firstName}`).digest("hex"),
        createTime: new Date()
    }

    let user = new User({
        username: req.query.username.trim().toLowerCase(),
        firstName: req.parameters.firstName.trim(),
        lastName: req.parameters.lastName.trim(),
        city: req.parameters.city.trim(),
        email: req.parameters.email.toLowerCase().trim(),
        inviteCode:req.parameters.inviteCode?req.parameters.inviteCode.trim():null,
        type: 0,
        access: 1,
        verifyToken: verifyToken
    });

    User.register(user, req.parameters.password, function (err, account) {
        if (err) {
            GLogger.audit(req,"register",Logger.AuditActionTypes.Signup, Logger.AuditStatusTypes.Failed)
            return res.send({ status: 1, message:"tips.auth.emailExists" });
        } else {

            GLogger.audit(req,"register",Logger.AuditActionTypes.Signup, Logger.AuditStatusTypes.Successful, account || user, {}, GAuditUserFields)
            //is admin
            if (adminEmail == user.get("email")) {
                return res.send({ status: 0, message: `tips.auth.adminInit`, id: account._id });
            }

            EmailUtil.sendEmailWithTemplate(req.parameters.email, `Welcome to GraphXR. Please verify your email address.`, 'accountVerification.ejs', {
                token: verifyToken.key,
                firstName: user.firstName
            }, req).then(emailHtml => {
                return res.send({ status: 0, message: `tips.auth.registerActiveBySelf`, id: account._id });
            }).catch(err => {
                return res.send({ status: 101, message: `tips.auth.registerActionWithAdmin`, id: account._id });
            })
        }
    });

});


//apiAuthGenerate
router.all('/apiAuthGenerate', auth.protectedManager, function (req, res) {
    return User.apiAuthGenerate(req.parameters.id, function (err, user) {
        if (err) {
            return res.send({ status: 1, message: "tips.failed" });
        } else {
            return res.send({ status: 0, message: "tips.successful", content: user });
        }
    });
});

//update
router.all('/update', auth.protected, function (req, res) {
    let paramUser = req.parameters.user;

    let allowFields = ['firstName', 'lastName', 'password', 'city'];
    let userId = req.parameters.userId;
    let user = {
        username: req.user.get("username"),
        email: req.user.get("email")
    }

    _.forEach(paramUser, (v, k) => {
        if (allowFields.includes(k) && v != "") {
            user[k] = v;
        }
    })

    user.username = user.username ? user.username : user.email;

    let password = String(user.password);
    if (user.password) {
        delete user.password;
    }

    if(AppConfig.ldap){
     UserLDAP.modifyUser(
            user.email, 
            user.firstName, 
            user.lastName, 
            password,
            null,
            (err =>{
                err && GLogger.error(err);
                // GLogger.audit(req,
                //     `Edit the user ${user ? user.email : paramUser.email }`,
                //     Logger.AuditActionTypes.UserModified, 
                //    err ? Logger.AuditStatusTypes.Failed :  Logger.AuditStatusTypes.Successful , 
                //     {
                //         _id:req.user.id,
                //         "email":user.email, 
                //         "firstName":user.firstName, 
                //         "lastName":user.lastName, 
                //         "password":password
                //     }, {
                //         _id:req.user.id,
                //         "email":user.email, 
                //         "firstName":req.user.get("firstName"), 
                //         "lastName":req.user.get("lastName"), 
                //     }, 
                //     GAuditUserFields);

                // res.send({ 
                //     status: err ? 1 : 0, 
                //     message: err ? "Edit failed" : "Edit successful." 
                // });
            })
        )
    }

    function editUserCallBack(err, user) {
        //Do not shoud password info to log
        let diffMessage = `Edit the user ${user ? user.email : paramUser.email }`;
        if (err) {
            GLogger.error(err);
            GLogger.audit(req,diffMessage,Logger.AuditActionTypes.UserModified, Logger.AuditStatusTypes.Failed, user || paramUser, {}, GAuditUserFields);
            return res.send({ status: 1, message: "tips.actionTips.updatedFailed" });
        } else {
            GLogger.audit(req,diffMessage,Logger.AuditActionTypes.UserModified, Logger.AuditStatusTypes.Successful , user, {}, GAuditUserFields);
            // _.forEach(paramUser, (v, k) => {
            //Update The Session Data
            req.user = user
            //  })
            return res.send({ status: 0, message: "tips.actionTips.updatedSuccessfully" });
        }
    }

    if (password && password.length > 0) {
        return User.setPassword(password, user,  function (err, newPasswordUser) {
            if (err) {
                return editUserCallBack(err);
            } else {
                return User.edit(userId, user, editUserCallBack);
            }
        });
    } else {
        return User.edit(userId, user, editUserCallBack);
    }

});

//getInfo
router.all('/getInfo', auth.protected, function (req, res) {
    let username = req.parameters.username;
    if (username) {
        username = decodeURI(username);
    }

    return User.getInfo(req.parameters.id, username, function (err, user) {
        if (err || !user) {
            return res.send({ status: 1, message: "tips.failed" });
        } else {
            return res.send({ status: 0, message: "tips.successful", content: user });
        }
    });
});

router.all('/getUserByKeyword', auth.protected, function (req, res) {

    return User.getUserByKeyword(decodeURI(req.parameters.message), req.parameters.exitedEmail, function (err, user) {
        if (err || !user) {
            return res.send({ status: 1, message: "tips.failed" });
        } else {
            return res.send({ status: 0, message: "tips.successful", content: user });
        }
    });
});

//update Recent Project
router.all('/updateRecentProject', auth.protected, function (req, res) {
    let id = req.parameters.id;

    return User.updateRecentProject(req.parameters.id, req.parameters.recentProject, function (err, user) {
        if (err) {
            return res.send({ status: 1, message: "tips.failed" });
        } else {
            return res.send({ status: 0, message: "tips.successful" });
        }
    });
});

//forgetpassword
router.all("/forgetpassword", function (req, res) {
    if(AppConfig.ldap){
        return res.send({ status: 1, message: `tips.auth.LDAPNotAllowForgot` });
    }

    if (!req.parameters.email) {
        return res.send({ status: 1, message: "tips.auth.usernameOrPasswordEmpty." });
    }
    let email = req.parameters.email.trim();
    return User.forgotPassword(email, function (err, user) {
        if (err || !user) {
            GLogger.log(err);
            GLogger.audit(req,"Forgotpassword",Logger.AuditActionTypes.UserModified, Logger.AuditStatusTypes.Failed);
            return res.send({ status: 1, message: "tips.auth.emailNotFound" });
        } else {
            GLogger.audit(req,"Forgotpassword",Logger.AuditActionTypes.UserModified, Logger.AuditStatusTypes.Successful);
            EmailUtil.sendEmailWithTemplate(email, `Did your forget your password?`, 'forgotPassword.ejs', {
                token: user.token.key,
                firstName: user.firstName
            }, req).then(emailHtml => {
                return res.send({ status: 0, message: "tips.auth.checkResetEmail" });
            }).catch(err => {
                console.error(err);
                return res.send({ status: 1, message: `tips.auth.resetWithAdmin` });
            });
        }
    })
});

//emailLogin
// router.all("/emailLogin", function (req, res) {
// 	let email = req.parameters.email.trim();
// 	return User.emailLogin(email, function (err, user) {
// 		if (err || !user) {
// 			GLogger.log(err);
// 			return res.send({ status: 1, message: "Sorry! Email address not found. Please try again." });
// 		} else {

// 			sendEmailWithTemplate(email, "Log In Without Password", 'emailLoginEmail.html', {
// 				token: user.emailAuth.code,
// 				firstName: user.firstName
// 			}, req).then(emailHtml => {
//                 return res.send({ status: 0, message: "Within a few minutes you'll get a secure link to login, please check you email." });

//             }).catch(err => {
//                 return res.send({ status: 1, message: err.message });
//             })
// 		}
// 	})
// });

//emailAuth
// router.all("/emailAuth", function (req, res) {
// 	let token = req.parameters.token;
// 	let email = req.parameters.email.trim();
// 	return User.emailLoginAuth(email, token, function (err, user) {
// 		if (err || !user) {
// 			GLogger.log(err);
// 			return res.send({ status: 1, message: "Sorry! The token has expired, Please try again." });
// 		} else {

// 			user = JSON.parse(JSON.stringify(user));
// 			req.query.username = user.username;
// 			req.query.email = user.email;
// 			req.query.token = user.emailAuth.code;
// 			passport.authenticate('emailAuth')(req, res, function (err, loginUser) {
// 				if (!err) {
// 					return res.send({ status: 0, message: "email auth success.", content: userInfo });
// 				} else {
// 					return res.send({ status: 1, message: err.message });
// 				}

// 			});

// 		}
// 	})
// });

// resetpassword
router.all("/resetpassword", function (req, res) {
    
    if(AppConfig.ldap){
        return res.send({ status: 1, message: `tips.auth.LDAPNotAllowReset` });
    }

    let token = req.parameters.token;
    let password = req.parameters.password;
    if (!req.parameters.email) {
        GLogger.audit(
            req,
            "Miss email address.",
            Logger.AuditActionTypes.UserModified,
            Logger.AuditStatusTypes.Failed);

        return res.send({ status: 1, message: "tips.auth.usernameOrPasswordEmpty" });
    }
    let email = req.parameters.email.trim();
    req.query.username = email;

    if (password.length < 8) {
        GLogger.audit(
            req,
            "Passwords must be 8 characters or longer.",
            Logger.AuditActionTypes.UserModified,
            Logger.AuditStatusTypes.Failed);
        return res.send({ status: 1, message: "tips.auth.passwordLess8" });
    } else {
        return User.resetPassword(token, password, email, function (err, user) {
            if (err) {
                GLogger.audit(
                    req,
                    "Sorry, the token has expired.",
                    Logger.AuditActionTypes.UserModified,
                    Logger.AuditStatusTypes.Failed);
                return res.send({ status: 1, message: "tips.auth.tokenExpired" });
            } else {
                user = JSON.parse(JSON.stringify(user));
                req.query.username = user.username;
                GLogger.audit(
                    req,
                    "resetpassword",
                    Logger.AuditActionTypes.UserModified,
                    Logger.AuditStatusTypes.Successful);
           
                return res.send({ status: 0, message:  "tips.successful" });
            }
        });
    }
})


//changepassword
router.all("/changepassword", auth.protected, function (req, res) {
    let newPassword = req.parameters.newPassword;
    let oldPassword = req.parameters.oldPassword;
    let uid = req.parameters.userId;
    let user = req.user;
    if(AppConfig.ldap){
        UserLDAP.modifyUser(
            user.get("email"), 
            user.get("firstName"), 
            user.get("lastName"), 
            newPassword,
            null,
            (err =>{
                err && GLogger.error(err);
                // GLogger.audit(
                //     req,
                //     "changepassword",
                //     Logger.AuditActionTypes.UserModified,
                //     err ? Logger.AuditStatusTypes.Failed : Logger.AuditStatusTypes.Successful);
                // res.send({ 
                //     status: err ? 1 : 0, 
                //     message: err ? "Password change failed" : "Password change successful." 
                // });
            })
        )
    }
    return User.changePassword(uid, oldPassword, newPassword, function (err, user) {
        if (err) {
            GLogger.audit(
                req,
                "changepassword",
                Logger.AuditActionTypes.UserModified,
                Logger.AuditStatusTypes.Failed);
            res.send({ status: 1, message: 'tips.failed' });
        } else {
            req.query.username = user.username;
            req.query.password = newPassword;
            passport.authenticate('local')(req, res, function (err, user) {
                req.login(user, (err) => {
                    if (err) {
                        GLogger.error(err);
                    }
                })
                GLogger.audit(
                    req,
                    "changepassword",
                    Logger.AuditActionTypes.UserModified,
                    Logger.AuditStatusTypes.Successful);
                res.send({ status: 0, message: 'tips.successful' });
            })
        }
    })
})


/*--------------------------------------*/
/****************for manager*************/
/*--------------------------------------*/
//getAll
router.all("/getAll", auth.protectedManager, getAll);
router.all("/", auth.protectedManager, getAll);

//add
router.all('/add', auth.protectedManager, function (req, res) {
    if(AppConfig.ldap){
        return res.send({ status: 1, message: `tips.auth.LDAPNotAllowAdd` });
    }

    let user = req.parameters.user;
    user.username = user.username ? user.username : user.email;
    let password = user.password;

    let noticeUser = user.notice;
    if (noticeUser !== null && noticeUser !== undefined) {
        delete user.notice;
    }

    if (user.password) {
        delete user.password;
    }

    User.register(user, password, function (err, account) {
        let diffMessage = `Register user ${user.email}`;
        if (err) {
            GLogger.log(err);
            GLogger.audit(
                req,
                diffMessage,
                Logger.AuditActionTypes.UserAdded,
                Logger.AuditStatusTypes.Failed,
                account || user,
                {},
                GAuditUserFields);
            return res.send({ status: 1, message: "tips.auth.emailExists" });
        } else {

            // if (noticeUser) {

            // }
            GLogger.audit(
                req,
                diffMessage,
                Logger.AuditActionTypes.UserAdded,
                Logger.AuditStatusTypes.Successful,
                user,
                {},
                GAuditUserFields);
            return res.send({ status: 0, message: "tips.actionTips.addedSuccessfully" });
        }
    });
});



//edit
router.all('/edit', auth.protectedManager, function (req, res) {

    let user = Object.assign({},req.parameters.user);
    user.username = user.username ? user.username : user.email;
    let password = user.password;

    if (user.password) {
        delete user.password;
    }

    if(AppConfig.ldap){
        UserLDAP.modifyUser(
            user.email, 
            user.firstName, 
            user.lastName, 
            password,
            null,
            ((err,newUser,oldUser) =>{
                err && GLogger.error(err)
                // GLogger.audit(
                //     req,
                //     `Edit User ${user.email}`,
                //     Logger.AuditActionTypes.UserModified,
                //     err ? Logger.AuditStatusTypes.Failed : Logger.AuditStatusTypes.Successful,
                //     Object.assign({_id:user._id}, newUser),
                //     Object.assign({_id:user._id}, oldUser),
                //     GAuditUserFields);
                // res.send({ 
                //     status: err ? 1 : 0, 
                //     message: err ? "Edit user failed" : "Edit user successful." 
                // });
            })
        )
    }

    function editUserCallBack(err,oldUser) {
        if (err) {
            GLogger.error(err);
            GLogger.audit(
                req,
               `Edit User ${user.email}`,
                Logger.AuditActionTypes.UserModified,
                Logger.AuditStatusTypes.Failed);
            return res.send({ status: 1, message: "tips.actionTips.updatedFailed" });
        } else {
            GLogger.audit(
                req,
                `Edit User ${user.email}`,
                Logger.AuditActionTypes.UserModified,
                Logger.AuditStatusTypes.Successful,
                req.parameters.user,
                oldUser,
                GAuditUserFields);
            return res.send({ status: 0, message:  "tips.actionTips.updatedSuccessfully" });
        }
    }

    User.checkUsernameAndEmail(user._id, user.username, user.email, function (err, existingUser) {
        if (!err) {
            if (password && password.length > 0) {
                 return User.setPassword(password, user, function (err, newPasswordUser) {
                    if (err) {
                         return editUserCallBack(err, existingUser);
                    } else {
                        return User.edit(newPasswordUser._id, newPasswordUser, (err, newUser, oldUser) =>{
                            return editUserCallBack(err, oldUser);
                        });
                    }
                });
            } else {
                return User.edit(user._id, user,(err, newUser, oldUser) =>{
                    return editUserCallBack(err, oldUser);
                });
            }
        } else {
            return res.send({ status: 1, message: err.message });
        }

    });

});

//manager
router.all('/setManagers', auth.protectedManager, function (req, res) {
    const ids = req.parameters.ids || [];
    const type =  req.parameters.type || 0;
    const emails = req.parameters.emails || ids;
    
    if(AppConfig.ldap){
        let email = emails[0];
        UserLDAP.modifyUser(
            email, 
            null, 
            null, 
            null,
            type,
            ((err,newUser,oldUser) =>{
                err && GLogger.error(err);
                // const diffMessage = emails.join(',').concat(type === 1 ? " add manager permission" : " delete manager permission");
                // GLogger.audit(
                //     req,
                //     diffMessage,
                //     type === 1 ? Logger.AuditActionTypes.UserManagerAdded : Logger.AuditActionTypes.UserManagerDeleted ,
                //     err ? Logger.AuditStatusTypes.Failed : Logger.AuditStatusTypes.Successful,
                //     Object.assign({_id:ids[0]}, newUser),
                //     Object.assign({_id:ids[0]}, oldUser),
                //     GAuditUserFields);
                // res.send({ 
                //     status: err ? 1 : 0, 
                //     message: err ? "Edit user failed" : "Edit user successful." 
                // });
            })
        )
    }

    return User.updateType(ids, type, function (err, users) {
      const diffMessage = emails.join(',').concat(type === 1 ? " add manager permission" : " delete manager permission");
      const auditActionType = type === 1 ? Logger.AuditActionTypes.UserManagerAdded : Logger.AuditActionTypes.UserManagerDeleted  ;
      const newUser = {type,_id:ids[0]};
      const oldUser = {type: type === 1 ? 0 : 1,_id:ids[0]};
      if (err) {
            GLogger.audit(
                req,
                diffMessage,
                auditActionType,
                Logger.AuditStatusTypes.Failed,
                newUser,
                oldUser
                );
            return res.send({
                status: 1,
                message: "tips.failed"
            });
        } else {
            GLogger.audit(
                req,
                diffMessage,
                auditActionType,
                Logger.AuditStatusTypes.Successful,
                newUser,
                oldUser);
            return res.send({
                status: 0,
                message: "tips.successful"
            });
        }
    });

})

router.all('/removeInactiveUsers', auth.protectedManager, function (req, res) {
    // const ids = req.parameters.ids || [];
    return User.removeInactiveUsers(function (err) {
        return res.send({
            status:err ? 1 : 0,
            message: err ? "tips.failed" :"tips.successful"
        });
    });
})

//getAll
function getAll(req, res) {
    User.getAll(req.parameters.condition, function (err, users) {
        if (err) {
            GLogger.error(err);
            return res.send({
                status: 1,
                message: "tips.failed"
            });
        } else {
            return res.send({
                status: 0,
                message: "tips.successful",
                content: users
            });
        }
    }, req.parameters.isCount);
}
/*--------------------------------------*/
/**************** end manager*************/
/*--------------------------------------*/




module.exports = router;
module.exports.verifyEmail = verifyEmail;