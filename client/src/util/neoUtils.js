const { Integer } = require("neo4j-driver");

/**
 * 
 * @param {number} bigNumber 
 * @returns 
 */
export function createInteger(bigNumber) {
    if (bigNumber > 9007199254740991) {
        // Max int that JavaScript can represent is 2^53.
        throw new Error('The 64-bit value is too big to be represented in JS :' + bigNumber);
    }

    var bigNumberAsBinaryStr = bigNumber.toString(2);
    // Convert the above binary str to 64 bit (actually 52 bit will work) by padding zeros in the left
    var bigNumberAsBinaryStr2 = '';
    for (var i = 0; i < 64 - bigNumberAsBinaryStr.length; i++) {
        bigNumberAsBinaryStr2 += '0';
    };

    bigNumberAsBinaryStr2 += bigNumberAsBinaryStr;

    return new Integer(parseInt(bigNumberAsBinaryStr2.substring(32), 2), parseInt(bigNumberAsBinaryStr2.substring(0, 32), 2))

    // {
    //     highInt32: parseInt(bigNumberAsBinaryStr2.substring(0, 32), 2),
    //     lowInt32: parseInt(bigNumberAsBinaryStr2.substring(32), 2)
    // };
}