const { createXHR } = require("./ajax");
/**
	responseHTML
	(c) 2007-2008 xul.fr		
	Licence Mozilla 1.1
*/


/**
	Searches for body, extracts and return the content
	New version contributed by users
*/
function getBody(content) {
	let test = content.toLowerCase();    // to eliminate case sensitivity
	var x = test.indexOf("<body");
	if (x === -1) return "";
	x = test.indexOf(">", x);
	if (x === -1) return "";
	var y = test.lastIndexOf("</body>");
	if (y === -1) y = test.lastIndexOf("</html>");
	if (y === -1) y = content.length;    // If no HTML then just grab everything till end
	return content.slice(x + 1, y);
}

/**
	Loads a HTML page
	Put the content of the body tag into the current page.
	Arguments:
		url of the other HTML page to load
		id of the tag that has to hold the content
*/
function loadHTML(url, fun, storage, param) {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (xhttp.readyState === 4) {
			if (xhttp.status === 200) {
				storage.innerHTML = getBody(xhttp.responseText);
				fun(storage, param);
			}
		}
	};
	xhttp.open("GET", url, true);
	xhttp.send();
}

/**
	Callback
	Assign directly a tag
*/
function processHTML(temp, target) {
	target.innerHTML = temp.innerHTML;
}

export function loadWholePage(url) {
	var y = document.getElementById("storage");
	var x = document.getElementById("displayed");
	loadHTML(url, processHTML, x, y);
}


/**
	Create responseHTML
	for acces by DOM's methods
*/
function processByDOM(responseHTML, target) {
	target.innerHTML = "Extracted by id:<br />";

	// does not work with Chrome/Safari
	//var message = responseHTML.getElementsByTagName("div").namedItem("two").innerHTML;
	var message = responseHTML.getElementsByTagName("div").item(1).innerHTML;

	target.innerHTML += message;

	target.innerHTML += "<br />Extracted by name:<br />";

	message = responseHTML.getElementsByTagName("form").item(0);
	target.innerHTML += message.dyn.value;
}

function accessByDOM(url) {
	//var responseHTML = document.createElement("body");	// Bad for opera
	var responseHTML = document.getElementById("storage");
	var y = document.getElementById("displayed");
	loadHTML(url, processByDOM, responseHTML, y);
}

