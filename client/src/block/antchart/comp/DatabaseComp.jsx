import { Button, Row, Col, Select, Switch, InputNumber, Dropdown, Menu, Radio, Space } from 'antd';
import { ColumnsComp, FilterComp, FilterMongoComp, SliceComp, SortComp, withPopMenu, ColumnsDetailComp, FilterDyComp } from 'block/code/CodeTools';
import _ from 'lodash';
import React from "react";
import {
    getColumnName, getColumnType,
    getSchema, getTableName, CodeMode, SelectProps, getColumnKeyType
} from "../../../util/utils";
import { RunCodeIcon } from '../../code/Icon';
import UploadData from './UploadData';
import { RawJSONComp } from './TableComp';
import { getMoV } from '../antchartUtils';
import { RenderType, SelectionTypes, mongo_Template, dynamo_Template, attrSelects } from 'util/databaseApi';

const { Option, OptGroup } = Select;
export const DatabaseComp = (props) => {
    /**@type {UploadData} */
    const settings = props.settings;
    const { dbTable, query, selectedTable, tables, needRun,
        filters, columns, selectedColumns, sorts, slice, client } = settings.state;
    const filterButton = withPopMenu(FilterComp, {
        icon: <i className="icon fas fa-filter"></i>,
        label: <span><span className="button-label">Filter&nbsp;&nbsp;</span>{filters.length - 1}</span>,
        columns: columns,
        filters: _.cloneDeep(filters),
        setFilters: (filters) => {
            if (!_.isEqual(filters[filters.length - 1], {})) {
                filters.push({});
            }
            settings.setState({ needRun: true, filters/*, nData: []*/ })
        }
    })
    const columnsButton = withPopMenu(ColumnsComp, {
        icon: <i className="icon fas fa-check-square"></i>,
        label: <span><span className="button-label">Columns&nbsp;&nbsp;</span>{selectedColumns.length}</span>,
        plainOptions: _.reduce(columns, (prev, column, index) => {
            prev.push({
                value: getColumnName(column), label:
                    <div className="d-flex justify-content-between">
                        <span>{getColumnName(column)}&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span className="text-muted small">{getColumnType(column)}{getColumnKeyType(column)}</span>
                    </div>
            }); return prev
        }, []),
        checkedList: selectedColumns,
        setCheckedList: (checkedList) => {
            settings.setState({ needRun: true, selectedColumns: checkedList/*, nData: []*/ });
        }
    })
    const columnsDetailsButton = !dbTable && withPopMenu(ColumnsDetailComp, {
        label: <span title={`Table columns`}><i className="icon fas fa-columns"></i>&nbsp;&nbsp;{columns.length}</span>,
        columns: _.map(columns, (column, index) => {
            return <div key={index} className="d-flex justify-content-between">
                <span>{getColumnName(column)}&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span className="text-muted small">{getColumnType(column)}{getColumnKeyType(column)}</span>
            </div>;
        })
    })
    const sortButton = withPopMenu(SortComp, {
        icon: <i className="icon fas fa-sort-amount-down-alt"></i>,
        label: <span><span className="button-label">Sort&nbsp;&nbsp;</span>{sorts.length - 1}</span>,
        columns: columns,
        sorts: _.cloneDeep(sorts),
        setSorts: (sorts) => {
            if (!_.isEqual(sorts[sorts.length - 1], {})) {
                sorts.push({});
            }
            settings.setState({ needRun: true, sorts/*, nData: []*/ })
        }
    })
    const sliceButton = withPopMenu(SliceComp, {
        icon: <i className="icon fas fa-cut"></i>,
        label: <span><span className="button-label">Slice&nbsp;&nbsp;</span>[{slice.from}, {slice.to}]</span>,
        slice: _.cloneDeep(slice),
        setSlice: (slice) => {
            settings.setState({ needRun: true, slice/*, nData: []*/ })
        },
        dialect: client && client.dialect
    })
    return <div className="database">
        <Space
            direction="vertical"
            size="small"
            style={{
                display: 'flex',
            }}
        >
            <div className="d-flex align-items-center div-gutter">
                {tables.length !== 0 && <Select getPopupContainer={SelectProps.getPopupContainer} value={selectedTable} placeholder={dbTable ? `Select a table` : "Show tables"} style={{ width: 200 }} allowClear notFoundContent={"None available"}
                    onChange={(value) => { settings.handleSelectTableChange(value, false) }}>
                    <OptGroup label="Tables">
                        {_.map(tables, (table) => {
                            let schema = getSchema(table);
                            let tableName = getTableName(table);
                            let optionLabel = `${schema ? schema + "." : ""}${tableName}`;
                            return <Option value={optionLabel} key={optionLabel}>{optionLabel}</Option>
                        })}
                    </OptGroup>
                </Select>}
                <Switch checkedChildren="Data Table" unCheckedChildren="Database Query" checked={dbTable} onChange={(checked) => {
                    settings.setState({ needRun: true, dbTable: checked })
                }} />
                {!dbTable && columnsDetailsButton}
                {client && <Button icon={<RunCodeIcon fill={needRun} />} onClick={() => {
                    settings.runCode(true)
                }}>Run</Button>}
            </div>
            {dbTable && client &&
                <div className="d-flex align-items-center">
                    {filterButton}
                    {columnsButton}
                    {sortButton}
                    {sliceButton}
                </div>}
            {!dbTable && client && <RawJSONComp renderType={CodeMode.sql} result={query} run={() => {
                settings.runCode(true);
            }} onChange={(cm, data, value) => {
                settings.setState({ needRun: true, query: value })
            }}></RawJSONComp>}
        </Space>
    </div>
}

export const ParameterSelectComp = (props) => {
    /**@type {UploadData} */
    const settings = props.settings;
    const { selectionType, nodeLabel, nodeLabels, propertyName, propertyNames, propertyNum, propertyValue, propertyValues } = settings.state;
    const chartSettings = settings.getSettings();
    const { chartConfig: { neo, selectedCategory, selectedCategoryFrom } } = chartSettings.state;
    return <div>
        <div className="d-flex flex-wrap align-items-center div-gutter">
            <span>Selection Type:</span>
            <Select
                showSearch
                getPopupContainer={SelectProps.getPopupContainer}
                value={selectionType}
                style={{ width: 120 }}
                defaultActiveFirstOption={false}
                showArrow={false}
                filterOption={false}
                onChange={(value) => {
                    settings.querySelectionTypes(selectedCategoryFrom, selectedCategory, value);
                }}
                notFoundContent={null}
                options={_.map(SelectionTypes, (d, k) => ({
                    value: k,
                    label: d.label,
                }))}
            />
            {selectionType && <>
                <span>{SelectionTypes[selectionType].nextLabel}:</span>
                <Select
                    getPopupContainer={SelectProps.getPopupContainer}
                    showSearch
                    placeholder="Start typing..."
                    value={nodeLabel || ""}
                    style={{ width: 120 }}
                    defaultActiveFirstOption={false}
                    showArrow={false}
                    filterOption={true}
                    onChange={(value) => {
                        settings.setState({ nodeLabel: value })
                    }}
                    notFoundContent={null}
                    options={_.map(nodeLabels, (d, index) => ({
                        value: d,
                        label: d,
                    }))}
                /></>}
            {nodeLabel && <>
                <span>Property Name:</span>
                <Select
                    getPopupContainer={SelectProps.getPopupContainer}
                    showSearch
                    placeholder="Start typing..."
                    value={propertyName || ""}
                    style={{ width: 120 }}
                    defaultActiveFirstOption={false}
                    showArrow={false}
                    filterOption={true}
                    onSearch={(newValue) => {
                        if (newValue) {
                            settings.queryPropertyNames(selectedCategoryFrom, selectedCategory, newValue);
                        } else {
                            settings.setState({ propertyNames: [] });
                        }
                    }}
                    onChange={(value) => {
                        settings.setState({ propertyName: value })
                    }}
                    notFoundContent={null}
                    options={_.map(propertyNames, (d, index) => ({
                        value: d,
                        label: d,
                    }))}
                /></>}
            <span>Number (optional):</span>
            <InputNumber min={-10000} max={10000} value={propertyNum} onChange={(v) => {
                settings.setState({ propertyNum: v })
            }} />
        </div>
        <div className="d-flex flex-wrap align-items-center div-gutter">
            {selectionType && nodeLabel && propertyName && <>
                <span>{nodeLabel} {propertyName}:</span>
                <Select
                    getPopupContainer={SelectProps.getPopupContainer}
                    showSearch
                    placeholder="Start typing..."
                    value={propertyValue}
                    style={{ width: 120 }}
                    defaultActiveFirstOption={false}
                    showArrow={false}
                    filterOption={false}
                    onSearch={(newValue) => {
                        if (newValue) {
                            settings.queryPropertyValues(selectedCategoryFrom, selectedCategory, selectionType, nodeLabel, propertyName, newValue);
                        } else {
                            settings.setState({ propertyValues: [] });
                        }
                    }}
                    onChange={(value) => {
                        settings.setState({ propertyValue: value })
                    }}
                    notFoundContent={null}
                    options={_.map(propertyValues, (d, index) => ({
                        value: d,
                        label: d,
                    }))}
                /></>}
            <Button icon={<RunCodeIcon fill={!_.isEqual({ selectionType, nodeLabel, propertyName, propertyNum, propertyValue },
                neo && { selectionType: neo.selectionType, nodeLabel: neo.nodeLabel, propertyName: neo.propertyName, propertyNum: neo.propertyNum, propertyValue: neo.propertyValue })} />} onClick={() => {
                    chartSettings.modifyChartConfig({ neo: _.assign(neo && _.cloneDeep(neo) || {}, { selectionType, nodeLabel, propertyName, propertyNum, propertyValue }) }, () => {
                        settings.renderChart();
                    });
                }}>Run</Button>
        </div>
        <div>{selectionType && nodeLabel && propertyName && propertyValue ? `${["neodash", nodeLabel, propertyName].join("_").toLowerCase()}${undefined !== propertyNum ? `_${propertyNum}` : ""} = "${propertyValue}"` : ""}</div>
        <div>Can Use <b>${`${["neodash", nodeLabel, propertyName].join("_").toLowerCase()}${undefined !== propertyNum ? `_${propertyNum}` : ""}`}</b> in a query to use the parameter.</div>
    </div>

}

/**
 * 
 * @param {{ nor: false, logic: "and", rules: [{}] }} filter 
 */
const transformMongoFilter = function (filter, collectionFields) {
    const { logic, rules, nor } = filter;
    let result;
    if (logic === "and") {
        result = _.reduce(rules.slice(0, rules.length - 1), (prev, rule, index) => {
            const { columnName, association, value } = rule;
            prev[columnName] = getMoV(rule, collectionFields);
            return prev;
        }, {});
    } else {
        result = {
            "$or": _.reduce(rules.slice(0, rules.length - 1), (prev, rule, index) => {
                const { columnName, association, value } = rule;
                prev.push({ [columnName]: getMoV(rule, collectionFields) })
                return prev;
            }, [])
        };
    }
    if (nor) {
        return { "$nor": [result] }
    } else {
        return result;
    }
}

export const MongoComp = (props) => {
    /**@type {UploadData} */
    const settings = props.settings;
    const { needRun, client, collectionNames, collectionFields, mongo } = settings.state;
    const chartSettings = settings.getSettings();
    const { chartConfig: { selectedCategoryFrom, selectedCategory, active } } = chartSettings.state;
    let mongoRun = () => {
        chartSettings.modifyChartConfig(!active ? {
            chartData: [],
            chartColumns: [],
            mongo: _.cloneDeep(mongo),
        } : {
            mongo: _.cloneDeep(mongo),
        }, () => {
            settings.renderChart(() => {
                settings.inspector();
            })
        })
    };
    const collectionFieldsButton = mongo.collectionName && collectionFields.length > 0 && withPopMenu(ColumnsDetailComp, {
        label: <span title={`Collection ${mongo.collectionName} fields`}><i className="icon fas fa-columns"></i>&nbsp;&nbsp;{collectionFields.length}</span>,
        columns: _.map(collectionFields, (column, index) => {
            return <div key={index} className="d-flex justify-content-between">
                <span>{getColumnName(column)}&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span className="text-muted small">{getColumnType(column)}{getColumnKeyType(column)}</span>
            </div>;
        })
    })
    let filters = _.assign({ nor: false, logic: "and", rules: [{}] }, mongo.filterTmp);
    const filterButton = withPopMenu(FilterMongoComp, {
        icon: <i className="icon fas fa-filter"></i>,
        label: <span><span className="button-label">Filter&nbsp;&nbsp;</span>{filters.rules.length - 1}</span>,
        columns: collectionFields,
        filters: filters,
        setFilters: (filters) => {
            if (!_.isEqual(filters.rules[filters.rules.length - 1], {})) {
                filters.rules.push({});
            }
            mongo.filterTmp = filters;
            mongo.filter = transformMongoFilter(filters, collectionFields);
            mongo.filterStr = JSON.stringify(mongo.filter);
            settings.setState({ needRun: true, mongo })
        }
    })
    let selectedFields = _.keys(mongo.projection);
    const projectionButton = withPopMenu(ColumnsComp, {
        icon: <i className="icon fas fa-check-square"></i>,
        label: <span><span className="button-label">Projection&nbsp;&nbsp;</span>{selectedFields.length}</span>,
        plainOptions: _.reduce(collectionFields, (prev, column, index) => {
            prev.push({
                value: getColumnName(column), label:
                    <div className="d-flex justify-content-between">
                        <span>{getColumnName(column)}&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span className="text-muted small">{getColumnType(column)}{getColumnKeyType(column)}</span>
                    </div>
            }); return prev
        }, []),
        checkedList: selectedFields,
        setCheckedList: (checkedList) => {
            mongo.projection = _.reduce(checkedList, (prev, curr, index) => {
                prev[curr] = 1;
                return prev;
            }, {});
            mongo.projectionStr = JSON.stringify(mongo.projection);
            settings.setState({ needRun: true, mongo });
        }
    })
    let sortFields = _.reduce(mongo.sort, (prev, value, key) => {
        prev.push({ column: key, direction: value > 0 ? "asc" : "desc" });
        return prev;
    }, [])
    sortFields.push({});
    const sortButton = withPopMenu(SortComp, {
        icon: <i className="icon fas fa-sort-amount-down-alt"></i>,
        label: <span><span className="button-label">Sort&nbsp;&nbsp;</span>{sortFields.length - 1}</span>,
        columns: collectionFields,
        sorts: sortFields,
        setSorts: (sorts) => {
            mongo.sort = _.reduce(sorts, (prev, curr, index) => {
                if (!_.isEqual(curr, {})) {
                    prev[curr.column] = curr.direction === "asc" ? 1 : -1;
                }
                return prev;
            }, {})
            mongo.sortStr = JSON.stringify(mongo.sort);
            settings.setState({ needRun: true, mongo })
        }
    })
    return <React.Fragment>
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
            <Col className="gutter-row" sm={24} md={12}>
                <Space
                    direction="vertical"
                    size="small"
                    style={{
                        display: 'flex',
                    }}
                >
                    <div className="d-flex flex-wrap align-items-center div-gutter">
                        <span> Collection Name:</span>
                        <Select
                            getPopupContainer={SelectProps.getPopupContainer}
                            showSearch
                            value={mongo.collectionName}
                            style={{ width: 120 }}
                            defaultActiveFirstOption={false}
                            showArrow={false}
                            filterOption={false}
                            onChange={(value) => {
                                _.assign(mongo, _.cloneDeep(mongo_Template));
                                mongo.collectionName = value;
                                settings.getCollectionFields(selectedCategoryFrom, selectedCategory, mongo.collectionName);
                                settings.setState({ needRun: true, mongo })
                            }}
                            notFoundContent={null}
                            options={_.map(collectionNames, (d, k) => ({
                                value: d,
                                label: d,
                            }))}
                        />
                        {collectionFieldsButton}
                    </div>
                    <div className="d-flex flex-wrap align-items-center div-gutter">
                        {filterButton}
                        <RawJSONComp className="limit-height" renderType={RenderType.RawJSON} result={mongo.filterStr} onChange={(cm, data, value) => {
                            mongo.filterStr = value;
                            try {
                                mongo.filter = JSON.parse(value);
                            } catch (e) {
                            }
                            settings.setState({ needRun: true, mongo })
                        }} run={mongoRun}></RawJSONComp>
                    </div>
                    <div className="d-flex flex-wrap align-items-center div-gutter">
                        {projectionButton}
                        <RawJSONComp className="limit-height" renderType={RenderType.RawJSON} result={mongo.projectionStr} onChange={(cm, data, value) => {
                            mongo.projectionStr = value;
                            try {
                                mongo.projection = JSON.parse(value);
                            } catch (e) {
                            }
                            settings.setState({ needRun: true, mongo })
                        }} run={mongoRun}></RawJSONComp>
                    </div>
                    <div className="d-flex flex-wrap align-items-center div-gutter">
                        {sortButton}
                        <RawJSONComp className="limit-height" renderType={RenderType.RawJSON} result={mongo.sortStr} onChange={(cm, data, value) => {
                            mongo.sortStr = value;
                            try {
                                mongo.sort = JSON.parse(value);
                            } catch (e) {
                            }
                            settings.setState({ needRun: true, mongo })
                        }} run={mongoRun}></RawJSONComp>
                    </div>
                    <div className="d-flex flex-wrap align-items-center div-gutter">
                        <span> Limit:</span>
                        <InputNumber min={0} max={1000000} value={mongo.limit} onChange={(v) => {
                            mongo.limit = v;
                            settings.setState({ needRun: true, mongo })
                        }} />
                        <span> Skip:</span>
                        <InputNumber min={0} max={1000000} value={mongo.skip} onChange={(v) => {
                            mongo.skip = v;
                            settings.setState({ needRun: true, mongo })
                        }} />
                    </div>
                </Space>
            </Col>
            <Col className="gutter-row" sm={24} md={12}>
                <div className="d-flex">
                    <Button icon={<RunCodeIcon fill={needRun && client} />} onClick={mongoRun}>Run</Button>
                </div>
            </Col>
        </Row>
    </React.Fragment>
}

export const DynamoComp = (props) => {
    /**@type {UploadData} */
    const settings = props.settings;
    const { needRun, client, TableNames, TableFields, dynamo } = settings.state;
    const chartSettings = settings.getSettings();
    const { chartConfig: { selectedCategoryFrom, selectedCategory, active } } = chartSettings.state;
    let dynamoRun = () => {
        chartSettings.modifyChartConfig(!active ? {
            chartData: [],
            chartColumns: [],
            dynamo: _.cloneDeep(dynamo),
        } : {
            dynamo: _.cloneDeep(dynamo),
        }, () => {
            settings.renderChart(() => {
                settings.inspector();
            })
        })
    };
    const TableFieldsButton = dynamo.TableName && TableFields.length > 0 && withPopMenu(ColumnsDetailComp, {
        label: <span title={`Table ${dynamo.TableName} fields`}><i className="icon fas fa-columns"></i>&nbsp;&nbsp;{TableFields.length}</span>,
        columns: _.map(TableFields, (column, index) => {
            return <div key={index} className="d-flex justify-content-between">
                <span>{getColumnName(column)}&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span className="text-muted small">{getColumnType(column)}{getColumnKeyType(column)}</span>
            </div>;
        })
    })
    const filterButton = <FilterDyComp
        label={<span> <i className="icon fas fa-filter"></i> <span className="button-label">Filter:&nbsp;&nbsp;</span>{dynamo.scanFilterTmp.length - 1}</span>}
        columns={TableFields}
        filters={_.cloneDeep(dynamo.scanFilterTmp)}
        setFilters={(filters) => {
            if (!_.isEqual(filters[filters.length - 1], {})) {
                filters.push({});
            }
            dynamo.scanFilterTmp = filters;
            dynamo.ScanFilter = _.reduce(dynamo.scanFilterTmp.slice(0, dynamo.scanFilterTmp.length - 1), (prev, rule, index) => {
                const { columnName, columnType, association, values } = rule;
                prev[columnName] = {
                    AttributeValueList: _.reduce(values, (prev, value, index) => {
                        prev.push({ [columnType]: `${value}` });
                        return prev;
                    }, []),
                    ComparisonOperator: association
                };
                return prev;
            }, {});
            dynamo.scanFilterStr = JSON.stringify(dynamo.ScanFilter);
            settings.setState({ needRun: true, dynamo })
        }}></FilterDyComp>

    return <React.Fragment>
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
            <Col className="gutter-row" sm={24} md={12}>
                <Space
                    direction="vertical"
                    size="small"
                    style={{
                        display: 'flex',
                    }}
                >
                    <div className="d-flex flex-wrap align-items-center div-gutter">
                        <span>Select a table:</span>
                        <Select
                            getPopupContainer={SelectProps.getPopupContainer}
                            showSearch
                            value={dynamo.TableName}
                            style={{ width: 120 }}
                            defaultActiveFirstOption={false}
                            showArrow={false}
                            filterOption={false}
                            onChange={(value) => {
                                _.assign(dynamo, _.cloneDeep(dynamo_Template));
                                dynamo.TableName = value;
                                settings.getTableFields(selectedCategoryFrom, selectedCategory, dynamo.TableName);
                                settings.setState({ needRun: true, dynamo })
                            }}
                            notFoundContent={null}
                            options={_.map(TableNames, (d, k) => ({
                                value: d,
                                label: d,
                            }))}
                        />
                        {TableFieldsButton}
                    </div>
                    <div className="d-flex flex-wrap align-items-center div-gutter">
                        <span>Select attribute projection:</span>
                        <Select
                            getPopupContainer={SelectProps.getPopupContainer}
                            showSearch
                            value={dynamo.Select}
                            style={{ width: 120 }}
                            defaultActiveFirstOption={false}
                            showArrow={false}
                            filterOption={false}
                            onChange={(value) => {
                                dynamo.Select = value;
                                dynamo.AttributesToGet = []
                                settings.setState({ needRun: true, dynamo })
                            }}
                            notFoundContent={null}
                            options={_.map(attrSelects, (d, k) => ({
                                value: d.value,
                                label: d.label,
                            }))}
                        />
                    </div>
                    {attrSelects[0].value !== dynamo.Select && <div className="d-flex flex-wrap align-items-center div-gutter">
                        <span>Select attribute projection:</span>
                        <Select
                            getPopupContainer={SelectProps.getPopupContainer}
                            mode="tags"
                            value={dynamo.AttributesToGet}
                            onChange={(value) => {
                                dynamo.AttributesToGet = value;
                                settings.setState({ needRun: true, dynamo })
                            }}
                            options={_.map(TableFields, (column, index) => ({
                                value: getColumnName(column),
                                label: <div className="d-flex justify-content-between">
                                    <span>{getColumnName(column)}&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    <span className="text-muted small">{getColumnType(column)}{getColumnKeyType(column)}</span>
                                </div>,
                            }))}
                        />
                    </div>}
                    <div className="d-flex flex-wrap align-items-center div-gutter">
                        <span>Condition operator:</span>
                        <Radio.Group onChange={(e) => {
                            dynamo.ConditionalOperator = e.target.value;
                            settings.setState({ needRun: true, dynamo })
                        }} value={dynamo.ConditionalOperator}>
                            <Radio value={"AND"}>And</Radio>
                            <Radio value={"OR"}>Or</Radio>
                        </Radio.Group>
                    </div>
                    {filterButton}
                    <div className="d-flex flex-wrap align-items-center div-gutter">
                        <span> Limit:</span>
                        <InputNumber min={0} max={1000000} value={dynamo.Limit} onChange={(v) => {
                            dynamo.Limit = v;
                            settings.setState({ needRun: true, dynamo })
                        }} />
                    </div>
                </Space>
            </Col>
            <Col className="gutter-row" sm={24} md={12}>
                <div className="d-flex">
                    <Button icon={<RunCodeIcon fill={needRun && client} />} onClick={dynamoRun}>Run</Button>
                </div>
            </Col>
        </Row>
    </React.Fragment>
}