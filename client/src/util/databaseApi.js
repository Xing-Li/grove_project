import { SELECTION_TYPES } from "block/antchart/config/CardConfig"
import { ShowType, fontAlignTypes, radarDefault, multiRadarDefault } from './chartConfigUtil'

const sizeTypes = ['small', 'medium', 'large'];
const verticalAlignTypes = ["bottom", "middle", "top"];

export const DefaultDatabase = {
    name: "",
    type: "",
    connectionHostedBy: "",
    host: "",
    proxyHost: "",
    password: "",
    port: 3306,
    location: "",
    createTime: new Date().getTime(),
    secret: "",
    sslTls: false,
}

export const DefaultSecret = {
    name: "",
    value: "",
    createTime: new Date().getTime(),
}

export const SelectionTypes = {
    "nodeProperty": {
        label: "Node Property",
        nextLabel: "Node Label",
        query: `CALL db.labels() YIELD label WITH label as nodeLabel RETURN DISTINCT nodeLabel`,
        queryValue: (nodeLabel, propertyName) => `MATCH (n:${nodeLabel}) 
        WHERE toLower(toString(n.${propertyName})) CONTAINS toLower($input) 
        RETURN DISTINCT n.${propertyName} as value ORDER BY size(toString(value)) ASC LIMIT 10`
    },
    "relationshipProperty": {
        label: "Relationship Property",
        nextLabel: "Relationship Type",
        query: `CALL db.relationshipTypes() YIELD relationshipType WITH relationshipType as relType RETURN DISTINCT relType`,
        queryValue: (relType, propertyName) => `"MATCH ()-[n:${relType}]->() 
        WHERE toLower(toString(n.${propertyName})) CONTAINS toLower($input) 
        RETURN DISTINCT n.${propertyName} as value ORDER BY size(toString(value)) ASC LIMIT 10"`
    },
}

export const RenderType = {
    Code: "Code",
    Markdown: "Markdown",
    Python: "Python",
    IFrame: "IFrame",
    RawJSON: "RawJSON",
    Table: "Table",
    Chart: "Chart",
    SingleValue: "SingleValue",
    ParameterSelect: "ParameterSelect",
    Graph: "Graph",
    Map: "Map",
    GaugeChart: "GaugeChart",
    SunburstChart: "SunburstChart",
    CirclePacking: "CirclePacking",
    Treemap: "Treemap",
    SankeyChart: "SankeyChart",
    ChoroplethMap: "ChoroplethMap",
    AreaMap: "AreaMap",
    Radar: "Radar",
}
export const isChartRenderType = function (renderType) {
    return !renderType || renderType === RenderType.Chart;
}
export const DEFAULT_ROW_HEIGHT = 30;
export const DEFAULT_COLS = { lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 };
export const DEFAULT_BREAK_POINTS = { lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0 };
export const DEFAULT_CELL_PADDING = { lg: 40, md: 40, sm: 30, xs: 20, xxs: 0 };
export const DEFAULT_DATA_GRID = {
    lg: { w: 6, h: 8, x: 0, y: 0, minW: 2, minH: 3 },
    md: { w: 5, h: 7, x: 0, y: 0, minW: 2, minH: 3 },
    sm: { w: 6, h: 6, x: 0, y: 0, minW: 2, minH: 3 },
    xs: { w: 4, h: 6, x: 0, y: 0, minW: 2, minH: 3 },
    xxs: { w: 2, h: 6, x: 0, y: 0, minW: 2, minH: 3 },
};
const DATA_GRID_TEXT = {
    lg: { w: 3, h: 4, x: 0, y: 0, minW: 2, minH: 3 },
    md: { w: 3, h: 4, x: 0, y: 0, minW: 2, minH: 3 },
    sm: { w: 6, h: 4, x: 0, y: 0, minW: 2, minH: 3 },
    xs: { w: 4, h: 4, x: 0, y: 0, minW: 2, minH: 3 },
    xxs: { w: 2, h: 4, x: 0, y: 0, minW: 2, minH: 3 },
}
const DATA_GRID_JSON = {
    lg: { w: 3, h: 8, x: 0, y: 0, minW: 2, minH: 3 },
    md: { w: 3, h: 8, x: 0, y: 0, minW: 2, minH: 3 },
    sm: { w: 6, h: 8, x: 0, y: 0, minW: 2, minH: 3 },
    xs: { w: 4, h: 8, x: 0, y: 0, minW: 2, minH: 3 },
    xxs: { w: 2, h: 8, x: 0, y: 0, minW: 2, minH: 3 },
}

const nodeColorSchemes = ["neodash", "nivo", "category10", "accent", "dark2", "paired", "pastel1", "pastel2", "set1", "set2", "set3"]
const colorSchemes = ["nivo", "category10", "accent", "dark2", "paired", "pastel1", "pastel2", "set1", "set2", "set3"]
const layerTypes = ["markers", "heatmap"];
const orientations = ["horizontal", "vertical"];
const linkBlendModes = ["normal", "multiply", "screen", "overlay", "darken", "lighten", "color-dodge", "color-burn", "hard-light", "soft-light", "difference", "exclusion", "hue", "saturation", "color", "luminosity"]
const positions = ["inside", "outside"];
const graphLayouts = ["force-directed", "tree", "radial"]


////////////notice that: configuration can only add root property can not alter old property's inner property, these may won't effect

export const RenderTypeDesc = {
    [RenderType.SingleValue]: {
        "helperText": "This report will show only the first value of the first row returned.",
        maxRecords: 1,
        settings: {
            "backgroundColor": {
                displayConfig: {
                    type: ShowType.Off, onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "#fafafa"
                    }
                }
            },
            "fontSize": 64,
            "color": {
                displayConfig: {
                    type: ShowType.Off, onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "rgba(0, 0, 0, 0.87)"
                    }
                }
            },
            "textAlign": {
                displayConfig: { type: ShowType.Select },
                selector: fontAlignTypes,
                value: 'left'
            },
            "verticalAlign": {
                displayConfig: { type: ShowType.Select },
                selector: verticalAlignTypes,
                value: 'top'
            },
            "autorun": true,
            description: {
                text: "Enter markdown here..."
            },
        },
        dataGrid: DATA_GRID_TEXT
    },
    [RenderType.ParameterSelect]: {
        label: "Parameter Select",
        helperText: "This report will let users interactively select Cypher parameters that are available globally, in all reports. A parameter can either be a node property, relationship property, or a free text field.",
        disableRefreshRate: true,
        disableCypherParameters: true,
        textOnly: true,
        maxRecords: 100,
        settings: {
            "backgroundColor": {
                displayConfig: {
                    type: ShowType.Off, onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "#fafafa"
                    }
                }
            },
            "suggestionLimit": {
                LABEL: "Value Suggestion Limit",
                displayConfig: { type: ShowType.Number },
                minV: 2,
                maxV: 100,
                step: 1,
                value: 5
            },
            "searchType": {
                LABEL: "Search Type",
                displayConfig: { type: ShowType.Select },
                selector: ["CONTAINS", "STARTS WITH", "ENDS WITH"],
                value: 'CONTAINS'
            },
        },
        dataGrid: DATA_GRID_TEXT
    },
    [RenderType.Code]: {
        "helperText": "Code is a javascript2 can run in the report.",
        settings: {
            "backgroundColor": {
                displayConfig: {
                    type: ShowType.Off, onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "#fafafa"
                    }
                }
            },
            "description": {
                text: "Enter markdown here..."
            }
        },
        dataGrid: DATA_GRID_TEXT
    },
    [RenderType.Markdown]: {
        helperText: "Markdown text specified above will be rendered in the report.",
        inputMode: "markdown",
        textOnly: true, // this makes sure that no query is executed, input of the report gets passed directly to the renderer.
        allowScrolling: true,
        settings: {
            "backgroundColor": {
                displayConfig: {
                    type: ShowType.Off, onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "#fafafa"
                    }
                }
            },
            "description": {
                text: "Enter markdown here..."
            }
        },
        dataGrid: DATA_GRID_TEXT
    },
    [RenderType.Python]: {
        helperText: "Python code run in the report.",
        inputMode: "python",
        textOnly: true, // this makes sure that no query is executed, input of the report gets passed directly to the renderer.
        allowScrolling: true,
        settings: {
            "backgroundColor": {
                displayConfig: {
                    type: ShowType.Off, onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "#fafafa"
                    }
                }
            },
            "description": {
                text: "Enter markdown here..."
            }
        },
        dataGrid: DATA_GRID_TEXT
    },
    [RenderType.IFrame]: {
        "helperText": "iFrame reports let you embed external webpages into your dashboard. Enter an URL in the query box above to embed it as an iFrame.", settings: {},
    },
    [RenderType.RawJSON]: {
        "helperText": "This report will render the raw data returned by Database.", settings: {},
        dataGrid: DATA_GRID_JSON
    },
    [RenderType.Table]: {
        "helperText": "A table will contain all returned data.",
        maxRecords: 1000,
        settings: {
            "backgroundColor": {
                displayConfig: {
                    type: ShowType.Off, onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "#fafafa"
                    }
                }
            },
            "transposed": {
                LABEL: "Transpose Rows & Columns",
                displayConfig: { type: ShowType.Boolean },
                value: false,
            },
            pageSize: 10,
            showSorterTooltip: {
                LABEL: "Show Sorter Tooltip",
                displayConfig: { type: ShowType.Boolean },
                value: true,
            },
        },
    },
    [RenderType.Chart]: {
        "helperText": <div>The report can render <code>Line / Bar / Column / Pie / Scatter / Bubble / Rose / Radar / Timeline / Heatmap / Word Cloud  Chart</code>.</div>,
        maxRecords: 1000,
        settings: {
        },
    },
    [RenderType.Graph]: {
        label: "Graph",
        helperText: "A graph visualization will draw all returned nodes, relationships and paths.",
        selection: {
            "properties": {
                label: "Node Properties",
                type: SELECTION_TYPES.NODE_PROPERTIES
            }
        },
        useNodePropsAsFields: true,
        autoAssignSelectedProperties: true,
        component: "NeoGraphChart",
        maxRecords: 1000,
        // The idea is to match a setting to its dependency, the operator represents the kind of relationship
        // between the different options (EX: if operator is false, then it must be the opposite of the setting it depends on)
        disabledDependency: { relationshipParticleSpeed: { dependsOn: "relationshipParticles", operator: false } },
        settings: {
            "backgroundColor": {
                displayConfig: {
                    type: ShowType.Off, onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "#fafafa"
                    }
                }
            },
            "nodeColorScheme": {
                displayConfig: { type: ShowType.Select },
                selector: nodeColorSchemes,
                value: 'neodash'
            },
            "nodeLabelColor": {
                displayConfig: {
                    type: ShowType.Off, onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "black"
                    }
                }
            },
            "nodeLabelFontSize": {
                LABEL: "Node Label Font Size",
                displayConfig: { type: ShowType.Number },
                minV: 1.5,
                maxV: 50,
                step: 0.5,
                value: 3.5
            },
            "defaultNodeSize": {
                LABEL: "Node Size",
                displayConfig: { type: ShowType.Number },
                minV: 2,
                maxV: 50,
                step: 1,
                value: 2
            },
            "nodeSizeProp": {
                LABEL: "Node Size Property",
                displayConfig: { type: ShowType.Text },
                value: "size"
            },
            "nodeColorProp": {
                LABEL: "Node Color Property",
                displayConfig: { type: ShowType.Text },
                value: "color"
            },
            "defaultRelColor": {
                LABEL: "Relationship Color",
                displayConfig: {
                    type: ShowType.Off, onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "#a0a0a0"
                    }
                }
            },
            "defaultRelWidth": {
                LABEL: "Relationship Width",
                displayConfig: { type: ShowType.Number },
                minV: 1,
                maxV: 50,
                step: 1,
                value: 1
            },
            "relLabelColor": {
                LABEL: "Relationship Label Color",
                displayConfig: {
                    type: ShowType.Off, onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "#a0a0a0"
                    }
                }
            },
            "relLabelFontSize": {
                LABEL: "Relationship Label Font Size",
                displayConfig: { type: ShowType.Number },
                minV: 1,
                maxV: 50,
                step: 0.25,
                value: 2.75
            },
            "relColorProp": {
                LABEL: "Relationship Color Property",
                displayConfig: { type: ShowType.Text },
                value: "color"
            },
            "relWidthProp": {
                LABEL: "Relationship Width Property",
                displayConfig: { type: ShowType.Text },
                value: "width"
            },
            "relationshipParticles": {
                LABEL: "Animated particles on Relationships",
                displayConfig: { type: ShowType.Boolean },
                value: false,
            },
            "relationshipParticleSpeed": {
                LABEL: "Speed of the particle animation",
                displayConfig: { type: ShowType.Number },
                minV: 0.001,
                maxV: 5,
                step: 0.001,
                value: 0.005,
            },
            "layout": {
                LABEL: "Graph Layout (experimental)",
                displayConfig: { type: ShowType.Select },
                selector: graphLayouts,
                value: "force-directed"
            },
            "showPropertiesOnHover": {
                LABEL: "Show pop-up on Hover",
                displayConfig: { type: ShowType.Boolean },
                value: true
            },
            "showPropertiesOnClick": {
                LABEL: "Show properties on Click",
                displayConfig: { type: ShowType.Boolean },
                value: true
            },
            "fixNodeAfterDrag": {
                LABEL: "Fix node positions after Drag",
                displayConfig: { type: ShowType.Boolean },
                value: true
            },
            "drilldownLink": {
                LABEL: "Drilldown Icon Link",
                displayConfig: { type: ShowType.Text },
                value: "http://bloom.neo4j.io"
            },
            "hideSelections": {
                LABEL: "Hide Property Selection",
                displayConfig: { type: ShowType.Boolean },
                value: false,
            },
            "autorun": {
                LABEL: "Auto-run query",
                displayConfig: { type: ShowType.Boolean },
                value: true
            },
            "iconStyle": {
                LABEL: "Icon Style on format { label : url}",
                displayConfig: { type: ShowType.Text },
                value: ""
            },
        }
    },
    [RenderType.Map]: {
        label: "Map",
        helperText: "A map will draw all nodes and relationships with spatial properties.",
        selection: {
            "properties": {
                label: "Node Properties",
                type: SELECTION_TYPES.NODE_PROPERTIES
            }
        },
        useNodePropsAsFields: true,
        component: "NeoMapChart",
        maxRecords: 1000,
        settings: {
            "backgroundColor": {
                displayConfig: {
                    type: ShowType.Off,
                    onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "#fafafa"
                    }
                }
            },
            "layerType": {
                displayConfig: { type: ShowType.Select },
                selector: layerTypes,
                value: 'markers'
            },
            "clusterMarkers": false,
            separateOverlappingMarkers: {
                LABEL: 'Seperate Overlapping Markers',
                displayConfig: { type: ShowType.Boolean },
                value: true
            },
            "nodeColorScheme": {
                displayConfig: { type: ShowType.Select },
                selector: nodeColorSchemes,
                value: 'neodash'
            },
            "defaultNodeSize": {
                LABEL: "Node Marker Size",
                displayConfig: { type: ShowType.Select },
                selector: sizeTypes,
                value: "large"
            },
            "nodeColorProp": {
                LABEL: "Node Color Property",
                displayConfig: { type: ShowType.Text },
                value: "color"
            },
            "defaultRelColor": {
                LABEL: "Relationship Color",
                displayConfig: {
                    type: ShowType.Off, onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "#666"
                    }
                }
            },
            "defaultRelWidth": {
                LABEL: "Relationship Width",
                displayConfig: { type: ShowType.Number },
                minV: 1,
                maxV: 15,
                step: 0.1,
                value: 3.5,
            },
            "relColorProp": {
                LABEL: "Relationship Color Property",
                displayConfig: { type: ShowType.Text },
                value: "color"
            },
            "relWidthProp": {
                LABEL: "Relationship Width Property",
                displayConfig: { type: ShowType.Text },
                value: "width"
            },
            "providerUrl": {
                LABEL: "Map Provider URL",
                displayConfig: { type: ShowType.Text },
                value: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            },
            "intensityProp": {
                LABEL: "Intensity Property (for heatmap)",
                displayConfig: { type: ShowType.Text },
                value: "intensity"
            },
            "hideSelections": {
                LABEL: "Hide Property Selection",
                displayConfig: { type: ShowType.Boolean },
                value: false
            },
            "autorun": {
                LABEL: "Auto-run query",
                displayConfig: { type: ShowType.Boolean },
                value: true
            },
            "description": {
                text: "Enter map here..."
            }
        }
    },
    [RenderType.GaugeChart]: {
        label: 'Gauge Chart',
        component: "NeoGaugeChart",
        helperText: (
            <div>
                A gauge chart expects a single <code>value</code>.
            </div>
        ),
        maxRecords: 1,
        selection: {
            value: {
                label: 'Value',
                type: SELECTION_TYPES.NUMBER,
                key: true,
            },
        },
        withoutFooter: true,
        settings: {
            backgroundColor: {
                displayConfig: {
                    type: ShowType.Off,
                    onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "#fafafa"
                    }
                }
            },
            nrOfLevels: {
                LABEL: 'Number of levels',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 10,
                step: 1,
                value: 3,
            },
            arcsLength: {
                LABEL: 'Comma-separated length of each arc',
                displayConfig: { type: ShowType.Text },
                value: '0.15, 0.55, 0.3',
            },
            arcPadding: {
                LABEL: 'Arc padding',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 1,
                step: 0.01,
                value: 0.02,
            },
            colors: {
                LABEL: 'Comma-separated arc colors',
                displayConfig: { type: ShowType.Text },
                value: '#5BE12C, #F5CD19, #EA4228',
            },
            textColor: {
                LABEL: 'Color of the text',
                displayConfig: { type: ShowType.Text },
                value: 'var(--text-color)',
            },
            animDelay: {
                LABEL: 'Delay in ms before needle animation',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 1000,
                step: 1,
                value: 0,
            },
            animateDuration: {
                LABEL: 'Duration in ms for needle animation',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 5000,
                step: 1,
                value: 2000,
            },
            marginLeft: {
                LABEL: "Margin Left (px)",
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 24,
            },
            marginRight: {
                LABEL: "Margin Right (px)",
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 24,
            },
            "marginTop": {
                LABEL: "Margin Top (px)",
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 24,
            },
            "marginBottom": {
                LABEL: "Margin Bottom (px)",
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 40,
            },
            refreshButtonEnabled: {
                LABEL: 'Refreshable',
                displayConfig: { type: ShowType.Boolean },
                value: false
            },
            fullscreenEnabled: {
                LABEL: 'Fullscreen enabled',
                displayConfig: { type: ShowType.Boolean },
                value: false
            },
            downloadImageEnabled: {
                LABEL: 'Download Image enabled',
                displayConfig: { type: ShowType.Boolean },
                value: false
            },
            autorun: {
                LABEL: 'Auto-run query',
                displayConfig: { type: ShowType.Boolean },
                value: true
            },
            refreshRate: {
                LABEL: 'Refresh rate (seconds)',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 10000,
                step: 1,
                value: 0,
            },
        }
    },
    [RenderType.SunburstChart]: {
        label: 'Sunburst Chart',
        component: "NeoSunburstChart",
        useReturnValuesAsFields: true,
        helperText: (
            <div>
                A Sunburst chart expects two fields: a <code>path</code> (list of strings) and a <code>value</code>.
            </div>
        ),
        selection: {
            index: {
                label: 'Path',
                type: SELECTION_TYPES.LIST,
            },
            value: {
                label: 'Value',
                type: SELECTION_TYPES.NUMBER,
                key: true,
            },
            key: {
                label: 'Inline',
                type: SELECTION_TYPES.LIST,
                optional: true,
            },
        },
        maxRecords: 3000,
        settings: {
            backgroundColor: {
                LABEL: 'Background Color',
                displayConfig: {
                    type: ShowType.Off,
                    onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "#fafafa"
                    }
                }
            },
            enableArcLabels: {
                LABEL: 'Show Values on Arcs',
                displayConfig: { type: ShowType.Boolean },
                value: true
            },
            interactive: {
                LABEL: 'Enable interactivity',
                displayConfig: { type: ShowType.Boolean },
                value: true
            },
            colors: {
                LABEL: 'Color Scheme',
                displayConfig: { type: ShowType.Select },
                selector: colorSchemes,
                value: "set2"
            },
            borderWidth: {
                LABEL: 'Arc border width (px)',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 0,
            },
            marginLeft: {
                LABEL: 'Margin Left (px)',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 24,
            },
            marginRight: {
                LABEL: 'Margin Right (px)',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 24,
            },
            marginTop: {
                LABEL: 'Margin Top (px)',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 24,
            },
            marginBottom: {
                LABEL: 'Margin Bottom (px)',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 40,
            },
            arcLabelsSkipAngle: {
                LABEL: 'Minimum Arc Angle for Label (degrees)',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 10,
            },
            cornerRadius: {
                LABEL: 'Slice Corner Radius',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 3,
            },
            inheritColorFromParent: {
                LABEL: 'Inherit color from parent',
                displayConfig: { type: ShowType.Boolean },
                value: true
            },
            refreshButtonEnabled: {
                LABEL: 'Refreshable',
                displayConfig: { type: ShowType.Boolean },
                value: false
            },
            fullscreenEnabled: {
                LABEL: 'Fullscreen enabled',
                displayConfig: { type: ShowType.Boolean },
                value: false,
            },
            downloadImageEnabled: {
                LABEL: 'Download Image enabled',
                displayConfig: { type: ShowType.Boolean },
                value: false,
            },
            autorun: {
                LABEL: 'Auto-run query',
                displayConfig: { type: ShowType.Boolean },
                value: true,
            },
            refreshRate: {
                LABEL: 'Refresh rate (seconds)',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 10000,
                step: 1,
                value: 0,
            },
        },
    },
    [RenderType.CirclePacking]: {
        label: "Circle Packing",
        component: "NeoCirclePackingChart",
        useReturnValuesAsFields: true,
        helperText: <div>A circle packing chart expects two fields: a <code>path</code> (list of strings) and
            a <code>value</code>.</div>,
        selection: {
            "index": {
                label: "Path",
                type: SELECTION_TYPES.LIST
            },
            "value": {
                label: "Value",
                type: SELECTION_TYPES.NUMBER,
                key: true
            },
            "key": {
                label: "Inline",
                type: SELECTION_TYPES.LIST,
                optional: true
            }
        },
        maxRecords: 3000,
        settings: {
            "backgroundColor": {
                displayConfig: {
                    type: ShowType.Off,
                    onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "#fafafa"
                    }
                }
            },
            "interactive": {
                LABEL: "Enable interactivity",
                displayConfig: { type: ShowType.Boolean },
                value: true
            },
            "colors": {
                LABEL: "Color Scheme",
                displayConfig: { type: ShowType.Select },
                selector: colorSchemes,
                value: "set2"
            },
            "showLabels": {
                LABEL: "Show Circle Labels",
                displayConfig: { type: ShowType.Boolean },
                value: true
            },
            "borderWidth": {
                LABEL: "Circle border width (px)",
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 20,
                step: 1,
                value: 0
            },
            "marginLeft": {
                LABEL: "Margin Left (px)",
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 24,
            },
            "marginRight": {
                LABEL: "Margin Right (px)",
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 24,
            },
            "marginTop": {
                LABEL: "Margin Top (px)",
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 24,
            },
            "marginBottom": {
                LABEL: "Margin Bottom (px)",
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 40,
            },
            "autorun": {
                LABEL: "Auto-run query",
                displayConfig: { type: ShowType.Boolean },
                value: true
            },
        }
    },
    [RenderType.Treemap]: {
        label: 'Treemap',
        component: "NeoTreeMapChart",
        useReturnValuesAsFields: true,
        helperText: (
            <div>
                A Tree Map chart expects two fields: a <code>path</code> (list of strings) and a <code>value</code>.
            </div>
        ),
        selection: {
            index: {
                label: 'Path',
                type: SELECTION_TYPES.LIST,
            },
            value: {
                label: 'Value',
                type: SELECTION_TYPES.NUMBER,
                key: true,
            },
            key: {
                label: 'Inline',
                type: SELECTION_TYPES.LIST,
                optional: true,
            },
        },
        maxRecords: 3000,
        settings: {
            backgroundColor: {
                LABEL: 'Background Color',
                displayConfig: {
                    type: ShowType.Off,
                    onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "#fafafa"
                    }
                }
            },
            interactive: {
                LABEL: 'Enable interactivity',
                displayConfig: { type: ShowType.Boolean },
                value: true
            },
            colors: {
                LABEL: 'Color Scheme',
                displayConfig: { type: ShowType.Select },
                selector: colorSchemes,
                value: "set2"
            },
            borderWidth: {
                LABEL: 'Rectangle border width (px)',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 0,
            },
            marginLeft: {
                LABEL: 'Margin Left (px)',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 24,
            },
            marginRight: {
                LABEL: 'Margin Right (px)',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 24,
            },
            marginTop: {
                LABEL: 'Margin Top (px)',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 24,
            },
            marginBottom: {
                LABEL: 'Margin Bottom (px)',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 40,
            },
            refreshButtonEnabled: {
                LABEL: 'Refreshable',
                displayConfig: { type: ShowType.Boolean },
                value: false
            },
            fullscreenEnabled: {
                LABEL: 'Fullscreen enabled',
                displayConfig: { type: ShowType.Boolean },
                value: false,
            },
            downloadImageEnabled: {
                LABEL: 'Download Image enabled',
                displayConfig: { type: ShowType.Boolean },
                value: false,
            },
            autorun: {
                LABEL: 'Auto-run query',
                displayConfig: { type: ShowType.Boolean },
                value: true,
            },
            refreshRate: {
                LABEL: 'Refresh rate (seconds)',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 10000,
                step: 1,
                value: 0,
            },
        }
    },
    [RenderType.SankeyChart]: {
        label: "Sankey Chart",
        useNodePropsAsFields: true,
        autoAssignSelectedProperties: true,
        ignoreLabelColors: true,
        helperText: <div>A Sankey chart expects Neo4j <code>nodes</code> and <code>weighted relationships</code>.</div>,
        selection: {
            "nodeProperties": {
                label: "Node Properties",
                type: SELECTION_TYPES.NODE_PROPERTIES
            }
        },
        maxRecords: 250,
        settings: {
            "backgroundColor": {
                displayConfig: {
                    type: ShowType.Off,
                    onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "#fafafa"
                    }
                }
            },
            "legend": {
                LABEL: "Show legend",
                displayConfig: { type: ShowType.Boolean },
                value: false
            },
            "interactive": {
                LABEL: "Enable interactivity",
                displayConfig: { type: ShowType.Boolean },
                value: true
            },
            "layout": {
                LABEL: "Sankey layout",
                displayConfig: { type: ShowType.Select },
                selector: orientations,
                value: "horizontal"
            },
            "colors": {
                LABEL: "Color Scheme",
                displayConfig: { type: ShowType.Select },
                selector: colorSchemes,
                value: "set2"
            },
            "labelPosition": {
                LABEL: "Control sankey label position",
                displayConfig: { type: ShowType.Select },
                selector: positions,
                value: "inside"
            },
            "labelOrientation": {
                LABEL: "Control sankey label orientation",
                displayConfig: { type: ShowType.Select },
                selector: orientations,
                value: 'horizontal'
            },
            "nodeBorderWidth": {
                LABEL: "Node border width (px)",
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 40,
                step: 1,
                value: 0
            },
            "marginLeft": {
                LABEL: "Margin Left (px)",
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 24,
            },
            "marginRight": {
                LABEL: "Margin Right (px)",
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 24,
            },
            "marginTop": {
                LABEL: "Margin Top (px)",
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 24,
            },
            "marginBottom": {
                LABEL: "Margin Bottom (px)",
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 40,
            },
            "labelProperty": {
                LABEL: "Relationship value Property",
                displayConfig: { type: ShowType.Text },
                value: "value"
            },
            "nodeThickness": {
                LABEL: "Node thickness (px)",
                displayConfig: { type: ShowType.Number },
                minV: 5,
                maxV: 50,
                step: 1,
                value: 18,
            },
            "nodeSpacing": {
                LABEL: "Spacing between nodes at an identical level (px)",
                displayConfig: { type: ShowType.Number },
                minV: 5,
                maxV: 50,
                step: 1,
                value: 18,
            },
            "linkBlendMode": {
                LABEL: "Link Blend Mode",
                displayConfig: { type: ShowType.Select },
                selector: linkBlendModes,
                value: 'exclusion'
            },
            "autorun": {
                LABEL: "Auto-run query",
                displayConfig: { type: ShowType.Boolean },
                value: true
            },
        }
    },
    [RenderType.ChoroplethMap]: {
        label: "Choropleth Map",
        useReturnValuesAsFields: true,
        helperText: <div>A Choropleth Map chart expects two fields: a <code>country code</code> (three-letter code) and
            a <code>value</code>.</div>,
        selection: {
            "index": {
                label: "Code",
                type: SELECTION_TYPES.TEXT
            },
            "value": {
                label: "Value",
                type: SELECTION_TYPES.NUMBER,
                key: true

            },
            "key": {
                label: "code",
                type: SELECTION_TYPES.TEXT,
                optional: true
            }
        },
        maxRecords: 300,
        settings: {
            "backgroundColor": {
                displayConfig: {
                    type: ShowType.Off,
                    onValue: {
                        displayConfig: { type: ShowType.Color },
                        value: "#fafafa"
                    }
                }
            },
            "interactive": {
                LABEL: "Enable Interactivity",
                displayConfig: { type: ShowType.Boolean },
                value: true
            },
            "legend": {
                LABEL: "Show Legend",
                displayConfig: { type: ShowType.Boolean },
                value: true
            },
            "colors": {
                LABEL: "Color Scheme",
                displayConfig: { type: ShowType.Select },
                selector: ["nivo", "BrBG", "RdYlGn", "YlOrRd", "greens"],
                value: "nivo"
            },
            "borderWidth": {
                LABEL: "Polygon Border Width (px)",
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 20,
                step: 1,
                value: 0
            },
            "marginLeft": {
                LABEL: "Margin Left (px)",
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 24
            },
            "marginRight": {
                LABEL: "Margin Right (px)",
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 24
            },
            "marginTop": {
                LABEL: "Margin Top (px)",
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 24
            },
            "marginBottom": {
                LABEL: "Margin Bottom (px)",
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 100,
                step: 1,
                value: 40
            },
            "autorun": {
                LABEL: "Auto-run Query",
                displayConfig: { type: ShowType.Boolean },
                value: true
            },
            "projectionScale": {
                LABEL: "Projection Scale",
                displayConfig: { type: ShowType.Number },
                minV: 50,
                maxV: 1000,
                step: 1,
                value: 100
            },
            "projectionTranslationX": {
                LABEL: "Projection X translation",
                displayConfig: { type: ShowType.Number },
                minV: -5,
                maxV: 5,
                step: 0.1,
                value: 0.5
            },
            "projectionTranslationY": {
                LABEL: "Projection Y translation",
                displayConfig: { type: ShowType.Number },
                minV: -5,
                maxV: 5,
                step: 0.1,
                value: 0.5
            },
            "labelProperty": {
                LABEL: "Tooltip Property",
                displayConfig: { type: ShowType.Text },
                value: "properties.name"
            },
        }
    },
    [RenderType.AreaMap]: {
        label: 'Area Map',
        helperText: (
            <div>
                An Area Map expects two fields: a <code>country code / region code</code> (three-letter code) and a
                <code>value</code>.
            </div>
        ),
        useReturnValuesAsFields: true,
        maxRecords: 300,
        component: "NeoAreaMapChart",
        selection: {
            index: {
                label: 'Code',
                type: SELECTION_TYPES.TEXT,
            },
            value: {
                label: 'Value',
                type: SELECTION_TYPES.NUMBER,
                key: true,
            },
        },
        settings: {
            providerUrl: {
                LABEL: 'Map Provider URL',
                displayConfig: { type: ShowType.Text },
                value: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            },
            colors: {
                LABEL: "Color Scheme",
                displayConfig: { type: ShowType.Select },
                selector: ["nivo", "BrBG", "RdYlGn", "YlOrRd", "greens"],
                value: "nivo"
            },
            countryCodeFormat: {
                LABEL: 'Country Code Format',
                displayConfig: { type: ShowType.Select },
                selector: ['Alpha-2', 'Alpha-3'],
                value: 'Alpha-2',
            },
            showLegend: {
                LABEL: 'Color Legend',
                displayConfig: { type: ShowType.Boolean },
                value: true,
            },
            mapDrillDown: {
                LABEL: 'Drilldown Enabled',
                displayConfig: { type: ShowType.Boolean },
                value: false,
            },
            refreshButtonEnabled: {
                LABEL: 'Refreshable',
                displayConfig: { type: ShowType.Boolean },
                value: false,
            },
            fullscreenEnabled: {
                LABEL: 'Fullscreen enabled',
                displayConfig: { type: ShowType.Boolean },
                value: false,
            },
            downloadImageEnabled: {
                LABEL: 'Download Image enabled',
                displayConfig: { type: ShowType.Boolean },
                value: false,
            },
            autorun: {
                LABEL: 'Auto-run query',
                displayConfig: { type: ShowType.Boolean },
                value: true,
            },
            refreshRate: {
                LABEL: 'Refresh rate (seconds)',
                displayConfig: { type: ShowType.Number },
                minV: 0,
                maxV: 10000,
                step: 1,
                value: 0,
            },
        },
    },
    [RenderType.Radar]: {
        label: "Radar Chart",
        useReturnValuesAsFields: true,
        helperText: <div>A radar chart expects two advanced configurations: a <code>Quantitative Variables</code> and
            an <code>Index Property</code>.</div>,
        selection: {
            "index": {
                label: "Index",
                type: SELECTION_TYPES.TEXT,
                key: true
            },
            "values": {
                label: "Value",
                type: SELECTION_TYPES.NUMBER,
                multiple: true,
                key: true
            },
        },
        maxRecords: 250,
        settings: radarDefault
    }
}

export const reD_Template = {
    dbTable: true,
    query: "",
    /**@type{string} */
    selectedTable: undefined,
    filters: [{}],
    selectedColumns: [],
    sorts: [{}],
    slice: { to: 100, from: 0 },
};
export const neo_Template = {
    /**@type{string} */
    query: undefined,
    selection: undefined,
    /**Define a numeric 'Relationship Property' in the report's advanced settings to view the sankey diagram. */
    labelProperty: undefined,
    /**prameter select use */
    /**@type {string} */selectionType: "",
    /**@type {string} */nodeLabel: "",
    /**@type {string} */propertyName: "",
    /**@type {number} */propertyNum: undefined,
    /**@type {string} */propertyValue: "",
    maxRecords: undefined,
};
export const mongo_Template = {
    collectionName: "",
    filter: {},
    filterTmp: {},
    filterStr: "{}",
    projection: {},
    projectionStr: "{}",
    column: [],
    lowerBound: "min",
    upperBound: "max",
    isObjectId: true,
    limit: 1000,
    sort: {},
    sortStr: "{}",
    skip: 0,
}
export const attrSelects = [
    { label: "All attributes", value: "ALL_ATTRIBUTES" },
    { label: "All projected attributes", value: "ALL_PROJECTED_ATTRIBUTES" },
    { label: "Specific attributes", value: "SPECIFIC_ATTRIBUTES" },
    { label: "Count", value: "COUNT" }
]
export const dynamo_Template = {
    tab: 0,
    scanFilterTmp: [{}],
    scanFilterStr: "{}",
    TableName: "",
    IndexName: "",
    Select: attrSelects[0].value,
    AttributesToGet: [],
    ConditionalOperator: "AND",
    ScanFilter: {},
    Limit: 1000,
}
export const awsRegions = `us-east-2
us-east-1
us-west-1
us-west-2
af-south-1
ap-east-1
ap-south-2
ap-southeast-3
ap-southeast-4
ap-south-1
ap-northeast-3
ap-northeast-2
ap-southeast-1
ap-southeast-2
ap-northeast-1
ca-central-1
eu-central-1
eu-west-1
eu-west-2
eu-south-1
eu-west-3
eu-south-2
eu-north-1
eu-central-2
me-south-1
me-central-1
sa-east-1
us-gov-east-1
us-gov-west-1
`.trim().split("\n");