import React from 'react';
import PropTypes from "prop-types";
import { Anchor, Row } from 'antd';
import Xarrow, { useXarrow, Xwrapper } from 'react-xarrows';
import { AnchorType } from 'util/utils';
import { swStyle } from './util/utils';
const { Link } = Anchor;
const STROKE_WIDTH = 1.5;
const CURVENESS = 0;
const SHOW_HEAD = true;
const HEAD_SHAPE = 'arrow1';
const HEAD_SIZE = 6;
const OUT_COLOR = 'var(--active-color)'//'#6636b4';
const IN_COLOR = 'var(--hover-shadow)'//'#1c1c1c';
const TARGET_COLOR = 'var(--target-color)'//'#1c1c1c';
const ArrowComponent = ({ id, ...props }) => {
    // const updateXarrow = useXarrow()
    return <span id={id} className="ant-anchor-link-title-before-hide" style={{ left: `${props.left}px` }} ></span>
}

/**
 * minimap of Page
 */
export default class Anchors extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        anchors: PropTypes.array,
        editorKey: PropTypes.string,
    };
    static defaultProps = {};

    constructor(props) {
        super(props);
        this.state = {
            className: this.props.className,
            hide: true,
            visible: false,
            onlyHeader: true,
            /**@type {[]} */
            anchors: this.props.anchors,
            startPoints: [],
            endPoints: [],
            currentPoint: undefined,
        }
    }

    componentDidMount() {
    }

    componentDidUpdate(preProps, preState) {
        if (preProps.anchors !== this.props.anchors) {
            this.setState({
                anchors: this.props.anchors,
                startPoints: [],
                endPoints: [],
                currentPoint: undefined,
            })
        }
    }

    focus(anchor, notSkipSameId = false, stateTmp) {
        let state = stateTmp || {};
        let { args, type, className, value, cacheNames } = anchor;
        const { currentPoint, anchors } = this.state;
        let id = `xarrow-compo-${className}`;
        if (notSkipSameId || currentPoint !== id) {
            let _tmp_startPoints = [],
                _tmp_endPoints = [],
                _tmp_currentPoint = id;
            if (type === AnchorType.Variant) {
                _.each(anchors, (anchor2, index) => {
                    if (anchor2.className != anchor.className && anchor2.args && anchor2.args.length) {
                        _.each(cacheNames, (cacheName) => {
                            if (anchor2.args && ~anchor2.args.indexOf(cacheName)) {
                                _tmp_endPoints.push(`xarrow-compo-${anchor2.className}`)
                                return false;
                            }
                        })
                    }
                })
            }
            _.each(anchors, (anchor2, index) => {
                if (anchor2.className != anchor.className && anchor2.cacheNames && anchor2.cacheNames.length) {
                    _.each(args, (arg) => {
                        if (~anchor2.cacheNames.indexOf(arg)) {
                            _tmp_startPoints.push(`xarrow-compo-${anchor2.className}`)
                            return false;
                        }
                    })
                }
            })
            _.assign(state, { startPoints: _tmp_startPoints, endPoints: _tmp_endPoints, currentPoint: _tmp_currentPoint });
            !stateTmp && this.setState(state);
        }
    }


    render() {
        const { editorKey } = this.props;
        const { anchors, visible, startPoints, endPoints, currentPoint, hide, onlyHeader } = this.state;
        const viewAnchors = onlyHeader ? _.filter(anchors, (a) => a.header > 0) : anchors
        return <div>
            <div className="d-flex justify-content-end normal-icon" title={decodeURI([`Click to ${hide ? "open" : "close"} minimap%0A`, hide ? "" : `DbClick to ${onlyHeader ? "detailed view" : "outline"}`].join(""))}>
                <i onDoubleClick={(e) => {
                    this.setState((state) => { return { onlyHeader: !state.onlyHeader } });
                }} onClick={(e) => { this.setState((state) => { return { hide: !state.hide } }) }}
                    className={`icon icon-xs ${hide ? "icon-inactive" : ""}`} dangerouslySetInnerHTML={{ __html: `<svg width="16" height="16" viewBox="0 0 16 16" fill="currentColor" fill-rule="evenodd" clip-rule="evenodd"><path d="M3 1.75C2.30964 1.75 1.75 2.30964 1.75 3C1.75 3.69036 2.30964 4.25 3 4.25C3.69036 4.25 4.25 3.69036 4.25 3C4.25 2.30964 3.69036 1.75 3 1.75ZM0.25 3C0.25 1.48122 1.48122 0.25 3 0.25C4.51878 0.25 5.75 1.48122 5.75 3C5.75 4.51878 4.51878 5.75 3 5.75C1.48122 5.75 0.25 4.51878 0.25 3Z"></path><path d="M13 11.75C12.3096 11.75 11.75 12.3096 11.75 13C11.75 13.6904 12.3096 14.25 13 14.25C13.6904 14.25 14.25 13.6904 14.25 13C14.25 12.3096 13.6904 11.75 13 11.75ZM10.25 13C10.25 11.4812 11.4812 10.25 13 10.25C14.5188 10.25 15.75 11.4812 15.75 13C15.75 14.5188 14.5188 15.75 13 15.75C11.4812 15.75 10.25 14.5188 10.25 13Z"></path><path d="M10 12C9.44771 12 9 11.5523 9 11V5C9 3.34315 7.65685 2 6 2H5V4H6C6.55228 4 7 4.44772 7 5V11C7 12.6569 8.34315 14 10 14H11V12H10Z"></path></svg>` }} />
            </div>
            <div className={`editorjs-anchor-box ${hide ? "hide" : ""}`}
                onMouseEnter={() => {
                    this.setState({ visible: true })
                }}
                onMouseLeave={() => {
                    this.setState({ visible: false })
                }}>
                <Anchor style={{ maxHeight: "calc(100vh - 55px - 41px - 38px - 55px)" }} targetOffset={window.innerHeight / 2}
                    onChange={(currentActiveLink) => {
                        let anchor = document.querySelector(`.editorjs-wrapper.${editorKey}>.editorjs-anchor a[href='${currentActiveLink}']`);
                        anchor && anchor.scrollIntoView({ behavior: "auto", block: "center", inline: "nearest" })
                    }}>
                    <Xwrapper>
                        {_.map(viewAnchors.length > 0 ? viewAnchors : [{ className: `editorjs-anchor-empty-id`, value: `Empty ${onlyHeader ? "outline" : "detailed view"}.`, type: AnchorType.Header, header: 2 }], (anchor, index) => {
                            let { type, className, value, header, headerContent } = anchor;
                            let id = `xarrow-compo-${className}`;
                            let _out = startPoints.indexOf(id)
                            let _in = endPoints.indexOf(id)
                            let style = `${~_out && '-out' || ~_in && '-in' || currentPoint === id && '-target'}`
                            return <Link key={className}
                                href={`#${className.substring("editorjs-anchor-".length)}`} title={<div
                                    title={value}
                                    link-data={className}
                                    className={`anchor-title-${type.toLowerCase()} d-flex justify-content-between`}
                                    onClick={(e) => {
                                        if (type === AnchorType.Header) {

                                        } else {
                                            let data = document.querySelector(`.editorjs-wrapper.${editorKey}>.editorjs .${className}`);
                                            if (data && data.closest(".code-edit")) {
                                                data.closest(".code-edit").editCode(true);
                                            } else {
                                                data && data.parentNode && swStyle(data.parentNode);
                                            }
                                        }
                                        // console.log(args);
                                        // this.focus(anchor);
                                        e.preventDefault();
                                        // e.stopPropagation();
                                    }}>
                                    <span className={`ant-anchor-link-title-before-${type.toLowerCase()} ant-anchor-link-title-before ant-anchor-link-title-before${style}`} id={id}>
                                        <ArrowComponent id={`${id}-out`} left={10}></ArrowComponent>
                                        <ArrowComponent id={`${id}-in`} left={16}></ArrowComponent>
                                    </span>
                                    <span className={`ant-anchor-link-title-text`}
                                        style={header > 0 ? {
                                            fontSize: `${0.75 + (6 - header) * 0.15}rem`,
                                            fontWeight: "bold",
                                        } : {}}  >{visible ? headerContent || value : ""}&nbsp;</span>
                                </div>} />
                        })}
                        {!!currentPoint && !hide && <Xarrow curveness={CURVENESS}
                            lineColor={TARGET_COLOR}
                            strokeWidth={STROKE_WIDTH} path={'straight'} showHead={false} start={`${currentPoint}`} end={endPoints.length ? `${currentPoint}-in` : `${currentPoint}-out`} />}
                        {!!startPoints.length && !hide && _.map(startPoints, (startPoint, index) => {
                            return <Xarrow key={startPoint}
                                curveness={CURVENESS}
                                lineColor={OUT_COLOR}
                                headColor={OUT_COLOR}
                                strokeWidth={STROKE_WIDTH} path={'straight'} showHead={SHOW_HEAD} headShape={HEAD_SHAPE} headSize={HEAD_SIZE} start={`${startPoint}`} end={`${startPoint}-out`} />
                        })}
                        {!!startPoints.length && !hide && _.map(startPoints, (startPoint, index) => {
                            return <Xarrow key={startPoint}
                                curveness={CURVENESS}
                                lineColor={OUT_COLOR}
                                headColor={OUT_COLOR}
                                strokeWidth={STROKE_WIDTH} path={'straight'} showHead={SHOW_HEAD} headShape={HEAD_SHAPE} headSize={HEAD_SIZE} start={`${startPoint}-out`} end={`${currentPoint}-out`} />
                        })}
                        {!!endPoints.length && !hide && _.map(endPoints, (endPoint, index) => {
                            return <Xarrow key={endPoint}
                                curveness={CURVENESS}
                                lineColor={IN_COLOR}
                                headColor={IN_COLOR}
                                strokeWidth={STROKE_WIDTH} path={'straight'} showHead={SHOW_HEAD} headShape={HEAD_SHAPE} headSize={HEAD_SIZE} start={`${endPoint}-in`} end={`${endPoint}`} />
                        })}
                        {!!endPoints.length && !hide && _.map(endPoints, (endPoint, index) => {
                            return <Xarrow key={endPoint}
                                curveness={CURVENESS}
                                lineColor={IN_COLOR}
                                headColor={IN_COLOR}
                                strokeWidth={STROKE_WIDTH} path={'straight'} showHead={SHOW_HEAD} headShape={HEAD_SHAPE} headSize={HEAD_SIZE} start={`${currentPoint}-in`} end={`${endPoint}-in`} />
                        })}
                    </Xwrapper>
                </Anchor>
            </div>
        </div>
    }
}