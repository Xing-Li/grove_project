
export default class Edge{
    constructor(source,target){
        this.sourceId = null;
        this.targetId = null;
        this.source = source;
        this.target = target;
        this.properties={}
    }

    cloneEdge(edge){
        this.sourceId = edge.sourceId
        this.targetId = edge.targetId
        this.properties = edge.properties
        this.relationship = edge.name
    }

}