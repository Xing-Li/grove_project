//hint data
window.globalScope = Object.assign({}, window, Object.assign({ await: "await", async: "async", Promise: "Promise", viewof: "viewof", }));
window.moduleScope = {};
/**
 * 
 * @param {Object} obj 
 */
export const addAllGlobalScope = function (obj) {
    if (obj instanceof Object) {
        _.map(obj, (v, k) => {
            addGlobalScope(k, v);
        })
    }
}
/**
 * 
 * @param {string} key 
 * @param {*} value 
 */
export const addGlobalScope = function (key, value) {
    if (undefined === window.globalScope[key] || window.globalScope[key] !== value) {
        window.globalScope[key] = value;
    }
}

export const removeGlobalScope = function (key, value) {
    if (undefined !== window.globalScope[key] && (undefined === value || window.globalScope[key] === value)) {
        delete window.globalScope[key];
    }
}



/**
 * @param {string} moduleName
 * @param {string} key 
 * @param {*} value 
 */
export const addModuleScope = function (moduleName, key, value) {
    if (undefined === window.moduleScope[moduleName]) {
        window.moduleScope[moduleName] = {};
    }
    let scope = window.moduleScope[moduleName];
    if (undefined === scope[key] || scope[key] !== value) {
        scope[key] = value;
    }
    window.tagEditors && _.each(window.tagEditors, (tagEditor) => {
        if (~tagEditor.moduleNames.indexOf(moduleName) && tagEditor.sscope) {
            tagEditor.sscope[key] = value;
        }
    })
    window.editor && ~window.editor.moduleNames.indexOf(moduleName) && window.editor.sscope && (window.editor.sscope[key] = value);
}

export const removeModuleScope = function (moduleName, key, value) {
    if (undefined === window.moduleScope[moduleName]) {
        return;
    }
    let scope = window.moduleScope[moduleName];
    if (undefined !== scope[key] && (undefined === value || scope[key] === value)) {
        delete scope[key];
    }
    window.tagEditors && _.each(window.tagEditors, (tagEditor) => {
        if (~tagEditor.moduleNames.indexOf(moduleName) && tagEditor.sscope && tagEditor.sscope[key] && value === tagEditor.sscope[key]) {
            delete tagEditor.sscope[key];
        }
    })
    window.editor && ~window.editor.moduleNames.indexOf(moduleName) && window.editor.sscope && window.editor.sscope[key] && (value === window.editor.sscope[key]) && (delete window.editor.sscope[key]);
}