import html from "@observablehq/stdlib/src/html";
import { isArray, isNumber, isObject, isString } from "lodash";

export const makeEl = (name, className) => {
  const el = html`<${name}></${name}>`
  if (className) el.className = className;
  return el;
}

export const parse = (args, componentOptions) => {
  const children = [];
  const props = {};

  const wrapChild = (child) => {
    if (componentOptions.children) {
      if (componentOptions.children.map(x => x.nodeName).includes(child.nodeName.toLowerCase())) {
        return child;
      } else {
        return componentOptions.children[0](child)
      }
    } else {
      return child;
    }
  }

  const pushChild = child => children.push(wrapChild(child))

  args.forEach(prop => {
    if (isArray(prop)) {
      // Assume array of elements
      prop.forEach(pushChild)
    } else if (prop.nodeType) {
      pushChild(prop)
    } else if (isString(prop)) {
      pushChild(html`${prop}`)
    } else if (isNumber(prop)) {
      pushChild(html`${prop}`)
    } else if (isObject(prop)) {
      Object.assign(props, prop)
    } else {
      pushChild(html`${String(prop)}`)
    }
  })

  return { children, props }
};

export const groveComponent = (renderOrOptions) => {
  const renderFn = typeof renderOrOptions === 'function' ? renderOrOptions : typeof renderOrOptions === 'object' ? renderOrOptions.render : undefined;
  const options = typeof renderOrOptions === 'object' ? renderOrOptions : {};
  const componentFn = (...args) => {
    const el = options.name ? makeEl(options.name, args.className) : undefined;
    const { children, props } = parse(args, options)
    if (renderFn) {
      if (props.children) children.push(...props.children)
      return renderFn({ ...props, children, el, })
    } else {
      if (props.className) el.className += ' ' + props.className
      if (options.className) el.className += ' ' + options.className
      children.forEach(child => el.appendChild(child))
      return el;
    }
  };
  componentFn.nodeName = options.name ?? 'GroveComponent'
  return componentFn;
}