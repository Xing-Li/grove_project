const _ = require('lodash');  
// const clients = {};
let logSocketInstance = null;

let LogSocket = function (io) {
    logSocketInstance = io.of('/log')
        .on("connection", function (socket) {

            console.info("Log SocketID:" + socket.id);
            // clients[socket.id] = socket;
            socket.itemsCount = 0;

            socket.on('disconnect', function () {
                console.info('a user disconnected, will remove client:' + socket.id);
                // delete clients[socket.id];
            });

        });

 
}


let log = function (type, message) {
     if(logSocketInstance){
        message = String(message)
         if(process.env['NODE_ENV'] ==='production' && message.length > 100 ){
            message = message.slice(0, 100);
         }
          logSocketInstance.sockets.forEach(socket => {
          socket && socket.connected && socket.emit('log', {
                type,
                message
            })
        })
     }
}

module.exports = LogSocket;
module.exports.log = log;