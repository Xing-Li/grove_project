import {
    AutoComplete, Button, Card, Checkbox, Collapse, Input, Pagination, Select, Space, Table, Tag, Tooltip
} from 'antd';
import {
    CheckCircleOutlined,
    ClockCircleOutlined,
    CloseCircleOutlined,
    ExclamationCircleOutlined,
    MinusCircleOutlined,
    SearchOutlined,
    SyncOutlined, WarningOutlined,
} from '@ant-design/icons';
import _ from 'lodash';
import React, { useEffect, useRef, useState } from "react";
import 'codemirror';
import CodeMirror from 'codemirror';
import { Controlled } from 'react-codemirror2';
import showHint, { cursorActivityFunc } from 'util/CodeMirror.showHint.js';
import { assembling, calcAutoType, extractAllKeys, highligthComp, nanOrBlank, stringifyNanOrBlank } from 'util/helpers';
import { linkKeys, code_editor_keys, cell_shortcuts_keys, CategoryFromType, CodeMode, getCodeModePlaceholder, isFailedResults, castType, EmptyResults, ModalType, DatabaseType } from 'util/utils';
import AntChartSetting from "../AntChartSetting";
import { groupSeparators } from 'util/chartConfigUtil';
import { countExtractDatas, ChartTypeTemplate } from './OptionsTypes';
import RenderObject from './RenderObject';
import { getRendererForValue, rendererForType, RenderSubValue } from '../util/ReportRecordProcessing';
import { RenderType } from 'util/databaseApi';
import { JoinComp, withPopMenu, sqliteDatasToChartDatas, ResetTable, filterDatasFunc, MAX_COLUMN_WIDTH } from 'block/code/CodeTools';
import { index } from 'd3';
import { valueIsNode, valueIsPath, valueIsRelationship } from '../util/ChartUtils';
const moment = require('moment');
const { Record, Node, Relationship, Path } = require('neo4j-driver');

function ApplyColumnType(column, value) {
    const renderer = getRendererForValue(value);
    const columnProperties = (renderer ? { type: renderer.type, renderCell: renderer.renderValue } : rendererForType["string"]);
    if (columnProperties) {
        column = { ...column, ...columnProperties }
    }
    return column;
}

export const ClientStatus = {
    "success": "success",
    "processing": "processing",
    "error": "error",
    "warning": "warning",
    "waiting": "waiting",
    "stop": "stop",
}

export function TagComp(props = { status: "stop", desc: "" }) {
    const { status, desc } = props;
    switch (status) {
        case "success":
            return <Tag icon={<CheckCircleOutlined />} color="success" title={desc}>{"success"}
            </Tag>;
        case "processing":
            return <Tag icon={<SyncOutlined spin />} color="processing" title={desc}>{"processing"}
            </Tag>
        case "error":
            return <Tag icon={<CloseCircleOutlined />} color="error" title={desc}>{"error"}
            </Tag>
        case "warning":
            return <Tag icon={<ExclamationCircleOutlined />} color="warning" title={desc}>{"warning"}
            </Tag>
        case "waiting":
            return <Tag icon={<ClockCircleOutlined />} color="blue" title={desc}>{"waiting"}
            </Tag>
        default:
            return <Tag icon={<MinusCircleOutlined />} color="default" title={desc}>{"stop"}
            </Tag>
    }
}
/**
 * Neo4j records to column datas
 * 
 * @param {boolean} transposed 
 * @param {*} records 
 * @param {boolean} addKey 
 */
export function extractColumnDatas(transposed, records, addKey = false, json = false) {
    if (!records.length) {
        return { columns: [], rows: [] }
    }
    const generateSafeColumnKey = (key) => {
        return key;
    }
    const columns = (transposed) ? ["Field"].concat(records.map((r, j) => "Value" + (j == 0 ? "" : " " + (j + 1).toString()))).map((key, i) => {
        const value = key;
        return ApplyColumnType({
            key: generateSafeColumnKey(key),
            title: generateSafeColumnKey(key),
            dataIndex: generateSafeColumnKey(key)
        }, value)
    }) : records[0].keys.map((key, i) => {
        const value = records[0]._fields[records[0]._fieldLookup[key]] //records[0].get(key);
        return ApplyColumnType({
            key: generateSafeColumnKey(key),
            title: generateSafeColumnKey(key),
            dataIndex: generateSafeColumnKey(key)
        }, value)
    });

    const rows = (transposed) ?
        records[0].keys.map((key, i) => {
            return Object.assign(
                addKey ? { key: i, Field: key } : { Field: key },
                ...records.map((r, j) => ({ ["Value" + (j == 0 ? "" : " " + (j + 1).toString())]: RenderSubValue(r._fields[i]) })));
        }) : records.map((record, rownumber) => {
            return Object.assign(
                addKey ? { key: rownumber } : {},
                ...record._fields.map((field, i) => ({
                    [generateSafeColumnKey(record.keys[i])]: typeof field === 'object' && field ? (json && (field instanceof Node || valueIsNode(field) || field instanceof Relationship || valueIsRelationship(field) || valueIsPath(field)) ? JSON.stringify(
                        valueIsPath(field) ? (_.each(_.pick(field.segments[0], ["start", "relationship", "end"]), (v, i) => v && _.assign(v, {
                            properties: _.reduce(v.properties ? v.properties : {}, (prev, v, k) => {
                                prev[k] = v && typeof v === 'object' ? v.toString() : v;
                                return prev;
                            }, {})
                        })), field) : _.assign(field, {
                            properties: _.reduce(field.properties, (prev, v, k) => {
                                prev[k] = v && typeof v === 'object' ? v.toString() : v;
                                return prev;
                            }, {})
                        })
                    ) : field.toString()) : field
                }))); ``
        });
    return { columns, rows };
}
/**
 * 
 * @param {{result: {records:Record}, saveFunc:()=>{}}} props 
 */
export const TableUIComp = (props = { selectedCategoryFrom: CategoryFromType.database, result: {}, backgroundColor: "#fafafa", transposed: false, pageSize: 10, saveFunc, }) => {
    const { result, editor, selectedCategoryFrom, selectedCategory, selectedClientType, transposed, showSorterTooltip, pageSize, backgroundColor, saveFunc } = props;
    if (!result) {
        return <div>Not Table Data!</div>
    }
    const [backgroundColorValue, setBackgroundColorValue] = useState(backgroundColor);
    useEffect(() => {
        setBackgroundColorValue(backgroundColor);
    }, [backgroundColor || "#fafafa"]);
    const [pageSizeValue, setPageSizeValue] = useState(pageSize);
    useEffect(() => {
        setPageSizeValue(pageSize);
    }, [pageSize || 10]);
    const [transposedValue, setTransposedValue] = useState(transposed);
    useEffect(() => {
        setTransposedValue(transposed);
    }, [transposed || false]);
    const [open, setOpen] = useState(true);
    const [hoverRow, setHoverRow] = useState(null);
    const [startEndMap, setStartEndMap] = useState({});
    const [filteredInfo, setFilteredInfo] = useState({});
    const [sortedInfo, setSortedInfo] = useState([]);
    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
    };
    const handleReset = () => {
        cache.clearFilters && cache.clearFilters();
        setFilteredInfo({});
        setSortedInfo([]);
    };
    const cache = {
        clearFilters: undefined,
    }
    const searchInput = useRef(null);
    if ((selectedCategoryFrom === CategoryFromType.neo4j || selectedCategoryFrom === CategoryFromType.client && selectedClientType === DatabaseType.Neo4j) && !(result instanceof Array)) {
        if (typeof result !== 'object' || result.error) {
            return <div>Not Table Data! {typeof result !== 'object' ? "not object result!" : JSON.stringify(result.error)}</div>
        }
        if (!result.records || !result.records.length) {
            return <div>0 Rows.</div>
        }
        const { columns, rows } = extractColumnDatas(transposedValue, result.records, true, true);
        const chartDatas = sqliteDatasToChartDatas(rows);
        const dataTypes = calcAutoType(rows, true, false);
        let multiple = columns.length;
        columns.length && (_.each(columns, (ncolumn, index) => {
            let property = ncolumn.title;
            let chartData = chartDatas[property];
            let Comp = undefined === chartData ? (props) => { } : chartData.ele;
            if (dataTypes[property].type === 'number') _.assign(ncolumn, { sorter: { compare: (a, b) => a[property] - b[property], multiple: multiple-- } })
            else if (dataTypes[property].type === 'string') _.assign(ncolumn, { sorter: { compare: (a, b) => a[property] > b[property] ? 1 : -1, multiple: multiple-- } })
            _.assign(ncolumn, {
                title: <div>{
                    <div className="text-primary">
                        {index === 0 && <span onClick={(e) => { setOpen(!open); e.stopPropagation(); }} className={`icon fas fa-chevron-${open ? "down" : "right"}`}></span>}
                        <span>{property}</span>
                    </div>}
                    {<div className={!open ? "hide" : ""}><Comp startEnd={startEndMap[property]} setFilterValues={(props) => {
                        let startEndMapTmp = _.cloneDeep(startEndMap);
                        if (props) {
                            startEndMapTmp[property] = props;
                        } else {
                            delete startEndMapTmp[property];
                        }
                        setStartEndMap(startEndMapTmp)
                    }} hoverRow={hoverRow} ></Comp></div>}
                    {index === 0 && result.notice && <Tooltip title={result.notice}> <WarningOutlined style={{ color: 'orange', fontSize: '1.5rem' }} /> </Tooltip>}
                </div>,
                className: startEndMap[property] ? "bg-washed-yellow" : "",
                render: (text) => {
                    if (nanOrBlank(text)) {
                        return <span className="text-muted">{stringifyNanOrBlank(text)}</span>
                    }
                    if (typeof text === 'string' && text.startsWith("{") && text.endsWith("}")) {
                        let inspectItem = JSON.parse(text);
                        if ((inspectItem.labels || inspectItem.type) && inspectItem.properties) {
                            return <Tooltip title={<Card className='grid-drag-cancel' bodyStyle={{ padding: "0px" }} bordered={false}>
                                <b style={{ padding: "10px" }}>
                                    {inspectItem.labels ? (inspectItem.labels.length > 0 ? inspectItem.labels.join(", ") : "Node") : inspectItem.type}
                                </b>
                                <RenderObject obj={inspectItem.properties} rows={true} cellLen={30} pagination={false}></RenderObject>
                            </Card>}>
                                <Button type="primary" shape="round" onClick={() => {
                                    window.currentEditor.react_component.setState({
                                        modalType: ModalType.NeoGraphItemInspect,
                                        modalData: {
                                            title: (inspectItem.labels && inspectItem.labels.join(", ")) || inspectItem.type,
                                            object: inspectItem.properties
                                        }
                                    });
                                }}>{inspectItem.labels || inspectItem.type}</Button>
                            </Tooltip>
                        } else if (valueIsPath(inspectItem)) {
                            return <div className='d-flex'>{_.map(_.pick(inspectItem.segments[0], ["start", "relationship", "end"]), (item, k) => <Tooltip key={k} title={<Card className='grid-drag-cancel' bodyStyle={{ padding: "0px" }} bordered={false}>
                                <b style={{ padding: "10px" }}>
                                    {item.labels ? (item.labels.length > 0 ? item.labels.join(", ") : "Node") : item.type}
                                </b>
                                <RenderObject obj={item.properties} rows={true} cellLen={30} pagination={false}></RenderObject>
                            </Card>}>
                                <Button type="primary" shape="round" onClick={() => {
                                    window.currentEditor.react_component.setState({
                                        modalType: ModalType.NeoGraphItemInspect,
                                        modalData: {
                                            title: (item.labels && item.labels.join(", ")) || item.type,
                                            object: item.properties
                                        }
                                    });
                                }}>{item.labels || item.type}</Button>
                            </Tooltip>)}</div>
                        } else {
                            return text;
                        }
                    } else {
                        return (text).toString()
                    }
                },
                ellipsis: true
            });
        }));
        let datas = filterDatasFunc(startEndMap, rows);
        return <Table size='small' style={{ backgroundColor: backgroundColorValue }} showSorterTooltip={false} sticky scroll={{ x: '90%' }} columns={columns} dataSource={datas} onChange={(pagination) => {
            setPageSizeValue(pagination.pageSize)
        }} pagination={datas.length > 5 && { size: 'small', pageSize: pageSizeValue || 5, pageSizeOptions: _.reduce([5, 10, 20, 50, 100, 200, 500, 1000], (prev, curr) => { if (datas.length >= curr) { prev.push(curr); } return prev; }, []), showTotal: (total, range) => `${range[0]}-${range[1]} of ${total}` }} />
    } else {
        if (!(result instanceof Array)) {
            return <div>Not Table Data!</div>
        }
        let keys = extractAllKeys(result);
        const chartDatas = sqliteDatasToChartDatas(result);
        const dataTypes = calcAutoType(result, true, false);
        let multiple = keys.length;
        let columns = _.reduce(keys, (prev, property, index) => {
            let chartData = chartDatas[property];
            let Comp = undefined === chartData ? (props) => { } : chartData.ele;
            let ncolumn = {
                key: property,
                title: <div>{
                    <div className="text-primary">
                        {index === 0 && <span onClick={(e) => { setOpen(!open); e.stopPropagation(); }} className={`icon fas fa-chevron-${open ? "down" : "right"}`}></span>}
                        <span>{property}</span>
                    </div>}
                    {<div className={!open ? "hide" : ""}><Comp startEnd={startEndMap[property]} setFilterValues={(props) => {
                        let startEndMapTmp = _.cloneDeep(startEndMap);
                        if (props) {
                            startEndMapTmp[property] = props;
                        } else {
                            delete startEndMapTmp[property];
                        }
                        setStartEndMap(startEndMapTmp)
                    }} hoverRow={hoverRow} ></Comp></div>}
                </div>,
                className: startEndMap[property] ? "bg-washed-yellow" : "",
                dataIndex: property,
                render: (text) => {
                    if (nanOrBlank(text)) {
                        return <span className="text-muted">{stringifyNanOrBlank(text)}</span>
                    }
                    return (text).toString()
                },
                ellipsis: true,
            };
            if (dataTypes[property].type === 'number') _.assign(ncolumn, { sorter: { compare: (a, b) => a[property] - b[property], multiple: multiple-- } })
            else if (dataTypes[property].type === 'string') _.assign(ncolumn, { sorter: { compare: (a, b) => a[property] > b[property] ? 1 : -1, multiple: multiple-- } })
            prev.push(ncolumn);
            return prev;
        }, []);
        const data = !~keys.indexOf("key") ? _.reduce(result, (prev, obj, index) => {
            prev.push({ key: index, ...obj });
            return prev;
        }, []) : result;
        let datas = filterDatasFunc(startEndMap, data);
        return <ResetTable reset={!_.isEmpty(startEndMap) || !_.isEmpty(filteredInfo) || !_.isEmpty(sortedInfo)} resetFunc={() => {
            setStartEndMap({});
            handleReset();
        }} saveFunc={saveFunc && (() => {
            saveFunc(startEndMap, filteredInfo, sortedInfo);
            setStartEndMap({});
            handleReset()
        })}>
            <Table style={{ backgroundColor: backgroundColorValue }} size="small" showSorterTooltip={false} sticky scroll={{ x: '90%' }} columns={columns} dataSource={datas} onRow={record => {
                return {
                    onMouseEnter: event => { open && setHoverRow(record) }, //
                    onMouseLeave: event => { open && setHoverRow(null) },
                };
            }} onChange={(pagination) => {
                setPageSizeValue(pagination.pageSize)
            }} pagination={datas.length > 5 && { size: 'small', pageSize: pageSizeValue || 5, pageSizeOptions: _.reduce([5, 10, 20, 50, 100, 200, 500, 1000], (prev, curr) => { if (datas.length >= curr) { prev.push(curr); } return prev; }, []), showTotal: (total, range) => `${range[0]}-${range[1]} of ${total}` }} />
        </ResetTable>
    }
}

/**
 * 
 * @param {{ renderType: "", result: undefined, onChange: (cm, data, value) => { }, run: () => { }, className:"" }} props 
 */
export const RawJSONComp = (props) => {
    const { result, renderType, onChange, onBlur, run, className } = props;
    let value;
    let options;
    if (renderType === RenderType.IFrame) {
        value = result;
        options = { mode: CodeMode.htmlmixed, placeholder: "Type Http Addrress." }
    } else if (renderType === CodeMode.sql) {
        value = result;
        options = { mode: CodeMode.sql, placeholder: getCodeModePlaceholder(CodeMode.sql) }
    } else if (renderType === RenderType.Code) {
        value = result;
        options = { mode: CodeMode.javascript2, placeholder: getCodeModePlaceholder(CodeMode.javascript2) }
    } else if (renderType === RenderType.Markdown) {
        value = result;
        options = { mode: CodeMode.markdown, placeholder: getCodeModePlaceholder(CodeMode.markdown) }
    } else if (renderType === RenderType.Python) {
        value = result;
        options = { mode: CodeMode.python, placeholder: getCodeModePlaceholder(CodeMode.python) }
    } else if (renderType === RenderType.RawJSON) {
        value = result;
        options = { mode: { name: CodeMode.javascript, json: true }, placeholder: "Type JSON." };
    } else {
        return <div>not support {renderType} now.</div>
    }
    const [cmvalue, setCmvalue] = useState(value);
    useEffect(() => {
        setCmvalue(value);
    }, [value]);
    let ccm;
    return <Controlled className={`code-edit-cm ${className || ""}`}
        editorDidMount={(cm, value, cb) => {
            ccm = cm;
        }}
        onFocus={(cm) => {
        }}
        value={cmvalue}
        onBeforeChange={onChange || ((cm, data, value) => {
            setCmvalue(value);
        })}
        onKeyUp={(cm, e) => {
            if (!(e.ctrlKey || e.metaKey) && !e.altKey && (/^[a-z$._]$/ig).test(e.key)) {
                showHint(cm, CodeMirror.hint.javascript, { "globalScope": window.currentEditor && window.currentEditor.sscope || window });
            }
        }}
        onCursorActivity={(cm) => { cursorActivityFunc.call(cm, cm) }}
        onBlur={(cm, e) => { onBlur && onBlur(cm); }}
        options={_.assign({
            theme: window.editor.getSettings().getTheme(),
            lineNumbers: true,
            lineWrapping: true,
            // viewportMargin: Infinity,
            // styleSelectedText: true,
            // matchBrackets: false,
            // styleActiveLine: false,
            // nonEmpty: false,
            foldGutter: true,
            // placeholder: "",
            gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
            readOnly: false,
            extraKeys: assembling([{
                key: linkKeys(code_editor_keys["Full Screen"]), value: function (cm) {
                    cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                }
            }, {
                key: "Esc", value: (cm) => {
                    if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                }
            }, {
                key: linkKeys(cell_shortcuts_keys["Force Run Current Cell"]), value: (cm) => {
                    run && run(ccm);
                }
            }, {
                key: linkKeys(cell_shortcuts_keys["Run Current Cell"]), value: (cm) => {
                    run && run(ccm);
                }
            }, {
                key: linkKeys(code_editor_keys["Show Editor LineNumbers"]), value: (cm) => {
                    cm.setOption("lineNumbers", !cm.getOption("lineNumbers"));
                }
            }, {
                key: linkKeys(code_editor_keys["Fold Code"]), value: function (cm) { cm.foldCode(cm.getCursor()); },
            }])
        }, options)}
    />
}

export const TurnPageRawJSONComp = (props) => {
    const { result, ...propsa } = props;
    if (!result) {
        return <RawJSONComp {...propsa} result={"Not have results!"}></RawJSONComp>
    }
    if (result instanceof Array && result.length > 100) {
        const { className, ...left } = propsa;
        const [pageSize, setPageSize] = useState(50);
        const [current, setCurrent] = useState(1);
        return <div>
            <RawJSONComp className={`limit-height ${className || ""}`} {...propsa} result={JSON.stringify(result.slice((current - 1) * pageSize, current * pageSize), undefined, 2)}></RawJSONComp>
            <Pagination className='d-flex justify-content-end'
                showSizeChanger
                onChange={(page, npageSize) => {
                    setCurrent(page);
                    setPageSize(npageSize);
                }}
                showTotal={(total, range) => `${range[0]}-${range[1]} of ${total}`}
                current={current}
                pageSize={pageSize}
                total={result.length}
            />
        </div>

    }
    return <RawJSONComp {...propsa} result={JSON.stringify(result, undefined, 2)}></RawJSONComp>;

}

/**
 * 
 * @param {{settings:AntChartSetting, saveFunc:()=>{}}} props 
 * @returns 
 */
const TableComp = (props) => {
    const { saveFunc, settings } = props;
    const { chartConfig: { selectedProperties, chartData }, properties, columns, data } = settings.state;
    let emptyFills = settings.state.chartConfig.emptyFills || {};
    let castColumns = settings.state.chartConfig.castColumns || {};
    let joinColumns = settings.state.chartConfig.joinColumns || {};
    let countMayIgnoreDatas = selectedProperties.length ? chartData.length - countExtractDatas(chartData, selectedProperties) : 0;
    if (isFailedResults(data)) {
        return <div className="text-danger">{JSON.stringify(data)}</div>
    }
    if (!data || undefined === data.length) {
        return <div>No Data.</div>
    }
    if (0 === data.length) {
        return <div>No results.</div>
    }
    const joinProperties = _.reduce(joinColumns, (prev, v, k) => { if (k && v && v instanceof Array && v.length > 0) { prev.push(k) } return prev; }, [])
    let _hasBlanks = [];
    const nData = _.map(data, (obj, index) => {
        let nobj = _.cloneDeep(obj);
        _.each(columns, (column) => {
            if (nanOrBlank(obj[column.key])) {
                if (!~_hasBlanks.indexOf(column.key)) {
                    _hasBlanks.push(column.key);
                }
                if (!nanOrBlank(emptyFills[column.key])) {
                    nobj[column.key] = emptyFills[column.key];
                }
            }
            if (castColumns && castColumns[column.key]) {
                nobj[column.key] = castType(nobj[column.key], castColumns[column.key]);
            }
        })
        joinColumns && _.each(joinColumns, (v, k) => {
            if (v && v instanceof Array && v.length > 0) {
                nobj[k] = _.values(_.pick(nobj, v)).join(groupSeparators[0]);
            }
        })
        if (!nobj.key) {
            nobj.key = index;
        }
        return nobj
    })
    const dataTypes = calcAutoType(nData, true, false);
    const chartDatas = sqliteDatasToChartDatas(nData);
    const [editProperty, setEditProperty] = useState(undefined);
    const [propertyValue, setPropertyValue] = useState(undefined);
    const [open, setOpen] = useState(false);
    const [hoverRow, setHoverRow] = useState(null);
    const [startEndMap, setStartEndMap] = useState({});
    const [filteredInfo, setFilteredInfo] = useState({});
    const [sortedInfo, setSortedInfo] = useState([]);
    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
    };
    const handleReset = () => {
        cache.clearFilters && cache.clearFilters();
        setFilteredInfo({});
        setSortedInfo([]);
    };
    const cache = {
        clearFilters: undefined,
    }
    const searchInput = useRef(null);
    const getColumnSearchProps = (dataIndex) => {
        return {
            filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters, close }) => {
                cache.clearFilters = clearFilters;
                return <div style={{ padding: 8, }} onKeyDown={(e) => e.stopPropagation()}>
                    <Input ref={searchInput} placeholder={`Search ${dataIndex}`} value={selectedKeys[0]}
                        onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                        onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        style={{ marginBottom: 8, display: 'block', }}
                    />
                    <Space>
                        <Button type="primary" onClick={() => handleSearch(selectedKeys, confirm, dataIndex)} icon={<SearchOutlined />} size="small">
                            Search
                        </Button>
                        <Button onClick={() => handleReset()} size="small">
                            Reset
                        </Button>
                    </Space>
                </div>
            },
            filterIcon: (filtered) => (
                <SearchOutlined
                    style={{
                        color: filtered ? '#1890ff' : undefined,
                    }}
                />
            ),
            onFilter: (value, record) => {
                let text = record[dataIndex];
                if (nanOrBlank(text)) {
                    return stringifyNanOrBlank(text) === value
                }
                return (text).toString().includes(value)
            },
            onFilterDropdownOpenChange: (visible) => {
                if (visible) {
                    setTimeout(() => searchInput.current?.select(), 100);
                }
            },
            render: (text) => {
                if (nanOrBlank(text)) {
                    return <span className="text-muted">{stringifyNanOrBlank(text)}</span>
                }
                if (~[EmptyResults].indexOf(nData)) {
                    return <span>{text}</span>
                }
                if (isFailedResults(nData)) {
                    return <span className="text-danger">{text}</span>
                }
                let fts = _.filter(filteredInfo, (v, k) => { return v && k === dataIndex });
                if (fts.length === 1) {
                    return highligthComp(filteredInfo[dataIndex][0], (text).toString(), false);
                } else {
                    return (text).toString()
                }
            },
        }
    };
    let multiple = properties.length;
    let reduceFunc = (prev, property, index) => {
        let chartData = chartDatas[property];
        let Comp = undefined === chartData ? (props) => { } : chartData.ele;
        let width = !open && property.length * 14 < MAX_COLUMN_WIDTH ? property.length * 14 : MAX_COLUMN_WIDTH;
        let sorted = _.filter(sortedInfo, (v, index) => { return v.columnKey === property });
        let ncolumn = {
            key: `pa_${property}`,
            title: !!~joinProperties.indexOf(property) ? <i className="icon fas fa-check-square"></i> : <Checkbox value={property} checked={!!~selectedProperties.indexOf(property)} onClick={(e) => { e.stopPropagation(); }}></Checkbox>,
            children: [
                {
                    key: `${property}`,
                    title: <div>{
                        <div className="text-primary">
                            {index === 0 && <span onClick={(e) => { setOpen(!open); e.stopPropagation(); }} className={`icon fas fa-chevron-${open ? "down" : "right"}`}></span>}
                            <span>{property}</span>
                        </div>}
                        {<div className={!open ? "hide" : ""}><Comp startEnd={startEndMap[property]} setFilterValues={(props) => {
                            let startEndMapTmp = _.cloneDeep(startEndMap);
                            if (props) {
                                startEndMapTmp[property] = props;
                            } else {
                                delete startEndMapTmp[property];
                            }
                            setStartEndMap(startEndMapTmp)
                        }} hoverRow={hoverRow} ></Comp></div>}
                    </div>,
                    className: startEndMap[property] ? "bg-washed-yellow" : "",
                    dataIndex: `${property}`,
                    width: width,
                    sortOrder: sorted.length === 1 && sorted[0].columnKey === property ? sorted[0].order : null,
                    ellipsis: true,
                    ...getColumnSearchProps(property),
                }],
            dataIndex: `pa_${property}`,
            width: width,
        };
        if (nData.length) {
            if (dataTypes[property].type === 'number') _.assign(ncolumn.children[0], { sorter: { compare: (a, b) => a[property] - b[property], multiple: multiple-- } })
            else if (dataTypes[property].type === 'string') _.assign(ncolumn.children[0], { sorter: { compare: (a, b) => a[property] > b[property] ? 1 : -1, multiple: multiple-- } })
        }
        prev.push(ncolumn);
        return prev;
    };
    let selectedColumns = _.reduce([...selectedProperties, ...joinProperties], (prev, property, index) => {
        reduceFunc(prev, property, index);
        prev[prev.length - 1].children[0].fixed = 'left';
        return prev;
    }, []);
    let noSelectedProperties = _.filter(properties, (v) => { return !~selectedProperties.indexOf(v) });
    _.reduce(noSelectedProperties, (prev, property, index) => {
        reduceFunc(prev, property, index);
        return prev;
    }, selectedColumns)
    const onCheckAllChange = e => {
        settings.onRowSelectionChanged(e.target.checked ? properties : [], undefined, emptyFills, castColumns, joinColumns, { type: ChartTypeTemplate.type });
    };
    let datas = filterDatasFunc(startEndMap, nData);
    return <React.Fragment>
        <div className="d-flex align-items-center">
            {<Checkbox indeterminate={properties.length !== selectedProperties.length}
                onChange={onCheckAllChange}
                checked={properties.length === selectedProperties.length}>
                Check all
            </Checkbox>} &nbsp;&nbsp;&nbsp;&nbsp; <span>Rows:{nData.length} Cols: {columns.length}.&nbsp;&nbsp;&nbsp;{"Please select the appropriate fields (no more than 4)"}</span>
            {withPopMenu(JoinComp, {
                icon: <i className="icon fas fa-plus-square"></i>,
                label: (
                    <span>
                        <span className="button-label">Join Columns&nbsp;&nbsp;</span>
                        {_.map(joinColumns, (v, k) => {
                            return <Tag key={k} closable title={v.join(groupSeparators[0])} onClose={(e) => {
                                delete joinColumns[k];
                                settings.onRowSelectionChanged(selectedProperties, undefined, emptyFills, castColumns, joinColumns, { type: ChartTypeTemplate.type });
                            }}>
                                {k}
                            </Tag>
                        })}
                    </span>
                ),
                columns: _.reduce(properties, (prev, property, index) => {
                    prev.push({
                        label: property,
                        value: property,
                    })
                    return prev;
                }, []),
                joinColumns: joinColumns,
                setJoinColumns: (joinColumns) => {
                    settings.onRowSelectionChanged(selectedProperties, undefined, emptyFills, castColumns, joinColumns, { type: ChartTypeTemplate.type });
                },
            })}
        </div>
        <Checkbox.Group style={{ maxWidth: "100%" }} value={selectedProperties} onChange={(checkedValues) => {
            // console.log('checked = ', checkedValues);
            settings.onRowSelectionChanged(checkedValues, undefined, emptyFills, castColumns, joinColumns, { type: ChartTypeTemplate.type });
        }}>
            <ResetTable reset={!_.isEmpty(startEndMap) || !_.isEmpty(filteredInfo) || !_.isEmpty(sortedInfo)} resetFunc={() => {
                setStartEndMap({});
                handleReset();
            }} saveFunc={saveFunc && (() => {
                saveFunc(startEndMap, filteredInfo, sortedInfo);
                setStartEndMap({});
                handleReset()
            })}> <Table className='table-hover-on-ellipsis' onChange={(pagination, filters, sorter, extra) => {
                setFilteredInfo(_.filter(filters, (v, k) => { return v }).length ? filters : {});
                setSortedInfo(sorter.length ? sorter : (sorter.columnKey && sorter.order ? [sorter] : []));
                // console.log("params", pagination, filters, sorter, extra);
            }} size="small" showSorterTooltip={false} sticky scroll={{ x: '90%' }} columns={selectedColumns} dataSource={datas} onRow={record => {
                return {
                    onMouseEnter: event => { open && setHoverRow(record) }, //
                    onMouseLeave: event => { open && setHoverRow(null) },
                };
            }} pagination={(!_.isEmpty(startEndMap) || !_.isEmpty(filteredInfo) || !_.isEmpty(sortedInfo) || datas.length > 5) && {
                size: 'small', pageSizeOptions: _.reduce([5, 10, 20, 50, 100, 200, 500, 1000], (prev, curr) => { if (datas.length >= curr) { prev.push(curr); } return prev; }, []),
                defaultPageSize: 5, showTotal: (total, range) => `${range[0]}-${range[1]} of ${total}`
            }}
                summary={(dataN) => {
                    return <Table.Summary>
                        {!!_hasBlanks.length && <Table.Summary.Row>
                            <Table.Summary.Cell key={"Handle empty cells"} index={0} colSpan={columns.length}>
                                {!!(selectedProperties.length && (countMayIgnoreDatas)) &&
                                    <span className="text-warning">{countMayIgnoreDatas} rows maybe ignore for having one or more empty cells.</span>}
                                You can fill empty cells with text.
                            </Table.Summary.Cell>
                        </Table.Summary.Row>}
                        {!!_hasBlanks.length && <Table.Summary.Row>
                            {_.map([...selectedProperties,
                            ...joinProperties, ...noSelectedProperties], (property, index) => {
                                let width = (!open && property.length * 14 < MAX_COLUMN_WIDTH ? property.length * 14 : MAX_COLUMN_WIDTH) - 16;
                                let dataType = dataTypes[property].type;
                                let options;
                                switch (dataType) {
                                    case 'boolean':
                                        options = [{ value: 'false', label: "false" }, { value: "none", label: "none" }]
                                        break;
                                    case 'number':
                                        options = [{ value: '0', label: "0" }, { value: 'NaN', label: "NaN" }, { value: "none", label: "none" }]
                                        break;
                                    case 'string':
                                        options = [{ value: '', label: "empty string" }, { value: "none", label: "none" }]
                                        break;
                                    case 'date':
                                        options = [{ value: moment(new Date()).format("YYYY-MM-DD"), label: moment(new Date()).format("YYYY-MM-DD") }, { value: "none", label: "none" }]
                                        break;
                                    case 'null':
                                        options = [{ value: '', label: "empty string" }, { value: "none", label: "none" }]
                                        break;
                                    default:
                                        options = [{ value: '', label: "empty string" }, { value: '0', label: "0" },
                                        { value: 'false', label: "false" }, { value: "none", label: "none" }];
                                        break;
                                }
                                return <Table.Summary.Cell key={property} index={index} colSpan={1}>
                                    {!!~_hasBlanks.indexOf(property) && <AutoComplete allowClear onChange={(value) => {
                                        if ("none" === value) {
                                            delete emptyFills[property];
                                        } else {
                                            emptyFills[property] = value;
                                        }
                                        settings.onRowSelectionChanged(selectedProperties, undefined, emptyFills, castColumns, joinColumns, { type: ChartTypeTemplate.type });
                                    }} value={undefined === emptyFills[property] ? "none" : emptyFills[property]}
                                        style={{ width: `100%` }}
                                        placeholder=""
                                        options={options}
                                    />}
                                </Table.Summary.Cell>
                            })}
                        </Table.Summary.Row>}
                        <Table.Summary.Row>
                            <Table.Summary.Cell key={"Handle cast columns"} index={0} colSpan={columns.length}>
                                <div className="d-flex align-items-center" >
                                    Cast columns data type.
                                    {!!editProperty && <>
                                        <span className="text-primary">{`${editProperty}: `}</span>
                                        <RawJSONComp className="limit-height" renderType={RenderType.RawJSON} result={propertyValue || ""}
                                            onChange={(cm, d, value) => {
                                                setPropertyValue(value);
                                            }}></RawJSONComp>
                                        <Button onClick={() => {
                                            try {
                                                _.each((nData), (obj, index) => {
                                                    castType(obj[editProperty], propertyValue);
                                                })
                                                castColumns[editProperty] = propertyValue;
                                                settings.onRowSelectionChanged(selectedProperties, undefined, emptyFills, castColumns, joinColumns, { type: ChartTypeTemplate.type });
                                            } catch (e) {
                                                console.error(e);
                                            }
                                        }}>Apply</Button>
                                        <Button onClick={() => {
                                            setEditProperty(undefined)
                                        }}>Close</Button></>}
                                </div>
                            </Table.Summary.Cell>
                        </Table.Summary.Row>
                        {!!_.keys(dataTypes).length && <Table.Summary.Row>
                            {_.map([...selectedProperties, ...joinProperties, ...noSelectedProperties], (property, index) => {
                                let width = (!open && property.length * 14 < MAX_COLUMN_WIDTH ? property.length * 14 : MAX_COLUMN_WIDTH) - 16;
                                let dataType = dataTypes[property].type;
                                if (!dataType) {
                                    return;
                                }
                                let options;
                                switch (dataType) {
                                    case 'number':
                                        options = [{ value: 'string', label: "string" }, { value: 'date', label: "date" }, { value: "none", label: dataType }]
                                        break;
                                    case 'string':
                                        options = [{ value: 'date', label: "date" }, { value: 'number', label: "number" }, { value: "none", label: dataType }]
                                        break;
                                    case 'date':
                                        options = [{ value: 'string', label: "string" }, { value: 'number', label: "number" }, { value: "none", label: dataType }]
                                        break;
                                    case 'boolean':
                                        options = [{ value: 'string', label: "string" }, { value: 'number', label: "number" }, { value: "none", label: dataType }]
                                        break;
                                    default:
                                        options = [];
                                        break;
                                }
                                return <Table.Summary.Cell key={property} index={index} colSpan={1}>
                                    <div className="hover-appear d-flex align-items-center normal-icon" style={{ width: `100%` }}>
                                        {!editProperty && !!options.length ? <Select allowClear onChange={(value) => {
                                            if ("none" === value) {
                                                delete castColumns[property];
                                            } else {
                                                castColumns[property] = value;
                                            }
                                            settings.onRowSelectionChanged(selectedProperties, undefined, emptyFills, castColumns, joinColumns, { type: ChartTypeTemplate.type });
                                        }} value={undefined === castColumns[property] ? "none" : castColumns[property]}
                                            style={{ width: `100%` }}
                                            placeholder={dataType}
                                            options={options}
                                        /> : dataType}
                                        <i className={`icon fas fa-pen ${property !== editProperty ? "icon-muted" : ""}`} title="Edit Transfer Function" onClick={() => {
                                            if (property === editProperty) {
                                                return setEditProperty(undefined);
                                            }
                                            setEditProperty(property);
                                            setPropertyValue(castColumns[property] || `(column)=>{
  return column;
}`);
                                        }}></i>
                                    </div>
                                </Table.Summary.Cell>
                            })}
                        </Table.Summary.Row>}
                    </Table.Summary>
                }} />
            </ResetTable>
        </Checkbox.Group>
        {/* <RenderObject obj={dataTypes} table={true}></RenderObject> */}
    </React.Fragment>
}

export default TableComp;