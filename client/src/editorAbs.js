import axios from 'axios';
import _ from 'lodash';
import moment from 'moment';
import ReactDOM from "react-dom";
import React from "react";
import Anchors from './Anchors';
import actions from './define/Actions';
import { v4 as uuidv4, NIL as NIL_UUID } from 'uuid';
import {
    fileSeparator,
    getContent,
    getFileContent, getFileKey,
    getFileName, getFileType, getFolderKey, transferToRealData, verFileTag, wrapContent
} from './file/fileUtils';
import MdFiles from './file/MdFiles';
import doc from './util/toDoc';
import {
    DefaultFileData, DefaultShareData, setItem, getItem, getFileNamesKey,
    getUndefinedKey, getUndefinedContent, ShapeChatInfo, USER_ID, PROJECT_ID,
    ShapeLinkChange, ShapeFileChange, isNotLogin, removeItem,
    DEFAULT_VER, TAG_TYPE, SYSTEM_USER, notMyOwnProject, FILESDIR, FILESDIR_NAME, DefaultOptions, COMMON, COMMON_EDITOR_KEY, setWindowState
} from "./util/localstorage";
import { ShapeElement, ShapeModule, ShapeInspector, ShapeVariable, ShapeEditor } from './util/hqApi';
import { ShapeBlock } from './util/editorjsApi';
import MsgHandler from 'file/MsgHandler';
import RenderTask from './util/renderTask';
import EDITOR_JS_TOOLS from './constants';
import EditorSettings, { DrawerType } from 'editorSettings';
import commonData from 'common/CommonData';
import { cmSearchFunc, cmHighlightFunc } from './util/CodeMirror.showHint'

const { ShareProjectCanOptions } = require("./util/MsgType");
const { escapeStringRegexp } = require('./util/regexUtils');
const { showToast, SETTINGS_NAME, showConfirm, dataURItoBlob, ReadWriteOptions,
    ModalType, ContentType, AnchorType, extractCanvas, canvasImage, extractHeader, saveLink, ReDbDialets } = require('./util/utils');
const { ROOT_URI, runVariables, expireModule, copyShareFile, getAbsoluteUrl,
    getReferRealFileUri, abosolutePath, dispose, createModule, getAndLoadModule, disposeModule, removeModule } = require('./util/hqUtils');
const { AllAcceptArr, isFileEditorJs, transferType, isFileNoEdit, isFileCsv, DEBUG_EDITOR, DEBUG_EDITOR_JS_LEVEL, isVisibleInViewport, highligthComp, maxC, sliceC, SearchType, ImageCache, SelectType } = require('./util/helpers.js');
const { errorDataFile, largeDataFile, DefaultInitData } = require('./block/data');
/**@type {import('html2canvas').default} */
const html2canvas = window.html2canvas;

const EditorJS = window.EditorJS;
export default class EditorAbs {
    constructor(settings) {
        this.renderTask = new RenderTask(this);
        /**@type{import('@editorjs/editorjs').default} */
        this.editorInstance = null;
        this.scrollPos = 0;
        this.moduleNames = [];
        /**@type {ShapeModule} */
        this.main = undefined;
        /**@type {ShapeModule} */
        this.currentMain = undefined;
        /**@type {DefaultFileData} */
        this.fileData = undefined;
        /**@type {DefaultShareData} */
        this.linkData = undefined;
        this.contentType = undefined;
        this.uploadUri = undefined;
        /**@type {MdFiles} */
        this.react_component = undefined;
        this.readOnly = true;
        this.key = undefined;
        this.editorjsKey = undefined;
        this.runComplete = false;
        this.sscope = {};
        this.attaches = new Set();
        /**@type {EditorSettings} */
        this.settings = settings || new EditorSettings();
        this.version = DEFAULT_VER;
        this.option = { data: DefaultInitData };
        this.ready = false;
        this.openSelect = false;
        /** flag if it need refresh editor when reopen the editor */
        this.needRefresh = false;
        this.cachePositions = [];
        this.currentPositionIndex = -1;
        this.searchText = "";
        this.searchConfig = {
            caseSensitive: false,
            wholeWord: false,
            reg: false,
        }
        /** flag if need delete self module's variables */
        this._delSelfModule = true;
        this.initEdit = false;
        this.ref = React.createRef();
    }
    initEditorJs(option) {
        this.option = option
        let holder = document.querySelector(`.editorjs-wrapper.${this.getTransferKey()}>.editorjs`);
        holder.aaa_id = this.getTransferKey();
        this.editorInstance = new EditorJS(_.assign(this.option, {
            logLevel: DEBUG_EDITOR_JS_LEVEL,
            readOnly: this.readOnly,
            holder: holder,
            data: JSON.parse(this.option.data),
            tools: EDITOR_JS_TOOLS(this.getTransferKey()),
            autofocus: true,
            placeholder: 'Type text or Ctrl + Q to select',
            defaultBlock: 'paragraph',
            onReady: () => {
                this.ready = true;
            },
            openSelect: () => {
                return this.openSelect;
            },
            focusBlockChangeFunc: (index) => {
                actions.variable(actions.types.FOCUS_BLOCK_CHANGE, [], () => {
                    return { index };
                });
            },
            menusPopFunc:/**
            * 
            * @param {HTMLDivElement} element 
            */
                (element, position, cb) => {
                    const elementCoords = element.getBoundingClientRect();
                    const left = elementCoords.left + 30;
                    const top = (position === 'up' ? elementCoords.top : elementCoords.height + elementCoords.top) + window.pageYOffset;
                    actions.variable(actions.types.MENUS_POP, [], () => {
                        return { position: { left, top }, cb: cb };
                    });
                },
            /**
             * wrote characters in block
             * @param {*} ele 
             */
            modifiersFunc:
                /**
                 * 
                 * @param {Element} ele 
                 * @param {Node} sourceNode 
                 */
                (ele, sourceNode) => {
                    let contentMutated = false;
                    if (!ele) {
                        ele = sourceNode && sourceNode.parentElement;
                    }
                    if (!ele) {
                        contentMutated = true;
                    } else if (ele.closest(`.ce-code`)) {
                        contentMutated = false;
                    } else if (ele.closest(`.ce-chart`)) {
                        contentMutated = false;
                    } else if (ele.closest(`.ce-plot-chart`)) {
                        contentMutated = false;
                    } else if (ele.closest(`.th-2`)) {
                        contentMutated = false;
                    } else if (ele.tagName === 'PRE' && ele.classList.contains('CodeMirror-line') ||
                        ele.tagName === 'DIV' && ele.classList.contains('CodeMirror-gutter-elt') ||
                        ele.tagName === 'SPAN' && ele.parentElement && ele.parentElement.tagName === 'PRE' && ele.parentElement.classList.contains('CodeMirror-line')) {
                        contentMutated = false;
                    } else {
                        contentMutated = true;
                    }
                    return contentMutated;
                },
            /**
             * attribute of block element modified
             * @param {HTMLElement} ele 
             */
            attrModifiersFunc: (ele) => {
                let contentMutated = false;
                if (!ele) {
                    contentMutated = true;
                } else if (ele.classList.contains(`vis-item`) || ele.classList.contains(`vis-item-content`) || ele.classList.contains(`ce-header`) && ele.tagName.match(/^H(\d)$/)) {//vis-timeline/header update
                    contentMutated = false;
                } else if (ele.closest(`.ce-code`)) {
                    contentMutated = false;
                } else if (ele.closest(`.tc-toolbox`)) {
                    contentMutated = false;
                } else if (ele.closest(`.ce-chart`)) {
                    contentMutated = false;
                } else if (ele.closest(`.ce-plot-chart`)) {
                    contentMutated = false;
                } else if (ele.closest(`.ce-block-border-left`)
                    || ele.closest(`.ce-block-border-right`)) {
                    contentMutated = false;
                } else if (ele.closest(`.th-2`)) {
                    contentMutated = false;
                } else if (ele.tagName === 'PRE' && ele.classList.contains('CodeMirror-line') ||
                    ele.tagName === 'DIV' && ele.classList.contains('CodeMirror-gutter-elt') ||
                    ele.tagName === 'SPAN' && ele.parentElement && ele.parentElement.tagName === 'PRE' && ele.parentElement.classList.contains('CodeMirror-line')) {
                    contentMutated = false;
                } else {
                    contentMutated = true;
                }
                return contentMutated;
            },
            /**
             * before paste insert
             */
            insertPrepare: this.insertPrepare,
            /**
             * config block that can use ctrl+a to select all blocks
             */
            selectionAllBlockNames: [],
            onChange: (methods) => {
                if (!this.readOnly && (!this.fileData && !this.linkData || this.fileData && !isFileNoEdit(this.fileData) || this.linkData) && !this.isLocalEditFile()) {
                    this.setNeedSave(true);
                    if (this.getData()) {
                        if (!(this.linkData && this.linkData.userId === SYSTEM_USER)) {
                            // expireModule((this.getData()).fileKey, this.getUploadUri())
                        }
                    }
                }
            },
        }));
    }
    insertPrepare = (savedData) => {
        let haveCodeTool = false;
        if (savedData) {
            savedData.map((data) => {
                if (data && data.tool && data.tool === "codeTool") {
                    data.data.codeData.dname = uuidv4();
                    haveCodeTool = true;
                }
                if (data && data.tool && data.tool === "plotChart") {
                    data.data.dname = uuidv4();
                    haveCodeTool = true;
                }
                if (data && data.tool && data.tool === "antChart") {
                    let obj = {};
                    let tmp = _.reduce(data.data.chartConfigs, (prev, chartConfig, key) => {
                        obj[chartConfig.chartKey] = uuidv4();
                        chartConfig.chartKey = obj[chartConfig.chartKey];
                        prev[chartConfig.chartKey] = chartConfig;
                        return prev;
                    }, {})
                    data.data.chartConfigs = tmp;
                    _.each(data.data.layouts, (layouts, screenSize) => {
                        _.each(layouts, (layout, index) => {
                            layout.i = obj[layout.i];
                        })
                    });
                    data.data.dname = uuidv4();
                }
            })
        }
        haveCodeTool && (this.runComplete = false);
    }

    setScrollPos(scrollPos) {
        this.scrollPos = scrollPos
    }
    getScrollPos() {
        return this.scrollPos;
    }
    /**
     * record imported module
     * 
     * @param {string} moduleName 
     * @param {string} referFileKey 
     */
    recordModule(moduleName, referFileKey) {
        let moduleNameTmp = this.getModuleName(moduleName, referFileKey)
        if (!moduleNameTmp) {
            return;
        }
        this.moduleNames.push(moduleNameTmp);
    }

    recordSelfModule() {
        this.recordModule((this.getData() || {}).fileKey, this.getUploadUri());
    }

    getSelfModuleName() {
        return this.getModuleName((this.getData() || {}).fileKey, this.getUploadUri());
    }

    getModuleName(moduleName, referFileKey) {
        if (!moduleName) {
            return;
        }
        return referFileKey ? abosolutePath(moduleName, referFileKey) : moduleName
    }

    getModuleNames() {
        return this.moduleNames;
    }
    /**
     * @returns {ShapeModule}
     */
    getMain() {
        return this.currentMain || this.main;
    }
    getFileData() {
        return this.fileData;
    }
    getLinkData() {
        return this.linkData;
    }
    getShared() {
        return this.shared;
    }
    getData() {
        return this.fileData || this.linkData
    }
    getUploadUri() {
        return this.uploadUri;
    }
    /**
     * @returns get editorJs
     */
    get() {
        return this.editorInstance;
    }
    getFileDataTitle() {
        if (this.editorInstance && this.editorInstance.blocks) {
            let block = this.editorInstance.blocks.getBlockByIndex(0);
            if (block.name === 'codeTool') {
                /**@type{HTMLElement} */
                let data = block.holder.querySelector(".code-edit-result.display > .data");
                if (data && data.querySelector("h1")) {
                    return data.querySelector("h1").innerText;
                }
            } else if (block.name === "header" && block.holder.querySelector(".ce-header")) {
                return block.holder.querySelector(".ce-header").innerText;
            }
        }
        return ""
    }

    /**
     * 
     * @param {string} moduleName 
     * @returns {boolean}
     */
    isMainModule(moduleName) {
        if (this.getData()) {
            return moduleName === abosolutePath((this.getData()).fileKey, this.getUploadUri());
        }
        return false;
    }
    getKey() {
        return this.key;
    }
    getTransferKey() {
        return this.editorjsKey;
    }
    setNeedSave(needSave) {
        if (this.needSave === needSave) {
            return;
        }
        this.needSave = needSave;
        this.needSaveTimeInSecond = new Date().getTime();
        actions.variable(actions.types.NEED_SAVE_FILES, [actions.types.NEED_SAVE_FILES], (needSaveFiles) => {
            if (!needSaveFiles) {
                needSaveFiles = {};
            }
            needSaveFiles[this.getTransferKey()] = this.needSave;
            return needSaveFiles;
        });
    }

    getNeedSaveTimeInSecond() {
        return this.needSaveTimeInSecond;
    }

    getNeedSave() {
        return this.needSave;
    }
    /**
    * set editorJs readOnly state
    * @param {boolean} state 
    */
    async setReadOnly(state) {
        this.runComplete = false;
        dispose(this.getMain());
        this.readOnly = await this.editorInstance.readOnly.toggle(state);
        await new Promise((resolve, reject) => {
            this.renderTask.submitRunOverTask(() => {
                try {
                    // window.currentEditor === this && window.scrollTo(0, this.getScrollPos());
                    this.editorInstance.caret.setToFirstBlock('start', 0);
                    document.querySelector(`.editorjs-wrapper.${this.getTransferKey()} .codex-editor__redactor`)
                        && document.querySelector(`.editorjs-wrapper.${this.getTransferKey()} .codex-editor__redactor`).click();
                    this.focusFolder();
                    resolve()
                } catch (e) {
                    reject(e)
                }
            })
        })
        if (window.currentEditor === this) {
            actions.variable(actions.types.CURRENT_EDITOR_READ_ONLY, [], () => { return window.currentEditor.getReadOnly() })
        }
        return this.readOnly;
    }
    /**
     * @returns {boolean}
     */
    getReadOnly() {
        return this.readOnly;
    }
    getVersion() {
        return this.version;
    }
    /**
     * 
     * @param {File[]} acceptedFiles 
     */
    async onImportFiles(acceptedFiles) {
        if (!this.fileData) {
            return;
        }
        return await this.react_component.onImport(acceptedFiles, getFileKey(this.fileData.fileKey, FILESDIR), false);
    }
    deleteCurrentBlock(e) {
        let api = this.editorInstance;
        if (!api.blocks || api.blocks.getCurrentBlockIndex() === -1) {
            return;
        }
        if (this.readOnly) {
            return;
        }
        if (api.selection.selections()) {
            return;
        }
        showConfirm("delete current block?", () => {
            let block = api.blocks.getBlockByIndex(api.blocks.getCurrentBlockIndex());
            if (block && block.name === 'codeTool' && block.holder) {
                api.blocks.delete(api.blocks.getCurrentBlockIndex());
            } else {
                api.blocks.delete(api.blocks.getCurrentBlockIndex());
            }
        }, undefined, { okType: 'danger', okText: "Delete" });
    }
    currentBlockKeydownFunc(e) {
        let api = this.editorInstance;
        if (!api || !api.blocks || api.blocks.getCurrentBlockIndex() === -1) {
            return;
        }
        if (this.readOnly) {
            return;
        }
        if (api.selection.selections()) {
            return;
        }
        let block = api.blocks.getBlockByIndex(api.blocks.getCurrentBlockIndex());
        block.keydownFunc && block.keydownFunc(e);
    }
    /**
     * 
     * @param {string} fileKey 
     */
    getInfo(fileKey) {
        let params = {};
        if (/^#/.test(fileKey)) {
            fileKey = fileKey.replace(/^#/, "");
        }
        if (/^\%/.test(fileKey)) {
            let match = fileKey.match(/^\%((projects\/){0,1}(\w+)\/)(.*)/)
            if (null !== match && match.length === 5) {
                params.uploadUri = `${ROOT_URI}${match[1]}`;
                fileKey = match[4];
                if (match[2]) {//?projects?
                    params.projectId = match[3];
                    if (params.projectId !== PROJECT_ID) {
                        let options = _.filter(DefaultOptions(), (value, index) => {
                            return value.projectId === params.projectId;
                        })
                        if (options.length) {
                            params.userId = options[0].userId;
                        } else {
                            params.userId = "undefined";
                        }
                    }
                } else {
                    params.projectId = COMMON;
                    params.userId = match[3];
                }
            } else {
                throw new Error('regex was not ok');
            }
        }
        let match = fileKey.match(/(\S+)@(\d+)$/)
        if (null !== match) {
            params.fileKey = match[1];
            params.version = parseInt(match[2]);
        } else {
            params.fileKey = fileKey;
            params.version = 0;
        }
        params.name = getFileName(params.fileKey);
        params.type = getFileType(params.fileKey);
        if (this.contentType === ContentType.File) {
            return this.react_component.getInfo(params);
        } else {
            return _.assign({}, this.linkData, params);
        }
    }
    /**
     * get file content from storage/ImageCache/server
     * @param {string} fileKey
     * @param {string} referFileKey may be is refer file
     * @returns {string|Blob} return null if folder or Blob if image or string or throw exception if fileName not exist in cache or network exception
     */
    async getReferRealFileContent(fileKeyTmp, referFileKey) {
        let { uri, fileKey } = getReferRealFileUri(fileKeyTmp, referFileKey);
        let match = fileKey.match(/(\S+)@(\d+)$/)
        let version = undefined;
        if (null !== match) {
            fileKey = match[1];
            version = parseInt(match[2]);
        }
        if (uri !== this.getUploadUri()) {
            return await getFileContent(uri, fileKey, version);
        }
        if (this.contentType === ContentType.Link) {
            return await getFileContent(this.linkData.uploadUri, fileKey, version);
        } else {
            return await this.react_component.getRealFileContentByFileKey(fileKey, version);
        }
    }
    /** 
     * @param {DefaultShareData} info
     * @param {boolean} history
     */
    handleSearch(info, history) {
        if (!info) {
            return;
        }
        return this.react_component.handleSearch(info, history);
    }
    switchTagEditor(fileKey, replaceEditor) {
        let info = this.getInfo(fileKey);
        const { projectId, userId } = this.react_component.state;
        if (info.projectId === projectId && info.userId === userId) {
            if (replaceEditor && replaceEditor.isCommon()) {
                return this.handleSearch(info, false);
            } else if (this.contentType === ContentType.File) {
                return this.switchFileTagEditor(info.fileKey, info.version, undefined, undefined, false, replaceEditor);
            } else {
                return this.switchLinkTagEditor(info, false, replaceEditor);
            }
        } else {
            return this.handleSearch(info, false);
        }
    }
    switchFileTagEditor(fileKeyOrData, fileVersion, content, shared, refresh, replaceEditor) {
        if (!fileKeyOrData) {
            return;
        }
        return this.react_component.switchFileTagEditor(fileKeyOrData, fileVersion, content, shared, refresh, replaceEditor);
    }
    switchLinkTagEditor(linkData, refresh, replaceEditor) {
        if (!linkData) {
            return;
        }
        return this.react_component.switchLinkTagEditor(linkData, refresh, replaceEditor);
    }

    refreshAnchors(focusAnchor) {
        if (this.refreshAnchorsTimeoutId) {
            return;
        }
        this.refreshAnchorsTimeoutId = setTimeout(this.refreshAnchorsFunc.bind(this, ...arguments), 500);
    }

    refreshAnchorsFunc(focusAnchor) {
        try {
            let anchors = [];
            for (let index = 0; this.editorInstance.blocks && index < this.editorInstance.blocks.getBlocksCount(); index++) {
                /**@type{ShapeBlock} */
                let block = this.editorInstance.blocks.getBlockByIndex(index);
                if (block.name === 'codeTool' || block.name === 'plotChart') {
                    /**@type{HTMLElement} */
                    let data = block.holder.querySelector(".code-edit-result.display > .data");
                    if (data && data.closest(".code-edit")) {
                        let value = data.closest(".code-edit").getAnchorValue();
                        let { error, error2 } = data.closest(".code-edit").state;
                        if (data.parentNode.parentNode && data.parentNode.parentNode.getAttribute("id")) {
                            let className = `editorjs-anchor-${data.parentNode.parentNode.getAttribute("id").replace(/\s/g, "_")}`;
                            data.classList.add(className);
                            let type = AnchorType.Common;
                            if (error || error2) {
                                type = AnchorType.Error;
                            } else if (data.cacheNames) {
                                type = AnchorType.Variant;
                            }
                            let args;
                            if (data.cacheNames) {
                                args = data.args || [];
                            } else {
                                args = data.vary && data.vary.args || []
                            }
                            anchors.push({ className, value, type, args, cacheNames: data.cacheNames, ...extractHeader(data) });
                        }
                    }
                } else if (block.name === 'antChart') {
                    let observablehqs = block.holder.querySelectorAll(".observablehq");
                    _.each(observablehqs, (data, index) => {
                        if (data.parentNode && data.parentNode.getAttribute("id")) {
                            let className = `editorjs-anchor-${data.parentNode.getAttribute("id").replace(/\s/g, "_")}`;
                            data.classList.add(className);
                            let value = data.getAttribute("d-text")
                            let type = AnchorType.Common;
                            if (data.cacheNames) {
                                type = AnchorType.Variant;
                            }
                            let args;
                            if (data.cacheNames) {
                                args = data.args || [];
                            } else {
                                args = data.vary && data.vary.args || []
                            }

                            anchors.push({ className, value, type, args, cacheNames: data.cacheNames, ...extractHeader(data) });
                        }
                    })
                } else if (block.name === 'header') {
                    let data = block.holder.querySelector(".ce-header");
                    data.id = `a-header-${index}`
                    let header = parseInt(data.tagName.match(/^H(\d)$/)[1]);
                    anchors.push({ className: `editorjs-anchor-${data.id.replace(/\s/g, "_")}`, value: data.textContent, type: AnchorType.Header, header: header, headerContent: data.textContent });
                }
            }
            if (!this.ref.current) {
                let container = document.querySelector(`.editorjs-wrapper.${this.getTransferKey()}>.editorjs-anchor`);
                if (!container) {
                    return;
                }
                ReactDOM.render(<Anchors ref={this.ref} anchors={anchors} editorKey={this.getTransferKey()}></Anchors>, container, () => {
                });
            } else {
                let state = {
                    anchors,
                    startPoints: [],
                    endPoints: [],
                    currentPoint: undefined,
                };
                focusAnchor && this.ref.current.focus(focusAnchor, true, state)
                this.ref.current.setState(state);
            }
        } finally {
            this.refreshAnchorsTimeoutId = undefined;
        }
    }
    pinCodes(pin) {
        for (let index = 0; index < this.editorInstance.blocks.getBlocksCount(); index++) {
            let block = this.editorInstance.blocks.getBlockByIndex(index);
            if (block.name === 'codeTool') {
                let pinCodeFunc = block.holder.querySelector(".code-edit") && block.holder.querySelector(".code-edit").pinCodeFunc;
                if (pinCodeFunc) {
                    pinCodeFunc(pin);
                }
            }
        }
    }
    /**
    * 
    * @param {String} text text to run
    * @param {HTMLElement} declare element that for variant declare
    * @param {ShapeElement} data element that for content display
    * @param {Function} successFunc the function after run success
    * @param {Function} failFunc the function after run fail
    * @param {boolean} now run it right now
    * @param {string} dname default name of block element 
    * @returns the result data
    */
    runFunc(text, declare, data, successFunc, failFunc, now, dname) {
        return this.getMain() && this.getMain().runEx(this, text, declare, data, successFunc, failFunc, now, dname);
    }
    /**
     * delete element's variables
     * @param {ShapeElement} ele 
     */
    deleteCacheV(ele) {
        if (!ele || !this._delSelfModule) {
            return;
        }
        this.getMain().removeEleVariables(ele);
    }
    deleteCacheName(name) {
        for (let index = 0; index < this.editorInstance.blocks.getBlocksCount(); index++) {
            let block = this.editorInstance.blocks.getBlockByIndex(index);
            if (block.name === 'codeTool') {
                let data = block.holder.querySelector(".code-edit-result .data");
                if (data && data.cacheNames && ~data.cacheNames.indexOf(name)) {
                    this.deleteCacheV(data);
                }
            }
        }
    }
    refreshAttaches(exceptHolder) {
        if (!this.runComplete) {
            return;
        }
        this.attaches.clear();
        for (let index = 0; index < this.editorInstance.blocks.getBlocksCount(); index++) {
            let block = this.editorInstance.blocks.getBlockByIndex(index);
            if ((block.name === 'codeTool' || block.name === 'plotChart') && exceptHolder !== block.holder) {
                let fileAttachments = block.holder.querySelector(".code-edit") &&
                    block.holder.querySelector(".code-edit").state.fileAttachments;
                if (fileAttachments) {
                    _.map(Array.from(fileAttachments.keys()), (value, index) => {
                        this.attaches.add(value);
                    })
                }
            } else if (block.name === 'simpleImage' && exceptHolder !== block.holder) {
                let alt = block.holder.querySelector("img") &&
                    block.holder.querySelector("img").getAttribute("alt")
                if (alt) {
                    this.attaches.add(alt);
                }
            }
        }
        actions.variable(actions.types.FILES_ATTACHES, [], () => {
            if (!window.filesAttaches) {
                window.filesAttaches = {};
            }
            window.filesAttaches[this.getTransferKey()] = this.attaches;
            return window.filesAttaches;
        })
    }
    /**
     * 
     * @param {string} attachName 
     */
    focusFileAttach(attachName) {
        for (let index = 0; index < this.editorInstance.blocks.getBlocksCount(); index++) {
            let block = this.editorInstance.blocks.getBlockByIndex(index);
            if (block.name === 'codeTool' || block.name === 'plotChart') {
                let codeEdit = block.holder.querySelector(".code-edit");
                /**@type{Map} */
                let fileAttachments = codeEdit && codeEdit.state.fileAttachments;
                if (fileAttachments && fileAttachments.has(attachName)) {
                    codeEdit && setTimeout(() => codeEdit.editCode(true));
                }
            } else if (block.name === 'simpleImage') {
                let alt = block.holder.querySelector("img") &&
                    block.holder.querySelector("img").getAttribute("alt")
                if (alt && alt === attachName) {
                    this.editorInstance.caret.setToBlock(index, "start", 0);
                }
            }
        }
    }
    focusImportSource(source) {
        for (let index = 0; index < this.editorInstance.blocks.getBlocksCount(); index++) {
            let block = this.editorInstance.blocks.getBlockByIndex(index);
            if (block.name === 'codeTool') {
                let codeEdit = block.holder.querySelector(".code-edit");
                let importSource = codeEdit && codeEdit.state.importSource;
                if (importSource && importSource === source) {
                    codeEdit && setTimeout(() => codeEdit.editCode(true));
                }
            }
        }
    }
    refreshRuns(exceptHolder) {
        for (let index = 0; index < this.editorInstance.blocks.getBlocksCount(); index++) {
            let block = this.editorInstance.blocks.getBlockByIndex(index);
            if ((block.name === 'codeTool' || block.name === 'plotChart') && (exceptHolder !== block.holder)) {
                block.holder.querySelector(".code-edit") && block.holder.querySelector(".code-edit").runCode();
            }
        }
    }
    isOpenSelect() {
        return this.openSelect;
    }
    multipleChoiceFunc(prevValue) {
        this.react_component && this.react_component.multipleChoiceFunc(prevValue);
    }
    selectBlocks(selected) {
        this.openSelect = selected;
        for (let index = 0; index < this.editorInstance.blocks.getBlocksCount(); index++) {
            let block = this.editorInstance.blocks.getBlockByIndex(index);
            /**@type {HTMLDivElement} */
            let checkBtn = block.holder.querySelector(".ce-block-border-left-wrap>span")
            checkBtn.className = selected ? `icon far fa-square` : `icon far fa-square hide`;
            /**@type {HTMLDivElement} */
            let d = block.holder.querySelector(".ce-block-border-left-wrap>i");
            d.classList.toggle('invisible', selected);
        }
    }
    selectBlock(index) {
        let block = this.editorInstance.blocks.getBlockByIndex(index);
        /**@type {HTMLDivElement} */
        let checkBtn = block.holder.querySelector(".ce-block-border-left-wrap>span")
        if (checkBtn.className === `icon far fa-square`) {
            checkBtn.click();
        }
    }
    async copySelectBlocks() {
        let datas = [];
        for (let index = 0; index < this.editorInstance.blocks.getBlocksCount(); index++) {
            let block = this.editorInstance.blocks.getBlockByIndex(index);
            /**@type {HTMLDivElement} */
            let checkBtn = block.holder.querySelector(".ce-block-border-left-wrap>span")
            if (!checkBtn.classList.contains(`fa-square`)) {
                let data = await block.save();
                datas.push(data);
            }
        }
        return datas;
    }
    getIndexOfSelectBlocks() {
        let datas = [];
        for (let index = 0; index < this.editorInstance.blocks.getBlocksCount(); index++) {
            let block = this.editorInstance.blocks.getBlockByIndex(index);
            /**@type {HTMLDivElement} */
            let checkBtn = block.holder.querySelector(".ce-block-border-left-wrap>span")
            if (!checkBtn.classList.contains(`fa-square`)) {
                datas.push(index);
            }
        }
        return datas;
    }
    /**
     *  run once of variables and inspectors when file loaded
     */
    async run() {
        if (this.runComplete) {
            return;
        }
        await runVariables(this.getMain());
        let promiseList = [];
        const { inspectors } = this.getMain()._cachedata;
        _.each(inspectors, (inspector) => {
            promiseList.push(inspector.varia.define(inspector.args, inspector.func));
        })
        await Promise.all(promiseList);
        this.refreshAnchors();
        this.runComplete = true;
        this.sscope = _.assign({}, window.globalScope);
        _.each(this.moduleNames, (moduleName) => {
            window.moduleScope[moduleName] && _.assign(this.sscope, window.moduleScope[moduleName])
        })
    }
    /**
     * check files...need enhause @xing '%' stands for {ROOT_URI}
     * @param {string} fileKey 
     * @param {string} referFileKey 
     * @returns {string} Error if file not found
     */
    getFileAttachmentUrl(fileKey, referFileKey) {
        let absoluteUrl = getAbsoluteUrl(fileKey, referFileKey);
        if (absoluteUrl) {
            return absoluteUrl;
        }
        if (this.contentType === ContentType.Link) {
            if (this.linkData) {
                let fileKey2 = getFileKey(getFileKey(this.linkData.fileKey, FILESDIR), fileKey);
                if (this.linkData.attachments && this.linkData.attachments[fileKey2]) {
                    return this.linkData.uploadUri + fileKey2;
                }
            }
        } else {
            if (this.fileData) {
                let fileKey2 = getFileKey(getFileKey(this.fileData.fileKey, FILESDIR), fileKey);
                if (this.react_component.state.fileNamesJson[fileKey2]) {
                    return this.react_component.state.uploadUri + fileKey2;
                }
            }
        }
        if (~fileKey.indexOf(".") &&
            ~AllAcceptArr.indexOf(fileKey.substring(fileKey.indexOf(".")))) {
            throw new Error(`File not found:${fileKey}`);
        }
        return fileKey;
    }
    /**
     * clear cache
     */
    clearCache() {
        if (this.autosaveTimeoutId) {
            clearTimeout(this.autosaveTimeoutId);
            this.autosaveTimeoutId = undefined;
        }
        if (this.refreshAnchorsTimeoutId) {
            clearTimeout(this.refreshAnchorsTimeoutId);
            this.refreshAnchorsTimeoutId = undefined;
        }
        this.cachePositions.splice(0, this.cachePositions.length);
    }

    getSearchText(re = false) {
        if ("" === this.searchText) {
            return this.searchText;
        }
        if (re) {
            const { caseSensitive, wholeWord, reg } = this.searchConfig;
            if (caseSensitive) {
                return this.searchText;
            } else if (reg) {
                return new RegExp(escapeStringRegexp(this.searchText), "g");
            } else if (!caseSensitive) {
                return new RegExp(escapeStringRegexp(this.searchText), "gi");
            }
        }
        return this.searchText;
    }

    isWholeWord() {
        return this.searchConfig.wholeWord;
    }

    setSearch(text, caseSensitive = false, wholeWord = false, reg = false) {
        this.searchText = text;
        this.searchConfig = {
            caseSensitive: caseSensitive,
            wholeWord: wholeWord,
            reg: reg,
        }
    }

    /**
     * @returns {{type: "cm", ele: HTMLElement, name: "block.name", ret: {
     *   cm: CodeMirror.Editor;
     *  line: string;
     *  from: CodeMirror.Position;
     *  to: CodeMirror.Position;
     *  }[]}[]
     * }
     */
    tagSearch() {
        const api = this.editorInstance;
        if (!api || !this.isReady()) {
            return [];
        }
        let tags = [];
        for (let index = 0; index < this.editorInstance.blocks.getBlocksCount(); index++) {
            let block = this.editorInstance.blocks.getBlockByIndex(index);
            switch (block.name) {
                case 'codeTool': {
                    /**@type{HTMLElement} */
                    let data = block.holder.querySelector(".code-edit-result > .data");
                    if (data) {
                        if (data.closest(".code-edit").cm) {
                            let cm = data.closest(".code-edit").cm;
                            let retTmp = cmSearchFunc.call(cm, cm, this.getSearchText(true), this.isWholeWord());
                            if (retTmp.length) {
                                let tag = { type: SearchType.cm, ele: data, ret: retTmp, name: block.name };
                                tags.push(tag);
                            }
                        } else if (data.closest(".code-edit").state.value) {
                            let ret2 = highligthComp(this.getSearchText(true), data.closest(".code-edit").state.value, this.isWholeWord());
                            if (ret2 && ret2.length) {
                                tags.push({ type: SearchType.nocm, ele: data, ret: ret2, name: block.name });
                            }
                        }
                    }
                    break;
                }
                default:
                    let ret3 = highligthComp(this.getSearchText(true), block.holder.innerText, this.isWholeWord());
                    if (ret3 && ret3.length) {
                        tags.push({ type: SearchType.others, ele: block.holder, ret: ret3, name: block.name });
                    }
                    break;
            }
        }
        return tags;
    }

    clearSearch() {
        this.setSearch("");
        this.tagSearch();
    }

    recordPosition = (position) => {
        if (this.cachePositions[this.currentPositionIndex] === position) {
            return;
        }
        if (this.currentPositionIndex !== this.cachePositions.length - 1) {
            this.cachePositions.splice(this.currentPositionIndex + 1, this.cachePositions.length);
        }
        this.cachePositions.push(position);
        this.currentPositionIndex = this.cachePositions.length - 1;
    }

    prevPosition = () => {
        for (let index = this.currentPositionIndex - 1; index >= 0; index--) {
            let className = this.cachePositions[index];
            let editorKey = this.getTransferKey();
            let data = document.querySelector(`.editorjs-wrapper.${editorKey}>.editorjs .${className}`);
            if (data) {
                this.currentPositionIndex = index;
                return data;
            }
        }
    }

    nextPosition = () => {
        for (let index = this.currentPositionIndex + 1; index < this.cachePositions.length; index++) {
            let className = this.cachePositions[index];
            let editorKey = this.getTransferKey();
            let data = document.querySelector(`.editorjs-wrapper.${editorKey}>.editorjs .${className}`);
            if (data) {
                this.currentPositionIndex = index;
                return data;
            }
        }
    }

    pinCode = () => {
        const api = this.editorInstance;
        if (!api || !this.isReady() || api.blocks.getCurrentBlockIndex() === -1 || this.getReadOnly()) {
            return;
        }
        const block = api.blocks.getBlockByIndex(api.blocks.getCurrentBlockIndex())
        if (block.name === 'codeTool') {
            /**@type{HTMLElement} */
            let data = block.holder.querySelector(".code-edit-result > .data");
            if (data) {
                let { pinCode } = data.closest(".code-edit").state;
                const indexs = this.openSelect ? this.getIndexOfSelectBlocks() : [api.blocks.getCurrentBlockIndex()];
                if (indexs.length >= 1) {
                    _.each(indexs, (blockIndex) => {
                        let blockByIndex = api.blocks.getBlockByIndex(blockIndex);
                        if (blockByIndex && blockByIndex.name === 'codeTool') {
                            /**@type{HTMLElement} */
                            let dataByIndex = blockByIndex.holder.querySelector(".code-edit-result.display > .data");
                            if (dataByIndex) {
                                let pinCodeFunc = dataByIndex.closest(".code-edit").pinCodeFunc;
                                pinCodeFunc(!pinCode);
                            }
                        }
                    })
                }
            }
        }
    }

    moveCellUp = () => {
        const api = this.editorInstance;
        if (!api || !this.isReady() || api.blocks.getCurrentBlockIndex() === -1 || this.getReadOnly()) {
            return;
        }
        const indexs = this.openSelect ? this.getIndexOfSelectBlocks() : [api.blocks.getCurrentBlockIndex()];
        if (indexs.length === 0 || indexs[0] === 0) {
            return;
        }
        _.each(indexs, (blockIndex) => {
            api.blocks.move(blockIndex - 1, blockIndex);
        })
        api.caret.setToBlock(api.blocks.getCurrentBlockIndex());
        const block = api.blocks.getBlockByIndex(api.blocks.getCurrentBlockIndex())
        if (!isVisibleInViewport(block.holder)) {
            block.holder.scrollIntoView({
                behavior: "auto", block: "center", inline: "nearest"
            })
        }
    }

    moveCellDown = () => {
        const api = this.editorInstance;
        if (!api || !this.isReady() || api.blocks.getCurrentBlockIndex() === -1 || this.getReadOnly()) {
            return;
        }
        const indexs = this.openSelect ? this.getIndexOfSelectBlocks() : [api.blocks.getCurrentBlockIndex()];
        if (indexs.length === 0 || indexs[indexs.length - 1] >= api.blocks.getBlocksCount() - 1) {
            return;
        }
        _.each(_.reverse(indexs), (blockIndex) => {
            api.blocks.move(blockIndex + 1, blockIndex);
        })
        api.caret.setToBlock(api.blocks.getCurrentBlockIndex());
        const block = api.blocks.getBlockByIndex(api.blocks.getCurrentBlockIndex())
        if (!isVisibleInViewport(block.holder)) {
            block.holder.scrollIntoView({
                behavior: "auto", block: "center", inline: "nearest"
            })
        }
    }

    getSettings() {
        return this.settings;
    }

    getDatabases() {
        return this.settings.getDatabases();
    }

    /**
     * upload settings.json to server
     */
    async saveSettings(rightnow) {
        if (isNotLogin() || notMyOwnProject()) {
            return;
        }
        if (rightnow) {
            this.settingsTimeout = false;
            let file = this.react_component.state.fileNamesJson[SETTINGS_NAME];
            let content = JSON.stringify(this.settings.getSettings(), undefined, 2);
            if (file) {
                await this.react_component.uploadMdFile(file, undefined, content);
            } else {
                await this.react_component.newFileFunc({ target: { value: SETTINGS_NAME } }, content, "", false)
            }
        } else {
            this.settingsTimeout = true;
        }
    }

    setCountSecond(countSecond) {
        this.countSecond = countSecond;
    }

    refreshViewCode() {
        let view_code = false;
        let usersSharePermission = this.settings.getUsersSharePermission()
        if (usersSharePermission) {
            if ((!usersSharePermission[USER_ID] ||
                usersSharePermission[USER_ID] >= ShareProjectCanOptions["View code"]) &&
                this.settings.getReadwrite_mode() === ReadWriteOptions[2]) {
                view_code = true;
            }
        }
        actions.variable(actions.types.VIEW_CODE, [], () => {
            return view_code;
        })
    }
    async needSaveCurrentFile() {
        let state = {};
        ++this.countSecond;
        let autoSaveInSecond = this.settings.getAutoSaveInSecond();
        if (this.settingsTimeout && this.countSecond % autoSaveInSecond === 0) {
            this.saveSettings(true);
        }
        if (!this.readOnly) {
            if (this.getNeedSave()) {
                console.log("autosave:", this.settings.getAutoSave(), this.countSecond % autoSaveInSecond === 0, new Date().getTime() - this.getNeedSaveTimeInSecond(), this.getNeedSaveTimeInSecond());
                if (this.settings.getAutoSave() && this.countSecond % autoSaveInSecond === 0 && new Date().getTime() - this.getNeedSaveTimeInSecond() >= 5000) {
                    await this.cacheOrUploadFile(false, state, true)
                } else if (this.countSecond % (autoSaveInSecond > 10 ? 5 : autoSaveInSecond || 5)) {
                    await this.cacheOrUploadFile(true, state)
                }
            } else if (this.settings.getAutoSave() && this.countSecond % autoSaveInSecond === 0) {
                await this.react_component.uploadAllFiles(undefined, state, true);
                if (this.linkData && this.shared) {
                    let path = `graphxr.observable.shared.${abosolutePath(this.linkData.fileKey, this.getUploadUri())}`;
                    let content = getItem(path);
                    if (content && await this.uploadMdFile(this.linkData, content, undefined, this.version, true)) {
                    }
                }
            }
        }
        !_.isEmpty(state) && this.react_component.setState(state);
    }
    /**
     * @returns get editorJs's latest content
     */
    async latestText() {
        let content = await this.editorInstance.save()
        delete content.time;
        return JSON.stringify(content);
    }
    /**
     * @returns get current file's itemKey
     */
    itemKey() {
        return (this.fileData && !this.version && this.fileData.fileKey) ?
            (getFileNamesKey() + "." + this.fileData.fileKey) : undefined;
    }
    /**
     * some (like csv,json,js) need encode the storage data
     * @returns {String}
     */
    getWrapperItem() {
        let key = this.itemKey();
        if (undefined === key) {
            return undefined;
        }
        let content = getItem(key);
        if (content === undefined) {
            return undefined;
        }
        return wrapContent(content, this.fileData, this.getUploadUri());
    }
    getAutoUploadMaxSize() {
        return this.settings.getAutoSaveInSecond() * 12 * 1024;
    }
    /**
     * save current file to storage
     * @param {boolean} cache only cache in browser or upload to server
     * @param {object} state transfer state that setState later or right now setState in the function
     * @returns  processing of save is ok or not
     */
    async cacheOrUploadFile(cache, state, largeCache = false) {
        if (this.readOnly) {
            return false;
        }
        let key = this.itemKey();
        if (key) {
            if (!isFileNoEdit(this.fileData)) {
                const currentData = await this.latestText();
                const currentRealData = transferToRealData(this.fileData, currentData);
                const oldRealData = transferToRealData(this.fileData, this.getWrapperItem() || this.option.data);
                let type = undefined;
                if (currentRealData !== oldRealData) {
                    if (currentRealData === "undefined") {
                        alert("Occur danger save error")
                        return false;
                    }
                    if (!cache) {
                        type = "upload";
                    } else {//in browser cache 
                        type = "cache";
                    }
                } else if (!cache && this.react_component.isNeedUpload(this.fileData)) {
                    type = "upload";
                }
                if (type) {
                    if (type === "upload" && largeCache && currentRealData.length > this.getAutoUploadMaxSize()) {
                        type = "cache";
                    }
                    if (type === "upload") {
                        if (await this.react_component.uploadMdFile(this.fileData, undefined, currentRealData, state, this.version)) {
                            this.option.data = currentData;
                            DEBUG_EDITOR && console.log("uploaded!");
                            return true;
                        }
                    } else if (type === "cache") {
                        setItem(key, currentRealData);
                        this.fileData.mtimeMs = (new Date()).getTime();
                        this.refreshFileData(this.fileData, this.fileData);
                        this.option.data = currentData;
                        state && _.assign(state, { fileNamesJson: this.react_component.setFileNamesJsonItem() });
                        DEBUG_EDITOR && console.log("save in cache!");
                        this.setNeedSave(false);
                        return true;
                    }
                } else {
                    console.log("Nothing changed. We don't need to save.");
                    this.setNeedSave(false);
                }
            }
        } else if (this.shared) {
            return this.cacheOrUploadShared(cache, largeCache);
        } else if (this.isLocalEditFile()) {
            console.warn("local edit file!");
            this.setNeedSave(false);
            return true;
        } else {
            let currentData = await this.latestText();
            setItem(getUndefinedKey(), currentData);
            this.option.data = currentData;
            // showToast("can not cache!", "error")
            this.setNeedSave(false);
            return true;
        }
        return false;
    }
    async saveCurrentFile(state) {
        return await this.cacheOrUploadFile(this.settings.getAutoSave() ? false : true, state);
    }
    async upload() {
        return await this.cacheOrUploadFile(false);
    }
    /**
     * 
     * @param {boolean} cache 
     * @returns boolean means success/fail 
     */
    async cacheOrUploadShared(cache, largeCache = false) {
        let file = this.linkData;
        let version = this.version;
        if (file && this.shared) {
            const currentData = await this.latestText()
            const currentRealData = transferToRealData(file, currentData);
            const oldRealData = transferToRealData(file, this.option.data);
            let type = undefined;
            if (currentRealData !== oldRealData) {
                if (currentRealData === "undefined") {
                    alert("Occur danger save error")
                    return false;
                }
                if (!cache) {
                    type = "upload";
                } else {//in browser cache 
                    type = "cache";
                }
            } else if (!cache) {
                if (file.mtimeMs && (!file.cache || file.mtimeMs > file.cache.mtimeMs)) {
                    type = "upload";
                }
            }
            if (type) {
                if (type === "upload" && largeCache && currentRealData.length > this.getAutoUploadMaxSize()) {
                    type = "cache";
                }
                if (type === "upload") {
                    if (await this.uploadMdFile(file, currentRealData, undefined, version)) {
                        this.option.data = currentData;
                        DEBUG_EDITOR && console.log("uploaded!");
                        return true;
                    }
                } else if (type === "cache") {
                    let path = `graphxr.observable.shared.${abosolutePath(file.fileKey, this.getUploadUri())}`;
                    setItem(path, currentRealData);
                    file.mtimeMs = (new Date()).getTime();
                    this.refreshLinkData();
                    this.option.data = currentData;
                    DEBUG_EDITOR && console.log("save in cache!");
                    this.setNeedSave(false);
                    return true;
                }
            } else {
                console.warn("not need save!");
                this.setNeedSave(false);
            }
        }
        return false;
    }
    /**
     * upload shared file
     * @param {DefaultShareData}  file 
     * @param {string} content 
     * @param {*} setLoading 
     * @param {number} version 
     * @param {boolean} largeCache
     * @returns {boolean}
     */
    async uploadMdFile(file, content, setLoading, version = DEFAULT_VER, largeCache = false) {
        if (version !== DEFAULT_VER) {
            showToast("Cannot upload version <> 0 file.", "error");
            return false;
        }
        if (!file.sharedId && file.tag !== TAG_TYPE.projectShare) {
            showToast("Cannot save this file.", "error");
            return false;
        }
        if (undefined === content || null === content) {
            showToast("Save failed.", "error")
            return false;
        }
        if (largeCache && content.length > this.getAutoUploadMaxSize()) {
            document.querySelectorAll(".fa-cloud-upload-alt").forEach((value) => {
                value.classList.add("shake");
            })
            showToast(`Large files cannot be auto-saved. Please save manually.`);
            return false;
        }
        let fd = new FormData();
        (file.tag === TAG_TYPE.projectShare) && fd.append("shareProjectId", file.projectId);
        file.sharedId && fd.append("sharedId", file.sharedId);
        fd.append("fileKey", file.fileKey);
        fd.append("fileName", getFileName(file.fileKey));
        fd.append('data', new Blob([content], { type: transferType(file.type || getFileType(file.fileKey)) }));
        fd.append('userId', file.userId);
        fd.append('projectId', file.projectId);
        fd.append('roomId', `${this.getUploadUri()}${file.fileKey}`);
        let res = await axios.post(`/api/grove/uploadFile`, fd);
        if (!res.data.status) {
            file.cache = res.data.content;
            file.mtimeMs = file.cache.mtimeMs;
            let path = `graphxr.observable.shared.${abosolutePath(file.fileKey, this.getUploadUri())}`;
            removeItem(path);
            if (this.linkData === file) {
                this.refreshLinkData();
                this.setNeedSave(false);
                if (isFileCsv(file)) {
                    const { _cachedata, _cachedata: { inspectors, variables } } = this.getMain();
                    let variable = variables["text"]
                    variable.varia.define(variable.name, variable.args, () => {
                        return content;
                    });
                }
            }
        }
        res.data.message && showToast(res.data.message, res.data.status ? "error" : "success");
        setLoading && setLoading(false);
        return !res.data.status;
    }

    /**
     * @returns {Promise<string>} return undefined if other file no need screenshot
     */
    async getScreenshot() {
        if (undefined === this.fileData || null === this.fileData) {
            return undefined;
        }
        if (!isFileEditorJs(this.fileData)) {
            return undefined;
        }
        let editorjsEle = document.querySelector(`.editorjs-wrapper.${this.getTransferKey()}>.editorjs`);
        editorjsEle.classList.add("print");
        let canvas = await html2canvas(editorjsEle, { backgroundColor: getComputedStyle(document.body).getPropertyValue('--background-color') });
        editorjsEle.classList.remove("print");
        let imgData = canvas.toDataURL("image/png", 1.0);
        return imgData;
    }
    /**
    * 
    * @param {boolean} loading 
    * @param {Function} cb callback
    * @param {boolean} loadingClosable
    */
    setWrapperLoading(loading, cb, loadingClosable = false) {
        this.react_component.setWrapperLoading(loading, cb, loadingClosable);
    }

    isCommon() {
        return this.editorjsKey === COMMON_EDITOR_KEY
    }

    isReady() {
        return this.ready;
    }
    getContentType() {
        return this.contentType;
    }
    /**
     * set fileData
     * @param {DefaultFileData} fileData 
     * @param {number} version 
     * @param {ShapeChatInfo} shared 
     * 
     */
    setFileData(fileData, version, shared) {
        /**@type {ShapeFileChange} */
        let state = {};
        if (!this.fileData && fileData) {
            this.linkData = undefined;
            _.assign(state, { resetLinkData: true });
        }
        this.fileData = fileData;
        this.fileData && (this.fileData.selectedVersion = version);
        this.shared = shared;
        if (window.currentEditor !== this) {
            this.setNeedRefresh(true);
        } else {
            actions.variable(actions.types.FILE_CHANGE, [], () => {
                return _.assign(state, {
                    file: this.fileData, version: version, shared: this.shared,
                    needUpload: this.react_component.isNeedUpload(this.fileData)
                });
            });
        }
    }
    /**
     * 
     * @param {DefaultShareData} linkData 
     */
    setLinkData(linkData) {
        /**@type {ShapeChatInfo} */
        let shared = undefined;
        if (linkData && (~[TAG_TYPE.shared, TAG_TYPE.projectShare].indexOf(linkData.tag)) &&
            linkData.userId && linkData.userId !== "undefined") {
            shared = _.assign({}, ShapeChatInfo, linkData);
            shared.isMaster = true;
        }
        let state = {};
        if (!this.linkData && linkData) {
            this.fileData = undefined;
            _.assign(state, { resetFileData: true });
        }
        this.linkData = linkData;
        this.linkData && (this.linkData.selectedVersion = linkData.version);
        this.shared = shared;
        if (window.currentEditor !== this) {
            this.setNeedRefresh(true);
        } else {
            actions.variable(actions.types.LINK_CHANGE, [], () => {
                return _.assign(state, {
                    file: this.linkData, shared: this.shared,
                    needUpload: !!(this.linkData && this.linkData.mtimeMs &&
                        (!this.linkData.cache || this.linkData.mtimeMs > this.linkData.cache.mtimeMs))
                });
            });
        }
    }
    refreshLinkData() {
        let state = {};
        if (window.currentEditor !== this) {
            this.setNeedRefresh(true);
        } else {
            actions.variable(actions.types.LINK_CHANGE, [], () => {
                return _.assign(state, {
                    file: this.linkData, shared: this.shared,
                    needUpload: !!(this.linkData && this.linkData.mtimeMs &&
                        (!this.linkData.cache || this.linkData.mtimeMs > this.linkData.cache.mtimeMs))
                });
            });
        }
    }
    refreshFileData(oldFile, newFile) {
        if (this.fileData === oldFile) {
            let state = {};
            this.fileData = newFile;
            if (window.currentEditor !== this) {
                this.setNeedRefresh(true);
            } else {
                actions.variable(actions.types.FILE_CHANGE, [], () => {
                    return _.assign(state, {
                        file: this.fileData, version: this.version, shared: this.shared,
                        needUpload: this.react_component.isNeedUpload(this.fileData)
                    });
                });
            }
        }
    }
    focusCurrentFileData() {
        if (this.fileData) {
            let state = { resetLinkData: true };
            actions.variable(actions.types.FILE_CHANGE, [], () => {
                return _.assign(state, {
                    file: this.fileData, version: this.version, shared: this.shared,
                    needUpload: this.react_component.isNeedUpload(this.fileData)
                });
            });
        } else if (this.linkData) {
            let state = { resetFileData: true };
            actions.variable(actions.types.LINK_CHANGE, [], () => {
                return _.assign(state, {
                    file: this.linkData, shared: this.shared,
                    needUpload: !!(this.linkData && this.linkData.mtimeMs &&
                        (!this.linkData.cache || this.linkData.mtimeMs > this.linkData.cache.mtimeMs))
                });
            });
        } else {
            let state = { resetLinkData: true };
            actions.variable(actions.types.FILE_CHANGE, [], () => {
                return _.assign(state, {
                    file: null, version: DEFAULT_VER, shared: undefined,
                    needUpload: false
                });
            });
        }
    }
    /**
    * @returns {Object<string, DefaultFileData>}
    */
    getFileList() {
        if (this.contentType === ContentType.Link) {
            if (this.linkData) {
                return this.linkData.attachments;
            }
        } else {
            if (this.fileData) {
                let fileNamesJson = this.react_component.state.fileNamesJson;
                let folderKey = getFolderKey(this.fileData.fileKey, FILESDIR_NAME);
                const list = {};
                _.keys(fileNamesJson).filter(fileKey => {
                    if (fileKey.startsWith(folderKey) && fileKey !== folderKey
                        && !~fileKey.substring(folderKey.length).indexOf(fileSeparator)) {
                        list[fileKey] = fileNamesJson[fileKey];
                    }
                })
                return list;
            }
        }
    }
    getCurrentBlockIndex() {
        const api = this.editorInstance;
        return api && api.blocks ? api.blocks.getCurrentBlockIndex() : undefined;
    }
    focusBlockByIndex(index) {
        const api = this.editorInstance;
        if (api) {
            const { blocks } = api;
            if (blocks) {
                api.caret.setToBlock(index, 'start', 0);
            }
        }
    }
    focusFolder(cb, tabs = false) {
        this.renderTask.submitRunOverTask(() => {
            document.querySelector(`.ant-drawer.drawer-folder`) &&
                document.querySelector(`.ant-drawer.drawer-folder`).focus();
            document.querySelector(`.file.${tabs ? "tabs-" : ""}active`) && document.querySelector(`.file.${tabs ? "tabs-" : ""}active`).focus();
            cb && cb();
        })
    }
    getNeedRefresh() {
        return this.needRefresh;
    }
    setNeedRefresh(needRefresh) {
        this.needRefresh = needRefresh;
    }
    getInitEdit() {
        return this.initEdit;
    }
    setInitEdit(initEdit) {
        this.initEdit = initEdit;
    }
    async refreshFile() {
        this.needRefresh = false;
        if (this.fileData) {
            await this.react_component.refreshFile(this.fileData, this.version);
        } else if (this.linkData) {
            let res = await axios.post(`/api/grove/linkTo`, _.assign({}, this.linkData,
                { userId: this.linkData.userId, projectId: this.linkData.projectId }));
            if (!res.data.status) {
                if (res.data.content instanceof Object) {
                    this.linkData.attachments = res.data.content;
                }
                this.linkData.userName = res.data.userName;
                const { fileKey } = this.linkData;
                let uri = this.linkData.uploadUri + `${fileKey}${this.version ?
                    `${verFileTag}${fileSeparator}${this.version}` : ""}`;
                let content = await getContent(this.linkData, uri)
                if (this.isCommon()) {
                    content = wrapContent(content, this.linkData, this.getUploadUri());
                    this.refreshLinkData();
                    await this.value(content, ContentType.Link, this.version);
                } else {
                    content = wrapContent(content, this.linkData, this.getUploadUri());
                    await this.value(content, this.version);
                }
            }
        } else if (this.editorInstance && this.isReady()) {
            if (commonData.getReadOnlyMode()) {
                !this.getReadOnly() && await this.setReadOnly(true);
            } else {
                this.getReadOnly() && await this.setReadOnly(false);
            }
        }
    }
    /**
    * 
    * @param {Blob} blob 
    * @param {string} type image file type 
    * @param {string} fileName file name 
    */
    processPaste(blob, type, fileName) {
        let folderName = `files`;
        let folderKey;
        fileName = (fileName !== "image.png" && fileName) ||
            `screenshot-${moment(new Date()).format("YYYY-MM-DD+hh-mm-ss")}.${type.replace("image/", "")}`;
        if (this.linkData) {
            folderKey = getFileKey(this.linkData.fileKey, `${folderName}${fileSeparator}`);
            return new Promise((resolve, reject) => {
                let fileKey = `${folderKey}${fileName}`
                let file = Object.assign({}, this.linkData, {
                    fileKey: fileKey
                })
                this.react_component.setWrapperLoading(true, () => {
                    let reader = new FileReader();
                    reader.onloadend = async (event) => {
                        let ret = await this.uploadMdFile(file, dataURItoBlob(event.target.result), undefined, this.version);
                        if (!ret) {
                            return reject("can not upload file");
                        }
                        let res = await axios.post(`/api/grove/linkTo`, this.linkData);
                        if (!res.data.status) {
                            if (res.data.content instanceof Object) {
                                this.linkData.attachments = res.data.content;
                            }
                            this.linkData.userName = res.data.userName;
                        } else {
                            return reject("fail to refresh files");
                        }
                        this.react_component.setWrapperLoading(false);
                        if (fileKey.startsWith(getFolderKey(this.linkData.fileKey, FILESDIR_NAME))) {
                            window.setTimeout(() =>
                                actions.variable(actions.types.REFRESH_FILE_ATTACHMENTS, [], () => { })
                            )
                        }
                        resolve({ url: fileName, caption: fileName });
                    };
                    reader.readAsDataURL(blob);
                })
            })
        } else if (this.fileData) {
            folderKey = getFileKey(this.fileData.fileKey, `${folderName}${fileSeparator}`)
            return new Promise((resolve, reject) => {
                let fileKey = `${folderKey}${fileName}`
                let file = Object.assign({}, DefaultFileData, {
                    fileKey: fileKey,
                    name: fileName,
                    type: type
                })
                this.react_component.setWrapperLoading(true, () => {
                    let reader = new FileReader();
                    reader.onloadend = async (event) => {
                        const { fileNamesJson } = this.react_component.state;
                        let state = {};
                        let ret = await this.react_component.uploadMdFile(file, undefined, event.target.result, state);
                        if (!ret) {
                            return reject("can not upload file");
                        }
                        !_.isEmpty(state) && this.react_component.setState(state, () => {
                            this.react_component.setWrapperLoading(false);
                            if (fileKey.startsWith(getFolderKey(this.fileData.fileKey, FILESDIR_NAME))) {
                                window.setTimeout(() =>
                                    actions.variable(actions.types.REFRESH_FILE_ATTACHMENTS, [], () => { })
                                )
                            }
                            resolve({ url: fileName, caption: fileName });
                        });
                    };
                    reader.readAsDataURL(blob);
                })
            })
        } else {
            return;
        }
    }

    async toDoc() {
        doc.clearDocument();
        for (let index = 0; index < this.editorInstance.blocks.getBlocksCount(); index++) {
            let block = this.editorInstance.blocks.getBlockByIndex(index);
            switch (block.name) {
                case 'codeTool':
                    /**@type{HTMLElement}  if data display then print */
                    let data = block.holder.querySelector(".code-edit-result.display > .data");
                    if (data) {
                        /**@type{HTMLElement} */
                        let declare = block.holder.querySelector(".code-edit-result.display > .declare");
                        doc.createContent("paragraph", `<div>${declare ? declare.innerHTML : ""}${await extractCanvas(data)}</div>`, 0, 'left');
                    }
                    /**@type{HTMLElement}  if data display then print */
                    let database = block.holder.querySelector(".code-edit-content > .database");
                    if (database) {
                        let scrollY = window.pageYOffset || window.scrollY || document.body.scrollTop;
                        let scrollX = window.pageXOffset || window.scrollX || document.body.scrollLeft;
                        window.scrollTo(0, 0);
                        let canvas = await html2canvas(database, { backgroundColor: getComputedStyle(document.body).getPropertyValue('--background-color') })
                        doc.createContent("paragraph", `${canvasImage(canvas)}`, 0, 'left');
                        let table = block.holder.querySelector(".code-edit-content > .ant-table-wrapper");
                        if (table) {
                            let canvas2 = await html2canvas(table, { backgroundColor: getComputedStyle(document.body).getPropertyValue('--background-color') })
                            doc.createContent("paragraph", `${canvasImage(canvas2)}`, 0, 'left');
                        }
                        window.scrollTo(scrollX, scrollY);
                    }


                    /**@type{HTMLElement} if code display then print */
                    let code = block.holder.querySelector(".code-edit-wrapper");
                    if (code) {
                        doc.createContent("paragraph", `<pre style="color: #f50;font-size:14px">${code.innerText}</pre>`, 0, 'left');
                    }

                    break;
                case 'code':
                    doc.createContent("paragraph", `<code>${block.holder.querySelector("textarea").value}</code>`, 0, 'left');
                    break;
                case 'table':
                    doc.createContent("paragraph", `${block.holder.querySelector(".tc-table").outerHTML}`, 0, 'left');
                    break;
                // case 'antChart':
                // doc.createImage('body', R)
                // break;
                default:
                    /**@type{HTMLElement} */
                    let content = block.holder.querySelector(".ce-block__content");
                    if (content) {
                        doc.createContent("paragraph", await extractCanvas(content), 0, 'left');
                    }
                    break;
            }
        }
        let fileName = this.getData() ? (this.getData()).name.replace(".", "_") : "unsaved file";
        doc.createDocument(`${fileName}.doc`);
    }

    /**
     * 
     * @returns variables of relational database
     */
    getSqlites() {
        return this.getDbs(...ReDbDialets);
    }
    /**
     * 
     * @returns variables of special type database
     */
    getDbs(...types) {
        const { variables } = this.getMain()._cachedata;
        return _.reduce(variables, (prev, v, k) => {
            let _value = v.varia._value;
            if (_value && (typeof _value) === 'object' &&
                (_value.constructor && _value.constructor.prototype.dialect && ~types.indexOf(_value.dialect))) {
                if (!~prev.indexOf(k)) {
                    prev.push(k);
                }
            }
            return prev;
        }, [])
    }
    getFileDataByKey(fileKey) {
        return this.react_component.state.fileNamesJson[fileKey];
    }
    replaceFile(selectedFileName, fileName) {
        for (let index = 0; index < this.editorInstance.blocks.getBlocksCount(); index++) {
            let block = this.editorInstance.blocks.getBlockByIndex(index);
            if (block.name === 'codeTool') {
                let replaceFile = block.holder.querySelector(".code-edit") && block.holder.querySelector(".code-edit").replaceFile;
                if (replaceFile) {
                    replaceFile(selectedFileName, fileName);
                }
            }
        }
    }
    removeFile(selectedFileName) {
        for (let index = 0; index < this.editorInstance.blocks.getBlocksCount(); index++) {
            let block = this.editorInstance.blocks.getBlockByIndex(index);
            if (block.name === 'codeTool') {
                let removeFile = block.holder.querySelector(".code-edit") && block.holder.querySelector(".code-edit").removeFile;
                if (removeFile) {
                    removeFile(selectedFileName);
                }
            }
        }
    }
    downloadFile(fileKey) {
        let url = this.getFileAttachmentUrl(fileKey);
        let fileName = url.substring(String(url).lastIndexOf(fileSeparator) + 1);
        if (url.startsWith(this.getUploadUri())) {
            if (ImageCache[url]) {
                saveLink(URL.createObjectURL(ImageCache[url]), fileName);
            } else {
                let downloadAnchorNode = document.createElement("a");
                downloadAnchorNode.setAttribute("href", url);
                downloadAnchorNode.setAttribute("download", fileName);
                document.body.appendChild(downloadAnchorNode);
                downloadAnchorNode.click();
            }
        } else {
            let downloadAnchorNode = document.createElement("a");
            downloadAnchorNode.setAttribute("href", url);
            downloadAnchorNode.setAttribute("download", fileName);
            document.body.appendChild(downloadAnchorNode);
            downloadAnchorNode.click();
        }
    }
    /**
     * 
     * @param {DefaultShareData} item 
     * @param {string} textTmp
     */
    async fork(item, textTmp) {
        // if (item.userId === USER_ID) {
        //     showToast("Your own project!", "warning");
        //     return;
        // }
        if (!item.attachments) {
            let res = await axios.post(`/api/grove/linkTo`, item);
            if (!res.data.status) {
                if (res.data.content instanceof Object) {
                    item.attachments = res.data.content;
                }
            }
        }
        let files = new Set();//attachments
        files.add(item.fileKey);
        let content = await copyShareFile(item, files, textTmp);
        this.react_component.setState({
            modalType: ModalType.Fork,
            treeSelectType: SelectType.dir,
            sourceFile: item,
            modalData: { content, files }
        });
    }
    isLinkDataReadOnly() {
        return this.linkData && this.linkData.tag === TAG_TYPE.published &&
            this.linkData.status <= ShareProjectCanOptions['View'];
    }
}