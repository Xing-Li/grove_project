import _ from 'lodash';
import { autoObjectsType, nanOrBlank } from 'util/helpers';
import { ShowType, TitleConfig, lineDefault, groupSeparators } from 'util/chartConfigUtil'
import {
    RenderType, RenderTypeDesc, mongo_Template, dynamo_Template, neo_Template, reD_Template
} from "util/databaseApi"
import { castType } from 'util/utils';
export const Fields = {
    startField: null,
    endField: null,
    contentField: null,
    groupField: null,
    xField: null,
    yField: null,
    seriesField: null,
    angleField: null,
    colorField: null,
    sizeField: null,
    wordField: null,
    weightField: null,
}
export const chartOptionsDefault = _.assign({}, Fields, _.cloneDeep(lineDefault));

export const ChartTypeTemplate = {
    type: '',
    config: _.assign({}, Fields, {
        /**@type {Array<Object<string,number|string>>} */
        data: [{}]
    })
}

export const ChartTypes = {
    "Base Timeline": "Base Timeline",
    "Groups Timeline": "Groups Timeline",
    "SubGroups Timeline": "SubGroups Timeline",

    "Base Range Timeline": "Base Range Timeline",
    "Groups Range Timeline": "Groups Range Timeline",
    "SubGroups Range Timeline": "SubGroups Range Timeline",

    "Basic Line": "Basic Line",
    "Multi Line": "Multi Line",

    "Basic Column": "Basic Column",
    "Multi Column": "Multi Column",

    "Basic Area": "Basic Area",
    "Multi Area": "Multi Area",

    "Basic Bar": "Basic Bar",
    "Multi Bar": "Multi Bar",

    "Basic Pie": "Basic Pie",

    "Basic Scatter": "Basic Scatter",
    "Multi Scatter": "Multi Scatter",

    "Basic Bubble": "Basic Bubble",
    "Multi Bubble": "Multi Bubble",

    "Basic Radar": "Basic Radar",
    "Multi Radar": "Multi Radar",

    "Basic Rose": "Basic Rose",
    "Multi Rose": "Multi Rose",

    "Continuous Heatmap": "Continuous Heatmap",
    "Heatmap": "Heatmap",
    "Uneven Heatmap": "Uneven Heatmap",

    "Basic Word Cloud": "Basic Word Cloud",
    "Word Cloud": "Word Cloud",

    "Waterfall": "Waterfall",
    // "Circle Packing": "Circle Packing",
    // "Sunburst": "Sunburst",

    "Liquid": "Liquid",
}

export const isTimelineType = function (type) {
    return !!~[ChartTypes['Base Timeline'], ChartTypes['Groups Timeline'], ChartTypes['Groups Range Timeline'], ChartTypes['SubGroups Timeline'], ChartTypes['SubGroups Range Timeline']].indexOf(type)
}

export const DataTypes = {
    "period date": "period date",
    "uniq date": "uniq date",
    "date": "date",
    "period number": "period number",
    "uniq number": "uniq number",
    "number": "number",
    "string": "string",
    "any": "any"
}

export const MatchDataTypesConfigs = [
    {
        types: [ChartTypes['Base Timeline']],
        config: { startField: DataTypes.date, contentField: DataTypes.any }
    },
    {
        types: [ChartTypes['Groups Timeline'], ChartTypes['SubGroups Timeline']],
        config: { startField: DataTypes.date, contentField: DataTypes.any, groupField: DataTypes.any }
    },
    {
        types: [ChartTypes['Base Range Timeline']],
        config: { startField: DataTypes.date, endField: DataTypes.date, contentField: DataTypes.any }
    },
    {
        types: [ChartTypes['Groups Range Timeline'], ChartTypes['SubGroups Range Timeline']],
        config: { startField: DataTypes.date, endField: DataTypes.date, contentField: DataTypes.any, groupField: DataTypes.any }
    },
    {
        types: [ChartTypes['Basic Line'], ChartTypes['Basic Area']],
        config: { xField: ["period date", "period number",], yField: "number" }
    },
    {
        types: [ChartTypes['Multi Line'], ChartTypes['Multi Area']],
        config: { xField: ["period date", "period number",], yField: "number", seriesField: "any" }
    },
    {
        types: [ChartTypes['Basic Column'], ChartTypes['Waterfall'], ChartTypes['Basic Bar'], ChartTypes['Basic Radar'], ChartTypes['Basic Rose']],
        config: { xField: 'any', yField: "number" }
    },
    {
        types: [ChartTypes['Multi Column'], ChartTypes['Multi Bar'], ChartTypes['Multi Radar'], ChartTypes['Multi Rose']],
        config: { xField: 'any', yField: "number", seriesField: "any" }
    },
    {
        types: [ChartTypes['Basic Bar']],
        config: { xField: "number", yField: 'any' }
    },
    {
        types: [ChartTypes['Multi Bar']],
        config: { xField: "number", yField: 'any', seriesField: "any" }
    },
    {
        types: [ChartTypes['Basic Pie']],
        config: { colorField: "any", "angleField": "number" }
    },
    {
        types: [ChartTypes['Basic Scatter']],
        config: { xField: "number", yField: "number" }
    },
    {
        types: [ChartTypes['Multi Scatter']],
        config: { xField: "number", yField: "number", colorField: "any" }
    },
    {
        types: [ChartTypes['Basic Bubble']],
        config: { xField: "number", yField: "number", sizeField: "number" }
    },
    {
        types: [ChartTypes['Multi Bubble']],
        config: { xField: "number", yField: "number", sizeField: "number", colorField: "any" }
    },
    {
        types: [ChartTypes['Continuous Heatmap']],
        config: { xField: "number", yField: "number", colorField: "number" }
    },
    {
        types: [ChartTypes['Heatmap']],
        config: { xField: "any", yField: "any", colorField: "number" }
    },
    {
        types: [ChartTypes['Uneven Heatmap']],
        config: { xField: "any", yField: "any", sizeField: "number", colorField: "number" }
    },
    {
        types: [ChartTypes['Word Cloud']],
        config: { wordField: "string", weightField: "number", colorField: "any" }
    },
    {
        types: [ChartTypes['Basic Word Cloud']],
        config: { wordField: "string", weightField: "number" }
    },
]

export const ChartTypeMatchDataTypes = _.reduce(MatchDataTypesConfigs, (prev, dataTypesConfig, index) => {
    _.each(dataTypesConfig.types, (type) => {
        prev[type] = dataTypesConfig.config
    })
    return prev;
}, {})
export const getKeys = function (chartType) {
    if (!chartType || !ChartTypeMatchDataTypes[chartType]) {
        return [];
    }
    return _.keys(ChartTypeMatchDataTypes[chartType])
}

export const Default_Display_Config = { type: ShowType.On };
export const chartDataFunc = function (data, selectedProperties, emptyFills = {}, castColumns = {}, joinColumns = {}) {
    let chartData = selectedProperties.length ? _.reduce(data, (prev, curr, index, list) => {
        let nobj = _.cloneDeep(curr);
        let _hasBlank = false;
        _.each(selectedProperties, (column) => {
            if (nanOrBlank(nobj[column])) {
                if (!nanOrBlank(emptyFills[column])) {
                    nobj[column] = emptyFills[column];//|| nobj[column];
                } else {
                    _hasBlank = true;
                }
            }
            if (castColumns && castColumns[column]) {
                nobj[column] = castType(nobj[column], castColumns[column])
            }
        })
        if (_hasBlank) {
            ///some data not right
        }
        let obj = _.pick(nobj, selectedProperties);
        joinColumns && _.each(joinColumns, (v, k) => {
            if (v && v instanceof Array && v.length > 0) {
                obj[k] = _.values(_.pick(nobj, v)).join(groupSeparators[0]);
            }
        })
        if (list.keysArr) {
            if (!prev.keysArr) {
                prev.keysArr = [];
            }
            prev.keysArr[prev.keysArr.length] = list.keysArr[index];
        }
        prev.push(obj);
        return prev;
    }, []) : [];
    return autoObjectsType(chartData, false);
}
/**
 * has blank data will ignore
 * @param {[]} datas 
 * @param {[]} selectedFields 
 */
export const extractDatas = function (datas, selectedFields) {
    return _.reduce(datas, (prev, curr, index, list) => {
        let obj = selectedFields ? _.pick(curr, selectedFields) : curr;
        if (_.filter(obj, (v, k) => { return nanOrBlank(v) }).length === 0) {
            if (list.keysArr) {
                if (!prev.keysArr) {
                    prev.keysArr = [];
                }
                prev.keysArr[prev.keysArr.length] = list.keysArr[index];
            }
            prev.push(obj);
        } else {
            // console.log(obj);
            ///some data not right //TODO
        }
        return prev;
    }, [])
}
export const countExtractDatas = function (datas, selectedFields) {
    return _.reduce(datas, (prev, curr) => {
        let obj = _.pick(curr, selectedFields);
        if (_.filter(obj, (v, k) => { return nanOrBlank(v) }).length === 0) {//data all right
            prev++;
        } else {
            // console.log(obj);
            ///some data not right //TODO
        }
        return prev;
    }, 0)
}
export const extractOption = function (option) {
    if (typeof option === 'object' && null !== option) {
        let displayConfig = option.displayConfig || Default_Display_Config
        if (displayConfig.type === ShowType.Off) {
            if (_.has(displayConfig, "offValue")) {
                return extractOption(displayConfig.offValue);
            } else {
                return;
            }
        }
        if (displayConfig.type === ShowType.On) {
            if (_.has(displayConfig, "onValue")) {
                return extractOption(displayConfig.onValue);
            }
        }
        if (displayConfig.type === ShowType.Text) {
            return extractOptions(option.value);
        }
        if (displayConfig.type === ShowType.JsText) {
            return extractOptions(option.value);
        }
        if (displayConfig.type === ShowType.Boolean) {
            return extractOptions(option.value);
        }
        if (displayConfig.type === ShowType.Select) {
            return extractOptions(option.value);
        }
        if (displayConfig.type === ShowType.Color) {
            return extractOptions(option.value);
        }
        if (displayConfig.type === ShowType.Padding) {
            return extractOptions(option.value);
        }
        if (displayConfig.type === ShowType.Number) {
            return extractOptions(option.value);
        }
        if (displayConfig.type === ShowType.Remove) {
            return;
        }
        option = extractOptions(option);
        if (_.isEmpty(option) && !(option instanceof Array)) {
            return;
        }
    }
    return option;
}

/**
 * @param {chartOptionsDefault} options 
 * @returns {chartOptionsDefault}
 */
export const extractOptions = function (options) {
    if (typeof options !== 'object') {
        return _.cloneDeep(options);
    }
    let copyOptions = _.cloneDeep(options);
    if (copyOptions instanceof Array) {
        let ret = _.reduce(copyOptions, (prev, option, key) => {
            option = extractOption(option);
            if (undefined !== option) {
                prev.push(option);
            }
            return prev;
        }, []);
        return ret;
    }
    /** reduce BUG,need check object is contains property "length" */
    if (_.has(options, 'length')) {
        delete copyOptions.length;
    }
    let ret = _.reduce(copyOptions, (prev, option, key) => {
        if (key === "displayConfig" || key === "LABEL") {
            return prev;
        }
        option = extractOption(option);
        if (undefined !== option) {
            prev[key] = option;
        }
        return prev;
    }, {});
    if (_.has(options, 'length')) {///reduce bug,need check object is contains length
        ret.length = options.length;
    }
    return ret;
}

/**
 * @type {Object.<string, chartOptionsDefault>}
 */
const mapRenderTypeOptions = {};
/**
 * 
 * @param {string} renderType
 */
export const getRenderTypeOptions = function (renderType) {
    if (!mapRenderTypeOptions[renderType]) {
        if (renderType === RenderType.Chart) {
            mapRenderTypeOptions[renderType] = _.assign({
                title: _.assign({
                    text: "empty chart title"
                }, TitleConfig),
                description: {
                    text: "a simple empty chart"
                },
            });
        } else {
            mapRenderTypeOptions[renderType] = _.assign({
                title: _.assign({
                    text: `empty ${renderType.camelPeakToBlankSplit()} title`
                }, TitleConfig),
                description: {
                    text: `${renderType.camelPeakToBlankSplit()} description`
                },
            }, RenderTypeDesc[renderType].settings)
        }
    }
    return mapRenderTypeOptions[renderType];
}

export const DefaultAntChartConfig = {
    // width: parseInt(370 * 100 / window.innerWidth),
    // height: parseInt(425 * 100 / window.innerWidth),
    chartKey: "default",
    createTime: new Date().getTime(),
    renderType: RenderType.Markdown,
    /** switch file storage of chart data */
    active: false,
    fetchPeriod: 0,
    /**@type {reD_Template} */
    reD: undefined,
    /**
     * @type { mongo_Template }
     */
    mongo: undefined,
    /**
     * @type { dynamo_Template }
     */
    dynamo: undefined,
    /**
     * @type {neo_Template}
     */
    neo: undefined,
    /**input value */
    text: "",
    selectedCategoryFrom: "",
    selectedCategory: "",
    selectedClientType: undefined,
    /** checked properties of Table */
    selectedProperties: [],
    selectedDb: "",
    /** stored chart data */
    chartData: [],
    emptyFills: {},
    castColumns: {},
    joinColumns: {},
    /**@type Array<{key:string,title:string,}> stored chart columns */
    chartColumns: [],
    /**chart type */
    type: ChartTypeTemplate.type,
    /**chart draw options */
    options: _.cloneDeep(getRenderTypeOptions(RenderType.Markdown)),
}