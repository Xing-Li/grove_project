
const cookieParser = require('cookie-parser');
const express = require('express');
const _ = require('lodash');

const ListOfUsers = {};
const SignalingSocket = function (io, socketCallback) {
    socketCallback = socketCallback || function () { };

    io.of('/signal/')
        .on('connection', onConnection);
    // to secure your socket.io usage: (via: docs/tips-tricks.md)
    // io.set('origins', 'https://domain.com');

    function appendUser(socket) {
        let alreadyExist = ListOfUsers[socket.userid];
        console.log("signal socket Id",socket.userid);
        let extra = {};

        if (alreadyExist && alreadyExist.extra) {
            extra = alreadyExist.extra;
        }

        let params = socket.handshake.query;

        if (params.extra) {
            try {
                if (typeof params.extra === 'string') {
                    params.extra = JSON.parse(params.extra);
                }
                extra = params.extra;
            } catch (e) {
                extra = params.extra;
            }
        }

        ListOfUsers[socket.userid] = {
            socket: socket,
            connectedWith: {},
            isPublic: false, // means: isPublicModerator
            extra: extra || {},
            maxParticipantsAllowed: params.maxParticipantsAllowed || 1000
        };
    }

    function onConnection(socket) {
        let params = socket.handshake.query;
        let socketMessageEvent = params.msgEvent || 'RTCMultiConnection-Message';

        let sessionid = params.sessionid;
        let autoCloseEntireSession = params.autoCloseEntireSession;

        if (params.enableScalableBroadcast) {
            if (!ScalableBroadcast) {
                ScalableBroadcast = require('./ScalableBroadcast.js');
            }
            ScalableBroadcast(socket, params.maxRelayLimitPerUser);
        }

        // [disabled]
        // if (false && !!ListOfUsers[params.userid]) {
        //     params.dontUpdateUserId = true;

        //     let useridAlreadyTaken = params.userid;
        //     params.userid = (Math.random() * 1000).toString().replace('.', '');
        //     socket.emit('userid-already-taken', useridAlreadyTaken, params.userid);
        //     return;
        // }

        socket.userid = params.userid;
        appendUser(socket);

        if (autoCloseEntireSession == 'false' && Object.keys(ListOfUsers).length == 1) {
            socket.shiftModerationControlBeforeLeaving = true;
        }

        socket.on('shift-moderator-control-on-disconnect', function () {
            socket.shiftModerationControlBeforeLeaving = true;
        });

        socket.on('extra-data-updated', function (extra) {
            try {
                if (!ListOfUsers[socket.userid]) return;
                ListOfUsers[socket.userid].extra = extra;

                for (let user in ListOfUsers[socket.userid].connectedWith) {
                    ListOfUsers[user].socket.emit('extra-data-updated', socket.userid, extra);
                }
            } catch (e) {
                pushLogs('extra-data-updated', e);
            }
        });

        socket.on('get-remote-user-extra-data', function (remoteUserId, callback) {
            callback = callback || function () { };
            if (!remoteUserId || !ListOfUsers[remoteUserId]) {
                callback('remoteUserId (' + remoteUserId + ') does NOT exist.');
                return;
            }
            callback(ListOfUsers[remoteUserId].extra);
        });

        socket.on('become-a-public-moderator', function () {
            try {
                if (!ListOfUsers[socket.userid]) return;
                ListOfUsers[socket.userid].isPublic = true;
            } catch (e) {
                pushLogs('become-a-public-moderator', e);
            }
        });

        let dontDuplicateListeners = {};
        socket.on('set-custom-socket-event-listener', function (customEvent) {
            if (dontDuplicateListeners[customEvent]) return;
            dontDuplicateListeners[customEvent] = customEvent;

            socket.on(customEvent, function (message) {
                try {
                    socket.broadcast.emit(customEvent, message);
                } catch (e) { }
            });
        });

        socket.on('dont-make-me-moderator', function () {
            try {
                if (!ListOfUsers[socket.userid]) return;
                ListOfUsers[socket.userid].isPublic = false;
            } catch (e) {
                pushLogs('dont-make-me-moderator', e);
            }
        });

        socket.on('get-public-moderators', function (userIdStartsWith, callback) {
            try {
                userIdStartsWith = userIdStartsWith || '';
                let allPublicModerators = [];
                for (let moderatorId in ListOfUsers) {
                    if (ListOfUsers[moderatorId].isPublic && moderatorId.indexOf(userIdStartsWith) === 0 && moderatorId !== socket.userid) {
                        let moderator = ListOfUsers[moderatorId];
                        allPublicModerators.push({
                            userid: moderatorId,
                            extra: moderator.extra
                        });
                    }
                }

                callback(allPublicModerators);
            } catch (e) {
                pushLogs('get-public-moderators', e);
            }
        });

        socket.on('changed-uuid', function (newUserId, callback) {
            callback = callback || function () { };

            if (params.dontUpdateUserId) {
                delete params.dontUpdateUserId;
                return;
            }

            try {
                if (ListOfUsers[socket.userid] && ListOfUsers[socket.userid].socket.userid == socket.userid) {
                    if (newUserId === socket.userid) return;

                    let oldUserId = socket.userid;
                    ListOfUsers[newUserId] = ListOfUsers[oldUserId];
                    ListOfUsers[newUserId].socket.userid = socket.userid = newUserId;
                    delete ListOfUsers[oldUserId];

                    callback();
                    return;
                }

                socket.userid = newUserId;
                appendUser(socket);

                callback();
            } catch (e) {
                pushLogs('changed-uuid', e);
            }
        });

        socket.on('set-password', function (password) {
            try {
                if (ListOfUsers[socket.userid]) {
                    ListOfUsers[socket.userid].password = password;
                }
            } catch (e) {
                pushLogs('set-password', e);
            }
        });

        socket.on('disconnect-with', function (remoteUserId, callback) {
            try {
                if (ListOfUsers[socket.userid] && ListOfUsers[socket.userid].connectedWith[remoteUserId]) {
                    delete ListOfUsers[socket.userid].connectedWith[remoteUserId];
                    socket.emit('user-disconnected', remoteUserId);
                }

                if (!ListOfUsers[remoteUserId]) return callback();

                if (ListOfUsers[remoteUserId].connectedWith[socket.userid]) {
                    delete ListOfUsers[remoteUserId].connectedWith[socket.userid];
                    ListOfUsers[remoteUserId].socket.emit('user-disconnected', socket.userid);
                }
                callback();
            } catch (e) {
                pushLogs('disconnect-with', e);
            }
        });

        socket.on('close-entire-session', function (callback) {
            try {
                let connectedWith = ListOfUsers[socket.userid].connectedWith;
                Object.keys(connectedWith).forEach(function (key) {
                    if (connectedWith[key] && connectedWith[key].emit) {
                        try {
                            connectedWith[key].emit('closed-entire-session', socket.userid, ListOfUsers[socket.userid].extra);
                        } catch (e) { }
                    }
                });

                delete shiftedModerationControls[socket.userid];
                callback();
            } catch (e) {
                pushLogs('close-entire-session', e);
            }
        });

        socket.on('check-presence', function (userid, callback) {
            if (!ListOfUsers[userid]) {
                callback(false, userid, {});
            } else {
                callback(userid !== socket.userid, userid, ListOfUsers[userid].extra);
            }
        });

        function onMessageCallback(message) {
            try {
                if (!ListOfUsers[message.sender]) {
                    socket.emit('user-not-found', message.sender);
                    return;
                }

                if (!message.message.userLeft && !ListOfUsers[message.sender].connectedWith[message.remoteUserId] && !!ListOfUsers[message.remoteUserId]) {
                    ListOfUsers[message.sender].connectedWith[message.remoteUserId] = ListOfUsers[message.remoteUserId].socket;
                    ListOfUsers[message.sender].socket.emit('user-connected', message.remoteUserId);

                    if (!ListOfUsers[message.remoteUserId]) {
                        ListOfUsers[message.remoteUserId] = {
                            socket: null,
                            connectedWith: {},
                            isPublic: false,
                            extra: {},
                            maxParticipantsAllowed: params.maxParticipantsAllowed || 1000
                        };
                    }

                    ListOfUsers[message.remoteUserId].connectedWith[message.sender] = socket;

                    if (ListOfUsers[message.remoteUserId].socket) {
                        ListOfUsers[message.remoteUserId].socket.emit('user-connected', message.sender);
                    }
                }

                if (ListOfUsers[message.sender].connectedWith[message.remoteUserId] && ListOfUsers[socket.userid]) {
                    message.extra = ListOfUsers[socket.userid].extra;
                    ListOfUsers[message.sender].connectedWith[message.remoteUserId].emit(socketMessageEvent, message);
                }
            } catch (e) {
                pushLogs('onMessageCallback', e);
            }
        }

        function joinARoom(message) {
            let roomInitiator = ListOfUsers[message.remoteUserId];

            if (!roomInitiator) {
                return;
            }

            let usersInARoom = roomInitiator.connectedWith;
            let maxParticipantsAllowed = roomInitiator.maxParticipantsAllowed;

            if (Object.keys(usersInARoom).length >= maxParticipantsAllowed) {
                socket.emit('room-full', message.remoteUserId);

                if (roomInitiator.connectedWith[socket.userid]) {
                    delete roomInitiator.connectedWith[socket.userid];
                }
                return;
            }

            let inviteTheseUsers = [roomInitiator.socket];
            Object.keys(usersInARoom).forEach(function (key) {
                inviteTheseUsers.push(usersInARoom[key]);
            });

            let keepUnique = [];
            inviteTheseUsers.forEach(function (userSocket) {
                if (userSocket.userid == socket.userid) return;
                if (keepUnique.indexOf(userSocket.userid) != -1) {
                    return;
                }
                keepUnique.push(userSocket.userid);

                if (params.oneToMany && userSocket.userid !== roomInitiator.socket.userid) return;

                message.remoteUserId = userSocket.userid;
                userSocket.emit(socketMessageEvent, message);
            });
        }

        let numberOfPasswordTries = 0;
        socket.on(socketMessageEvent, function (message, callback) {
            if (message.remoteUserId && message.remoteUserId === socket.userid) {
                // remoteUserId MUST be unique
                return;
            }

            try {
                if (message.remoteUserId && message.remoteUserId != 'system' && message.message.newParticipationRequest) {
                    if (ListOfUsers[message.remoteUserId] && ListOfUsers[message.remoteUserId].password) {
                        if (numberOfPasswordTries > 3) {
                            socket.emit('password-max-tries-over', message.remoteUserId);
                            return;
                        }

                        if (!message.password) {
                            numberOfPasswordTries++;
                            socket.emit('join-with-password', message.remoteUserId);
                            return;
                        }

                        if (message.password != ListOfUsers[message.remoteUserId].password) {
                            numberOfPasswordTries++;
                            socket.emit('invalid-password', message.remoteUserId, message.password);
                            return;
                        }
                    }

                    if (ListOfUsers[message.remoteUserId]) {
                        joinARoom(message);
                        return;
                    }
                }

                if (message.message.shiftedModerationControl) {
                    if (!message.message.firedOnLeave) {
                        onMessageCallback(message);
                        return;
                    }
                    shiftedModerationControls[message.sender] = message;
                    return;
                }

                // for v3 backward compatibility; >v3.3.3 no more uses below block
                if (message.remoteUserId == 'system') {
                    if (message.message.detectPresence) {
                        if (message.message.userid === socket.userid) {
                            callback(false, socket.userid);
                            return;
                        }

                        callback(!!ListOfUsers[message.message.userid], message.message.userid);
                        return;
                    }
                }

                if (!ListOfUsers[message.sender]) {
                    ListOfUsers[message.sender] = {
                        socket: socket,
                        connectedWith: {},
                        isPublic: false,
                        extra: {},
                        maxParticipantsAllowed: params.maxParticipantsAllowed || 1000
                    };
                }

                // if someone tries to join a person who is absent
                if (message.message.newParticipationRequest) {
                    let waitFor = 60 * 10; // 10 minutes
                    let invokedTimes = 0;
                    (function repeater() {
                        if (typeof socket == 'undefined' || !ListOfUsers[socket.userid]) {
                            return;
                        }

                        invokedTimes++;
                        if (invokedTimes > waitFor) {
                            socket.emit('user-not-found', message.remoteUserId);
                            return;
                        }

                        // if user just come online
                        if (ListOfUsers[message.remoteUserId] && ListOfUsers[message.remoteUserId].socket) {
                            joinARoom(message);
                            return;
                        }

                        setTimeout(repeater, 1000);
                    })();

                    return;
                }

                onMessageCallback(message);
            } catch (e) {
                pushLogs('on-socketMessageEvent', e);
            }
        });

        socket.on('disconnect', function () {
            try {
                if (socket && socket.namespace && socket.namespace.sockets) {
                    delete socket.namespace.sockets[this.id];
                }
            } catch (e) {
                pushLogs('disconnect', e);
            }

            try {
                let message = shiftedModerationControls[socket.userid];

                if (message) {
                    delete shiftedModerationControls[message.userid];
                    onMessageCallback(message);
                }
            } catch (e) {
                pushLogs('disconnect', e);
            }

            try {
                // inform all connected users
                if (ListOfUsers[socket.userid]) {
                    let firstUserSocket = null;

                    for (let s in ListOfUsers[socket.userid].connectedWith) {
                        if (!firstUserSocket) {
                            firstUserSocket = ListOfUsers[socket.userid].connectedWith[s];
                        }

                        ListOfUsers[socket.userid].connectedWith[s].emit('user-disconnected', socket.userid);

                        if (ListOfUsers[s] && ListOfUsers[s].connectedWith[socket.userid]) {
                            delete ListOfUsers[s].connectedWith[socket.userid];
                            ListOfUsers[s].socket.emit('user-disconnected', socket.userid);
                        }
                    }

                    if (socket.shiftModerationControlBeforeLeaving && firstUserSocket) {
                        firstUserSocket.emit('become-next-modrator', sessionid);
                    }
                }
            } catch (e) {
                pushLogs('disconnect', e);
            }

            delete ListOfUsers[socket.userid];
        });

        if (socketCallback) {
            socketCallback(socket);
        }
    }
};



function pushLogs() {
    console.log(arguments.toString());
}


const UsersRouter = express.Router();
UsersRouter.use('/users', function (req, res) {
    res.send({
        status: 0,
        message: "Success",
        content: _.map(ListOfUsers,function(socketUser){
            return socketUser.socket.userid;
        })
    })
});

module.exports = SignalingSocket;
module.exports.UsersRouter = UsersRouter;