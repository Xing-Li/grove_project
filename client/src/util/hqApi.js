/**
 * An observer watches a variable, being notified via asynchronous callback 
 * whenever the variable changes state. See the standard inspector for reference.
 */
export class IObservers {
    /**
     * Called shortly before the variable is computed. For a generator variable, 
     * this occurs before the generator is constructed, but not before each 
     * subsequent value is pulled from the generator.
     */
    pending = () => { }
    /**
     * Called shortly after the variable is fulfilled with a new value.
     * @param {*} value 
     */
    fulfilled = (value) => { }
    /**
     * Called shortly after the variable is rejected with the given error.
     * @param {Error} error 
     */
    rejected = (error) => { }
    /**
     * delete inspector cache
     */
    delete = () => { }
    /**
     * set hint name of variant
     * 
     * @param {string} tname 
     */
    setTname = (tname) => { }
    /**
     * 
     * @param {function} successFunc 
     */
    setSuccessFunc = (successFunc) => { }
    /**
     * 
     * @param {function} failFunc 
     */
    setFailFunc = (failFunc) => { }
}
/**
 * A variable defines a piece of state in a reactive program, 
 * akin to a cell in a spreadsheet. Variables may be named to 
 * allow the definition of derived variables: variables whose 
 * value is computed from other variables’ values. Variables 
 * are scoped by a module and evaluated by a runtime.
 */
export class IVariable {
    /**
     * Redefines this variable to have the specified name, taking the variables with the names specified in inputs as arguments to the specified definition function.
     *  If name is null or not specified, this variable is anonymous and may not be referred to by other variables.
     *  The named inputs refer to other variables (possibly imported) in this variable’s module. 
     * Circular inputs are not allowed; the variable will throw a ReferenceError upon evaluation.
     *  If inputs is not specified, it defaults to the empty array. If definition is not a function, the variable is defined to have the constant value of definition.
     * 
     * The definition function may return a promise; derived variables will be computed after the promise resolves. 
     * The definition function may likewise return a generator; the runtime will pull values from the generator on every animation frame, or if the generator yielded a promise, after the promise is resolved. 
     * When the definition is invoked, the value of this is the variable’s previous value, or undefined if this is the first time the variable is being computed under its current definition. 
     * Thus, the previous value is preserved only when input values change; it is not preserved if the variable is explicitly redefined.
     * 
     * @param {string} [name] 
     * @param {[]} [inputs] 
     * @param {Function} [definition] 
     * @returns  may return a promise
     */
    define = async (name, inputs, definition) => { }
    /**
     * import {name as alias} from "module"
     * 
     * Redefines this variable as an alias of the variable with the specified name in the specified module. 
     * The subsequent name of this variable is the specified name, or if specified, the given alias. 
     * The order of arguments corresponds to the standard import statement: import {name as alias} from "module". 
     * 
     * @param {string} name 
     * @param {string} [alias] 
     * @param {*} module 
     */
    import = (name, alias, module) => { };
    /**
     * Deletes this variable’s current definition and name, if any. 
     * Any variable in this module that references this variable as an input will subsequently throw a ReferenceError. 
     * If exactly one other variable defined this variable’s previous name, such that that variable throws a ReferenceError due to its duplicate definition,
     * that variable’s original definition is restored.
     */
    delete = () => { }
}
/**
 * A module is a namespace for variables; within a module, variables 
 * should typically have unique names. Imports allow variables to be 
 * referenced across modules.
 */
export class IModule {
    constructor() {
        this._runtime = {};
        /**
         * @type Map<string,IVariable>
         */
        this._scope = new Map();
    }
    /**
     * 
     * @param {IObservers} [observer] 
     * @returns {IVariable}
     */
    variable = (observer) => { }
    /**
     * 
     * @param {Array<{name:string,alias:string}>|string} specifiers 
     * @param {*} source 
     */
    derive = (specifiers, source) => { }
    /**
     * A convenience method for variable.define; equivalent to:
     * 
     * module.variable().define(name, inputs, definition)
     * 
     * @param {string} [name] 
     * @param {[string]} [inputs] 
     * @param {function|*} definition 
     * @returns  may return a promise
     */
    define = (name, inputs, definition) => { }
    /**
     * A convenience method for variable.import; equivalent to:
     * 
     * module.variable().import(name, alias, from)
     * 
     * @param {string} name 
     * @param {string} [alias] 
     * @param {*} from 
     */
    import = (name, alias, from) => { }
    /**
     * 
     * @param {string} name 
     * @param {[string]} inputs 
     * @param {function|*} definition 
     */
    redefine = (name, inputs, definition) => { }
    /**
     * Returns a promise to the next value of the variable with the specified name on this module. If no such variable exists, or if more than one variable has the specified name, throws a runtime error.
     * 
     * @param {string} name 
     * @returns {Promise}
     */
    value = (name = "") => { }
}
export class IRuntime {
    /**
     * 
     * @param {function} [define] 
     * @param {function} [observer] 
     * @returns {IModule}
     */
    module = (define, observer) => { };
    /**
     * Disposes this runtime, invalidating all active variables and disabling future computation.
     */
    dispose = () => { }
}

export const VariantType = {
    import: "import",
    variant: "variant"
}
export class ShapeInspector {
    constructor() {
        /**
         * @type{IObservers}
         */
        this.inspector = new IObservers();
        /**
         * @type {IVariable}
         */
        this.varia = new IVariable();
        /**@type {Array<string>} */
        this.args = []
    }
    func = () => { }
};
export class ShapeElement extends HTMLElement {
    constructor() {
        /**@type{ShapeInspector} */
        this.vary = new ShapeInspector();
        /**@type {Array<string>} */
        this.cacheNames = []
        /**@type {Array<string>} */
        this.args = []
    }
}
export class ShapeVariable {
    constructor() {
        this.type = VariantType.variant;
        this.name = "";
        this.args = [];
        this.func = () => { };
        this.varia = new IVariable();
        /**
         * @type {ShapeRedefine[]}
         */
        this.redefines = [];
    }
};
export class ShapeRedefine {
    constructor() {
        this.type = VariantType.variant;
        this.name = "";
        this.args = [];
        this.func = () => { };
    }
}

export class ShapeModule extends IModule {
    constructor() {
        this._expire = false;
        this._moduleName = "";
        this._cachedata = {
            /** 
             * all variables 
             * 
             * @type {Object.<string,ShapeVariable>}
             */
            variables: {},
            /** 
             * all inspectors 
             * 
             * @type {[ShapeInspector]}
             */
            inspectors: [],
            /** 
             * cache-data of variables which func used graph api
             * <variableName, variableName>
             * @type {Object.< string,string> }
             */
            graphVariables: {},
            /**
             * cache-data of inspectors which func used graph api
             * @type {[ShapeInspector]}
             */
            graphInspectors: [],
            /** 
             * import modules
             * <moduleName, {names,localNames}>
             * @type {Object.<string,{names:[],localNames:[]}> }
             */
            importModules: {},
        }
    }

    /**
    * 
    * @param {ShapeEditor} editor
    * @param {String} text text to run
    * @param {HTMLElement} declare element that for variant declare
    * @param {ShapeElement} data element that for content display
    * @param {Function} successFunc the function after run success
    * @param {Function} failFunc the function after run fail
    * @param {boolean} now run it right now
    * @param {string} dname default name of block element 
    * @returns the result data
    */
    runEx = async function (editor, text, declare, data, successFunc, failFunc, now, dname) {
    }
    /**
     * inspector
     * 
     * @param {ShapeElement} ele element
     * @param {Array<string>} args arguments
     * @param {Function} func function
     * @param {String} tname hint name of variant
     * @param {String} dname default name of block element
     * @param {Function} successFunc function call when Inspector fulfilled
     * @param {Function} failFunc function call when Inspector rejected as Error occured
     * @param {boolean} now flag if run it right now
     * @returns  may return a promise
     */
    inspect = async function (ele, args, func, tname, dname, successFunc, failFunc, now) {
    }
    /**
     * variable extends
     * 
     * @param {string} name variant name
     * @param {[]} args arguments
     * @param {Function} func function to define variant
     * @param {boolean} now flag if run it right now
     * @returns  may return a promise
     */
    variableEx = async function (name, args = [], func, now) {
    }
    /**
     * delete variables
     * 
     * @param {string[]} variableNames 
     */
    deleteVariables = function (variableNames) {
    }
    /**
     * remove element's own variables
     * 
     * @param {ShapeElement} ele 
     */
    removeEleVariables = function (ele) {
    }
}

export class ShapeEditor {
    /**
     * settings's databases config
     */
    getDatabases = () => { }
    /**
     * @return {string} module name of current file
     */
    getSelfModuleName = () => { }
    /**
     * get file content from storage/ImageCache/server
     * @param {string} fileKey
     * @param {string} referFileKey may be is refer file
     * @returns {string|Blob} return null if folder or Blob if image or string or throw exception if fileName not exist in cache or network exception
     */
    getReferRealFileContent = (fileKey, referFileKey) => { }
    /**
     * check files...need enhause @xing '%' stands for {ROOT_URI}
     * @param {string} fileKey 
     * @param {string} referFileKey 
     * @returns {string}
     */
    getFileAttachmentUrl = (fileKey, referFileKey) => { }
    /**
     * 
     * @param {string} moduleName 
     * @returns {boolean}
     */
    isMainModule = (moduleName) => { }
    /**
    * @returns {ShapeModule}
    */
    getMain = () => { }
    /**
     * record imported module
     * 
     * @param {string} moduleName 
     * @param {string} referFileKey 
     */
    recordModule = (moduleName, referFileKey) => { }
    /**
     * get imported modules
     * @returns {[]}
     */
    getModuleNames = () => { }
    /**
     * editor's key
     * @returns {string} in fact is a absolute path of file, example: "uploadUri + { file || linkFile }.key"
     */
    getKey = () => { }
    /**
     * editor's key transfered string
     * 
     * @returns {string} "common" if commonEditor or (editor's key).replaceNoWordToUnderline();
     */
    getTransferKey = () => { }
    /**
    * 
    * @param {String} text text to run
    * @param {HTMLElement} declare element that for variant declare
    * @param {ShapeElement} data element that for content display
    * @param {Function} successFunc the function after run success
    * @param {Function} failFunc the function after run fail
    * @param {boolean} now run it right now
    * @param {string} dname default name of block element 
    * @returns the result data
    */
    runFunc = async function (text, declare, data, successFunc, failFunc, now, dname) {
    }
    /**
     * delete element's variables
     * @param {ShapeElement} ele 
     */
    deleteCacheV(ele) {
    }
}