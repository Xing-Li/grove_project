import observe from "@observablehq/stdlib/src/generators/observe.js";
import { require as requireDefault, requireFrom, resolveBase, resolveAnywhere } from "./grove-require";

const requireModule = requireFrom(resolveAnywhere);
export default function () {
  return observe(function (change) {
    var graphxrApi = change(window.graphxrApi);
    requireModule('@kineviz/graphxr-api@0.0.227').then(async module => {
      if (!module) {
        window.graphxrApi = new Error("can not load module");
        change(window.graphxrApi)
        return
      }
      window.graphxrApi = await module.getApi();
      change(window.graphxrApi)
      _app.controller.API.on("change", () => {
        actions.variable(actions.types.REFRESH_GRAPH, [], () => {
          return window.graphxrApi;
        });
      }, "groveRefreshChange");
    })
    return function () {
      //remove observe???
    };
  });
}
