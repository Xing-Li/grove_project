import { Collapse, Table } from 'antd';
import { getNodesByLabel, _graph } from "common/graphOps";
import * as d3 from 'd3';
import { v4 as uuidv4, NIL as NIL_UUID } from 'uuid';
import _ from 'lodash';
import { autoObjectsType, calcAutoType, extractAllKeys, isFileArrow, isFileCsv, isFileDb, isFileExcel, isFileJson } from 'util/helpers';
import { CategoryFromType, DatabaseType, getColumnName, getColumnType, ReDatabaseTypes } from "util/utils";
import { ShareData, DbClients } from "../../stdlib/ExInspector";
import { chartDataFunc, DefaultAntChartConfig } from './comp/OptionsTypes';
import RenderObject from './comp/RenderObject';
import { ShapeEditor } from 'util/hqApi';

const { DefaultDatabase, RenderType } = require("util/databaseApi");
const { Panel } = Collapse;

export const StepTypes = {
    UploadData: "UploadData",
    ChooseChart: "ChooseChart",
    ConfigChart: "ConfigChart",
    ExportFiles: "ExportFiles",
}

export const NextStepsOfRenderType = (renderType) => {
    if (renderType === RenderType.Chart) {
        return [StepTypes.ChooseChart, StepTypes.ConfigChart, StepTypes.ExportFiles];
    } else {
        return [StepTypes.ConfigChart, StepTypes.ExportFiles];
    }
}

export const MAX_SIZE = 10000;
export const NEW_CHART_CONFIG = _.cloneDeep(_.assign({}, DefaultAntChartConfig, { active: true }));
/**
 * init charts component
 */
export const genDefaultAntChartData = function () {
    let defaultAntChartData = { chartConfigs: {}, layouts: {} };
    let chartConfig = _.cloneDeep(NEW_CHART_CONFIG);
    chartConfig.createTime = new Date().getTime();
    chartConfig.chartKey = uuidv4();
    defaultAntChartData.chartConfigs[chartConfig.chartKey] = chartConfig;
    return defaultAntChartData;
}
export const isSelectorValid = ((dummyElement) =>
    (selector) => {
        try { dummyElement.querySelector(selector) } catch { return false }
        return true
    })(document.createDocumentFragment())

/**
 * 
 * @param {ShapeEditor} editor 
 * @param {CategoryFromType} selectedCategoryFrom 
 */
export const getCatogeries = function (editor, selectedCategoryFrom) {
    if (!selectedCategoryFrom) return [];
    switch (selectedCategoryFrom) {
        case CategoryFromType.main:
            return _.uniq(_graph.nodes.map((n) => n.data.detail.type))
        case CategoryFromType.shareData:
            return ShareData[editor.getSelfModuleName()] ? _.reduce(ShareData[editor.getSelfModuleName()], (prev, value, key) => {
                if ((value instanceof Array || window.opener && value instanceof window.opener.Array || value instanceof NewArray) && value.length > 0) {
                    prev.push(key);
                }
                return prev;
            }, []) : [];
        case CategoryFromType.files:
            return _.reduce(editor.getFileList(), (prev, value, key) => {
                if (isFileCsv(value) || isFileJson(value) || isFileDb(value) || isFileExcel(value) || isFileArrow(value)) {
                    prev.push(value);
                }
                return prev;
            }, []);
        case CategoryFromType.database:
            return _.reduce(editor.getDatabases(), /** @param {DefaultDatabase} value */(prev, value, index) => {
                if (~ReDatabaseTypes.indexOf(value.type)) {
                    prev.push(value);
                }
                return prev;
            }, []);
        case CategoryFromType.neo4j:
            return _.reduce(editor.getDatabases(), /** @param {DefaultDatabase} value */(prev, value, index) => {
                if (value.type === DatabaseType.Neo4j) {
                    prev.push(value);
                }
                return prev;
            }, []);
        case CategoryFromType.mongodb:
            return _.reduce(editor.getDatabases(), /** @param {DefaultDatabase} value */(prev, value, index) => {
                if (value.type === DatabaseType.MongoDB) {
                    prev.push(value);
                }
                return prev;
            }, []);
        case CategoryFromType.dynamodb:
            return _.reduce(editor.getDatabases(), /** @param {DefaultDatabase} value */(prev, value, index) => {
                if (value.type === DatabaseType.DynamoDB) {
                    prev.push(value);
                }
                return prev;
            }, []);
        case CategoryFromType.client:
            return _.reduce(window.DbClients[editor.getSelfModuleName()] || {}, /** @param {DefaultDatabase} value */(prev, value, key) => {
                if (!~prev.indexOf(value)) {
                    value.key = key;
                    prev.push(value);
                }
                return prev;
            }, []);
    }
}

export function extractNodeDataAllKeys(nodes) {
    let obj = {};
    _.each(nodes, (n) => {
        _.each(n.data.detail.data, (value, key) => {
            if (undefined === obj[key]) {
                obj[key] = value;
            }
        })
    })
    return _.keys(obj);
}

export const getMainColumnsData = function (selectedCategory, date = false) {
    let columnsData = { properties: [], columns: [], data: [] };
    let nodes = getNodesByLabel(selectedCategory);
    columnsData.properties = extractNodeDataAllKeys(nodes);
    columnsData.columns = _.reduce(columnsData.properties, (prev, property, index) => {
        prev.push({
            key: property,
            title: property,
            dataIndex: property,
        });
        return prev;
    }, []);
    //store nodes's id,maybe used for timeline
    let keysArr = [];
    columnsData.data = nodes.map((n) => { keysArr[keysArr.length] = n.id; return n.data.detail.data });
    columnsData.data.keysArr = keysArr;
    autoObjectsType(columnsData.data, date);
    return columnsData;
}

export const getShareDataColumnsData = function (moduleName, selectedCategory, date = false) {
    let columnsData = { properties: [], columns: [], data: [] };
    let nodes = ShareData[moduleName] && ShareData[moduleName][selectedCategory] || [];
    columnsData.properties = extractAllKeys(nodes);
    columnsData.columns = _.reduce(columnsData.properties, (prev, property, index) => {
        prev.push({
            key: property,
            title: property,
            dataIndex: property,
        });
        return prev;
    }, []);
    columnsData.data = nodes;
    autoObjectsType(columnsData.data, date);
    return columnsData;
}

export const getDbClients = function (moduleName, selectedCategory) {
    return DbClients[moduleName] && DbClients[moduleName][selectedCategory] || null;
}

export const getFilesColumnsData = async function (selectedCategory, typed) {
    let columnsData = { properties: [], columns: [], data: [] };
    let fileData = window.currentEditor.react_component.getFileData(selectedCategory);
    if (isFileDb(fileData) || isFileExcel(fileData) || isFileArrow(fileData)) {
        return columnsData;
    }
    let text = await window.currentEditor.react_component.getRealFileContentByFileKey(selectedCategory)
    let nodes;
    if (isFileCsv(fileData)) {
        nodes = d3.csvParse(text, typed && d3.autoType);
    } else if (isFileJson(fileData)) {
        function findNodesFunc(nodes) {
            if (nodes instanceof Array) {
                _.each(nodes, /**
                     * 
                     * @param {{}} v 
                     * @param {number} i 
                     * @param {[]} arr 
                     */ (v, i, arr) => {
                        if (!(typeof v === 'object' && _.filter(v, (value, key) => {
                            return typeof value === 'object'
                        }).length === 0)) {
                            nodes = undefined;
                            return false;
                        }
                    }
                );
            } else if (nodes instanceof Object) {
                let find = undefined;
                _.each(nodes, (v, k) => {
                    let a;
                    if (v instanceof Array || v instanceof Object) {
                        a = findNodesFunc(v);
                        if (a) {
                            find = a;
                            return false;
                        }
                    }
                })
                nodes = find;
            } else {
                nodes = undefined;
            }
            return nodes;
        }
        nodes = JSON.parse(text);
        nodes = findNodesFunc(nodes);
        if (!nodes) {
            return columnsData;
        }
    } else {
        return columnsData;
    }
    columnsData.properties = extractAllKeys(nodes);
    columnsData.columns = _.reduce(columnsData.properties, (prev, property, index) => {
        prev.push({
            key: property,
            title: property,
            dataIndex: property,
        });
        return prev;
    }, []);
    columnsData.data = nodes;
    autoObjectsType(columnsData.data, typed);
    return columnsData;
}

/**
 * get data from main/files/shareData
 * 
 * @param {*} moduleName editor self module name
 * @param {*} selectedCategoryFrom 
 * @param {*} selectedCategory 
 * @param {boolean} typed is need auto parse data type and parse date
 * @returns {Promise<{properties:[], columns:[], data:[]}>}
 */
export const getColumnsData = async function (moduleName, selectedCategoryFrom, selectedCategory, typed = false) {
    if (!selectedCategoryFrom || !selectedCategory) return { properties: [], columns: [], data: [] };
    switch (selectedCategoryFrom) {
        case CategoryFromType.main: {
            return getMainColumnsData(selectedCategory, typed)
        }
        case CategoryFromType.shareData: {
            return getShareDataColumnsData(moduleName, selectedCategory, typed)
        }
        case CategoryFromType.files: {
            return getFilesColumnsData(selectedCategory, typed)
        }
    }
    return { properties: [], columns: [], data: [] };
}
/**
 * relational databases results to columns data
 * 
 * @param {*} nodes 
 */
export const extractColumnsData = function (nodes) {
    let columnsData = { properties: [], columns: [], data: [] };
    columnsData.properties = extractAllKeys(nodes);
    columnsData.columns = _.reduce(columnsData.properties, (prev, property, index) => {
        prev.push({
            key: property,
            title: property,
            dataIndex: property,
        });
        return prev;
    }, []);
    columnsData.data = nodes;
    autoObjectsType(columnsData.data, false);
    return columnsData;
}
export const resultsToChartData = function (results, selectedProperties, emptyFills, castColumns, joinColumns) {
    const { data, properties, columns } = extractColumnsData(results);
    let chartData = chartDataFunc(data, selectedProperties, emptyFills, castColumns, joinColumns)
    let chartColumns = _.reduce([...selectedProperties,
    ..._.reduce(joinColumns, (prev, v, k) => { if (k && v && v instanceof Array && v.length > 0) { prev.push(k) } return prev; }, [])], (prev, property, index) => {
        prev.push({
            key: property,
            title: property,
            dataIndex: property,
        });
        return prev;
    }, []);
    return { chartData, chartColumns };
}
export const DataComp = function (props = { columns: [], data: [], open: false, header: "Data" }) {
    const { columns, data, open, header } = props;
    let nData = _.map(data, (obj, index) => {
        if (!obj.key) {
            let nobj = _.cloneDeep(obj);
            nobj.key = index;
            return nobj;
        }
        return obj
    })
    return <Collapse defaultActiveKey={open ? "Data" : undefined} style={{
        zIndex: 1,
        position: "absolute"
    }}>
        <Panel header={<span> {header || "Data"}&nbsp;&nbsp;&nbsp;Rows:{nData.length} Cols: {columns.length}</span>
        } key="Data">
            <Table size='small' scroll={{ x: '90%' }} columns={columns} dataSource={nData} sticky />
            <RenderObject obj={calcAutoType(data, true)} table={true}></RenderObject>
        </Panel>
    </Collapse>
}
export const getMoV = function (rule, collectionFields) {
    const { columnName, association, value } = rule;
    let type = getColumnType(_.filter(collectionFields, (f) => { return getColumnName(f) === columnName })[0] || {});
    switch (association) {
        case 'eq':
            if (type === "boolean") {
                return /^true$/i.test(value)
            } else if (~["int", "short", "long", "byte"].indexOf(type)) {
                return parseInt(value);
            } else if (~["double", "float"].indexOf(type)) {
                return parseFloat(value);
            } else if (~["object"].indexOf(type)) {
                return `ObjectId:${value}`;
            } else {
                return value;
            }
        case 'ne':
            if (type === "boolean") {
                return { "$ne": /^true$/i.test(value) }
            } else if (~["int", "short", "long", "byte"].indexOf(type)) {
                return { "$ne": parseInt(value) };
            } else if (~["double", "float"].indexOf(type)) {
                return { "$ne": parseFloat(value) };
            } else if (~["object"].indexOf(type)) {
                return { "$ne": `ObjectId:${value}` };
            } else {
                return { "$ne": value };
            }
        case 'lt':
            if (~["int", "short", "long", "byte"].indexOf(type)) {
                return { "$lt": parseInt(value) };
            } else if (~["double", "float"].indexOf(type)) {
                return { "$lt": parseFloat(value) };
            } else {
                return { "$lt": value };
            }
        case 'gt':
            if (~["int", "short", "long", "byte"].indexOf(type)) {
                return { "$gt": parseInt(value) };
            } else if (~["double", "float"].indexOf(type)) {
                return { "$gt": parseFloat(value) };
            } else {
                return { "$gt": value };
            }
        case 'lte':
            if (~["int", "short", "long", "byte"].indexOf(type)) {
                return { "$lte": parseInt(value) };
            } else if (~["double", "float"].indexOf(type)) {
                return { "$lte": parseFloat(value) };
            } else {
                return { "$lte": value };
            }
        case 'gte':
            if (~["int", "short", "long", "byte"].indexOf(type)) {
                return { "$gte": parseInt(value) };
            } else if (~["double", "float"].indexOf(type)) {
                return { "$gte": parseFloat(value) };
            } else {
                return { "$gte": value };
            }
        case 'in':
            if (~["int", "short", "long", "byte"].indexOf(type)) {
                return {
                    "$in": _.map(_.split(value, ","), (v) => {
                        return parseInt(v)
                    })
                };
            } else if (~["double", "float"].indexOf(type)) {
                return {
                    "$in": _.map(_.split(value, ","), (v) => {
                        return parseFloat(v)
                    })
                };
            } else {
                return {
                    "$in": _.map(_.split(value, ","), (v) => {
                        return v.trim();
                    })
                };
            }
        case 'nin':
            if (~["int", "short", "long", "byte"].indexOf(type)) {
                return {
                    "$nin": _.map(_.split(value, ","), (v) => {
                        return parseInt(v)
                    })
                };
            } else if (~["double", "float"].indexOf(type)) {
                return {
                    "$nin": _.map(_.split(value, ","), (v) => {
                        return parseFloat(v)
                    })
                };
            } else {
                return {
                    "$nin": _.map(_.split(value, ","), (v) => {
                        return v.trim();
                    })
                };
            }
        case 'c':
            return { "$regex": `${value}`, "$options": "i" };
        case 'nc':
            return { "$not": { "$regex": `${value}`, "$options": "i" } };
        case 'n':
            return null;
        case 'nn':
            return { "$ne": null }
    };
}