import { parseCell as parseCellOrigin, peekId } from '@observablehq/parser';
import { Inspector, Runtime } from '@observablehq/runtime';
import { Library } from '@observablehq/stdlib';
import axios from 'axios';
import _ from 'lodash';
import { createLinkElement } from 'stdlib/link';
import { fileSeparator, getFileKey, saveArrayToFile, verFileTag } from '../file/fileUtils';
import ExInspector from "../stdlib/ExInspector";
import ExLibrary from '../stdlib/ExLibrary';
import Common from './Common';
import { addAllGlobalScope } from './hintUtil';
import { ShapeEditor, ShapeElement, ShapeInspector, ShapeModule, ShapeVariable, VariantType } from './hqApi';
import { getCodeModeValue, showToast } from './utils';
const { AsyncFunction, GeneratorFunction, AsyncGeneratorFunction, DEBUG_HQ } = require('./helpers');
const { DefaultShareData, COMMON, SYSTEM_USER, SYSTEM_USER_URI, FILESDIR } = require('./localstorage');



String.prototype.then = function (func) { return func(this) }
window.Library = Library;
window.library = new ExLibrary();
addAllGlobalScope(window.library);
window.runtime = new Runtime(window.library);
window.Runtime = Runtime;
window.Inspector = Inspector;
window.ExInspector = ExInspector;
// window.parseModule = parseModule;
window.peekId = peekId;
window.saveArrayToFile = saveArrayToFile;
/**@type {Object.<string,ShapeModule>} */
const moduleMap = {};
window.moduleMap = moduleMap;

export const parseCell = function () {
    let input = arguments[0].trim();
    let match;
    if (match = input.match(/^export\s+([a-z$_][a-z0-9$_]{0,})\s+(as|to)\s+["'](\S+\.csv)["']$/i)) {
        return {
            variant: match[1],
            type: match[2],
            path: match[3],
        }
    }
    return parseCellOrigin(...arguments);
}
window.parseCell = parseCell;
export const createModule = /**@returns {ShapeModule} */ function (moduleName) {
    /**@type {ShapeModule} */
    let module = window.runtime.module();
    module._moduleName = moduleName;
    module._cachedata = {
        variables: {},
        inspectors: [],
        graphVariables: {},
        graphInspectors: [],
        importModules: {},
    }
    module.runEx = function (editor, text, declare, data, successFunc, failFunc, now, dname) {
        return runFunc(editor, this, text, declare, data, successFunc, failFunc, now, dname);
    }
    module.inspect = function (ele, args, func, tname, dname, successFunc, failFunc, now) {
        return inspectFunc(this, ele, args, func, tname, dname, successFunc, failFunc, now)
    }
    module.removeEleVariables = function (ele) {
        if (!ele) {
            return;
        }
        this.deleteVariables(ele.cacheNames);
        let classNames = _.map(ele.cacheNames, (cacheName) => {
            return `editorjs-anchor-${cacheName.replace(/\s/g, "_")}`;
        })
        ele.classList.remove(...classNames)
        if (ele.vary) {
            ele.vary.varia.delete();
            ele.vary.inspector.delete();
        }
        ele.removeAttribute("id");
        // delete ele.vary;
        delete ele.cacheNames;
        delete ele.args;
    }
    module.variableEx = function (name, args = [], func, now) {
        return variableFunc(this, name, args, func, now)
    }
    module.deleteVariables = function (variableNames) {
        if (variableNames && (typeof variableNames === "object") && (variableNames instanceof Array) && variableNames.length) {
            _.each(variableNames, (variableName) => {
                if (typeof variableName !== "string") {
                    return;
                }
                let names = [variableName, `viewof ${variableName}`, `initial ${variableName}`, `mutable ${variableName}`];
                _.each(names, (name) => {
                    const { variables } = this._cachedata;
                    if (variables[name] !== undefined) {
                        variables[name].varia.delete();
                        delete variables[name];
                    }
                })
            })
        }
    }
    return module;
}

export const ROOT_URI = "/tmp/observable/";
export const canOverWrite = false;
const attachsMapByUrl = {};
/**
 * variable
 * 
 * @param {ShapeModule} module 
 * @param {string} name 
 * @param {[]} args 
 * @param {Function} func 
 * @param {boolean} now run it right now
 */
export const variableFunc = function (module, name, args = [], func, now) {
    const { variables, graphVariables } = module._cachedata;
    if (variables[name] === undefined) {
        variables[name] = {
            type: VariantType.variant,
            name,
            args,
            func,
            varia: module.variable(),
            redefines: undefined
        }
    } else {
        if (!variables[name].redefines) {
            variables[name].redefines = [];
        }
        variables[name].redefines.push({
            type: VariantType.variant,
            name,
            args,
            func
        })
    }
    if (graphVariables) {
        if (~args.indexOf("graphApi")) {
            if (graphVariables[name] === undefined) {
                graphVariables[name] = name;
            }
        } else {
            delete graphVariables[name];
        }
    }
    if (now) {
        let variable = variables[name]
        variable = !variable.redefines ? variable :
            _.assign({}, variable, variable.redefines[variable.redefines.length - 1])
        return variable.varia.define(variable.name, variable.args, variable.func);
    }
}
/**
 * inspector
 * 
 * @param {ShapeModule} module 
 * @param {ShapeElement} ele element
 * @param {Array<string>} args argments
 * @param {Function} func function
 * @param {String} tname hint name of variant
 * @param {String} dname default name of block element
 * @param {Function} successFunc function call when Inspector fulfilled
 * @param {Function} failFunc function call when Inspector rejected as Error occured
 * @param {boolean} now run it right now
 * 
 */
export const inspectFunc = function (module, ele, args, func, tname, dname, successFunc, failFunc, now) {
    const { inspectors, graphInspectors } = module._cachedata;
    /**@type {ExInspector} */
    let inspector;
    if (ele.vary === undefined) {
        inspector = new ExInspector(ele, tname, dname, module._moduleName, successFunc, failFunc);
        ele.vary = {
            inspector: inspector,
            varia: module.variable(inspector),
        }
        inspectors.push(ele.vary);
    }
    inspector = ele.vary.inspector;
    ele.vary.args = args;
    ele.vary.func = func;
    inspector.setTname(tname);
    inspector.setFailFunc(failFunc)
    inspector.setSuccessFunc(successFunc);
    if (graphInspectors) {
        if (~args.indexOf("graphApi")) {
            if (!~graphInspectors.indexOf(ele.vary)) {
                graphInspectors.push(ele.vary)
            }
        } else {
            if (~graphInspectors.indexOf(ele.vary)) {
                graphInspectors.splice(graphInspectors.indexOf(ele.vary), 1);
            }
        }
    }
    if (now) {
        return ele.vary.varia.define(ele.vary.args, ele.vary.func);
    }
}
/**
 * Redefines this variable as an alias of the variable with the specified name in the specified module. 
 * 
 * @param {String[]} names
 * @param {String[]} aliases 
 * @param {ShapeModule} fromModule 
 * @param {ShapeModule} toModule 
 * @param {Array<{name:string,alias:string}>} specifiers point some variables
 */
export const importFunc = async function (names, aliases, fromModule, toModule, specifiers) {
    const { variables } = toModule._cachedata;
    let tmpModule;
    if (undefined !== specifiers && specifiers.length > 0) {
        tmpModule = await fromModule.derive(specifiers, toModule);
    } else {
        tmpModule = fromModule;
    }
    for (let index = 0; index < names.length; index++) {
        let name = names[index];
        let alias = aliases[index];
        if (variables[alias] === undefined) {
            variables[alias] = {
                type: VariantType.import,
                name: alias,
                varia: toModule.variable(),
                redefines: undefined
            };
        } else {
            if (!variables[alias].redefines) {
                variables[alias].redefines = [];
            }
            variables[alias].redefines.push({
                type: VariantType.import,
                name: alias
            })
        }
        await variables[alias].varia.import(name, alias, tmpModule);
    }
}

/**
 * 
 * @param {string} moduleName 
 * @param {string} referFileKey 
 * @returns {ShapeModule}
 */
export const getAndLoadModule = (moduleName, referFileKey) => {
    if (!moduleName) {
        return;
    }
    moduleName = referFileKey ? abosolutePath(moduleName, referFileKey) : moduleName
    if (moduleMap[moduleName] === undefined) {
        moduleMap[moduleName] = createModule(moduleName);
        // console.log("create: " + moduleName);
    }
    return moduleMap[moduleName];
}
export const removeModule = (moduleName, referFileKey) => {
    moduleName = referFileKey ? abosolutePath(moduleName, referFileKey) : moduleName
    if (moduleMap[moduleName]) {
        delete moduleMap[moduleName];
        // console.log("remove: " + moduleName);
    }
}
/**
 * 
 * @param {string} moduleName 
 * @param {string} referFileKey
 */
export const expireModule = (moduleName, referFileKey) => {
    moduleName = referFileKey ? abosolutePath(moduleName, referFileKey) : moduleName
    if (moduleMap[moduleName]) {
        expire(moduleMap[moduleName]);
    }
}
/**
 * 
 * @param {ShapeModule} module 
 */
export const expire = (module) => {
    if (module) {
        module._expire = true;
    }
}
/**
 * 
 * @param {string} moduleName 
 * @param {string} referFileKey
 */
export const disposeModule = (moduleName, referFileKey) => {
    moduleName = referFileKey ? abosolutePath(moduleName, referFileKey) : moduleName
    if (moduleMap[moduleName]) {
        dispose(moduleMap[moduleName]);
    }
}
/**
 * @param {ShapeModule} module 
 */
export const dispose = function (module) {
    if (!module) {
        return;
    }
    const { _cachedata, _cachedata: { inspectors, variables } } = module;
    _.each(_.values(variables), /**@param {ShapeVariable} variable */(variable) => {
        variable.varia.delete();
    });
    _.each(inspectors,  /**@param {ShapeInspector} inspector */(inspector) => {
        inspector.varia.delete();
        inspector.inspector.delete();
    });
    _cachedata.variables = {};
    _cachedata.inspectors = [];
    _cachedata.graphVariables = {};
    _cachedata.graphInspectors = [];
    _cachedata.importModules = {}
    // console.log("dispose: " + module._moduleName);
}
/**
 * @param {ShapeModule} module 
 */
export const eraser = function (module) {
    if (!module) {
        return;
    }
    const { _cachedata, _cachedata: { inspectors } } = module;
    _.each(inspectors, /**@param {ShapeInspector} inspector */(inspector) => {
        inspector.varia.delete();
        inspector.inspector.delete();
    });
    _cachedata.variables = {};
    _cachedata.inspectors = [];
    _cachedata.graphVariables = {};
    _cachedata.graphInspectors = [];
    _cachedata.importModules = {}
}
/**
 * 
 * @param {string} moduleName 
 * @param {string} referFileKey
 * @returns {ShapeModule}
 */
export const getModule = (moduleName, referFileKey) => {
    moduleName = referFileKey ? abosolutePath(moduleName, referFileKey) : moduleName
    if (window.editor.isMainModule(moduleName)) {
        return window.editor.getMain();
    } else if (_.filter(window.tagEditors, /**@param {ShapeEditor} tagEditor */(tagEditor) => {
        return tagEditor.isMainModule(moduleName)
    }).length > 0) {
        return _.filter(window.tagEditors, (tagEditor) => {
            return tagEditor.isMainModule(moduleName)
        })[0].getMain();
    }
    return moduleMap[moduleName];
}
/**
 * import variant from module
 * 
 * @param {ShapeEditor} editor
 * @param {ShapeModule} toModule 
 * @param {String} fromModuleName 
 * @param {String[]} names 
 * @param {String[]} localNames
 * @param {Function} successFunc
 * @param {Function} failFunc
 * @param {{}} cells - Deprecated: transfer some redefine cells
 * @param {Array<{name:string,alias:string}>} specifiers -
 * @param {string} referFileKey
 * @returns
 * 
 */
export const importModule = async (editor, fromModuleName, toModule,
    names, localNames, successFunc, failFunc, cells, specifiers, referFileKey) => {
    try {
        let moduleName = referFileKey ? abosolutePath(fromModuleName, referFileKey) : fromModuleName
        toModule._cachedata.importModules[moduleName] = { names, localNames };
        let fromModule = getModule(fromModuleName, referFileKey);
        editor.recordModule(fromModuleName, referFileKey);
        if (fromModule === undefined) {
            fromModule = getAndLoadModule(fromModuleName, referFileKey);
        } else if ((cells !== undefined && fromModule !== editor.getMain())) {
            fromModule = createModule(`cells_${moduleName}`);//TODO: now not use
        } else if (fromModule._expire) {

        } else {
            await importFunc(names, localNames, fromModule, toModule, specifiers);
            await successFunc();
            return;
        }
        let text = await editor.getReferRealFileContent(fromModuleName, referFileKey);
        if (text === null) {
            failFunc(new Error(`Failed to fetch dynamically imported module:${fromModuleName}`));
            disposeModule(fromModuleName, referFileKey)
            removeModule(fromModuleName, referFileKey);
            return;
        }
        let json = JSON.parse(text);
        let promiseList = [];
        let blocks = _.filter(json.blocks,
            (block) => { return block.type === "codeTool" && block.data.codeData.value });
        _.each(blocks, (block, index, blocks) => {
            promiseList.push(addImportVariable(editor, fromModule, block, blocks, cells, false, /^\%/.test(fromModuleName) && fromModuleName || referFileKey, moduleName));
        })
        await Promise.all(promiseList);
        promiseList = [];
        _.each(blocks, (block, index, blocks) => {
            promiseList.push(addVariable(editor, fromModule, block, blocks, cells, false, /^\%/.test(fromModuleName) && fromModuleName || referFileKey, moduleName));
        })
        await Promise.all(promiseList);
        await runVariables(fromModule);
        fromModule._expire && (delete fromModule._expire);
        await importFunc(names, localNames, fromModule, toModule, specifiers);
        await successFunc();
    } catch (e) {
        disposeModule(fromModuleName, referFileKey)
        removeModule(fromModuleName, referFileKey)
        console.error(e.message);
        failFunc(e);
    }
}

export const getReferRealFileUri = function (fileKey, referFileKey) {
    let uri = null;
    let projectId = null;
    let userId = null;
    if (/^\%/.test(fileKey)) {
        let match = fileKey.match(/^\%((projects\/){0,1}(\w+)\/)(.*)/)
        let tag;
        if (null !== match && match.length === 5) {
            tag = match[1];
            projectId = match[2] !== undefined ? match[3] : COMMON;
            userId = match[2] !== undefined ? undefined : match[3];
            fileKey = match[4];
        } else {
            throw new Error('regex was not ok');
        }
        uri = `${ROOT_URI}${tag}`;
    } else if (!referFileKey) {
        throw new Error('referFileKey can not be null!');
    } else if (/^\%/.test(referFileKey)) {
        let match = referFileKey.match(/^\%((projects\/){0,1}(\w+)\/)(.*)/)
        let tag;
        if (null !== match && match.length === 5) {
            tag = match[1];
            projectId = match[2] !== undefined ? match[3] : COMMON;
            userId = match[2] !== undefined ? undefined : match[3];
        } else {
            throw new Error('regex was not ok');
        }
        uri = `${ROOT_URI}${tag}`;
    } else {
        let match = referFileKey.substring(ROOT_URI.length)
            .match(/^((projects\/){0,1}(\w+)\/)(.*)/)
        let tag;
        if (null !== match && match.length === 5) {
            tag = match[1];
            projectId = match[2] !== undefined ? match[3] : COMMON;
            userId = match[2] !== undefined ? undefined : match[3];
        } else if (referFileKey === SYSTEM_USER_URI) {
            return {
                uri: referFileKey,
                /**  */
                fileKey,
                /** common|projectId */
                projectId: "systemProjectId",
                userId: SYSTEM_USER
            };
        } else {
            throw new Error('regex was not ok');
        }
        uri = referFileKey;
    }
    return {
        uri,
        /**  */
        fileKey,
        /** common|projectId */
        projectId,
        userId
    };
}


export const abosolutePath = function (fileKeyTmp, referFileKey) {
    let { uri, fileKey } = getReferRealFileUri(fileKeyTmp, referFileKey);
    let match = fileKey.match(/(\S+)@(\d+)$/)
    let version = undefined;
    if (null !== match) {
        fileKey = match[1];
        version = parseInt(match[2]);
    }
    return `${uri}${fileKey}${version ?
        `${verFileTag}${fileSeparator}${version}` : ""}`;
}

/**
 * sort variables and run
 * @param {*} module 
 */
export const runVariables = async function (module) {
    const { variables } = module._cachedata;
    let arr = [];
    /** @type {Object.<string,ShapeVariable>} */
    let variants = _.filter(_.values(variables),/**@param {ShapeVariable} variable */(variable) => {
        if (variable.type === VariantType.import) { arr.push(variable.name) }
        return variable.type === VariantType.variant
    })
    let libraryKeys = _.keys(window.library);
    libraryKeys.push("invalidation");
    _.each(variants, (variable) => {
        if (~libraryKeys.indexOf(variable.name)) {
            libraryKeys.splice(libraryKeys.indexOf(variable.name), 1);
        }
    })
    arr.push(...libraryKeys);
    /** @type [ShapeVariable] */
    let variablesTmp =
        _.reduce(variants,
            /**@param {ShapeVariable} variable */
            (prev, variable) => {
                if (variable.redefines) {
                    prev.push(_.assign({}, variable, variable.redefines[variable.redefines.length - 1]));
                } else {
                    prev.push(variable)
                }
                return prev;
            },
            []
        );
    /** @type [ShapeVariable] */
    let narr = [];
    let len = variablesTmp.length;
    for (let index = 0; index < len; index++) {
        for (let j = 0; j < variablesTmp.length; j++) {
            let variable = variablesTmp[j];
            if (_.filter(variable.args, (arg) => { return !~arr.indexOf(arg) }).length === 0) {
                variablesTmp.splice(j, 1);
                j--;
                narr.push(variable);
                arr.push(variable.name);
            }
        }
    }
    if (variablesTmp.length > 0) {
        for (let index = 0; index < len; index++) {
            for (let j = 0; j < variablesTmp.length; j++) {
                let variable = variablesTmp[j];
                if (_.filter(variable.args, (arg) => { return !(~arr.indexOf(arg) || self[arg]) }).length === 0) {
                    variablesTmp.splice(j, 1);
                    j--;
                    narr.push(variable);
                    arr.push(variable.name);
                }
            }
        }
    }
    if (variablesTmp.length > 0) {
        console.log(variablesTmp);
    }
    narr.push(...variablesTmp);
    let promiseList = [];
    _.each(narr, (variable) => {
        if (variable.type === VariantType.variant) {
            promiseList.push(variable.varia.define(variable.name, variable.args, variable.func));
        }
    })
    await Promise.all(promiseList);
}

export const getAbsoluteUrl = function (fileKey, referFileKey) {
    if (/^(\w+:)|\/\//i.test(fileKey)) {
        return fileKey;
    }
    if (/^[.]{0,2}\//i.test(fileKey)) {
        return fileKey;
    }
    if (!fileKey.length || /^[\s.]/.test(fileKey) || /\s$/.test(fileKey)) {
        throw new Error("illegal name");
    }
    let uri = null;
    if (/^\%/.test(referFileKey)) {
        let match = referFileKey.match(/^\%((projects\/){0,1}(\w+)\/)(.*)/)
        let tag;
        if (null !== match && match.length === 5) {
            tag = match[1];
        } else {
            throw new Error('regex was not ok');
        }
        uri = `${ROOT_URI}${tag}`;
    } else {
        uri = referFileKey;
    }
    if (/^\$/.test(fileKey)) {
        return fileKey.replace(/^\$/, "/static/commonLibs/");
    } else if (/^#/.test(fileKey)) {
        return fileKey.replace(/^#/, uri);
    } else if (/^\%/.test(fileKey)) {
        let match = fileKey.match(/^\%((projects\/){0,1}(\w+)\/)(.*)/)
        let tag;
        if (null !== match && match.length === 5) {
            tag = match[1];
            fileKey = match[4];
        } else {
            throw new Error('regex was not ok');
        }
        return `${ROOT_URI}${tag}${fileKey}`;
    }
    if (~fileKey.indexOf(fileSeparator)) {
        return `${uri}${fileKey}`;
    }
}

const getFileAttachmentUrl = async function (editor, fileKeyTmp, referFileKey, moduleName) {
    let absoluteUrl = getAbsoluteUrl(fileKeyTmp, referFileKey);
    if (absoluteUrl) {
        return absoluteUrl;
    }
    const { uri, projectId, userId } = getReferRealFileUri(fileKeyTmp, referFileKey);
    if (uri === editor.getUploadUri()) {
        let fileKey = moduleName.substring(uri.length);
        let fileKey2 = getFileKey(getFileKey(fileKey, FILESDIR), fileKeyTmp);
        if (editor.react_component.state.fileNamesJson[fileKey2]) {
            return uri + fileKey2;
        }
    }
    //TODO other referFileKey
    if (!attachsMapByUrl[moduleName]) {
        let res = await axios.post(`/api/grove/linkTo`, { fileKey: moduleName.substring(uri.length), projectId, userId });
        if (!res.data.status) {
            if (res.data.content instanceof Object) {
                attachsMapByUrl[moduleName] = res.data.content;
            }
        }
    }
    let fileKey2 = getFileKey(getFileKey(moduleName.substring(uri.length), FILESDIR), fileKeyTmp);
    if (attachsMapByUrl[moduleName] && attachsMapByUrl[moduleName][fileKey2]) {
        return uri + fileKey2;
    }
}

export const getCellName = function (text) {
    if (typeof text === 'string' && text.trim() === '') {
        return;
    }
    let cell;
    try {
        cell = text.trim() ? parseCell(text) : undefined;
    } catch (err) {
        console.error(err);
        return;
    }
    if (!cell || cell.constructor.name === "Object") {
        return;
    }
    if (cell.body === null) {
        return;
    }
    if (cell.id !== null) {
        let name;
        if (cell.id.type === "ViewExpression") {
            name = cell.id.id.name;
        } else {
            name = cell.id.name;
        }
        return name;
    }
    return;
}

/**
 * add varialble to module
 * 
 * @param {ShapeEditor} editor
 * @param {ShapeModule} module
 * @param {*} block - codeTool block data
 * @param {*} blocks codeTool blocks data
 * @param {{}} cells with some block need replace
 * @param {boolean} now
 * @param {string} referFileKey
 * @param {string} moduleName
 */
const addVariable = async (editor, module, block, blocks, cells, now = false, referFileKey, moduleName) => {
    const { variables } = module;
    let { value: text, codeMode, db, sqlType, tableName, dbTable, selectedTable, filters, selectedColumns, sorts, slice } = block.data.codeData;
    if (typeof text === 'string' && text.trim() === '') {
        return;
    }
    text = getCodeModeValue(text, codeMode, db, sqlType, tableName, dbTable, selectedTable, filters, selectedColumns, sorts, slice);
    let cell = parseCell(text);
    if (cell.constructor.name === "Object") {
        return;
    }
    if (cell.body === null) {
        return;
    }
    if (cell.id !== null) {
        let name;
        if (cell.id.type === "ViewExpression") {
            name = cell.id.id.name;
        } else {
            name = cell.id.name;
        }
        if (cells !== undefined && cells[name] != undefined) {
            text = cells[name];
            cell = parseCell(text);
        }
    }
    if (cell.fileAttachments && cell.fileAttachments.size) {
        let str = "";
        let startIndex = 0;
        /** @type Map<string,Array<string>> */
        let map = cell.fileAttachments;
        for (const [key, files] of map.entries()) {
            let nkey = await getFileAttachmentUrl(editor, key, referFileKey, moduleName);
            _.each(files, (file, index, files) => {
                str = `${str}${text.substring(startIndex, file.start)}"${nkey}"`
                startIndex = file.end;
            })
        }
        str = `${str}${text.substring(startIndex)}`;
        text = str;
        cell = parseCell(text);
    }
    let cellBody = text.substring(cell.body.start, cell.body.end);
    let cellBodyTmp = '';
    let prevStartIndex = cell.body.start;
    let argArr = [];
    let args = _.reduce(cell.references, (args, curr, key) => {
        if (~["ViewExpression"].indexOf(curr.type)) {
            let vName = `viewof ${curr.id.name}`;
            let arg;
            if (!~args.indexOf(vName)) {
                args.push(vName);
                arg = `$${argArr.length}`;
                argArr.push(arg);
            } else {
                arg = argArr[args.indexOf(vName)]
            }
            cellBodyTmp = cellBodyTmp + text.substring(prevStartIndex, curr.start)
                + `${arg}`;
        } else if (~["MutableExpression"].indexOf(curr.type)) {
            let vName = `mutable ${curr.id.name}`;
            let arg;
            if (!~args.indexOf(vName)) {
                args.push(vName);
                arg = `$${argArr.length}`;
                argArr.push(arg);
            } else {
                arg = argArr[args.indexOf(vName)]
            }
            cellBodyTmp = cellBodyTmp + text.substring(prevStartIndex, curr.start)
                + `${arg}.value`;
        } else {
            let vName = curr.name;
            if (!~args.indexOf(vName)) {
                args.push(vName);
                argArr.push(vName);
            }
            cellBodyTmp = cellBodyTmp + text.substring(prevStartIndex, curr.start)
                + text.substring(curr.start, curr.end);
        }
        prevStartIndex = curr.end;
        return args;
    }, []);
    cellBodyTmp = cellBodyTmp + text.substring(prevStartIndex, cell.body.end);
    if (cellBody !== cellBodyTmp) {
        cellBody = cellBodyTmp;
    }
    let Constructor;
    if (cell.async && cell.generator) {
        Constructor = AsyncGeneratorFunction;
    } else if (cell.async) {
        Constructor = AsyncFunction;
    } else if (cell.generator) {
        Constructor = GeneratorFunction;
    } else {
        Constructor = Function;
    }
    if (cell.id === null) {
        if (cell.body.type === "ImportDeclaration") {
            if (cell.body.specifiers !== undefined) {
                // let cells = getCells(cell, blocks);
                let specifiers = getSpecifiers(cell);
                let tmp_names = [];
                let tmp_localNames = [];
                for (let j = 0; j < cell.body.specifiers.length; j++) {
                    let curr = cell.body.specifiers[j];
                    if (curr !== undefined && curr.type === "ImportSpecifier") {
                        if (curr.view) {
                            tmp_names.push(`viewof ${curr.imported.name}`);
                            tmp_localNames.push(`viewof ${curr.local.name}`);
                            tmp_names.push(curr.imported.name);
                            tmp_localNames.push(curr.local.name);
                        } else {
                            tmp_names.push(curr.imported.name);
                            tmp_localNames.push(curr.local.name);
                        }
                    }
                }
                let fromModuleName = cell.body.source.value;
                await importModule(editor, fromModuleName, module, tmp_names, tmp_localNames, () => {
                    DEBUG_HQ && console.log(`import from ${fromModuleName} success!!!`);
                }, (err) => {
                    showToast(err.message);
                    DEBUG_HQ && console.log(`import from ${fromModuleName} fail!!!`);
                }, undefined, specifiers, referFileKey);
            }
        }
    } else {
        let func;
        let name;
        if (cell.body.type === "BlockStatement") {
            func = new Constructor(argArr, `'use strict';${cellBody}`);
        } else {
            func = new Constructor(argArr, `'use strict';return ${cellBody}`);
        }
        if (~["ViewExpression", "MutableExpression"].indexOf(cell.id.type)) {
            name = cell.id.id.name;
        } else {
            name = cell.id.name;
        }
        if (cell.id.type === "ViewExpression") {
            module.variableEx(`viewof ${name}`, args, func, now);
            args = ["Generators", `viewof ${name}`];
            func = (G, _) => G.input(_);
        } else if (cell.id.type === "MutableExpression") {
            module.variableEx(`initial ${name}`, args, func, now);
            args = ["Mutable", `initial ${name}`];
            func = (M, _) => new M(_);
            module.variableEx(`mutable ${name}`, args, func, now);
            args = [`mutable ${name}`];
            func = _ => _.generator;
        }
        module.variableEx(name, args, func, now);
    }
};
/**
 * add varialble to module
 * 
 * @param {ShapeEditor} editor
 * @param {ShapeModule} module
 * @param {*} block - codeTool block data
 * @param {*} blocks codeTool blocks data
 * @param {{}} cells with some block need replace
 * @param {boolean} now
 * @param {string} referFileKey
 * @param {string} moduleName
 */
const addImportVariable = async (editor, module, block, blocks, cells, now = false, referFileKey, moduleName) => {
    let { value: text, codeMode, db, sqlType, tableName, dbTable, selectedTable, filters, selectedColumns, sorts, slice } = block.data.codeData;
    if (typeof text === 'string' && text.trim() === '') {
        return;
    }
    text = getCodeModeValue(text, codeMode, db, sqlType, tableName, dbTable, selectedTable, filters, selectedColumns, sorts, slice);
    let cell = parseCell(text);
    if (cell.constructor.name === "Object") {
        return;
    }
    if (cell.body === null) {
        return;
    }
    if (cell.id === null) {
        if (cell.body.type === "ImportDeclaration") {
            if (cell.body.specifiers !== undefined) {
                // let cells = getCells(cell, blocks);
                let specifiers = getSpecifiers(cell);
                let tmp_names = [];
                let tmp_localNames = [];
                for (let j = 0; j < cell.body.specifiers.length; j++) {
                    let curr = cell.body.specifiers[j];
                    if (curr !== undefined && curr.type === "ImportSpecifier") {
                        if (curr.view) {
                            tmp_names.push(`viewof ${curr.imported.name}`);
                            tmp_localNames.push(`viewof ${curr.local.name}`);
                            tmp_names.push(curr.imported.name);
                            tmp_localNames.push(curr.local.name);
                        } else {
                            tmp_names.push(curr.imported.name);
                            tmp_localNames.push(curr.local.name);
                        }
                    }
                }
                let fromModuleName = cell.body.source.value;
                await importModule(editor, fromModuleName, module, tmp_names, tmp_localNames, () => {
                    DEBUG_HQ && console.log(`import from ${fromModuleName} success!!!`);
                }, (err) => {
                    showToast(err.message);
                    DEBUG_HQ && console.log(`import from ${fromModuleName} fail!!!`);
                }, undefined, specifiers, referFileKey);
            }
        }
    }
};
/**
 * 
 * @param {*} cell 
 * @returns {Array<{name:string,alias:string}>}
 */
export const getSpecifiers = (cell) => {
    if (cell.body.injections && cell.body.injections.length > 0) {
        let specifiers = _.reduce(cell.body.injections, (prev, curr, index) => {
            prev.push({ name: curr.imported.name, alias: curr.local.name });
            return prev;
        }, []);
        return specifiers;
    }
    return undefined;
}

// const getCells = (cell, blocks) => {
//     let cells = undefined;
//     if (cell.body.injections && cell.body.injections.length > 0) {
//         let names = _.reduce(cell.body.injections, (prev, curr, index) => {
//             prev[curr.imported.name] = curr.local.name;//imported as local
//             return prev;
//         }, {});
//         cells = {};
//         _.each(blocks, (block) => {
//             let { value: text, codeMode, db, sqlType, tableName, dbTable, selectedTable, filters, selectedColumns, sorts, slice } = block.data.codeData;
//             if (typeof text === 'string' && text.trim() === '') {
//                 return;
//             }
//             text = getCodeModeValue(text, codeMode, db, sqlType, tableName, dbTable, selectedTable, filters, selectedColumns, sorts, slice);
//             let cell = parseCell(text);
//             if (cell.id === null) {
//             } else {
//                 let name;
//                 if (cell.id.type === "ViewExpression") {
//                     name = cell.id.id.name;
//                 } else {
//                     name = cell.id.name;
//                 }
//                 if (~_.keys(names).indexOf(name)) {
//                     cells[names[name]] = text.replace(name, names[name]);
//                 }
//             }
//         });
//     }
//     return _.isEmpty(cells) ? undefined : cells;
// }
/**
 * 
 * @param {DefaultShareData} item 
 * @param {Set} copyFiles 
 * @param {string} textTmp
 */
export const copyShareFile = async (item, copyFiles, textTmp) => {
    try {
        let text = textTmp || await getRealFileContent(item);
        if (text === null) {
            return null;
        }
        let filterFileKey = (key) => {
            if (/^(\w+:)|\/\//i.test(key)) {
                return false;
            }
            if (/^[.]{0,2}\//i.test(key)) {
                return false;
            }
            if (!key.length || /^[\s.]/.test(key) || /\s$/.test(key)) {
                return false;
            }
            if (/^\$/.test(key)) {
                return false;
            } else if (/^#/.test(key)) {
                return true;
            } else if (/^\%/.test(key)) {
                return false;
            }
            if (~key.indexOf(fileSeparator)) {
                return true;
            }
            let fileKey = getFileKey(getFileKey(item.fileKey, FILESDIR), key);
            if (item.attachments && item.attachments[fileKey]) {
                copyFiles.add(fileKey);
            }
            return false;
        }
        let json = JSON.parse(text);
        let blocks = _.filter(json.blocks, (block) => {
            return block.type === "codeTool" && block.data.codeData.value
        });
        for (let index = 0; index < blocks.length; index++) {
            let block = blocks[index];
            let { value, codeMode, db, sqlType, tableName, dbTable, selectedTable, filters, selectedColumns, sorts, slice } = block.data.codeData;
            if (typeof value === 'string' && value.trim() === '') {
                continue;
            }
            value = getCodeModeValue(value, codeMode, db, sqlType, tableName, dbTable, selectedTable, filters, selectedColumns, sorts, slice);
            let cell = parseCell(value);
            if (cell.body !== null && cell.id === null) {
                if (cell.body.type === "ImportDeclaration") {
                    if (cell.body.specifiers !== undefined) {
                        let source = cell.body.source;
                        let fromModuleName = source.value;
                        if (!/^\%/.test(String(fromModuleName))) {
                            block.data.codeData.value = `${value.substring(0, source.start)
                                }'%${item.projectId !== COMMON ?
                                    `projects/${item.projectId}` :
                                    `${item.userId}`
                                }/${value.substring(source.start + 1, source.end - 1)
                                }'${value.substring(source.end)}`;
                        }
                    }
                }
            }
            if (cell.fileAttachments && cell.fileAttachments.size) {
                let fileKeys = [];
                /** @type Map */
                let map = cell.fileAttachments;
                map.forEach((/** @type Array */value, key, map) => {
                    if (filterFileKey(key)) {
                        fileKeys.push(key);
                    }
                })
                if (fileKeys.length) {
                    let str = "";
                    let startIndex = 0;
                    _.each(fileKeys, (fileKey) => {
                        let files = map.get(fileKey);
                        _.each(files, (file, index, files) => {
                            str = `${str}${value.substring(startIndex, file.start)}"%${item.projectId !== COMMON ?
                                `projects/${item.projectId}` :
                                `${item.userId}`
                                }/${fileKey.replace(/^#/, "")}"`
                            if (index < files.length - 1) {
                                startIndex = file.end;
                            } else {
                                str = `${str}${value.substring(file.end)}`;
                            }
                        })
                    })
                    block.data.codeData.value = str;
                }
            }

        }
        let imgBlocks = _.filter(json.blocks, (block) => {
            return block.type === "simpleImage";
        });
        for (let index = 0; index < imgBlocks.length; index++) {
            let key = imgBlocks[index].data.url;
            if (filterFileKey(key)) {
                let url = `%${item.projectId !== COMMON ?
                    `projects/${item.projectId}` :
                    `${item.userId}`
                    }/${fileKey.replace(/^#/, "")}`;
                imgBlocks[index].data.url = url;
            }
        }
        return JSON.stringify(json);
    } catch (e) {
        console.error(e.message);
    }
}

/**
 * 
 * @param {DefaultShareData} item 
 */
const getRealFileContent = async function (item) {
    let response = await Common.fetch(item.uploadUri + item.fileKey);
    if (!response.ok) {
        throw new Error('Network response was not ok');
    }
    return await response.text();
}


/**
 * 
 * @param {ShapeEditor} editor
 * @param {ShapeModule} module
 * @param {String} text text to run
 * @param {HTMLElement} declare element that for variant declare
 * @param {ShapeElement} data element that for content display
 * @param {Function} successFunc the function after run success
 * @param {Function} failFunc the function after run fail
 * @param {boolean} now run it right now
 * @param {string} dname default name of block element 
 * @returns the result data
 */
export const runFunc = async (editor, module, text, declare, data, successFunc, failFunc, now, dname) => {
    try {
        if (typeof text === 'string' && text.trim() === '') {
            editor.deleteCacheV(data);
            module.inspect(data, [], `undefined`, undefined, dname, undefined, undefined, now);
            successFunc();
            return `undefined`;
        }
        let cell = parseCell(text);
        if (cell.constructor.name === "Object") {
            editor.deleteCacheV(data);
            declare.innerHTML = ``;
            data.innerHTML = "&nbsp;";
            let args = [cell.variant];
            let syncFileKey = cell.path
            let func = async function (variant) {
                'use strict';
                return window.saveArrayToFile(variant, syncFileKey);
            }
            module.inspect(data, args, func, undefined, dname, successFunc, failFunc, now);
            successFunc();
            return;
        }
        if (cell.fileAttachments && cell.fileAttachments.size) {
            let str = "";
            let startIndex = 0;
            /** @type Map */
            let map = cell.fileAttachments;
            map.forEach((/** @type Array */files, key, map) => {
                let nkey = editor.getFileAttachmentUrl(key, editor.getUploadUri());
                _.each(files, (file, index, files) => {
                    str = `${str}${text.substring(startIndex, file.start)}"${nkey}"`
                    startIndex = file.end;
                })
            })
            str = `${str}${text.substring(startIndex)}`;
            text = str;
            cell = parseCell(text);
        }
        if (cell.body === null) {
            editor.deleteCacheV(data);
            module.inspect(data, [], `undefined`, undefined, dname, undefined, undefined, now);
            successFunc();
            return `undefined`;
        }
        let cellBody = text.substring(cell.body.start, cell.body.end);
        let cellBodyTmp = '';
        let prevStartIndex = cell.body.start;
        let argArr = [];
        let args = _.reduce(cell.references, (args, curr, key) => {
            if (~["ViewExpression"].indexOf(curr.type)) {
                let vName = `viewof ${curr.id.name}`;
                let arg;
                if (!~args.indexOf(vName)) {
                    args.push(vName);
                    arg = `$${argArr.length}`;
                    argArr.push(arg);
                } else {
                    arg = argArr[args.indexOf(vName)];
                }
                cellBodyTmp = cellBodyTmp + text.substring(prevStartIndex, curr.start)
                    + `${arg}`;
            } else if (~["MutableExpression"].indexOf(curr.type)) {
                let vName = `mutable ${curr.id.name}`;
                let arg;
                if (!~args.indexOf(vName)) {
                    args.push(vName);
                    arg = `$${argArr.length}`;
                    argArr.push(arg);
                } else {
                    arg = argArr[args.indexOf(vName)];
                }
                cellBodyTmp = cellBodyTmp + text.substring(prevStartIndex, curr.start)
                    + `${arg}.value`;
            } else {
                let vName = curr.name;
                if (!~args.indexOf(vName)) {
                    args.push(vName);
                    argArr.push(vName);
                }
                cellBodyTmp = cellBodyTmp + text.substring(prevStartIndex, curr.start)
                    + text.substring(curr.start, curr.end);
            }
            prevStartIndex = curr.end;
            return args;
        }, []);
        cellBodyTmp = cellBodyTmp + text.substring(prevStartIndex, cell.body.end);
        if (cellBody !== cellBodyTmp) {
            cellBody = cellBodyTmp;
        }
        let Constructor;
        if (cell.async && cell.generator) {
            Constructor = AsyncGeneratorFunction;
        } else if (cell.async) {
            Constructor = AsyncFunction;
        } else if (cell.generator) {
            Constructor = GeneratorFunction;
        } else {
            Constructor = Function;
        }
        if (cell.id === null) {
            if (cell.body === null) {
                editor.deleteCacheV(data);
                module.inspect(data, [], `null`, undefined, dname, undefined, undefined, now);
                successFunc();
                return `null`;
            }
            let func;
            if (cell.body.type === "BinaryExpression") {
                func = new Constructor(argArr, `'use strict';return ${cellBody}`);
            } else if (~["BlockStatement"].indexOf(cell.body.type)) {
                func = new Constructor(argArr, `'use strict';${cellBody}`);
            } else if (~["AwaitExpression"].indexOf(cell.body.type)) {
                func = new AsyncFunction(argArr, `return ${cellBody}`);
            } else if (~["YieldExpression"].indexOf(cell.body.type)) {
                func = new GeneratorFunction(argArr, `return ${cellBody}`);
            } else if (~["CallExpression", "Identifier", "TaggedTemplateExpression",
                "ObjectExpression", "ArrayExpression", "MemberExpression",
                "Literal", "TemplateLiteral", "NewExpression", "SequenceExpression"].indexOf(cell.body.type)) {
                func = new Constructor(argArr, `'use strict';return ${cellBody}`)
            } else if (cell.body.type === "ViewExpression") {
                func = new Constructor(argArr, `'use strict';return ${cellBody}`);
            } else if (cell.body.type === "ImportDeclaration") {
                if (cell.body.specifiers.length > 0 &&
                    cell.body.specifiers[0].type === "ImportSpecifier") {
                    // let blocks = await editor.getBlocks();
                    // let cells = getCells(cell, blocks);
                    let specifiers = getSpecifiers(cell);
                    let names = [];
                    let localNames = [];
                    for (let j = 0; j < cell.body.specifiers.length; j++) {
                        let curr = cell.body.specifiers[j];
                        if (curr.view) {
                            names.push(`viewof ${curr.imported.name}`);
                            localNames.push(`viewof ${curr.local.name}`);
                            names.push(curr.imported.name);
                            localNames.push(curr.local.name);
                        } else {
                            names.push(curr.imported.name);
                            localNames.push(curr.local.name);
                        }
                        let alreadyDeclare = !(data.cacheNames && ~data.cacheNames.indexOf(curr.local.name)) && !canOverWrite && module._cachedata.variables[curr.local.name];
                        if (alreadyDeclare) {
                            let err = new Error(`can not repeat declare!`);
                            err.name = `${curr.local.name}`;
                            err.stack = undefined;//let error stack not display
                            // console.error(err);
                            module.inspect(data, [], err, undefined, dname, undefined, undefined, now);
                            failFunc(err);
                            return;
                        }
                        editor.deleteCacheName(curr.local.name);
                    }
                    editor.deleteCacheV(data);
                    let fromModuleName = cell.body.source.value;
                    await importModule(editor, fromModuleName, module, names, localNames, () => {
                        data.cacheNames = localNames;
                        let classNames = _.map(data.cacheNames, (cacheName) => {
                            let className = `editorjs-anchor-${cacheName.replace(/\s/g, "_")}`;
                            return className;
                        })
                        data.classList.add(...classNames);
                        declare.innerHTML = `${text.substring(cell.body.start, cell.body.source.start)}&nbsp;`;
                        return module.inspect(data, [], () => {
                            return createLinkElement(fromModuleName);
                        }, undefined, dname, successFunc, failFunc, now);
                    }, (err) => {
                        module.inspect(data, [], () => {
                            return err;
                        }, undefined, dname, () => {
                            failFunc(err);
                        }, failFunc, now);
                    }, undefined, specifiers, editor.getUploadUri());
                    return;
                }
            } else {
                func = new Constructor(argArr, `'use strict';${cellBody}`);
            }
            editor.deleteCacheV(data);
            if (func !== undefined) {
                declare.innerHTML = ``;
                data.innerHTML = "&nbsp;";
                module.inspect(data, args, func, undefined, dname, successFunc, failFunc, now);
            }
        } else {
            //reference {hqUtils#addVariable}
            let func;
            let name;
            if (cell.body.type === "BlockStatement") {
                func = new Constructor(argArr, `'use strict';${cellBody}`);
            } else {
                func = new Constructor(argArr, `'use strict';return ${cellBody}`);
            }
            if (~["ViewExpression", "MutableExpression"].indexOf(cell.id.type)) {
                name = cell.id.id.name;
            } else {
                name = cell.id.name;
            }
            let alreadyDeclare = !(data.cacheNames && ~data.cacheNames.indexOf(name)) && !canOverWrite && module._cachedata.variables[name];
            if (alreadyDeclare) {
                let err = new Error(`can not repeat declare!`);
                err.name = `${name}`;
                err.stack = undefined;//let error stack not display
                // console.error(err);
                module.inspect(data, [], err, undefined, dname, undefined, undefined, now);
                failFunc(err);
                return;
            } else if ((data.cacheNames && ~data.cacheNames.indexOf(name)) && module._cachedata.variables[name]) {
                func = func.bind(module._cachedata.variables[name].varia._value);
            } else {
                // func = func.bind(undefined);
            }
            editor.deleteCacheV(data);
            editor.deleteCacheName(name);
            data.args = args;
            if (cell.id.type === "ViewExpression") {
                module.variableEx(`viewof ${name}`, args, func, now);
                args = ["Generators", `viewof ${name}`];
                func = (G, _) => G.input(_);
            } else if (cell.id.type === "MutableExpression") {
                module.variableEx(`initial ${name}`, args, func, now);
                args = ["Mutable", `initial ${name}`];
                func = (M, _) => new M(_);
                module.variableEx(`mutable ${name}`, args, func, now);
                args = [`mutable ${name}`];
                func = _ => _.generator;
            }
            module.variableEx(name, args, func, now);
            //reference {hqUtils#addVariable}
            data.cacheNames = [name];
            let classNames = _.map(data.cacheNames, (cacheName) => {
                let className = `editorjs-anchor-${cacheName.replace(/\s/g, "_")}`;
                return className;
            })
            data.classList.add(...classNames);
            let resultFunc;
            if (cell.id.type === "ViewExpression") {
                args = [`viewof ${name}`];
                resultFunc = x => x;
            } else {
                args = [name];
                resultFunc = x => x;
            }
            data.innerHTML = "";
            if (!~["TaggedTemplateExpression"].indexOf(cell.body.type) &&
                cell.id.type !== "ViewExpression"
            ) {
                declare.innerHTML = `&nbsp;${name}&nbsp;=&nbsp;`;
                module.inspect(data, args, resultFunc, name, dname, successFunc, failFunc, now);
            } else {
                declare.innerHTML = ``;
                module.inspect(data, args, resultFunc, undefined, dname, successFunc, failFunc, now);
            }
        }
    } catch (err) {
        editor.deleteCacheV(data);
        // console.error(err);
        err.stack = undefined;//let error stack not display
        module.inspect(data, [], err, undefined, dname, undefined, undefined, now);
        failFunc(err);
    }
}