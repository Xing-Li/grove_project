import { AutoComplete, Button, Modal, Select, Space } from 'antd';
import axios from 'axios';
import _ from 'lodash';
import PropTypes from "prop-types";
import React from 'react';
import Spinner from '../components/Spinner.jsx';
import actions from '../define/Actions.js';
import { getFileIcon } from '../util/helpers.js';
const { FileObj, DirObj } = require("./fileUtils");
const { showToast, copyContent } = require("../util/utils");
const { ShapeShared, USER_ID, PROJECT_USER_ID, USER_EMAIL, getUploadUri } = require("../util/localstorage")
const { SHAREDS_TYPE, cm_kickout_room, ShareProjectCanOptions, CanOptions } = require("../util/MsgType");

const { Option } = Select;
export default class Share extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        value: PropTypes.oneOfType([PropTypes.instanceOf(DirObj), PropTypes.instanceOf(FileObj)]),
        userId: PropTypes.string,
        projectId: PropTypes.string,
        shareProjectId: PropTypes.string,
        projectName: PropTypes.string,
        refreshShareds: PropTypes.func,
        disabled: PropTypes.bool,
        visible: PropTypes.bool,
    };
    static defaultProps = {};

    constructor(props) {
        super(props);
        const { value, userId, projectId, projectName, shareProjectId, visible } = this.props;
        let fileKey;
        if (value instanceof FileObj) {
            fileKey = value.file.fileKey;
        } else if (value instanceof DirObj) {
            fileKey = value.dir;
            showToast("Error file!");
        }
        this.state = {
            visible: visible,
            options: [],
            email: "",
            users: [],
            fileKey: fileKey,
            projectName: projectName,
            projectId: projectId,
            shareProjectId: shareProjectId,
            userId: userId,
            status: ShareProjectCanOptions["Edit"],
            /**@type{Object.<string,ShapeShared>}} */
            shareds: {},
        }
    }

    componentDidMount() {
    }

    componentDidUpdate(prevProps, prevState) {
        let state = {};
        if (prevProps.visible !== this.props.visible) {
            _.assign(state, { visible: false });
        }
        !_.isEmpty(state) && this.setState(state);
    }

    refreshSharedsFunc = (state) => {
        const { refreshShareds } = this.props;
        state.shareds && refreshShareds(state.shareds);
        this.setState(state);
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    handleOk = e => {
        // console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleCancel = e => {
        // console.log(e);
        this.setState({
            visible: false,
        });
    };

    getUserByKeyword = async (message, exitedEmail, stateTmp) => {
        let state = stateTmp || {};
        let res = await axios.post(`/api/user/getUserByKeyword`, { message: message, exitedEmail: exitedEmail });
        if (!res.data.status) {
            const { shareds } = this.state;
            // console.log(res.data.content)
            let arr = _.reduce(res.data.content, (arr, value, index) => {
                if (_.filter(shareds, (shared) => {
                    return shared.toUserId._id === value._id ||
                        value._id === USER_ID
                }).length === 0) {
                    arr.push({ value: value.email })
                }
                return arr;
            }, [])
            _.assign(state, { options: arr, users: res.data.content })
            !stateTmp && this.setState(state);
        }
    }
    getAllShareds = async (stateTmp) => {
        let state = stateTmp || {};
        const { fileKey, projectId, userId } = this.state;
        let condition = {
            userId: userId,
            fileKey,
            projectId,
            type: SHAREDS_TYPE.User_Shared_By_FileKey
        }
        let res = await axios.post(`/api/grove/getAllShareds`, condition);
        if (!res.data.status) {
            _.assign(state, {
                shareds: _.reduce(res.data.content, (prev, curr, index) => {
                    prev[curr._id] = curr;
                    return prev;
                }, {})
            })
            !stateTmp && this.refreshSharedsFunc(state)
        }
    }

    /** @param {ShapeShared} item */
    remove = async (item) => {
        const _id = item._id;
        const { shareds, shareProjectId, projectId, userId, fileKey } = this.state;
        if (item.toUserId._id === USER_ID) {
            return;
        }
        let res = await axios.post(`/api/grove/removeShared`, {
            _id,
            shareProjectId,
            projectId,
            userId
        });
        if (!res.data.status) {
            let shared = shareds[_id];
            delete shareds[_id];
            this.refreshSharedsFunc({ shareds });
            actions.variable(actions.types.TRIGGER, [], () => {
                return {
                    roomId: `${getUploadUri(projectId, userId)}${fileKey}`,
                    msgId: cm_kickout_room,
                    params: { outUserId: shared.toUserId._id }
                };
            })
        }
        res.data.message && showToast(res.data.message, res.data.status ? "error" : "success");
    }

    share = async () => {
        const { email, users, fileKey, projectId, shareProjectId, userId, projectName, status, shareds } = this.state;
        let user = _.find(users, (user, index, users) => {
            return user.email === email;
        })
        if (!user) {
            return showToast("can not find user!", "warning");
        }
        if (_.filter(shareds, (shared) => {
            return shared.toUserId.email === email
        }).length !== 0) {
            return showToast("can not share again!", "warning");
        }
        let res = await axios.post(`/api/grove/addShared`, {
            toUser: user,
            projectId,
            shareProjectId,
            userId: userId,
            projectName,
            projectUser: userId,
            fileKey,
            status
        });
        if (!res.data.status) {
            shareds[res.data.content._id] = res.data.content;
            this.refreshSharedsFunc({ shareds })
        }
        res.data.message && showToast(res.data.message, res.data.status ? "error" : "success");
    }

    handleChange = (value) => {
        this.setState({ status: value })
    }

    /**
     * 
     * @param {ShapeShared} item
     * @param {*} value 
     */
    handleItemChange = async (item, value) => {
        const { shareds } = this.state;
        let res = await axios.post(`/api/grove/setSharedStatus`, {
            _id: item._id,
            status: value
        });
        if (!res.data.status) {
            item.status = res.data.content.status;
            this.setState({ shareds })
        }
        res.data.message && showToast(res.data.message, res.data.status ? "error" : "success");
    }

    render() {
        const { options, email, shareds, status, visible, projectId, projectName, userId, fileKey } = this.state;
        const { value, className, title, disabled } = this.props;
        const editUrl = `${window.opener.location.origin}${window.opener.location.pathname}?groveFileKey=${fileKey}`
        const shareUrl = `${window.opener.location.origin}${window.opener.location.pathname.replace('/p/', '/share/')}?groveUserId=${userId}&groveProjectId=${projectId}&groveFileKey=${fileKey}&groveFullscreen=true&groveReadonly=true&groveBlockMode=hide`;
        return <React.Fragment>
            <Spinner title={title || "Share to user"} type="i" className={`f7 icon fas fa-share-alt-square ${className || ""}`}
                disabled={disabled}
                onClick={async (e, setLoading) => {
                    if (!visible) {
                        let state = { visible: true };
                        await this.getAllShareds(state);
                        this.refreshSharedsFunc(state)
                    }
                    setLoading && setLoading(false)
                    e.stopPropagation();
                }}></Spinner>
            <Modal zIndex={1002}
                title={<div><h4>{`Share Settings`}</h4>
                    <div className="file-name-display normal-icon blablano"><i className={`icon ${getFileIcon(value.file)}`}></i>
                        <span>{value.toString()}</span>
                    </div></div>}
                visible={this.state.visible}
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                    <Button key="Close" onClick={this.handleCancel}>
                        Close
                    </Button>]}
            ><Space direction="vertical">
                    <Button onClick={() => copyContent(shareUrl)}>Copy Share Link</Button>
                    <Button onClick={() => copyContent(editUrl)}>Copy Edit Link</Button>
                    {
                        _.map(shareds, /** @param {ShapeShared} item */(item, key) => {
                            return <Space key={item._id} wrap>
                                <span>{`${item.toUserId.firstName} ${item.toUserId.lastName}`}</span>
                                <span>{item.toUserId.email}</span>
                                <Select defaultValue={item.status} onChange={(value) => {
                                    this.handleItemChange(item, value)
                                }}>
                                    {
                                        _.map(CanOptions, (value, key) => {
                                            return <Option key={value} value={value}>{key}</Option>
                                        })
                                    }
                                </Select>
                                <Button type="dashed" disabled={item.toUserId._id === USER_ID} danger onClick={() => {
                                    this.remove(item)
                                }}>
                                    Remove
                                </Button>
                            </Space>
                        })
                    }
                    <Space className="add" wrap>
                        <AutoComplete
                            style={{ width: 170 }}
                            placeholder="Share By Email:"
                            defaultValue={email}
                            allowClear
                            onChange={async (value, option) => {
                                let state = { email: value }
                                await this.getUserByKeyword(value, [USER_EMAIL], state)
                                this.setState(state);
                            }}
                            onSelect={(value, option) => {
                                this.setState({ email: value })
                            }}
                            options={options}
                        />
                        <Select value={status} style={{ width: 120 }} onChange={this.handleChange}>
                            {
                                _.map(CanOptions, (value, key) => {
                                    return <Option key={value} value={value}>{key}</Option>
                                })
                            }
                        </Select>
                        <Button type="primary" onClick={(e) => {
                            this.share();
                        }}>Add</Button>
                    </Space>
                </Space>
            </Modal>
        </React.Fragment>
    }
}
