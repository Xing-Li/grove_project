import { require as requireDefault, requireFrom, resolveBase, resolveAnywhere } from "./grove-require";
const requireModule = requireFrom(resolveAnywhere);

import { requireModuleUsingGlobalLeaksPattern } from "./requireModuleUsingGlobalLeaksPattern";
import {
  getApi
} from './graphxrApi';
import groveApi from "./grove/groveApi";
import sightxrApi from "./sightxr/sightxrApi";
import DOM from "@observablehq/stdlib/src/dom/index.js";
import Generators from "@observablehq/stdlib/src/generators/index.js";
import html from "@observablehq/stdlib/src/html.js";
import leaflet from "./leaflet.js";
import mermaid from "./mermaid.js";
import Mutable from "@observablehq/stdlib/src/mutable.js";
import now from "@observablehq/stdlib/src/now.js";
import Promises from "@observablehq/stdlib/src/promises/index.js";
import SQLite, { SQLiteDatabaseClient } from "./sqlite.js";
import DatabaseClient, { datasToDb, workbookToDb, createDatabaseClient } from "./databaseClient.js";
import { chartConstructor } from "./drawChart";
import Secret from "./secret.js";
import Link from "./link.js";
import svg from "@observablehq/stdlib/src/svg.js";
import vegalite from "./vegalite.js";
import Files from "./files.js";
import md from "./md.js";
import py, { pyodide } from "./pyodide.js";
import tex from "./tex.js";
import FileAttachments, { AbstractFile, FileAttachment } from "./fileAttachment.js";
import width from "./width.js";
import graphApi from "./graphApi.js";
import selectMainDatas from "./selectMainDatas.js";
import selectFilesDatas from "./selectFilesDatas.js";
import selectShareDataDatas from "./selectShareDataDatas.js";
import selectDbClients from "./selectDbClients.js";
import { __query } from "./table.js";
import xmljs from 'xml-js'
import jszip from "jszip"
import { DuckDBClient } from "./duckdb.js";
import { arquero, arrow4, d3, graphviz, htl, inputs, lodash, plot, topojson } from "./dependencies.js";

// Imports for React cell
import * as antd from 'antd'
import * as antdIcons from '@ant-design/icons'
import * as ReactDOM from 'react-dom'
import React from "react";
import { getColumnsData } from "block/antchart/antchartUtils";
import { copyContent } from "util/utils";

export const SampleDatasets = {
  // Sample datasets
  // https://observablehq.com/@observablehq/datasets
  aapl: () => new FileAttachment("https://static.observableusercontent.com/files/3ccff97fd2d93da734e76829b2b066eafdaac6a1fafdec0faf6ebc443271cfc109d29e80dd217468fcb2aff1e6bffdc73f356cc48feb657f35378e6abbbb63b9").csv({ typed: true }),
  alphabet: () => new FileAttachment("https://static.observableusercontent.com/files/75d52e6c3130b1cae83cda89305e17b50f33e7420ef205587a135e8562bcfd22e483cf4fa2fb5df6dff66f9c5d19740be1cfaf47406286e2eb6574b49ffc685d").csv({ typed: true }),
  cars: () => new FileAttachment("https://static.observableusercontent.com/files/048ec3dfd528110c0665dfa363dd28bc516ffb7247231f3ab25005036717f5c4c232a5efc7bb74bc03037155cb72b1abe85a33d86eb9f1a336196030443be4f6").csv({ typed: true }),
  citywages: () => new FileAttachment("https://static.observableusercontent.com/files/39837ec5121fcc163131dbc2fe8c1a2e0b3423a5d1e96b5ce371e2ac2e20a290d78b71a4fb08b9fa6a0107776e17fb78af313b8ea70f4cc6648fad68ddf06f7a").csv({ typed: true }),
  diamonds: () => new FileAttachment("https://static.observableusercontent.com/files/87942b1f5d061a21fa4bb8f2162db44e3ef0f7391301f867ab5ba718b225a63091af20675f0bfe7f922db097b217b377135203a7eab34651e21a8d09f4e37252").csv({ typed: true }),
  flare: () => new FileAttachment("https://static.observableusercontent.com/files/a6b0d94a7f5828fd133765a934f4c9746d2010e2f342d335923991f31b14120de96b5cb4f160d509d8dc627f0107d7f5b5070d2516f01e4c862b5b4867533000").csv({ typed: true }),
  industries: () => new FileAttachment("https://static.observableusercontent.com/files/76f13741128340cc88798c0a0b7fa5a2df8370f57554000774ab8ee9ae785ffa2903010cad670d4939af3e9c17e5e18e7e05ed2b38b848ac2fc1a0066aa0005f").csv({ typed: true }),
  miserables: () => new FileAttachment("https://static.observableusercontent.com/files/31d904f6e21d42d4963ece9c8cc4fbd75efcbdc404bf511bc79906f0a1be68b5a01e935f65123670ed04e35ca8cae3c2b943f82bf8db49c5a67c85cbb58db052").json(),
  olympians: () => new FileAttachment("https://static.observableusercontent.com/files/31ca24545a0603dce099d10ee89ee5ae72d29fa55e8fc7c9ffb5ded87ac83060d80f1d9e21f4ae8eb04c1e8940b7287d179fe8060d887fb1f055f430e210007c").csv({ typed: true }),
  penguins: () => new FileAttachment("https://static.observableusercontent.com/files/715db1223e067f00500780077febc6cebbdd90c151d3d78317c802732252052ab0e367039872ab9c77d6ef99e5f55a0724b35ddc898a1c99cb14c31a379af80a").csv({ typed: true }),
  weather: () => new FileAttachment("https://static.observableusercontent.com/files/693a46b22b33db0f042728700e0c73e836fa13d55446df89120682d55339c6db7cc9e574d3d73f24ecc9bc7eb9ac9a1e7e104a1ee52c00aab1e77eb102913c1f").csv({ typed: true }),
}

export default Object.assign(function Library() {
  Object.defineProperties(this, properties(_.assign({
    // e.g. react(antd(({Button}) => <Button>hello</Button>))
    antd: () => (fn) => React.createElement('div', { className: 'antd-cell-wrapper' }, fn({ ...antd, icons: antdIcons })),
    /* React, react required by React cell */
    React: () => React,
    react: () => (reactEl) => {
      const domEl = html`<div></div>`
      ReactDOM.render(reactEl, domEl)
      return domEl;
    },
    aq: () => requireModule.alias({ "apache-arrow": arrow4.resolve() })(arquero.resolve()), // TODO upgrade to apache-arrow@9
    Arrow: () => requireModule(arrow4.resolve()), // TODO upgrade to apache-arrow@9

    ZipArchive: () => ZipArchive,
    FileAttachment: () => FileAttachments(resolveBase),
    Mutable: () => Mutable,
    Plot: () => requireModule(plot.resolve()),
    __query: () => __query,
    SQLite: () => SQLite(requireModule),
    DatabaseClient: () => DatabaseClient,
    SQLiteDatabaseClient: () => SQLiteDatabaseClient,
    topojson: () => requireModule(topojson.resolve()),
    createDatabaseClient: () => createDatabaseClient,
    workbookToDb: () => workbookToDb,
    datasToDb: () => datasToDb,
    Secret: () => Secret,
    Link: () => Link,
    _: () => requireModule(lodash.resolve()),
    d3: () => requireModule(d3.resolve(), "d3-hexbin@0.2.2/build/d3-hexbin.js"),
    DuckDBClient: () => DuckDBClient,
    Inputs: () => requireModule(inputs.resolve()).then(Inputs => ({ ...Inputs, file: Inputs.fileOf(AbstractFile) })),
    L: () => leaflet(requireModule),
    mermaid: () => mermaid(requireModule),
    dot: () => requireModule(graphviz.resolve()),
    htl: () => requireModule(htl.resolve()),
    html: () => html,
    md: () => md(requireModule),
    py: () => py,
    pyodide: () => pyodide(),
    now,
    require: () => requireModule,
    resolve: () => requireDefault.resolve,
    requireModuleUsingGlobalLeaksPattern: () => requireModuleUsingGlobalLeaksPattern,
    svg: () => svg,
    tex: () => tex(requireModule),
    vl: () => vegalite(requireModule),
    xmljs: () => xmljs,
    jszip: () => jszip,
    copyContent: () => copyContent,

    Button: () => async (label, onClick) => groveApi.AsyncButton({ label, onClick }),
    Grove: () => groveApi,
    selectedNodes: () => groveApi.getSelectedNodes(),
    selectedEdges: () => groveApi.getSelectedEdges(),
    graph: () => groveApi.onGraphDataUpdate(),
    SightXR: () => sightxrApi,

    /**
     * getApi loads GraphXR API at a specific version, or the latest if the version is not specified.
     */
    getApi: () => getApi,
    width,
    graphApi,
    chartConstructor: () => chartConstructor,
    getColumnsData: () => getColumnsData,
    selectMainDatas,
    selectFilesDatas,
    selectShareDataDatas,
    selectDbClients,

    // Note: these are namespace objects, and thus exposed directly rather than
    // being wrapped in a function. This allows library.Generators to resolve,
    // rather than needing module.value.
    DOM,
    Files,
    Generators,
    Promises
  }, SampleDatasets)));
}, { resolve: requireDefault.resolve });

function properties(values) {
  return Object.fromEntries(Object.entries(values).map(property));
}

function property([key, value]) {
  return [key, ({ value, writable: true, enumerable: true })];
}

