const LDAP = require("ldapjs");
const Async = require("async");
const Logger = require("./../logger");

const GLogger = Logger.getLogger(__filename);
const LDAPConfig = AppConfig.ldap || null;

class UserLDAP {
  constructor() {
    this._bindErr = null;
    this.client = null;
    this._init();
  }

  _init() {
    if (LDAPConfig && LDAPConfig.server.url && LDAPConfig.server.bindDN) {
      const client = LDAP.createClient({
        url: [LDAPConfig.server.url],
      });

      client.on("error", (err) => {
        this._bindErr = err;
        GLogger.error(err);
      });

      client.bind(LDAPConfig.server.bindDN, LDAPConfig.server.bindCredentials, (err) => {
        if (err) {
          this._bindErr = err;
          return GLogger.error(err);
        }
        this.client = client;
      });
    }
  }

  modifyManager(userDN, cb, userType = 1) {
    if (!this.client) {
      return cb(new Error("Please check the LDAP config in config.js"));
    }

    Async.waterfall(
      [
        //1. check the admin userType
        (callback) => {
          if (LDAPConfig.server.adminDN) {
            this.search(
              LDAPConfig.server.adminDN,
              {
                filter: `(member=${userDN})`,
                scope: "sub",
                sizeLimit: 1,
                attributes: ["dn"],
              },
              (err, res) => {
                GLogger.log("admin:", res);
                callback(err, res && res.length > 0 ? true : false);
              }
            );
          } else {
            callback(null, false);
          }
        },
        //2. try add or remove the userType
        (isExist, callback) => {
          //2.1 handle the add logic
          if ((LDAPConfig.server.adminDN && !isExist && userType === 1) || (LDAPConfig.server.adminDN && isExist && userType === 0)) {
            let isAdd = LDAPConfig.server.adminDN && !isExist && userType === 1 ? true : false;
            this.client.modify(
              LDAPConfig.server.adminDN,
              new LDAP.Change({
                operation: isAdd ? "add" : "delete",
                modification: {
                  member: userDN,
                },
              }),
              (err) => {
                callback(err);
              }
            );
          } else {
            callback(null);
          }
        },
      ],
      (err) => {
        err && GLogger.error("modifyManager:", err);
        cb(err);
      }
    );
  }
  modifyUserObject(dn, userInfo, cb) {
    if (!this.client) {
      return cb(new Error("Please check the LDAP config in config.js"));
    }
    if (Object.keys(userInfo).length === 0) {
      return cb(null);
    }

    Async.waterfall(
      Object.keys(userInfo).map((k) => {
        return (callback) => {
          this.client.modify(
            dn,
            new LDAP.Change({
              operation: "replace",
              modification: { [k]: userInfo[k] },
            }),
            (err) => {
              callback(err);
            }
          );
        };
      }),
      (err) => {
        err && GLogger.error("modifyUserObject: ", err);
        cb(err);
      }
    );
  }

  modifyUser(email, firstName, lastName, password, userType, cb) {
    if (!this.client) {
      return cb(new Error("Please check the LDAP config in config.js"));
    }
    Async.waterfall(
      [
        //0. check the email
        (callback) => {
          callback(!email ? new Error("miss email") : null);
        },
        //1. check the user
        (callback) => {
          this.search(
            LDAPConfig.server.searchBase,
            {
              filter: `(mail=${email})`,
              scope: "sub",
              sizeLimit: 1,
              attributes: ["dn", "mail", "uid", "userid", "sn", "surname", "cn", "commonname"],
            },
            (err, res) => {
              callback(err, res ? res[0] : {});
            }
          );
        },
        //2. found the field names
        (profile, callback) => {
          if (!profile || !profile.dn) {
            return callback(new Error("Can not found the user"));
          }
          let ldapFieldConfig = {
            firstName: profile.cn ? "cn" : profile.commonname ? "commonname" : "cn", //default cn
            lastName: profile.sn ? "sn" : profile.surname ? "surname" : "sn",
            email: "mail",
            password: "userPassword",
            dn: profile.dn,
          };
          let ldapUser = {
            firstName: profile.given_name,
            lastName: profile.family_name,
            email: profile.email,
            username: profile.email,
          };
          return callback(null, ldapFieldConfig, ldapUser);
        },
        //3. modify the data
        (ldapFieldConfig, ldapUser, callback) => {
          let currentUser = { firstName, lastName, password };
          let modifyObj = {};
          let oldUser = {};
          let newUser = {};
          Object.keys(currentUser)
            .filter((k) => currentUser[k] && ldapUser[k] !== currentUser[k])
            .forEach((k) => {
              modifyObj[ldapFieldConfig[k]] = currentUser[k];
              oldUser[k] = ldapUser[k];
              newUser[k] = currentUser[k];
            });
          this.modifyUserObject(ldapFieldConfig.dn, modifyObj, (err) => {
            callback(err, ldapFieldConfig.dn, newUser, oldUser);
          });
        },
        //4. the admin userType modify
        (userDN, newUser, oldUser, callback) => {
          if ([0, 1].includes(userType)) {
            this.modifyManager(
              userDN,
              (err) => {
                oldUser.userType = userType === 1 ? 0 : 1;
                newUser.userType = userType;
                callback(err, newUser, oldUser);
              },
              userType
            );
          } else {
            callback(null, newUser, oldUser);
          }
        },
      ],
      (err, newUser, oldUser) => {
        err && GLogger.error("modifyUser:", err);
        cb(err, newUser, oldUser);
      }
    );
  }

  /**
   * 
   * @param {String} searchBase e.g. cn=foo, o=example
   * @param {Object} options   
   * e.g. const opts = {
      filter: "(&(l=Seattle)(email=*@foo.com))",
      scope: "sub",
      sizeLimit:1,
      attributes: ["dn", "sn", "cn"],
    };
   * @param {function} cb (err, items) => {}
   */
  search(searchBase, options, cb) {
    let alredyCB = false;
    const callback = (err, res) => {
      err && GLogger.error("search:", err);
      !alredyCB && cb(err, res);
      alredyCB = true;
    };

    if (!this.client) {
      return callback(new Error("Please check the LDAP config in config.js"));
    }

    if (this._bindErr) {
      return callback(this._bindErr);
    }

    this.client.search(searchBase, options, function (searchErr, searchResult) {
      if (searchErr) {
        return callback(searchErr);
      }

      let items = [];
      searchResult.on("searchEntry", function (entry) {
        items.push(entry.object);
      });

      searchResult.on("error", callback);

      searchResult.on("end", function (result) {
        if (result.status !== 0) {
          let err = "non-zero status from LDAP search: " + result.status;
          return callback(err);
        }
        //Only return the first one
        return callback(null, items);
      });
    });
  }
}
const GUserLDAP = new UserLDAP();
module.exports = GUserLDAP;
