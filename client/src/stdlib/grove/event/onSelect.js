import * as stdlib from "@observablehq/stdlib";

/**
 * @example
 * ```js
 * selectedNodeIds = onSelect(
 *   'id',
 *   (event) => {
 *     console.log(event);
 *     return event.nodeIds;
 *   }
 * )
 * ```
 */
export default function onSelect(id, callback) {
  const { Generators } = new stdlib.Library();
  return Generators.observe((emit) => {
    const unregister = window.gxr.onSelect(id, (event) => {
      emit(callback(event));
    });

    // Initial state
    emit(callback({
      type: "new",
      nodeIds: window.gxr.nodes(":selected").ids(),
      edgeIds: window.gxr.edges(":selected").ids()
    }));

    return unregister;
  });
}
