import { getChartConstructor, getExOptions } from '../block/antchart/comp/CheckChartTypes';
import { DefaultAntChartConfig } from '../block/antchart/comp/OptionsTypes';
/** @param {DefaultAntChartConfig}  chartConfig*/
export function drawChart(chartConfig) {
    const { type } = chartConfig;
    const container = document.createElement("div");
    let exOptions = getExOptions(chartConfig);
    let constructorFunc = getChartConstructor(type)
    if (constructorFunc) {
        delete exOptions.width;
        delete exOptions.height;
    } else {
        return container;
    }
    const chart = new constructorFunc(container, exOptions);
    chart.render();
    return container;
}
export const chartConstructor = getChartConstructor;