import { Area, Column, Line } from '@antv/g2plot';
import { Collapse, Select, Space, Tabs, Row, Col, Alert, Button, Affix } from 'antd';
import cx from "classnames";
import _ from 'lodash';
import PropTypes from "prop-types";
import React from "react";
import { Sticky, StickyContainer } from 'react-sticky';
import AntChartSetting from "../AntChartSetting";
import { DataComp, MAX_SIZE } from '../antchartUtils';
import { checkChartTypes, getChartConfig } from './CheckChartTypes';
import { getMultiColors } from 'util/chartConfigUtil'
import { ChartTypesDesc } from './chartDescriptions'
import {
    ChartTypes, extractOptions, Fields, chartOptionsDefault, ChartTypeMatchDataTypes, getKeys, DataTypes, extractDatas,
    ChartTypeTemplate, DefaultAntChartConfig
} from './OptionsTypes';
import { autoObjectsType, calcAutoType, extractAllKeys } from 'util/helpers';
import { showToast } from 'util/utils';
import actions from 'define/Actions';
import { ChevronDownIcon } from 'block/code/Icon';
const { Panel } = Collapse;
const { Option } = Select;
const { TabPane } = Tabs;

const renderTabBar = (props, DefaultTabBar) => (
    <Sticky bottomOffset={80}>
        {({ style }) => (
            <DefaultTabBar {...props} className="site-custom-tab-bar" style={{ ...style }} />
        )}
    </Sticky>
);
/**
 * 
 * @param {DefaultAntChartConfig} chartConfig 
 */
function getKey(chartConfig) {
    const { type } = chartConfig;
    let key;
    key = _.reduce(_.keys(Fields), (prev, fieldName, index) => {
        chartConfig.options[fieldName] && prev.push(chartConfig.options[fieldName])
        return prev;
    }, [type]).join("_");
    return key;
}

const KEYS_LENS = {
    'ontinuous Heatmap': 2,
    'Heatmap': 2,
    'Uneven Heatmap': 3,
    'Word Cloud': 2,
}

const DEFAULT_WIDTH = 560;
const DEFAULT_HEIGHT = 376;
export default class ChooseChart extends React.Component {
    static propTypes = {
        settings: PropTypes.object,
        className: PropTypes.string,
    };

    static defaultProps = {};
    constructor(props) {
        super(props);
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        const { chartConfig: { chartData, chartColumns, type, options } } = settings.state;
        const load = chartData.length && type;
        let FieldsTmp = _.cloneDeep(Fields);
        let selectedChartType = null;
        if (load) {
            _.merge(FieldsTmp, _.pick(options, _.keys(Fields)));
            selectedChartType = type;
        }
        this.state = _.merge({
            className: this.props.className,
            activeKey: null,
            hoverChartType: null,
            scrollSize: 2,
            load,
            selectedChartType,
            keys: getKeys(selectedChartType),
            warnings: {}
        }, FieldsTmp)
    }

    componentDidMount() {
        let self = this;
        let last_known_scroll_position = 0;
        this.ticking = false;
        let targetEle = document.querySelector(".chart-setting-drawer .ant-drawer-body");
        this.scrollFunc = function (ev) {
            last_known_scroll_position = targetEle.scrollTop;
            if (targetEle.clientHeight + targetEle.scrollTop + 1 >= targetEle.scrollHeight) {
                if (!self.ticking) {
                    self.ticking = true;
                    window.requestAnimationFrame(async function () {
                        // Do something with the scroll position
                        if (self.chartTypesTmp && self.chartTypesTmp.length < self.state.scrollSize) {
                            console.log("i am bottom!");
                            self.ticking = false;
                            return;
                        }
                        self.setState({ scrollSize: self.state.scrollSize + 2 }, () => {
                            self.ticking = false;
                        })
                    });
                }
            }
        };
        targetEle.addEventListener("scroll", this.scrollFunc);
    }

    componentWillUnmount() {
        let targetEle = document.querySelector(".chart-setting-drawer .ant-drawer-body");
        targetEle.removeEventListener("scroll", this.scrollFunc)
        this.unmount = true;
    }

    componentDidUpdate(preProps, preState) {
        const { className } = this.props;
        let state = {};
        if (preProps.className !== className) {
            _.assign(state, { className });
        }
        !_.isEmpty(state) && this.setState(state);
    }

    onChange = (value) => {
        console.log(`selected ${value}`);
    }

    onBlur = () => {
        console.log('blur');
    }

    onFocus = () => {
        console.log('focus');
    }

    onSearch = (val) => {
        console.log('search:', val);
    }

    SelectChartTypeComp = (props) => {
        const { allDatas } = props;
        const { selectedChartType, angleField, colorField, xField, yField, keys, load } = this.state;
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        const { state: { chartConfig: { chartData, chartColumns } } } = settings;
        if (!selectedChartType) {
            return <div></div>
        }
        let dataTypesConfig = ChartTypeMatchDataTypes[selectedChartType];
        let types = calcAutoType(allDatas, true, true);
        return <Space direction="horizontal" wrap={true} size="middle">
            {
                _.map(keys, (key) => {
                    let filterColumns = KEYS_LENS[selectedChartType] ? chartColumns : _.filter(chartColumns, (chartColumn) => {
                        return chartColumn.key && (chartColumn.key === this.state[key] ||
                            !~_.values(_.pick(this.state, _.keys(Fields))).indexOf(chartColumn.key))
                    });
                    let suits = [];
                    switch (dataTypesConfig[key]) {
                        case DataTypes['period date']:
                        case DataTypes['uniq date']:
                        case DataTypes['date']:
                            suits = _.filter(filterColumns, (chartColumn) => {
                                return types[chartColumn.key] && types[chartColumn.key].type === 'date'
                            })
                            filterColumns.sort((a, b) => { return suits.indexOf(b) - suits.indexOf(a) })
                            break;
                        case DataTypes['period number']:
                        case DataTypes['uniq number']:
                        case DataTypes['number']:
                            suits = _.filter(filterColumns, (chartColumn) => {
                                return types[chartColumn.key] && types[chartColumn.key].type === 'number'
                            })
                            filterColumns.sort((a, b) => { return suits.indexOf(b) - suits.indexOf(a) })
                            break;
                        default:
                            suits = filterColumns;
                            break;
                    }
                    return <div key={key} className="d-flex align-items-center" >
                        <div>{key.camelPeakToBlankSplit().firstUpperCase()}:</div>
                        <Select notFoundContent={"None are suitable"} allowClear={true} value={this.state[key]} style={{ minWidth: 120 }} onChange={(value) => {
                            let state = {}
                            state[key] = value;
                            this.setState(state, () => { });
                        }} >
                            {_.map(filterColumns, (chartColumn) => {
                                return <Option disabled={!!!~suits.indexOf(chartColumn)} key={chartColumn.key} value={chartColumn.key}>{chartColumn.key}</Option>
                            })}
                        </Select>
                        {!!!suits.length && <div className="text-warning">{"None are suitable"}</div>}
                    </div>
                })
            }
        </Space>
    }

    DrawChartComp = (props) => {
        const { allDatas } = props;
        const { keys, selectedChartType } = this.state;
        if (!keys.length || _.filter(keys, (key, index) => { return !this.state[key] }).length) {
            return <div>&nbsp;</div>
        }
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        const { state: { chartConfig: { chartData, chartColumns } } } = settings;
        if (!allDatas || !allDatas.length || allDatas.length < 1) {
            return <div></div>;
        }
        let chartType = {
            type: selectedChartType,
            config: _.reduce(keys, (prev, key, index) => { prev[key] = this.state[key]; return prev; }, {})
        }
        let series = chartType.config.colorField || chartType.config.seriesField;
        if (series) {
            let set = _.reduce(allDatas, (prev, curr, index) => {
                prev.add(curr[series]);
                return prev;
            }, new Set());
            _.assign(chartType.config, {
                color: getMultiColors(set.size)
            })
        }
        const baseChartConfig = getChartConfig(chartType)
        const { chartConfig } = settings.state;
        let key = getKey(baseChartConfig);
        let currentKey = getKey(chartConfig);
        return <DrawChart key={key} className={currentKey === key ? "selected" : ""} onClick={(e) => {
            const { chartEditor, chartConfig } = settings.state;
            delete baseChartConfig.constructorFunc;
            if (chartConfig.type === baseChartConfig.type &&
                _.isEqual(chartConfig.options, baseChartConfig.options)) {
                return showToast("Drawing!", 'warning');
            }
            settings.setState({ chartConfig: _.assign({}, chartConfig, baseChartConfig) }, () => {
                chartEditor && chartEditor.run(undefined, false);
            })
        }} chartConfig={baseChartConfig} chartData={chartData} allDatas={allDatas}></DrawChart>
    }



    SuggestedComp = (props) => {
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        /**@type {[ChartTypeTemplate]} */
        const chartTypes = props.chartTypes;
        /**use extract datas not origin datas */
        const allDatas = props.allDatas;
        const { chartConfig, chartConfig: { chartData, type } } = settings.state;
        const { hoverChartType } = this.state;
        let typesByCategoryTmp = _.reduce(chartTypes, (prev, chartType, index) => {
            let family = (ChartTypesDesc[chartType.type] || {}).Family || "Others";
            if (!prev[family]) {
                prev[family] = [];
            }
            prev[family].push(chartType)
            return prev;
        }, {});
        let count = 0;
        const typesByCategory = _.reduce(typesByCategoryTmp, (prev, curr, key) => {
            if (count < this.state.scrollSize) {
                prev[key] = this.state.scrollSize - count < curr.length ? curr.slice(0, this.state.scrollSize - count) : curr;
                count += prev[key].length;
            }
            return prev;
        }, {})
        return <Row>
            <Col md={{ span: 24 }} lg={{ span: 16 }}>
                <div className="">
                    {chartTypes.length ? _.map(typesByCategory, (cTypes, category) => {
                        return <div key={category}>
                            <h3>{category.substring(category.indexOf("-") + 1).trim().camelPeakToBlankSplit()}</h3>
                            <div className="d-flex align-content-center flex-wrap">
                                {_.map(cTypes, (chartType, index) => {
                                    const baseChartConfig = getChartConfig(chartType);
                                    let key = getKey(baseChartConfig);
                                    let currentKey = getKey(chartConfig);
                                    return <DrawChart key={key} className={currentKey === key ? "selected" : ""} onMouseOver={() => {
                                        this.setState({ hoverChartType: chartType.type });
                                    }} onClick={(e) => {
                                        const { chartEditor, chartConfig } = settings.state;
                                        delete baseChartConfig.constructorFunc;
                                        if (chartConfig.type === baseChartConfig.type &&
                                            _.isEqual(chartConfig.options, baseChartConfig.options)) {
                                            return showToast("Drawing!", 'warning');
                                        }
                                        settings.setState({ chartConfig: _.assign({}, chartConfig, baseChartConfig) }, () => {
                                            chartEditor && chartEditor.run(undefined, false);
                                        })
                                    }} chartConfig={baseChartConfig} chartData={chartData} allDatas={allDatas}></DrawChart>
                                })}
                            </div>
                        </div>
                    }) : <Alert message={<span>No chart can be generated for the currently selected field. Try <Button onClick={() => {
                        this.setState({ activeKey: "Manual" })
                    }}
                        type="link">Manual</Button></span>} type="error" showIcon />}
                    {!!chartTypes.length && chartTypes.length > this.state.scrollSize && <Button icon={<ChevronDownIcon />} size='small' onClick={() => {
                        if (!this.ticking) {
                            this.ticking = true;
                            if (this.chartTypesTmp && this.chartTypesTmp.length < this.state.scrollSize) {
                                console.log("i am bottom!");
                                this.ticking = false;
                                return;
                            }
                            this.setState({ scrollSize: this.state.scrollSize + 2 }, () => {
                                this.ticking = false;
                            })
                        }
                    }} > More results  </Button>}
                </div>
            </Col>
            <Col md={{ span: 0 }} lg={{ span: 8 }}>
                <div className="chart-description">
                    <h1>{hoverChartType || type}</h1>
                    {_.map(ChartTypesDesc[hoverChartType || type], (v, k) => {
                        return <div key={k}>
                            <h3 > {k.camelPeakToBlankSplit()}  </h3>
                            {_.map(v.split("\n"), (piece, index) => {
                                return <p key={index}>{piece.trim()}</p>
                            })}
                        </div>
                    })}
                </div>
            </Col>
        </Row>
    }

    render() {
        /**@type {AntChartSetting} */
        const settings = this.props.settings;
        const { chartConfig: { chartData, chartColumns, type } } = settings.state;
        /**use the extract datas don't use origin chartData!!! */
        let chartDataTmp = _.cloneDeep(chartData)
        chartData.keysArr && (chartDataTmp.keysArr = chartData.keysArr);
        const allDatas = autoObjectsType(extractDatas(chartDataTmp), false)
        let chartTypes = checkChartTypes(allDatas);
        this.chartTypesTmp = chartTypes;
        const { className, selectedChartType, activeKey, load, hoverChartType } = this.state;
        const allKeys = extractAllKeys(allDatas);
        let sortChartTypes = _.sortBy(_.reduce(ChartTypes, (prev, curr, chartType) => {
            prev.push({
                chartType,
                highlight: _.filter(chartTypes, (ctype) => {
                    return ctype.type === chartType
                }).length,
                disabled: (KEYS_LENS[chartType] || getKeys(chartType).length) > allKeys.length,
            })
            return prev;
        }, []), [function (o) { return o.highlight ? -2 : (o.disabled ? 0 : -1) }])
        return <div
            className={`choose-chart ${className || ''}`}>
            <Tabs activeKey={activeKey || (chartTypes.length ? "Suggested" : "Manual")} onChange={(activeKey) => {
                this.setState({ activeKey })
            }}>
                <TabPane tab="Suggested" key="Suggested">
                    <h2>Select a chart</h2>
                    <this.SuggestedComp chartTypes={chartTypes} allDatas={allDatas}></this.SuggestedComp>
                </TabPane>
                <TabPane tab="Manual" key="Manual">
                    <Row >
                        <Col md={{ span: 24 }} lg={{ span: 8 }}>
                            <Space direction="vertical" size={"large"}>
                                <div className="d-flex align-items-center" >
                                    <div>Choose a chart type:</div><Select
                                        allowClear
                                        showSearch
                                        style={{ width: 200 }}
                                        placeholder="Search to Select"
                                        defaultValue={load ? type : undefined}
                                        optionFilterProp="children"
                                        filterOption={(input, option) =>
                                            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                        }
                                        filterSort={(optionA, optionB) =>
                                            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                                        }
                                        onChange={(chartType) => {
                                            let state = { selectedChartType: chartType, keys: getKeys(chartType), hoverChartType: chartType };
                                            let arr = _.filter(chartTypes, (ctype) => { return ctype.type === chartType });
                                            if (arr.length) {
                                                _.assign(state, _.pick(arr[0].config, _.keys(Fields)))
                                            } else {
                                                _.assign(state, _.cloneDeep(Fields))
                                            }
                                            this.setState(state);
                                        }}
                                    >
                                        {_.map(sortChartTypes, (obj, index) => {
                                            const { chartType, highlight, disabled } = obj;
                                            return <Option disabled={disabled}
                                                className={cx({
                                                    "text-primary": highlight
                                                })}
                                                key={chartType} value={chartType}>{`${chartType}`}</Option>;
                                        })}
                                    </Select>
                                </div>
                                <this.SelectChartTypeComp allDatas={allDatas}></this.SelectChartTypeComp>
                            </Space>
                        </Col>
                        <Col md={{ span: 24 }} lg={{ span: 8 }}>
                            <this.DrawChartComp allDatas={allDatas}></this.DrawChartComp>
                        </Col>
                        <Col md={{ span: 24 }} lg={{ span: 8 }}>
                            <h1>{hoverChartType || type}</h1>
                            {_.map(ChartTypesDesc[hoverChartType || type], (v, k) => {
                                return <div key={k}>
                                    <h3 > {k.camelPeakToBlankSplit()}  </h3>
                                    {_.map(v.split("\n"), (piece, index) => {
                                        return <p key={index}>{piece.trim()}</p>
                                    })}
                                </div>
                            })}
                        </Col>
                    </Row>
                </TabPane>
            </Tabs>
            <DataComp data={allDatas} columns={chartColumns}></DataComp>
        </div>;
    }
}

class DrawChart extends React.Component {

    static propTypes = {
        chartData: PropTypes.array,
        allDatas: PropTypes.array,
        chartConfig: PropTypes.object,
        className: PropTypes.string,
    }

    static defaultProps = {};
    constructor(props) {
        super(props);
        const { chartData, allDatas, chartConfig, className, ...propsa } = this.props;
        /**
         * @type {{
            options: chartOptionsDefault,
            constructorFunc: Line | Area | Column,
            type: string,
        }}
         */
        let chartConfigTmp = chartConfig;
        this.state = {
            /**@type{[]} */
            chartData,
            allDatas,
            ...chartConfigTmp,
            clazz: chartConfigTmp.type.replace(/\s/g, '-'),
            className,
            propsa,
            scale: 0.7,
        }
        this.domRef = React.createRef();
        this.warningRef = React.createRef();
        this.darkModeElement = document.createElement("div");
    }

    componentDidUpdate(preProps, preState) {
        const { className, chartData, allDatas } = this.props;
        let state = {};
        if (preProps.className !== className) {
            _.assign(state, { className });
        }
        if (!_.isEqual(preProps.chartData, chartData)) {
            _.assign(state, { chartData });
        }
        if (!_.isEqual(preProps.allDatas, allDatas)) {
            _.assign(state, { allDatas });
        }
        !_.isEmpty(state) && this.setState(state, () => {
            if (state.chartData || state.allDatas) {
                this.draw();
            }
        });
    }

    componentWillUnmount() {
        actions.deleteCache(this.darkModeElement);
        this.destroy();
    }

    destroy() {
        if (this.chart) {
            this.chart.destroy();
            this.chart = undefined;
        }
    }

    draw = () => {
        if (this.chart) {
            this.destroy();
        }
        const { chartData, allDatas, options, constructorFunc, scale, type } = this.state;
        /**@type {HTMLElement} */
        const container = this.domRef.current;
        if (container && constructorFunc) {
            container.innerHTML = "";
            let exOptionsTmp = _.assign(
                _.cloneDeep(options),
                {
                    width: DEFAULT_WIDTH * scale,
                    height: DEFAULT_HEIGHT * scale,
                    data: extractDatas(allDatas, _.values(_.pick(options, _.keys(Fields))))
                }
            );
            let exOptions = extractOptions(exOptionsTmp);
            exOptions.data.keysArr = exOptionsTmp.data.keysArr;
            if (exOptions.data.length > MAX_SIZE) {
                container.innerHTML = `<div class="d-flex align-items-center" style="width: ${DEFAULT_WIDTH * scale}px; height: ${DEFAULT_HEIGHT * scale}px"> 
                    Too many data(length more than ${MAX_SIZE}), can not display right now!
                </div>`
            } else {
                exOptions.theme = this.darkMode ? "dark" : "default";
                /** */
                this.chart = new constructorFunc(container, exOptions);
                this.chart.render();
            }
            if (type !== ChartTypes['Liquid']) {
                let minus = chartData.length - exOptions.data.length;
                this.warningRef.current && (this.warningRef.current.innerHTML = minus ? `${minus} rows ignore for having one or more empty cells` : "");
            }
        }
    }

    componentDidMount() {
        // this.draw();
        actions.inspector(this.darkModeElement, [actions.types.DARK_MODE], (darkMode) => {
            if (this.darkMode !== darkMode) {
                this.darkMode = darkMode;
                this.draw();
            }
        })
    }

    render() {
        const { className, clazz, propsa, type, options, scale } = this.state;
        return <div className={`draw-chart ${className ? className : ""}`}
            style={{
                width: DEFAULT_WIDTH * scale,
                // maxWidth: "calc(100vw - 106px)",
                // overflowX: "auto",
            }}>
            <h6>{options.title.text}
            </h6>
            <div ref={this.domRef}
                style={{
                    width: DEFAULT_WIDTH * scale, height: DEFAULT_HEIGHT * scale, overflowY: "auto",
                }}
                className={`draw-chart-type ${clazz}`} {...propsa}></div >
            <div className="text-warning" ref={this.warningRef}></div>
        </div >
    }
}