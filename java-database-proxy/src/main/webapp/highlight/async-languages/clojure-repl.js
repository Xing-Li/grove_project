/*
Language: Clojure REPL
Description: Clojure REPL sessions
Author: Ivan Sagalaev <maniac@softwaremaniacs.org>
Requires: clojure.js
Category: lisp
*/

define(["./clojure.js"], (...$requiredLanguages) => function(hljs) {
        hljs.registerLanguage("clojure", $requiredLanguages[0]);
          
          
  return {
    contains: [
      {
        className: 'meta',
        begin: /^([\w.-]+|\s*#_)?=>/,
        starts: {
          end: /$/,
          subLanguage: 'clojure'
        }
      }
    ]
  }
}
)