import observe from "@observablehq/stdlib/src/generators/observe.js";

export default function() {
  return observe(function(change) {
    var width = change(document.body.clientWidth - 104);
    function resized() {
      var w = document.body.clientWidth - 104;
      if (w !== width) change(width = w);
    }
    window.addEventListener("resize", resized);
    return function() {
      window.removeEventListener("resize", resized);
    };
  });
}
