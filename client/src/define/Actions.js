import { createModule } from "util/hqUtils";
import ActionsAbs from "./ActionsAbs";
const { DefaultDatabase } = require("util/databaseApi");
const { ReadWriteOptions } = require("util/utils");

window.innerMain = createModule("inner");
/**
 * use hq's runtime handle UIs state
 */
class Actions extends ActionsAbs {
    constructor() {
        super(window.innerMain);
        this.types = {
            /**{ DbClients, key } db clients */
            DB_CLIENTS: 'DB_CLIENTS',
            /**{ ShareData, key } can be treated as chart data */
            SHARE_DATA: 'SHARE_DATA',
            /**
             * boolean 
             */
            LINE_NUMBERS: 'LINE_NUMBERS',
            /**
             * boolean 
             */
            VIM_ENABLED: 'VIM_ENABLED',
            /**
             * boolean
             */
            AUTO_FORMAT: 'AUTO_FORMAT',
            /**
             * boolean
             */
            DARK_MODE: 'DARK_MODE',
            /**boolean*/
            MASK: 'MASK',
            /**@type {import ('../util/localstorage').DefaultShareData} */
            LINK_CHANGE: 'LINK_CHANGE',
            /**@type {import ('../util/localstorage').DefaultFileData} */
            FILE_CHANGE: 'FILE_CHANGE',
            /**@type {{editorKey:string}} */
            RUN_COMPLETE: 'RUN_COMPLETE',
            /**undefined current open file's attachments need refresh */
            REFRESH_FILE_ATTACHMENTS: 'REFRESH_FILE_ATTACHMENTS',
            /**
             * number
             */
            FILE_VERSION_CHANGE: 'FILE_VERSION_CHANGE',
            /**
             * {
             * widthScale: percent number,
             * heightScale:percent number
             * }
             */
            RESIZE_WINDOW: "RESIZE_WINDOW",
            /**
             *number 
             */
            AUTO_SAVE_IN_SECOND: "AUTO_SAVE_IN_SECOND",
            /**
             * boolean
             */
            AUTO_SAVE: "AUTO_SAVE",
            /**
             * boolean
             */
            READONLY_CHECKED: "READONLY_CHECKED",
            /**
             * @type {Object.<string,boolean>} current editor readonly state modified
             */
            CURRENT_EDITOR_READ_ONLY: "CURRENT_EDITOR_READ_ONLY",
            /**
             * @type {ReadWriteOptions}
             */
            READWRITE_MODE: "READWRITE_MODE",
            /**
             * string
             */
            NOTICE_MODE: "NOTICE_MODE",
            /**
             * boolean view all codes
             */
            VIEW_CODE: "VIEW_CODE",
            /**
             * @type {Object.<string,Set>}
             */
            FILES_ATTACHES: "FILES_ATTACHES",
            /**
             * string
             */
            THEME: "THEME",
            /**
             * long number
             */
            NOW_TIME: "NOW_TIME",
            /**
             * @type {Object.<string,boolean>}
             */
            NEED_SAVE_FILES: "NEED_SAVE_FILES",
            /** */
            FOCUS_BLOCK_CHANGE: "FOCUS_BLOCK_CHANGE",
            /** */
            MENUS_POP: "MENUS_POP",
            /**@type {{editorKey:string, openSelect: boolean}} */
            OPEN_SELECT: "OPEN_SELECT",
            /**
             * boolean
             */
            READY: "READY",
            /** */
            TRIGGER: 'TRIGGER',
            MDFILES: 'MDFILES',
            /**Users in the file room */
            USERS: 'USERS',
            /**project shared to users */
            PROJECT_SHARE_USERS: "PROJECT_SHARE_USERS",
            /**@type {Object.<string, Object>} project shared to users permission */
            USERS_SHARE_PERMISSION: "USERS_SHARE_PERMISSION",
            /**@type {[DefaultDatabase]} */
            DATABASES: "DATABASES",
            SECRETS: "SECRETS",
            LOADING: 'LOADING',
            SAFE_MODE: 'SAFE_MODES',
            READ_ONLY_MODE: 'READ_ONLY_MODE',
            FULL_SCREEN: 'FULL_SCREEN',
            SW_PROJECT: 'SW_PROJECT',
            HINT: 'HINT',
            /**{drawerObj:DrawerType,cb:function,only:boolean} only:only open it or switch*/
            DRAWER_TYPE: 'DRAWER_TYPE',
            CHART_EDITOR: 'CHART_EDITOR',
            /**@type {object} graphxrApi */
            REFRESH_GRAPH: 'REFRESH_GRAPH',
            UPDATE_GXR: 'UPDATE_GXR',
            /**{file:object}*/
            RENAME: "RENAME",
            /**boolean */
            DUPLICATE: "DUPLICATE",
            /**boolean */
            DOWNLOAD: "DOWNLOAD",
            /**boolean */
            DOWNLOAD_FILE: "DOWNLOAD_FILE",
            /**{acceptedFiles: [], selectPlace: boolean} */
            IMPORT_FILES: "IMPORT_FILES",
            /**boolean */
            UPLOAD_CURRENT_FILE: "UPLOAD_CURRENT_FILE",
            /**@type{Object.<string, EditorAbs} tag editors */
            TAG_EDITORS: "TAG_EDITORS",
            /**@type{EditorAbs} */
            CURRENT_EDITOR: 'CURRENT_EDITOR',
            /**@type{DefaultChartConfig} */
            CHART_FULL_SCREEN: 'CHART_FULL_SCREEN',
            /**@type{{data:HTMLElement}} */
            OPEN_CODE: 'OPEN_CODE',
        }
    }
}
const actions = new Actions();
window.actions = actions;
export default actions;