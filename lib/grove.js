const express = require('express');
const fs = require('fs');
const fsextra = require('fs-extra');
const path = require('path');
const zlib = require('zlib');
const tar = require('tar');
const mkdirp = require('mkdirp');
const async = require('async');
const _ = require("lodash");
const multer = require('multer');// for parsing multipart/form-data
const JSZip = require('jszip');
const iconv = require('iconv-lite');

const { sendFileStatus, sendProjectStatus } = require('../modules/websocket/GroveChatSocket');
const auth = require('../modules/util/auth');
/**published */
const GroveShareProjects = require('../modules/groveShareProjects/GroveShareProjects.model');
/**shared */
const GroveShared = require('../modules/groveShared/GroveShared.model');
const CustomDB = require('../modules/neo4j/customDB.model');
const User = require('../modules/user/user.model');
const UserShareProjects = require('../modules/userShareProjects/UserShareProjects.model');

const formDataParse = multer().any();
const router = express.Router();
const VER_FILE_TAG = ".ver"
const COMMON = "common";
const FAIL_STATUS = 1;
const SUCCESS_STATUS = 0;
const BOOK_ID = "observable";
const DEFAULT_VER = 0;
const SETTINGS_NAME = "settings.json";
const adminEmail = AppConfig && AppConfig.adminEmail ? (AppConfig.adminEmail).trim().toLowerCase() : null;
const EXPECT_PROJECT_IDS = ["userGuideProjectId"];

const maxLength = 50 * 1024 * 1024;
const { SHAREDS_TYPE, CanOptions, ShareProjectCanOptions, DefaultShareProjectCanOption, FileStatus, ProjectStatus } = require("../../modules/websocket/MsgType")
const { fileSeparator, DefaultFileType, DefaultFileData, DefaultFolderData, DefaultFileSettings, isFileGzip, isFileZip,
    fileNameReg, folderNameReg, imageTypeReg, TmpDir, isFileEditorJs, getFileType, getFileName } = require('./fileUtils');
const { Runtime, Module, observer, mdDataFunc, trimFileName } = require('./translate');

const State = {
    variable: "variable",
    import: "import",
}

/**
 * 
 * @param {*} type 
 * @param {*} fileVerPath 
 * @returns {DefaultFileSettings}
 */
function getAndLoadSettings(type, fileVerPath) {
    if (!isFileEditorJs({ type })) {
        return {};
    }
    let settingsFile = path.join(fileVerPath, "settings.json");
    let settings = DefaultFileSettings;
    if (!fs.existsSync(fileVerPath)) {
        fs.mkdirSync(fileVerPath, { recursive: true });
        fs.writeFileSync(settingsFile, JSON.stringify(settings, undefined, 2));
    }
    settings = JSON.parse(fs.readFileSync(settingsFile).toString("utf-8"))
    !settings.messages && (settings.messages = DefaultFileSettings.messages);
    return settings;
}
/**
 * 
 * @param {string} walkPath 
 * @param {string} rootPath 
 * @param {Array} projects 
 * @param {Function} callback 
 */
function handleWalk(walkPath, rootPath, projects, callback, noSettings = false) {
    let ret = {};
    let sharedFileKeys = _.reduce(projects, (prev, project) => {
        prev[project.fileKey] = project;
        return prev;
    }, {})
    if (!fs.existsSync(walkPath) || !fs.statSync(walkPath).isDirectory()) {
        return callback(null, ret)
    }
    let walk = function (dir, done) {
        let list = fs.readdirSync(dir);
        let i = 0;
        (function next() {
            let fileName = list[i++];
            if (!fileName) {
                return done(null, ret);
            }
            if (fileName.endsWith(VER_FILE_TAG)) {
                next();
                return;
            }
            let filePath = dir + fileSeparator + fileName;
            let stat = fs.statSync(filePath)
            if (stat && stat.isDirectory()) {
                let folderKey = (filePath.substring(rootPath.length + 1) + fileSeparator).replace(/\\/g, fileSeparator);
                ret[folderKey] = Object.assign({}, DefaultFolderData,
                    { folderKey: folderKey, name: fileName });
                walk(filePath, function (error) {
                    if (error) {
                        console.error(error);
                        return done(error);
                    }
                    next();
                });
            } else {
                let fileKey = filePath.substring(rootPath.length + 1).replace(/\\/g, fileSeparator);
                let type = getFileType(fileKey, fileName);
                try {
                    let settings = noSettings ? {} : getAndLoadSettings(type, path.join(dir, `${fileName}${VER_FILE_TAG}`));
                    ret[fileKey] = Object.assign({}, DefaultFileData,
                        {
                            fileKey: fileKey,
                            name: fileName,
                            size: stat.size,
                            mtimeMs: stat.mtimeMs,
                            type: type,
                            version: settings.version,
                            final: settings.final,
                            versionMtimeMs: settings.versionMtimeMs || DefaultFileSettings.versionMtimeMs,
                            previewImageUpload: settings.previewImageUpload || DefaultFileSettings.previewImageUpload,
                            messages: settings.messages,
                            shareData: sharedFileKeys[fileKey] || null
                        });
                } catch (error) {
                    console.error(error);
                    return done(error);
                }
                next();
            }
        })();
    };
    walk(walkPath, callback);
}

function translate(newDir, dirKey, mName) {
    let indexjsFilePath = path.join(newDir, "index.js");
    if (!fs.existsSync(indexjsFilePath)) {
        return;
    }
    let matchArr = fs.readFileSync(indexjsFilePath).toString("utf-8").match(/"\.\/(.*)"/);
    if (!matchArr) {
        return;
    }
    // fs.unlinkSync(indexjsFilePath);
    let mainFileName = trimFileName(matchArr[1]);
    let list = fs.readdirSync(newDir);
    for (let index = 0; index < list.length; index++) {
        let fileName = list[index];
        if (!fileName.endsWith(".js")) {
            continue;
        }
        let filePath = path.join(newDir, fileName);
        let content = fs.readFileSync(filePath).toString("utf-8");
        // fs.unlinkSync(filePath);
        let Flag = "export default ";
        if (!~content.indexOf(Flag)) {
            continue;
        }
        let defines = _.reduce(
            content.substring(0, content.indexOf(Flag)).trim().split("\n"),
            (prev, curr) => {
                let matchArr = curr.match(/"\.\/(.*\.js)"/);
                if (matchArr) {
                    let fileKey = `${dirKey}${trimFileName(matchArr[1])}`;
                    prev.push(`${fileKey}`);
                }
                return prev;
            }, []
        );
        let funcBody = content.substring(content.indexOf("{") + 1, content.lastIndexOf("}"))
            .replace(/,import\.meta\.url/g, "");
        let args = ["runtime", "observer", "URL", "Map"];
        _.each(defines, (define, index) => {
            args.push(`define${index + 1}`);
        })
        let runtime = new Runtime(dirKey);
        let url = String;
        let func = new Function(args, funcBody);
        /**@type Module */
        let ret = func(runtime, observer, url, runtime.map(), ...defines);
        let newFileName = trimFileName(fileName);
        newFileName = mainFileName === newFileName ? mName : newFileName;
        let newFileContent = JSON.stringify(mdDataFunc(ret.getBlocks()));
        fs.writeFileSync(path.join(newDir, newFileName), newFileContent);
        /**@type Map */
        let map = runtime.arrs;
        map && map.forEach((relativePath, fileName) => {
            let oldFilePath = path.join(newDir, String(relativePath).toString());
            let newFilePath = path.join(newDir, "files", fileName);
            fs.renameSync(oldFilePath, newFilePath);
        })
    }
}


function translate2(newDir, dirKey, mName) {
    let indexjsFilePath = path.join(newDir, "index.js");
    if (!fs.existsSync(indexjsFilePath)) {
        return;
    }
    let matchArr = fs.readFileSync(indexjsFilePath).toString("utf-8").match(/"\.\/(.*)"/);
    if (!matchArr) {
        return;
    }
    // fs.unlinkSync(indexjsFilePath);
    let mainFileName = trimFileName(matchArr[1]);
    let list = fs.readdirSync(newDir);
    for (let index = 0; index < list.length; index++) {
        let fileName = list[index];
        if (!fileName.endsWith(".js")) {
            continue;
        }
        let filePath = path.join(newDir, fileName);
        let content = fs.readFileSync(filePath).toString("utf-8");
        // fs.unlinkSync(filePath);
        let FuncFlag = "function ";
        let Flag = "export default ";
        if (!~content.indexOf(FuncFlag)) {
            continue;
        }
        if (!~content.indexOf(Flag)) {
            continue;
        }
        if (content.indexOf(FuncFlag) > content.indexOf(Flag)) {
            let defines = _.reduce(
                content.substring(0, content.indexOf(Flag)).trim().split("\n"),
                (prev, curr) => {
                    let matchArr = curr.match(/"\.\/(.*\.js)"/);
                    if (matchArr) {
                        let fileKey = `${dirKey}${trimFileName(matchArr[1])}`;
                        prev.push(`${fileKey}`);
                    }
                    return prev;
                }, []
            );
            let funcBody = content.substring(content.indexOf("{") + 1, content.lastIndexOf("}"))
                .replace(/,import\.meta\.url/g, "");
            let args = ["runtime", "observer", "URL", "Map"];
            _.each(defines, (define, index) => {
                args.push(`define${index + 1}`);
            })
            let runtime = new Runtime(dirKey);
            let url = String;
            let func = new Function(args, funcBody);
            /**@type Module */
            let ret = func(runtime, observer, url, runtime.map(), ...defines);
            let newFileName = trimFileName(fileName);
            newFileName = mainFileName === newFileName ? mName : newFileName;
            let newFileContent = JSON.stringify(mdDataFunc(ret.getBlocks()));
            fs.writeFileSync(path.join(newDir, newFileName), newFileContent);
            /**@type Map */
            let map = runtime.arrs;
            map && map.forEach((relativePath, fileName) => {
                let oldFilePath = path.join(newDir, String(relativePath).toString());
                let newFilePath = path.join(newDir, "files", fileName);
                fs.renameSync(oldFilePath, newFilePath);
            })
        } else {
            let AsyncFuncFlag = "async ";
            let funcStartIndex;
            if (~content.indexOf(AsyncFuncFlag) && content.indexOf(AsyncFuncFlag) < content.indexOf(FuncFlag)) {
                funcStartIndex = content.indexOf(AsyncFuncFlag);
            } else {
                funcStartIndex = content.indexOf(FuncFlag);
            }
            let defines = _.reduce(
                content.substring(0, funcStartIndex).trim().split("\n"),
                (prev, curr) => {
                    let matchArr = curr.match(/"\.\/(.*\.js)"/);
                    if (matchArr) {
                        let fileKey = `${dirKey}${trimFileName(matchArr[1])}`;
                        prev.push(`${fileKey}`);
                    }
                    return prev;
                }, []
            );
            let functionsContent = content.substring(funcStartIndex, content.indexOf(Flag));
            let definesContent = content.substring(content.indexOf(Flag));
            let funcBody = functionsContent.replace(/\?\./g, ".$wenhao$.") + definesContent.substring(definesContent.indexOf("{") + 1, definesContent.lastIndexOf("}"))
                .replace(/,\s{0,}import\.meta\.url/g, "");
            let args = ["runtime", "observer", "URL", "Map"];
            _.each(defines, (define, index) => {
                args.push(`define${index + 1}`);
            })
            let runtime = new Runtime(dirKey);
            let url = String;
            try {
                let func = new Function(args, funcBody);
                /**@type Module */
                let ret = func(runtime, observer, url, runtime.map(), ...defines);
                let newFileName = trimFileName(fileName);
                newFileName = mainFileName === newFileName ? mName : newFileName;
                let newFileContent = JSON.stringify(mdDataFunc(ret.getBlocks())).replace(/\.\$wenhao\$\./g, "?.");
                fs.writeFileSync(path.join(newDir, newFileName), newFileContent);
                /**@type Map */
                let map = runtime.arrs;
                map && map.forEach((relativePath, fileName) => {
                    let oldFilePath = path.join(newDir, String(relativePath.url).toString());
                    let newFilePath = path.join(newDir, "files", fileName);
                    try {
                        fs.renameSync(oldFilePath, newFilePath);
                    } catch (e) {
                        console.error(e);
                    }
                })
            } catch (error) {
                console.error(error);
            }
        }
    }
}


function handleWriteNotebookFile(file, fileDataBuffer, rootDir, dir, callback, unzip = false) {
    const { type, fileName } = file;
    let filePath = path.join(rootDir, dir, fileName);
    let fileKey = filePath.substring(rootDir.length + 1).replace(/\\/g, fileSeparator);
    if (!fileName || fileName == '') {
        return callback(new Error("No file name"), null);
    }
    if (!type) {
        return callback(new Error("Unsupport type"), null);
    }
    try {
        // fileDataBuffer = isFileGzip(file) ? Buffer.from(fileDataBuffer.toString(), 'base64') : fileDataBuffer;
        let currentUploadDir = path.join(rootDir, dir);
        if (!fs.existsSync(currentUploadDir)) {
            fs.mkdirSync(currentUploadDir, { recursive: true });
        }
        let verDir = path.join(rootDir, dir, `${fileName}${VER_FILE_TAG}`);
        let settings = getAndLoadSettings(type, verDir);
        async.waterfall([
            callback => {
                fs.writeFile(filePath, fileDataBuffer, 'binary', function (err) {
                    if (err) {
                        return callback(err, null)
                    } else {
                        fs.stat(filePath, function (err, stat) {
                            if (err) {
                                return callback(err, null)
                            } else {
                                const fileData = Object.assign({}, DefaultFileData,
                                    {
                                        fileKey: fileKey,
                                        name: fileName,
                                        type: type,
                                        size: stat.size,
                                        mtimeMs: stat.mtimeMs,
                                        version: settings.version,
                                        final: settings.final,
                                        versionMtimeMs: settings.versionMtimeMs || DefaultFileSettings.versionMtimeMs,
                                        previewImageUpload: settings.previewImageUpload || DefaultFileSettings.previewImageUpload,
                                        messages: settings.messages,
                                        shareData: undefined//Do not need transfer to client , so undefined
                                    });
                                if (unzip && isFileGzip(file)) {
                                    handleUnzip(file, rootDir, dir, (err, gzip) => {
                                        callback(err, fileData, gzip);
                                    });
                                } else {
                                    callback(null, fileData)
                                }
                            }
                        })
                    }
                });
            }], function (err, fileData, gzip) {
                if (err) {
                    console.log(err);
                    return callback(err, null)
                }
                callback(null, fileData, gzip);
            })
    } catch (error) {
        console.error(error);
        return callback(new Error(`Can't write the path : ${filePath}`), null);
    }
}
function handleUnzip(file, rootDir, dir, callback) {
    const { type, fileName } = file;
    let filePath = path.join(rootDir, dir, fileName);
    if (!fileName || fileName == '') {
        return callback(new Error("No file name"), null);
    }
    if (!type) {
        return callback(new Error("Unsupport type"), null);
    }
    if (!(isFileGzip(file) || isFileZip(file))) {
        return callback(new Error("Not zip or gzip file"), null);
    }
    async.waterfall([
        callback => {
            let newDir = path.join(rootDir, dir,
                fileName.substring(0, fileName.lastIndexOf('.')));
            if (!fs.existsSync(newDir)) {
                fs.mkdirSync(newDir, { recursive: true });
            }
            if (isFileGzip(file)) {
                fs.createReadStream(filePath)
                    .on('error', (err) => {
                        callback(err, null)
                    }).pipe(zlib.Unzip())
                    .pipe(tar.x({
                        C: newDir,
                        strip: 1,
                        filter: (path, stat) => {
                            return path.startsWith("./files/") || (path.endsWith(".js") && !path.endsWith("runtime.js"))
                        }
                    }).on("close", () => {
                        translate2(newDir, newDir.substring(rootDir.length + 1).replace(/\\/g, fileSeparator) + fileSeparator, fileName.substring(0, fileName.lastIndexOf('.')));
                        handleWalk(newDir, rootDir, [], function (error, ret) {
                            if (error) {
                                console.log(err);
                                callback(error, null)
                            } else {
                                callback(null, ret);
                            }
                        })
                    }));
            } else {
                new JSZip.external.Promise(function (resolve, reject) {
                    fs.readFile(filePath, function (err, data) {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(data);
                        }
                    });
                }).then(function (data) {
                    return JSZip.loadAsync(data, {
                        decodeFileName: function (bytes) {
                            return iconv.decode(bytes, 'gb2312');
                        }
                    });
                }).then(zip => {
                    const promiseList = _.map(_.keys(zip.files), function (zipFileKey) {
                        let file = zip.files[zipFileKey];
                        if (file.dir) {
                            return (callback) => {
                                let dest = newDir + fileSeparator + zipFileKey;
                                if (!fs.existsSync(dest)) {
                                    try {
                                        fs.mkdirSync(dest, { recursive: true });
                                    } catch (error) {
                                        return callback(error);
                                    }
                                }
                                return callback(null);
                            }
                        } else {
                            return (callback) => {
                                zip.file(zipFileKey).async('nodebuffer').then(function (content) {
                                    let dest = newDir + fileSeparator + zipFileKey;
                                    fs.writeFileSync(dest, content);
                                    callback(null)
                                });
                            }
                        }
                    })
                    async.waterfall(promiseList, function (err) {
                        if (err) {
                            console.log(err);
                            return callback(err)
                        }
                        translate2(newDir, newDir.substring(rootDir.length + 1).replace(/\\/g, fileSeparator) + fileSeparator, fileName.substring(0, fileName.lastIndexOf('.')));
                        handleWalk(newDir, rootDir, [], function (error, ret) {
                            if (error) {
                                console.log(error);
                                callback(error, null)
                            } else {
                                callback(null, ret);
                            }
                        })
                    })
                }, function (error) {
                    console.log(error);
                    callback(error, null)
                })
            }
        }], function (err, gzip) {
            if (err) {
                console.log(err);
                return callback(err, null)
            }
            callback(null, gzip);
        })
}
function getUploadUri(projectId, userId) {
    if (!projectId || !userId) {
        throw new Error("error!");
    }
    let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [userId];
    return `/tmp/observable/${rootDirName.join('/')}/`
}
/**
 * 
 * @param {*} req 
 * @param {*} projectId 
 * @param {*} userId project owner id
 * @param {string} shareProjectId if a share project
 * @param {Function} cb 
 * @param {number} permission 
 * @param {string} fileKey
 */
function checkProject(req, projectId, userId, shareProjectId, cb, permission, fileKey) {
    if (projectId === COMMON) {
        return cb(null, null);
    }
    async.waterfall([
        callback => {
            if (shareProjectId) {
                if (shareProjectId !== projectId) {
                    return callback(new Error("error share project"));
                }
                if (fileKey === SETTINGS_NAME) {
                    return callback(new Error(`can not modify ${SETTINGS_NAME}`));
                }
                UserShareProjects.getAllUserByProject(shareProjectId, function (err, users) {
                    if (err) {
                        return callback(err)
                    } else {
                        let user = !users ? undefined : _.find(users, (user, index) => {
                            user = JSON.parse(JSON.stringify(user))
                            return user.userId === req.user.id;
                        })
                        if (!user) {
                            return callback(new Error("can not find share user"));
                        }
                        let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [userId];
                        let settingsPath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, `settings.json`);
                        try {
                            let userSettings = JSON.parse(fs.readFileSync(settingsPath).toString("utf-8"));
                            if (((userSettings.usersSharePermission && userSettings.usersSharePermission[req.user.id]) || DefaultShareProjectCanOption) <
                                (permission === undefined ? ShareProjectCanOptions["Operate file system"] : permission)) {
                                return callback(new Error("can not edit project"))
                            }
                            callback(null, users);
                        } catch (err) {
                            return callback(err);
                        }
                    }
                });
            } else {
                callback(null, null);
            }
        }, (users, callback) => {
            if (users) {
                return callback(null, { user: userId });
            }
            if (~EXPECT_PROJECT_IDS.indexOf(projectId) && req.user.get("email") === adminEmail) {
                return callback(null, { user: userId });
            }
            CustomDB.getInfo(projectId,
                (err, project) => {
                    err = err || (!project ? new Error('Can not found the project') : null);
                    callback(err, project)
                })
        }, (project, callback) => {
            project = JSON.parse(JSON.stringify(project));
            if (project.user !== userId) {
                return callback(new Error("Not your own project"))
            }
            callback(null, project);
        }], (err, project) => {
            cb(err, project);
        });
}
/**
 * copy project 's grove folder
 * @param {*} fromProjectId 
 * @param {*} toProjectId 
 * @param {boolean} noSettings 
 */
function duplicateProjectFolder(fromProjectId, toProjectId, noSettings) {
    let fromRootDirName = [`projects`, fromProjectId];
    let toRootDirName = [`projects`, toProjectId];
    let fromFolderPath = path.join(TmpDir, `${BOOK_ID}`, ...fromRootDirName);
    let toFolderPath = path.join(TmpDir, `${BOOK_ID}`, ...toRootDirName);
    if (!fs.existsSync(fromFolderPath)) {
        return;
    }
    try {
        if (!fs.existsSync(toFolderPath)) {
            fs.mkdirSync(toFolderPath, { recursive: true });
        }
        fsextra.copySync(fromFolderPath, toFolderPath, {
            dereference: true,
            filter: noSettings ? file => {
                return file !== path.join(fromFolderPath, SETTINGS_NAME)
            } : undefined,
        });
    } catch (error) {
        return error;
    }
}
router.post('/uploadFolder', auth.protected, formDataParse, function (req, res, next) {
    let folderKey = req.body.folderKey;
    let folderName = req.body.folderName;
    let shareProjectId = req.body.shareProjectId;
    let userId = shareProjectId ? req.body.userId : req.user.id;
    let projectId = req.body.projectId;
    let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [userId];
    let rootDir = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName);
    let currentUploadDir = path.join(rootDir, folderKey);
    checkProject(req, projectId, userId, shareProjectId, (err, project) => {
        if (err) {
            let resJSON = {
                status: FAIL_STATUS,
                message: err.message,
            };
            return res.send(resJSON)
        }
        if (!fs.existsSync(currentUploadDir)) {
            try {
                fs.mkdirSync(currentUploadDir, { recursive: true });
                let folderData = Object.assign({}, DefaultFolderData,
                    { folderKey: folderKey, name: folderName });
                sendProjectStatus(getUploadUri(projectId, userId), req.user.id,
                    ProjectStatus["uploadFolder"], {
                    folderData
                })
                return res.send({
                    status: SUCCESS_STATUS,
                    message: "Upload folder Successful.",
                    content: folderData
                })
            } catch (error) {
                return res.send({
                    status: FAIL_STATUS,
                    message: `Can't create the folder : ${folderKey}`
                })
            }
        } else {
            return res.send({
                status: FAIL_STATUS,
                message: `Same name file exists! : ${folderKey}`
            })
        }
    })
});
router.post('/updateFileSettings', auth.protected, formDataParse, function (req, res, next) {
    let fileKey = req.body.fileKey;
    let type = getFileType(fileKey);
    let dir = (fileKey && ~fileKey.lastIndexOf(fileSeparator)) ? fileKey.substring(0, fileKey.lastIndexOf(fileSeparator)) : "";
    let fileName = req.body.fileName || (getFileName(fileKey));
    let shareProjectId = req.body.shareProjectId;
    let userId = shareProjectId ? req.body.userId : req.user.id;
    let projectId = req.body.projectId;
    let previewImageUpload = req.body.previewImageUpload;
    let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [userId];
    let rootDir = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName);
    let verDir = path.join(rootDir, dir, `${fileName}${VER_FILE_TAG}`);
    if (!isFileEditorJs({ type })) {
        return res.send({
            status: 1,
            message: "Can't update the file's settings",
        })
    }
    checkProject(req, projectId, userId, shareProjectId, (err, project) => {
        if (err) {
            let resJSON = {
                status: FAIL_STATUS,
                message: err.message,
            };
            return res.send(resJSON)
        }
        let settings = getAndLoadSettings(type, verDir);
        settings.previewImageUpload = previewImageUpload;
        let settingsFile = path.join(verDir, "settings.json");
        fs.writeFileSync(settingsFile, JSON.stringify(settings, undefined, 2));
        return res.send({
            status: SUCCESS_STATUS,
            message: "Update File Settings Successful."
        })
    }, undefined, fileKey);
});
router.post('/uploadFile', auth.protected, formDataParse, function (req, res, next) {
    let fileDataBuffer = null;
    let sharedId = req.body.sharedId;
    let shareProjectId = req.body.shareProjectId;
    let userId = (sharedId || shareProjectId) ? req.body.userId : req.user.id;
    let roomId = req.body.roomId;
    let fileKey = req.body.fileKey;
    let type = getFileType(fileKey);
    let dir = (fileKey && ~fileKey.lastIndexOf(fileSeparator)) ? fileKey.substring(0, fileKey.lastIndexOf(fileSeparator)) : "";
    let fileName = req.body.fileName || (getFileName(fileKey));
    let projectId = req.body.projectId;
    let unzip = req.body.unzip;
    let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [userId];
    if (req.file) {
        fileDataBuffer = req.file.buffer;
        fileName = fileName || req.file.originalname;
    } else if (req.files && req.files.length > 0) {
        fileDataBuffer = req.files[0].buffer;
        fileName = fileName || req.files[0].originalname;
    } else if (req.body.data) {
        fileDataBuffer = req.body.data;
    } else {
        return res.send({
            status: 1,
            message: "Can't got the file's buffer",
        })
    }
    if (fileDataBuffer.length > maxLength) {
        return res.send({
            status: 1,
            message: `${fileName} is larger than ${maxLength}`,
        })
    }
    let permission = CanOptions["Edit"];
    async.waterfall([
        callback => {
            checkProject(req, projectId, userId, shareProjectId, (err, project) => {
                if (err) {
                    return callback(err);
                }
                callback(null, project);
            }, permission, fileKey);
        }, (project, callback) => {
            if (sharedId) {
                GroveShared.getShared(sharedId, function (err, shared_project) {
                    if (err) {
                        return callback(err)
                    } else if (!shared_project) {
                        return callback(new Error("can not find shared project"))
                    } else {
                        if (shared_project.status < permission) {
                            return callback(new Error("can not edit shared project"))
                        } else if (shared_project.userId.toString() !== userId
                            || shared_project.projectId !== projectId
                            || shared_project.fileKey !== fileKey) {
                            return callback(new Error("error shared project"))
                        }
                        callback(null, shared_project);
                    }
                });
            } else {
                callback(null)
            }
        }], function (err, ret) {
            if (err) {
                let resJSON = {
                    status: FAIL_STATUS,
                    message: err.message,
                };
                return res.send(resJSON)
            }
            handleWriteNotebookFile({ fileKey, fileName, type }, fileDataBuffer,
                path.join(TmpDir, `${BOOK_ID}`, ...rootDirName),
                dir, function (err, ret, gzip) {
                    if (err) {
                        return res.send({
                            status: FAIL_STATUS,
                            message: err.message,
                        })
                    } else {
                        sendProjectStatus(getUploadUri(projectId, userId), req.user.id,
                            ProjectStatus["uploadFile"], {
                            fileData: ret,
                            gzip
                        })
                        sendFileStatus(roomId || `${getUploadUri(projectId, userId)}${fileKey}`, req.user.id, FileStatus["updated"]);
                        return res.send({
                            status: SUCCESS_STATUS,
                            message: "Upload file Successful.",
                            content: ret,
                            gzip
                        })
                    }
                }, unzip);
        }
    );
});
router.post('/unzip', formDataParse, function (req, res, next) {
    let sharedId = req.body.sharedId;
    let shareProjectId = req.body.shareProjectId;
    let userId = (sharedId || shareProjectId) ? req.body.userId : req.user.id;
    let roomId = req.body.roomId;
    let fileKey = req.body.fileKey;
    let type = getFileType(fileKey);
    let dir = (fileKey && ~fileKey.lastIndexOf(fileSeparator)) ? fileKey.substring(0, fileKey.lastIndexOf(fileSeparator)) : "";
    let fileName = req.body.fileName || (getFileName(fileKey));
    let projectId = req.body.projectId;
    let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [userId];
    let permission = CanOptions["Edit"];
    async.waterfall([
        callback => {
            checkProject(req, projectId, userId, shareProjectId, (err, project) => {
                if (err) {
                    return callback(err);
                }
                callback(null, project);
            }, permission, fileKey);
        }, (project, callback) => {
            if (sharedId) {
                GroveShared.getShared(sharedId, function (err, shared_project) {
                    if (err) {
                        return callback(err)
                    } else if (!shared_project) {
                        return callback(new Error("can not find shared project"))
                    } else {
                        if (shared_project.status < permission) {
                            return callback(new Error("can not edit shared project"))
                        } else if (shared_project.userId.toString() !== userId
                            || shared_project.projectId !== projectId
                            || shared_project.fileKey !== fileKey) {
                            return callback(new Error("error shared project"))
                        }
                        callback(null, shared_project);
                    }
                });
            } else {
                callback(null)
            }
        }], function (err, ret) {
            if (err) {
                let resJSON = {
                    status: FAIL_STATUS,
                    message: err.message,
                };
                return res.send(resJSON)
            }
            handleUnzip({ fileKey, fileName, type }, path.join(TmpDir, `${BOOK_ID}`, ...rootDirName), dir, (err, gzip) => {
                sendProjectStatus(getUploadUri(projectId, userId), req.user.id,
                    ProjectStatus["uploadFile"], {
                    fileData: undefined,
                    gzip
                })
                sendFileStatus(roomId || `${getUploadUri(projectId, userId)}${fileKey}`, req.user.id, FileStatus["updated"]);
                return res.send({
                    status: SUCCESS_STATUS,
                    message: "Unzip file Successful.",
                    content: undefined,
                    gzip: gzip
                })
            })
        }
    );
})
router.post('/versionFiles', formDataParse, function (req, res, next) {
    let fileKey = req.body.fileKey;
    let projectId = req.body.projectId;
    let userId = req.body.userId;
    let splits = _.filter(fileKey.split(fileSeparator), (c) => { return c !== "" });
    let dirs = splits.slice(0, splits.length - 1);
    let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [userId];
    let fileName = getFileName(fileKey);
    let walkPath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, ...dirs, `${fileName}${VER_FILE_TAG}`);
    if (!fs.existsSync(walkPath)) {
        return res.send({
            status: 0,
            message: "Get version files Successful.",
            content: {},
            userName: userName
        })
    }
    async.waterfall([callback => {
        handleWalk(walkPath, walkPath, [], function (error, ret) {
            if (error) {
                return callback(new Error(`Can't get version files : ${walkPath}`))
            } else {
                return callback(null, ret);
            }
        }, true);
    }], function (error, result) {
        if (error) {
            let resJSON = {
                status: "failed",
                message: `Can't get version files : ${walkPath}`,
            };
            return res.send(resJSON)
        } else {
            return res.send({
                status: 0,
                message: "Get version files Successful.",
                content: result,
            })
        }
    })
});
router.post('/linkTo', formDataParse, function (req, res, next) {
    let fileKey = req.body.fileKey;
    let projectId = req.body.projectId;
    let userId = req.body.userId;
    let splits = _.filter(fileKey.split(fileSeparator), (c) => { return c !== "" });
    let dirs = splits.slice(0, splits.length - 1);
    let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [userId];
    let rootPath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName);
    let walkPath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, ...dirs, "files");
    async.waterfall([callback => {
        if (userId && userId !== "undefined") {
            User.getInfo(userId, undefined, (err, userInfo) => {
                callback(null, userInfo)
            })
        } else {
            callback(null, { firstName: "UnKnown", lastName: "Author" })
        }
    }], function (error, result) {
        if (!result) {
            let resJSON = {
                status: "failed",
                message: `Can't get user info : ${userId}`,
            };
            return res.send(resJSON)
        }
        let userName = `${result.firstName} ${result.lastName}`;
        let email = result.email;
        if (error) {
            let resJSON = {
                status: "failed",
                message: `Can't get LinkTo info : ${walkPath}`,
            };
            return res.send(resJSON)
        }
        if (!fs.existsSync(walkPath)) {
            return res.send({
                status: 0,
                message: "Get LinkTo info Successful.",
                content: {},
                userName: userName,
                email: email,
            })
        }
        handleWalk(walkPath, rootPath, [], function (error, ret) {
            if (error) {
                let resJSON = {
                    status: "failed",
                    message: `Can't get LinkTo info : ${walkPath}`,
                };
                return res.send(resJSON)
            } else {
                return res.send({
                    status: 0,
                    message: "Get LinkTo info Successful.",
                    content: ret,
                    userName: userName,
                    email: email,
                })
            }
        });
    });
});
router.post('/loadShare', formDataParse, function (req, res, next) {
    let userId = req.body.userId;
    let projectId = req.body.projectId;
    let fileKey = req.body.fileKey;
    let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [userId];
    let filePath = path.join(TmpDir, `${BOOK_ID}`, rootDirName.join('/'), fileKey || '');
    async.waterfall([
        callback => {
            if (!fileKey || !fs.existsSync(filePath)) {
                callback(null, null);
            } else {
                let stat = fs.statSync(filePath);
                callback(null, {
                    fileKey: fileKey,
                    name: path.basename(fileKey),
                    type: getFileType(fileKey),
                    size: stat.size,
                    mtimeMs: stat.mtimeMs,
                    version: DefaultFileSettings.version,
                    final: DefaultFileSettings.final,
                    versionMtimeMs: DefaultFileSettings.versionMtimeMs,
                    shareData: null
                })
            }
        },
    ], (err, fileInfo) => {
        return res.send({
            status: 0,
            content: !fileInfo ? {} : { [fileKey]: fileInfo },
            message: "Load Successful.",
            uploadUri: `/tmp/${BOOK_ID}/${rootDirName.join("/")}/`,
        })
    })
});

router.post('/load', auth.protected, formDataParse, function (req, res, next) {
    let settings = req.body.settings;
    let userId = req.user.id;
    let projectId = req.body.projectId;
    let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [userId];
    let walkPath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName);
    let settingsPath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, `settings.json`);
    if (!fs.existsSync(walkPath)) {
        fs.mkdirSync(walkPath, { recursive: true });
    }
    if (!fs.existsSync(settingsPath) && settings) {
        fs.writeFileSync(settingsPath, JSON.stringify(settings, undefined, 2));
    }
    async.waterfall([
        callback => {
            GroveShareProjects.getAllShareProjects({ option: { userId, projectId } }, function (err, projects) {
                if (err) {
                    return callback(err)
                }
                callback(null, projects);
            });
        }, (projects, callback) => {
            handleWalk(walkPath, walkPath, projects, callback);
        }], function (error, ret) {
            if (error) {
                let resJSON = {
                    status: "failed",
                    message: `Can't load the path : ${walkPath}`,
                };
                return res.send(resJSON)
            } else {
                let userSettings = settings;
                try {
                    userSettings = JSON.parse(fs.readFileSync(settingsPath).toString("utf-8"))
                } catch (error) {
                    userSettings = settings;
                }
                return res.send({
                    status: 0,
                    message: "Load Successful.",
                    content: ret,
                    uploadUri: `/tmp/${BOOK_ID}/${rootDirName.join("/")}/`,
                    settings: userSettings
                })
            }
        })
});

router.post('/removeFile', auth.protected, formDataParse, function (req, res, next) {
    let _id = req.body._id;
    let fileKey = req.body.fileKey;
    let shareProjectId = req.body.shareProjectId;
    let userId = shareProjectId ? req.body.userId : req.user.id;
    let projectId = req.body.projectId;
    let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [userId];
    let filePath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, fileKey);
    let verFilePath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, `${fileKey}${VER_FILE_TAG}`);
    async.waterfall([
        callback => {
            checkProject(req, projectId, userId, shareProjectId, (err, project) => {
                if (err) {
                    return callback(err);
                }
                callback(null, project);
            }, undefined, fileKey);
        }, (project, callback) => {
            if (!_id) {
                return callback(null);
            }
            GroveShareProjects.deleteShareProjectById(_id, function (err, result) {
                callback(err, result);
            })
        }], (err, result) => {
            if (err) {
                console.error(err);
                let resJSON = {
                    status: "failed",
                    message: err.message,
                };
                return res.send(resJSON)
            }
            try {
                if (fs.existsSync(filePath)) {
                    fs.unlinkSync(filePath);
                }
                if (fs.existsSync(verFilePath) && fs.rmdirSync) {
                    fs.rmdirSync(verFilePath, { recursive: true });
                }
                sendProjectStatus(getUploadUri(projectId, userId), req.user.id,
                    ProjectStatus["removeFile"], {
                    fileKey
                })
                return res.send({
                    status: 0,
                    message: "Remove file Successful.",
                    content: fileKey,
                })
            } catch (error) {
                let resJSON = {
                    status: "failed",
                    message: `Can't remove the file : ${fileKey}`,
                };
                return res.send(resJSON)
            }
        });
});

router.post('/renameFile', auth.protected, formDataParse, function (req, res, next) {
    let _id = req.body._id;
    let oldFileKey = req.body.oldFileKey;
    let newFileKey = req.body.newFileKey;
    /** @type Array */
    let oldSplits = _.filter(oldFileKey.split(fileSeparator), (c) => { return c !== "" });
    let oldDirs = oldSplits.slice(0, oldSplits.length - 1);
    /** @type Array */
    let newSplits = _.filter(newFileKey.split(fileSeparator), (c) => { return c !== "" });
    let newDirs = newSplits.slice(0, newSplits.length - 1);
    let shareProjectId = req.body.shareProjectId;
    let userId = shareProjectId ? req.body.userId : req.user.id;
    let projectId = req.body.projectId;
    let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [userId];
    let oldFileName = oldSplits[oldSplits.length - 1];
    let newFileName = newSplits[newSplits.length - 1];
    let oldFilePath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, ...oldSplits);
    let newFilePath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, ...newSplits);
    let oldFileVerPath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName,
        ...oldDirs, `${oldFileName}${VER_FILE_TAG}`);
    let newFileVerPath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName,
        ...newDirs, `${newFileName}${VER_FILE_TAG}`);
    let move = newFileName === oldFileName;
    if (!move && !newFileName.match(fileNameReg)) {
        let resJSON = {
            status: "failed",
            message: `Not valid file name!`,
        };
        return res.send(resJSON)
    }
    async.waterfall([
        callback => {
            checkProject(req, projectId, userId, shareProjectId, (err, project) => {
                if (err) {
                    return callback(err);
                }
                callback(null, project);
            }, undefined, oldFileKey);
        }, (project, callback) => {
            if (!_id) {
                return callback(null);
            }
            GroveShareProjects.deleteShareProjectById(_id, function (err, result) {
                callback(err, result);
            })
        }
    ], (err, result) => {
        if (err) {
            console.error(err);
            let resJSON = {
                status: "failed",
                message: err.message,
            };
            return res.send(resJSON)
        }
        try {
            fs.renameSync(oldFilePath, newFilePath);
            if (isFileEditorJs({ type: getFileType(oldFileKey) })) {
                fs.renameSync(oldFileVerPath, newFileVerPath);
            }
            let stat = fs.statSync(newFilePath);
            let type = getFileType(newFileKey);
            let settings = getAndLoadSettings(type, newFileVerPath);
            let newFileData = Object.assign({}, DefaultFileData,
                {
                    fileKey: newFileKey,
                    name: newFileName,
                    size: stat.size,
                    mtimeMs: stat.mtimeMs,
                    type: type,
                    version: settings.version,
                    final: settings.final,
                    versionMtimeMs: settings.versionMtimeMs || DefaultFileSettings.versionMtimeMs,
                    previewImageUpload: settings.previewImageUpload || DefaultFileSettings.previewImageUpload,
                    messages: settings.messages,
                    shareData: null
                });
            sendProjectStatus(getUploadUri(projectId, userId), req.user.id,
                ProjectStatus["renameFile"], {
                oldFileKey,
                newFileData
            })
            return res.send({
                status: 0,
                message: _.isEqual(oldDirs, newDirs) ? "Rename file Successful." : "Move file Successful.",
                content: newFileData
            })
        } catch (error) {
            let resJSON = {
                status: "failed",
                message: `Can't rename file : ${oldFileKey}`,
            };
            return res.send(resJSON)
        }
    })
});

router.post('/renameFolder', auth.protected, formDataParse, function (req, res, next) {
    let oldFolderKey = req.body.oldFolderKey;
    let newFolderKey = req.body.newFolderKey;
    let shareProjectId = req.body.shareProjectId;
    let userId = shareProjectId ? req.body.userId : req.user.id;
    let projectId = req.body.projectId;
    let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [userId];
    /** @type Array */
    let oldDirs = _.filter(oldFolderKey.split(fileSeparator), (c) => { return c !== "" });
    /** @type Array */
    let newDirs = _.filter(newFolderKey.split(fileSeparator), (c) => { return c !== "" });
    let errorDirs = _.filter(newDirs, (folderName) => {
        return !folderName.match(folderNameReg)
    })
    if (errorDirs.length) {
        let resJSON = {
            status: "failed",
            message: `Not valid folder name!` + errorDirs.join(" "),
        };
        return res.send(resJSON);
    }
    let oldFolderPath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, ...oldDirs);
    let newFolderPath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, ...newDirs);
    if (!fs.existsSync(oldFolderPath)) {
        let resJSON = {
            status: "failed",
            message: `Not exist folder ${oldFolderPath} ! `,
        };
        return res.send(resJSON);
    }
    checkProject(req, projectId, userId, shareProjectId, (err, project) => {
        if (err) {
            let resJSON = {
                status: FAIL_STATUS,
                message: err.message,
            };
            return res.send(resJSON)
        }
        try {
            fs.renameSync(oldFolderPath, newFolderPath);
            let rootPath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName);
            let walkPath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, ...newDirs.slice(0, newDirs.length - 1));
            handleWalk(walkPath, rootPath, [], function (error, ret) {
                if (error) {
                    let resJSON = {
                        status: "failed",
                        message: `Can't load the path : ${walkPath}`,
                    };
                    return res.send(resJSON)
                } else {
                    sendProjectStatus(getUploadUri(projectId, userId), req.user.id,
                        ProjectStatus["renameFolder"], {
                        oldFolderKey,
                        newFolderKey,
                        ret
                    })
                    return res.send({
                        status: 0,
                        message: "Rename Folder Successful.",
                        content: ret
                    })
                }
            });
        } catch (error) {
            let resJSON = {
                status: "failed",
                message: `Can't rename folder : ${oldFolderKey}`,
            };
            return res.send(resJSON)
        }
    });
});
router.post('/copyFolder', auth.protected, formDataParse, function (req, res, next) {
    let fromFolderKey = req.body.fromFolderKey;
    let fromShareProjectId = req.body.fromShareProjectId;
    let fromUserId = fromShareProjectId ? req.body.fromUserId : req.user.id;
    let fromProjectId = req.body.fromProjectId;
    let fromRootDirName = fromProjectId !== COMMON ? [`projects`, fromProjectId] : [fromUserId];
    let toFolderKey = req.body.toFolderKey;
    let toShareProjectId = req.body.toShareProjectId;
    let toUserId = toShareProjectId ? req.body.toUserId : req.user.id;
    let toProjectId = req.body.toProjectId;
    let toRootDirName = toProjectId !== COMMON ? [`projects`, toProjectId] : [toUserId];
    /** @type Array */
    let fromDirs = _.filter(fromFolderKey.split(fileSeparator), (c) => { return c !== "" });
    /** @type Array */
    let toDirs = _.filter(toFolderKey.split(fileSeparator), (c) => { return c !== "" });
    let errorDirs = _.filter(toDirs, (folderName) => {
        return !folderName.match(folderNameReg)
    })
    if (errorDirs.length) {
        let resJSON = {
            status: "failed",
            message: `Not valid folder name!` + errorDirs.join(" "),
        };
        return res.send(resJSON);
    }
    let fromFolderPath = path.join(TmpDir, `${BOOK_ID}`, ...fromRootDirName, ...fromDirs);
    let toFolderPath = path.join(TmpDir, `${BOOK_ID}`, ...toRootDirName, ...toDirs);
    if (!fs.existsSync(fromFolderPath)) {
        let resJSON = {
            status: "failed",
            message: `Not exist folder ${fromFolderPath} ! `,
        };
        return res.send(resJSON);
    }
    async.waterfall([
        callback => {
            checkProject(req, fromProjectId, fromUserId, fromShareProjectId, (err, project) => {
                if (err) {
                    return callback(err);
                }
                callback(null, project);
            });
        }, (fromProject, callback) => {
            checkProject(req, toProjectId, toUserId, toShareProjectId, (err, project) => {
                if (err) {
                    return callback(err);
                }
                callback(null, project);
            });
        }, (toProject, callback) => {
            try {
                if (!fs.existsSync(toFolderPath)) {
                    fs.mkdirSync(toFolderPath, { recursive: true });
                }
                fsextra.copySync(fromFolderPath, toFolderPath);
                let rootPath = path.join(TmpDir, `${BOOK_ID}`, ...toRootDirName);
                let walkPath = path.join(TmpDir, `${BOOK_ID}`, ...toRootDirName, ...toDirs.slice(0, toDirs.length - 1));
                handleWalk(walkPath, rootPath, [], function (error, ret) {
                    if (error) {
                        return callback(error);
                    } else {
                        sendProjectStatus(getUploadUri(toProjectId, toUserId), req.user.id,
                            ProjectStatus["copyFolder"], {
                            ret
                        })
                        return res.send({
                            status: 0,
                            message: "Copy Folder Successful.",
                            content: ret
                        })
                    }
                });
            } catch (error) {
                return callback(error);
            }
        }], (err, result) => {
            if (err) {
                let resJSON = {
                    status: FAIL_STATUS,
                    message: err.message,
                };
                return res.send(resJSON)
            }
        });
});

router.post('/removeFolder', auth.protected, formDataParse, function (req, res, next) {
    let ret = {};
    let _ids = req.body._ids;
    let removeFolderKey = req.body.folderKey;
    let shareProjectId = req.body.shareProjectId;
    let userId = shareProjectId ? req.body.userId : req.user.id;
    let projectId = req.body.projectId;
    let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [userId];
    let walkPath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, removeFolderKey);
    async.waterfall([
        callback => {
            checkProject(req, projectId, userId, shareProjectId, (err, project) => {
                if (err) {
                    return callback(err);
                }
                callback(null, project);
            });
        }, (project, callback) => {
            if (!_ids.length) {
                return callback(null);
            }
            GroveShareProjects.deleteShareProjectByIds(_ids, function (err, result) {
                callback(err, result);
            })
        }], (err, result) => {
            if (err) {
                console.error(err);
                let resJSON = {
                    status: FAIL_STATUS,
                    message: err.message,
                };
                return res.send(resJSON)
            }
            try {
                if (fs.existsSync(walkPath) && fs.rmdirSync) {
                    fs.rmdirSync(walkPath, { recursive: true });
                }
                sendProjectStatus(getUploadUri(projectId, userId), req.user.id,
                    ProjectStatus["removeFolder"], {
                    removeFolderKey
                })
                return res.send({
                    "status": 0,
                    "message": "Remove folder Successful.",
                })
            } catch (error) {
                let resJSON = {
                    status: FAIL_STATUS,
                    message: `Can't remove folder : ${removeFolderKey}`,
                };
                return res.send(resJSON)
            }
        });
});
router.post('/tagVersion', auth.protected, formDataParse, function (req, res, next) {
    let fileKey = req.body.fileKey;
    let fileName = getFileName(fileKey);
    let type = getFileType(fileKey);
    let file = { type: type };
    let userId = req.user.id;
    let projectId = req.body.projectId;
    let projectUser = req.body.projectUser;
    let message = req.body.message;
    let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [userId];
    let filePath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, fileKey);
    if (message.length > 100) {
        let resJSON = {
            status: "failed",
            message: `Message too large : ${fileKey}`,
        };
        return res.send(resJSON)
    }
    if (!isFileEditorJs(file)) {
        let resJSON = {
            status: "failed",
            message: `Can only tag version to grove file : ${fileKey}`,
        };
        return res.send(resJSON)
    }
    if (projectUser !== userId) {
        let resJSON = {
            status: "failed",
            message: `Can't tag version to others project : ${fileKey}`,
        };
        return res.send(resJSON)
    }
    if (!fs.existsSync(filePath)) {
        let resJSON = {
            status: "failed",
            message: `Can't tag version to nonexist project : ${fileKey}`,
        };
        return res.send(resJSON)
    }
    let stat = fs.statSync(filePath);
    let verDir = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, `${fileKey}${VER_FILE_TAG}`);
    let settingsFile = path.join(verDir, "settings.json");
    let settings = getAndLoadSettings(type, verDir);
    if (settings.versionMtimeMs === stat.mtimeMs) {
        let resJSON = {
            status: "failed",
            message: `Can't tag version to no change project : ${fileKey}`,
        };
        return res.send(resJSON)
    }
    settings.version += 1;
    if (settings.final === 0) {
        settings.final = settings.version;
    }
    settings.versionMtimeMs = stat.mtimeMs;
    settings.messages[settings.version] = message;
    fs.copyFileSync(filePath, path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, `${fileKey}${VER_FILE_TAG}`, `${settings.version}`));
    fs.writeFileSync(settingsFile, JSON.stringify(settings, undefined, 2));
    return res.send({
        status: 0,
        message: "Tag version to project success!",
        content: Object.assign({}, DefaultFileData,
            {
                fileKey: fileKey,
                name: fileName,
                size: stat.size,
                mtimeMs: stat.mtimeMs,
                type: type,
                version: settings.version,
                final: settings.final,
                versionMtimeMs: settings.versionMtimeMs || DefaultFileSettings.versionMtimeMs,
                previewImageUpload: settings.previewImageUpload || DefaultFileSettings.previewImageUpload,
                messages: settings.messages,
                shareData: undefined//Do not need transfer to client , so undefined
            })
    });
});
router.post('/publish', auth.protected, formDataParse, function (req, res, next) {
    let fileKey = req.body.fileKey;
    let fileName = getFileName(fileKey);
    let type = getFileType(fileKey);
    let file = { type: type };
    let projectId = req.body.projectId;
    let shareProjectId = req.body.shareProjectId;
    let userId = shareProjectId ? req.body.userId : req.user.id;
    let projectUser = req.body.projectUser;
    let message = req.body.message;
    let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [userId];
    let filePath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, fileKey);
    if (!isFileEditorJs(file)) {
        let resJSON = {
            status: "failed",
            message: `Can only publish grove file : ${fileKey}`,
        };
        return res.send(resJSON)
    }
    if (projectUser !== userId) {
        let resJSON = {
            status: "failed",
            message: `Can't publish others project : ${fileKey}`,
        };
        return res.send(resJSON)
    }
    if (!fs.existsSync(filePath)) {
        let resJSON = {
            status: "failed",
            message: `Can't publish nonexist project : ${fileKey}`,
        };
        return res.send(resJSON)
    }
    let stat = fs.statSync(filePath);
    let verDir = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, `${fileKey}${VER_FILE_TAG}`);
    let settingsFile = path.join(verDir, "settings.json");
    let settings = getAndLoadSettings(type, verDir);
    settings.version += 1;
    if (settings.final === 0) {
        settings.final = settings.version;
    }
    settings.versionMtimeMs = stat.mtimeMs;
    settings.messages[settings.version] = message;
    settings.previewImageUpload = DefaultFileSettings.previewImageUpload;
    fs.copyFileSync(filePath, path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, `${fileKey}${VER_FILE_TAG}`, `${settings.version}`));
    fs.writeFileSync(settingsFile, JSON.stringify(settings, undefined, 2));
    let addInfo = {
        userId: userId,
        projectId: projectId,
        projectName: req.body.projectName,
        fileKey: fileKey,
        version: settings.version,
        status: req.body.status,
        title: req.body.title,
        fileName: getFileName(fileKey),
    }
    async.waterfall([
        callback => {
            if (projectId === COMMON) {
                return callback(null, null);
            }
            if (~EXPECT_PROJECT_IDS.indexOf(projectId) && req.user.get("email") === adminEmail) {
                return callback(null, null);
            }
            CustomDB.getInfo(projectId,
                (err, project) => {
                    err = err || (!project ? new Error('Can not found the project') : null);
                    callback(err, project)
                })
        },
        (project, callback) => {
            if (project) {
                project = JSON.parse(JSON.stringify(project));
                if (project.user !== userId) {
                    return callback(new Error("You only can publish your project"))
                }
            }
            GroveShareProjects.addShareProject(addInfo, function (err, result) {
                callback(err, result);
            })
        }], (err, result) => {
            if (err) {
                let resJSON = {
                    status: "failed",
                    message: `Can't publish the project : ${filePath}`,
                };
                return res.send(resJSON)
            }
            return res.send({
                status: 0,
                message: "Add publish project success!",
                content: Object.assign({}, DefaultFileData,
                    {
                        fileKey: fileKey,
                        name: fileName,
                        size: stat.size,
                        mtimeMs: stat.mtimeMs,
                        type: type,
                        version: settings.version,
                        final: settings.final,
                        versionMtimeMs: settings.versionMtimeMs || DefaultFileSettings.versionMtimeMs,
                        previewImageUpload: settings.previewImageUpload || DefaultFileSettings.previewImageUpload,
                        messages: settings.messages,
                        shareData: result
                    })
            });
        });
});

router.post('/republish', auth.protected, formDataParse, function (req, res, next) {
    let _id = req.body._id;
    let fileKey = req.body.fileKey;
    let fileName = getFileName(fileKey);
    let userId = req.user.id;
    let projectId = req.body.projectId;
    let projectUser = req.body.projectUser;
    if (projectUser !== userId) {
        let resJSON = {
            status: "failed",
            message: `Can't republish others project : ${fileKey}`,
        };
        return res.send(resJSON)
    }
    let message = req.body.message;
    let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [userId];
    let filePath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, fileKey);
    if (!fs.existsSync(filePath)) {
        let resJSON = {
            status: "failed",
            message: `Can't republish nonexist project : ${fileKey}`,
        };
        return res.send(resJSON)
    }
    let stat = fs.statSync(filePath);
    let type = getFileType(fileKey);
    let verDir = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, `${fileKey}${VER_FILE_TAG}`);
    let settingsFile = path.join(verDir, "settings.json");
    let settings = getAndLoadSettings(type, verDir);
    settings.version += 1;
    if (settings.final === 0) {
        settings.final = settings.version;
    }
    settings.versionMtimeMs = stat.mtimeMs;
    settings.messages[settings.version] = message;
    fs.copyFileSync(filePath, path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, `${fileKey}${VER_FILE_TAG}`, `${settings.version}`));
    fs.writeFileSync(settingsFile, JSON.stringify(settings, undefined, 2));
    let updateInfo = {
        userId: userId,
        projectId: projectId,
        projectName: req.body.projectName,
        fileKey: fileKey,
        version: settings.version,
        status: req.body.status,
        title: req.body.title,
        fileName: getFileName(fileKey),
    }
    async.waterfall([
        callback => {
            GroveShareProjects.getShareProject(_id, function (err, project) {
                if (err) {
                    return callback(err)
                }
                callback(null, project);
            });
        },
        (project, callback) => {
            project = JSON.parse(JSON.stringify(project));
            if (project.userId !== userId) {
                return callback(new Error("You only can republish you own project"))
            }
            GroveShareProjects.updateShareProjectById(_id, updateInfo, function (err, result) {
                callback(err, result);
            })
        }], (err, result) => {
            if (err) {
                let resJSON = {
                    status: "failed",
                    message: `Can't republish the project : ${filePath}`,
                };
                return res.send(resJSON)
            }
            return res.send({
                status: 0,
                message: "Republish project success!",
                content: Object.assign({}, DefaultFileData,
                    {
                        fileKey: fileKey,
                        name: fileName,
                        size: stat.size,
                        mtimeMs: stat.mtimeMs,
                        type: type,
                        version: settings.version,
                        final: settings.final,
                        versionMtimeMs: settings.versionMtimeMs || DefaultFileSettings.versionMtimeMs,
                        previewImageUpload: settings.previewImageUpload || DefaultFileSettings.previewImageUpload,
                        messages: settings.messages,
                        shareData: result
                    })
            });
        });
});

router.post('/fork', auth.protected, formDataParse, function (req, res, next) {
    let fileDataBuffer = req.body.content;
    let fileKeys = req.body.fileKeys;
    let mainFileKey = req.body.mainFileKey;
    let renameMainFileKey = req.body.renameMainFileKey;
    let fromUserId = req.body.userId;
    let projectId = req.body.projectId;
    let toUserId = req.body.toUserId;
    let toShareProjectId = req.body.toShareProjectId;
    let toProjectId = req.body.toProjectId;
    let toFolderKey = req.body.toFolderKey || "";
    let fromRootDirName = projectId !== COMMON ? [`projects`, projectId] : [fromUserId];
    let toRootDirName = toProjectId !== COMMON ? [`projects`, toProjectId] : [toUserId];
    let fromRootDir = path.join(TmpDir, `${BOOK_ID}`, ...fromRootDirName);
    let toRootDir = path.join(TmpDir, `${BOOK_ID}`, ...toRootDirName);
    async.waterfall([
        (callback) => {
            checkProject(req, toProjectId, toUserId, toShareProjectId, (err, project) => {
                if (err) {
                    return callback(err);
                }
                callback(null, project);
            });
        }, (toProject, callback) => {
            let files = {};
            for (let index = 0; index < fileKeys.length; index++) {
                let fileKey = fileKeys[index];
                let newFileKey;
                if (mainFileKey === fileKey && renameMainFileKey) {
                    newFileKey = `${toFolderKey}${renameMainFileKey}`;
                } else {
                    newFileKey = `${toFolderKey}${fileKey}`;
                }
                if (!fs.existsSync(path.join(fromRootDir, fileKey))) {
                    continue;
                }
                let fileName = getFileName(newFileKey);
                let dir = (newFileKey && ~newFileKey.lastIndexOf(fileSeparator)) ?
                    newFileKey.substring(0, newFileKey.lastIndexOf(fileSeparator)) : "";
                let type = getFileType(newFileKey, fileName);
                let verDir = path.join(toRootDir, dir, `${fileName}${VER_FILE_TAG}`);
                let settings = getAndLoadSettings(type, verDir);
                let toDir = ~newFileKey.lastIndexOf(fileSeparator) ? path.join(toRootDir,
                    newFileKey.substring(0, newFileKey.lastIndexOf(fileSeparator))) : toRootDir;
                if (!fs.existsSync(toDir)) {
                    fs.mkdirSync(toDir, { recursive: true });
                }
                let filePath = path.join(toRootDir, newFileKey);
                if (mainFileKey === fileKey) {
                    fs.writeFileSync(filePath, fileDataBuffer, 'utf8');
                } else {
                    fs.copyFileSync(path.join(fromRootDir, fileKey), filePath);
                }
                let stat = fs.statSync(filePath);
                files[newFileKey] = Object.assign({}, DefaultFileData,
                    {
                        fileKey: newFileKey,
                        name: fileName,
                        type: type,
                        size: stat.size,
                        mtimeMs: stat.mtimeMs,
                        version: settings.version,
                        final: settings.final,
                        versionMtimeMs: settings.versionMtimeMs || DefaultFileSettings.versionMtimeMs,
                        previewImageUpload: settings.previewImageUpload || DefaultFileSettings.previewImageUpload,
                        messages: settings.messages,
                        shareData: undefined//Do not need transfer to client , so undefined
                    })
            }
            callback(null, files);
        }], (err, result) => {
            if (err) {
                let resJSON = {
                    status: FAIL_STATUS,
                    message: err.message,
                };
                return res.send(resJSON)
            }
            sendProjectStatus(getUploadUri(toProjectId, toUserId), req.user.id,
                ProjectStatus["copyFolder"], {
                ret: result
            })
            res.send({
                status: 0,
                content: result,
                message: "Fork success!",
            });
        });
});

router.post('/getAllShareProjects', formDataParse, function (req, res) {
    let projectId = req.body.projectId;
    let userId = req.body.userId;
    let condition = req.body.condition;//new version add condition
    let option = {};
    if ((typeof projectId) === "string" && projectId.trim()) {
        option.projectId = projectId;
    }
    if ((typeof userId) === "string" && userId.trim()) {
        option.userId = userId;
    }
    if (!condition) {
        condition = { option: option };
    }
    GroveShareProjects.getAllShareProjects(condition, function (err, projects) {
        if (err) {
            let resJSON = {
                status: "failed",
                message: `Can't get published projects : ${projectId}`,
            };
            return res.send(resJSON)
        }
        projects = JSON.parse(JSON.stringify(projects));
        _.each(projects, (project) => {
            const { userId: { _id }, projectId } = project;
            let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [_id];
            project.uploadUri = `/tmp/${BOOK_ID}/${rootDirName.join("/")}/`;
        })
        return res.send({
            status: 0,
            message: "Get published projects success!",
            content: projects
        });
    });
});

router.post('/unpublish', auth.protected, formDataParse, function (req, res) {
    let _id = req.body._id
    let userId = req.user.id;
    async.waterfall([
        callback => {
            GroveShareProjects.getShareProject(_id, function (err, project) {
                if (err) {
                    return callback(err)
                }
                callback(null, project);
            });
        },
        (project, callback) => {
            project = JSON.parse(JSON.stringify(project));
            if (project.userId !== userId) {
                return callback(new Error("You only can unpublish you own project"))
            }
            GroveShareProjects.deleteShareProjectById(_id, function (err, result) {
                callback(err, result);
            })
        }], (err, result) => {
            if (err) {
                console.error(err);
                let resJSON = {
                    status: "failed",
                    message: `Can't remove pubished project : ${_id}`,
                };
                return res.send(resJSON)
            }
            return res.send({
                status: 0,
                message: "unpublish project success"
            });
        });
});
router.post('/setSharedStatus', auth.protected, formDataParse, function (req, res) {
    let _id = req.body._id;
    let status = req.body.status;
    GroveShared.updateSharedById(_id, { status }, function (err, result) {
        if (err) {
            let resJSON = {
                status: "failed",
                message: `Can't set shared user status`,
            };
            return res.send(resJSON)
        }
        return res.send({
            status: 0,
            message: "Set shared user status success!",
            content: result
        });
    })
});
router.post('/setShareProjectStatus', auth.protected, formDataParse, function (req, res) {
    let _id = req.body._id;
    let status = req.body.status;
    GroveShareProjects.updateShareProjectById(_id, { status }, function (err, result) {
        if (err) {
            let resJSON = {
                status: "failed",
                message: `Can't set share project status`,
            };
            return res.send(resJSON)
        }
        return res.send({
            status: 0,
            message: "Set share project status success!",
            content: result
        });
    })
});
router.post('/addShared', auth.protected, formDataParse, function (req, res) {
    let toUser = req.body.toUser;
    let toUserId = toUser._id;
    let fileKey = req.body.fileKey;
    let fileName = getFileName(fileKey);
    let type = getFileType(fileKey);
    let file = { type: type };
    let projectId = req.body.projectId;
    let shareProjectId = req.body.shareProjectId;
    let projectName = req.body.projectName;
    let projectUser = req.body.projectUser;
    let status = req.body.status;
    let userId = shareProjectId ? req.body.userId : req.user.id;
    let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [userId];
    let filePath = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, fileKey);
    if (!isFileEditorJs(file)) {
        let resJSON = {
            status: "failed",
            message: `Can only share grove file : ${fileKey}`,
        };
        return res.send(resJSON)
    }
    if (projectUser !== userId) {
        let resJSON = {
            status: "failed",
            message: `Can't share others project : ${fileKey}`,
        };
        return res.send(resJSON)
    }
    if (!fs.existsSync(filePath)) {
        let resJSON = {
            status: "failed",
            message: `Can't share nonexist project : ${fileKey}`,
        };
        return res.send(resJSON)
    }
    let stat = fs.statSync(filePath);
    let verDir = path.join(TmpDir, `${BOOK_ID}`, ...rootDirName, `${fileKey}${VER_FILE_TAG}`);
    let settings = getAndLoadSettings(type, verDir);
    let addInfo = {
        userId: userId,
        toUserId: toUserId,
        projectId: projectId,
        projectName: projectName,
        fileKey: fileKey,
        version: DEFAULT_VER,
        status: status
    }
    async.waterfall([
        callback => {
            checkProject(req, projectId, userId, shareProjectId, (err, project) => {
                if (err) {
                    return callback(err);
                }
                callback(null, project);
            });
        }, (project, callback) => {
            GroveShared.addShared(addInfo, function (err, result) {
                callback(err, result);
            })
        }, (result, callback) => {
            GroveShared.getAllShareds({ _id: result._id }, function (err, projects) {
                if (err) {
                    return callback(err);
                }
                projects = JSON.parse(JSON.stringify(projects));
                _.each(projects, (project) => {
                    const { userId: { _id }, projectId } = project;
                    let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [_id];
                    project.uploadUri = `/tmp/${BOOK_ID}/${rootDirName.join("/")}/`;
                })
                callback(err, projects[0]);
            })
        }], (err, result) => {
            if (err) {
                let resJSON = {
                    status: "failed",
                    message: `Can't share the project : ${filePath}`,
                };
                return res.send(resJSON)
            }
            sendProjectStatus(getUploadUri(projectId, userId), req.user.id,
                ProjectStatus["addShared"], {
                ret: result
            })
            return res.send({
                status: 0,
                message: "Add share project success!",
                content: result
            });
        });
});
router.post('/removeShared', auth.protected, formDataParse, function (req, res) {
    let _id = req.body._id
    let projectId = req.body.projectId;
    let shareProjectId = req.body.shareProjectId;
    let userId = shareProjectId ? req.body.userId : req.user.id;
    async.waterfall([
        callback => {
            checkProject(req, projectId, userId, shareProjectId, (err, project) => {
                if (err) {
                    return callback(err);
                }
                callback(null, project);
            });
        }, (project, callback) => {
            GroveShared.getShared(_id, function (err, project) {
                if (err) {
                    return callback(err)
                } else if (!project) {
                    return callback(new Error("can not find shared project"))
                }
                callback(null, project);
            });
        },
        (project, callback) => {
            project = JSON.parse(JSON.stringify(project));
            if (project.userId !== userId) {
                return callback(new Error("You only can remove you own share project"))
            }
            GroveShared.deleteSharedById(_id, function (err, result) {
                callback(err, project);
            })
        }], (err, shared) => {
            if (err) {
                console.error(err);
                let resJSON = {
                    status: "failed",
                    message: `Can't remove shared project : ${_id}`,
                };
                return res.send(resJSON)
            }
            sendProjectStatus(getUploadUri(projectId, userId), req.user.id,
                ProjectStatus["removeShared"], {
                shared: shared
            })
            return res.send({
                status: 0,
                message: "remove shared project success"
            });
        });
});
router.post('/getAllShareds', formDataParse, function (req, res) {
    let projectId = req.body.projectId;
    let userId = req.body.userId;
    let toUserId = req.body.toUserId;
    let fileKey = req.body.fileKey;
    let type = parseInt(req.body.type);
    let condition;
    switch (type) {
        case SHAREDS_TYPE.Shared_To_User:
            condition = { toUserId };
            break;
        case SHAREDS_TYPE.User_Shared_By_FileKey:
            condition = { userId, fileKey, projectId };
            break;
        case SHAREDS_TYPE.User_Shared_By_ProjectId:
            condition = { projectId, userId };
            break;
        default:
            return res.send({
                status: FAIL_STATUS,
                message: `Can't get right type`,
            })
    }
    GroveShared.getAllShareds(condition, function (err, projects) {
        if (err) {
            let resJSON = {
                status: "failed",
                message: `Can't get shared users : ${projectId}`,
            };
            return res.send(resJSON)
        }
        projects = JSON.parse(JSON.stringify(projects));
        _.each(projects, (project) => {
            const { userId: { _id }, projectId } = project;
            let rootDirName = projectId !== COMMON ? [`projects`, projectId] : [_id];
            project.uploadUri = `/tmp/${BOOK_ID}/${rootDirName.join("/")}/`;
        })
        return res.send({
            status: 0,
            message: "Get shared users success!",
            content: projects
        });
    });
});
router.post('/getProjectShareUsers', formDataParse, function (req, res) {
    let projectId = req.body.projectId;
    UserShareProjects.getAllUserDetailByProject(projectId, function (err, users) {
        if (err) {
            console.error(err);
        }
        return res.send({ status: err ? FAIL_STATUS : SUCCESS_STATUS, message: err ? err.message : "success", content: users });
    });
});
module.exports = { groveRouter: router, duplicateProjectFolder };