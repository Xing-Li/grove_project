import editor from "../../../commonEditor";
import { FILESDIR } from "../../../util/localstorage";

export function parseSimpleImageToMarkdown(blocks) {
  let src = editor.getFileAttachmentUrl(blocks.url, editor.getUploadUri());
  src = src.startsWith(editor.getUploadUri()) ? `${FILESDIR}${blocks.url}` : src;
  return `![${blocks.caption}](${src} "${blocks.caption}")`.concat('\n');
}