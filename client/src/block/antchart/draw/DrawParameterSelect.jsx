import { Select } from 'antd';
import _ from 'lodash';
import React from "react";
import databaseClient from '../../../stdlib/databaseClient';
import { SelectionTypes } from '../../../util/databaseApi';
import { CategoryFromType, showToast } from '../../../util/utils';
import { DefaultAntChartConfig } from '../comp/OptionsTypes';
import { extractColumnDatas } from '../comp/TableComp';

export default class DrawParameterSelect extends React.Component {
    constructor(props) {
        super(props);
        const { declareEle, ele, neo, selectedCategoryFrom, selectedCategory, updateConfig, editor } = this.props;
        this.state = {
            /**@type {DefaultAntChartConfig["neo"]} */
            neo,
            selectedCategoryFrom,
            selectedCategory, updateConfig
        };
        this.domRef = React.createRef();
    }

    componentDidUpdate(prevProps, prevState) {
        const { declareEle, ele, neo, selectedCategory, updateConfig } = this.props;
        let state = {};
        if (prevProps.declareEle !== declareEle || prevProps.ele !== ele) {
            this.draw();
        }
        if (prevProps.neo !== neo) {
            _.assign(state, { neo });
        }
        if (prevProps.selectedCategory !== selectedCategory) {
            _.assign(state, { selectedCategory });
        }
        if (prevProps.updateConfig !== updateConfig) {
            _.assign(state, { updateConfig });
        }
        !_.isEmpty(state) && this.setState(state);
    }

    draw() {
        const { declareEle, ele } = this.props;
        /**@type {HTMLElement} */
        const container = this.domRef.current;
        if (container && ele) {
            container.innerHTML = "";
            declareEle && container.appendChild(declareEle);
            container.appendChild(ele)
        }
    }

    componentDidMount() {
        this.draw();
    }

    setEditorLoading = (editorLoading = false) => {
        this.props.setEditorLoading(editorLoading)
    }

    queryPropertyValues = async (selectedCategoryFrom, selectedCategory, selectionType, nodeLabel, propertyName, value) => {
        const { editor } = this.props;
        let client = selectedCategoryFrom === CategoryFromType.client ? window.DbClients[editor.getSelfModuleName()][selectedCategory] : databaseClient(selectedCategory)
        if (!client) {
            return;
        }
        let query = SelectionTypes[selectionType].queryValue(nodeLabel, propertyName);
        let prameters = { input: value };
        this.setEditorLoading(true);
        let result = await client.query(query, prameters);
        if (result && !result.error && result.records) {
            this.setState({
                propertyValues: _.reduce(extractColumnDatas(false, result.records, false).rows, (prev, obj, index) => {
                    prev.push(_.values(obj)[0]);
                    return prev;
                }, []),
            }, this.setEditorLoading)
        } else {
            showToast("query result error!", "error");
            this.setState({ propertyValues: [], },
                this.setEditorLoading)
        }
    };

    render() {
        const { selectedCategoryFrom, selectedCategory, neo, neo: { selectionType, nodeLabel, propertyName, propertyValue }, propertyValues, updateConfig } = this.state;
        return <div>
            {selectionType && nodeLabel && propertyName &&
                <div className="d-flex flex-wrap align-items-center div-gutter">
                    <span>{nodeLabel} {propertyName}:</span>
                    <Select
                        showSearch
                        placeholder="Start typing..."
                        value={propertyValue}
                        style={{ width: 120 }}
                        defaultActiveFirstOption={false}
                        showArrow={false}
                        filterOption={false}
                        onSearch={(newValue) => {
                            if (newValue) {
                                this.queryPropertyValues(selectedCategoryFrom, selectedCategory, selectionType, nodeLabel, propertyName, newValue);
                            } else {
                                this.setState({ propertyValues: [] });
                            }
                        }}
                        onChange={(value) => {
                            updateConfig(_.assign({}, neo, { propertyValue: value }));
                        }}
                        notFoundContent={null}
                        options={_.map(propertyValues, (d, index) => ({
                            value: d,
                            label: d,
                        }))}
                    />
                </div>}
            <div className="d-flex justity-content-start" ref={this.domRef}></div>
        </div>
    }
}