import React, { Component } from "react";
import classNames from "classnames";
import * as PropTypes from "prop-types";
import Codemirror, { showHint } from "codemirror";
import { createCypherEditor } from 'cypher-codemirror';
import { Dropdown, Menu } from "antd";
import { ChevronDownIcon } from "block/code/Icon";
import actions from "define/Actions";
const { code_editor_keys, cell_shortcuts_keys, linkKeys } = require('../../../util/utils');
const { assembling } = require('../../../util/helpers');

export class CypherEditor extends Component {
  constructor(props) {
    super(props);
    const { run } = this.props;
    this.state = {
      isFocused: false,
      items: props.items || [
        {
          value: `MATCH (n)-[r]->(m) RETURN *`,
          label: "It's the entire graph!",
        },
        {
          value: `MATCH (p:Person)--(m:Movie)
  RETURN p.name as Person, COUNT(m) as Movies 
  ORDER BY Movies DESC`,
          label: 'Template',
        },
        {
          value: `MATCH (n)-[r]-(m) RETURN * LIMIT 100`,
          label: 'MATCH (n)-[r]-(m) RETURN * LIMIT 100',
        },
      ]
    };
    this.options = {
      lineNumbers: true,
      mode: "cypher",
      theme: window.editor.getSettings().getTheme(),
      placeholder: "Type Cypher, then Shift-Enter.",
      gutters: ["cypher-hints"],
      lineWrapping: true,
      readOnly: props.readOnly,
      autofocus: true,
      smartIndent: false,
      lineNumberFormatter: this.lineNumberFormatter,
      lint: true,
      extraKeys:
        assembling([{
          key: linkKeys(code_editor_keys["Auto-complete"]), value: "autocomplete"
        }, {
          key: linkKeys(cell_shortcuts_keys["Force Run Current Cell"]), value: (cm) => {
            run && run();
          }
        }, {
          key: linkKeys(cell_shortcuts_keys["Run Current Cell"]), value: (cm) => {
            run && run();
          }
        }, {
          key: linkKeys(code_editor_keys["Show Editor LineNumbers"]), value: (cm) => {
            cm.setOption("lineNumbers", !cm.getOption("lineNumbers"));
          }
        }, {
          key: linkKeys(code_editor_keys["Fold Code"]), value: function (cm) { cm.foldCode(cm.getCursor()); },
        }]),
      hintOptions: {
        completeSingle: false,
        closeOnUnfocus: false,
        alignWithWord: true,
        async: true
      },
      autoCloseBrackets: {
        explode: ""
      },
      ...(this.props.options || {})
    };
    this.schema = this.props.autoCompleteSchema;
    this.themeElement = document.createElement("div");
  }

  normalizeLineEndings(str) {
    if (!str) return str;
    return str.replace(/\r\n|\r/g, "\n");
  }

  lineNumberFormatter = line => {
    if (!this.codeMirror || this.codeMirror.lineCount() === 1) {
      return "$";
    } else {
      return line;
    }
  };

  getCodeMirrorInstance() {
    return Codemirror;
  }

  componentDidMount() {
    const textareaNode = this.editorReference;
    const { editor, editorSupport } = createCypherEditor(
      textareaNode,
      this.options
    );

    /**@type{Codemirror.Editor} */
    this.codeMirror = editor;
    this.codeMirror.on("keyup", this.codemirrorKeyup);
    this.codeMirror.on("change", this.codemirrorValueChanged);
    this.codeMirror.on("focus", () => this.focusChanged(true));
    this.codeMirror.on("blur", () => this.focusChanged(false));
    this.codeMirror.on("scroll", this.scrollChanged);
    this.codeMirror.setValue(this.props.value);
    this.editorSupport = editorSupport;
    this.editorSupport.setSchema(this.schema);

    if (this.props.initialPosition) {
      this.goToPosition(this.props.initialPosition);
    }
    actions.inspector(this.themeElement, [actions.types.THEME], (theme) => {
      this.setTheme(theme);
    });
  }

  componentWillUnmount() {
    actions.deleteCache(this.themeElement);
  }

  setTheme = (theme) => {
    this.codeMirror && this.codeMirror.setOption("theme", theme);
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      if (this.codeMirror && this.codeMirror.getValue() !== nextProps.value) {
        this.codeMirror.setValue(nextProps.value);
      }
    }
  }

  goToPosition(position) {
    for (let i = 0; i < position.line; i++) {
      this.codeMirror.execCommand("goLineDown");
    }

    for (let i = 0; i <= position.column; i++) {
      this.codeMirror.execCommand("goCharRight");
    }
  }

  getCodeMirror() {
    return this.codeMirror;
  }

  focus() {
    if (this.codeMirror) {
      this.codeMirror.focus();
    }
  }

  focusChanged = focused => {
    let state = {
      isFocused: focused
    }
    this.setState(state);
    this.props.onFocusChange && this.props.onFocusChange(focused);
  };

  scrollChanged = cm => {
    this.props.onScroll && this.props.onScroll(cm.getScrollInfo());
  };

  codemirrorValueChanged = (doc, change) => {
    if (this.props.onValueChange && change.origin !== "setValue") {
      this.props.onValueChange(doc.getValue(), change);
    }
  };

  codemirrorKeyup = (cm, e) => {
    if (!(e.ctrlKey || e.metaKey) && !e.altKey && (/^[a-z$._]$/ig).test(e.key)) {
      showHint(cm, Codemirror.hint.cypher);
    }
  };

  render() {
    const { items } = this.state;
    const editorClassNames = classNames(
      "ReactCodeMirror",
      { "ReactCodeMirror--focused": this.state.isFocused },
      this.props.className
    );
    return (
      <div className="code-edit-wrapper">
        <div className="code-edit-wrapper-icons d-flex align-items-center">
          {<Dropdown overlay={<Menu className="data-html2canvas-ignore import-export-menu normal-icon" onClick={(info) => {
            let item = items[info.key];
            this.codeMirror.setValue(item.value);
            if (this.props.onValueChange) {
              this.props.onValueChange(this.codeMirror.getValue());
            }
          }}>
            {_.map(items, (item, index) => {
              return <Menu.Item key={index}>{item.label}</Menu.Item>
            })}
          </Menu>}
            placement="bottomCenter" trigger={['click']}>
            <ChevronDownIcon className="text-primary" />
          </Dropdown>}
        </div>
        <div
          className={editorClassNames}
          ref={ref => (this.editorReference = ref)}
        />
      </div>
    );
  }
}

CypherEditor.propTypes = {
  /** override default options  */
  options: PropTypes.object,
  autoCompleteSchema: PropTypes.object,
  onValueChange: PropTypes.func,
  /** set intial value */
  value: PropTypes.string
};

CypherEditor.defaultProps = {
  autoCompleteSchema: undefined,
  onValueChange: () => { },
  value: ""
};

export default CypherEditor;