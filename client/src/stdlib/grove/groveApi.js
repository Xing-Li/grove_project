// fs
import { importGxrf } from './fs/importGxrf'
import { save, load } from './fs/fileSystem'

// ui
import './groveApi.scss'
import { AsyncButton } from './ui/AsyncButton'
import { Button } from './ui/Button'
import { ButtonGroup } from './ui/ButtonGroup'
import { Div } from './ui/Div'
import { Icon } from './ui/Icon'
import { showToast } from '../../util/utils'
import { Table } from './ui/Table'
import { Td } from './ui/Td'
import { Th } from './ui/Th'
import { Tr } from './ui/Tr'

// events
import onSelect from './event/onSelect'
import getSelectedNodes from './event/getSelectedNodes'
import getSelectedEdges from './event/getSelectedEdges'
import onGraphDataUpdate from './event/onGraphDataUpdate'

// api
import { getApi } from '../graphxrApi'

// Grove
export default {
  importGxrf,
  saveFile: save,
  loadFile: load,
  AsyncButton,
  Button,
  ButtonGroup,
  Div,
  getApi,
  Icon,
  onSelect,
  getSelectedNodes,
  getSelectedEdges,
  onGraphDataUpdate,
  showToast,
  Table,
  Td,
  Th,
  Tr,
  onLoad: (fn) => {
    console.log("[Grove] Checking if we should setup");
    const key = "GROVE_SETUP";
    if (!window[key]) {
      console.log("[Grove] Running setup");
      window[key] = true;
      fn();
    } else {
      console.log("[Grove] We already setup");
    }
  }
}