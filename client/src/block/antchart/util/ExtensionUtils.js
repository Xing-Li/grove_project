import { REPORT_TYPES } from "../config/ReportConfig";
import { ADVANCED_REPORT_TYPES } from "../config/AdvancedChartsReportConfig";
import { DEFAULT_NODE_LABELS } from "../config/ReportConfig";
import { SELECTION_TYPES } from "../config/CardConfig";
import { RenderType } from "util/databaseApi";
import { extractColumnDatas } from "../comp/TableComp";
import { calcAutoType } from "util/helpers";


// Components can call this to check if any extension is enabled. For example, to decide whether to all rule-based styling.
export const extensionEnabled = (extensions, name) => {
    return extensions && extensions[name];
}

// Tell the application what charts are available, dynmically, based on the selected extensions.
export const getReportTypes = (extensions) => {
    return { ...REPORT_TYPES, ...ADVANCED_REPORT_TYPES };
}

export const getFieldsSelection = (renderType, fields, oldSelection, extensions, records) => {
    const reportTypes = getReportTypes(extensions);
    if (!reportTypes[renderType] || !reportTypes[renderType].selection) {
        return;
    }
    const selectableFields = reportTypes[renderType].selection; // The dictionary of selectable fields as defined in the config.
    const autoAssignSelectedProperties = reportTypes[renderType].autoAssignSelectedProperties;
    const selectables = (selectableFields) ? Object.keys(selectableFields) : [];

    // If the new set of fields is not equal to the current set of fields, we ned to update the field selection.
    if (Object.keys(oldSelection).length !== 0) {
        selectables.forEach((selection, i) => {
            if (fields.includes(oldSelection[selection])) {
                // If the current selection is still present in the new set of fields, no need to reset.
                // Also we ignore this on a node property selector.
                /* continue */
            } else if (selectableFields[selection].optional) {
                // If the fields change, always set optional selections to none.
                if (selectableFields[selection].multiple) {
                    oldSelection[selection] = ["(none)"];
                } else {
                    oldSelection[selection] = "(none)";
                }
            } else {
                if (fields.length > 0) {
                    if (renderType === RenderType.Radar) {
                        if (selection === 'index') {
                            oldSelection[selection] = fields[Math.min(i, fields.length - 1)];
                        } else if (selection === 'values') {
                            const { columns, rows } = extractColumnDatas(false, records);
                            let fieldsType = calcAutoType(rows, false, false);
                            // only update if the old selection no longer covers the new set of fields...
                            if (!oldSelection[selection] || !oldSelection[selection].every(v => fields.includes(v))) {
                                oldSelection[selection] = _.filter(fields.slice(Math.min(i, fields.length - 1)), (field, index) => {
                                    return fieldsType[field].type === 'number';
                                });
                            }
                        }
                    } else if (selectableFields[selection].multiple) {// For multi selections, select the Nth item of the result fields as a single item array.
                        // only update if the old selection no longer covers the new set of fields...
                        if (!oldSelection[selection] || !oldSelection[selection].every(v => fields.includes(v))) {
                            oldSelection[selection] = [fields[Math.min(i, fields.length - 1)]];
                        }

                    } else if (selectableFields[selection].type == SELECTION_TYPES.NODE_PROPERTIES) {
                        // For node property selections, select the most obvious properties of the node to display.
                        const selection = {};
                        fields.forEach(nodeLabelAndProperties => {
                            const label = nodeLabelAndProperties[0];
                            const properties = nodeLabelAndProperties.slice(1);
                            var selectedProp = oldSelection[label] ? oldSelection[label] : undefined;
                            if (autoAssignSelectedProperties) {
                                DEFAULT_NODE_LABELS.forEach(prop => {
                                    if (properties.indexOf(prop) !== -1) {
                                        if (selectedProp == undefined) {
                                            selectedProp = prop;
                                        }
                                    }
                                });
                                selection[label] = selectedProp ? selectedProp : "(label)";
                            } else {
                                selection[label] = selectedProp ? selectedProp : "(no label)";
                            }

                        });
                        _.assign(oldSelection, selection)
                    } else {
                        // Else, default the selection to the Nth item of the result set fields.
                        oldSelection[selection] = [fields[Math.min(i, fields.length - 1)]];
                    }
                }
            }
        });
    }
    return oldSelection;
}
