export const requireModuleUsingGlobalLeaksPattern = async (url) => {
  console.log("[requireModuleUsingGlobalLeaksPattern] Loading module from", url)
  const permaurl = (await fetch(url)).url;
  let iframe = document.createElement("iframe");
  document.body.appendChild(iframe);
  try {
    let beforeKeys = new Set(Object.keys(iframe.contentWindow));
    iframe.contentWindow.document.open();
    let script = iframe.contentWindow.document.appendChild(
      iframe.contentWindow.document.createElement("script")
    );
    script.src = permaurl;
    await new Promise((resolve, reject) => {
      script.addEventListener("load", resolve);
      script.addEventListener("error", reject);
    });
    let newKeys = Object.keys(iframe.contentWindow)
      .filter(k => !beforeKeys.has(k))
      .filter(k => !["ENV", "require"].includes(k));
    if (newKeys.length) {
      return iframe.contentWindow[newKeys[0]];
    }
    iframe.contentWindow.document.close();
  } catch(e) {}
}