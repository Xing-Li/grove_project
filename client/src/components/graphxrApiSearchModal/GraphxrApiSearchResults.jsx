import React from "react";
import * as graphxrApi from "../../stdlib/graphxrApi";

function GraphxrApiSearchResults({ version, results }) {
  return (
    <ul className="searchResults">
      {results.map((result, index) => {
        return (
          <li key={result.name} id={`searchResult-${index}`}>
            <a href={graphxrApi.getDocUrl(result.name, version)} target="_blank">
              <span className="searchTitle">{result.name}</span>
              <span className="searchDescription">{result.description}</span>
            </a>
          </li>
        );
      })}
    </ul>
  );
}

export default React.memo(GraphxrApiSearchResults);
