import { Card, Dropdown, Menu, Tooltip, Input, Select } from 'antd';
import { getFileAttachmentContent, getFileKey, replaceParam } from 'file/fileUtils';
import PropTypes from "prop-types";
import React from 'react';
import { v4 as uuidv4, NIL as NIL_UUID } from 'uuid';
import TagEditor from "./tagEditor";
import { CodeMode, getCodeModeIcon } from "./util/utils";
import _ from 'lodash';
import { escapeStringRegexp } from './util/regexUtils';
const { FILESDIR, GUIDE_LINK_URI } = require("./util/localstorage");
const { AllAccept, codeTime, formatSizeUnits, checkIsMobile, getFileIcon, searchFunc } = require("./util/helpers");

const PREV_TOOL_OPTIONS = {
    paragraph: {},
    codeTool: {},
    antChart: {},
    plotChart: {},
    header: {},
    list: {},
    code: {},
    warning: {},
    quote: {},
    delimiter: {},
};
const TABLE_TOOL_OPTIONS = {
    table: {},
}
const toolsOptions = (editor, options) => _.reduce(options, (prev, v, k) => {
    let config = editor.editorInstance.tools.getAllToolsConfig(k);
    let option = {
        title: config.title,
        icon: `<div style="fill: currentColor;" class="tools-select-icon thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0" >${config.icon}</div>`,
        className: "",
        toolName: k,
        shortcut: config.shortcut,
    }
    prev[k] = option;
    return prev;
}, {});
const PREV_OPTIONS_2 = {
    jsx: {
        codeData: { value: "", codeMode: CodeMode.jsx },
        title: "JSX",
        icon: `<div class="tools-select-icon thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"><i class="${getCodeModeIcon(CodeMode.jsx)}"></i></div>`,
        className: "",
    },
    markdown: {
        codeData: { value: "", codeMode: CodeMode.markdown },
        title: "Markdown",
        icon: `<div class="tools-select-icon thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"><i class="${getCodeModeIcon(CodeMode.markdown)}"></i></div>`,
        className: "",
    },
    python: {
        codeData: { value: "", codeMode: CodeMode.python },
        title: "Python",
        icon: `<div class="tools-select-icon thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"><i class="${getCodeModeIcon(CodeMode.python)}"></i></div>`,
        className: "",
    },
    html: {
        codeData: { value: "", codeMode: CodeMode.htmlmixed },
        title: "HTML",
        icon: `<div class="tools-select-icon thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"><i class="${getCodeModeIcon(CodeMode.htmlmixed)}"></i></div>`,
        className: "",
    },
}
const DATA_OPTIONS = {
    file_attachment: {
        codeData: {
            pinCode: true,
            value: ``
        },
        processFile: true,
        title: "File attachment",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path
            d="M24.7851 9.69193L24.0524 10.3725L24.7851 9.69193ZM20.2008 14.0556L19.4849 14.7538L20.2008 14.0556ZM22.4652 13.2134L28.5448 19.4466L29.9766 18.0501L23.8969 11.8169L22.4652 13.2134ZM27.6793 20.2908L20.9167 13.3574L19.4849 14.7538L26.2476 21.6873L27.6793 20.2908ZM24.0524 10.3725L30.5173 17.3321L31.9827 15.9709L25.5177 9.01134L24.0524 10.3725ZM20.7743 10.4956C21.5997 9.46123 23.1518 9.40298 24.0524 10.3725L25.5177 9.01134C23.785 7.14606 20.7991 7.25813 19.2111 9.24804L20.7743 10.4956ZM20.9167 13.3574C20.1543 12.5758 20.0933 11.3489 20.7743 10.4956L19.2111 9.24804C17.9009 10.8898 18.0183 13.2502 19.4849 14.7538L20.9167 13.3574ZM28.5448 20.2908C28.3076 20.534 27.9165 20.534 27.6793 20.2908L26.2476 21.6873C27.2697 22.7352 28.9544 22.7352 29.9766 21.6873L28.5448 20.2908ZM28.5448 19.4466C28.7739 19.6814 28.7739 20.056 28.5448 20.2908L29.9766 21.6873C30.9633 20.6756 30.9633 19.0617 29.9766 18.0501L28.5448 19.4466Z"
            fill="currentColor"></path>
    </svg>`,
        className: "", desc: "Upload a local file",
    },
    fetch_from_url: {
        codeData: {
            pinCode: true,
            value: `{param} = fetch("https://api.weather.gov/gridpoints/OKX/33,37/forecast").then((response) => response.json())`
        },
        firstParam: 'data',
        title: "Fetch from URL",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path fill-rule="evenodd" clip-rule="evenodd"
            d="M5 10C5 8.89543 5.89543 8 7 8H13L17 12V20C17 21.1046 16.1046 22 15 22H7C5.89543 22 5 21.1046 5 20V10ZM12 10H7V20H15V13H12V10Z"
            fill="currentColor"></path>
        <rect x="8" y="14" width="5" height="2" fill="currentColor"></rect>
        <rect x="8" y="17" width="6" height="2" fill="currentColor"></rect>
        <circle cx="39" cy="15" r="6.25" stroke="currentColor" stroke-width="1.5"></circle>
        <line x1="33" y1="17.25" x2="45" y2="17.25" stroke="currentColor" stroke-width="1.5">
        </line>
        <path d="M33 13H45" stroke="currentColor" stroke-width="1.5"></path>
        <path d="M39 9C39.5 9.83333 41.5 11.6875 41.5 15C41.5 19 39 21 39 21"
            stroke="currentColor" stroke-width="1.5"></path>
        <path d="M39 9C38.5 9.83333 36.5 11.6875 36.5 15C36.5 19 39 21 39 21"
            stroke="currentColor" stroke-width="1.5"></path>
        <path d="M21 15L19 15" stroke="currentColor" stroke-width="2"></path>
        <path d="M24 15L22 15" stroke="currentColor" stroke-width="2"></path>
        <path d="M27 15L25 15" stroke="currentColor" stroke-width="2"></path>
        <path d="M30 15L28 15" stroke="currentColor" stroke-width="2"></path>
    </svg>`,
        className: "", desc: "Request a file or API via an HTTP",
    },
    database_query: {
        codeData: { value: "", codeMode: CodeMode.sql, db: undefined, tableName: "" },
        title: "Database query",
        icon: `<div class="tools-select-icon thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"><i class="${getCodeModeIcon(CodeMode.sql)}"></i></div>`,
        className: "", desc: "Query a SQL database",
    },
    proxy_array: {
        codeData: { value: "", codeMode: CodeMode.javascript2, pinCode: true, syncFile: true },
        title: "Proxy Array",
        icon: `<div class="tools-select-icon thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"><i class="fas fa-box"></i></div>`,
        className: "", desc: "",
        cb: /**@param {import('@editorjs/editorjs').default} api */(api) => {
            setTimeout(() => {
                let block = api.blocks.getBlockByIndex(api.blocks.getCurrentBlockIndex());
                if (block && block.name === 'codeTool' && block.holder) {
                    block.holder.querySelector(".code-edit") && block.holder.querySelector(".code-edit").configSyncFile();
                }
            })
        },
    },
    save_array: {
        codeData: {
            viewInputs: true, pinCode: true,
            value: `viewof {param} = Inputs.button("Save Array To Csv File",{reduce:()=>{ 
    return saveArrayToFile(arrayObj, "fileName.csv");
}})`},
        firstParam: 'button',
        title: "Save Array",
        icon: `<div class="tools-select-icon thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"><i class="fas fa-file-csv"></i></div>`,
        className: "", desc: "Save a array to a csv file",
    },
}
const TABLES_OPTIONS = {
    data_table: {
        codeData: { value: "", codeMode: CodeMode.sql, db: undefined, tableName: "", dbTable: true },
        title: "Data Table",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="18" y="9" width="14" height="12" rx="1" stroke="currentColor" stroke-width="2">
        </rect>
        <rect x="19" y="14" width="12" height="2" fill="currentColor"></rect>
        <rect x="20" y="11" width="2" height="2" fill="currentColor"></rect>
        <rect x="23" y="11" width="7" height="2" fill="currentColor"></rect>
    </svg>`,
        className: "", desc: "Filter and summarize tabular data",
    },
    table_javascript: {
        codeData: { value: `viewof {param} = Inputs.table(cars)` },
        firstParam: 'table',
        title: "JavaScript Table",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="3.5" y="3.5" width="43" height="23" rx="1.5" stroke="currentColor"></rect>
        <g fill="currentColor">
            <rect x="6" y="6" width="14" height="4" rx="1"></rect>
            <rect x="22" y="6" width="18" height="4" rx="1"></rect>
            <g fill-opacity="0.35">
                <rect x="6" y="13" width="8" height="4" rx="1"></rect>
                <rect x="6" y="20" width="10" height="4" rx="1"></rect>
                <rect x="22" y="13" width="22" height="4" rx="1"></rect>
                <rect x="22" y="20" width="21" height="4" rx="1"></rect>
            </g>
        </g>
    </svg>`,
        className: "", desc: "Choose rows from a tabular dataset",
    },
    table_markdown: {
        codeData: {
            codeMode: CodeMode.markdown,
            value: `| name   | quantity |
| ------ | -------- |
| apples | 12       |
| pears  | 34       |` },
        title: "Markdown Table",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="currentColor"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="14" y="3" width="2" height="5" rx="1"></rect>
        <rect x="14" y="9" width="2" height="6" rx="1"></rect>
        <rect x="14" y="16" width="2" height="5" rx="1"></rect>
        <rect x="14" y="22" width="2" height="5" rx="1"></rect>
        <rect x="34" y="3" width="2" height="5" rx="1"></rect>
        <rect x="34" y="9" width="2" height="6" rx="1"></rect>
        <rect x="34" y="16" width="2" height="5" rx="1"></rect>
        <rect x="34" y="22" width="2" height="5" rx="1"></rect>
        <rect x="3" y="11" width="9" height="2" rx="1"></rect>
        <rect x="38" y="11" width="9" height="2" rx="1"></rect>
        <rect x="18" y="11" width="14" height="2" rx="1"></rect>
        <g fill-opacity="0.35">
            <rect x="18" y="3" width="14" height="5" rx="1"></rect>
            <rect x="3" y="3" width="9" height="5" rx="1"></rect>
            <rect x="3" y="16" width="9" height="5" rx="1"></rect>
            <rect x="3" y="22" width="9" height="5" rx="1"></rect>
            <rect x="18" y="16" width="14" height="5" rx="1"></rect>
            <rect x="18" y="22" width="14" height="5" rx="1"></rect>
            <rect x="38" y="22" width="9" height="5" rx="1"></rect>
            <rect x="38" y="16" width="9" height="5" rx="1"></rect>
            <rect x="38" y="3" width="9" height="5" rx="1"></rect>
        </g>
    </svg>`,
        className: "", desc: "Content in rows and columns",
    },
}
const INPUTS_OPTIONS = {
    inputs_button: {
        codeData: { value: `viewof {param} = Inputs.button("Click me")` },
        firstParam: 'button',
        title: "Button",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="13" y="11" width="24" height="8" rx="1" fill="currentColor"
            fill-opacity="0.35"></rect>
        <rect x="3.5" y="7.5" width="43" height="15" rx="3.5" stroke="currentColor"></rect>
    </svg>`,
        className: "", desc: "Do something on click",
    },
    inputs_checkbox: {
        codeData: { value: `viewof {param} = Inputs.checkbox(["A", "B"], {label: "Select some", value: ["A"]})` },
        firstParam: 'checkboxes',
        title: "Checkbox",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="29.5" y="8.5" width="13" height="13" rx="1.5" stroke="currentColor"></rect>
        <rect x="7.5" y="8.5" width="13" height="13" rx="1.5" fill="currentColor"
            stroke="currentColor"></rect>
        <path d="M11 16L13 18L18 12" stroke="#f5f5f5" stroke-width="2" stroke-linecap="round"
            stroke-linejoin="round"></path>
    </svg>`,
        className: "",
        desc: "Choose one or many from a list",
    },
    inputs_color: {
        codeData: { value: `viewof {param} = Inputs.color({label: "Favorite color", value: "#4682b4"})` },
        firstParam: 'color',
        title: "Color",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <defs>
            <clipPath id="clip0_1995_7024">
                <rect x="3" y="3" width="44" height="24" rx="2" fill="white"></rect>
            </clipPath>
        </defs>
        <g clip-path="url(#clip0_1995_7024)">
            <g stroke="currentColor">
                <rect x="6.5" y="6.5" width="5" height="5" rx="0.5"></rect>
                <rect x="14.5" y="6.5" width="5" height="5" rx="0.5"></rect>
                <rect x="22.5" y="6.5" width="5" height="5" rx="0.5"></rect>
                <rect x="30.5" y="6.5" width="5" height="5" rx="0.5"></rect>
                <rect x="38.5" y="6.5" width="5" height="5" rx="0.5"></rect>
            </g>
            <g fill="currentColor">
                <g fill-opacity="0.35">
                    <rect x="6" y="14" width="6" height="6" rx="1"></rect>
                    <rect x="14" y="14" width="6" height="6" rx="1"></rect>
                    <rect x="22" y="14" width="6" height="6" rx="1"></rect>
                    <rect x="30" y="14" width="6" height="6" rx="1"></rect>
                    <rect x="38" y="14" width="6" height="6" rx="1"></rect>
                </g>
                <rect x="6" y="22" width="6" height="6" rx="1"></rect>
                <rect x="14" y="22" width="6" height="6" rx="1"></rect>
                <rect x="22" y="22" width="6" height="6" rx="1"></rect>
                <rect x="30" y="22" width="6" height="6" rx="1"></rect>
                <rect x="38" y="22" width="6" height="6" rx="1"></rect>
            </g>
        </g>
        <rect x="3.5" y="3.5" width="43" height="23" rx="1.5" stroke="currentColor"></rect>
    </svg>`,
        className: "",
        desc: "Choose a color from a palette",
    },
    inputs_date: {
        codeData: { value: `viewof {param} = Inputs.date({label: "Date", value: "2022-07-10"})` },
        firstParam: 'date',
        title: "Date",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <g stroke="currentColor">
            <rect x="35.5" y="12.5" width="7" height="6" rx="0.5"></rect>
            <rect x="3.5" y="7.5" width="43" height="15" rx="3.5"></rect>
        </g>
        <g fill="currentColor">
            <path
                d="M36 11.5C36 11.2239 36.2239 11 36.5 11V11C36.7761 11 37 11.2239 37 11.5V13H36V11.5Z">
            </path>
            <path
                d="M41 11.5C41 11.2239 41.2239 11 41.5 11V11C41.7761 11 42 11.2239 42 11.5V13H41V11.5Z">
            </path>
            <rect x="36" y="13" width="6" height="1"></rect>
            <rect x="7" y="11" width="24" height="8" rx="1" fill-opacity="0.35"></rect>
        </g>
    </svg>`,
        className: "",
        desc: "Choose a date from a calendar",
    },
    inputs_file: {
        codeData: { value: `viewof {param} = Inputs.file({label: "Data"})` },
        firstParam: 'file',
        title: "File",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <g stroke="currentColor">
            <path
                d="M42 18.5H37C36.7239 18.5 36.5 18.2761 36.5 18V12C36.5 11.7239 36.7239 11.5 37 11.5H39.5858C39.7184 11.5 39.8456 11.5527 39.9393 11.6464L41.1464 12.8536L42.3536 14.0607C42.4473 14.1544 42.5 14.2816 42.5 14.4142V18C42.5 18.2761 42.2761 18.5 42 18.5Z">
            </path>
            <path d="M39.5 11.5V14C39.5 14.2761 39.7239 14.5 40 14.5H42.5"></path>
            <rect x="3.5" y="7.5" width="43" height="15" rx="3.5"></rect>
        </g>
        <rect x="7" y="11" width="24" height="8" rx="1" fill="currentColor" fill-opacity="0.35">
        </rect>
    </svg>`,
        className: "",
        desc: "Choose a local file",
    },
    inputs_radios: {
        codeData: { value: `viewof {param} = Inputs.radio(["A", "B"], {label: "Select one", value: "A"})` },
        firstParam: 'radios',
        title: "Radio",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="29.5" y="8.5" width="13" height="13" rx="6.5" stroke="currentColor"></rect>
        <rect x="7.5" y="8.5" width="13" height="13" rx="6.5" fill="currentColor"
            stroke="currentColor"></rect>
        <circle cx="14" cy="15" r="3" fill="#f5f5f5"></circle>
    </svg>`,
        className: "",
        desc: "Choose one from a list",
    },
    inputs_range: {
        codeData: { value: `viewof {param} = Inputs.range([0, 100], {label: "Amount", step: 1})` },
        firstParam: 'range',
        title: "Range",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="3.5" y="12.5" width="43" height="5" rx="2.5" stroke="currentColor"></rect>
        <rect x="3" y="12" width="31" height="6" rx="3" fill="currentColor"></rect>
        <circle cx="32" cy="15" r="5.5" fill="#f5f5f5" stroke="currentColor"></circle>
    </svg>`,
        className: "",
        desc: "Choose a number with a slider",
    },
    inputs_search: {
        codeData: { value: `viewof {param} = Inputs.search(cars)` },
        firstParam: 'search',
        title: "Search",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="3.5" y="7.5" width="43" height="15" rx="3.5" stroke="currentColor"></rect>
        <path fill-rule="evenodd" clip-rule="evenodd"
            d="M10.0625 16.25C11.2706 16.25 12.25 15.2706 12.25 14.0625C12.25 12.8544 11.2706 11.875 10.0625 11.875C8.85438 11.875 7.875 12.8544 7.875 14.0625C7.875 15.2706 8.85438 16.25 10.0625 16.25ZM10.0625 17.5C11.961 17.5 13.5 15.961 13.5 14.0625C13.5 12.164 11.961 10.625 10.0625 10.625C8.16402 10.625 6.625 12.164 6.625 14.0625C6.625 15.961 8.16402 17.5 10.0625 17.5Z"
            fill="currentColor"></path>
        <rect x="11.7324" y="16.6161" width="1.25" height="3.125"
            transform="rotate(-45 11.7324 16.6161)" fill="currentColor"></rect>
    </svg>`,
        className: "",
        desc: "Full-text search of tabular data",
    },
    inputs_select: {
        codeData: { value: `viewof {param} = Inputs.select(["A", "B"], {label: "Select one"})` },
        firstParam: 'select',
        title: "Select",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="6" y="11" width="23" height="8" rx="1" fill="currentColor" fill-opacity="0.35">
        </rect>
        <path d="M36 14L39 17L42 14" stroke="currentColor" stroke-width="2"
            stroke-linecap="round" stroke-linejoin="round"></path>
        <rect x="3.5" y="7.5" width="43" height="15" rx="3.5" stroke="currentColor"></rect>
    </svg>`,
        className: "",
        desc: "Choose one from a dropdown",
    },
    inputs_text: {
        codeData: { value: `viewof {param} = Inputs.text({label: "Name"})` },
        firstParam: 'text',
        title: "Text",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="7" y="11" width="11" height="8" rx="1" fill="currentColor" fill-opacity="0.35">
        </rect>
        <path d="M21 11V19" stroke="currentColor" stroke-width="2" stroke-linecap="round"
            stroke-linejoin="round"></path>
        <rect x="3.5" y="7.5" width="43" height="15" rx="3.5" stroke="currentColor"></rect>
    </svg>`,
        className: "",
        desc: "Freeform, single-line text input",
    },
    inputs_textarea: {
        codeData: { value: `viewof {param} = Inputs.textarea({label: "Description"})` },
        firstParam: 'textarea',
        title: "Textarea",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="3.5" y="3.5" width="43" height="23" rx="1.5" stroke="currentColor"></rect>
        <g fill="currentColor" fill-opacity="0.35">
            <rect x="7" y="7" width="9" height="6" rx="1"></rect>
            <rect x="19" y="7" width="24" height="6" rx="1"></rect>
            <rect x="7" y="17" width="21" height="6" rx="1"></rect>
        </g>
        <path d="M31 17V23" stroke="currentColor" stroke-width="2" stroke-linecap="round"
            stroke-linejoin="round"></path>
    </svg>`,
        className: "",
        desc: "Freeform, multi-line text input",
    },
    inputs_toggle: {
        codeData: { value: `viewof {param} = Inputs.toggle({label: "Active", value: true})` },
        firstParam: 'toggle',
        title: "Toggle",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="12" y="8" width="26" height="14" rx="7" fill="currentColor"></rect>
        <circle cx="31" cy="15" r="5.5" fill="#f5f5f5" stroke="currentColor"></circle>
    </svg>`,
        className: "",
        desc: "Choose on or off",
    },
    inputs_form: {
        codeData: {
            value: `viewof {param} = Inputs.form({
            option1: Inputs.checkbox(["A", "B"], {label: "Select some"}),
            option2: Inputs.range([0, 100], {label: "Amount", step: 1}),
            option3: Inputs.radio(["A", "B"], {label: "Select one"}),
            option4: Inputs.select(["A", "B"], {label: "Select one"})
          })`},
        firstParam: 'form',
        title: "Form",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="20.5" y="4.5" width="9" height="9" rx="1.5" stroke="currentColor"></rect>
        <rect x="36.5" y="4.5" width="9" height="9" rx="1.5" stroke="currentColor"></rect>
        <rect x="4.5" y="4.5" width="9" height="9" rx="1.5" fill="currentColor"
            stroke="currentColor"></rect>
        <path d="M6.85742 9.71456L8.28599 11.1431L11.8574 6.85742" stroke="#f5f5f5"
            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
        <rect x="4.5" y="20.5" width="42" height="4" rx="2" stroke="currentColor"></rect>
        <rect x="4" y="20" width="33" height="5" rx="2.5" fill="currentColor"></rect>
        <circle cx="33.5" cy="22.5" r="4" fill="#f5f5f5" stroke="currentColor"></circle>
    </svg>`,
        className: "", desc: "Combine multiple inputs",
    },
};

const PLOT_OPTIONS = {
    area_chart: {
        codeData: {
            value: `Plot.plot({
    marks: [
        Plot.areaY(aapl, {x: "Date", y: "Close"}),
        Plot.ruleY([0])
    ]
})` },
        title: "Area chart",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path
            d="M12.0417 18.2281L4 25C3.63098 25.369 3.89233 26 4.41421 26H45.0001C45.5523 26 46.0001 25.5523 46.0001 25V5.20711C46.0001 5.0745 45.9474 4.94732 45.8536 4.85355C45.6584 4.65829 45.3418 4.65829 45.1465 4.85355L36.3787 13.6213C35.8161 14.1839 35.0531 14.5 34.2574 14.5H26.7427C25.9471 14.5 25.184 14.8161 24.6214 15.3787L21.4792 18.5208C20.5905 19.4096 19.2419 19.6501 18.1007 19.1234L15.2312 17.799C14.1748 17.3114 12.9317 17.4787 12.0417 18.2281Z"
            fill="currentColor" stroke="currentColor" stroke-width="2" stroke-linecap="round"
            stroke-linejoin="round"></path>
    </svg>`,
        className: "", desc: "Values over time as filled areas",
    },
    band_area_chart: {
        codeData: {
            value: `Plot.plot({
    marks: [
        Plot.areaY(weather.slice(-365), {x: "date", y1: "temp_min", y2: "temp_max", curve: "step"})
    ]
})` },
        title: "Band area chart",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path
            d="M12.0416 18.2281L4.11345 24.9045C4.03861 24.9675 3.97743 25.0451 3.93367 25.1327C3.62808 25.7438 4.26801 26.3962 4.88495 26.1024L12.5552 22.4499C13.167 22.1586 13.8594 22.0829 14.5196 22.2353L18.7752 23.2174C19.5588 23.3982 20.3821 23.2569 21.0604 22.8252L24.9634 20.3415C25.3172 20.1163 25.7143 19.9679 26.129 19.9056L35.0005 18.5749C35.3312 18.5253 35.6512 18.4208 35.9475 18.2656L44.392 13.8423C45.3806 13.3245 46 12.3007 46 11.1848V5.20711C46 5.0745 45.9473 4.94732 45.8536 4.85355C45.6583 4.65829 45.3417 4.65829 45.1464 4.85355L36.3787 13.6213C35.8161 14.1839 35.053 14.5 34.2574 14.5H26.7426C25.947 14.5 25.1839 14.8161 24.6213 15.3787L21.4792 18.5208C20.5904 19.4096 19.2419 19.6501 18.1007 19.1234L15.2312 17.799C14.1747 17.3114 12.9316 17.4787 12.0416 18.2281Z"
            fill="currentColor" stroke="currentColor" stroke-width="2" stroke-linecap="round"
            stroke-linejoin="round"></path>
    </svg>`,
        className: "", desc: "Values over time as filled areas",
    },
    stacked_area_chart: {
        codeData: {
            value: `Plot.plot({
    y: {
        tickFormat: "s"
    },
    marks: [
        Plot.areaY(industries, {x: "date", y: "unemployed", fill: "industry"}),
        Plot.ruleY([0])
    ]
})` },
        title: "Stacked area chart",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path
            d="M12.0415 18.2282L4.05513 24.9536C4.01851 24.9844 3.98576 25.0196 3.9576 25.0583C3.67249 25.4502 3.95242 26 4.43704 26H44.9999C45.5522 26 45.9999 25.5523 45.9999 25V5.20711C45.9999 5.0745 45.9472 4.94732 45.8535 4.85355C45.6582 4.65829 45.3416 4.65829 45.1464 4.85355L36.3786 13.6213C35.816 14.1839 35.0529 14.5 34.2573 14.5H26.7425C25.9469 14.5 25.1838 14.8161 24.6212 15.3787L21.4791 18.5208C20.5903 19.4096 19.2418 19.6501 18.1006 19.1234L15.2311 17.799C14.1746 17.3114 12.9315 17.4787 12.0415 18.2282Z"
            fill="currentColor" stroke="currentColor" stroke-width="2" stroke-linecap="round"
            stroke-linejoin="round"></path>
        <path
            d="M2.5 26L12.0338 22.3681C12.6675 22.1267 13.3522 22.0503 14.0235 22.1462L18.4871 22.7839C19.4614 22.9231 20.4527 22.6982 21.2716 22.1523L24.7876 19.8082C25.5707 19.2862 26.5128 19.057 27.4482 19.1609L33.6581 19.8509C34.5286 19.9476 35.4067 19.756 36.1578 19.3053L47.5 12.5"
            stroke="#f5f5f5"></path>
    </svg>`,
        className: "", desc: "Values over time as filled areas",
    },
    arrow_chart: {
        codeData: {
            value: `Plot.plot({
    x: {
        type: "log"
    },
    marks: [
        Plot.arrow(citywages, {
        x1: "POP_1980",
        y1: "R90_10_1980",
        x2: "POP_2015",
        y2: "R90_10_2015",
        bend: true
        })
    ]
})` },
        title: "Arrow chart",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <line x1="17.0882" y1="23.5885" x2="32.5885" y2="9.91178" stroke="currentColor"
            stroke-width="2" stroke-linecap="round"></line>
        <line x1="32.4215" y1="24.7095" x2="39.7095" y2="5.57849" stroke="currentColor"
            stroke-width="2" stroke-linecap="round"></line>
        <line x1="24.4148" y1="21.7125" x2="29.7125" y2="7.58521" stroke="currentColor"
            stroke-width="2" stroke-linecap="round"></line>
        <line x1="37.4019" y1="21.7185" x2="40.7185" y2="12.5981" stroke="currentColor"
            stroke-width="2" stroke-linecap="round"></line>
        <line x1="6.04284" y1="19.5864" x2="21.5864" y2="4.95716" stroke="currentColor"
            stroke-width="2" stroke-linecap="round"></line>
        <line x1="7.05652" y1="24.5869" x2="18.5869" y2="13.9435" stroke="currentColor"
            stroke-width="2" stroke-linecap="round"></line>
    </svg>`,
        className: "", desc: "Connected pairs of values",
    },
    Bar_chart: {
        codeData: {
            value: `Plot.plot({
    marks: [
        Plot.barY(alphabet, {x: "letter", y: "frequency", sort: {x: "y", reverse: true}}),
        Plot.ruleY([0])
    ]
})` },
        title: "Bar chart",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path
            d="M3 13C3 11.8954 3.89543 11 5 11H9C10.1046 11 11 11.8954 11 13V25C11 26.1046 10.1046 27 9 27H5C3.89543 27 3 26.1046 3 25V13Z"
            fill="currentColor"></path>
        <path
            d="M12 16C12 14.8954 12.8954 14 14 14H18C19.1046 14 20 14.8954 20 16V25C20 26.1046 19.1046 27 18 27H14C12.8954 27 12 26.1046 12 25V16Z"
            fill="currentColor"></path>
        <rect x="21" y="18" width="8" height="9" rx="2" fill="currentColor"></rect>
        <path
            d="M30 8C30 6.89543 30.8954 6 32 6H36C37.1046 6 38 6.89543 38 8V25C38 26.1046 37.1046 27 36 27H32C30.8954 27 30 26.1046 30 25V8Z"
            fill="currentColor"></path>
        <path
            d="M39 13C39 11.8954 39.8954 11 41 11H45C46.1046 11 47 11.8954 47 13V25C47 26.1046 46.1046 27 45 27H41C39.8954 27 39 26.1046 39 25V13Z"
            fill="currentColor"></path>
    </svg>`,
        className: "", desc: "Values by category as bars",
    },
    Horizontal_bar_chart: {
        codeData: {
            value: `Plot.plot({
    marks: [
        Plot.barX(alphabet, {x: "frequency", y: "letter", sort: {y: "x", reverse: true}}),
        Plot.ruleX([0])
    ]
})` },
        title: "Horizontal bar chart",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path
            d="M3 5C3 3.89543 3.89543 3 5 3H21C22.1046 3 23 3.89543 23 5V8C23 9.10457 22.1046 10 21 10H5C3.89543 10 3 9.10457 3 8V5Z"
            fill="currentColor"></path>
        <rect x="3" y="11" width="14" height="7" rx="2" fill="currentColor"></rect>
        <path
            d="M3 21C3 19.8954 3.89543 19 5 19H31C32.1046 19 33 19.8954 33 21V24C33 25.1046 32.1046 26 31 26H5C3.89543 26 3 25.1046 3 24V21Z"
            fill="currentColor"></path>
    </svg>`,
        className: "", desc: "Values by category as bars",
    },
    Temporal_bar_chart: {
        codeData: {
            value: `Plot.plot({
    marks: [
        Plot.rectY(weather.slice(-42), {x: "date", y: "wind", interval: d3.utcDay}),
        Plot.ruleY([0])
    ]
})` },
        title: "Temporal bar chart",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path
            d="M3 13C3 11.8954 3.89543 11 5 11H9C10.1046 11 11 11.8954 11 13V25C11 26.1046 10.1046 27 9 27H5C3.89543 27 3 26.1046 3 25V13Z"
            fill="currentColor"></path>
        <path
            d="M12 16C12 14.8954 12.8954 14 14 14H18C19.1046 14 20 14.8954 20 16V25C20 26.1046 19.1046 27 18 27H14C12.8954 27 12 26.1046 12 25V16Z"
            fill="currentColor"></path>
        <rect x="21" y="18" width="8" height="9" rx="2" fill="currentColor"></rect>
        <path
            d="M30 8C30 6.89543 30.8954 6 32 6H36C37.1046 6 38 6.89543 38 8V25C38 26.1046 37.1046 27 36 27H32C30.8954 27 30 26.1046 30 25V8Z"
            fill="currentColor"></path>
        <path
            d="M39 13C39 11.8954 39.8954 11 41 11H45C46.1046 11 47 11.8954 47 13V25C47 26.1046 46.1046 27 45 27H41C39.8954 27 39 26.1046 39 25V13Z"
            fill="currentColor"></path>
    </svg>`,
        className: "", desc: "Values over time as rects",
    },
    Box_plot: {
        codeData: {
            value: `Plot.plot({
    marks: [
        Plot.boxX(olympians, {x: "weight", y: "sport", sort: {y: "x"}})
    ]
})` },
        title: "Box plot",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="14.5" y="6.5" width="15" height="7" rx="0.5" stroke="currentColor"></rect>
        <rect x="22.5" y="16.5" width="15" height="7" rx="0.5" stroke="currentColor"></rect>
        <rect x="19" y="6" width="2" height="8" fill="currentColor"></rect>
        <rect x="31" y="16" width="2" height="8" fill="currentColor"></rect>
        <path d="M9 10C9 9.44772 9.44772 9 10 9H14V11H10C9.44772 11 9 10.5523 9 10V10Z"
            fill="currentColor"></path>
        <path d="M16 20C16 19.4477 16.4477 19 17 19H22V21H17C16.4477 21 16 20.5523 16 20V20Z"
            fill="currentColor"></path>
        <rect x="6" y="9" width="2" height="2" rx="1" fill="currentColor"></rect>
        <path d="M30 9H36C36.5523 9 37 9.44772 37 10V10C37 10.5523 36.5523 11 36 11H30V9Z"
            fill="currentColor"></path>
        <path d="M38 19H43C43.5523 19 44 19.4477 44 20V20C44 20.5523 43.5523 21 43 21H38V19Z"
            fill="currentColor"></path>
    </svg>`,
        className: "", desc: "Quantitative distributions as boxes",
    },
    Cell_chart: {
        codeData: {
            value: `Plot.plot({
    marks: [
        Plot.cell(weather.slice(-365), {
        x: d => d.date.getUTCDate(),
        y: d => d.date.getUTCMonth(),
        fill: "temp_max"
        })
    ]
})` },
        title: "Cell chart",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="8.5" y="12.5" width="5" height="5" rx="0.5" stroke="currentColor"></rect>
        <rect x="15.5" y="5.5" width="5" height="5" rx="0.5" stroke="currentColor"></rect>
        <rect x="15" y="12" width="6" height="6" rx="1" fill="currentColor"></rect>
        <rect x="22" y="5" width="6" height="6" rx="1" fill="currentColor"></rect>
        <rect x="22" y="12" width="6" height="6" rx="1" fill="currentColor"></rect>
        <rect x="29.5" y="5.5" width="5" height="5" rx="0.5" stroke="currentColor"></rect>
        <rect x="29.5" y="12.5" width="5" height="5" rx="0.5" stroke="currentColor"></rect>
        <rect x="36" y="5" width="6" height="6" rx="1" fill="currentColor"></rect>
        <rect x="36" y="12" width="6" height="6" rx="1" fill="currentColor"></rect>
        <rect x="8" y="19" width="6" height="6" rx="1" fill="currentColor"></rect>
        <rect x="15.5" y="19.5" width="5" height="5" rx="0.5" stroke="currentColor"></rect>
        <rect x="22.5" y="19.5" width="5" height="5" rx="0.5" stroke="currentColor"></rect>
    </svg>`,
        className: "", desc: "Values by category in a grid",
    },
    Faceted_chart: {
        codeData: {
            value: `Plot.plot({
    facet: {
        data: penguins,
        x: "species"
    },
    marks: [
        Plot.frame(),
        Plot.dot(penguins, {x: "culmen_length_mm", y: "culmen_depth_mm"})
    ]
})` },
        title: "Faceted chart",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <circle cx="8.5" cy="20.5" r="2" stroke="currentColor"></circle>
        <circle cx="11.5" cy="11.5" r="2" stroke="currentColor"></circle>
        <circle cx="13.5" cy="15.5" r="2" stroke="currentColor"></circle>
        <circle cx="17.5" cy="19.5" r="2" stroke="currentColor"></circle>
        <circle cx="31.5" cy="14.5" r="2" stroke="currentColor"></circle>
        <circle cx="33.5" cy="10.5" r="2" stroke="currentColor"></circle>
        <circle cx="42.5" cy="7.5" r="2" stroke="currentColor"></circle>
        <circle cx="39.5" cy="11.5" r="2" stroke="currentColor"></circle>
        <path d="M25 5L25 26" stroke="currentColor" stroke-width="2" stroke-linecap="round">
        </path>
    </svg>`,
        className: "", desc: "Small multiples",
    },
    Histogram_chart: {
        codeData: {
            value: `Plot.plot({
    marks: [
        Plot.rectY(olympians, Plot.binX({y: "count"}, {x: "weight"})),
        Plot.ruleY([0])
    ]
})` },
        title: "Histogram chart",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path
            d="M3 11C3 9.89543 3.89543 9 5 9H9C10.1046 9 11 9.89543 11 11V25C11 26.1046 10.1046 27 9 27H5C3.89543 27 3 26.1046 3 25V11Z"
            fill="currentColor"></path>
        <path
            d="M12 7C12 5.89543 12.8954 5 14 5H18C19.1046 5 20 5.89543 20 7V25C20 26.1046 19.1046 27 18 27H14C12.8954 27 12 26.1046 12 25V7Z"
            fill="currentColor"></path>
        <path
            d="M21 17C21 15.8954 21.8954 15 23 15H27C28.1046 15 29 15.8954 29 17V25C29 26.1046 28.1046 27 27 27H23C21.8954 27 21 26.1046 21 25V17Z"
            fill="currentColor"></path>
        <path
            d="M30 22C30 20.8954 30.8954 20 32 20H36C37.1046 20 38 20.8954 38 22V25C38 26.1046 37.1046 27 36 27H32C30.8954 27 30 26.1046 30 25V22Z"
            fill="currentColor"></path>
        <path
            d="M39 25C39 23.8954 39.8954 23 41 23H45C46.1046 23 47 23.8954 47 25V25C47 26.1046 46.1046 27 45 27H41C39.8954 27 39 26.1046 39 25V25Z"
            fill="currentColor"></path>
    </svg>`,
        className: "", desc: "Quantitative distribution as bars",
    },
    Line_chart: {
        codeData: {
            value: `Plot.plot({
    marks: [
        Plot.ruleY([0]),
        Plot.lineY(aapl, {x: "Date", y: "Close"})
    ]
})` },
        title: "Line chart",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path
            d="M5.5 24.5L12.0258 18.3821C12.9169 17.5466 14.2257 17.3349 15.3348 17.8468L18.1007 19.1234C19.2419 19.6501 20.5904 19.4096 21.4792 18.5208L24.6213 15.3787C25.1839 14.8161 25.947 14.5 26.7426 14.5H34.2574C35.053 14.5 35.8161 14.1839 36.3787 13.6213L44.5 5.5"
            stroke="currentColor" stroke-width="2" stroke-linecap="round"
            stroke-linejoin="round"></path>
    </svg>`,
        className: "", desc: "Values over time as lines",
    },
    Moving_average_line_chart: {
        codeData: {
            value: `Plot.plot({
    marks: [
        Plot.ruleY([0]),
        Plot.lineY(aapl, Plot.windowY({x: "Date", y: "Close", k: 10, reduce: "mean"}))
    ]
})` },
        title: "Moving average line chart",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path
            d="M42.5 6.5L37.7448 12.3874C34.308 16.6425 28.5479 18.2447 23.4076 16.3755V16.3755C18.6077 14.6301 13.2295 15.9041 9.7226 19.6172L7 22.5"
            stroke="currentColor" stroke-width="2" stroke-linecap="round"
            stroke-linejoin="round"></path>
    </svg>`,
        className: "", desc: "Values over time as lines",
    },
    Multi_series_line_chart: {
        codeData: {
            value: `Plot.plot({
    marks: [
        Plot.ruleY([0]),
        Plot.lineY(industries, {x: "date", y: "unemployed", z: "industry"})
    ]
})` },
        title: "Multi-series line chart",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="currentColor"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <g fill-opacity="0.35">
            <path
                d="M35.6702 11.5014C34.9458 10.8618 34.0064 10.5 33.0228 10.5H28.7119C27.2973 10.5 25.9878 11.2472 25.2682 12.4651L25.0793 12.7848C25.6086 12.598 26.1706 12.5 26.7426 12.5H34.2574C34.5226 12.5 34.7769 12.3946 34.9645 12.2071L35.6702 11.5014Z">
            </path>
            <path
                d="M7.9091 19.5H5.5C4.94772 19.5 4.5 19.9477 4.5 20.5C4.5 21.0523 4.94772 21.5 5.5 21.5H5.77577L7.9091 19.5Z">
            </path>
            <path
                d="M11.6242 21.5H14.0132C14.2281 21.5 14.4417 21.5347 14.6456 21.6026L17.885 22.6824C19.6731 23.2785 21.6348 22.5453 22.5936 20.9226L23.5871 19.2413L22.8934 19.935C21.4121 21.4163 19.1646 21.8171 17.2626 20.9393L14.4967 19.6627C14.127 19.4921 13.6907 19.5627 13.3937 19.8412L11.6242 21.5Z">
            </path>
            <path
                d="M36.6257 15.9035L37.8067 17.4388C38.5639 18.4232 39.7353 19 40.9772 19H44.5C45.0523 19 45.5 18.5523 45.5 18C45.5 17.4477 45.0523 17 44.5 17H40.9772C40.3563 17 39.7705 16.7116 39.392 16.2194L38.182 14.6464L37.7929 15.0355C37.445 15.3834 37.0512 15.6747 36.6257 15.9035Z">
            </path>
            <path
                d="M38.9524 8.21924L37.2871 7.14172C36.6398 6.72285 35.8851 6.5 35.1141 6.5H26.7427C25.6818 6.5 24.6644 6.92143 23.9143 7.67157L19.6716 11.9142C19.2965 12.2893 18.7878 12.5 18.2574 12.5H14.2082C13.5873 12.5 12.9748 12.6446 12.4194 12.9223L5.05283 16.6056C4.55885 16.8526 4.35863 17.4532 4.60562 17.9472C4.85261 18.4412 5.45328 18.6414 5.94726 18.3944L13.3138 14.7111C13.5915 14.5723 13.8978 14.5 14.2082 14.5H18.2574C19.3183 14.5 20.3357 14.0786 21.0858 13.3284L25.3285 9.08579C25.7035 8.71071 26.2123 8.5 26.7427 8.5H35.1141C35.4996 8.5 35.8769 8.61142 36.2006 8.82086L37.5061 9.66556L38.9524 8.21924Z">
            </path>
            <path
                d="M40.9406 11.8879L43.9568 13.8396C44.4205 14.1396 45.0396 14.0069 45.3396 13.5433C45.6396 13.0796 45.507 12.4605 45.0433 12.1604L42.3869 10.4416L40.9406 11.8879Z">
            </path>
        </g>
        <path
            d="M5.5 24.5L12.0258 18.3821C12.9169 17.5466 14.2257 17.3349 15.3348 17.8468L18.1007 19.1234C19.2419 19.6501 20.5904 19.4096 21.4792 18.5208L24.6213 15.3787C25.1839 14.8161 25.947 14.5 26.7426 14.5H34.2574C35.053 14.5 35.8161 14.1839 36.3787 13.6213L44.5 5.5"
            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
            stroke-linejoin="round"></path>
    </svg>`,
        className: "", desc: "Multiple values over time as lines",
    },
    Scatterplot_chart: {
        codeData: {
            value: `Plot.plot({
    marks: [
        Plot.dot(cars, {x: "power (hp)", y: "economy (mpg)"})
    ]
})` },
        title: "Scatterplot chart",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <circle cx="10.5" cy="20.5" r="2" stroke="currentColor"></circle>
        <circle cx="14.5" cy="15.5" r="2" stroke="currentColor"></circle>
        <circle cx="16.5" cy="16.5" r="2" stroke="currentColor"></circle>
        <circle cx="28.5" cy="14.5" r="2" stroke="currentColor"></circle>
        <circle cx="34.5" cy="13.5" r="2" stroke="currentColor"></circle>
        <circle cx="36.5" cy="12.5" r="2" stroke="currentColor"></circle>
        <circle cx="23.5" cy="18.5" r="2" stroke="currentColor"></circle>
        <circle cx="20.5" cy="12.5" r="2" stroke="currentColor"></circle>
        <circle cx="40.5" cy="8.5" r="2" stroke="currentColor"></circle>
    </svg>`,
        className: "", desc: "Correlation or distribution as dots",
    },
    Tick_chart: {
        codeData: {
            value: `Plot.plot({
    marks: [
        Plot.tickX(cars, {x: "economy (mpg)", y: "year"})
    ]
})` },
        title: "Tick chart",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="8" y="9" width="2" height="12" rx="1" fill="currentColor"></rect>
        <rect x="11" y="9" width="2" height="12" rx="1" fill="currentColor"></rect>
        <rect x="19" y="9" width="2" height="12" rx="1" fill="currentColor"></rect>
        <rect x="22" y="9" width="5" height="12" rx="1" fill="currentColor"></rect>
        <rect x="28" y="9" width="3" height="12" rx="1" fill="currentColor"></rect>
        <rect x="32" y="9" width="2" height="12" rx="1" fill="currentColor"></rect>
        <rect x="40" y="9" width="2" height="12" rx="1" fill="currentColor"></rect>
    </svg>`,
        className: "", desc: "Quantitative distributions as lines",
    },
    Top_10_bar_chart: {
        codeData: {
            value: `Plot.plot({
    marks: [
        Plot.barX(olympians, Plot.groupY({x: "count"}, {y: "nationality", sort: {y: "x", reverse: true, limit: 10}})),
        Plot.ruleX([0])
    ]
})` },
        title: "Top 10 bar chart",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path
            d="M3 5C3 3.89543 3.89543 3 5 3H35C36.1046 3 37 3.89543 37 5V8C37 9.10457 36.1046 10 35 10H5C3.89543 10 3 9.10457 3 8V5Z"
            fill="currentColor"></path>
        <path
            d="M3 13C3 11.8954 3.89543 11 5 11H29C30.1046 11 31 11.8954 31 13V16C31 17.1046 30.1046 18 29 18H5C3.89543 18 3 17.1046 3 16V13Z"
            fill="currentColor"></path>
        <path
            d="M3 21C3 19.8954 3.89543 19 5 19H23C24.1046 19 25 19.8954 25 21V24C25 25.1046 24.1046 26 23 26H5C3.89543 26 3 25.1046 3 24V21Z"
            fill="currentColor"></path>
    </svg>`,
        className: "", desc: "Group and sort categories by count",
    },
    Weighted_top_10_bar_chart: {
        codeData: {
            value: `Plot.plot({
    marks: [
        Plot.barX(olympians, Plot.groupY({x: "sum"}, {x: "gold", y: "nationality", sort: {y: "x", reverse: true, limit: 10}})),
        Plot.ruleX([0])
    ]
})` },
        title: "Weighted top 10 bar chart",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path
            d="M3 5C3 3.89543 3.89543 3 5 3H35C36.1046 3 37 3.89543 37 5V8C37 9.10457 36.1046 10 35 10H5C3.89543 10 3 9.10457 3 8V5Z"
            fill="currentColor"></path>
        <path
            d="M3 13C3 11.8954 3.89543 11 5 11H29C30.1046 11 31 11.8954 31 13V16C31 17.1046 30.1046 18 29 18H5C3.89543 18 3 17.1046 3 16V13Z"
            fill="currentColor"></path>
        <path
            d="M3 21C3 19.8954 3.89543 19 5 19H23C24.1046 19 25 19.8954 25 21V24C25 25.1046 24.1046 26 23 26H5C3.89543 26 3 25.1046 3 24V21Z"
            fill="currentColor"></path>
    </svg>`,
        className: "", desc: "Group and sort categories by value",
    },
    Hexbin_chart: {
        codeData: {
            value: `Plot.plot({
    color: {
        scheme: "ylgnbu"
    },
    marks: [
        Plot.hexagon(olympians, Plot.hexbin({fill: "sum"}, {x: "weight", y: "height"}))
    ]
})` },
        title: "Hexbin chart",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="currentColor"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path d="M27 5L30.0311 6.75V10.25L27 12L23.9689 10.25V6.75L27 5Z"></path>
        <path d="M19 19L22.0311 20.75V24.25L19 26L15.9689 24.25V20.75L19 19Z"></path>
        <path d="M27 19L30.0311 20.75V24.25L27 26L23.9689 24.25V20.75L27 19Z"></path>
        <path d="M23 12L26.0311 13.75V17.25L23 19L19.9689 17.25V13.75L23 12Z"></path>
        <g fill-opacity="0.35">
            <path d="M19 5L22.0311 6.75V10.25L19 12L15.9689 10.25V6.75L19 5Z"></path>
            <path d="M35 5L38.0311 6.75V10.25L35 12L31.9689 10.25V6.75L35 5Z"></path>
            <path d="M15 12L18.0311 13.75V17.25L15 19L11.9689 17.25V13.75L15 12Z"></path>
            <path d="M31 12L34.0311 13.75V17.25L31 19L27.9689 17.25V13.75L31 12Z"></path>
        </g>
        <g stroke="currentColor" fill="none">
            <path
                d="M8.46891 7.03868L11 5.57735L13.5311 7.03868V9.96133L11 11.4226L8.46891 9.96133V7.03868Z">
            </path>
            <path
                d="M8.46891 21.0387L11 19.5774L13.5311 21.0387V23.9613L11 25.4226L8.46891 23.9613V21.0387Z">
            </path>
            <path
                d="M32.4689 21.0387L35 19.5774L37.5311 21.0387V23.9613L35 25.4226L32.4689 23.9613V21.0387Z">
            </path>
            <path
                d="M36.4689 14.0387L39 12.5774L41.5311 14.0387V16.9613L39 18.4226L36.4689 16.9613V14.0387Z">
            </path>
        </g>
    </svg>`,
        className: "", desc: "Distribution as hexagonal bins",
    },
    Beeswarm_chart: {
        codeData: {
            value: `Plot.plot({
    height: 180,
    marks: [
        Plot.dot(cars, Plot.dodgeY({x: "weight (lb)"}))
    ]
})` },
        title: "Beeswarm chart",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="currentColor"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <circle cx="7" cy="23" r="2"></circle>
        <circle cx="10" cy="18" r="2"></circle>
        <circle cx="13" cy="23" r="2"></circle>
        <circle cx="25" cy="23" r="2"></circle>
        <circle cx="31" cy="23" r="2"></circle>
        <circle cx="31" cy="18" r="2"></circle>
        <circle cx="40" cy="8" r="2"></circle>
        <circle cx="25" cy="18" r="2"></circle>
        <circle cx="40" cy="18" r="2"></circle>
        <g fill-opacity="0.35">
            <circle cx="37" cy="23" r="2"></circle>
            <circle cx="43" cy="23" r="2"></circle>
            <circle cx="19" cy="23" r="2"></circle>
            <circle cx="7" cy="13" r="2"></circle>
            <circle cx="31" cy="13" r="2"></circle>
            <circle cx="43" cy="13" r="2"></circle>
            <circle cx="31" cy="8" r="2"></circle>
        </g>
    </svg>`,
        className: "", desc: "Distribution as dodged dots",
    },
}

const MAPS_OPTIONS = {
    Interactive_map: {
        codeData: {
            value: `map1 = {
    const container = yield htl.html\`<div style="height: 500px;">\`;
    const map = L.map(container).setView([37.774, -122.423], 13);
    L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
        attribution: "© <a href=https://www.openstreetmap.org/copyright>OpenStreetMap</a> contributors"
    }).addTo(map);
}` },
        title: "Interactive map",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path d="M29 19H31V20H30V22H29V19Z" fill="currentColor"></path>
        <path d="M38 23V21V20H40V19H42V20H43V21H44V23H43V24H42V25H41V23H38Z"
            fill="currentColor"></path>
        <path d="M46 23V24H45V25H44H43V24H44H45V23H46Z" fill="currentColor"></path>
        <path d="M41 18V17H43V18H44V19H42V18H41Z" fill="currentColor"></path>
        <path d="M37 19H40V18H39V17H40V16H41V15H39V16H38V17H36V18H37V19Z" fill="currentColor">
        </path>
        <path
            d="M23 11H24H25V10H26V9H27H28V10H27V11H29V12H30V13H32V14H33V16H34V17H35V14H36V16H38V14H39V11H38V10H39H40V12H41V9H40V8H39V7H40H41V8H42V5H39V4H37V5H34V4H31V5H30V6H29V4H28V6H27V5H26V7H24V8H23V9H22V11H23Z"
            fill="currentColor"></path>
        <path
            d="M24 11V12H29V13H30V15H28V16H30V17H29V18H28V20V21H27V23H26V24H25V23H24V17H21V16H20V14H21V13H22V12H23V11H24Z"
            fill="currentColor"></path>
        <path
            d="M6 12V9H5V8H4V7H5V6H6V5H8V6H12V5H13H14V4H16V6H14V7H16V9H15V10H14V11H13V12H12V13H10V14V15H11H15V16H16V17H17V18H19V20H18V22H17V24H16V25H15V27H16V28H15V27H13V21H12V20H11V16H9V15H8V13H7V12H6Z"
            fill="currentColor"></path>
        <path d="M17 5V4H21V5H20V6H19V7H18V5H17Z" fill="currentColor"></path>
        <path d="M21 6H22V7H21V6Z" fill="currentColor"></path>
        <path d="M23 3H25V4H23V3Z" fill="currentColor"></path>
        <path d="M23 5H25V6H24V7H23V5Z" fill="currentColor"></path>
    </svg>`,
        className: "", desc: "Zoomable map with LeafletValues over time as filled areas",
    },
}
const Trees_and_Networks_OPTIONS = {
    Tidy_tree: {
        codeData: {
            value: `Plot.plot({
    axis: null,
    margin: 20,
    marginRight: 120,
    marks: [
        Plot.tree(flare.slice(0, 50), {path: "name", delimiter: "."})
    ]
})` },
        title: "Tidy tree",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path
            d="M10 15H14C15.1046 15 16 14.1046 16 13V11C16 9.89543 16.8954 9 18 9H22 M10 15H14C15.1046 15 16 15.8954 16 17V19C16 20.1046 16.8954 21 18 21H22 M40 4H36C34.8954 4 34 4.89543 34 6V7C34 8.10457 33.1046 9 32 9H28H40 M32 21H28H40M32 21C33.1046 21 34 20.1046 34 19V18C34 16.8954 34.8954 16 36 16H40M32 21C33.1046 21 34 21.8954 34 23V24C34 25.1046 34.8954 26 36 26H40"
            stroke="currentColor" stroke-opacity="0.35" stroke-width="2"></path>
        <g fill="currentColor">
            <rect x="5" y="14" width="4" height="2" rx="0.5"></rect>
            <rect x="23" y="8" width="4" height="2" rx="0.5"></rect>
            <rect x="23" y="20" width="4" height="2" rx="0.5"></rect>
            <rect x="41" y="15" width="4" height="2" rx="0.5"></rect>
            <rect x="41" y="20" width="4" height="2" rx="0.5"></rect>
            <rect x="41" y="25" width="4" height="2" rx="0.5"></rect>
            <rect x="41" y="3" width="4" height="2" rx="0.5"></rect>
            <rect x="41" y="8" width="4" height="2" rx="0.5"></rect>
        </g>
    </svg>`,
        className: "", desc: "Tree diagram with Plot",
    },
    Circle_packing: {
        value_2: `import {Pack} from "${GUIDE_LINK_URI}pack/pack"`,
        value_2Param: "Pack",
        codeData: {
            value: `Pack(flare, {
    path: (d) => d.name.replaceAll(".", "/"), // e.g. flare/animate/Easing
    label: (d) => d.name.split(".").pop(), // display text
    value: (d) => d?.size, // area of each circle
    title: (d, n) => [n.id, n.value.toLocaleString()].join("\\n"), // hover text
    width,
    height: 720
})` },
        title: "Circle packing",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <g stroke="currentColor">
            <circle cx="33" cy="13" r="9.5"></circle>
            <g stroke-opacity="0.35">
                <circle cx="14.5" cy="10.5" r="7" stroke="#C4C4C4"></circle>
                <circle cx="20" cy="22" r="3.5" stroke="#C4C4C4"></circle>
            </g>
        </g>
        <g fill="currentColor">
            <circle cx="30" cy="9" r="3"></circle>
            <circle cx="37" cy="14" r="4"></circle>
            <circle cx="29" cy="16" r="3"></circle>
            <g fill-opacity="0.35">
                <circle cx="16.5" cy="12.5" r="2.5"></circle>
                <circle cx="12.5" cy="8.5" r="2.5"></circle>
                <circle cx="20" cy="22" r="2"></circle>
            </g>
        </g>
    </svg>`,
        className: "", desc: "Nested hierarchy with D3",
    },
    Icicle_partition: {
        value_2: `import {Icicle} from "${GUIDE_LINK_URI}icicle/icicle"`,
        value_2Param: "Icicle",
        codeData: {
            value: `Icicle(flare, {
    path: (d) => d.name.replaceAll(".", "/"), // e.g. flare/animate/Easing
    label: (d) => d.name.split(".").pop(), // display text
    value: (d) => d?.size, // height of each rect
    title: (d, n) => [n.id, n.value.toLocaleString()].join("\\n"), // hover text
    width,
    height: 500
})` },
        title: "Icicle partition",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="currentColor"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="5" y="5" width="12" height="8"></rect>
        <rect x="18" y="9" width="13" height="4"></rect>
        <rect x="32" y="11" width="13" height="2"></rect>
        <g fill-opacity="0.35">
            <rect x="5" y="14" width="12" height="6"></rect>
            <rect x="18" y="14" width="13" height="2"></rect>
            <rect x="18" y="17" width="13" height="3"></rect>
            <path d="M18 5H31V8H18V5Z"></path>
            <rect x="32" y="9" width="13" height="1"></rect>
            <rect x="32" y="17" width="13" height="1"></rect>
            <rect x="32" y="19" width="13" height="1"></rect>
            <path d="M32 21H45V24C45 24.5523 44.5523 25 44 25H32V21Z"></path>
            <rect x="5" y="21" width="12" height="4"></rect>
            <path d="M18 21H31V25H18V21Z"></path>
        </g>
    </svg>`,
        className: "", desc: "Left-to-right hierarchy with D3",
    },
    Sunburst_partition: {
        value_2: `import {Sunburst} from "${GUIDE_LINK_URI}sunburst/sunburst"`,
        value_2Param: "Sunburst",
        codeData: {
            value: `Sunburst(flare, {
    path: (d) => d.name.replaceAll(".", "/"), // e.g. flare/animate/Easing
    label: (d) => d.name.split(".").pop(), // display text
    value: (d) => d?.size, // angle of each arc
    title: (d, n) => [n.id, n.value.toLocaleString()].join("\\n"), // hover text
    width,
    height: 720
})` },
        title: "Sunburst partition",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="currentColor"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path fill-opacity="0.35"
            d="M23.9648 12.1524L21.9997 8.67561C22.7855 8.30283 23.6335 8.07995 24.5 8.0179V12.0116C24.3177 12.0421 24.1386 12.0892 23.9648 12.1524Z M24.5 7.01473C23.4609 7.07979 22.4438 7.34711 21.5061 7.80229L19.5357 4.31631C20.0106 4.07343 20.4998 3.86315 21 3.68631C22.1242 3.28884 23.3038 3.06032 24.5 3.01044V7.01473Z M30.1374 21.1337L32.8953 24.0368C32.072 24.7562 31.1554 25.3586 30.1716 25.8284C29.6279 26.0881 29.0637 26.3073 28.4834 26.4833C26.595 27.0562 24.5944 27.1544 22.6589 26.7694C20.7234 26.3844 18.9128 25.5281 17.3873 24.2761C17.2123 24.1325 17.0417 23.9843 16.8756 23.8315L19.7071 21C19.7783 21.0628 19.8507 21.1244 19.9243 21.1848C20.9414 22.0195 22.1486 22.5905 23.4391 22.8472C24.6441 23.0869 25.8869 23.0456 27.0713 22.7282C27.1176 22.7158 27.1637 22.703 27.2098 22.6897C27.2474 22.6789 27.285 22.6678 27.3225 22.6564C28.3561 22.3429 29.3131 21.8241 30.1374 21.1337Z M26.6958 17.511L29.4464 20.4064C28.7363 20.9905 27.9161 21.4304 27.032 21.6986C26.7691 21.7783 26.5025 21.8423 26.2336 21.8905C25.3756 22.0441 24.494 22.0365 23.6344 21.8655C22.5053 21.6409 21.4491 21.1414 20.5593 20.4111C20.5111 20.3716 20.4635 20.3314 20.4165 20.2907L23.2404 17.4667C23.5891 17.7154 23.9873 17.8879 24.4089 17.9718C24.8976 18.069 25.4027 18.0442 25.8796 17.8995C26.1706 17.8112 26.4456 17.68 26.6958 17.511Z M21.9927 14.6304C21.9897 14.6546 21.987 14.6788 21.9846 14.703C21.9358 15.1989 22.01 15.6992 22.2007 16.1595C22.2888 16.3723 22.4006 16.5735 22.5333 16.7596L19.7093 19.5836C19.2192 19.0178 18.8213 18.3751 18.5328 17.6788C18.0923 16.6153 17.9209 15.4595 18.0337 14.3139C18.0609 14.0381 18.1043 13.765 18.1635 13.4959L21.9927 14.6304Z M22.2768 13.6716C22.3365 13.5492 22.4046 13.4306 22.4807 13.3167C22.6508 13.0621 22.8579 12.8357 23.0942 12.6444L21.1291 9.16767C21.0857 9.19646 21.0427 9.22572 21 9.25545C20.2847 9.75353 19.6666 10.3823 19.1797 11.111C18.8808 11.5585 18.6355 12.0371 18.4476 12.5371L22.2768 13.6716Z M17.4858 12.2521C17.7036 11.6563 17.9924 11.0864 18.3475 10.5549C18.9506 9.65233 19.7297 8.88393 20.6355 8.29435L18.6652 4.80836C17.2163 5.70891 15.9735 6.90978 15.0224 8.33317C14.4431 9.20009 13.9815 10.1354 13.6465 11.1145L17.4858 12.2521Z M13.3624 12.0733C13.2182 12.6467 13.1161 13.2317 13.0578 13.8238C12.8644 15.7877 13.1583 17.769 13.9134 19.5922C14.4538 20.8968 15.2184 22.0916 16.1685 23.1244L19 20.2929C18.4181 19.6333 17.947 18.8799 17.6081 18.0618C17.1046 16.8462 16.9086 15.5252 17.0376 14.2158C17.071 13.8767 17.1259 13.5411 17.2017 13.2109L13.3624 12.0733Z">
        </path>
        <path
            d="M25.5 3.01044V7.01473C26.6847 7.0889 27.8376 7.42579 28.8749 8.00002C28.9168 8.02321 28.9585 8.04678 29 8.07074C29.151 8.15792 29.2995 8.2502 29.4451 8.34749C30.7355 9.20975 31.7468 10.4283 32.3565 11.8541L36.1589 10.5866C36.1353 10.5269 36.1112 10.4673 36.0866 10.4078C35.1783 8.2151 33.6402 6.34096 31.6668 5.02238C30.8325 4.46488 29.9361 4.01728 29 3.68631C27.8778 3.28956 26.6984 3.0604 25.5 3.01044Z M36.4877 11.5311L32.6915 12.7965C32.9952 13.8564 33.0769 14.9692 32.9298 16.0647L36.8758 16.7224C37.1281 14.9823 36.9947 13.2101 36.4877 11.5311Z M36.6911 17.7054L32.7349 17.046C32.7327 17.0545 32.7304 17.0629 32.7282 17.0713C32.7158 17.1176 32.7029 17.1638 32.6897 17.2098C32.3434 18.4149 31.7181 19.5237 30.8624 20.445L33.6203 23.3481C34.5276 22.4112 35.2716 21.3374 35.8284 20.1716C36.2033 19.3866 36.4933 18.56 36.6911 17.7054Z M25.5 8.0179V12.0116C25.9214 12.0821 26.325 12.2413 26.6834 12.4807C27.1816 12.8136 27.57 13.2869 27.7993 13.8405C28.0286 14.3942 28.0887 15.0034 27.9717 15.5911C27.8826 16.0395 27.6935 16.4599 27.4208 16.8222L30.1714 19.7177C31.0296 18.777 31.6163 17.6184 31.8655 16.3657C31.8742 16.3217 31.8826 16.2777 31.8904 16.2336C32.1262 14.9168 31.9798 13.5589 31.4672 12.3212C30.9545 11.0836 30.0978 10.0199 29 9.25546C28.9633 9.22988 28.9263 9.20463 28.889 9.17973C27.8769 8.50346 26.7084 8.10442 25.5 8.0179Z">
        </path>
    </svg>`,
        className: "", desc: "Radial hierarchy with D3",
    },
    Treemap: {
        value_2: `import {Treemap} from "${GUIDE_LINK_URI}treemap/treemap"`,
        value_2Param: "Treemap",
        codeData: {
            value: `Treemap(flare, {
    path: (d) => d.name.replaceAll(".", "/"), // e.g. flare/animate/Easing
    label: (d) => d.name.split(".").pop(), // display text
    group: (d) => d.name.split(".")[1], // for color; e.g. animate
    value: (d) => d?.size, // area of each rect
    title: (d, n) => [n.id, n.value.toLocaleString()].join("\\n"), // hover text
    width,
    height: 500
})` },
        title: "Treemap",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="currentColor"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="5" y="5" width="20" height="12"></rect>
        <g fill-opacity="0.35">
            <rect x="26" y="5" width="18" height="7"></rect>
            <rect x="26" y="13" width="9" height="7"></rect>
            <rect x="36" y="13" width="8" height="7"></rect>
            <rect x="5" y="18" width="20" height="7"></rect>
            <rect x="26" y="21" width="12" height="4"></rect>
            <rect x="39" y="21" width="5" height="4"></rect>
        </g>
    </svg>`,
        className: "", desc: "Nested hierarchy with D3",
    },
    Force_directed_graph: {
        value_2: `import {ForceGraph} from "${GUIDE_LINK_URI}force-directed-graph/force-directed-graph"`,
        value_2Param: "ForceGraph",
        codeData: {
            value: `ForceGraph(miserables, {
    nodeId: (d) => d.id, // node identifier, to match links
    nodeGroup: (d) => d.group, // group identifier, for color
    nodeTitle: (d) => d.id, // hover text
    width,
    height: 520,
    invalidation // stop when the cell is re-run
})` },
        title: "Force-directed graph",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="currentColor"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path fill-opacity="0.35"
            d="M9.45007 9.62691L14.221 13.8678C14.4808 13.2308 14.9531 12.7032 15.5499 12.3731L10.779 8.13226C10.5192 8.76917 10.0469 9.29678 9.45007 9.62691Z M19.6441 13.5815C19.8712 14.0038 20 14.4869 20 15C20 15.1908 19.9822 15.3773 19.9482 15.5582L27.3559 14.4186C27.1288 13.9962 27 13.5131 27 13C27 12.8093 27.0178 12.6227 27.0518 12.4418L19.6441 13.5815Z M32.0826 10.8407C32.5645 11.3055 32.8912 11.9302 32.9773 12.6294L37.9174 10.1593C37.4355 9.69448 37.1088 9.06979 37.0227 8.37064L32.0826 10.8407Z M41.4899 10.6045C41.051 10.8561 40.5423 11 40 11C39.8374 11 39.6778 10.9871 39.5222 10.9622L40.5101 16.3956C40.9491 16.1439 41.4577 16 42 16C42.1626 16 42.3222 16.0129 42.4778 16.0379L41.4899 10.6045Z M39.4982 20.6562C39.1834 20.1816 39 19.6122 39 19C39 18.9042 39.0045 18.8094 39.0133 18.7159L32.5018 20.3438C32.8166 20.8184 33 21.3878 33 22C33 22.0958 32.9955 22.1906 32.9867 22.2841L39.4982 20.6562Z M29 19.1707C29.3128 19.0602 29.6494 19 30 19C30.3506 19 30.6872 19.0602 31 19.1707V15.8293C30.6872 15.9399 30.3506 16 30 16C29.6494 16 29.3128 15.9399 29 15.8293V19.1707Z M32.0826 15.1593L39.0227 18.6294C39.1088 17.9302 39.4355 17.3055 39.9174 16.8407L32.9773 13.3706C32.8912 14.0698 32.5645 14.6945 32.0826 15.1593Z M15.7066 17.7076C15.0888 17.412 14.588 16.9112 14.2924 16.2934L11.2934 19.2924C11.9112 19.588 12.412 20.0888 12.7076 20.7066L15.7066 17.7076Z">
        </path>
        <circle cx="8" cy="7" r="2"></circle>
        <circle cx="17" cy="15" r="2"></circle>
        <circle cx="10" cy="22" r="2"></circle>
        <circle cx="30" cy="13" r="2"></circle>
        <circle cx="42" cy="19" r="2"></circle>
        <circle cx="30" cy="22" r="2"></circle>
        <circle cx="40" cy="8" r="2"></circle>
    </svg>`,
        className: "", desc: "Node-link diagram with D3",
    },
    Graphviz: {
        codeData: {
            value: `dot\`digraph g {
    rankdir = LR;
    a -> b -> c;
}\`` },
        title: "Graphviz",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <g fill="currentColor">
            <path
                d="M19.7071 15.7071C19.3166 15.3166 19.3166 14.6834 19.7071 14.2929L24.2929 9.70711C24.6834 9.31658 25.3166 9.31658 25.7071 9.70711L30.2929 14.2929C30.6834 14.6834 30.6834 15.3166 30.2929 15.7071L25.7071 20.2929C25.3166 20.6834 24.6834 20.6834 24.2929 20.2929L19.7071 15.7071Z">
            </path>
            <rect x="39" y="5" width="8" height="8" rx="2"></rect>
            <rect x="39" y="17" width="8" height="8" rx="2"></rect>
            <rect x="3.5" y="11.5" width="7" height="7" rx="1.5" stroke="currentColor"></rect>
        </g>
        <path stroke="currentColor" stroke-opacity="0.35" stroke-width="2"
            d="M38 21L36 21C35.4477 21 35 20.5523 35 20L35 15M35 15L32 15M35 15L35 10C35 9.44772 35.4477 9 36 9L38 9 M12 15L18 15">
        </path>
    </svg>`,
        className: "", desc: "Node-link diagram with DOT",
    },
    Mermaid: {
        codeData: {
            value: `mermaid\`graph LR
a-->b
b-->c\`` },
        title: "Mermaid",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <g fill="currentColor">
            <path
                d="M19.7071 15.7071C19.3166 15.3166 19.3166 14.6834 19.7071 14.2929L24.2929 9.70711C24.6834 9.31658 25.3166 9.31658 25.7071 9.70711L30.2929 14.2929C30.6834 14.6834 30.6834 15.3166 30.2929 15.7071L25.7071 20.2929C25.3166 20.6834 24.6834 20.6834 24.2929 20.2929L19.7071 15.7071Z">
            </path>
            <rect x="39" y="5" width="8" height="8" rx="2"></rect>
            <rect x="39" y="17" width="8" height="8" rx="2"></rect>
            <rect x="3.5" y="11.5" width="7" height="7" rx="1.5" stroke="currentColor"></rect>
        </g>
        <path stroke="currentColor" stroke-opacity="0.35" stroke-width="2"
            d="M38 21L36 21C35.4477 21 35 20.5523 35 20L35 15M35 15L32 15M35 15L35 10C35 9.44772 35.4477 9 36 9L38 9 M12 15L18 15">
        </path>
    </svg>`,
        className: "", desc: "Node-link diagram with Mermaid",
    },
}
const Text_OPTIONS = {
    Heading_1: {
        codeData: {
            codeMode: CodeMode.markdown,
            value: `# heading`,
        },
        title: "Heading 1",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path
            d="M25.2607 21V8.31738H23.002V13.5908H16.832V8.31738H14.5645V21H16.832V15.498H23.002V21H25.2607ZM31.2197 21H33.4873V8.31738H31.2285L27.915 10.6465V12.7822L31.0703 10.5498H31.2197V21Z"
            fill="currentColor"></path>
    </svg>`,
        className: "", desc: "Main headline",
    },
    Heading_2: {
        codeData: {
            codeMode: CodeMode.markdown,
            value: `## heading`,
        },
        title: "Heading 2",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path
            d="M24.5361 20V10.1357H22.7793V14.2373H17.9805V10.1357H16.2168V20H17.9805V15.7207H22.7793V20H24.5361ZM26.6963 13.0547H28.3506C28.3506 12.0156 29.0342 11.3047 30.1074 11.3047C31.1396 11.3047 31.7549 11.9951 31.7549 12.8906C31.7549 13.6426 31.4541 14.1143 30.21 15.3721L26.792 18.8105V20H33.6416V18.5371H29.1846V18.4209L31.3037 16.3359C32.999 14.6748 33.498 13.875 33.498 12.7266C33.498 11.0859 32.165 9.88965 30.2031 9.88965C28.1455 9.88965 26.6963 11.1885 26.6963 13.0547Z"
            fill="currentColor"></path>
    </svg>`,
        className: "", desc: "Section headline",
    },
    Heading_3: {
        codeData: {
            codeMode: CodeMode.markdown,
            value: `### heading`,
        },
        title: "Heading 3",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path
            d="M24.4927 19V11.2495H23.1123V14.4722H19.3418V11.2495H17.9561V19H19.3418V15.6377H23.1123V19H24.4927ZM28.0483 15.541H29.0205C29.9766 15.541 30.5566 16.019 30.5566 16.7979C30.5566 17.5498 29.9282 18.0547 29.0366 18.0547C28.1396 18.0547 27.5327 17.5928 27.4736 16.873H26.147C26.2114 18.2749 27.3662 19.1934 29.0527 19.1934C30.7285 19.1934 31.9746 18.2266 31.9746 16.8945C31.9746 15.8364 31.314 15.1543 30.2612 14.9932V14.9019C31.083 14.6816 31.6685 14.064 31.6738 13.1294C31.6792 12.0176 30.75 11.0562 29.1011 11.0562C27.4199 11.0562 26.3672 12.0068 26.2974 13.3657H27.5972C27.6562 12.5923 28.1826 12.1626 29.0259 12.1626C29.8477 12.1626 30.3364 12.6675 30.3364 13.3066C30.3364 14.0103 29.7886 14.5044 28.9937 14.5044H28.0483V15.541Z"
            fill="currentColor"></path>
    </svg>`,
        className: "", desc: "Subhead",
    },
    Bulleted_list: {
        codeData: {
            codeMode: CodeMode.markdown,
            value: `- text`,
        },
        title: "Bulleted list",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="currentColor"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <g fill-opacity="0.35">
            <rect x="16" y="3" width="23" height="6" rx="1"></rect>
            <rect x="16" y="12" width="16" height="6" rx="1"></rect>
            <rect x="16" y="21" width="26" height="6" rx="1"></rect>
        </g>
        <circle cx="10" cy="6" r="2"></circle>
        <circle cx="10" cy="15" r="2"></circle>
        <circle cx="10" cy="24" r="2"></circle>
    </svg>`,
        className: "", desc: "Unordered series of items",
    },
    Numbered_list: {
        codeData: {
            codeMode: CodeMode.markdown,
            value: `1. text`,
        },
        title: "Numbered list",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="currentColor"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <g fill-opacity="0.35">
            <rect x="16" y="3" width="23" height="6" rx="1"></rect>
            <rect x="16" y="12" width="16" height="6" rx="1"></rect>
            <rect x="16" y="21" width="26" height="6" rx="1"></rect>
        </g>
        <path
            d="M8.89062 9H10.5469V3.36328H8.89062L7.47656 4.30859V5.69531L8.8125 4.8125H8.89062V9ZM7.41406 14.1953H8.92188C8.92188 13.7617 9.19141 13.4336 9.66406 13.4336C10.1055 13.4336 10.3359 13.7188 10.3359 14.043C10.3359 14.3281 10.25 14.5547 9.55859 15.1445L7.51172 16.8984V18H11.9922V16.7344H9.71484V16.6562L10.7734 15.75C11.6367 15.0117 11.9336 14.5781 11.9336 13.9062C11.9336 12.9219 11.1523 12.207 9.74609 12.207C8.35547 12.207 7.41406 13.0039 7.41406 14.1953ZM9.04297 23.6562H9.77344C10.25 23.6562 10.5391 23.8828 10.5391 24.2656C10.5391 24.6289 10.2344 24.8711 9.77734 24.8711C9.29297 24.8711 8.99219 24.6328 8.98438 24.25H7.40625C7.45703 25.4062 8.40234 26.1562 9.80469 26.1562C11.2305 26.1562 12.2227 25.4648 12.2227 24.4414C12.2227 23.7109 11.7578 23.207 10.918 23.0938V23.0156C11.5508 22.875 11.9922 22.4297 11.9922 21.7461C11.9922 20.8906 11.2422 20.207 9.85938 20.207C8.46094 20.207 7.55859 20.9297 7.55078 22.0664H9.04688C9.0625 21.6641 9.32812 21.418 9.78516 21.418C10.2109 21.418 10.4688 21.6523 10.4688 21.9883C10.4688 22.3398 10.207 22.5781 9.78125 22.5781H9.04297V23.6562Z">
        </path>
    </svg>`,
        className: "", desc: "Ordered series of items",
    },
    Blockquote: {
        codeData: {
            codeMode: CodeMode.markdown,
            value: `> text`,
        },
        title: "Blockquote",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="currentColor"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="3" y="3" width="5" height="6" rx="1" fill-opacity="0.35"></rect>
        <rect x="8" y="12" width="15" height="6" rx="1" fill="currentColor"></rect>
        <rect x="3" y="21" width="14" height="6" rx="1" fill-opacity="0.35"></rect>
        <rect x="27" y="3" width="14" height="6" rx="1" fill="currentColor" fill-opacity="0.35">
        </rect>
        <rect x="43" y="3" width="4" height="6" rx="1" fill-opacity="0.35"></rect>
        <rect x="10" y="3" width="15" height="6" rx="1" fill-opacity="0.35"></rect>
        <rect x="25" y="12" width="4" height="6" rx="1"></rect>
        <rect x="31" y="12" width="5" height="6" rx="1"></rect>
        <rect x="19" y="21" width="26" height="6" rx="1" fill-opacity="0.35"></rect>
    </svg>`,
        className: "", desc: "Indented text",
    },
    Image: {
        codeData: {
            codeMode: CodeMode.markdown,
            value: `![text](url)`,
        },
        title: "Image",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path
            d="M11.2915 19.958L6 23V24H44V21.7428C44 20.8583 43.6097 20.0189 42.9333 19.4488L38.4286 15.6522L36.0789 13.5985C35.0125 12.6665 33.4395 12.6072 32.306 13.4564L25.8929 18.2609L21.9998 21.0448C21.2351 21.5917 20.2583 21.7484 19.3609 21.4683L13.6806 19.6952C12.8824 19.446 12.0164 19.5413 11.2915 19.958Z"
            fill="currentColor" stroke="currentColor" stroke-width="2" stroke-linecap="round"
            stroke-linejoin="round"></path>
        <rect x="3.5" y="3.5" width="43" height="23" rx="1.5" stroke="currentColor"
            stroke-opacity="0.35"></rect>
        <circle cx="16" cy="12" r="4" fill="currentColor"></circle>
    </svg>`,
        className: "", desc: "Display a picture or figure",
    },
    Code_block: {
        codeData: {
            codeMode: CodeMode.markdown,
            value: `~~~

~~~`,
        },
        title: "Code block",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="4" y="4" width="42" height="6" fill="currentColor" fill-opacity="0.35"></rect>
        <rect x="3.5" y="3.5" width="43" height="23" rx="1.5" stroke="currentColor"
            stroke-opacity="0.35"></rect>
        <path d="M28 13L32 17L28 21" stroke="currentColor" stroke-width="2"
            stroke-linecap="round" stroke-linejoin="round"></path>
        <path d="M22 21L18 17L22 13" stroke="currentColor" stroke-width="2"
            stroke-linecap="round" stroke-linejoin="round"></path>
    </svg>`,
        className: "", desc: "Preformatted monospaced text",
    },
    Horizontal_rule: {
        codeData: {
            codeMode: CodeMode.markdown,
            value: `---`,
        },
        title: "Horizontal rule",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="5" y="14" width="40" height="2" rx="1" fill="currentColor"></rect>
    </svg>`,
        className: "", desc: "Section divider",
    },
    Mathematical_formula: {
        codeData: {
            codeMode: CodeMode.tex,
            value: `c = \pm\sqrt{a^2 + b^2}`,
        },
        title: "Mathematical formula",
        icon: `<div class="tools-select-icon thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"><i class="${getCodeModeIcon(CodeMode.tex)}"></i></div>`,
        className: "", desc: "Typeset math with TeX",
    },
}

const Graphics_OPTIONS = {
    D3_starter: {
        codeData: {
            value: `{param} = {
    const width = 640;
    const height = 400;
    const margin = {top: 20, right: 30, bottom: 30, left: 40};
    
    const x = d3.scaleUtc()
        .domain(d3.extent(aapl, d => d.Date))
        .range([margin.left, width - margin.right]);
    
    const y = d3.scaleLinear()
        .domain([0, d3.max(aapl, d => d.Close)])
        .range([height - margin.bottom, margin.top]);
    
    const svg = d3.create("svg")
        .attr("width", width)
        .attr("height", height);
    
    svg.append("g")
        .attr("transform", \`translate(0,\${height - margin.bottom})\`)
        .call(d3.axisBottom(x));
    
    svg.append("g")
        .attr("transform", \`translate(\${margin.left},0)\`)
        .call(d3.axisLeft(y));
    
    svg.append("path")
        .attr("fill", "none")
        .attr("stroke", "steelblue")
        .attr("stroke-width", 1.5)
        .attr("d", d3.line()
            .x(d => x(d.Date))
            .y(d => y(d.Close))
            (aapl));
    
    return svg.node();
}` },
        firstParam: "chart",
        title: "D3 starter",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path
            d="M15 5H16.776C19.5414 5 22.1935 6.00089 24.149 7.78249C26.1044 9.56408 27.203 11.9804 27.203 14.5C27.203 17.0196 26.1044 19.4359 24.149 21.2175C22.1935 22.9991 19.5414 24 16.776 24H15V19.8242H16.776C18.3259 19.8242 19.8122 19.2632 20.9081 18.2648C22.004 17.2663 22.6197 15.9121 22.6197 14.5C22.6197 13.0879 22.004 11.7337 20.9081 10.7352C19.8122 9.73676 18.3259 9.17582 16.776 9.17582H15V5ZM23.3074 5H30.6406C31.849 4.99999 33.0323 5.31362 34.052 5.90417C35.0718 6.49472 35.8857 7.33774 36.3986 8.33453C36.9115 9.33132 37.1021 10.4406 36.9481 11.5325C36.7941 12.6244 36.3018 13.6538 35.5289 14.5C36.3018 15.3462 36.7941 16.3756 36.9481 17.4675C37.1021 18.5594 36.9115 19.6687 36.3986 20.6655C35.8857 21.6623 35.0718 22.5053 34.052 23.0958C33.0323 23.6864 31.849 24 30.6406 24H23.3074C25.1119 22.9702 26.5921 21.5313 27.6034 19.8242H30.6404C30.8736 19.8242 31.1046 19.7823 31.3201 19.701C31.5355 19.6197 31.7313 19.5005 31.8962 19.3502C32.0612 19.2 32.192 19.0216 32.2812 18.8253C32.3705 18.629 32.4164 18.4185 32.4164 18.206C32.4164 17.9935 32.3705 17.7831 32.2812 17.5868C32.192 17.3905 32.0612 17.2121 31.8962 17.0619C31.7313 16.9116 31.5355 16.7924 31.3201 16.7111C31.1046 16.6298 30.8736 16.5879 30.6404 16.5879H28.8644C29.1515 15.2083 29.1515 13.7917 28.8644 12.4121H30.6404C30.8736 12.4121 31.1046 12.3702 31.3201 12.2889C31.5355 12.2076 31.7313 12.0884 31.8962 11.9381C32.0612 11.7879 32.192 11.6095 32.2812 11.4132C32.3705 11.2169 32.4164 11.0065 32.4164 10.794C32.4164 10.5815 32.3705 10.371 32.2812 10.1747C32.192 9.9784 32.0612 9.80002 31.8962 9.64976C31.7313 9.49951 31.5355 9.38032 31.3201 9.299C31.1046 9.21768 30.8736 9.17582 30.6404 9.17582H27.6034C26.5921 7.46865 25.1119 6.02981 23.3074 5V5Z"
            fill="currentColor"></path>
    </svg>`,
        className: "", desc: "Scales, axes, margins, and a line",
    },
    SVG: {
        codeData: {
            codeMode: CodeMode.htmlmixed,
            value: `<svg width=\${width} height="200" viewBox="0 0 \${width} 200">
    <rect width="100%" height="100%" fill="hsl(216deg 100% 13%)" />
</svg>` },
        title: "SVG",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path
            d="M35 12.2308C35 15.5 29 20.0769 25 24C21 20.0769 15 15.5 15 12.2308C15 8.96154 17 7 19.6667 7C22.3333 7 24.3333 9.61538 25 10.9231C25.6667 9.61538 27.6667 7 30.3333 7C33 7 35 8.96154 35 12.2308Z"
            stroke="currentColor" fill="none" stroke-width="2"></path>
    </svg>`,
        className: "", desc: "Expressive declarative graphics",
    },
    Canvas: {
        codeData: {
            value: `{param} = {
    const context = DOM.context2d(width, 200);
    context.fillStyle = "hsl(216deg 100% 13%)";
    context.fillRect(0, 0, width, 200);
    return context.canvas;
}` },
        firstParam: "canvas",
        title: "Canvas",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <rect x="30" y="16" width="9" height="9" rx="1" stroke="currentColor" fill="none"
            stroke-width="2"></rect>
        <circle cx="25.5" cy="8.5" r="5.5" fill="currentColor" fill-opacity="0.35"></circle>
        <path
            d="M16.433 12.75L21.6292 21.75C21.8216 22.0833 21.5811 22.5 21.1962 22.5H10.8038C10.4189 22.5 10.1784 22.0833 10.3708 21.75L15.567 12.75C15.7594 12.4167 16.2406 12.4167 16.433 12.75Z"
            fill="currentColor" stroke="#currentColor"></path>
    </svg>`,
        className: "", desc: "Low-level, high-performance",
    },
}
const Imports_OPTIONS = {
    Import_notebook: {
        codeData: {
            value: `import {cell} from "@user/notebook"`
        },
        title: "Import notebook",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path d="M22 21.5858L25 24.5858L28 21.5858" stroke="currentColor" stroke-width="2">
        </path>
        <path d="M25 16L25 24" stroke="currentColor" stroke-width="2"></path>
        <path
            d="M19 7C19 5.89543 19.8954 5 21 5H27L31 9V17C31 18.1046 30.1046 19 29 19H27V17H29V10H26V7H21V17H23V19H21C19.8954 19 19 18.1046 19 17V7Z"
            fill="currentColor"></path>
    </svg>`,
        className: "", desc: "Reuse any code on Observable",
    },
    Import_library: {
        codeData: {
            value: `{param} = import("https://cdn.skypack.dev/library@version")`
        },
        firstParam: "library",
        title: "Import library",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path fill-rule="evenodd" clip-rule="evenodd"
            d="M9 5H17V13H15V7H13V13H9V5Z M29 5H41V13H39V7H37V13H35V7H33V13H29V5Z M27 5H19V15H23V13H27V5ZM25 7H23V11H25V7Z"
            fill="currentColor"></path>
        <path d="M22 21.5858L25 24.5858L28 21.5858 M25 16L25 24" stroke="currentColor"
            stroke-width="2"></path>
    </svg>`,
        className: "", desc: "Load modern JS libraries from npm",
    },
    Require_library: {
        codeData: {
            value: `{param} = require("library@version")`
        },
        firstParam: "library",
        title: "Require library",
        icon: `<svg width="50" height="30" viewBox="0 0 50 30" fill="none"
        class="jsx-fba29814d32d7d5f thumbnail mid-gray ml2 br2 bg-near-white highlight-blue flex-shrink-0"
        style="width: 50px; height: 30px;">
        <path fill-rule="evenodd" clip-rule="evenodd"
            d="M9 5H17V13H15V7H13V13H9V5Z M29 5H41V13H39V7H37V13H35V7H33V13H29V5Z M27 5H19V15H23V13H27V5ZM25 7H23V11H25V7Z"
            fill="currentColor"></path>
        <path d="M22 21.5858L25 24.5858L28 21.5858 M25 16L25 24" stroke="currentColor"
            stroke-width="2"></path>
    </svg>`,
        className: "", desc: "Load legacy JS libraries from npm",
    },
}
export default class ToolsSelect extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        position: PropTypes.object,
        editor: PropTypes.object,
    };
    static defaultProps = {};
    constructor(props) {
        super(props);
        this.state = {
            position: props.position,
            /**
             * @type {function}
            * type?: string,
            * data?: BlockToolData,
            * config?: ToolConfig,
            * needToFocus?: boolean,
            */
            cb: props.cb,
            /**
             * @type {function}
            */
            selectFileFunc: props.selectFileFunc,
            /**@type{TagEditor} */
            editor: props.editor,
            searchValue: "",
            selectedKeyIndex: 0,
            visible: false,
        }
        this.ref = React.createRef();
        this.inputRef = React.createRef();
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    componentDidUpdate(preProps, preState) {
        let state = {};
        if (preProps.position !== this.props.position) {
            _.assign(state, { position: this.props.position });
        }
        if (preProps.cb !== this.props.cb) {
            _.assign(state, { cb: this.props.cb });
        }
        if (preProps.editor !== this.props.editor) {
            _.assign(state, { editor: this.props.editor });
        }
        !_.isEmpty(state) && this.setState(state, () => {
            if (state.position) {
                this.setState({ visible: true }, () => {
                    this.inputRef.current && this.inputRef.current.focus();
                })
            }
        });
    }

    /**
     * 
     * @param {string|RegExp} searchValue 
     * @param {string} content 
     */
    highligthComp = (searchValue, content) => {
        if (!content || !searchValue) {
            return <span>{content}</span>
        }
        return _.map(searchFunc(content, new RegExp(escapeStringRegexp(searchValue), "gi")), (obj, index) => {
            const { start, end, flag } = obj;
            return <span key={index} className={flag ? "highlight-warning" : ""}>{content.substring(start, end)}</span>
        })
    }

    menuFunc = (v, key, display = false) => {
        const { editor, searchValue } = this.state;
        let searchLower = searchValue.toLowerCase();
        let titleLower = v.title.toLowerCase();
        let visible = display || searchLower === "" || ~titleLower.indexOf(searchLower);
        return {
            key,
            visible,
            onClick: async (info) => {
                const { cb, selectFileFunc } = this.state;
                let config = {};
                if (undefined !== v.codeData) {
                    let codeData = _.cloneDeep(v.codeData);
                    (undefined !== v.firstParam) && (codeData.value = replaceParam(editor, v.firstParam, codeData.value))
                    _.assign(config, { codeData: codeData });
                }
                if (v.processFile && selectFileFunc) {
                    let uploadFileCb = (fileNames) => {
                        if (fileNames && fileNames.length >= 1) {
                            let fileName = fileNames[0];
                            let fileData = editor.getFileDataByKey(getFileKey(getFileKey(editor.getData().fileKey, FILESDIR), fileName))
                            config.codeData.value = getFileAttachmentContent(editor, fileData);
                            cb(v.toolName || "codeTool", config, undefined, true);
                            v.cb && v.cb(editor.editorInstance);
                        }
                    }
                    selectFileFunc(uploadFileCb);
                } else {
                    cb(v.toolName || "codeTool", config, undefined, true);
                    v.cb && v.cb(editor.editorInstance);
                    if (v.value_2) {
                        const { variables } = editor.getMain()._cachedata;
                        if (!v.value_2Param || !variables[v.value_2Param]) {
                            let api = editor.editorInstance;
                            let index = api.blocks.getCurrentBlockIndex();
                            api.blocks.insert("codeTool", {
                                "codeData": {
                                    "pinCode": true,
                                    "value": v.value_2
                                }
                            }, undefined, index + 1, true);
                            api.caret.setToBlock(index + 1, 'start');
                            api.toolbar.rightOpen();
                        }
                    }
                }
            },
            item: <div className={visible ? "" : "hide"} key={key}>
                <div className="d-flex align-items-center" title={v.desc}>
                    <i className={v.className} dangerouslySetInnerHTML={{ __html: v.icon }} />
                    {this.highligthComp(searchValue, v.title)}
                    {v.shortcut ? ` (${v.shortcut.replace("CMD", "Ctrl")})` : ""}
                </div>
            </div>
        }
    }

    render() {
        const { visible, position, editor, searchValue, selectedKeyIndex } = this.state;
        let menusObj = {};
        let menusTitles = {};//flag if title display
        if (!!(editor && editor.isReady())) {
            let tmenuFunc = (title) => {
                return (v, k, collection) => {
                    let titleDisplay = !!~title.toLowerCase().indexOf(searchValue.toLowerCase());
                    if (!menusTitles[title]) {
                        if (titleDisplay) {
                            menusTitles[title] = true;
                        } else if (!!~v.title.toLowerCase().indexOf(searchValue.toLowerCase())) {
                            menusTitles[title] = true;
                        }
                    }
                    return this.menuFunc(v, k, titleDisplay)
                }
            };
            let addTitleFunc = (title, options, noTitle) => {
                let arr = [];
                let childrenMenus = _.map(options, tmenuFunc(title));
                !noTitle && arr.push({
                    key: title,
                    visible: menusTitles[title],
                    disabled: true,
                    item: <div key={title} style={{ cursor: "default" }} className={`text-muted
                ${menusTitles[title] ? "" : "hide"}
                `} > {this.highligthComp(searchValue, title.firstUpperCase())}  </div>
                });
                arr.push(...childrenMenus);
                return arr;
            }
            menusObj.common = addTitleFunc("common", _.assign({}, toolsOptions(editor, PREV_TOOL_OPTIONS), PREV_OPTIONS_2), true);
            menusObj.data = addTitleFunc("data", _.reduce(DATA_OPTIONS, (prev, curr, key) => {
                if (!(curr.processFile && !editor.getData())) {
                    prev[key] = curr;
                }
                return prev;
            }, {}))
            menusObj.tables = addTitleFunc("tables", _.assign(toolsOptions(editor, TABLE_TOOL_OPTIONS), TABLES_OPTIONS));
            menusObj.inputs = addTitleFunc("inputs", INPUTS_OPTIONS)
            menusObj.plot = addTitleFunc("plot", PLOT_OPTIONS)
            menusObj.maps = addTitleFunc("maps", MAPS_OPTIONS)
            menusObj['trees-and-networks'] = addTitleFunc("trees-and-networks", Trees_and_Networks_OPTIONS)
            menusObj.text = addTitleFunc("text", Text_OPTIONS)
            menusObj.graphics = addTitleFunc("graphics", Graphics_OPTIONS)
            menusObj.imports = addTitleFunc("imports", Imports_OPTIONS)
        }
        let menuItems = _.reduce(menusObj, (prev, v, k) => {
            _.each(v, (value, index) => {
                if (value.visible) {
                    prev[value.key] = {
                        onClick: value.onClick,
                        option: {
                            value: value.key,
                            label: value.item,
                            disabled: value.disabled,
                        }
                    };
                }
            })
            return prev;
        }, {});
        return <div className={`${visible ? "" : "hide"} tools-select`}>
            <div style={checkIsMobile() ? { left: 0, top: `calc(${window.pageYOffset}px + 100vh - 256px)` } : position}>
                <Select ref={this.inputRef} allowClear
                    size={"small"}
                    autoFocus={visible}
                    defaultActiveFirstOption={visible}
                    open={visible}
                    showSearch
                    listHeight={256}
                    style={checkIsMobile() ? { width: "90vw" } : {
                        width: "220px",
                    }}
                    value={""}
                    placeholder="Select a tool"
                    optionFilterProp="children"
                    onChange={(value) => {
                        menuItems[value].onClick();
                        this.setState({ visible: false, searchValue: "" });
                    }}
                    onSearch={(value) => {
                        this.setState({ searchValue: value })
                    }}
                    filterOption={(input, option) =>
                        (option?.value ?? '').toLowerCase().includes(input.toLowerCase())
                    }
                    onBlur={() => {
                        this.setState({ visible: false, searchValue: "" });
                    }}
                    onInputKeyDown={(e) => {
                        if (~["Escape"].indexOf(e.key)) {//TODO
                            this.setState({ visible: false, searchValue: "" })
                        } else if (~["Enter"].indexOf(e.key)) {//TODO
                            let activeOption = document.querySelector(".ant-select-dropdown:not(.ant-select-dropdown-hidden) .ant-select-item.ant-select-item-option.ant-select-item-option-active");
                            if (!activeOption) {
                                let firstLi = document.querySelector(".ant-select-dropdown:not(.ant-select-dropdown-hidden) .ant-select-item.ant-select-item-option:not(.ant-select-item-option-disabled)");
                                firstLi && firstLi.click();
                            }
                        }
                    }}
                    options={_.reduce(menuItems, (prev, curr) => {
                        prev.push(curr.option);
                        return prev;
                    }, [])} />
            </div>
        </div >
    }
}