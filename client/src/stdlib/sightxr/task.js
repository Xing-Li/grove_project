export default async function task(env, payload) {
  const response = await fetch(`${env.baseUrl}/jobs`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  });
  return response.json()
}