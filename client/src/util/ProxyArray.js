export default class ProxyArray {

    constructor(editorjsKey, variantName, cb) {
        this.editorjsKey = editorjsKey;
        this.variantName = variantName;
        this.cb = (cb && cb.bind(this.array)) || (() => {
            let editor = this.editorjsKey === window.editor.getTransferKey() ? window.editor :
                _.filter(window.tagEditors, (tagEditor) => { return tagEditor.getTransferKey() === this.editorjsKey })[0];
            const { variables } = editor.getMain()._cachedata;
            let name = this.variantName
            let variable = variables[name]
            variable = !variable.redefines ? variable :
                _.assign({}, variable, variable.redefines[variable.redefines.length - 1])
            if (!variable.binded) {
                variable.func = variable.func.bind(variable.varia._value);
                variable.binded = true;
            }
            variable.varia.define(variable.name, variable.args, variable.func);
        });
    }

    /**
     * @returns {Array}
     */
    get array() {
        let editor = this.editorjsKey === window.editor.getTransferKey() ? window.editor :
            _.filter(window.tagEditors, (tagEditor) => { return tagEditor.getTransferKey() === this.editorjsKey })[0];
        const { variables } = editor.getMain()._cachedata;
        let name = this.variantName
        let variable = variables[name]
        variable = !variable.redefines ? variable :
            _.assign({}, variable, variable.redefines[variable.redefines.length - 1])
        if (undefined === variable.varia._value) {
            throw new Error("array not defined!");
        }
        return variable.varia._value;
    }

    push(...items) {
        let len = this.array.push(...items);
        this.cb();
        return len;
    }


    pop() {
        let any = this.array.pop()
        this.cb();
        return any;
    }

    splice() {
        let removes;
        removes = this.array.splice(...arguments);
        this.cb();
        return removes;
    }

    fill() {
        let n = this.array.fill(...arguments);
        this.cb();
        return n;
    }

    reverse() {
        let n = this.array.reverse();
        this.cb();
        return n;
    }

    shift() {
        let n = this.array.shift();
        this.cb();
        return n;
    }

    unshift() {
        let n = this.array.unshift(...arguments);
        this.cb();
        return n;
    }

    sort() {
        let n = this.array.sort(...arguments);
        this.cb();
        return n;
    }

    set length(len) {
        this.array.length = len;
        this.cb();
        return len;
    }
    get length() {
        return this.array.length
    }

    at() {
        return this.array.at(...arguments);
    }
    concat(...items) {
        return this.array.concat(...items)
    }
    copyWithin() {
        return this.array.copyWithin(...arguments);
    }

    entries() {
        return this.array.entries(...arguments);
    }
    every() {
        return this.array.every(...arguments);
    }
    filter() {
        return this.array.filter(...arguments);
    }
    find() {
        return this.array.find(...arguments);
    }
    findIndex() {
        return this.array.findIndex(...arguments);
    }
    flat() {
        return this.array.flat(...arguments);
    }
    flatMap() {
        return this.array.flatMap(...arguments);
    }
    forEach() {
        return this.array.forEach(...arguments);
    }
    includes() {
        return this.array.includes(...arguments);
    }
    indexOf() {
        return this.array.indexOf(...arguments);
    }
    join() {
        return this.array.join(...arguments);
    }
    keys() {
        return this.array.keys(...arguments);
    }
    lastIndexOf() {
        return this.array.lastIndexOf(...arguments);
    }
    map() {
        return this.array.map(...arguments);
    }
    reduce() {
        return this.array.reduce(...arguments);
    }
    reduceRight() {
        return this.array.reduceRight(...arguments);
    }
    slice() {
        return this.array.slice(...arguments);
    }
    some() {
        return this.array.some(...arguments);
    }
    toLocaleString() {
        return this.array.toLocaleString(...arguments);
    }
    toString() {
        return this.array.toString(...arguments);
    }
    values() {
        return this.array.values(...arguments);
    }
}