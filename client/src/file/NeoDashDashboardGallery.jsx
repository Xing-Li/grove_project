import {
    Button, Select, Space, Form,
    Input,
    Radio,
    Cascader,
    DatePicker,
    InputNumber,
    TreeSelect,
    Switch,
    Row,
    Col,
    List,
    Card,
} from 'antd';
import Text from 'antd/lib/typography/Text';
import axios from 'axios';
import _ from 'lodash';
import PropTypes from "prop-types";
import React from 'react';
import MdFiles from './MdFiles';
import Modal from 'antd/lib/modal/Modal';
import { WIDTH, Z_INDEX, Title, getFileName, getFileKey, getFileContent, getFileAttachmentContent } from './fileUtils.js';
import { ModalType, showToast, DefaultSecret } from '../util/utils.js';
import { FILESDIR, GUIDE_LINK_URI, getUploadUri } from 'util/localstorage';
import { ImageType, SelectType } from 'util/helpers';
import { getReferRealFileUri } from 'util/hqUtils';

const { Meta } = Card;
const { Option } = Select;
const FOLDER_NAME = "NeoDashDashboardGallery"
export default class NeoDashDashboardGallery extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        width: PropTypes.number,
        react_component: PropTypes.object,
    };
    static defaultProps = {};

    constructor(props) {
        super(props);
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
        let realFileUri = getReferRealFileUri(GUIDE_LINK_URI);
        this.state = {
            modalName: ModalType.NeoDashDashboardGallery,
            name: "",
            value: "",
            selectedIndex: 0,
            datas: [
                {
                    title: "Blank",
                    description: "An empty notebook.",
                    uploadUri: realFileUri.uri,
                    fileKey: `${FOLDER_NAME}/blank`
                },
            ]
        };
    }

    async componentDidMount() {
        let projectId = window.currentEditor && window.currentEditor.getInfo(GUIDE_LINK_URI).projectId;
        if (projectId && projectId !== "common") {
            let res = (await axios.post(`/api/grove/load`, { projectId: projectId }));
            if (!res.data.status && res.data.content instanceof Object) {
                let files = _.reduce(
                    res.data.content,
                    (prev, v, k) => {
                        if (k !== `${FOLDER_NAME}/` && k.startsWith(`${FOLDER_NAME}/`) && !~k.indexOf(FILESDIR) && v.type !== SelectType.dir) {
                            prev.push(v);
                        }
                        return prev;
                    },
                    []
                )
                if (files.length) {
                    let realFileUri = getReferRealFileUri(GUIDE_LINK_URI);
                    this.setState({
                        datas: _.reduce(files, (prev, file, index) => {
                            const { fileKey, name } = file;
                            prev.push({
                                title: name.camelPeakToBlankSplit().firstUpperCase(),
                                uploadUri: realFileUri.uri,
                                fileKey: `${fileKey}`,
                            })
                            return prev;
                        }, [])
                    })
                }
            }
        }
    }

    componentDidUpdate(prevProps, prevState) {
    }

    handleOk = () => {
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
    };

    onFormLayoutChange = ({ size }) => {
        setComponentSize(size);
    };

    render() {
        const { className, width } = this.props;
        const { name, value, modalName, selectedIndex, datas } = this.state;
        /**@type {MdFiles}*/
        const react_component = this.props.react_component;
        const { modalType, modalData: { cb, newFileName, newFileKey } } = react_component.state;
        const closeFunc = () => {
            react_component.setState({
                modalType: ModalType.None,
            });
        };
        return <Modal zIndex={Z_INDEX}
            className={`modal-comp data-html2canvas-ignore ${className}`}
            width={width || WIDTH}
            title={<div><h4>{modalName}</h4></div>}
            visible={modalType !== ModalType.None}
            onCancel={closeFunc}
            footer={<div className="d-flex justify-content-end">
                <Button key="back" onClick={closeFunc}>
                    Cancel
                </Button>&nbsp;&nbsp;
                <Button key="addNew" type="primary" onClick={async () => {
                    const { uploadUri, fileKey } = datas[selectedIndex];
                    await window.currentEditor.switchTagEditor(`${GUIDE_LINK_URI}${fileKey}`);
                    closeFunc();
                }}>
                    Open
                </Button>
            </div>}
        >
            <List
                grid={{
                    gutter: 16
                }}
                dataSource={datas}
                renderItem={ /** @param {ShapeShared} item */ (item, index) => {
                    let fileName = getFileName(item.fileKey);
                    let imageFileKey = getFileKey(getFileKey(item.fileKey, FILESDIR), fileName);
                    let imageURL = `${item.uploadUri}${imageFileKey}${ImageType}`
                    return <List.Item>
                        <Card className={`card-file ${selectedIndex === index ? "selected" : ""}`} hoverable
                            cover={<div className="image-wrap  bg-img"
                                style={{ backgroundImage: `url(${imageURL})` }} onClick={() => {
                                    this.setState({ selectedIndex: index })
                                }} onDoubleClick={async () => {
                                    const { uploadUri, fileKey } = datas[index];
                                    await window.currentEditor.switchTagEditor(`${GUIDE_LINK_URI}${fileKey}`);
                                    closeFunc();
                                }}>
                            </div>}
                        >
                            <Meta
                                title={<div>
                                    <div className="blablano">{`${item.title}`}</div>
                                </div>}
                            // description={<div className="blablano">{`${item.description}`}</div>}
                            />
                        </Card>
                    </List.Item>
                }}
            />
        </Modal >
    }
}