import _ from 'lodash';
import React from "react";
import { ShapeEditor } from '../../../util/hqApi';
import actions from '../../../define/Actions';
import { getChartConstructor } from '../comp/CheckChartTypes';

export default class DrawPlot extends React.Component {
    constructor(props) {
        super(props);
        const { type, exOptions, darkMode } = this.props;
        this.domRef = React.createRef();
        /**@type {ShapeEditor } */
        this.editor = this.props.editor;
        this.declareElement = document.createElement("div");
        this.dataElement = document.createElement("div");
    }

    componentDidUpdate(prevProps, prevState) {
        const { type, exOptions, darkMode } = this.props;
        if (prevProps.type !== type || !_.isEqual(prevProps.exOptions, exOptions) || prevProps.darkMode !== darkMode) {
            this.draw();
        }
    }

    componentWillUnmount() {
        this.destroy();
        this.clearCache();
    }

    clearCache() {
        if (!this.editor) {
            return;
        }
        this.editor.deleteCacheV(this.declareElement);
        this.editor.deleteCacheV(this.dataElement);
        this.declareElement.innerHTML = ""
        this.dataElement.innerHTML = ""
    }

    destroy() {
        if (this.chart) {
            this.chart.destroy();
            this.chart = undefined;
        }
    }

    draw() {
        if (this.chart) {
            this.destroy();
        }
        const { type, exOptions, darkMode, limitWh } = this.props;
        let constructorFunc = getChartConstructor(type)
        let exOptionsTmp = _.clone(exOptions);
        if (constructorFunc && !limitWh) {
            delete exOptionsTmp.width;
            delete exOptionsTmp.height;
        }
        exOptionsTmp.theme = darkMode ? "dark" : "default";
        /**@type {HTMLElement} */
        const container = this.domRef.current;
        if (container && constructorFunc) {
            container.innerHTML = "";
            this.chart = new constructorFunc(container, exOptionsTmp);
            this.chart.render(
                /**
                 * @param {(string | number)[]} visibleItems  Get an array with the ids of the currently visible items.
                 */
                async (visibleItems) => {
                    const { chartKey } = this.props;
                    this.clearCache();
                    this.editor && (await this.editor.runFunc(`_timeline_${chartKey.replaceNoWordToUnderline()} = ${JSON.stringify(visibleItems)}`,
                        this.declareElement, this.dataElement, () => {
                        }, () => {
                        }, true, `_tm_${chartKey.replaceNoWordToUnderline()}`));
                });
        }
    }

    componentDidMount() {
        this.draw();
    }

    render() {
        const { height } = this.props;
        return <div className="relative" style={{ height: height }} ref={this.domRef}></div>
    }
}