import _ from 'lodash';
import editor from '../commonEditor';
import { ContentType } from '../util/utils';
import actions from '../define/Actions';
import { expireModule } from '../util/hqUtils';
import MdFiles from './MdFiles';

const { SelectType, ImageCache, isFileNoEdit } = require('../util/helpers.js');
const { SETTINGS_NAME } = require("../util/utils");
const { rootDir, getMdFile, getUri } = require("./fileUtils");
const {
    getFileNamesKey, getUndefinedContent, removeItem,
    DefaultFileData, DefaultFolderData,
    DEFAULT_VER, ShapeShared
} = require("../util/localstorage");
export default class MsgHandler {
    constructor(react_component) {
        /**@type{MdFiles} */
        this.react_component = react_component;
    }

    removeFile = async (fileKey) => {
        const { activeFileKey, cloud } = this.react_component.state;
        if (fileKey === null) {
            return;
        }
        const state = {}
        this.react_component.removeFileCache(fileKey);
        _.assign(state, {
            fileNamesJson: this.react_component.setFileNamesJsonItem(), cloud: cloud,
        });
        let openEditor = this.react_component.getOpenEditor(fileKey);
        if (openEditor && openEditor.isCommon()) {
            openEditor.setFileData(undefined);
            await openEditor.value(getUndefinedContent(), ContentType.File);
            _.assign(state, {
                selectType: SelectType.dir, rename: false, activeFileKey: null
            });
        } else {
            openEditor && this.react_component.closeTagEditor(openEditor);
        }
        !_.isEmpty(state) && this.react_component.setState(state);
    }

    /**
     * 
     * @param {string} oldFileKey 
     * @param {DefaultFileData} newFileData 
     */
    renameOrMoveFile = async (oldFileKey, newFileData) => {
        const { activeFileKey, fileVersion, fileNamesJson, uploadUri, cloud, userId, projectId, shareProjectId } = this.react_component.state;
        const oldFile = fileNamesJson[oldFileKey];
        let openEditor = this.react_component.getOpenEditor(oldFileKey);
        const newFileName = newFileData.name;
        const newFileKey = newFileData.fileKey;
        let state = {};
        let content;
        cloud[newFileKey] = _.cloneDeep(newFileData);
        fileNamesJson[newFileKey] = newFileData;
        removeItem(getFileNamesKey() + '.' + newFileKey);
        let oldUri = getUri(uploadUri, oldFileKey);
        if (isFileNoEdit(oldFile) && ImageCache[oldUri]) {
            let newUri = getUri(uploadUri, newFileKey);
            ImageCache[newUri] = ImageCache[oldUri];
            content = ImageCache[newUri];
        } else if (openEditor && openEditor.isCommon()) {
            content = await getMdFile(uploadUri, cloud, newFileData, false, fileVersion);
        }
        if (openEditor && openEditor.isCommon()) {
            this.react_component.removeFileCache(oldFileKey);
            fileNamesJson[newFileKey].selected = true;
            let fileData = fileNamesJson[newFileKey];
            await this.react_component.loadNewFile(content, fileData, DEFAULT_VER);
            this.react_component.setState(_.assign(state, {
                activeFileKey: newFileKey, fileVersion: DEFAULT_VER, selectType: SelectType.file,
                rename: false, renameFileKey: null, fileNamesJson: this.react_component.setFileNamesJsonItem(), cloud: cloud
            }), () => {
                openEditor.focusFolder();
            })
        } else {
            this.react_component.removeFileCache(oldFileKey);
            fileNamesJson[newFileKey].selected = false;
            if (openEditor) {
                await this.switchFileTagEditor(newFile, DEFAULT_VER, content, undefined, false, openEditor);
            }
            this.react_component.setState(_.assign(state, {
                rename: false, renameFileKey: null, fileNamesJson: this.react_component.setFileNamesJsonItem(), cloud: cloud
            }))
        }
    };


    /**
     * 
     * @param {DefaultFileData} f 
     * @param {Object<string,DefaultFileData>} gzip 
     */
    uploadFile = async (f, gzip) => {
        const { cloud, fileNamesJson, notOwn, uploadUri } = this.react_component.state;
        _.each([f, ..._.values(gzip)], async (file) => {
            if (!file) {
                return;
            }
            cloud[file.fileKey] = _.cloneDeep(file);
            //NOTE: shareData not overwrite here
            fileNamesJson[file.fileKey] !== undefined ?
                _.assign(fileNamesJson[file.fileKey], file) :
                (fileNamesJson[file.fileKey] = file);
            removeItem(getFileNamesKey() + '.' + file.fileKey);
            let state = {};
            _.assign(state, { cloud: cloud, fileNamesJson: this.react_component.setFileNamesJsonItem() })
            this.react_component.setState(state);
            let openEditor = this.react_component.getOpenEditor(file.fileKey)
            if (openEditor) {
                openEditor.refreshFileData(openEditor.getFileData(), fileNamesJson[file.fileKey]);
            }
            expireModule(file.fileKey, uploadUri);
            if (notOwn && file.fileKey === SETTINGS_NAME) {
                let settings = JSON.parse(await getMdFile(uploadUri, cloud, fileNamesJson[file.fileKey], true));
                if (!_.isEqual(settings.usersSharePermission, editor.getSettings().getUsersSharePermission())) {
                    actions.variable(actions.types.USERS_SHARE_PERMISSION, [], () => {
                        return settings.usersSharePermission;
                    })
                    editor.getSettings().setUsersSharePermission(settings.usersSharePermission);
                }
            }
        })
    }

    removeFolder = async (folderKey) => {
        let state = {};
        await this.react_component.removeFolderSuccessFunc(folderKey, state)
        !_.isEmpty(state) && this.react_component.setState(state);
    }

    /**
     * 
     * @param {string} oldFolderKey 
     * @param {string} newFolderKey 
     * @param {Object.<string,DefaultFileData>} ret 
     */
    renameOrMoveFolder = async (oldFolderKey, newFolderKey, ret) => {
        const { activeFolderKey, activeFileKey, linkedFilesMap, sharedFilesMap,
            fileNamesJson, uploadUri, cloud, userId, projectId, shareProjectId } = this.react_component.state;
        let state = {};
        _.each(_.keys(fileNamesJson), (key, index) => {
            if (key.startsWith(oldFolderKey)) {
                delete cloud[key];
                delete fileNamesJson[key];
            }
        });
        _.map(ret, (value, key) => {
            cloud[key] = _.cloneDeep(value);
            fileNamesJson[key] = value;
        })
        this.react_component.setFileNamesJsonItem();
        if (activeFileKey && activeFileKey.startsWith(oldFolderKey)) {
            editor.setLinkData(undefined);
            editor.setFileData(undefined);
            await editor.value(getUndefinedContent(), ContentType.File);
            _.assign(state, {
                activeFileKey: null
            });
        }
        _.assign(state, {
            activeFolderKey: newFolderKey, selectType: SelectType.dir,
            rename: false, fileNamesJson: this.react_component.setFileNamesJsonItem(), cloud: cloud
        });
        this.react_component.setState(state, () => {
            editor.focusFolder();
        })
    }


    /**
     * 
     * @param {DefaultFolderData} folder 
     */
    uploadFolder = async (folder) => {
        const { cloud, fileNamesJson } = this.react_component.state;
        cloud[folder.folderKey] = _.cloneDeep(folder);
        fileNamesJson[folder.folderKey] !== undefined ?
            _.assign(fileNamesJson[folder.folderKey], folder) :
            (fileNamesJson[folder.folderKey] = folder);
        this.react_component.setState({ cloud: cloud, fileNamesJson: this.react_component.setFileNamesJsonItem() }, () => {
        });
    }

    /**
     * 
     * @param {Object.<string,DefaultFileData>} ret 
     */
    copyFolderUpdate = (ret) => {
        const { cloud, fileNamesJson } = this.react_component.state;
        _.map(ret, (value, key) => {
            cloud[key] = _.cloneDeep(value);
            fileNamesJson[key] = value;
        })
        this.react_component.setFileNamesJsonItem();
        this.react_component.setState({ fileNamesJson: this.react_component.setFileNamesJsonItem(), cloud: cloud }, () => {
        });
    }


    /**
     * 
     * @param {ShapeShared} shared 
     */
    addShared = (shared) => {
        const { sharedFilesMap, sharedVisible } = this.react_component.state;
        const shareds = _.cloneDeep(sharedFilesMap[shared.fileKey] || {});
        shareds[shared._id] = shared;
        this.react_component.refreshShareds(shared.fileKey, shareds, () => {
            this.react_component.setState({ sharedVisible: !sharedVisible });
        });
    }


    /**
     * 
     * @param {ShapeShared} shared 
     */
    removeShared = (shared) => {
        const { sharedFilesMap, sharedVisible } = this.react_component.state;
        const shareds = _.cloneDeep(sharedFilesMap[shared.fileKey] || {});
        delete shareds[shared._id];
        this.react_component.refreshShareds(shared.fileKey, shareds, () => {
            this.react_component.setState({ sharedVisible: !sharedVisible });
        });
    }
}