import { Timeline, DataSet } from 'vis-timeline/standalone'
import { loadMultiColors } from 'util/chartConfigUtil'
import { chartOptionsDefault } from './OptionsTypes'
import { VizControl } from "common/graphOps";
import NavigateComp, { destroyInterval, addMarkers, removeMarkers } from './NavigatorComp';
import React from "react";
import ReactDOM from "react-dom";

/**
 * 
 * @param {{start:Date,end:Date,byUser:boolean,event}} properties 
 * @param {*} cb
 * @param {chartOptionsDefault} exOptions
 * @param {Timeline} timeline
 * @param {Array<{start: Date, end: Date, id: number}>} itemsArr
 */
const rangechangedFunc = function (properties, cb, exOptions, timeline, itemsArr) {
    const { start, end, byUser } = properties;
    if (!timeline.closeMarker) {
        let ids = [];
        if (timeline.getCustomTime(timeline.startMarkerId) < start || timeline.getCustomTime(timeline.startMarkerId) > end) {
            timeline.setCustomTime(start, timeline.startMarkerId);
            ids.push(timeline.startMarkerId);
        }
        if (timeline.getCustomTime(timeline.endMarkerId) < start || timeline.getCustomTime(timeline.endMarkerId) > end) {
            timeline.setCustomTime(end, timeline.endMarkerId);
            ids.push(timeline.endMarkerId);
        }
        if (ids.length) {
            timechangedFunc(ids, cb, exOptions, timeline, itemsArr);
            let startMarktime = timeline.getCustomTime(timeline.startMarkerId);
            let endMarktime = timeline.getCustomTime(timeline.endMarkerId);
            timeline.dragMarkersPeriod = endMarktime - startMarktime;
        }
    }
}
/**
 * 
 * @param {[]} ids
 * @param {*} cb
 * @param {chartOptionsDefault} exOptions
 * @param {Timeline} timeline
 * @param {Array<{start: Date, end: Date, id: number}>} itemsArr
 */
const timechangedFunc = function (ids, cb, exOptions, timeline, itemsArr, byUser = false) {
    let rangeIds;
    if (timeline.closeMarker) {
        rangeIds = timeline.getVisibleItems()
    } else {
        timechangeFunc(ids, timeline, byUser);
        let startMarktime = timeline.getCustomTime(timeline.startMarkerId);
        let endMarktime = timeline.getCustomTime(timeline.endMarkerId);
        if (timeline.getWindow().start === startMarktime && timeline.getWindow().end === endMarktime) {
            rangeIds = timeline.getVisibleItems()
        } else {
            rangeIds = _.reduce(itemsArr, (prev, curr, index) => {
                if (curr.start >= startMarktime && curr.start <= endMarktime && (!curr.end || curr.end >= startMarktime && curr.end <= endMarktime)) {
                    prev.push(curr.id);
                } else if (curr.end && curr.start <= endMarktime && curr.end >= startMarktime) {
                    prev.push(curr.id);
                }
                return prev;
            }, [])
        }
    }
    const { data, linkMain } = exOptions;
    if (linkMain && data.keysArr) {
        let ids = _.map(rangeIds, (id, index) => {
            return data.keysArr[id];
        })
        // console.log(ids.join(","));
        VizControl.deSelectNodesById(VizControl.getSelectedNodeIds)
        VizControl.selectNodesById(ids);
    }
    cb && cb(_.map(rangeIds, (id, index) => {
        return exOptions.data[id];
    }))
}
/**
 * 
 * @param {[]} ids
 * @param {Timeline} timeline
 */
const timechangeFunc = function (ids, timeline, byUser = false) {
    let startMarktime = timeline.getCustomTime(timeline.startMarkerId);
    let endMarktime = timeline.getCustomTime(timeline.endMarkerId);
    let flagRet = false;
    if (timeline.dragMarkers) {
        let period = timeline.dragMarkersPeriod;
        if (startMarktime > timeline.getWindow().end - period ||
            endMarktime > timeline.getWindow().end) {
            timeline.setCustomTime(new Date(timeline.getWindow().end.getTime() - period), timeline.startMarkerId);
            timeline.setCustomTime(timeline.getWindow().end, timeline.endMarkerId);
            flagRet = true;
        }
        if (startMarktime < timeline.getWindow().start ||
            endMarktime < timeline.getWindow().start + period) {
            timeline.setCustomTime(timeline.getWindow().start, timeline.startMarkerId);
            timeline.setCustomTime(new Date(timeline.getWindow().start.getTime() + period), timeline.endMarkerId);
            flagRet = true;
        }
        if (!flagRet) {
            if (~(ids.indexOf(timeline.startMarkerId))) {
                timeline.setCustomTime(startMarktime, timeline.startMarkerId);
                timeline.setCustomTime(new Date(startMarktime.getTime() + period), timeline.endMarkerId);
            }
            if (~(ids.indexOf(timeline.endMarkerId))) {
                timeline.setCustomTime(new Date(endMarktime.getTime() - period), timeline.startMarkerId);
                timeline.setCustomTime(endMarktime, timeline.endMarkerId);
            }
            flagRet = true;
        }
        let startMarktime2 = timeline.getCustomTime(timeline.startMarkerId);
        let endMarktime2 = timeline.getCustomTime(timeline.endMarkerId);
        if (startMarktime2 == endMarktime2 || endMarktime2 - startMarktime2 < period) {
            debugger
        }
    } else {
        if (~(ids.indexOf(timeline.startMarkerId)) && startMarktime > endMarktime) {
            timeline.setCustomTime(endMarktime, timeline.startMarkerId);
            flagRet = true;
        }
        if (~(ids.indexOf(timeline.startMarkerId)) && startMarktime < timeline.getWindow().start) {
            timeline.setCustomTime(timeline.getWindow().start, timeline.startMarkerId);
            flagRet = true;
        }
        if (~(ids.indexOf(timeline.endMarkerId)) && endMarktime < startMarktime) {
            timeline.setCustomTime(startMarktime, timeline.endMarkerId);
            flagRet = true;
        }
        if (~(ids.indexOf(timeline.endMarkerId)) && endMarktime > timeline.getWindow().end) {
            timeline.setCustomTime(timeline.getWindow().end, timeline.endMarkerId);
            flagRet = true;
        }
    }
    return flagRet
}

/**
 * 
 * @param {*} properties
 * @param {Timeline} timeline
 */
const doubleClickFunc = function (properties, timeline) {
    let eventProps = timeline.getEventProperties(properties.event);
    if (eventProps.what === 'custom-time') {
        timeline.dragMarkers = !timeline.dragMarkers;
        if (timeline.dragMarkers) {
            let startMarktime = timeline.getCustomTime(timeline.startMarkerId);
            let endMarktime = timeline.getCustomTime(timeline.endMarkerId);
            timeline.dragMarkersPeriod = endMarktime - startMarktime;
        }
        if (eventProps.event.target.parentNode.parentNode) {
            /**@type {HTMLElement} */
            let ele = eventProps.event.target.parentNode.parentNode;
            ele.querySelector(`.vis-custom-time.${timeline.startMarkerId}`).classList.toggle("mark-frozen", timeline.dragMarkers)
            ele.querySelector(`.vis-custom-time.${timeline.endMarkerId}`).classList.toggle("mark-frozen", timeline.dragMarkers)
        }
    }
}

export class GroupsTimelineConstruct {
    /** 
     * @param { HTMLElement } container
     * @param {chartOptionsDefault}  exOptions
    */
    constructor(container, exOptions) {
        this.container = container;
        this.exOptions = exOptions;
        // https://motocal.com/
        // this.groups = new DataSet([
        //     { "content": "Formula E", "id": "Formula E", "value": 1, className: 'openwheel' },
        //     { "content": "WRC", "id": "WRC", "value": 2, className: 'rally' },
        //     { "content": "MotoGP", "id": "MotoGP", "value": 3, className: 'motorcycle' },
        // ]);
        const { startField, endField, contentField, groupField, data, type, title, description, linkMain, timeSpacing, theme, hideLabel, ...leftOptions } = exOptions;
        let otherStr = "Other";
        let groupSet = Array.from(_.reduce(data, (prev, curr, index) => {
            if (undefined !== curr[groupField] && !prev.has(curr[groupField])) {
                prev.add(`${curr[groupField]}`);
            } else if (undefined === curr[groupField]) {
                prev.add(otherStr);
            }
            return prev;
        }, new Set()));
        let multiColors = loadMultiColors(groupSet.length);
        let time = new Date().getTime();
        let groupMap = _.reduce(groupSet, (prev, curr, index) => {
            let obj = {
                "content": curr, "id": curr, "value": index, className: `_${time}_${index}`
            }
            prev[curr] = obj;
            return prev;
        }, {});
        let groupArr = _.values(groupMap);
        this.groups = new DataSet(groupArr);

        this._style = document.createElement("style");
        this._style.innerHTML = _.reduce(groupArr, (prev, value, index) => {
            prev += `.vis-item.${value.className}  { background-color: ${multiColors[index].value}; }\n`;
            return prev;
        }, "")

        // create a dataset with items
        // note that months are zero-based in the JavaScript Date object, so month 3 is April
        let startDate = undefined, endDate = undefined;
        this.itemsArr = _.reduce(data, (prev, curr, index) => {
            if (!curr[startField]) {
                return prev;
            }
            let groupName = `${undefined !== curr[groupField] ? curr[groupField] : otherStr}`;
            let obj = {
                start: typeof curr[startField] === 'string' ? new Date(curr[startField]) : curr[startField],
                group: groupName,
                className: groupMap[groupName].className,
                title: hideLabel ? curr[contentField] || "" : "",
                content: `${!hideLabel && curr[contentField] || ""}`,
                id: index,
            }
            startDate = !startDate ? obj.start : (obj.start < startDate ? obj.start : startDate);
            endDate = !endDate ? obj.start : (obj.start > endDate ? obj.start : endDate);
            if (endField && curr[endField]) {
                obj.end = typeof curr[endField] === 'string' ? new Date(curr[endField]) : curr[endField]
                obj.type = type;
                endDate = !endDate ? obj.end : (obj.end > endDate ? obj.end : endDate);
            } else {
                obj.type = type === "box" ? type : "point";
            }
            prev.push(obj);
            return prev;
        }, [])
        if (endDate && startDate) {
            let period = endDate.getTime() - startDate.getTime();
            startDate = new Date(startDate.getTime() - period * Math.abs(timeSpacing) / 100);
            endDate = new Date(endDate.getTime() + period * Math.abs(timeSpacing) / 100);
        } else {
            startDate = new Date();
            endDate = new Date(new Date().getTime() + 1000000);
        }
        this.startMarkDate = new Date(startDate.getTime() - 1);
        this.endMarkDate = new Date(endDate.getTime() + 1);
        this.items = new DataSet(this.itemsArr);
        this.menu = document.createElement('div');
        this.options = _.assign({
            // option groupOrder can be a property name or a sort function
            // the sort function must compare two groups and return a value
            //     > 0 when a > b
            //     < 0 when a < b
            //       0 when a == b
            groupOrder: function (a, b) {
                return a.value - b.value;
            },
            groupOrderSwap: function (a, b, groups) {
                let v = a.value;
                a.value = b.value;
                b.value = v;
            },
            groupTemplate: (group) => {
                if (!group) {
                    return null;
                }
                let container = document.createElement('div');
                let label = document.createElement('span');
                label.innerHTML = group.content + ' ';
                container.insertAdjacentElement('afterBegin', label);
                let hide = document.createElement('span');
                hide.className = "icon-bordered icon-muted icon fas fa-eye-slash";
                hide.title = "Hide Group"
                hide.addEventListener('click', () => {
                    this.groups.update({ id: group.id, visible: false });
                    this.hideGroup(group.id);
                });
                container.insertAdjacentElement('beforeEnd', hide);
                return container;
            },
            orientation: 'both',
            editable: false,
            groupEditable: true,
            snap: null,
            ...leftOptions
        }, timeSpacing === -1 ? {} : {
            min: startDate,
            max: endDate,
        });
    }

    hideGroup(groupId) {
        this.timeline && this.timeline.hideGroupCb && this.timeline.hideGroupCb(groupId);
    }

    // function to make all groups visible again
    showAllGroups = () => {
        this.groups.forEach((group) => {
            this.groups.update({ id: group.id, visible: true });
        })
    };

    render(cb) {
        if (this.timeline) {
            this.destroy();
        }
        this.timeline = new Timeline(this.container);
        this.container.appendChild(this._style);
        this.container.appendChild(this.menu);
        this.timeline.setItems(this.items);
        this.timeline.setOptions(this.options);
        this.timeline.setGroups(this.groups);
        this.timeline.startMarkDate = this.startMarkDate || new Date();
        this.timeline.endMarkDate = this.endMarkDate || new Date();
        this.timeline.startMarkerId = "startMarker";
        this.timeline.endMarkerId = "endMarker";
        this.timeline.closeMarker = true;
        if (this.timeline.closeMarker) {
            // removeMarkers(this.timeline)
        } else {
            addMarkers(this.timeline);
        }
        ReactDOM.render(<NavigateComp timeline={this.timeline} zoomKey={this.exOptions.zoomKey} showAllGroups={this.showAllGroups} />, this.menu);
        this.timeline.on('rangechanged', (properties) => {
            rangechangedFunc(properties, cb, this.exOptions, this.timeline, this.itemsArr);
        });
        this.timeline.on('timechange', ({ id, time, event }) => {
            timechangeFunc([id], this.timeline, true);
        });
        this.timeline.on('timechanged', ({ id, time, event }) => {
            timechangedFunc([id], cb, this.exOptions, this.timeline, this.itemsArr, true);
        });
        this.timeline.on('doubleClick', (properties) => {
            doubleClickFunc(properties, this.timeline);
        });
    }

    destroy() {
        if (this.timeline) {
            this.timeline.destroy();
            destroyInterval(this.timeline);
            this.container.innerHTML = "";
            this.timeline = undefined;
            this.container = undefined;
        }
    }
}

export class SubGroupsTimelineConstruct {
    /** 
     * @param { HTMLElement } container
     * @param {chartOptionsDefault}  exOptions
    */
    constructor(container, exOptions) {
        this.container = container;
        this.exOptions = exOptions;
        // https://motocal.com/
        // this.groups = new DataSet([
        //     { "content": "Formula E", "id": "Formula E", "value": 1, className: 'openwheel' },
        //     { "content": "WRC", "id": "WRC", "value": 2, className: 'rally' },
        //     { "content": "MotoGP", "id": "MotoGP", "value": 3, className: 'motorcycle' },
        // ]);
        const { startField, endField, contentField, groupField, groupSeparator, subGroupF, data, type, title, description, linkMain, timeSpacing, theme, hideLabel, ...leftOptions } = exOptions;
        let otherStr = "Other";
        let subGroupFunc = ((groupField, row = {}) => {
            /**@type{""} */
            let groupName = `${undefined !== row[groupField] ? row[groupField] : otherStr}`.toCommonName();
            let sourceSplits = groupName.split(groupSeparator);
            let ret = [];
            for (let i = 1; i <= sourceSplits.length; i++) {
                let splits = sourceSplits.slice(0, i);
                let parentGroupSplits = splits.length >= 2 ? splits.slice(0, splits.length - 1) : undefined;
                ret.push({
                    groupName: splits.join(groupSeparator),
                    parentGroupSplits: parentGroupSplits,
                    treeLevel: splits.length,
                    content: splits[splits.length - 1]
                })
            }
            return ret;
        })
        let idCount = 0, subIdCount = 1000000;
        let time = new Date().getTime();
        let groupMap = _.reduce(data, (prev, curr, index) => {
            let arr = subGroupFunc(groupField, curr);
            _.each(arr, (obj, i) => {
                const { groupName, parentGroupSplits, treeLevel, content } = obj;
                if (undefined !== prev[groupName] && prev[groupName].id) {
                    return;
                }
                let id = treeLevel === 1 ? idCount++ : subIdCount++;
                _.set(prev, `${groupName}.id`, id);
                _.set(prev, `${groupName}.treeLevel`, treeLevel);
                _.set(prev, `${groupName}.content`, content);
                _.set(prev, `${groupName}.value`, id);
                treeLevel === 1 && _.set(prev, `${groupName}.className`, `_${time}_${id}`);
                if (parentGroupSplits) {
                    let parentKey = parentGroupSplits.join(groupSeparator);
                    if (!_.get(prev, `${parentKey}.nestedGroups`)) {
                        _.set(prev, `${parentKey}.nestedGroups`, []);
                    }
                    if (!~_.get(prev, `${parentKey}.nestedGroups`).indexOf(id)) {
                        _.get(prev, `${parentKey}.nestedGroups`).push(id);
                    }
                    _.set(prev, `${groupName}.title`, `${groupName}(${parentGroupSplits.join("/")}/${content})`);
                } else {
                    _.set(prev, `${groupName}.title`, content);
                }
            })
            return prev;
        }, {});
        let multiColors = loadMultiColors(idCount);
        let groupArr = _.values(groupMap);
        this.groups = new DataSet(groupArr);

        this._style = document.createElement("style");
        this._style.innerHTML = _.reduce(groupArr, (prev, value, index) => {
            if (value.treeLevel === 1) {
                prev += `.vis-item.${value.className}  { background-color: ${multiColors[value.id].value}; }\n`;
            }
            return prev;
        }, "")

        // create a dataset with items
        // note that months are zero-based in the JavaScript Date object, so month 3 is April
        let startDate = undefined, endDate = undefined;
        this.itemsArr = _.reduce(data, (prev, curr, index) => {
            if (!curr[startField]) {
                return prev;
            }
            let groupName = `${undefined !== curr[groupField] ? curr[groupField] : otherStr}`.toCommonName();
            let obj = {
                start: typeof curr[startField] === 'string' ? new Date(curr[startField]) : curr[startField],
                group: _.get(groupMap, groupName).id,
                className: _.get(groupMap, groupName).className,
                title: hideLabel ? curr[contentField] || "" : "",
                content: `${!hideLabel && curr[contentField] || ""}`,
                id: index,
            }
            startDate = !startDate ? obj.start : (obj.start < startDate ? obj.start : startDate);
            endDate = !endDate ? obj.start : (obj.start > endDate ? obj.start : endDate);
            if (endField && curr[endField]) {
                obj.end = typeof curr[endField] === 'string' ? new Date(curr[endField]) : curr[endField]
                obj.type = type;
                endDate = !endDate ? obj.end : (obj.end > endDate ? obj.end : endDate);
            } else {
                obj.type = type === "box" ? type : "point";
            }
            prev.push(obj);
            return prev;
        }, [])
        if (endDate && startDate) {
            let period = endDate.getTime() - startDate.getTime();
            startDate = new Date(startDate.getTime() - period * Math.abs(timeSpacing) / 100);
            endDate = new Date(endDate.getTime() + period * Math.abs(timeSpacing) / 100);
        } else {
            startDate = new Date();
            endDate = new Date(new Date().getTime() + 1000000);
        }
        this.startMarkDate = new Date(startDate.getTime() - 1);
        this.endMarkDate = new Date(endDate.getTime() + 1);
        this.items = new DataSet(this.itemsArr);
        this.menu = document.createElement('div');
        this.options = _.assign({
            // option groupOrder can be a property name or a sort function
            // the sort function must compare two groups and return a value
            //     > 0 when a > b
            //     < 0 when a < b
            //       0 when a == b
            groupOrder: function (a, b) {
                return a.value - b.value;
            },
            groupOrderSwap: function (a, b, groups) {
                let v = a.value;
                a.value = b.value;
                b.value = v;
            },
            groupTemplate: (group) => {
                if (!group) {
                    return null;
                }
                let container = document.createElement('div');
                let label = document.createElement('span');
                label.innerHTML = group.content + ' ';
                container.insertAdjacentElement('afterBegin', label);
                let hide = document.createElement('span');
                hide.className = "icon-bordered icon-muted icon fas fa-eye-slash";
                hide.title = "Hide Group"
                hide.addEventListener('click', () => {
                    this.groups.update({ id: group.id, visible: false });
                    this.hideGroup(group.id);
                });
                container.insertAdjacentElement('beforeEnd', hide);
                return container;
            },
            orientation: 'both',
            editable: false,
            groupEditable: true,
            snap: null,
            ...leftOptions
        }, timeSpacing === -1 ? {} : {
            min: startDate,
            max: endDate,
        });
    }

    hideGroup(groupId) {
        this.timeline && this.timeline.hideGroupCb && this.timeline.hideGroupCb(groupId);
    }

    // function to make all groups visible again
    showAllGroups = () => {
        this.groups.forEach((group) => {
            this.groups.update({ id: group.id, visible: true });
        })
    };

    render(cb) {
        if (this.timeline) {
            this.destroy();
        }
        this.timeline = new Timeline(this.container);
        this.container.appendChild(this._style);
        this.container.appendChild(this.menu);
        this.timeline.setItems(this.items);
        this.timeline.setOptions(this.options);
        this.timeline.setGroups(this.groups);
        this.timeline.startMarkDate = this.startMarkDate || new Date();
        this.timeline.endMarkDate = this.endMarkDate || new Date();
        this.timeline.startMarkerId = "startMarker";
        this.timeline.endMarkerId = "endMarker";
        this.timeline.closeMarker = true;
        if (this.timeline.closeMarker) {
            // removeMarkers(this.timeline)
        } else {
            addMarkers(this.timeline);
        }
        ReactDOM.render(<NavigateComp timeline={this.timeline} zoomKey={this.exOptions.zoomKey} showAllGroups={this.showAllGroups} />, this.menu);
        this.timeline.on('rangechanged', (properties) => {
            rangechangedFunc(properties, cb, this.exOptions, this.timeline, this.itemsArr);
        });
        this.timeline.on('timechange', ({ id, time, event }) => {
            timechangeFunc([id], this.timeline, true);
        });
        this.timeline.on('timechanged', ({ id, time, event }) => {
            timechangedFunc([id], cb, this.exOptions, this.timeline, this.itemsArr, true);
        });
        this.timeline.on('doubleClick', (properties) => {
            doubleClickFunc(properties, this.timeline);
        });
    }

    destroy() {
        if (this.timeline) {
            this.timeline.destroy();
            destroyInterval(this.timeline);
            ReactDOM.unmountComponentAtNode(this.menu);
            this.container.innerHTML = "";
            this.timeline = undefined;
            this.container = undefined;
        }
    }
}

export class TimelineConstruct {
    /** @param {chartOptionsDefault}  exOptions*/
    constructor(container, exOptions) {
        this.container = container;
        this.exOptions = exOptions;
        const { startField, endField, contentField, groupField, data, type, title, description, linkMain, timeSpacing, theme, hideLabel, ...leftOptions } = exOptions;
        let startDate = undefined, endDate = undefined;
        this.itemsArr = _.reduce(data, (prev, curr, index) => {
            if (!curr[startField]) {
                return prev;
            }
            let obj = {
                id: index,
                title: hideLabel ? curr[contentField] || "" : "",
                content: `${!hideLabel && curr[contentField] || ""}`,
                start: typeof curr[startField] === 'string' ? new Date(curr[startField]) : curr[startField],
            }
            startDate = !startDate ? obj.start : (obj.start < startDate ? obj.start : startDate);
            endDate = !endDate ? obj.start : (obj.start > endDate ? obj.start : endDate);
            if (endField && curr[endField]) {
                obj.end = typeof curr[endField] === 'string' ? new Date(curr[endField]) : curr[endField]
                obj.type = type;
                endDate = !endDate ? obj.end : (obj.end > endDate ? obj.end : endDate);
            } else {
                obj.type = type === "box" ? type : "point";
            }
            prev.push(obj);
            return prev;
        }, []);
        if (endDate && startDate) {
            let period = endDate.getTime() - startDate.getTime();
            startDate = new Date(startDate.getTime() - period * Math.abs(timeSpacing) / 100);
            endDate = new Date(endDate.getTime() + period * Math.abs(timeSpacing) / 100);
        } else {
            startDate = new Date();
            endDate = new Date(new Date().getTime() + 1000000);
        }
        this.startMarkDate = new Date(startDate.getTime() - 1);
        this.endMarkDate = new Date(endDate.getTime() + 1);
        this.items = new DataSet(this.itemsArr);
        this.menu = document.createElement('div');
        this.dragSlider = document.createElement("div");
        this.dragSlider.className = "drag-slider";
        this.options = _.assign({
            min: startDate,
            max: endDate,
            snap: null,
            ...leftOptions
        }, timeSpacing === -1 ? {} : {
            min: startDate,
            max: endDate,
        });
    }

    render(cb) {
        if (this.timeline) {
            this.destroy();
        }
        this.timeline = new Timeline(this.container, this.items, this.options);
        this.container.appendChild(this.menu);
        this.timeline.startMarkDate = this.startMarkDate || new Date();
        this.timeline.endMarkDate = this.endMarkDate || new Date();
        this.timeline.startMarkerId = "startMarker";
        this.timeline.endMarkerId = "endMarker";
        this.timeline.closeMarker = true;
        if (this.timeline.closeMarker) {
            // removeMarkers(this.timeline)
        } else {
            addMarkers(this.timeline);
        }
        ReactDOM.render(<NavigateComp timeline={this.timeline} zoomKey={this.exOptions.zoomKey} />, this.menu);
        this.timeline.on('rangechanged', (properties) => {
            rangechangedFunc(properties, cb, this.exOptions, this.timeline, this.itemsArr);
        });
        this.timeline.on('timechange', ({ id, time, event }) => {
            timechangeFunc([id], this.timeline, true);
        });
        this.timeline.on('timechanged', ({ id, time, event }) => {
            timechangedFunc([id], cb, this.exOptions, this.timeline, this.itemsArr, true);
        });
        this.timeline.on('doubleClick', (properties) => {
            doubleClickFunc(properties, this.timeline);
        });
    }

    destroy() {
        if (this.timeline) {
            this.timeline.destroy();
            destroyInterval(this.timeline);
            ReactDOM.unmountComponentAtNode(this.menu);
            this.container.innerHTML = "";
            this.timeline = undefined;
            this.container = undefined;
        }
    }
}