import prettier from "@observablehq/prettier";
import parserBabel from "@observablehq/prettier/parser-babel.js";
import prettierStandalone from "@observablehq/prettier/standalone.js";
import { Button, Cascader, Dropdown, Input, Menu, Select, Spin } from "antd";
import Modal from "antd/lib/modal/Modal";
import Text from "antd/lib/typography/Text";
import { getColumnsData } from "block/antchart/antchartUtils";
import "codemirror";
import CodeMirror from "codemirror";
import "codemirror/addon/comment/comment";
import "codemirror/addon/display/placeholder.js";
import "codemirror/addon/hint/show-hint";
import "codemirror/keymap/vim.js";
import commonData from "common/CommonData";
import _ from "lodash";
import PropTypes from "prop-types";
import React from "react";
import { Controlled } from "react-codemirror2";
import { ShareData } from "stdlib/ExInspector";
import actions from "../../define/Actions.js";
import MyDropzone from "../../file/MyDropzone.jsx";
import { Z_INDEX, fileSeparator, getFileName } from "../../file/fileUtils.js";
import showHint, { cmSearchFunc, cursorActivityFunc, insertText } from "../../util/CodeMirror.showHint.js";
import ChooseDatas from "./ChooseDatas";
import {
  ColumnsComp,
  ColumnsDetailComp,
  FilterComp,
  SliceComp,
  SortComp,
  TableComp,
  assignFiltersSorts,
  contentEditable,
  proxyArrayCode,
  withPopMenu,
  ErrorComp,
} from "./CodeTools";
import { AttachmentIcon, CheckboxSvgIcon, ColumnsSvgIcon, CutSvgIcon, FilterSvgIcon, LevelDownIcon, PinIcon, RunCodeIcon, SortSvgIcon } from "./Icon";
import { __query } from "stdlib/table.js";
import Icon, { Loading3QuartersOutlined } from "@ant-design/icons";

const { ImageCache, createElemnt, isVisibleInViewport, SelectType, ConvertToCSV, osfunction, OSType, assembling,
  isFileDb, isFileExcel, highligthComp, SearchType, isFileArrow, } = require("../../util/helpers.js");
require("codemirror/addon/fold/foldcode");
require("codemirror/addon/fold/foldgutter");
require("codemirror/addon/fold/comment-fold");
require("codemirror/addon/dialog/dialog");
require("javascript2/search/searchcursor");
require("javascript2/search/search");
require("codemirror/addon/scroll/annotatescrollbar");
require("codemirror/addon/search/matchesonscrollbar");
require("codemirror/addon/search/jump-to-line");
require("codemirror/addon/selection/active-line");
require("./tablist");
require("javascript2/hint/javascript-hint");
require("codemirror/mode/javascript/javascript");
require("codemirror/mode/jsx/jsx");
require("codemirror/mode/htmlmixed/htmlmixed");
require("codemirror/mode/markdown/markdown");
require("codemirror/mode/sql/sql");
require("codemirror/mode/powershell/powershell");
require("javascript2/javascript2");
const {
  showToast,
  swStyle,
  saveLink,
  copyContent,
  getCodeModeIcon,
  getCodeModePlaceholder,
  ReadWriteOptions,
  EmptyResults,
  CodeMode,
  getCodeModeValue,
  getCodeModeTag,
  AnchorType,
  ModalType,
  CategoryFromType,
  minusCodeModeTag,
  getSchema,
  getTableName,
  getColumnName,
  getColumnType,
  isFailedResults,
  commentEncoder,
  commentDecoder,
  ShortcutTitle,
  code_editor_keys,
  cell_shortcuts_keys,
  linkKeys,
  DatabaseType, formatTable, columnFunc,
  FailedResults,
  assembleErrMessage,
  getColumnKeyType,
  getOperations,
  getCodeModeSvg,
} = require("../../util/utils");
const { parseCell, getCellName } = require("../../util/hqUtils");
const { Option, OptGroup } = Select;
const { SubMenu } = Menu;
const myCustomPlugin = {
  parsers: {
    "js-parser": {
      parse(text) {
        const ast = parserBabel.parsers.babel.parse(text);
        return ast;
      },
      astFormat: "estree",
    },
  },
};

const getVName = function (selectedCategoryFrom, selectedCategory) {
  if (selectedCategoryFrom === CategoryFromType.files) {
    return selectedCategory.toCommonName();
  } else {
    return `${selectedCategoryFrom}_${selectedCategory}`.toCommonName();
  }
};
const noSaveState = {
  /** open edit */
  openEdit: false,
  /**  */
  needRun: true,
  /** sure to delete current block */
  sure: false,
  /** */
  readwrite_mode: "CURRENT",
  view_code: false,
  /**@type {Map} */
  fileAttachments: undefined,
  importSource: undefined,
  visible: false,
  confirmLoading: false,
  acceptedFile: undefined,
  selectedFileName: undefined,
  readOnly: false,
  safeMode: false,
  lineNumbers: false,
  selected: false,
  openSelect: false,
  vimEnabled: false,
  insertMode: false,
  sqliteNames: [],
  nData: [],
  datasets: [],
  tables: [],
  columns: [],
  /**@type {Error} */
  error: undefined,
  /**@type {Error} */
  error2: undefined,
  codeLoading: false,
};
const defState = {
  viewInputs: false,
  syncFile: false,
  syncFileKey: undefined,
  variantName: undefined,
  db: undefined,
  dbTable: false,
  tableName: "",
  selectedTable: undefined,
  filters: [{}],
  selectedColumns: [],
  sorts: [{}],
  slice: { to: 100, from: 0 },
  selectedCategoryFrom: "",
  selectedCategory: "",
};
const CodeOptions = {
  mode: CodeMode.javascript2,
  lineNumbers: true,
  lineWrapping: true,
  viewportMargin: Infinity,
  styleSelectedText: true,
  matchBrackets: false,
  search: { bottom: false },
  // styleActiveLine: { nonEmpty: false },
  foldGutter: true,
  cursorBlinkRate: 530,
  placeholder: "Type,then shift-enter to run",
  gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
  searchbox: true,
};
const debug = false;
const dataShape = {
  value: PropTypes.string,
  pinCode: PropTypes.bool,
  /**flag the cell can not run,just display */
  deadCode: PropTypes.bool,
  /**flag the cell will hide when readonly mode */
  hide: PropTypes.bool,
  readOnly: PropTypes.bool,
  /**default name of block element */
  dname: PropTypes.string,
  /**js,js2,md */
  codeMode: PropTypes.string,
  viewInputs: PropTypes.bool,
  syncFile: PropTypes.bool,
  syncFileKey: PropTypes.string,
  variantName: PropTypes.string,
  db: PropTypes.string,
  /** flag is a Db-Table component */
  dbTable: PropTypes.bool,
  /** variant name of current DbTable or DatabaseQuery */
  tableName: PropTypes.string,
  selectedTable: PropTypes.string,
  filters: PropTypes.array,
  selectedColumns: PropTypes.array,
  sorts: PropTypes.array,
  slice: PropTypes.object,
  selectedCategoryFrom: PropTypes.string,
  selectedCategory: PropTypes.string,
};

const PLACEMENT = { placement: "top" };
const JUMP_TITLE = ShortcutTitle(code_editor_keys["Jump to defining cell"]);
const loadAndClearMarkers = function (cm) {
  if (!cm) { return; }
  if (!cm.textMarkers) {
    cm.textMarkers = [];
  }
  for (let i = 0; i < cm.textMarkers.length; i++) { cm.textMarkers[i].clear(); }
  cm.textMarkers.splice(0, cm.textMarkers.length);
}
export default class Code extends React.Component {
  static propTypes = {
    readOnly: PropTypes.bool,
    data: PropTypes.shape(dataShape),
    api: PropTypes.object,
    saveData: PropTypes.func,
    settingWrapper: PropTypes.object,
    editor: PropTypes.object,
  };

  static defaultProps = {};
  /**
   * Creates
   *
   * @constructor
   * @param {{api:import('@editorjs/editorjs').default}} props
   */
  constructor(props) {
    super(props);
    this.props = props;
    const {
      readOnly,
      data,
      data: { value, codeMode, db, sqlType, tableName, dbTable, selectedTable, filters, selectedColumns, sorts, slice, selectedCategoryFrom, selectedCategory, },
      editor,
    } = this.props;
    /**@type{import ('../../tagEditor').default} */
    this.editor = editor;
    let cell;
    try {
      cell = parseCell(
        getCodeModeValue(
          value,
          codeMode,
          db,
          sqlType,
          tableName,
          dbTable,
          selectedTable,
          filters,
          selectedColumns,
          sorts,
          slice
        )
      );
    } catch (err) { }
    /**@type {noSaveState & dataShape } */
    this.state = _.assign(
      {},
      _.cloneDeep(noSaveState),
      {
        readOnly: readOnly,
        fileAttachments: undefined,
        safeMode: commonData.getSafeMode(),
        lineNumbers: this.editor.getSettings().getLineNumbers(),
        vimEnabled: this.editor.getSettings().getVimEnabled(),
        sqliteNames: db ? [db] : [],
      },
      _.cloneDeep(defState),
      data
    );
    this.ref = React.createRef();
    this.syncRef = React.createRef();
    this.domRef = React.createRef();
    this.openSelectElement = document.createElement("div");
    this.view_codeElement = document.createElement("div");
    this.readwrite_modeElement = document.createElement("div");
    this.themeElement = document.createElement("div");
    this.safeModeElement = document.createElement("div");
    this.lineNumbersElement = document.createElement("div");
    this.vimEnabledElement = document.createElement("div");
    /**flag block removed */
    this.unmount = false;
    /**datas that will be instance to sqlite */
    this.selectedDatas = undefined;
    /**value that will run2 */
    this.cacheValue = "";
    /**flag cache run finished */
    this.cacheLoaded = false;
  }

  componentDidMount() {
    const { api, settingWrapper } = this.props;
    this.domRef.current.editCode = this.editCode.bind(this);
    this.domRef.current.runCode = this.runCode.bind(this);
    this.domRef.current.state = this.state;
    this.domRef.current.getAnchorValue = this.getAnchorValue;
    this.domRef.current.removeFile = this.removeFile.bind(this);
    this.domRef.current.replaceFile = this.replaceFile.bind(this);
    this.domRef.current.pinCodeFunc = this.pinCodeFunc.bind(this);
    this.domRef.current.hideCode = this.hideCode.bind(this);
    this.domRef.current.insertValue = this.insertValue.bind(this);
    this.domRef.current.syncFileFunc = this.syncFileFunc.bind(this);
    this.domRef.current.configSyncFile = this.configSyncFile.bind(this);
    this.domRef.current.cm = this.cm;
    actions.inspector(this.openSelectElement, [actions.types.OPEN_SELECT], ({ editorKey, openSelect }) => {
      if (this.editor.getTransferKey() === editorKey) {
        this.setState({ openSelect });
      }
    });
    actions.inspector(this.view_codeElement, [actions.types.VIEW_CODE], (view_code) => {
      this.setState({ view_code: view_code });
    });
    actions.inspector(this.readwrite_modeElement, [actions.types.READWRITE_MODE], (readwrite_mode) => {
      this.setState({ readwrite_mode: readwrite_mode });
    });
    actions.inspector(this.themeElement, [actions.types.THEME], (theme) => {
      this.setTheme(theme);
    });
    actions.inspector(this.safeModeElement, [actions.types.SAFE_MODE], (safeMode) => {
      this.setState({ safeMode });
    });
    actions.inspector(this.lineNumbersElement, [actions.types.LINE_NUMBERS], (lineNumbers) => {
      this.setState({ lineNumbers });
    });
    actions.inspector(this.vimEnabledElement, [actions.types.VIM_ENABLED], (vimEnabled) => {
      this.setState({ vimEnabled });
    });
    api.listeners.on(
      this.domRef.current.parentElement.parentElement.querySelector(".ce-block-border-left-wrap"),
      "mousedown",
      (e) => {
        if (e.target.tagName !== "SPAN") {
          setTimeout(this.editCode);
        }
      },
      false
    );
    api.listeners.on(
      this.domRef.current.parentElement.parentElement.querySelector(".ce-block-border-right-wrap"),
      "mousedown",
      (e) => {
        if (e.target.tagName !== "SPAN") {
          setTimeout(this.editCode);
        }
      },
      false
    );
    this.editorDidMount();
  }

  getVExpression = (selectedCategoryFrom, selectedCategory, selectedDatas, data) => {
    if (selectedCategoryFrom === CategoryFromType.files && isFileDb(this.editor.react_component.getFileData(selectedCategory))) {
      return `${selectedDatas} = await FileAttachment('${selectedCategory}').sqlite()`;
    } else if (selectedCategoryFrom === CategoryFromType.files && isFileExcel(this.editor.react_component.getFileData(selectedCategory))) {
      return `${selectedDatas} = await workbookToDb(await FileAttachment('${selectedCategory}').xlsx())`;
    } else if (selectedCategoryFrom === CategoryFromType.files && isFileArrow(this.editor.react_component.getFileData(selectedCategory))) {
      return `${selectedDatas} = await DuckDBClient.of({
        ${selectedCategory.substring(~selectedCategory.lastIndexOf(fileSeparator) ? selectedCategory.lastIndexOf(fileSeparator) + 1 : 0).toCommonName()}: await FileAttachment("${selectedCategory}")
      })`;
    } else if (selectedCategoryFrom === CategoryFromType.database) {
      return `${selectedDatas} = DatabaseClient('${selectedCategory}')`;
    } else {
      return `${selectedDatas} = await datasToDb('${selectedDatas}', ${JSON.stringify(data)});`;
    }
  }

  cacheRun = async () => {
    const { selectedCategoryFrom, selectedCategory } = this.state;
    if (selectedCategoryFrom && selectedCategory) {
      if (selectedCategoryFrom === CategoryFromType.shareData && (!ShareData[this.editor.getSelfModuleName()] || !ShareData[this.editor.getSelfModuleName()][selectedCategory])) {
        // setTimeout(() => { this.cacheRun() }, 200);
        return;
      }
      let selectedDatas = getVName(selectedCategoryFrom, selectedCategory);
      let cacheValue;
      if (~this.editor.getSqlites().indexOf(selectedDatas)) {
        cacheValue = `${selectedDatas}`;
      } else {
        const { data } = await getColumnsData(this.editor.getSelfModuleName(), selectedCategoryFrom, selectedCategory);
        if (this.unmount) {
          return;
        }
        cacheValue = this.getVExpression(selectedCategoryFrom, selectedCategory, selectedDatas, data);
      }
      this.selectedDatas = selectedDatas;
      await this.run_2(cacheValue, true);
      this.cacheValue = cacheValue;
    }
  };

  componentWillUnmount() {
    actions.deleteCache(this.openSelectElement);
    actions.deleteCache(this.view_codeElement);
    actions.deleteCache(this.readwrite_modeElement);
    actions.deleteCache(this.themeElement);
    actions.deleteCache(this.safeModeElement);
    actions.deleteCache(this.lineNumbersElement);
    actions.deleteCache(this.vimEnabledElement);
    this.unmount = true;
  }

  editorDidMount = () => {
    const { openEdit, pinCode, needRun, hide, syncFile, view_code, safeMode, readOnly, openSelect } = this.state;
    const { settingWrapper, api } = this.props;
    let editCodeBtn = settingWrapper.querySelector(".edit-code");
    if (openEdit) {
      editCodeBtn.classList.remove("anticon-muted");
      this.cm && this.cm.focus(); //need focus!!!!
      let index = api.blocks.getCurrentBlockIndex();
      api.caret.setToBlock(index, "start", 0);
    } else {
      editCodeBtn.classList.add("anticon-muted");
    }
    if (pinCode || safeMode) {
      let index = api.blocks.getCurrentBlockIndex();
      api.caret.setToBlock(index, "start", 0);
    }
    let runCodeBtn = settingWrapper.querySelector(".run-code");
    if (needRun) {
      runCodeBtn.classList.remove("anticon-muted");
    } else {
      runCodeBtn.classList.add("anticon-muted");
    }
    let hideCodeBtn = settingWrapper.querySelector(".hide-code");
    if (hide) {
      hideCodeBtn.classList.remove("anticon-muted");
    } else {
      hideCodeBtn.classList.add("anticon-muted");
    }
    let syncCsvFileBtn = settingWrapper.querySelector(".sync-csv-file");
    if (syncFile) {
      syncCsvFileBtn.classList.remove("anticon-muted");
    } else {
      syncCsvFileBtn.classList.add("anticon-muted");
    }
    if (this.domRef.current) {
      /**@type {HTMLFontElement} */
      let d = this.domRef.current.parentElement.parentElement.querySelector(".ce-block-border-left-wrap>i");
      d.className = `icon fas fa-chevron-${openEdit ? "down" : "right"} ${pinCode || safeMode || readOnly || openSelect ? "hide" : ""
        }`;
      d.setAttribute("title", openEdit ? "Edit cell" : "Close cell Esc");
    }
    if (pinCode || openEdit || view_code || safeMode) {
      let realValue = this.getRealValue();
      let cell;
      try {
        cell = realValue.trim() ? parseCell(realValue) : undefined;
      } catch (err) { }
      this.flagReference(cell);
    }
  };

  /**
   * flag cell variants
   * @param {*} cell 
   */
  flagReference(cell) {
    if (!this.cm || !cell) { return }
    loadAndClearMarkers(this.cm);
    const { codeMode, db, tableName, dbTable } = this.state;
    let minusNum = minusCodeModeTag(codeMode, db, tableName, dbTable);
    _.each(cell.references, (v) => {
      let desc = window.library && window.library[v.name]
        ? typeof window.runtime._builtin._scope.get(v.name)._value
        : JUMP_TITLE;
      if (codeMode === CodeMode.jsx) {
        let cursor = this.cm.getSearchCursor(v.name);
        while (cursor.findNext()) {
          let wordRange = this.cm.findWordAt(cursor.from());
          if (v.name === this.cm.getRange(wordRange.anchor, wordRange.head)) {
            this.cm.textMarkers.push(this.cm.markText(cursor.from(), cursor.to(), { className: "reference-mark", title: desc }));
          }
        }
      } else {
        this.cm.textMarkers.push(this.cm.markText(
          this.cm.posFromIndex(minusNum ? v.start - minusNum : v.start),
          this.cm.posFromIndex(minusNum ? v.end - minusNum : v.end),
          { className: "reference-mark", title: desc }
        ));
      }
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { openEdit, pinCode, safeMode, needRun, hide, openSelect, syncFile } = this.state;
    const { settingWrapper, saveData, data } = this.props;
    const readOnly = data.readOnly !== undefined ? data.readOnly : this.props.readOnly;
    if (this.state !== prevState) {
      this.domRef.current.state = this.state;
    }
    if (readOnly !== this.state.readOnly) {
      this.setState({ readOnly: readOnly });
      this.cm && this.cm.setOption("readOnly", readOnly);
    }
    if (openEdit !== prevState.openEdit) {
      let editCodeBtn = settingWrapper.querySelector(".edit-code");
      if (openEdit) {
        editCodeBtn.classList.remove("anticon-muted");
        this.cm && this.cm.focus();
      } else {
        editCodeBtn.classList.add("anticon-muted");
      }
    }
    if (needRun !== prevState.needRun) {
      let runCodeBtn = settingWrapper.querySelector(".run-code");
      if (needRun) {
        runCodeBtn.classList.remove("anticon-muted");
      } else {
        runCodeBtn.classList.add("anticon-muted");
      }
    }
    if (hide !== prevState.hide) {
      let hideCodeBtn = settingWrapper.querySelector(".hide-code");
      if (hide) {
        hideCodeBtn.classList.remove("anticon-muted");
      } else {
        hideCodeBtn.classList.add("anticon-muted");
      }
    }
    let syncCsvFileBtn = settingWrapper.querySelector(".sync-csv-file");
    if (syncFile) {
      syncCsvFileBtn.classList.remove("anticon-muted");
    } else {
      syncCsvFileBtn.classList.add("anticon-muted");
    }
    if (
      openEdit !== prevState.openEdit ||
      pinCode !== prevState.pinCode ||
      safeMode !== prevState.safeMode ||
      readOnly !== prevState.readOnly ||
      openSelect !== prevState.openSelect
    ) {
      /**@type {HTMLFontElement} */
      let d = this.domRef.current.parentElement.parentElement.querySelector(".ce-block-border-left-wrap>i");
      d.className = `icon fas fa-chevron-${openEdit ? "down" : "right"} ${pinCode || safeMode || readOnly || openSelect ? "hide" : ""
        }`;
    }
    if (!_.isEqual(prevState, this.state)) {
      this.domRef.current && (this.domRef.current.state = this.state);
      let dataTmp = this.getData(this.state);
      let prevDataTmp = this.getData(prevState);
      if (!_.isEqual(prevDataTmp, dataTmp) || this.needSave) {
        saveData(dataTmp);
        this.needSave = false;
      }
    }
  }

  getData(stateData) {
    let dataTmp = {};
    _.each(_.keys(stateData), (key) => {
      if (!(_.has(noSaveState, key) || (_.has(defState, key) && _.isEqual(defState[key], stateData[key])))) {//if not (not need save state or default state) then save it
        dataTmp[key] = _.cloneDeep(stateData[key]);
      }
    });
    return dataTmp;
  }

  /**
   * edit code
   */
  editCode = (open) => {
    const { readOnly, error, codeMode, dbTable } = this.state;
    if (!readOnly && (undefined === open || open !== this.state.openEdit)) {
      this.setState(
        (state) => {
          return { openEdit: !state.openEdit };
        },
        () => { }
      );
    } else {
      if (this.domRef.current) {
        if (!isVisibleInViewport(this.domRef.current)) {
          this.domRef.current.scrollIntoView({
            behavior: "auto",
            block: "nearest",
            inline: "nearest",
          });
        }
        this.focusResult(error);
        let codeEle = this.ref.current;
        if (codeEle.parentNode && codeEle.parentNode.getAttribute("id")) {
          let anchor = document.querySelector(
            `.editorjs-wrapper.${this.editor.getTransferKey()}>.editorjs-anchor a[href='#${codeEle.parentNode.getAttribute(
              "id"
            )}']`
          );
          anchor && anchor.scrollIntoView({ behavior: "auto", block: "center", inline: "nearest" });
        }
        this.focusAnchor();
      }
      this.cm && this.cm.focus();
    }
  };

  /**
   * run code
   * @param {boolean} force force run
   * @param {boolean} now run it right now
   */
  runCode = async (force = false, now = true) => {
    const { needRun, deadCode, safeMode, codeMode, db } = this.state;
    if (deadCode || codeMode === CodeMode.javascript || safeMode || this.unmount) {
      return;
    }
    if (needRun || force) {
      if (db && ~[CodeMode.sql].indexOf(codeMode) && !this.state.codeLoading) {
        this.setCodeLoading(true);
      }
      await this.run(now);
    }
  };

  pinCodeFunc = (pin) => {
    this.setState(
      (state) => {
        let nstate = {};
        if (!(undefined !== pin && pin === state.pinCode)) {
          _.assign(nstate, { pinCode: !state.pinCode });
        }
        return nstate;
      },
      () => {
        this.cm && this.cm.focus();
      }
    );
  };

  setTheme = (theme) => {
    this.cm && this.cm.setOption("theme", theme);
  };

  insertValue = (value) => {
    this.cm && this.cm.replaceSelection(value, "around");
  };

  hideCode = (cb) => {
    this.setState(
      (state) => {
        return { hide: !state.hide };
      },
      () => {
        this.cm && this.cm.focus();
      }, cb);
  };

  isDeclare = (text) => { };

  getRealValue = (copy = false) => {
    const { value, codeMode, db, sqlType, tableName, dbTable, selectedTable, filters, selectedColumns, sorts, slice } =
      this.state;
    return getCodeModeValue(
      value,
      codeMode,
      db,
      sqlType,
      tableName,
      dbTable,
      selectedTable,
      filters,
      selectedColumns,
      sorts,
      slice
    );
  };

  formatCode = (cm) => {
    try {
      const { codeMode, value } = this.state;
      if (!value) {
        return;
      }
      let formatValue;
      if (codeMode === CodeMode.javascript) {
        let re = commentEncoder(value);
        formatValue = prettier.format(re.realValue, {
          parser: "js-parser",
          plugins: [myCustomPlugin],
        });
        formatValue = commentDecoder(re.map, formatValue, re.first, re.end);
      } else if (codeMode === CodeMode.javascript2) {
        let realValue = this.getRealValue();
        let re = commentEncoder(realValue);
        let cell = undefined;
        try {
          cell = re.realValue.trim() ? parseCell(re.realValue) : undefined;
        } catch (err) { }
        if (!cell) {
          return;
        }
        formatValue = commentDecoder(re.map, prettierStandalone.format(".", { parser: () => cell }), re.first, re.end);
      } else {
        return;
      }
      cm.getDoc().setValue(formatValue);
    } catch (e) {
      console.error(e);
      // ignore
    }
  };

  copyLink = () => {
    let data = this.ref.current.querySelector(":scope > .data");
    let codeEle = this.ref.current;
    let str = `html\`<a href="#${(codeEle.parentNode && codeEle.parentNode.getAttribute("id")) || ""}">${(data && data.cacheNames) || (codeEle.parentNode && codeEle.parentNode.getAttribute("id")) || ""
      }</a>\``;
    copyContent(str);
  };

  /**
   *
   * @param {boolean} now run it right now
   */
  run = async (now = false) => {
    let realValue = this.getRealValue();
    let declare = this.ref.current.querySelector(":scope > .declare");
    declare.innerHTML = "";
    let data = this.ref.current.querySelector(":scope > .data");
    data.innerHTML = "&nbsp;";
    let once = false;
    const { syncFile, variantName, initRead, dname, viewInputs, codeMode, syncFileKey } = this.state;
    if (syncFile && variantName) {
      realValue = proxyArrayCode({ variantName, dname, editorjsKey: this.editor.getTransferKey() });
    }
    let cell;
    try {
      cell = realValue.trim() ? parseCell(realValue) : undefined;
    } catch (err) { }
    await this.editor.runFunc(
      realValue,
      declare,
      data,
      async () => {
        if (this.unmount) {
          return;
        }
        if (this.editor.getSettings().getAutoFormat() && this.cm) {
          this.formatCode(this.cm);
        }
        this.focusResult();
        const { db, tableName, selectedTable, needRun, fileAttachments, importSource, error, selectedCategoryFrom, selectedCategory } = this.state;
        let state = {
          needRun: false,
          fileAttachments: cell ? cell.fileAttachments : (selectedCategoryFrom === CategoryFromType.files && selectedCategory ?
            new Map().set(~selectedCategory.indexOf(fileSeparator) ? selectedCategory.substring(selectedCategory.lastIndexOf(fileSeparator) + 1) : selectedCategory, selectedCategory) :
            undefined),
          importSource: cell && cell.body && cell.body.type === 'ImportDeclaration' ? cell.body.source.value : undefined,
          error: undefined
        };
        let oldState = { needRun, fileAttachments, importSource, error };
        if (db) {
          let nData;
          const { variables } = this.editor.getMain()._cachedata;
          if (tableName) {
            nData = variables[tableName].varia._value;
          } else if (ShareData[this.editor.getSelfModuleName()] && ShareData[this.editor.getSelfModuleName()][`_${dname.replaceNoWordToUnderline()}`]) {
            nData = ShareData[this.editor.getSelfModuleName()][`_${dname.replaceNoWordToUnderline()}`];
          }
          if (nData) {
            if (!nData.length && selectedTable) {
              nData = EmptyResults;
            }
            _.assign(state, { nData });
          }
          if (variables[db] && variables[db].varia._value) {
            try {
              let dbClient = variables[db].varia._value;
              if (dbClient.dialect === 'duckdb' && nData && nData.length) {
                nData = _.reduce(nData, (prev, data, index) => {
                  prev.push(_.reduce(data, (p, v, k) => {
                    p[k] = v;
                    return p;
                  }, {}))
                  return prev;
                }, [])
                _.assign(state, { nData });
              }
              let datasets, tables;
              if (!this.state.datasets.length && !this.state.tables.length) {
                if (dbClient.dialect === DatabaseType.BigQuery) {
                  datasets = await dbClient.getDatasets();
                  if (this.unmount) {
                    return;
                  }
                  if (isFailedResults(datasets)) {
                    datasets = [];
                  }
                  if (datasets && datasets.length) {
                    let promiseList = [];
                    _.each(datasets, (dataset) => {
                      promiseList.push(dbClient.describeTables({ schema: dataset.id, location: dataset.location }));
                    })
                    let tbs = await Promise.all(promiseList);
                    tables = _.concat(...tbs);
                  } else {
                    tables = assembleErrMessage("Haven't datasets!");
                  }
                } else {
                  datasets = [];
                  tables = await dbClient.describeTables();
                }
                if (this.unmount) {
                  return;
                }
                if (isFailedResults(tables)) {
                  tables = [];
                }
                _.assign(state, { datasets, tables });
              }
              if (selectedTable) {
                if (!this.state.columns.length) {
                  const [schema, table] =
                    selectedTable && ~selectedTable.indexOf(".") ? selectedTable.split(".") : [null, selectedTable];
                  let columns = await dbClient.describeColumns({ schema, table });
                  if (this.unmount) {
                    return;
                  }
                  _.assign(state, { columns });
                }
              }
            } catch (e) {
              console.error(e);
            }
          }
        }
        if (!(_.isEqual(state, oldState) && once)) {
          //may be successFunc is called as a timer when use variant "now" etc.
          once = true;
          if (this.unmount) {
            return;
          }
          this.state.codeLoading && this.setCodeLoading(false);
          this.setState(state, () => {
            if (!_.isEqual(fileAttachments, this.state.fileAttachments)) {
              this.editor.refreshAttaches();
            }
            this.refreshAnchors(now);
          });
          this.flagReference(cell);
        }
      },
      (err) => {
        if (this.unmount) {
          return;
        }
        this.focusResult(err);
        const { fileAttachments, codeMode } = this.state;
        if (err && (typeof err.message === "string" || err.message instanceof String)) {
          let tagLen = getCodeModeTag(codeMode).length;
          tagLen = tagLen ? tagLen + 1 : tagLen;
          if (undefined !== err.pos && undefined !== err.raisedAt) {
            if (this.cm) {
              loadAndClearMarkers(this.cm);
              this.cm.textMarkers.push(this.cm.markText(
                this.cm.posFromIndex(err.pos - tagLen),
                this.cm.posFromIndex((err.raisedAt != err.pos ? err.raisedAt : err.raisedAt + 1) - tagLen),
                { className: "b--error-color" }
              ));
            }
          } else if (String(err.message).endsWith(" is not defined")) {
            let reference = err.message.substring(0, err.message.indexOf(" is not defined"));
            let foundErrorCellName = false;
            _.each(cell.references, (v) => {
              if (v.name === reference) {
                if (this.cm) {
                  loadAndClearMarkers(this.cm);
                  this.cm.textMarkers.push(this.cm.markText(
                    this.cm.posFromIndex(v.start - tagLen),
                    this.cm.posFromIndex(v.end - tagLen),
                    { className: "b--error-color" }
                  ));
                  foundErrorCellName = true;
                }
                return false;
              }
            });
            if (!foundErrorCellName) {
              /////// TODO
            }
          } else if (err.message === "array not defined!") {
            setTimeout(this.runCode, 200);
          }
        }
        this.setState({
          needRun: true,
          fileAttachments: cell ? cell.fileAttachments : undefined,
          importSource: cell && cell.body && cell.body.type === 'ImportDeclaration' ? cell.body.source.value : undefined,
          error: err
        }, () => {
          if (!_.isEqual(fileAttachments, this.state.fileAttachments)) {
            this.editor.refreshAttaches();
          }
          this.refreshAnchors(now);
          this.state.codeLoading && this.setCodeLoading(false);
        });
      },
      now,
      `_${dname.replaceNoWordToUnderline()}`
    );
    if ((syncFile && variantName) || viewInputs || (CodeMode.sql === codeMode && this.cacheValue.trim())) {
      let realValue_2;
      if (syncFile && variantName) {
        let content =
          syncFileKey && initRead
            ? dsv(await this.editor.react_component.getRealFileContentByFileKey(syncFileKey), ",")
            : [];
        if (this.unmount) {
          return;
        }
        realValue_2 = `${variantName}_origin = {
                    let initData = JSON.parse(\`${JSON.stringify(content)}\`);
                    let a =  this || initData;
                    return a;
                }`;
      } else if (viewInputs) {
        try {
          let cell = realValue.trim() ? parseCell(realValue) : undefined;
          if (cell && cell.id !== null) {
            if (~["ViewExpression"].indexOf(cell.id.type)) {
              realValue_2 = cell.id.id.name;
            }
          }
        } catch (err) { }
      } else if (CodeMode.sql === codeMode && this.cacheValue.trim()) {
        realValue_2 = this.cacheValue.trim();
      }
      if (undefined !== realValue_2) {
        this.run_2(realValue_2, now);
      }
    }
  };

  run_2 = async (realValue_2, now = false) => {
    const { dname, codeMode } = this.state;
    if (!this.syncRef.current) {
      return;
    }
    let declare = this.syncRef.current.querySelector(":scope > .declare");
    declare.innerHTML = "";
    let data = this.syncRef.current.querySelector(":scope > .data");
    data.innerHTML = "&nbsp;";
    await this.editor.runFunc(
      realValue_2,
      declare,
      data,
      () => {
        if (this.unmount) {
          return;
        }
        this.focusResult2();
        const { fileAttachments, selectedCategoryFrom, selectedCategory } = this.state;
        let state = _.assign(
          { error2: undefined },
          selectedCategoryFrom === CategoryFromType.files && selectedCategory ? {
            fileAttachments: new Map().set(~selectedCategory.indexOf(fileSeparator) ? selectedCategory.substring(selectedCategory.lastIndexOf(fileSeparator) + 1) : selectedCategory, selectedCategory)
          } : {})
        this.setState(state, () => {
          if (!_.isEqual(fileAttachments, this.state.fileAttachments)) {
            this.editor.refreshAttaches();
          }
          this.refreshAnchors(now);
        });
      },
      (err) => {
        if (this.unmount) {
          return;
        }
        this.focusResult2(err);
        const { fileAttachments, selectedCategoryFrom, selectedCategory } = this.state;
        let state = _.assign(
          { error2: err },
          selectedCategoryFrom === CategoryFromType.files && selectedCategory ? {
            fileAttachments: new Map().set(~selectedCategory.indexOf(fileSeparator) ? selectedCategory.substring(selectedCategory.lastIndexOf(fileSeparator) + 1) : selectedCategory, selectedCategory)
          } : {})
        this.setState(state, async () => {
          if (!_.isEqual(fileAttachments, this.state.fileAttachments)) {
            this.editor.refreshAttaches();
          }
          this.refreshAnchors(now);
          if (CodeMode.sql === codeMode && err.message === "can not repeat declare!" && err.name) {
            this.selectedDatas = err.name;
            let cacheValue = `${this.selectedDatas}`;
            await this.run_2(cacheValue, true);
            this.cacheValue = cacheValue;
          }
        });
      },
      now,
      `_2_${dname.replaceNoWordToUnderline()}`
    );
  };

  focusResult = (err) => {
    swStyle(this.ref.current, err);
  };

  focusResult2 = (err) => {
    swStyle(this.syncRef.current, err);
  };

  getAnchorValue = () => {
    const { value, codeMode, dbTable, selectedCategoryFrom, selectedCategory } = this.state;
    if (CodeMode.sql === codeMode && dbTable) {
      return `DataTable ${selectedCategoryFrom || ""} ${selectedCategory || ""}`;
    }
    return value;
  };

  getAnchorState = () => {
    let codeEle = this.ref.current;
    if (codeEle.parentNode && codeEle.parentNode.getAttribute("id")) {
      let anchor = document.querySelector(
        `.editorjs-wrapper.${this.editor.getTransferKey()}>.editorjs-anchor a[href='#${codeEle.parentNode.getAttribute(
          "id"
        )}']`
      );
      if (anchor) {
        const { error, error2, value } = this.state;
        let data = this.domRef.current.querySelector(".code-edit-result.display > .data");
        if (data) {
          let className = `editorjs-anchor-${codeEle.parentNode.getAttribute("id").replace(/\s/g, "_")}`;
          let type = AnchorType.Common;
          if (error || error2) {
            type = AnchorType.Error;
          } else if (data.cacheNames) {
            type = AnchorType.Variant;
          }
          let args;
          if (data.cacheNames) {
            args = data.args || [];
          } else {
            args = (data.vary && data.vary.args) || [];
          }
          return { className, value: this.getAnchorValue(), type, args, cacheNames: data.cacheNames };
        }
      }
    }
  };

  refreshAnchors = (now = false) => {
    if (now) {
      const { api } = this.props;
      if (~api.blocks.getCurrentBlockIndex()) {
        let block = api.blocks.getBlockByIndex(api.blocks.getCurrentBlockIndex());
        if (
          block &&
          block.name === "codeTool" &&
          block.holder &&
          block.holder.querySelector(".code-edit") &&
          this.domRef.current &&
          block.holder.querySelector(".code-edit") === this.domRef.current
        ) {
          this.editor.refreshAnchors(this.getAnchorState());
        } else {
          this.editor.refreshAnchors();
        }
      }
    } else if (this.editor.runComplete) {
      this.editor.refreshAnchors();
    }
  };

  focusAnchor = () => {
    let anchorState = this.getAnchorState();
    if (anchorState) {
      let anchor = document.querySelector(
        `.editorjs-wrapper.${this.editor.getTransferKey()}>.editorjs-anchor a[href='#${this.ref.current.parentNode.getAttribute(
          "id"
        )}']`
      );
      anchor && anchor.scrollIntoView({ behavior: "auto", block: "center", inline: "nearest" });
      this.editor.ref.current && this.editor.ref.current.focus(anchorState);
    }
  };

  showModal = (selectedFileName) => {
    this.setState({ selectedFileName, visible: true });
  };

  handleOk = () => {
    const { acceptedFile, selectedFileName } = this.state;
    if (acceptedFile) {
      this.editor.onImportFiles([acceptedFile]).then((values) => {
        if (!values || values.length !== 1) {
          this.setState({ visible: false, acceptedFile: undefined });
          return;
        }
        this.replaceFile(selectedFileName, values[0], { visible: false, acceptedFile: undefined });
      });
    } else {
      this.setState({ visible: false, acceptedFile: undefined });
    }
  };

  removeFile = (selectedFileName, stateTmp) => {
    const { fileAttachments, value, codeMode, db, tableName, dbTable } = this.state;
    let minus = minusCodeModeTag(codeMode, db, tableName, dbTable);
    if (!fileAttachments || fileAttachments.size <= 0) {
      return;
    }
    const files = fileAttachments.get(selectedFileName);
    if (!files) {
      return;
    }
    let state = stateTmp || {};
    this.setState(
      _.assign(state, {
        needRun: true,
      }),
      () => {
        this.runCode(false, true);
      }
    );
  };

  replaceFile = (selectedFileName, fileName, stateTmp) => {
    const { fileAttachments, value, codeMode, db, tableName, dbTable } = this.state;
    let minus = minusCodeModeTag(codeMode, db, tableName, dbTable);
    if (!fileAttachments || fileAttachments.size <= 0) {
      return;
    }
    const files = fileAttachments.get(selectedFileName);
    if (!files) {
      return;
    }
    let state = stateTmp || {};
    let str = "";
    let startIndex = 0;
    _.each(files, (file, index, files) => {
      str = `${str}${value.substring(startIndex, file.start - minus)}"${fileName}"`;
      if (index < files.length - 1) {
        startIndex = file.end - minus;
      } else {
        str = `${str}${value.substring(file.end - minus)}`;
      }
    });
    this.setState(
      _.assign(state, {
        value: str,
        needRun: true,
      }),
      () => {
        this.runCode(false, true);
      }
    );
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  onBlurFunc = () => {
    const { openEdit, needRun } = this.state;
    (() => {
      const { settingWrapper } = this.props;
      let editCodeBtn = settingWrapper.querySelector(".edit-code");
      if (editCodeBtn && editCodeBtn.parentElement.querySelector(":hover") === editCodeBtn) {
        return;
      }
      let exceptEles = [
        ".code-edit-wrapper-icons",
        ".code-edit-wrapper-tools",
        ".ce-block-border-left-wrap",
        ".ce-block-border-right-wrap",
      ];
      for (let index = 0; index < exceptEles.length; index++) {
        let editIcons =
          this.domRef.current && this.domRef.current.parentElement.parentElement.querySelector(exceptEles[index]);
        if (editIcons && editIcons.parentElement.querySelector(":hover") === editIcons) {
          return;
        }
      }
      let editIcons = document
        .querySelector(`.editorjs-wrapper.${this.editor.getTransferKey()}`)
        .querySelector(".ce-toolbar__settings-btn");
      if (editIcons && editIcons.parentElement.querySelector(":hover") === editIcons) {
        return;
      }
      // let dialog = document.querySelector(`.editorjs-wrapper.${this.editor.getTransferKey()}`).querySelector(".CodeMirror-dialog");
      // console.log(dialog && dialog.querySelector(':focus'))
      // if (dialog && dialog.querySelector(':focus') === dialog.querySelector("input")) {
      //     return;
      // }
      let exEdits = document.querySelectorAll(".bottom-affix i.icon");
      for (let index = 0; index < exEdits.length; index++) {
        if (exEdits[index] && exEdits[index].parentElement.querySelector(":hover") === exEdits[index]) {
          return;
        }
      }
      // openEdit && this.editCode();
    })();
    if (!this.editor.getSettings().getVimEnabled()) {
      this.editor.runComplete && needRun && this.runCode();
    }
  };

  async syncFileFunc(change, jsonContent) {
    const { syncFileKey, syncFile } = this.state;
    let react_component = this.editor.react_component;
    if (syncFile && change && syncFileKey) {
      let content;
      if (syncFileKey.toLowerCase().endsWith(".json")) {
        content = JSON.stringify(jsonContent);
      } else if (syncFileKey.toLowerCase().endsWith(".csv")) {
        content = ConvertToCSV(jsonContent);
      }
      if (!react_component.state.fileNamesJson[syncFileKey]) {
        await react_component.newFileFunc({ target: { value: getFileName(syncFileKey) } }, content, syncFileKey, false);
      } else {
        await react_component.uploadMdFile(react_component.state.fileNamesJson[syncFileKey], undefined, content);
      }
    }
  }

  syncCsvFile() {
    this.setState(
      (state) => {
        let nstate = { syncFile: !state.syncFile };
        if (!state.syncFile) {
          _.assign(nstate, { value: "", fileAttachments: undefined }, { syncFileKey: "" });
        } else {
          _.assign(nstate, { value: "", pinCode: true, fileAttachments: undefined });
        }
        return nstate;
      },
      () => {
        let declare = this.ref.current.querySelector(":scope > .declare");
        declare.innerHTML = "";
        let data = this.ref.current.querySelector(":scope > .data");
        data.innerHTML = "&nbsp;";
        this.editor.deleteCacheV(data);

        let sync_declare = this.syncRef.current.querySelector(":scope > .declare");
        sync_declare.innerHTML = "";
        let sync_data = this.syncRef.current.querySelector(":scope > .data");
        sync_data.innerHTML = "&nbsp;";
        this.editor.deleteCacheV(sync_data);
        const { syncFile } = this.state;
        syncFile && this.configSyncFile();
      }
    );
  }

  configSyncFile() {
    const { dname, syncFileKey, variantName } = this.state;
    let realValue = this.getRealValue();
    let shareDataKey;
    try {
      let cell = realValue.trim() ? parseCell(realValue) : undefined;
      if (cell && cell.id !== null) {
        if (~["ViewExpression", "MutableExpression"].indexOf(cell.id.type)) {
          shareDataKey = cell.id.id.name;
        } else {
          shareDataKey = cell.id.name;
        }
      }
      shareDataKey = shareDataKey || dname;
    } catch (err) {
      console.error(err);
      showToast(err);
      return;
    }
    this.editor.react_component.setState({
      modalType: ModalType.SyncFile,
      treeSelectType: SelectType.file,
      sourceFile: null,
      modalData: {
        content: JSON.stringify(ShareData[this.editor.getSelfModuleName()] && ShareData[this.editor.getSelfModuleName()][shareDataKey] || []),
        syncFileKey,
        variantName,
        cbFunc: ({ syncFileKey, variantName, initRead }) => {
          let value = "";
          this.setState({ syncFileKey, variantName, initRead }, () => {
            this.runCode(true);
          });
        },
      },
    });
  }

  handleSelectDatabaseChange = async (db) => {
    let datasets, tables;
    let sqlType;
    this.setCodeLoading(true);
    if (db) {
      const dbClient = this.editor.getMain()._cachedata.variables[db].varia._value;
      sqlType = dbClient.dialect;
      if (sqlType === DatabaseType.BigQuery) {
        datasets = await dbClient.getDatasets();
        if (this.unmount) {
          return;
        }
        if (isFailedResults(datasets)) {
          datasets = [];
        }
        if (datasets && datasets.length) {
          let promiseList = [];
          _.each(datasets, (dataset) => {
            promiseList.push(dbClient.describeTables({ schema: dataset.id, location: dataset.location }));
          })
          let tbs = await Promise.all(promiseList);
          tables = _.concat(...tbs);
        } else {
          tables = assembleErrMessage("Haven't datasets!");
        }
      } else {
        datasets = [];
        tables = await dbClient.describeTables();
      }
      if (this.unmount) {
        return;
      }
      if (isFailedResults(tables)) {
        tables = [];
      }
    } else {
      datasets = [];
      tables = [];
      sqlType = undefined;
    }
    this.setState({ needRun: true, db, sqlType, datasets, tables, selectedTable: undefined, nData: [] }, async () => {
      await this.runCode();
      this.setCodeLoading(false);
    });
  };

  setCodeLoading(codeLoading) {
    this.setState({ codeLoading });
  }

  handleSelectTableChange = async (selectedTable, operationType) => {
    const [schema, table] =
      selectedTable && ~selectedTable.indexOf(".") ? selectedTable.split(".") : [null, selectedTable];
    const { db, dbTable, datasets } = this.state;
    let dbClient = this.editor.getMain()._cachedata.variables[db].varia._value;
    if (dbClient.dialect === DatabaseType.BigQuery && schema) {
      let arr = _.filter(datasets, (dataset) => { return dataset.id === schema });
      dbClient.location = arr.length ? arr[0].location : undefined;
    }
    if (dbTable) {
      if (!selectedTable) {
        this.setState(
          {
            needRun: true,
            selectedTable: undefined,
            columns: [],
            nData: [],
            filters: [{}],
            selectedColumns: [],
            sorts: [{}],
            slice: { to: 100, from: 0 },
          },
          async () => {
            await this.runCode();
          }
        );
        return;
      }
      this.setCodeLoading(true);
      let columns = await dbClient.describeColumns({ schema, table });
      if (this.unmount) {
        return;
      }
      let columnNames = _.reduce(
        columns,
        (prev, column, index) => {
          prev.push(getColumnName(column));
          return prev;
        },
        []
      );
      this.setState(
        {
          needRun: true,
          selectedTable,
          columns,
          nData: [],
          filters: [{}],
          selectedColumns: columnNames,
          sorts: [{}],
          slice: { to: 100, from: 0 },
        },
        async () => {
          await this.runCode();
        }
      );
    } else {
      if (!selectedTable) {
        this.setState({
          selectedTable: undefined, columns: []
        }, () => {
        });
        return;
      }
      let columns = await dbClient.describeColumns({ schema, table });
      if (this.unmount) {
        return;
      }
      let state = { columns, selectedTable, needRun: true };
      const { value } = this.state;
      switch (operationType) {
        case 'Scan table':
          insertText(this.cm, `SELECT * FROM ${formatTable(selectedTable, dbClient.dialect)} LIMIT 100`);
          // _.assign(state, { value: `${value}${`SELECT * FROM ${formatTable(selectedTable, dbClient.dialect)}`}` })
          break;
        case 'Query table':
          let c = getColumnName(columns[0]);
          insertText(this.cm, `SELECT * FROM ${formatTable(selectedTable, dbClient.dialect)} WHERE ${columnFunc(c, dbClient.dialect)} = `)
          // _.assign(state, { value: `${value}${`SELECT * FROM ${formatTable(selectedTable, dbClient.dialect)} WHERE ${columnFunc(c, dbClient.dialect)} = `}` })
          break;
        case 'Table name':
          insertText(this.cm, selectedTable);
          // _.assign(state, { value: `${value}${selectedTable}` })
          break;
      }
      this.setState(state);
    }
  };

  DatabaseComp = () => {
    const { api } = this.props;
    const {
      openEdit,
      pinCode,
      safeMode,
      nData,
      sqlType,
      db,
      dbTable,
      selectedTable,
      tables,
      needRun,
      sqliteNames,
      tableName,
      filters,
      columns,
      selectedColumns,
      sorts,
      slice,
      dname,
      selectedCategoryFrom,
      selectedCategory,
    } = this.state;
    let dbClient = this.editor.getMain()._cachedata.variables[db] ? this.editor.getMain()._cachedata.variables[db].varia._value : undefined;
    const filterButton = withPopMenu(FilterComp, {
      icon: <FilterSvgIcon />,
      label: (
        <span>
          <span className="button-label">Filter&nbsp;&nbsp;</span>
          {filters.length - 1}
        </span>
      ),
      columns: columns,
      filters: filters,
      setFilters: (filters) => {
        if (!_.isEqual(filters[filters.length - 1], {})) {
          filters.push({});
        }
        this.setState({ needRun: true, filters/*, nData: []*/ });
        this.needSave = true;
      },
    });
    const columnsButton = withPopMenu(ColumnsComp, {
      icon: <CheckboxSvgIcon />,
      label: (
        <span>
          <span className="button-label">Columns&nbsp;&nbsp;</span>
          {selectedColumns.length}
        </span>
      ),
      plainOptions: _.reduce(
        columns,
        (prev, column, index) => {
          prev.push({
            value: getColumnName(column),
            label: (
              <div className="d-flex justify-content-between">
                <span>{getColumnName(column)}&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <span className="text-muted small">{getColumnType(column)}{getColumnKeyType(column)}</span>
              </div>
            ),
          });
          return prev;
        },
        []
      ),
      checkedList: selectedColumns,
      setCheckedList: (checkedList) => {
        this.setState({ needRun: true, selectedColumns: checkedList/*, nData: []*/ });
        this.needSave = true;
      },
    });
    const columnsDetailsButton =
      !dbTable &&
      withPopMenu(ColumnsDetailComp, {
        label: (
          <span title={`Table columns`}>
            <ColumnsSvgIcon />&nbsp;&nbsp;{columns.length}
          </span>
        ),
        columns: _.map(columns, (column, index) => {
          return (
            <div key={index} className="d-flex justify-content-between" style={{ cursor: "pointer" }} onClick={() => {
              insertText(this.cm, getColumnName(column));
              // copyContent(getColumnName(column));
            }}>
              <span>{getColumnName(column)}&nbsp;&nbsp;&nbsp;&nbsp;</span>
              <span className="text-muted small">{getColumnType(column)}{getColumnKeyType(column)}</span>
            </div>
          );
        }),
      });
    const sortButton = withPopMenu(SortComp, {
      icon: <SortSvgIcon />,
      label: (
        <span>
          <span className="button-label">Sort&nbsp;&nbsp;</span>
          {sorts.length - 1}
        </span>
      ),
      columns: columns,
      sorts: sorts,
      setSorts: (sorts) => {
        if (!_.isEqual(sorts[sorts.length - 1], {})) {
          sorts.push({});
        }
        this.setState({ needRun: true, sorts/*, nData: []*/ });
        this.needSave = true;
      },
    });
    const sliceButton = withPopMenu(SliceComp, {
      icon: <CutSvgIcon />,
      label: (
        <span>
          <span className="button-label">Slice&nbsp;&nbsp;</span>[{slice.from}, {slice.to}]
        </span>
      ),
      slice: slice,
      setSlice: (slice) => {
        this.setState({ needRun: true, slice/*, nData: []*/ });
        this.needSave = true;
      },
      dialect: dbClient && dbClient.dialect
    });
    const convertSqlButton = <Dropdown disabled={!db} overlay={<Menu className="data-html2canvas-ignore normal-icon" onClick={async (info) => {
      if (!db) {
        return;
      }
      let dbClient = this.editor.getMain()._cachedata.variables[db].varia._value;
      if (!dbClient) {
        return;
      }
      let operations = getOperations(db, sqlType, selectedTable, filters, selectedColumns, sorts, slice);
      if (!operations) {
        return;
      }
      let sql = await __query.getSql(dbClient, operations);
      switch (info.key) {
        case "Convert":
          this.setState({ needRun: true, dbTable: undefined, value: sql })
          this.needSave = true;
          break;
        case "Copy":
          copyContent(sql);
          break;
      }
    }}>
      <Menu.Item key="Convert">
        Convert to SQL
      </Menu.Item>
      <Menu.Item key="Copy">
        Copy SQL
      </Menu.Item>
    </Menu>} placement="bottomLeft">
      <Button disabled={!db} icon={<LevelDownIcon />}>
        <span>
          <span className="button-label">SQL</span>
        </span>
      </Button></Dropdown>
    return (
      <div className={`database ${openEdit || pinCode || safeMode || !nData || !nData.length ? "" : "hide"}`}>
        <div>
          <Input
            onBlur={this.onBlurFunc}
            style={{ width: 120 }}
            allowClear
            value={tableName}
            placeholder="unnamed"
            onChange={(e) => {
              this.setState({ needRun: true, tableName: e.target.value });
            }}
          />
          &nbsp;=&nbsp;
          <ChooseDatas
            optionTypes={[CategoryFromType.files, CategoryFromType.main, CategoryFromType.shareData, CategoryFromType.database]}
            editor={this.editor}
            dname={dname}
            selectedCategoryFrom={selectedCategoryFrom}
            selectedCategory={selectedCategory}
            onCategoryChanged={async ({ selectedCategoryFrom, selectedCategory, refresh }) => {
              if (!this.editor.runComplete) {
                return;
              }
              let cacheValue,
                selectedDatas = undefined,
                state = {
                  db: undefined,
                  selectedTable: undefined,
                  tables: []/*, nData: []*/,
                  /**prevent render more hooks */
                };
              if (selectedCategoryFrom !== this.state.selectedCategoryFrom) {
                _.assign(state, { selectedCategoryFrom });
              }
              if (selectedCategory !== this.state.selectedCategory) {
                _.assign(state, { selectedCategory });
              }
              if (selectedCategoryFrom && selectedCategory) {
                if (
                  selectedCategoryFrom === this.state.selectedCategoryFrom &&
                  selectedCategory === this.state.selectedCategory &&
                  this.selectedDatas
                ) {
                  selectedDatas = this.selectedDatas;
                  const { data } = await getColumnsData(this.editor.getSelfModuleName(), selectedCategoryFrom, selectedCategory);
                  if (this.unmount) {
                    return;
                  }
                  cacheValue = this.getVExpression(selectedCategoryFrom, selectedCategory, selectedDatas, data);
                } else {
                  selectedDatas = getVName(selectedCategoryFrom, selectedCategory);
                  if (~this.editor.getSqlites().indexOf(selectedDatas)) {
                    cacheValue = `${selectedDatas}`;
                  } else {
                    const { data } = await getColumnsData(this.editor.getSelfModuleName(), selectedCategoryFrom, selectedCategory);
                    if (this.unmount) {
                      return;
                    }
                    cacheValue = this.getVExpression(selectedCategoryFrom, selectedCategory, selectedDatas, data);
                  }
                }
                if (this.cacheValue === cacheValue) {
                  // console.error("needn't run2!")
                  return;
                }
              } else {
                cacheValue = "";
              }
              this.selectedDatas = selectedDatas;
              this.setState(state, async () => {
                await this.run_2(cacheValue, true);
                this.cacheValue = cacheValue;
              });
            }}
          ></ChooseDatas>
          <Select
            value={db}
            style={{ width: 200 }}
            placeholder="Select a database"
            allowClear
            notFoundContent={"None available"}
            onDropdownVisibleChange={(open) => {
              if (!open) {
                return;
              }
              const { sqliteNames } = this.state;
              let sqlites = this.editor.getSqlites().sort((a, b) => {
                if (this.selectedDatas && a === this.selectedDatas) {
                  return -1;
                } else if (this.selectedDatas && b === this.selectedDatas) {
                  return 1;
                } else {
                  return a > b ? 1 : a === b ? 0 : -1;
                }
              });
              if (!_.isEqual(sqliteNames, sqlites)) {
                this.setState({ sqliteNames: sqlites });
              }
            }}
            onChange={this.handleSelectDatabaseChange}
          >
            {sqliteNames.length === 0 && (
              <Option value="None available" key={"Database cells NULL"} disabled>
                None available
              </Option>
            )}
            {_.map(sqliteNames, (sqlite) => {
              let label = this.selectedDatas === sqlite ? `[${this.selectedDatas}]` : sqlite;
              return (
                <Option value={sqlite} key={sqlite}>
                  {label}
                </Option>
              );
            })}
          </Select>
          {tables.length !== 0 && dbTable && (
            <Select
              value={selectedTable}
              placeholder={dbTable ? `Select a table` : "Show tables"}
              style={{ width: 200 }}
              allowClear
              notFoundContent={"None available"}
              onChange={this.handleSelectTableChange}
            >
              <OptGroup label="Tables">
                {_.map(tables, (table) => {
                  let schema = getSchema(table);
                  let tableName = getTableName(table);
                  let optionLabel = `${schema ? schema + "." : ""}${tableName}`;
                  return (
                    <Option value={optionLabel} key={optionLabel}>
                      {optionLabel}
                    </Option>
                  );
                })}
              </OptGroup>
            </Select>
          )}
          {tables.length !== 0 && !dbTable && (
            <Cascader style={{ width: 200 }} changeOnSelect
              options={_.map(tables, (table, index) => {
                let schema = getSchema(table);
                let tableName = getTableName(table);
                let optionLabel = `${schema ? schema + "." : ""}${tableName}`;
                return {
                  value: optionLabel,
                  label: optionLabel,
                  children: [{
                    value: "Scan table",
                    label: "Scan table",
                  }, {
                    value: "Query table",
                    label: "Query table",
                  }, {
                    value: "Table name",
                    label: "Table name",
                  }]
                }
              })}
              expandTrigger="hover"
              displayRender={(label) => {
                return label[0];
              }}
              placeholder={`Tables(${tables.length})`}
              onChange={(label) => {
                this.handleSelectTableChange(label[0], label[1]);
              }}
            />
          )}
          {!dbTable && columnsDetailsButton}
          <Button
            icon={<RunCodeIcon fill={needRun} />}
            onClick={() => {
              this.runCode();
            }}
          >
            Run
          </Button>
        </div>

        {dbTable && (
          <div className="d-flex ">
            {filterButton}
            {columnsButton}
            {sortButton}
            {sliceButton}
            {convertSqlButton}
          </div>
        )}
      </div>
    );
  };

  CmComp = () => {
    const { api } = this.props;
    const {
      value,
      openEdit,
      dbTable,
      readOnly,
      dname,
      codeMode,
      syncFile,
      vimEnabled,
      lineNumbers,
      insertMode,
    } = this.state;
    return <Controlled
      className={`code-edit-cm`}
      cursor={{
        line: 0,
        ch: 0,
      }}
      editorDidMount={(cm, value, cb) => {
        contentEditable(cm, this.state.readOnly);
        this.cm = cm;
        this.domRef.current && (this.domRef.current.cm = this.cm);
        this.editorDidMount();
        if (this.editor.getSearchText()) {
          actions.variable(actions.types.OPEN_CODE, [], () => {
            return {
              type: SearchType.cm,
              data: this.ref.current.querySelector(":scope > .data"),
              ret: cmSearchFunc.call(this.cm, this.cm, this.editor.getSearchText(true), this.editor.isWholeWord())
            };
          });
        }
      }}
      editorWillUnmount={(lib) => {
        this.cm = undefined;
        this.domRef.current && (this.domRef.current.cm = this.cm);
        if (this.editor.getSearchText()) {
          actions.variable(actions.types.OPEN_CODE, [], () => {
            return {
              type: SearchType.nocm,
              data: this.ref.current.querySelector(":scope > .data"),
              ret: highligthComp(this.editor.getSearchText(true), value, this.editor.isWholeWord())
            };
          });
        }
      }}
      onFocus={(cm) => {
        api.tooltip.hide();
        !openEdit && this.editCode(true);
        CodeMirror.commands.save = (cm) => {
          this.setState({ value: cm.getValue() });
          this.runCode(true, true);
        };
        CodeMirror.keyMap.pcDefault["Ctrl-F"] = CodeMirror.keyMap.pcDefault["Cmd-F"] = undefined;
        CodeMirror.keyMap.pcDefault["Shift-Ctrl-F"] = CodeMirror.keyMap.pcDefault["Shift-Cmd-F"] = undefined;
        CodeMirror.keyMap.pcDefault["Ctrl-G"] = CodeMirror.keyMap.pcDefault["Cmd-G"] = undefined;
        CodeMirror.keyMap.pcDefault["Shift-Ctrl-G"] = CodeMirror.keyMap.pcDefault["Shift-Cmd-G"] = undefined;
        // CodeMirror.keyMap.pcDefault["Shift-Ctrl-R"] = CodeMirror.keyMap.pcDefault["Shift-Cmd-Alt-F"] = undefined;
        this.focusAnchor();
        let className = `editorjs-anchor-${`code-${dname}`.replace(/\s/g, "_")}`;
        this.editor.recordPosition(className);
      }}
      onBlur={(cm) => {
        this.onBlurFunc();
      }}
      value={value}
      options={_.assign(CodeOptions, {
        placeholder: getCodeModePlaceholder(codeMode),
        mode: codeMode,
        keyMap: vimEnabled ? (insertMode ? "vim-insert" : "vim") : "default",
        theme: this.editor.getSettings().getTheme(),
        readOnly: syncFile || readOnly || dbTable,
        lineNumbers: lineNumbers,
        extraKeys: Object.assign(
          assembling([
            {
              key: "Ctrl-Alt-G",
              value: (cm) => {
                const { error } = this.state;
                if (error && String(error.message).endsWith(" is not defined")) {
                  let m = error.message.match(/(\w+) is not defined$/);
                  if (m && m.length) {
                    let editorKey = this.editor.getTransferKey();
                    let className = `editorjs-anchor-${m[1].replace(/\s/g, "_")}`;
                    let data = document.querySelector(
                      `.editorjs-wrapper.${editorKey}>.editorjs .${className}`
                    );
                    data && data.closest(".code-edit") && setTimeout(() => data.closest(".code-edit").editCode(true));
                  }
                } else if (error && error.input) {
                  let editorKey = this.editor.getTransferKey();
                  let className = `editorjs-anchor-${error.input.replace(/\s/g, "_")}`;
                  let data = document.querySelector(
                    `.editorjs-wrapper.${editorKey}>.editorjs .${className}`
                  );
                  data && data.closest(".code-edit") && setTimeout(() => data.closest(".code-edit").editCode(true));
                }
              },
            },
            {
              key: linkKeys(code_editor_keys["Jump to defining cell"]),
              value: /**@param {CodeMirror.Editor} cm */ (cm) => {
                const { codeMode, db, tableName, dbTable } = this.state;
                let wordRange = cm.findWordAt(cm.getCursor("anchor"));
                let range = cm.getRange(wordRange.anchor, wordRange.head)
                let realValue = this.getRealValue();
                let cell;
                try {
                  cell = realValue.trim() ? parseCell(realValue) : undefined;
                } catch (err) { }
                cell && _.each(cell.references, (v) => {
                  if (cm) {
                    if (v.name === range) {
                      let editorKey = this.editor.getTransferKey();
                      let className = `editorjs-anchor-${v.name.replace(/\s/g, "_")}`;
                      let data = document.querySelector(`.editorjs-wrapper.${editorKey}>.editorjs .${className}`);
                      data && data.closest(".code-edit") && setTimeout(() => data.closest(".code-edit").editCode(true));
                    }
                  }
                });
              },
            },
            {
              key: linkKeys(code_editor_keys["Fold Code"]),
              value: function (cm) {
                cm.foldCode(cm.getCursor());
              },
            },
          ]),
          readOnly
            ? {}
            : _.assign(
              assembling([
                {
                  key: linkKeys(code_editor_keys["Full Screen"]),
                  value: (cm) => {
                    cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                  },
                },
                {
                  key: linkKeys(code_editor_keys["Show Editor LineNumbers"]),
                  value: (cm) => {
                    // cm.setOption("lineNumbers", !cm.getOption("lineNumbers"));
                    actions.variable(actions.types.LINE_NUMBERS, [], () => {
                      return !cm.getOption("lineNumbers");
                    });
                    this.editor.getSettings().setLineNumbers(!cm.getOption("lineNumbers"));
                  },
                },
                {
                  key: linkKeys(code_editor_keys["Auto-indent"]),
                  value: "shiftTabAndUnindentMarkdownList",
                },
                {
                  key: linkKeys(cell_shortcuts_keys["Run Current Cell"]),
                  value: (cm) => {
                    this.runCode(false, true);
                  },
                },
                {
                  key: linkKeys(cell_shortcuts_keys["Force Run Current Cell"]),
                  value: (cm) => {
                    this.runCode(true, true);
                  },
                },
                {
                  key: linkKeys(code_editor_keys["Format Code"]),
                  value: this.formatCode,
                },
                {
                  key: linkKeys(cell_shortcuts_keys["Insert cell above"]),
                  value: (cm) => {
                    let block = api.blocks.getBlockByIndex(api.blocks.getCurrentBlockIndex());
                    let element = block.holder.querySelector(".ce-block-add-up>.icon.fas.fa-plus") || block.holder;
                    element.click();
                  },
                },
                {
                  key: linkKeys(cell_shortcuts_keys["Insert cell below"]),
                  value: (cm) => {
                    let block = api.blocks.getBlockByIndex(api.blocks.getCurrentBlockIndex());
                    let element = block.holder.querySelector(".ce-block-add-down>.icon.fas.fa-plus") || block.holder;
                    element.click();
                  },
                },
                {
                  key: linkKeys(cell_shortcuts_keys["Focus Previos Editable Cell"]),
                  value: (cm) => {
                    api.caret.setToPreviousBlock("start", 0, true);
                  },
                },
                {
                  key: linkKeys(cell_shortcuts_keys["Focus Next Editable Cell"]),
                  value: (cm) => {
                    api.caret.setToNextBlock("start", 0, true);
                  },
                },
                {
                  key: linkKeys(code_editor_keys["Comment Lines"]),
                  value: "toggleComment",
                },
                {
                  key: linkKeys(code_editor_keys["Auto-complete"]),
                  value: "autocomplete",
                },
              ]),
              vimEnabled
                ? {}
                : {
                  Esc: (cm) => {
                    if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                    this.editCode(false);
                  },
                }
            )
        ),
      })}
      onCursorActivity={(cm) => { cursorActivityFunc.call(this, cm) }}
      onBeforeChange={(cm, data, value) => {
        const { codeMode, nData } = this.state;
        let state = { value: value, needRun: true };
        // codeMode === CodeMode.sql &&
        //   nData &&
        //   nData.length &&
        //   _.assign(state, { nData: [] }); /**prevent render more hooks */
        this.setState(state);
      }}
      onChange={(cm, data, value) => {
        if (this.editor.getSearchText()) {
          actions.variable(actions.types.OPEN_CODE, [], () => {
            return {
              type: SearchType.cm,
              data: this.ref.current.querySelector(":scope > .data"),
              ret: cmSearchFunc.call(this.cm, this.cm, this.editor.getSearchText(true), this.editor.isWholeWord())
            };
          });
        }
      }}
      onKeyUp={(cm, e) => {
        if (readOnly) {
          return;
        }

        // If insert mode changed, update our React state
        if (
          this.editor.getSettings().getVimEnabled() &&
          cm.state.vim.insertModeOnKeyDown ^ cm.state.vim.insertMode
        ) {
          this.setState({
            insertMode: cm.state.vim.insertMode,
          });
        }

        if (!(e.ctrlKey || e.metaKey) && !e.altKey && /^[a-z$._]$/gi.test(e.key)) {
          // Don't show hints if the user is entering insert mode.
          if (
            !this.editor.getSettings().getVimEnabled() ||
            (cm.state.vim.insertModeOnKeyDown && cm.state.vim.insertMode)
          ) {
            showHint(cm, CodeMirror.hint.javascript, { globalScope: this.editor && this.editor.sscope || window });
          }
        }
      }}
      onKeyDown={(cm, event) => {
        if (this.editor.getSettings().getVimEnabled()) {
          cm.state.vim.insertModeOnKeyDown = cm.state.vim.insertMode;
        }

        if (readOnly) {
          return;
        }

        if (
          api.toolbar.blockSettingsOpened() &&
          ~["Enter", "Delete", "Backspace", "ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight"].indexOf(
            event.key
          )
        ) {
          event.preventDefault();
          return;
        }

        if (
          ~["Enter", "Delete", "Backspace", "ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight"].indexOf(
            event.key
          )
        ) {
          contentEditable(cm, this.state.readOnly);
        }
      }}
    />
  }

  render() {
    const { saveData, api } = this.props;
    const {
      value,
      openEdit,
      pinCode,
      deadCode,
      safeMode,
      dbTable,
      nData,
      error,
      codeLoading,
      needRun,
      readOnly,
      readwrite_mode,
      view_code,
      dname,
      codeMode,
      openSelect,
      viewInputs,
      syncFile,
      variantName,
      fileAttachments,
      hide,
      confirmLoading,
      visible,
      acceptedFile,
    } = this.state;
    return (
      <div
        ref={this.domRef}
        contentEditable={false}
        onClick={() => {
          this.focusAnchor();
        }}
        className={`code-edit ${hide && readOnly ? "hide" : ""} ${openSelect ? "open-select" : ""} ${readOnly ? "read-only" : `${openEdit ? "edit" : ""}`
          }`}
      >
        <div id={`code-${dname}`} className="code-edit-content">
          {CodeMode.sql === codeMode && <TableComp saveFunc={dbTable && (
            /**
             * 
             * @param {{}} startEndMap 
             * @param {{}} filteredInfo 
             * @param {[]} sortedInfo 
             */
            (startEndMap, filteredInfo, sortedInfo) => {
              const { filters, sorts } = this.state;
              assignFiltersSorts(startEndMap, filteredInfo, sortedInfo, filters, sorts);
              this.setState({ needRun: true, filters, sorts,/*, nData: []*/ }, () => {
                this.runCode();
              });
              this.needSave = true;
            })} nData={nData}></TableComp>}
          <div
            className={`${deadCode || codeMode === CodeMode.javascript || safeMode || hide || (CodeMode.sql === codeMode && !error)
              ? "hide" : "display"} code-edit-result d-flex justity-content-start`}
            ref={this.ref}
          >
            <ErrorComp component={this} ></ErrorComp>
            <div className="declare">&nbsp;</div>
            <div className="data flex-fill"></div>
          </div>
          <div
            className={`${!((syncFile && variantName) || viewInputs || (CodeMode.sql === codeMode && (openEdit || pinCode || safeMode || !nData || !nData.length) && this.cacheValue.trim()))
              ? "hide" : "display"} sync-file code-edit-result d-flex justity-content-start`}
            ref={this.syncRef}
          >
            <div className="declare">&nbsp;</div>
            <div className="data flex-fill"></div>
          </div>
          {((view_code || safeMode || hide && !readOnly) || readwrite_mode !== ReadWriteOptions[0] ||
            (!readOnly && readwrite_mode === ReadWriteOptions[2])) && (
              <div className={`code-edit-wrapper ${openEdit || pinCode || safeMode ? "" : "hide"}`}>
                {!readOnly && (
                  <div className="code-edit-wrapper-tools normal-icon">
                    <PinIcon
                      onMouseEnter={(event) => {
                        api.tooltip.show(event.currentTarget, `${pinCode || safeMode ? "Unpin cell" : "Pin cell"}`, {
                          placement: "bottom",
                        });
                      }}
                      onMouseLeave={(event) => {
                        api.tooltip.hide();
                      }}
                      onMouseUp={() => {
                        this.pinCodeFunc();
                      }}
                      className={`${pinCode || safeMode ? "" : "anticon-inactive"}`}
                    />
                    <Dropdown
                      disabled={deadCode || CodeMode.sql === codeMode}
                      overlay={
                        <Menu className="normal-icon">
                          {_.map(_.values(CodeMode), (mode) => {
                            return (
                              <Menu.Item title={mode} key={mode}>
                                <Icon className="icon" component={getCodeModeSvg(mode)}
                                  onClick={() => {
                                    if (codeMode === mode) {
                                      return;
                                    }
                                    this.setState((state) => {
                                      let nstate = { codeMode: mode, needRun: true };
                                      let trimValue = value.trim();
                                      if (mode === CodeMode.htmlmixed && state.codeMode !== CodeMode.htmlmixed) {
                                        let match = trimValue.match(/^html\`([\s\S]*)\`$/i);
                                        if (null !== match && match.length === 2) {
                                          _.assign(nstate, { value: match[1] });
                                        }
                                      } else if (mode === CodeMode.markdown && state.codeMode !== CodeMode.markdown) {
                                        let match = trimValue.match(/^md\`([\s\S]*)\`$/i);
                                        if (null !== match && match.length === 2) {
                                          _.assign(nstate, { value: match[1] });
                                        }
                                      } else if (mode === CodeMode.python && state.codeMode !== CodeMode.python) {
                                        let match = trimValue.match(/^py\`([\s\S]*)\`$/i);
                                        if (null !== match && match.length === 2) {
                                          _.assign(nstate, { value: match[1] });
                                        }
                                      } else if (mode === CodeMode.tex && state.codeMode !== CodeMode.tex) {
                                        let match = trimValue.match(/^tex\`([\s\S]*)\`$/i);
                                        if (null !== match && match.length === 2) {
                                          _.assign(nstate, { value: match[1] });
                                        }
                                      } else if (
                                        mode === CodeMode.javascript2 &&
                                        state.codeMode === CodeMode.markdown
                                      ) {
                                        let match = trimValue.match(/^md\`([\s\S]*)\`$/i);
                                        if (null === match) {
                                          _.assign(nstate, { value: `md\`${trimValue}\`` });
                                        }
                                      } else if (
                                        mode === CodeMode.javascript2 &&
                                        state.codeMode === CodeMode.python
                                      ) {
                                        let match = trimValue.match(/^py\`([\s\S]*)\`$/i);
                                        if (null === match) {
                                          _.assign(nstate, { value: `py\`${trimValue}\`` });
                                        }
                                      } else if (mode === CodeMode.javascript2 && state.codeMode === CodeMode.tex) {
                                        let match = trimValue.match(/^tex\`([\s\S]*)\`$/i);
                                        if (null === match) {
                                          _.assign(nstate, { value: `tex\`${trimValue}\`` });
                                        }
                                      } else if (
                                        mode === CodeMode.javascript2 &&
                                        state.codeMode === CodeMode.htmlmixed
                                      ) {
                                        let match = trimValue.match(/^html\`([\s\S]*)\`$/i);
                                        if (null === match) {
                                          _.assign(nstate, { value: `html\`${trimValue}\`` });
                                        }
                                      }
                                      return nstate;
                                    });
                                  }}
                                />
                              </Menu.Item>
                            );
                          })}
                        </Menu>
                      }
                      trigger={["click"]}
                    >
                      <Icon component={getCodeModeSvg(codeMode)}
                        onMouseEnter={(event) => {
                          api.tooltip.show(event.currentTarget, `${codeMode}`, { placement: "bottom" });
                        }}
                        onMouseLeave={(event) => {
                          api.tooltip.hide();
                        }}
                      />
                    </Dropdown>
                  </div>
                )}
                {CodeMode.sql === codeMode && <this.DatabaseComp></this.DatabaseComp>}
                {codeMode !== CodeMode.sql && (
                  <div className="code-edit-wrapper-icons d-flex align-items-center">
                    {syncFile && (
                      <i
                        title={`Proxy Array`}
                        className="fas fa-box"
                        onMouseUp={() => {
                          this.configSyncFile();
                        }}
                      ></i>
                    )}
                    {!!(fileAttachments && fileAttachments.size) && (
                      <Dropdown
                        overlay={
                          <Menu>
                            {_.map(Array.from(fileAttachments.keys()), (key, index) => {
                              return (
                                <Menu.Item key={`${key}`}>
                                  <div>
                                    <Button
                                      type="link"
                                      title="download"
                                      onClick={() => {
                                        this.editor.downloadFile(key);
                                      }}
                                    >
                                      {key}
                                    </Button>
                                    <Button
                                      type="primary"
                                      onClick={() => {
                                        this.showModal(key);
                                      }}
                                    >
                                      {" "}
                                      Replace{" "}
                                    </Button>
                                  </div>
                                  <Modal
                                    zIndex={Z_INDEX}
                                    title={`Replace File`}
                                    visible={visible}
                                    onOk={this.handleOk}
                                    confirmLoading={confirmLoading}
                                    onCancel={this.handleCancel}
                                    footer={[
                                      <Button key="back" onClick={this.handleOk}>
                                        Upload and replace
                                      </Button>,
                                    ]}
                                  >
                                    <Text type="secondary">
                                      All references to {key} in the selected cell will be replaced with references to
                                      the new file.
                                    </Text>
                                    <MyDropzone
                                      input={true}
                                      multiple={false}
                                      handleFilesFunc={(acceptedFiles) => {
                                        acceptedFiles.length >= 1 && this.setState({ acceptedFile: acceptedFiles[0] });
                                      }}
                                    >
                                      <div className="text-center">
                                        <div>Drag and drop files to this panel or</div>
                                        <div className="btn btn-light">Select a file</div>
                                        {acceptedFile && (
                                          <ul>
                                            <li key={acceptedFile.path}>
                                              {acceptedFile.path} - {acceptedFile.size} bytes
                                            </li>
                                          </ul>
                                        )}
                                      </div>
                                    </MyDropzone>
                                    <small className="font-italic text-secondary"> Maximum file size: 50 MB </small>
                                  </Modal>
                                </Menu.Item>
                              );
                            })}
                          </Menu>
                        }
                        trigger={["click"]}
                      >
                        <span
                          onMouseEnter={(event) => {
                            api.tooltip.show(event.currentTarget, `View attached files`, { placement: "top" });
                          }}
                          onMouseLeave={(event) => {
                            api.tooltip.hide();
                          }}
                        >
                          {fileAttachments.size || ""}
                          <AttachmentIcon fill={fileAttachments.size || false}></AttachmentIcon>
                        </span>
                      </Dropdown>
                    )}
                    <RunCodeIcon
                      onMouseEnter={(event) => {
                        api.tooltip.show(
                          event.currentTarget,
                          createElemnt(`<div>Run Code</div>
                                <div class="ce-toolbar__plus-shortcut">${linkKeys(
                            cell_shortcuts_keys["Run Current Cell"]
                          )}</div>`),
                          { placement: "top" }
                        );
                      }}
                      onMouseLeave={(event) => {
                        api.tooltip.hide();
                      }}
                      onMouseUp={() => this.runCode(false, true)}
                      fill={needRun}
                    />
                  </div>
                )}
                {!dbTable && (openEdit || pinCode || safeMode) && <this.CmComp></this.CmComp>}
              </div>
            )}
        </div>
        {codeLoading && (
          <div className="code-loading-top">
            <Spin spinning={true} indicator={<Loading3QuartersOutlined style={{ fontSize: 24, }} spin />}>
              <div className="code-full-div"></div>
            </Spin>
          </div>
        )}
      </div>
    );
  }
}
