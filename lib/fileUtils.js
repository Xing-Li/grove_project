const _ = require("lodash");
const path = require('path');
const { ROOT_PATH ,TEMP_PATH } = require('../Constant');

const fileSeparator = '/';
const DefaultFileType = "text/plain";
/**
 * notice all init properties need false or 0
 */
const DefaultFileSettings = {
    /**latest version*/
    version: 0,
    /**final version*/
    final: 0,
    /**latest version file modified time*/
    versionMtimeMs: 0,
    /**preview Image upload*/
    previewImageUpload: false,
    /**version message */
    messages: {}
};
const DefaultFileData = {
    fileKey: "fileKey", name: "fileName", selected: false, type: DefaultFileType, size: 0, mtimeMs: 0,
    version: 0, final: 0, versionMtimeMs: 0, previewImageUpload: false, messages: null, shareData: null,
}
delete DefaultFileData.selected;
const DefaultFolderData = {
    folderKey: "folderKey", name: "folderName", selected: false, type: 'dir'
}
delete DefaultFolderData.selected;
const fileNameReg = /^[0-9a-zA-Z-+_\.\(\) ]*$/;
const folderNameReg = /^[0-9a-zA-Z-+_]*$/;
const imageTypes = "jpg|png|gif|ps|jpeg|jfif".split("|")
const imageTypeReg = new RegExp(`\\.(${imageTypes.join("|")})$`);
const acceptedImageTypes = _.reduce(imageTypes, (prev, curr) => {
    prev.push(`image/${curr}`)
    return prev;
}, []);
const videoTypes = "mp4|webm|ogg".split("|")
const videoTypeReg = new RegExp(`\\.(${videoTypes.join("|")})$`);
const acceptedVideoTypes = _.reduce(videoTypes, (prev, curr) => {
    prev.push(`video/${curr}`)
    prev.push(`application/${curr}`)
    prev.push(`text/${curr}`)
    return prev;
}, []);
const audioTypes = "mp3|webm|ogg|ogv|wave|wav|x-wav|x-pn-wav".split("|")
const audioTypeReg = new RegExp(`\\.(${audioTypes.join("|")})$`);
const acceptedAudioTypes = _.reduce(audioTypes, (prev, curr) => {
    prev.push(`audio/${curr}`)
    prev.push(`application/${curr}`)
    prev.push(`text/${curr}`)
    return prev;
}, []);
const TmpDir = TEMP_PATH;
const pngSig = [0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a];
const jpgSig = [0xff, 0xd8, 0xff];
const gifSig = [0x47, 0x49, 0x46, 0x38, [0x37, 0x39], 0x61];

function checkSig(buffer, sig) {
    let len = sig.length;
    for (let i = 0; i < len; i++) {
        let b = buffer[i],
            s = sig[i],
            m = false;
        if ('number' == typeof s) {
            m = s === b;
        }
        else {
            for (let k in s) {
                let o = s[k];
                if (o === b) {
                    m = true;
                }
            }
        }
        if (!m) {
            return false;
        }
    }
    return true;
}

function getImageType(buffer) {
    if (checkSig(buffer, pngSig)) return "png";
    if (checkSig(buffer, jpgSig)) return "jpg";
    if (checkSig(buffer, gifSig)) return "gif";
    return false;
}

/**
 * 'image/jpg', 'image/png', 'image/gif', 'image/ps', 'image/jpeg', 'image/jfif'
 * @param {*} file 
 * @returns true/false or undefined if file undefined
 */
function isFileImage(file) {
    return file && acceptedImageTypes.includes(file['type'].toLowerCase())
}
function isFileNoEdit(file) {
    return isFileImage(file) || isFileGzip(file) || isFileZip(file) || isFileDb(file) || isFileVideo(file) || isFileExcel(file) || isFileAudio(file) || isFileGraphxr(file);
}
function isFileGzip(file) {
    return file && (file['type'].toLowerCase() === 'text/tgz' || file['type'].toLowerCase() === 'application/gzip');
}
function isFileZip(file) {
    return file && (file['type'].toLowerCase() === 'text/zip' || file['type'].toLowerCase() === 'application/zip');
}
function isFileGraphxr(file) {
    return file && (file['type'].toLowerCase() === 'text/graphxr' ||
        file['type'].toLowerCase() === 'graphxr');
}
function isFileVideo(file) {
    return file && (acceptedVideoTypes.includes(file['type'].toLowerCase()));
}
function isFileAudio(file) {
    return file && (acceptedAudioTypes.includes(file['type'].toLowerCase()));
}
/**
 * excel file
 * @param {*} file 
 */
function isFileExcel(file) {
    return file && (file['type'].toLowerCase() === 'text/xlsx' || file['type'].toLowerCase() === 'text/xls');
}
/**
 * Json file
 * @param {*} file 
 */
function isFileJson(file) {
    return file && file['type'].toLowerCase() === 'text/json';
}

function isFileDb(file) {
    return file && file['type'].toLowerCase() === 'text/db';
}

function isFilePlain(file) {
    return file && file['type'].toLowerCase() === 'text/plain';
}
/**
 * js file
 * @param {*} file 
 */
function isFileJs(file) {
    return file && file['type'].toLowerCase() === 'text/js';
}

function isFileSvg(file) {
    return file && file['type'].toLowerCase() === 'text/svg';
}
/**
 * markdown file
 * @param {*} file 
 */
function isFileMd(file) {
    return file && (
        ~['text/md', 'text/markdown'].indexOf(file['type'].toLowerCase()) ||
        (
            ~file['name'].indexOf(".") &&
            ~['md', 'markdown'].indexOf(file['name'].substring(file['name'].indexOf(".") + 1).toLowerCase())
        )
    );
}
/**
 * csv file
 * @param {*} file 
 */
function isFileCsv(file) {
    return file && file['type'].toLowerCase() === 'text/csv';
}

const isFileEditorJs = file => {
    if (!file) return false;
    else if (isFileJs(file)) return false;
    else if (isFileSvg(file)) return false;
    else if (isFileJson(file)) return false;
    else if (isFileCsv(file)) return false;
    else if (isFileNoEdit(file)) return false;
    else return true;
}

const getFileType = (fileKey) => {
    let type;
    if (imageTypeReg.test(fileKey.toLowerCase())) {
        type = `image/${imageTypeReg.exec(fileKey.toLowerCase())[0].substring(1)}`
    } else if (videoTypeReg.test(fileKey.toLowerCase())) {
        type = `video/${videoTypeReg.exec(fileKey.toLowerCase())[0].substring(1)}`
    } else if (audioTypeReg.test(fileKey.toLowerCase())) {
        type = `audio/${audioTypeReg.exec(fileKey.toLowerCase())[0].substring(1)}`
    } else if (fileKey.toLowerCase().endsWith('.graphxr')) {
        return 'graphxr'
    } else {
        let fileName = getFileName(fileKey);
        type = (fileName && ~fileName.lastIndexOf('.')) ? `text/${fileName.substring(fileName.lastIndexOf('.') + 1)}` : DefaultFileType;
    }
    return type;
}

const getFileName = (fileKey) => {
    return (fileKey && ~fileKey.lastIndexOf('/')) ? fileKey.substring(fileKey.lastIndexOf('/') + 1) : fileKey
}

module.exports = {
    fileSeparator, DefaultFileType, DefaultFileData, DefaultFolderData, DefaultFileSettings, isFileGzip, isFileZip,
    fileNameReg, folderNameReg, imageTypeReg, TmpDir, getImageType, isFileEditorJs, getFileType, getFileName
}