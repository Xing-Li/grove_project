import { CloseOutlined } from '@ant-design/icons';
import { Drawer } from 'antd';
import _ from 'lodash';
import PropTypes from "prop-types";
import React from 'react';
import Draggable from 'react-draggable';
import { DEBUG_DRAG } from 'util/helpers';
const MAX_SCALE = 100;
/**
 * draggable drawer
 */
export default class DrawerEx extends React.Component {
    static propTypes = {
        dkey: PropTypes.string,
        children: PropTypes.any,
        className: PropTypes.string,
        direction: PropTypes.string,
        dragCallback: PropTypes.func,
        initHeight: PropTypes.number,
        fullScreen: PropTypes.bool,
        initWidth: PropTypes.number
    }

    static defaultProps = {};
    constructor(props) {
        super(props);
        const { drawerKey, initHeight, fullScreen, initWidth, direction, children, className, dragCallback, ...propsa } = this.props;
        this.state = {
            drawerKey: drawerKey,
            activeDrags: 0,
            deltaPosition: {
                x: 0, y: 0
            },
            controlledPosition: {
                x: 0, y: 0
            },
            initHeight, fullScreen, initWidth, direction, propsa
        };
    }

    componentDidUpdate(prevProps, prevState) {
        let state = {};
        if (!_.isEqual(prevProps, this.props)) {
            const { drawerKey, initHeight, fullScreen, initWidth, direction, children, className, dragCallback, ...propsa } = this.props;
            _.assign(state, { initHeight, fullScreen, initWidth, direction, propsa });
            if (prevProps.drawerKey !== drawerKey) {
                _.assign(state, {
                    drawerKey,
                    activeDrags: 0,
                    deltaPosition: {
                        x: 0, y: 0
                    },
                    controlledPosition: {
                        x: 0, y: 0
                    },
                })
            }
        }
        !_.isEmpty(state) && this.setState(state);
    }

    handleDrag = (e, ui) => {
        const { deltaPosition: { x, y }, direction } = this.state;
        let n = ("left" === direction || "top" === direction) ?
            {
                x: x - ui.deltaX,
                y: y - ui.deltaY,
            } : {
                x: x + ui.deltaX,
                y: y + ui.deltaY,
            }
        this.setState({
            deltaPosition: n,
            controlledPosition: { x: ui.x, y: ui.y }
        }, () => {
            const { dragCallback } = this.props;
            dragCallback && dragCallback(n);
            DEBUG_DRAG && console.log("drag")
        });
    };

    onStart = () => {
        this.setState({ activeDrags: ++this.state.activeDrags });
    };

    onStop = () => {
        this.setState({ activeDrags: --this.state.activeDrags });
    };

    // For controlled component
    adjustXPos = (e) => {
        e.preventDefault();
        e.stopPropagation();
        const { x, y } = this.state.controlledPosition;
        this.setState({ controlledPosition: { x: x - 10, y } });
    };

    adjustYPos = (e) => {
        e.preventDefault();
        e.stopPropagation();
        const { controlledPosition } = this.state;
        const { x, y } = controlledPosition;
        this.setState({ controlledPosition: { x, y: y - 10 } });
    };

    onControlledDrag = (e, position) => {
        const { x, y } = position;
        this.setState({ controlledPosition: { x, y } });
    };

    onControlledDragStop = (e, position) => {
        this.onControlledDrag(e, position);
        this.onStop();
    };

    render() {
        const { children, className } = this.props;
        const { initHeight, fullScreen, initWidth, direction, propsa } = this.state;
        const axis = ("left" === direction || "right" === direction) ? "x" : "y";
        const { deltaPosition, controlledPosition } = this.state;
        const dragHandlers = { onStart: this.onStart, onStop: this.onStop };
        let obj = { placement: direction };
        let style = {};
        let bounds;
        if (axis === "x") {
            obj.width = fullScreen ? window.innerWidth : (initWidth - deltaPosition.x.toFixed(0));
            style[direction] = initWidth;
            bounds = {
                left: window.innerWidth - initWidth > MAX_SCALE ? -(window.innerWidth - initWidth - MAX_SCALE) : 0,
                right: initWidth - MAX_SCALE > 0 ? initWidth - MAX_SCALE : 0
            }
        } else {
            obj.height = fullScreen ? window.innerHeight : (initHeight - deltaPosition.y.toFixed(0));
            style[direction] = initHeight;
            bounds = {
                top: window.innerHeight - initHeight > MAX_SCALE ? -(window.innerHeight - initHeight - MAX_SCALE) : 0,
                bottom: initHeight - MAX_SCALE > 0 ? initHeight - MAX_SCALE : 0,
            }
        }

        return <Drawer className={`${className || ""}`}
            {...obj} {...propsa} closeIcon={<CloseOutlined title='Close (Esc)' />}>
            <div className={(axis === "y" ? "drawer-hor" : "drawer-type")}>
                <Draggable position={controlledPosition} bounds={bounds} axis={axis} onDrag={this.handleDrag} {...dragHandlers}>
                    <div className={`resize ${(!propsa.visible || fullScreen) ? "hide" : ""}`} style={style}></div>
                </Draggable>
                {children}
            </div>
        </Drawer>
    }
}
