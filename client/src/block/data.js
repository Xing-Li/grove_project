import { DefaultFileData } from "../util/localstorage";
import { isFileEditorJs, isFileJson } from "../util/helpers";
import { CodeMode, showToast } from "../util/utils";

const version = "2.19.1";

const InitData = {
    blocks: [
    ],
    "version": version
};
const Data = {
    blocks: [
        {
            "type": "codeTool",
            "data": {
                "codeData": {
                    "value": `md\`# Untitled\``,
                    "pinCode": true
                }
            }
        }
    ],
    "version": version
};
/**
 * csv json can not large then xxKB
 * @param {*} fileData 
 */
export function isLarge(fileData) {
    if (!fileData) {
        return 0;
    }
    const JsonMaxSize = 512;
    if (isFileJson(fileData) && fileData.size > JsonMaxSize * 1024) {
        return JsonMaxSize;
    }
    return 0;
}
/**
 * 
 * @param {DefaultFileData} fileData 
 */
export function largeDataFile(fileData) {
    if (!fileData) {
        return;
    }
    let largeSize = isLarge(fileData);
    if (largeSize) {
        return {
            blocks: [
                {
                    "type": "codeTool",
                    "data": {
                        "codeData": {
                            "value": `md\`#### ${fileData.type} ${fileData.fileKey
                                } too large ${new Number(fileData.size / 1024).toFixed(2)}KB > ${largeSize}KB! \``,
                            "pinCode": false
                        }
                    }
                }
            ],
            "version": version
        }
    }
};
export const checkContent = function (fileData, content) {
    if (isFileEditorJs(fileData)) {
        return !errorDataFile(content);
    } else {
        return true;
    }
}
/**
 * 
 * @param {string} content 
 */
export function errorDataFile(content) {
    try {
        if (content.startsWith(`<!DOCTYPE html>`)) {
            return {
                blocks: [
                    {
                        "type": "codeTool",
                        "data": {
                            "codeData": {
                                "value": `md\`#### FILE NOT FOUND! \``,
                                "pinCode": false
                            }
                        }
                    }
                ],
                "version": version
            }
        }
        let json = JSON.parse(content);
        if (json.version === version && json.blocks && (json.blocks instanceof Array)) {
            return;
        }
        return {
            blocks: [
                {
                    "type": "codeTool",
                    "data": {
                        "codeData": {
                            "value":
                                `md\`#### Explain file error, not right data: version -> ${json.version}, blocks -> ${json.blocks}\``,
                            "pinCode": false
                        }
                    }
                }
            ],
            "version": version
        }
    } catch (e) {
        // console.error(content);
        return {
            blocks: [
                {
                    "type": "codeTool",
                    "data": {
                        "codeData": {
                            "value": `md\`#### Explain file error, not right json: ${e.message}! \``,
                            "pinCode": false
                        }
                    }
                }
            ],
            "version": version
        }
    }
};

export function jsonDataFunc(jsonText) {
    return {
        blocks: [
            {
                "type": "codeTool",
                "data": {
                    "codeData": {
                        "value": `{return ${jsonText}}`,
                        "pinCode": false
                    }
                }
            }
        ],
        "version": version
    };
}

export function jsDataFunc(jsText) {
    return {
        blocks: [
            {
                "type": "codeTool",
                "data": {
                    "codeData": {
                        "value": jsText,
                        "pinCode": true,
                        "deadCode": true,
                        codeMode: CodeMode.javascript
                    }
                }
            }
        ],
        "version": version
    };
}
export function htmlDataFunc(htmlText) {
    return {
        blocks: [
            {
                "type": "codeTool",
                "data": {
                    "codeData": {
                        "value": htmlText,
                        "pinCode": true,
                        "deadCode": false,
                        codeMode: CodeMode.htmlmixed
                    }
                }
            }
        ],
        "version": version
    };
}
export function svgDataFunc(svgText) {
    return {
        blocks: [
            {
                "type": "codeTool",
                "data": {
                    "codeData": {
                        "value": `md\`${svgText}\``,
                        "pinCode": false,
                        "readOnly": true,
                    }
                }
            }
        ],
        "version": version
    };
}

/**
 * 
 * @param {string} csvText 
 */
export function csvDataFunc(csvText) {
    let content = [];
    String(csvText).split(/\r?\n/).forEach((line) => {
        content.push(line.split(","));
    })
    const CsvMaxSize = 32;
    if (csvText.length < CsvMaxSize * 1024) {
        return {
            blocks: [
                {
                    "type": "codeTool",
                    "data": {
                        "codeData": {
                            "pinCode": false,
                            "value": `Inputs.table(penguins)`
                        }
                    }
                },
                {
                    "type": "table",
                    "data": {
                        "content": content
                    }
                },
                {
                    "type": "codeTool",
                    "data": {
                        "codeData": {
                            "pinCode": false,
                            // "hide": true,
                            "readOnly": true,
                            "value": `text = \`${csvText}\``
                        }
                    }
                },
                {
                    "type": "codeTool",
                    "data": {
                        "codeData": {
                            "pinCode": false,
                            "hide": true,
                            "readOnly": true,
                            "value": `Table = Inputs.table // deprecated alias`
                        }
                    }
                },
                {
                    "type": "codeTool",
                    "data": {
                        "codeData": {
                            "pinCode": false,
                            "hide": true,
                            "readOnly": true,
                            "value": `penguins = await csv( text,{ typed : true})`
                        }
                    }
                },
                {
                    "type": "codeTool",
                    "data": {
                        "codeData": {
                            "pinCode": false,
                            "hide": true,
                            "readOnly": true,
                            "value": `async function dsv(text, delimiter, {array = false, typed = false} = {}) {
                            const [d3] = await Promise.all([require("d3-dsv@2.0.0/dist/d3-dsv.min.js")]);
                            return (delimiter === "\t"
                                ? (array ? d3.tsvParseRows : d3.tsvParse)
                                : (array ? d3.csvParseRows : d3.csvParse))(text, typed && d3.autoType);
                          }`
                        }
                    }
                },
                {
                    "type": "codeTool",
                    "data": {
                        "codeData": {
                            "pinCode": false,
                            "hide": true,
                            "readOnly": true,
                            "value": `async function csv(text,options) {
                            return dsv(text, ",", options);
                        }`
                        }
                    }
                },
            ],
            "version": version
        };
    } else {
        return {
            blocks: [
                {
                    "type": "codeTool",
                    "data": {
                        "codeData": {
                            "pinCode": false,
                            "value": `Inputs.table(penguins)`
                        }
                    }
                },
                {
                    "type": "codeTool",
                    "data": {
                        "codeData": {
                            "pinCode": false,
                            // "hide": true,
                            "readOnly": true,
                            "value": `text = \`${csvText}\``
                        }
                    }
                },
                {
                    "type": "codeTool",
                    "data": {
                        "codeData": {
                            "pinCode": false,
                            "hide": true,
                            "readOnly": true,
                            "value": `Table = Inputs.table // deprecated alias`
                        }
                    }
                },
                {
                    "type": "codeTool",
                    "data": {
                        "codeData": {
                            "pinCode": false,
                            "hide": true,
                            "readOnly": true,
                            "value": `penguins = await csv( text,{ typed : true})`
                        }
                    }
                },
                {
                    "type": "codeTool",
                    "data": {
                        "codeData": {
                            "pinCode": false,
                            "hide": true,
                            "readOnly": true,
                            "value": `async function dsv(text, delimiter, {array = false, typed = false} = {}) {
                            const [d3] = await Promise.all([require("d3-dsv@2.0.0/dist/d3-dsv.min.js")]);
                            return (delimiter === "\t"
                                ? (array ? d3.tsvParseRows : d3.tsvParse)
                                : (array ? d3.csvParseRows : d3.csvParse))(text, typed && d3.autoType);
                          }`
                        }
                    }
                },
                {
                    "type": "codeTool",
                    "data": {
                        "codeData": {
                            "pinCode": false,
                            "hide": true,
                            "readOnly": true,
                            "value": `async function csv(text,options) {
                            return dsv(text, ",", options);
                        }`
                        }
                    }
                },
            ],
            "version": version
        };
    }
}

export function imageDataFunc(url, caption) {
    return {
        blocks: [
            {
                type: "simpleImage",
                data: {
                    url: url,
                    caption: caption,
                    withBorder: true,
                    withBackground: true,
                    stretched: false
                }
            }
        ],
        "version": version
    };
}

export function gzipDataFunc(content, fileName) {
    return {
        blocks: [
            {
                "type": "codeTool",
                "data": {
                    "codeData": {
                        "value": `${fileName} is a gzip file`,
                        "pinCode": true,
                        "deadCode": true
                    }
                }
            }
        ],
        "version": version
    };
}
export function zipDataFunc(content, fileName) {
    return {
        blocks: [
            {
                "type": "codeTool",
                "data": {
                    "codeData": {
                        "value": `${fileName} is a zip file`,
                        "pinCode": true,
                        "deadCode": true
                    }
                }
            }
        ],
        "version": version
    };
}
export function graphxrDataFunc(content, fileName) {
    return {
        blocks: [
            {
                "type": "codeTool",
                "data": {
                    "codeData": {
                        "value": `${fileName} is a graphxr file`,
                        "pinCode": true,
                        "deadCode": true
                    }
                }
            }
        ],
        "version": version
    };
}

export function dbDataFunc(content, fileName) {
    return {
        blocks: [
            {
                "type": "codeTool",
                "data": {
                    "codeData": {
                        "value": `${fileName} is a database file`,
                        "pinCode": true,
                        "deadCode": true
                    }
                }
            }
        ],
        "version": version
    };
}

export function excelDataFunc(url, fileName) {
    return {
        blocks: [
            {
                "type": "codeTool",
                "data": {
                    "codeData": {
                        "value": `FileAttachment("${url}").xlsx()`,
                        "pinCode": true,
                    }
                }
            }
        ],
        "version": version
    };
}

export function arrowDataFunc(url, fileName) {
    return {
        blocks: [
            {
                "type": "codeTool",
                "data": {
                    "codeData": {
                        "value": `FileAttachment("${url}").arrow()`,
                        "pinCode": true,
                    }
                }
            }
        ],
        "version": version
    };
}


export function audioDataFunc(url, fileName) {
    let tag = fileName.substring(fileName.indexOf(".") + 1);
    tag = (tag === "ogv") ? "ogg" : tag;
    return {
        blocks: [
            {
                "type": "codeTool",
                "data": {
                    "codeData": {
                        "value": `html\`<audio   style='max-width:640px;width:100%;border-radius:2px'  controls>
                        <source src="${url}" type="audio/${tag}">
                      Your browser does not support the audio tag.
                      </audio >\``,
                        "pinCode": false,
                        // "hide": true,
                        "readOnly": true,
                    }
                }
            }
        ],
        "version": version
    };
}
export function videoDataFunc(url, fileName) {
    let tag = fileName.substring(fileName.indexOf(".") + 1);
    return {
        blocks: [
            {
                "type": "codeTool",
                "data": {
                    "codeData": {
                        "value": `html\`<video style='max-width:640px;width:100%;border-radius:2px'  controls>
                        <source src="${url}" type="video/${tag}">
                      Your browser does not support the video tag.
                      </video>\``,
                        "pinCode": false,
                        // "hide": true,
                        "readOnly": true,
                    }
                }
            }
        ],
        "version": version
    };
}

/**
 * editorJs's default content
 */
export const DefaultData = JSON.stringify(Data);
export const DefaultInitData = JSON.stringify(InitData);